/*$('#form_evaluated').change(function() {
  $('#form_evaluator_name').val('');
});*/

$('#form_evaluator_name').focus(function() {
    $.ajax({
        type: "GET",
        data: {form_id : $('#form_id').val()},
        url: globals.basepath + "/admin/pollscheduling/" + pollscheduling + "/evaluator/ajax",
        success: function(response_data){
          if (response_data != ''){
            var object_data = eval('('+response_data+')');
            var wievaluators = object_data;
            $('#form_evaluator_name').autocomplete({
              minLength: 0,
              source: wievaluators,
              select: function(event, ui) {
                //On select, update hidden id
                $('#form_iden').val(ui.item.id);
              }
            });  
          }

        }
   });
});
