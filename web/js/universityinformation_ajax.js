$('.universityselect').change( function()  {
    $.ajax({
        type: "GET",
        data: {company_id : $(this).val()},
        url: globals.basepath + "/admin/universityinformation/ajax/formelementsbyuniversity",
        success: function(response_data){
            if (response_data != ''){
                
                var object_data = eval('('+response_data+')');
                
                $('.select_companyfaculty').html(object_data.select_companyfaculty).show();
                $('#fishman_authbundle_workinginformationtype_companyfaculty_id').val('');
                
                $('.select_companycareer').html(object_data.select_companycareer).show();
                $('#fishman_authbundle_workinginformationtype_companycareer_id').val('');
                
                $('.select_headquarter').html(object_data.select_headquarter).show();
                $('#fishman_authbundle_workinginformationtype_headquarter_id').val('');

                $.uniform.update();
                $('.companyfaculty').uniform();
                $('.companycareer').uniform();
                $('.headquarter').uniform();
                
            }
            else{
                $('.select_companyfaculty').html('<em>No se encontro información para esta Empresa</em>');
                $('.select_companycareer').html('<em>No se encontro información para esta Empresa</em>');
                $('.select_headquarter').html('<em>No se encontro información para esta Empresa</em>');
            }
            
            $('.companyfaculty').change( function() {
                select_companyfaculty($(this).val());
                $('#fishman_authbundle_workinginformationtype_companyfaculty_id').val($(this).val());
            });
            
            $('.companycareer').change( function() {
                $('#fishman_authbundle_workinginformationtype_companycareer_id').val($(this).val());
            });
            
            $('.headquarter').change( function() {
                $('#fishman_authbundle_workinginformationtype_headquarter_id').val($(this).val());
            });
       }
   });
});

function select_companyfaculty(facultyid) {
    $.ajax({
        type: "GET",
        data: { faculty_id : facultyid },
        url: globals.basepath + "/admin/universityinformation/ajax/formelementsbycompanyfaculty",
        success: function(response_data){
            if (response_data != ''){
                
                var object_data = eval('('+response_data+')');
                
                $('.select_companycareer').html(object_data.select_companycareer).show();
                $('#fishman_authbundle_workinginformationtype_companycareer_id').val('');
                
                $.uniform.update();
                $('.companycareer').uniform();
                
            }
            else{
                $('.select_companycareer').html('<em>No se encontro información para esta Empresa</em>');
            }
            
            $('.companycareer').change( function() {
                $('#fishman_authbundle_workinginformationtype_companycareer_id').val($(this).val());
            });
       }
   });
}

$('.companyfaculty').change( function() {
    select_companyfaculty($('.universityselect').val(), $(this).val());
    $('#fishman_authbundle_workinginformationtype_companyfaculty_id').val($(this).val());
});

$('.companycareer').change( function() {
    $('#fishman_authbundle_workinginformationtype_companycareer_id').val($(this).val());
});

$('.headquarter').change( function() {
    $('#fishman_authbundle_workinginformationtype_headquarter_id').val($(this).val());
});
