$('.countryselect').change( function() {
    $.ajax({
        type: "GET",
        data: { ubigeo_id : $(this).val(), ubigeo_type : 'department' },
        url: globals.basepath + "/admin/headquarter/ajax/formelementsbyubigeo",
        success: function(response_data){
            if (response_data != ''){
                
                var object_data = eval('('+response_data+')');
                
                $('.select_department').html(object_data.select_ubigeo);
                $('.departmentvalue').val('');
                
                $.uniform.update();
                $('.department').uniform();

                $('.departmentselect').change( function() {
                    ubigeo_provinces($(this).val());
                    ubigeo_select_department($(this).val());
                });
                
            }
            else{
                $('.select_department').html('<em>No se encontraron departamentos del país</em>');
            }
       }
   });
});

$('.departmentselect').change( function() {
    ubigeo_provinces($(this).val());
    ubigeo_select_department($(this).val());
});
    
$('.provinceselect').change( function() {
    ubigeo_districts($(this).val());
    ubigeo_select_province($(this).val());
});

$('.districtselect').change( function()  {
    $('.districtvalue').val($(this).val());
});

function ubigeo_provinces(value) {
    $.ajax({
        type: "GET",
        data: { ubigeo_id : value, ubigeo_type : 'province' },
        url: globals.basepath + "/admin/headquarter/ajax/formelementsbyubigeo",
        success: function(response_data){
            if (response_data != ''){
                
                var object_data = eval('('+response_data+')');
                
                $('.select_province').html(object_data.select_ubigeo);
                $('.provincevalue').val('');
            
                $.uniform.update();
                $('.province').uniform();
    
                $('.provinceselect').change( function() {
                    ubigeo_districts($(this).val());
                    ubigeo_select_province($(this).val());
                });
                
            }
            else{
                $('.select_province').html('<em>No no encontraron provincias del departamento</em>');
            }
       }
   });
}

function ubigeo_districts(value) {
    $.ajax({
        type: "GET",
        data: { ubigeo_id : value, ubigeo_type : 'district'},
        url: globals.basepath + "/admin/headquarter/ajax/formelementsbyubigeo",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                
                $('.select_district').html(object_data.select_ubigeo);
                $('.districtvalue').val('');
                
                $.uniform.update();
                $('.district').uniform();

                $('.districtselect').change( function()  {
                    $('.districtvalue').val($(this).val());
                });
                
            }
            else{
                $('.select_province').html('<em>No no encontraron provincias del departamento</em>');
            }
       }
   });
}

function ubigeo_select_department(value) {
 
    $('.departmentvalue').val(value);
    
    if (value != '') {
      $('.formProvince').removeClass('formActiveOff');
    }
    else {
      $('.formProvince').addClass('formActiveOff');
    }
    $('.formDistrict').addClass('formActiveOff');
    
    $('.districtvalue').val('');
    
    $('.provinceselect').change( function() {
        ubigeo_select_province($(this).val());
    });
}

function ubigeo_select_province(value) {

    $('.provincevalue').val(value);
    
    if (value != '') {
        $('.formDistrict').removeClass('formActiveOff');
    }
    else {
        $('.formDistrict').addClass('formActiveOff');
    }
                
    $('.districtselect').change( function()  {
        $('.districtvalue').val($(this).val());
    });
}