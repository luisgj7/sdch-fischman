$('#options_company').change( function() {
    companyid = $('#options_company').val();
    $.ajax({
        type: "GET",
        data: { 'companyid': companyid },
        url: globals.basepath + "/dashboard/report/result/elementsbycompany",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                
                $('#options_pollscheduling option').remove();
                $('#options_pollscheduling').append(new Option('SELECCIONE UNA OPCIÓN', '', true, true));
                $('.selectOptionsData1 option').remove();
                $('.selectOptionsData2 option').remove();
                
                $('.pollsByCompany').html(object_data.polls);
                //$('.filterOptions').html(object_data.filters);
                $('.organizationalUnitsByCompany .leftBox select').html(object_data.organizationalunits);
                $('.chargesByCompany .leftBox select').html(object_data.charges);
                $('.headquartersByCompany .leftBox select').html(object_data.headquarters);
                $('.facultiesByCompany .leftBox select').html(object_data.faculties);
                $('.careersByCompany .leftBox select').html(object_data.careers);
                $('.gradesByCompany .leftBox select').html(object_data.grades);
                
                $('.formFilters').addClass('formActiveOff');
                $('.formFilterOptions').addClass('formActiveOff');
                
                $.uniform.update();
                $('#options_poll').uniform();
                
                $('#options_poll').change( function() {
                    update_pollschedulings_by_poll(companyid, $(this).val());
                });
                
            }
            else{
                $('.pollsByCompany').html('<em>No hay encuestas para esta empresa</em>');
            }
        }
    });
});

function update_pollschedulings_by_poll(companyid, pollid) {
    $.ajax({
        type: "GET",
        data: { 'companyid': companyid, 'pollid': pollid },
        url: globals.basepath + "/dashboard/report/result/pollschedulingperiodsbypoll",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                
                $('.evaluatedsByPollscheduling .selectOptionsData1 option').remove();
                $('.evaluatedsByPollscheduling .selectOptionsData2 option').remove();
                
                $('.pollschedulingsByPoll').html(object_data.periods);
                
                $('.formFilters').addClass('formActiveOff');
                $('.formFilterOptions').addClass('formActiveOff');
                
                $.uniform.update();
                $('#options_pollscheduling').uniform();
                
                $('#options_pollscheduling').change( function() {
                    update_evaluateds_by_pollscheduling(companyid, $(this).val());
                });
            }
            else{
                $('#options_pollscheduling').html('<em>No hay periodos para esta encuesta</em>');
            }
        }
    });
}

function update_evaluateds_by_pollscheduling(companyid, pollschedulingid) {
    $.ajax({
        type: "GET",
        data: { 'pollschedulingid': pollschedulingid },
        url: globals.basepath + "/dashboard/report/result/elementsbypollscheduling",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                
                $('.evaluatedsByPollscheduling .selectOptionsData1 option').remove();
                $('.evaluatedsByPollscheduling .selectOptionsData2 option').remove();
                
                $('.filterOptions').html(object_data.filters);
                $('.evaluatedsByPollscheduling .leftBox select').html(object_data.evaluateds);
                
                if (object_data.type == 'not_evaluateds' || companyid == 2) {
                    $('.formFilters').addClass('formActiveOff');
                }
                else {
                    $('.formFilters').removeClass('formActiveOff');
                }
                $('.formFilterOptions').addClass('formActiveOff');
                
                $.uniform.update();
                $('#options_filter').uniform();
                
                $('#options_filter').change(function () {
                    show_filter_options($(this).val());
                });
            }
        }
    });
}

$('#options_poll').change( function() {
    update_pollschedulings_by_poll(companyid, $(this).val());
});

$('#options_pollscheduling').change( function() {
    update_evaluateds_by_pollscheduling(companyid, $(this).val());
});

// Active Filters depndence ByFilter

if ($('#options_filter').length) {
    var option = $('select#options_filter :selected').val();
    show_filter_options(option);
    if ($('#options_filter option').size() > 1) {
        $('.formFilters').removeClass('formActiveOff');
    }
    $('#options_filter').change(function () {
        show_filter_options($(this).val());
    });
}

function show_filter_options(option) {
    $('.formFilterOptions').addClass('formActiveOff');
    switch (option) {
        case 'organizationalunits':
            $('.formFilterOrganizationalUnits').removeClass('formActiveOff');
            break;
        case 'charges':
            $('.formFilterCharges').removeClass('formActiveOff');
            break;
        case 'headquarters':
            $('.formFilterHeadquarters').removeClass('formActiveOff');
            break;
        case 'evaluateds':
            $('.formFilterEvaluateds').removeClass('formActiveOff');
            break;
        case 'faculties':
            $('.formFilterFaculties').removeClass('formActiveOff');
            break;
        case 'careers':
            $('.formFilterCareers').removeClass('formActiveOff');
            break;
        case 'grades':
            $('.formFilterGrades').removeClass('formActiveOff');
            break;
    }
}

// Validation in select multiple

$('button.autoselectOptionData2').click(function () {
    $("select.selectOptionsData2 option").attr("selected", true);
    $("select.selectOptionsCopy2 option").attr("selected", true);
});

// Validation form Benchmarking Internal

$(".crudBenchmarking").validate({
    rules : {
        'options_company' : 'required',
        'options_poll' : 'required',
        'options_level' : 'required',
        'options_pollscheduling' : 'required',
        'options_filter' : 'required',
        'organizationalunits_options[data2][]' : 'required',
        'charges_options[data2][]' : 'required',
        'headquarters_options[data2][]' : 'required',
        'evaluateds_options[data2][]' : 'required',
        'faculties_options[data2][]' : 'required',
        'careeres_options[data2][]' : 'required',
        'grades_options[data2][]' : 'required'
    },
    messages : {
        'options_company' : {
            required : "Seleccione la empresa."
        },
        'options_poll' : {
            required : "Seleccione la encuesta."
        },
        'options_level' : {
            required : "Seleccione el nivel de agrupación."
        },
        'options_pollscheduling' : {
            required : "Seleccione el periodo."
        },
        'options_filter' : {
            required : "Seleccione el filtro."
        },
        'organizationalunits_options[data2][]' : {
            required : "Seleccione la(s) unidades organizativa(s)."
        },
        'charges_options[data2][]' : {
            required : "Seleccione el/los cargo(s)."
        },
        'headquarters_options[data2][]' : {
            required : "Seleccione la(s) sede(s)."
        },
        'evaluateds_options[data2][]' : {
            required : "Seleccione el/los evaluado(s)."
        },
        'faculties_options[data2][]' : {
            required : "Seleccione el la(s) facultad(es)."
        },
        'careeres_options[data2][]' : {
            required : "Seleccione la(s) carrera(s)."
        },
        'grades_options[data2][]' : {
            required : "Seleccione el/los grado(s)."
        }
    }
});

//Validation in Save Button

$('form').submit(function () {
    var status;
    status = $(".formControl").valid(); //Validate again

    if(status==true) { 
        $('button[type$="submit"]').attr("disabled", "disabled");
    }
    
});
