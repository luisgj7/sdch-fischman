//Set autocomplete input
$('#form_evaluated').autocomplete({
    minLength: 0,
    source: workinginformations,
    select: function(event, ui) {
      //On select, update hidden id
      $('#form_id').val(ui.item.id);
    }
});

$('#form_evaluator').autocomplete({
    minLength: 0,
    source: workinginformations,
    select: function(event, ui) {
      //On select, update hidden id
      $('#form_id').val(ui.item.id);
    }
});

$('#form_evaluator_name').autocomplete({
    minLength: 0,
    source: workinginformations,
    select: function(event, ui) {
      //On select, update hidden id
      $('#form_iden').val(ui.item.id);
    }
});