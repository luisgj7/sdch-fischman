$('select.notificationType').change( function() {
  var type = $(this).val(); 
  if (type != '') {
    $.ajax({
        type: "GET",
        data: "type=" + $(this).val(),
        url: globals.basepath + "/admin/poll/notification/" + pollid + "/typenotificationpoll",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.selectOptionsData1').html(object_data.multi_select).show();
                $('.selectOptionsData2').html('').show();
                $.uniform.update();
                $('.type_notification').uniform();
            }
            else{
                $('.notificationTypeSelected').html('<em>No hay un tipo de notificación seleccionada</em>');
            }
        }
    });
  };
});
