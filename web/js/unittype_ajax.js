$('.boss').change( function()  {
    $.ajax({
        type: "GET",
        data: {parent_id : $(this).val()},
        url: globals.basepath + "/admin/companyorganizationalunit/ajax/choicebyparent2",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.unitType').html(object_data.select_unittype).show();
                $.uniform.update();
                $('.unitTypeDefault').uniform();
                $('.utvalue').val(object_data.unittype);
            }
            else{
                $('.unitType').html('<em>No no encontro información para esta Empresa</em>');
            }

            $('.unitTypeDefault').change( function()  {
              var utd = $(this).val();
              $('.utvalue').val(utd);
            });

       }
   });
});

$('.unitTypeDefault').change( function()  {
  var utd = $(this).val();
  $('.utvalue').val(utd);
});
