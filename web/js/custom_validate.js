a_formats = {
  'alpha' : /^[a-zA-Z\s\áéíóúñÁÉÍÓÚÑ]*$/,
  'alphanum' : /^[a-zA-Z0-9\s\/":°.,%@()_?¿!¡#\'\-\áéíóúñÁÉÍÓÚÑ]*$/,
  'alphanum2' : /^[a-zA-Z0-9\s\-.]*$/,
  'alphanum3' : /^[a-zA-Z0-9\s\.áéíóúñÁÉÍÓÚÑ]*$/,
  'unsigned' : /^\d+$/,
  'hour' : /^[0-9\s\:]*$/,
  'integer' : /^[\d\s\--]+$/,
  'email' : /^[\w-\.]+\@[\w\.-]+\.[a-zA-Z]{2,4}$/,
  'phone' : /^[\d\s\()--\/*]+$/,
  'period' : /^[\d\s\--]+$/,
  'celular' : /^[\d\s\()--]+$/,
  'version' : /^[0-9\s\.]*$/,
};

(function($) {

  jQuery.validator.addMethod("regular_expressions", function(value, element, regular) {
    if(value.length > 0) {
      return value.match(regular);
    }
    return true;
  }, "Ingrese correctamente el valor adecuado.");

  jQuery.validator.addMethod("regular_expressions_hour", function(value, element, regular) {
    if(value.length > 0) {
      if(value.length <= 2) {
        if(value <= 24) {
          return true;
        } else {
          return false;
        }
      }
      if(value.length >= 3) {
        value_hour = value.split(':');
        if(value_hour[1] <= 60) {
          return true;
        } else {
          return false;
        }
      }
      if(value.charAt(0).match(a_formats.unsigned)) {
        return value.match(regular);
      } else {
        return false;
      }
    }
    return true;
  }, "Ingrese una hora válida (12:30).");

  jQuery.validator.addMethod("required_select", function(value, element, reference) {
    if(reference.val() != '') {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, "Seleccione una opción.");

  jQuery.validator.addMethod("required_select_and_text", function(value, element, reference) {
    if (value == '' && reference.val() == '') {
      return true;
    } else {
      if (value != '') {
        return true
      } else {
        return false;
      }
    }
    return true;
  }, "Seleccione una de las opciones.");

  jQuery.validator.addMethod("required_select_peru", function(value, element, reference) {
    if(reference.val() == 1) {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, "Seleccione una opción.");

  jQuery.validator.addMethod("required_select_other_country", function(value, element, reference) {
    if(reference.val() != 1) {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, "Ingrese la Ciudad.");

  jQuery.validator.addMethod("document_type", function(value, element, reference) {
    if(value.length > 0) {
      if(reference.val() == 'dni') {
        if(value.length == 8) {
          return value.match(a_formats.unsigned);
        } else {
          return false;
        }
      } else if(reference.val() == 'ruc') {
        if(value.length == 11) {
          return value.match(a_formats.unsigned);
        } else {
          return false;
        }
      } else if(reference.val() == 'nit') {
        if(value.length == 8) {
          return value.match(a_formats.unsigned);
        } else {
          return false;
        }
      } else if(reference.val() != '') {
        if(value.length >= 4 && value.length <= 20) {
          return value.match(a_formats.alphanum2);
        } else {
          return false;
        }
      }
    } else if(reference.val() != '') {
      return false;
    }
    return true;
  }, "Ingrese correctamente su número de documento.");

  jQuery.validator.addMethod("nature", function(value, element, reference) {
    if(!reference.hasClass('formActiveOff')) {
      if(value.length > 0) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }, "Seleccione una opción.");

  jQuery.validator.addMethod("religious_affiliation", function(value, element, reference) {
    if(reference.val() > 0) {
      if(value.length != '') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, "Seleccione la afiliación religiosa.");

  jQuery.validator.addMethod("activity_type", function(value, element, reference) {
    if(value.length > 0) {
      if(reference.val() == 4) {
        return true;
      } else {
        return false;
      }
    } else if(reference.val() != '') {
      return false;
    }
    return true;
  }, "Seleccione el tipo de respuesta.");

  jQuery.validator.addMethod("active_field", function(value, element, reference) {
    if(reference) {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
    return true;
  }, "Rellene correctamente el campo.");

  jQuery.validator.addMethod("type_active_file", function(value, element, reference) {
    if($('.formCrudEdit').length) {
      if($('.valueDocument').length) {
        if(reference.val() == 'document' && $('.valueDocument').html() != '') {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      if(!$('.formTypeDocument').hasClass('formActiveOff')) {
        if(value != '') {
          return true;
        } else {
          return false;
        }
      }
    }
    return true;
  }, "Seleccione el documento.");

  //Add method validate schoolinformation

  jQuery.validator.addMethod("information_type", function(value, element, reference) {
    if(reference.hasClass('workinginformation')) {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }, "Seleccione la empresa.");
  
  jQuery.validator.addMethod("information_type2", function(value, element, reference) {
    if(reference.hasClass('schoolinformation')) {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }, "Seleccione el colegio.");
  
  jQuery.validator.addMethod("information_type3", function(value, element, reference) {
    if(reference.hasClass('universityinformation')) {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }, "Seleccione la universidad.");

  jQuery.validator.addMethod("ejecution_active_field", function(value, element, reference) {
    if(reference.val() == 'automatic') {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, "Rellene campo.");

  jQuery.validator.addMethod("replay_active_select", function(value, element, reference) {
    if(reference.val() == 1) {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, "Seleccione una opción.");

  jQuery.validator.addMethod("answertype_active_alignment", function(value, element, reference) {
    if(reference.val() == 'unique_option' || reference.val() == 'multiple_option') {
      if(value != '') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, "Seleccione una opción.");

  jQuery.validator.addMethod("asigned_select_multiple", function(value, element) {
    var i = 0;
    $(".selectOptionsData2 option").each(function() {
      i += 1;
    });
    if(i > 0) {
      return true;
    } else {
      return false;
    }
  }, "Agregue una o más opciones.");
  
  jQuery.validator.addMethod("options_select_multiple", function(value, element) {
    var i = 0;
    $(".selectOptionsData2 option").each(function() {
      i += 1;
    });
    if(i > 0) {
      return true;
    } else {
      return false;
    }
  }, "Agregue una o más opciones.");

  jQuery.validator.addMethod("select_options_add", function(value, element, reference) {
    if(reference.val() == 'unique_option' || reference.val() == 'multiple_option' || reference.val() == 'selection_option') {
      var i = 0;
      $(".selectOptions option").each(function() {
        i += 1;
      });
      if(i > 0) {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }, "Agregue una o más opciones.");

  $(".formAsigneds").validate({
    rules : {
      'form[profile]' : 'required'
    },
    messages : {
      'form[profile]' : {
        required : "Seleccione un perfil."
      }
    }
  });
  
  $(".formAsignedsEvaluators").validate({
    rules : {
      'form[rol]' : 'required'
    },
    messages : {
      'form[rol]' : {
        required : "Seleccione un rol."
      }
    }
  });
  
  $(".formSearchEvaluators").validate({
    rules: {
      'form[evaluated]': {
        required: true,
        regular_expressions: a_formats.alphanumber
      },
      'form[evaluator]': {
        required: true,
        regular_expressions: a_formats.alphanumber
      },
      'form[type]': 'required'
    },
    messages: {
      'form[evaluated]': {
        required: 'Digite el nombre del evaluado.',
        regular_expressions: 'Sólo se permiten caracteres alfanuméricos.'
      },
      'form[evaluator]': {
        required: 'Digite el nombre del evaluador.',
        regular_expressions: 'Sólo se permiten caracteres alfanuméricos.'
      },
      'form[type]': {
        required : "Seleccione una opción."
      }
    }
  });
  
  $(".formIframe").validate({
    rules: {
      'fishman_workshopbundle_plangoaltype[section]': {
        required: true
      },
      'fishman_workshopbundle_plangoaltype[goal]': {
        required: true
      },
      'fishman_workshopbundle_plangoaltype[in_charge]': {
        required: true
      },
      'fishman_workshopbundle_plangoaltype[deadline]': {
        required: true
      },
      'fishman_workshopbundle_plangoaltype[completed]': {
        required: true
      }
    },
    messages: {
      'fishman_workshopbundle_plangoaltype[section]': {
        required: 'Seleccione el tipo'
      },
      'fishman_workshopbundle_plangoaltype[goal]': {
        required: 'Es necesario que escriba el objetivo'
      },
      'fishman_workshopbundle_plangoaltype[in_charge]': {
        required: 'Seleccione la persona a cargo'
      },
      'fishman_workshopbundle_plangoaltype[deadline]': {
        required: 'Seleccione el plazo para cumplir el objetivo'
      },
      'fishman_workshopbundle_plangoaltype[completed]': {
        required: 'Seleccione si el objetivo se completó'
      }
    }
  });
  
  $(".crud").validate({
    rules : {
      'fishman_authbundle_usertype[username]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      'fishman_authbundle_usertype[password]' : {
        required : true,
        minlength : 4,
        maxlength : 16
      },
      'fishman_authbundle_usertype[surname]' : {
        required : true,
        regular_expressions : a_formats.alpha,
        minlength : 3,
        maxlength : 20
      },
      'fishman_authbundle_usertype[names]' : {
        required : true,
        regular_expressions : a_formats.alpha,
        minlength : 3,
        maxlength : 60
      },
      'fishman_authbundle_usertype[identity]' : {
        required : true,
        required_select_and_text : $('#fishman_authbundle_usertype_numberidentity')
      },
      'fishman_authbundle_usertype[numberidentity]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        document_type : $('#fishman_authbundle_usertype_identity')
      },
      /*'fishman_authbundle_usertype[birthday]' : {
        required : true,
        regular_expressions : a_formats.dates
      },
      'fishman_authbundle_usertype[marital_status]' : 'required',*/
      'fishman_authbundle_usertype[sex]' : 'required',
      'fishman_authbundle_usertype[email]' : {
        required : true,
        email : true,
        minlength : 6,
        maxlength : 60
      },
      'fishman_authbundle_usertype[phone]' : {
        required : false,
        regular_expressions : a_formats.phone,
        minlength : 5,
        maxlength : 32
      },
      'fishman_authbundle_usertype[enabled]' : 'required',
      'fishman_authbundle_workinginformationtype[companyid]' : {
        information_type: $('#fishman_authbundle_workinginformationtype_companyid'),
        information_type2: $('#fishman_authbundle_workinginformationtype_companyid'),
        information_type3: $('#fishman_authbundle_workinginformationtype_companyid')
      },
      'fishman_authbundle_workinginformationtype[code]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 3,
        maxlength : 30
      },
      'fishman_authbundle_workinginformationtype[datein]' : {
        regular_expressions : a_formats.dates
      },
      'fishman_authbundle_workinginformationtype[dateout]' : {
        regular_expressions : a_formats.dates
      },
      'fishman_authbundle_workinginformationtype[email]' : {
        required : true,
        email : true,
        minlength : 6,
        maxlength : 60
      },
      'fishman_authbundle_workinginformationtype[unit_type]' : 'required',
      'company_organization_unit' : 'required',
      'company_charge' : 'required',
      'company_faculty' : 'required',
      'company_career' : 'required',
      'headquarter' : 'required',
      'fishman_authbundle_workinginformationtype[grade]' : 'required',
      'fishman_authbundle_workinginformationtype[study_year]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 4,
        maxlength : 30
      },
      'fishman_authbundle_workinginformationtype[academic_year]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 4,
        maxlength : 30
      },
      'workshop_categories' : 'required',
      'workshop_workshops' : 'required',
      'fishman_workshopbundle_workshopschedulingtype[custom_header_title_text]' : {
        required_select : $('#custom_header_title')
      },
      'fishman_workshopbundle_workshopschedulingtype[custom_header_title_description]' : {
        required_select : $('#custom_header_title')
      },
      'fishman_workshopbundle_workshopschedulingtype[initdate]' : 'required',
      'fishman_workshopbundle_workshopschedulingtype[enddate]' : 'required',
      'fishman_workshopbundle_workshopschedulingtype[description]' : 'required',
      'fishman_workshopbundle_workshopschedulingtype[company]' : 'required',
      'fishman_workshopbundle_workshopschedulingtype[status]' : 'required',
      'fishman_authbundle_workinginformationtype[boss]' : 'required',
      'fishman_authbundle_workinginformationtype[status]' : 'required',
      'fishman_entitybundle_facultytype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      'fishman_entitybundle_facultytype[status]' : 'required',
      'fishman_entitybundle_categorytype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      'fishman_entitybundle_categorytype[status]' : 'required',
      'fishman_entitybundle_chargetype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      'fishman_entitybundle_chargetype[status]' : 'required',
      'fishman_entitybundle_talenttype[talent]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      'fishman_entitybundle_talenttype[status]' : 'required',
      'fishman_entitybundle_organizationalunittype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 120
      },
      'fishman_entitybundle_headquartertype[code]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 3,
        maxlength : 30
      },
      'fishman_entitybundle_headquartertype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 120
      },
      'fishman_entitybundle_headquartertype[via]' : 'required',
      'fishman_entitybundle_headquartertype[address]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 255
      },
      'fishman_entitybundle_headquartertype[country]' : 'required',
      'ubigeo_department' : {
        required_select_peru : $('.countryselect')
      },
      'ubigeo_province' : {
        required_select : $('.departmentselect')
      },
      'ubigeo_district' : {
        required_select : $('.provinceselect')
      },
      'fishman_entitybundle_headquartertype[city]' : {
        required_select_other_country : $('.countryselect'),
        regular_expressions : a_formats.alphanum
      },
      'fishman_entitybundle_headquartertype[status]' : 'required',
      'fishman_entitybundle_companytype[code]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 3,
        maxlength : 30
      },
      'fishman_entitybundle_companytype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      'fishman_entitybundle_companytype[document]' : {
        accept : 'jpg,png,jpeg'
      },
      'fishman_entitybundle_companytype[document_type]' : {
        required : true,
        required_select_and_text : $('#fishman_entitybundle_companytype_ruc')
      },
      'fishman_entitybundle_companytype[ruc]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        document_type : $('#fishman_entitybundle_companytype_document_type')
      },
      'company_data[type_school]' : {
        required : true
      },
      'company_data[type_university]' : {
        required : true
      },
      'fishman_entitybundle_companytype[industry]' : {
        required : true
      },
      'fishman_entitybundle_companytype[nature]' : {
        required : true
      },
      'fishman_entitybundle_companytype[contact]' : {
        required : true,
        regular_expressions : a_formats.alpha,
        minlength : 3,
        maxlength : 120
      },
      'fishman_entitybundle_companytype[anex]' : {
        regular_expressions : a_formats.unsigned,
        minlength : 1,
        maxlength : 6
      },
      'fishman_entitybundle_companytype[phone]' : {
        required : true,
        regular_expressions : a_formats.phone,
        minlength : 5,
        maxlength : 16
      },
      'fishman_entitybundle_companytype[email]' : {
        required : true,
        email : true,
        minlength : 6,
        maxlength : 60
      },
      'fishman_entitybundle_companytype[type_school]' : {
        nature : $('.formFieldsetSchool')
      },
      'fishman_entitybundle_companytype[gender]' : {
        nature : $('.formFieldsetSchool')
      },
      'fishman_entitybundle_companytype[religious_affiliation_on]' : {
        nature : $('.formFieldsetSchool')
      },
      'fishman_entitybundle_companytype[religious_affiliation]' : {
        religious_affiliation : $('#fishman_entitybundle_companytype_religious_affiliation_on')
      },
      'fishman_entitybundle_companytype[education_system]' : {
        nature : $('.formFieldsetSchool')
      },
      'fishman_entitybundle_companytype[internal_school]' : {
        nature : $('.formFieldsetSchool')
      },
      'fishman_entitybundle_companytype[type_university]' : {
        nature : $('.formFieldsetUniversity')
      },
      'fishman_entitybundle_companytype[status]' : 'required',
      'fishman_entitybundle_companyorganizationalunittype[code]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 1,
        maxlength : 30
      },
      'fishman_entitybundle_companyorganizationalunittype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 120
      },
      'fishman_entitybundle_companyorganizationalunittype[unit_type]' : 'required',
      'fishman_entitybundle_companyorganizationalunittype[parentid]' : 'required',
      'fishman_entitybundle_companyorganizationalunittype[status]' : 'required',
      'fishman_entitybundle_companychargetype[code]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 1,
        maxlength : 30
      },
      'fishman_entitybundle_companychargetype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 1,
        maxlength : 60
      },
      'fishman_entitybundle_companychargetype[status]' : 'required',
      'fishman_entitybundle_companyfacultytype[code]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 1,
        maxlength : 30
      },
      'fishman_entitybundle_companyfacultytype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 1,
        maxlength : 60
      },
      'fishman_entitybundle_companyfacultytype[status]' : 'required',
      'fishman_entitybundle_companycareertype[code]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 1,
        maxlength : 30
      },
      'fishman_entitybundle_companycareertype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 1,
        maxlength : 60
      },
      'fishman_entitybundle_companycareertype[companyfaculty]' : 'required',
      'fishman_entitybundle_companycareertype[status]' : 'required',
      'fishman_entitybundle_companycoursetype[code]' : {
        required : true,
        regular_expressions : a_formats.alphanum2,
        minlength : 1,
        maxlength : 30
      },
      'fishman_entitybundle_companycoursetype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 1,
        maxlength : 60
      },
      'fishman_entitybundle_companycoursetype[status]' : 'required',
      'fishman_notificationbundle_notificationtype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 150
      },
      'fishman_notificationbundle_notificationtype[notification_type_id]' : {
        required_select : $('#fishman_notificationbundle_notificationtype_notification_type')
      },
      'fishman_notificationbundle_notificationtype[notification_type_status]' : {
        required_select : $('#fishman_notificationbundle_notificationtype_notification_type')
      },
      'fishman_notificationbundle_notificationtype[subject]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 200
      },
      'fishman_notificationbundle_notificationtype[since]' : {
        regular_expressions : a_formats.unsigned,
        min : 1,
        ejecution_active_field : $('#fishman_notificationbundle_notificationtype_ejecution_type'),
        maxlength : 2
      },
      'fishman_notificationbundle_notificationtype[repeat_period]' : {
        replay_active_select : $('#fishman_notificationbundle_notificationtype_replay')
      },
      'fishman_notificationbundle_notificationtype[repeat_range]' : {
        replay_active_select : $('#fishman_notificationbundle_notificationtype_replay')
      },
      'fishman_notificationbundle_notificationtype[repetition_number]' : {
        regular_expressions : a_formats.unsigned,
        min : 1,
        maxlength : 2
      },
      'fishman_notificationbundle_notificationtype[sequence]' : 'required',
      'fishman_notificationbundle_notificationtype[status]' : 'required',
      'asigned_options[data2][]' : 'asigned_select_multiple',
      'fishman_notificationbundle_notificationschedulingtype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 150
      },
      'fishman_notificationbundle_notificationschedulingtype[notification_type_id]' : {
        required_select : $('#fishman_notificationbundle_notificationschedulingtype_notification_type')
      },
      'fishman_notificationbundle_notificationschedulingtype[notification_type_status]' : {
        required_select : $('#fishman_notificationbundle_notificationschedulingtype_notification_type')
      },
      'fishman_notificationbundle_notificationschedulingtype[subject]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 200
      },
      'fishman_notificationbundle_notificationschedulingtype[since]' : {
        regular_expressions : a_formats.unsigned,
        min : 1,
        ejecution_active_field : $('#fishman_notificationbundle_notificationschedulingtype_ejecution_type'),
        maxlength : 2
      },
      'fishman_notificationbundle_notificationschedulingtype[repeat_period]' : {
        replay_active_select : $('#fishman_notificationbundle_notificationschedulingtype_replay')
      },
      'fishman_notificationbundle_notificationschedulingtype[repeat_range]' : {
        replay_active_select : $('#fishman_notificationbundle_notificationschedulingtype_replay')
      },
      'fishman_notificationbundle_notificationschedulingtype[repetition_number]' : {
        regular_expressions : a_formats.unsigned,
        min : 1,
        maxlength : 3
      },
      'fishman_notificationbundle_notificationschedulingtype[sequence]' : 'required',
      'fishman_notificationbundle_notificationschedulingtype[status]' : 'required',
      'fishman_workshopbundle_workshoptype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 200
      },
      'fishman_workshopbundle_workshoptype[description]' : 'required',
      'fishman_workshopbundle_workshoptype[category]' : 'required',
      'fishman_workshopbundle_workshoptype[status]' : 'required',
      'fishman_workshopbundle_workshopdocumenttype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 150
      },
      'fishman_workshopbundle_workshopdocumenttype[category]' : 'required',
      'fishman_workshopbundle_workshopdocumenttype[description]' : 'required',
      'fishman_workshopbundle_workshopdocumenttype[type]' : 'required',
      'fishman_workshopbundle_workshopdocumenttype[video]' : {
        active_field : $('#fishman_workshopbundle_workshopdocumenttype_video').parent().parent().hasClass('formActiveOff')
      },
      'fishman_workshopbundle_workshopdocumenttype[embed]' : {
        minlength : 15,
        active_field : $('#fishman_workshopbundle_workshopdocumenttype_embed').parent().parent().hasClass('formActiveOff')
      },
      'fishman_workshopbundle_workshopdocumenttype[file]' : {
        type_active_file : $('#fishman_workshopbundle_workshopdocumenttype_type'),
        accept : 'doc|docx|xls|xlsx|ppt|pptx|txt|pdf|png|jpg|gif'
      },
      'fishman_workshopbundle_workshopdocumenttype[website]' : {
        active_field : $('#fishman_workshopbundle_workshopdocumenttype_website').parent().parent().hasClass('formActiveOff'),
        url : true
      },
      'fishman_workshopbundle_workshopdocumenttype[sequence]' : 'required',
      'fishman_workshopbundle_workshopdocumenttype[status]' : 'required',
      'fishman_workshopbundle_workshopactivitytype[category]' : 'required',
      'fishman_workshopbundle_workshopactivitytype[activity]' : 'required',
      'fishman_workshopbundle_workshopactivitytype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 240
      },
      'fishman_workshopbundle_workshopactivitytype[answer_type]' : {
        activity_type : $('#fishman_workshopbundle_workshopactivitytype_activity')
      },
      'fishman_workshopbundle_workshopactivitytype[alignment]' : {
        answertype_active_alignment : $('#fishman_workshopbundle_workshopactivitytype_answer_type')
      },
      'fishman_workshopbundle_workshopactivitytype[duration]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        min : 1
      },
      'fishman_workshopbundle_workshopactivitytype[period]' : 'required',
      'fishman_workshopbundle_workshopactivitytype[sequence]' : 'required',
      'fishman_workshopbundle_workshopactivitytype[status]' : 'required',
      'fishman_workshopbundle_workshopschedulingdocumenttype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 150
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[category]' : 'required',
      'fishman_workshopbundle_workshopschedulingdocumenttype[description]' : 'required',
      'fishman_workshopbundle_workshopschedulingdocumenttype[type]' : 'required',
      'fishman_workshopbundle_workshopschedulingdocumenttype[video]' : {
        active_field : $('#fishman_workshopbundle_workshopschedulingdocumenttype_video').parent().parent().hasClass('formActiveOff')
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[embed]' : {
        minlength : 15,
        active_field : $('#fishman_workshopbundle_workshopschedulingdocumenttype_embed').parent().parent().hasClass('formActiveOff')
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[file]' : {
        type_active_file : $('#fishman_workshopbundle_workshopschedulingdocumenttype_type'),
        accept : 'doc|docx|xls|xlsx|ppt|pptx|txt|pdf|png|jpg|gif'
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[website]' : {
        active_field : $('#fishman_workshopbundle_workshopschedulingdocumenttype_website').parent().parent().hasClass('formActiveOff'),
        url : true
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[sequence]' : 'required',
      'fishman_workshopbundle_workshopschedulingdocumenttype[status]' : 'required',
      'fishman_workshopbundle_workshopschedulingactivitytype[category]' : 'required',
      'fishman_workshopbundle_workshopschedulingactivitytype[activity]' : 'required',
      'fishman_workshopbundle_workshopschedulingactivitytype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 240
      },
      'workshopschedulingactivity_pollscheudling_id' : 'required', 
      'fishman_workshopbundle_workshopschedulingactivitytype[answer_type]' : {
        activity_type : $('#fishman_workshopbundle_workshopschedulingactivitytype_activity')
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[alignment]' : {
        answertype_active_alignment : $('#fishman_workshopbundle_workshopschedulingactivitytype_answer_type')
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[duration]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        min : 1
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[period]' : 'required',
      'fishman_workshopbundle_workshopschedulingactivitytype[datein]' : {
        required : true,
        regular_expressions : a_formats.dates
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[dateout]' : {
        required : true,
        regular_expressions : a_formats.dates
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[sequence]' : 'required',
      'fishman_workshopbundle_workshopschedulingactivitytype[status]' : 'required',
      'fishman_workshopbundle_workshopschedulingpeopletype[profile]' : 'required',
      'fishman_workshopbundle_workshopschedulingpeopletype[status]' : 'required',
      'fishman_pollbundle_benchmarkingtype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
      },
      'fishman_pollbundle_benchmarkingtype[period]' : {
        required : true,
        regular_expressions : a_formats.period,
        minlength : 4,
        maxlength : 10
      },
      'fishman_pollbundle_benchmarkingtype[status]' : 'required',
      'poll_categories' : 'required',
      'poll_polls' : 'required',
      'fishman_pollbundle_entitypolltype[duration]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        min : 1
      },
      'fishman_pollbundle_entitypolltype[period]' : 'required',
      'fishman_pollbundle_entitypolltype[sequence]' : 'required',
      'fishman_pollbundle_entitypolltype[status]' : 'required',
      'fishman_pollbundle_polltype[title]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 5,
        maxlength : 200
      },
      'fishman_pollbundle_polltype[version]' : {
        required : true,
        regular_expressions : a_formats.version,
      },
      'fishman_pollbundle_polltype[description]' : 'required',
      'fishman_pollbundle_polltype[category]' : 'required',
      'fishman_pollbundle_polltype[type]' : 'required',
      'fishman_pollbundle_polltype[duration]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        min : 1
      },
      'fishman_pollbundle_polltype[period]' : 'required',
      'fishman_pollbundle_polltype[level]' : 'required',
      'fishman_pollbundle_polltype[sequence_question]' : 'required',
      'fishman_pollbundle_polltype[divide]' : 'required',
      'fishman_pollbundle_polltype[number_question]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        min : 1
      },
      'fishman_pollbundle_polltype[sequence]' : 'required',
      'fishman_pollbundle_polltype[status]' : 'required',
      'fishman_pollbundle_pollsectiontype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 5,
        maxlength : 255
      },
      'fishman_pollbundle_pollsectiontype[type]' : 'required',
      'fishman_pollbundle_pollsectiontype[sequence]' : 'required',
      'fishman_pollbundle_pollsectiontype[status]' : 'required',
      'fishman_pollbundle_pollquestiontype[question]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 5,
        maxlength : 255
      },
      'fishman_pollbundle_pollquestiontype[type]' : 'required',
      'fishman_pollbundle_pollquestiontype[alignment]' : {
        answertype_active_alignment : $('#fishman_pollbundle_pollquestiontype_type')
      },
      'select_options_add[]' : {
        select_options_add : $('select.answerType')
      },
      'fishman_pollbundle_pollquestiontype[pollsection]' : 'required',
      'fishman_pollbundle_pollquestiontype[sequence]' : 'required',
      'fishman_pollbundle_pollquestiontype[status]' : 'required',
      'fishman_pollbundle_pollschedulingtype[description]' : 'required',
      'fishman_pollbundle_pollschedulingtype[company_id]' : 'required',
      'fishman_pollbundle_pollschedulingtype[custom_header_title_text]' : {
        required_select : $('#custom_header_title')
      },
      'fishman_pollbundle_pollschedulingtype[custom_header_title_description]' : {
        required_select : $('#custom_header_title')
      },
      'fishman_pollbundle_pollschedulingtype[type]' : 'required',
      'fishman_pollbundle_pollschedulingtype[multiple_evaluation]' : 'required',
      'fishman_pollbundle_pollschedulingtype[pollscheduling_period]' : {
        required : true,
        regular_expressions : a_formats.period,
        minlength : 4,
        maxlength : 10
      },
      'fishman_pollbundle_pollschedulingtype[duration]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        min : 1
      },
      'fishman_pollbundle_pollschedulingtype[period]' : 'required',
      'fishman_pollbundle_pollschedulingtype[initdate]' : {
        required : true,
        regular_expressions : a_formats.dates
      },
      'fishman_pollbundle_pollschedulingtype[enddate]' : {
        required : true,
        regular_expressions : a_formats.dates
      },
      'fishman_pollbundle_pollschedulingtype[message_gratitude]' : 'required',
      'fishman_pollbundle_pollschedulingtype[message_end]' : 'required',
      'fishman_pollbundle_pollschedulingtype[message_inactive]' : 'required',
      'fishman_pollbundle_pollschedulingtype[access_view_number]' : {
        required : true,
        regular_expressions : a_formats.unsigned,
        min : 1
      },
      'fishman_pollbundle_pollschedulingtype[status_bar]' : 'required',
      'fishman_pollbundle_pollschedulingtype[counselor_report]' : 'required',
      'fishman_pollbundle_pollschedulingtype_title_company' : 'required',
      'fishman_pollbundle_pollschedulingtype_logo_company' : 'required',
      'fishman_pollbundle_pollschedulingtype[pollscheduling_anonymous]' : 'required',
      'fishman_pollbundle_pollschedulingtype[sequence]' : 'required',
      'fishman_pollbundle_pollschedulingtype[status]' : 'required',
      'options_integrant' : 'required',
      'options_company' : 'required',
      'options_poll' : 'required',
      'options_evaluated' : 'required',
      'options_level' : 'required',
      'fishman_pollbundle_pollschedulingsectiontype[name]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 5,
        maxlength : 255
      },
      'fishman_pollbundle_pollschedulingsectiontype[type]' : 'required',
      'fishman_pollbundle_pollschedulingsectiontype[sequence]' : 'required',
      'fishman_pollbundle_pollschedulingsectiontype[status]' : 'required',
      'fishman_pollbundle_pollschedulingquestiontype[question]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 5,
        maxlength : 255
      },
      'fishman_pollbundle_pollschedulingquestiontype[type]' : 'required',
      'fishman_pollbundle_pollschedulingquestiontype[alignment]' : {
        answertype_active_alignment : $('#fishman_pollbundle_pollschedulingquestiontype_type')
      },
      'fishman_pollbundle_pollschedulingquestiontype[pollschedulingsection]' : 'required',
      'fishman_pollbundle_pollschedulingquestiontype[sequence]' : 'required',
      'fishman_pollbundle_pollschedulingquestiontype[status]' : 'required',
    },
    messages : {
      'fishman_authbundle_usertype[username]' : {
        required : 'Ingrese el nombre de usuario.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_authbundle_usertype[password]' : {
        required : 'Ingrese la contraseña.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo."),
      },
      'fishman_authbundle_usertype[surname]' : {
        required : 'Ingrese el apellido paterno.',
        regular_expressions : 'Sólo se permiten caracteres alfabéticos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_authbundle_usertype[names]' : {
        required : 'Ingrese sus nombres.',
        regular_expressions : 'Sólo se permiten caracteres alfabéticos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_authbundle_usertype[identity]' : {
        required : "Seleccione un documento de identidad.",
        required_select_and_text : "Seleccione un documento de identidad."
      },
      'fishman_authbundle_usertype[numberidentity]' : {
        required : 'Ingrese su número de documento.'
      },
      'fishman_authbundle_usertype[sex]' : {
        required : 'Seleccione el sexo.'
      },
      'fishman_authbundle_usertype[email]' : {
        required : 'Ingrese su correo electrónico.',
        email : 'Ingrese un correo electrónico válido.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_authbundle_usertype[phone]' : {
        regular_expressions : 'Sólo se permiten números y los siguientes caracteres \"()-*\".',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_authbundle_usertype[enabled]' : {
        required : 'Seleccione el estado.'
      },
      'fishman_authbundle_workinginformationtype[companyid]' : {
        regular_expressions : 'Sólo se permiten seleccionar.',
      },
      'fishman_authbundle_workinginformationtype[code]' : {
        required : 'Ingrese el código.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_authbundle_workinginformationtype[datein]' : {
        regular_expressions : 'Sólo se permite formato de fecha.'
      },
      'fishman_authbundle_workinginformationtype[dateout]' : {
        regular_expressions : 'Sólo se permite formato de fecha.'
      },
      'fishman_authbundle_workinginformationtype[email]' : {
        required : 'Ingrese su correo electrónico.',
        email : 'Ingrese un correo electrónico válido.'
      },
      'fishman_authbundle_workinginformationtype[boss]' : {
        required : 'Seleccione el responsable ó jefe.'
      },
      'fishman_authbundle_workinginformationtype[unit_type]' : {
        required : 'Seleccione el tipo unidad.'
      },
      'company_organization_unit' : {
        required : "Seleccione la unidad organizativa."
      },
      'company_charge' : {
        required : "Seleccione el cargo."
      },
      'company_faculty' : {
        required : "Seleccione la facultad."
      },
      'company_career' : {
        required : "Seleccione la carrera."
      },
      'headquarter' : {
        required : "Seleccione la sede."
      },
      'fishman_authbundle_workinginformationtype[grade]' : {
        required : "Seleccione el grado."
      },
      'fishman_authbundle_workinginformationtype[study_year]' : {
        required : 'Ingrese los años de estudios.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_authbundle_workinginformationtype[academic_year]' : {
        required : 'Ingrese el ciclo académico.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_authbundle_workinginformationtype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_facultytype[name]' : {
        required : "Ingrese el nombre.",
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_facultytype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_categorytype[name]' : {
        required : "Ingrese el nombre.",
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_categorytype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_chargetype[name]' : {
        required : "Ingrese el nombre.",
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_chargetype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_talenttype[talent]' : {
        required : "Ingrese el nombre.",
        regular_expressions : 'Sólo se permiten caracteres alfabéticos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_talenttype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_organizationalunittype[name]' : {
        required : "Ingrese el nombre.",
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_organizationalunittype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_headquartertype[code]' : {
        required : "Ingrese el código.",
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_headquartertype[name]' : {
        required : "Ingrese el nombre.",
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_headquartertype[via]' : {
        required : "Seleccione la vía."
      },
      'fishman_entitybundle_headquartertype[address]' : {
        required : "Ingrese la dirección.",
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_headquartertype[country]' : {
        required : "Seleccione el país."
      },
      'ubigeo_department' : {
        required_select_peru : "Seleccione el departamento."
      },
      'ubigeo_province' : {
        required_select : "Seleccione la provincia."
      },
      'ubigeo_district' : {
        required_select : "Seleccione el distrito."
      },
      'fishman_entitybundle_headquartertype[city]' : {
        required_country_other_country : "Ingrese la Ciudad.",
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.'
      },
      'fishman_entitybundle_headquartertype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_companytype[code]' : {
        required : 'Ingrese el código.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companytype[name]' : {
        required : 'Ingrese la razón social.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companytype[document]' : {
        accept : "Sólo se permiten: jpg,png,jpeg",
      },
      'fishman_entitybundle_companytype[document_type]' : {
        required : "Seleccione el tipo de documento.",
        required_select_and_text : "Seleccione el tipo de documento."
      },
      'fishman_entitybundle_companytype[ruc]' : {
        required : 'Ingrese el número de documento.',
        regular_expressions : 'Sólo se permiten números.',
      },
      'company_data[type_school]' : {
        required : 'Seleccione el tipo de colegio.'
      },
      'company_data[type_university]' : {
        required : 'Seleccione el tipo de Universidad.'
      },
      'fishman_entitybundle_companytype[industry]' : {
        required : 'Seleccione la industria.'
      },
      'fishman_entitybundle_companytype[nature]' : {
        required : 'Seleccione la naturalaeza de la empresa.'
      },
      'fishman_entitybundle_companytype[contact]' : {
        required : 'Ingrese el nombre de contacto.',
        regular_expressions : 'Sólo se permiten caracteres alfabéticos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companytype[anex]' : {
        regular_expressions : 'Sólo se permiten números.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companytype[phone]' : {
        required : 'Ingrese el nro. de teléfono.',
        regular_expressions : 'Sólo se permiten números y los siguientes caracteres \"()-*\".',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companytype[email]' : {
        required : 'Ingrese el correo electrónico.',
        email : 'Ingrese un correo electrónico válido.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companytype[type_school]' : {
        nature : 'Seleccione el tipo.'
      },
      'fishman_entitybundle_companytype[gender]' : {
        nature : 'Seleccione el género.'
      },
      'fishman_entitybundle_companytype[religious_affiliation_on]' : {
        nature : 'Seleccione una opción.'
      },
      'fishman_entitybundle_companytype[education_system]' : {
        nature : 'Seleccione el sistema educativo.'
      },
      'fishman_entitybundle_companytype[internal_school]' : {
        nature : 'Seleccione una opción.'
      },
      'fishman_entitybundle_companytype[type_university]' : {
        nature : 'Seleccione el tipo.'
      },
      'fishman_entitybundle_companytype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_companyorganizationalunittype[code]' : {
        required : 'Ingrese el código.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companyorganizationalunittype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companyorganizationalunittype[unit_type]' : {
        required : "Seleccione el tipo unidad."
      },
      'fishman_entitybundle_companyorganizationalunittype[parentid]' : {
        required : "Seleccione la unidad padre."
      },
      'fishman_entitybundle_companyorganizationalunittype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_companychargetype[code]' : {
        required : 'Ingrese el código.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companychargetype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companychargetype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_companyfacultytype[code]' : {
        required : 'Ingrese el código.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companyfacultytype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companyfacultytype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_companycareertype[code]' : {
        required : 'Ingrese el código.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companycareertype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companycareertype[companyfaculty]' : {
        required : "Seleccione la facultad."
      },
      'fishman_entitybundle_companycareertype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_entitybundle_companycoursetype[code]' : {
        required : 'Ingrese el código.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companycoursetype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_entitybundle_companycoursetype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_notificationbundle_notificationtype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_notificationbundle_notificationtype[subject]' : {
        required : 'Ingrese el mensaje.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_notificationbundle_notificationtype[since]' : {
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_notificationbundle_notificationtype[repetition_number]' : {
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_notificationbundle_notificationtype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_notificationbundle_notificationtype[message]' : {
        required : "Ingrese el mensaje."
      },
      'fishman_notificationbundle_notificationtype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_notificationbundle_notificationschedulingtype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_notificationbundle_notificationschedulingtype[subject]' : {
        required : 'Ingrese el mensaje.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_notificationbundle_notificationschedulingtype[since]' : {
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_notificationbundle_notificationschedulingtype[repetition_number]' : {
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_notificationbundle_notificationschedulingtype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_notificationbundle_notificationschedulingtype[notification_type]' : {
        required : "Seleccione el tipo."
      },
      'fishman_notificationbundle_notificationschedulingtype[message]' : {
        required : "Ingrese el mensaje."
      },
      'fishman_notificationbundle_notificationschedulingtype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_workshopbundle_workshoptype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_workshopbundle_workshoptype[description]' : {
        required : 'Ingrese una descripción.'
      },
      'fishman_workshopbundle_workshoptype[category]' : {
        required : "Seleccione una categoría."
      },
      'fishman_workshopbundle_workshoptype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_workshopbundle_workshopdocumenttype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_workshopbundle_workshopdocumenttype[category]' : {
        required : "Seleccione una categoría."
      },
      'fishman_workshopbundle_workshopdocumenttype[description]' : {
        required : 'Ingrese una descripción.'
      },
      'fishman_workshopbundle_workshopdocumenttype[type]' : {
        required : "Seleccione el tipo de documento."
      },
      'fishman_workshopbundle_workshopdocumenttype[file]' : {
        accept : "Sólo se permiten: doc, docx, xls, xlsx, ppt, pptx, txt, pdf, jpg, png, gif",
        required : "Seleccione el documento."
      },
      'fishman_workshopbundle_workshopdocumenttype[embed]' : {
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
      },
      'fishman_workshopbundle_workshopdocumenttype[website]' : {
        url : "Por favor ingrese una ruta URL válida."
      },
      'fishman_workshopbundle_workshopdocumenttype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_workshopbundle_workshopdocumenttype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_workshopbundle_workshopactivitytype[category]' : {
        required : "Seleccione una categoría."
      },
      'fishman_workshopbundle_workshopactivitytype[activity]' : {
        required : "Seleccione una actividad."
      },
      'fishman_workshopbundle_workshopactivitytype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_workshopbundle_workshopactivitytype[answer_type]' : {
        activity_type : "Seleccione el tipo de respuesta."
      },
      'fishman_workshopbundle_workshopactivitytype[duration]' : {
        required : 'Ingrese el número de duración.',
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}.")
      },
      'fishman_workshopbundle_workshopactivitytype[period]' : {
        required : 'Seleccione el periodo de duración.'
      },
      'fishman_workshopbundle_workshopactivitytype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_workshopbundle_workshopactivitytype[status]' : {
        required : "Seleccione el estado."
      },
      'workshop_categories' : {
        required : 'Seleccione la categoría.'
      },
      'workshop_workshops' : {
        required : 'Seleccione el taller.'
      },
      'fishman_workshopbundle_workshopschedulingtype[custom_header_title_text]' : {
        required_select : "Ingrese el título de cabecera."
      },
      'fishman_workshopbundle_workshopschedulingtype[custom_header_title_description]' : {
        required_select : "Ingrese la descripción de cabecera."
      },
      'fishman_workshopbundle_workshopschedulingtype[initdate]' : {
        required : 'Seleccione la fecha de Inicio.'
      },
      'fishman_workshopbundle_workshopschedulingtype[enddate]' : {
        required : 'Seleccione la fecha final.'
      },
      'fishman_workshopbundle_workshopschedulingtype[description]' : {
        required : 'Ingrese la descripción .'
      },
      'fishman_workshopbundle_workshopschedulingtype[company]' : {
        required : 'Seleccione la empresa.'
      },
      'fishman_workshopbundle_workshopschedulingtype[status]' : {
        required : 'Seleccione el estado.'
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[category]' : {
        required : "Seleccione una categoría."
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[description]' : {
        required : 'Ingrese una descripción.'
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[type]' : {
        required : "Seleccione el tipo de documento."
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[file]' : {
        accept : "Sólo se permiten: doc, docx, xls, xlsx, ppt, pptx, txt, pdf, png, jpg, gif",
        required : "Seleccione el documento."
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[website]' : {
        url : "Por favor ingrese una ruta URL válida."
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_workshopbundle_workshopschedulingdocumenttype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[category]' : {
        required : "Seleccione una categoría."
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[activity]' : {
        required : "Seleccione una actividad."
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'workshopschedulingactivity_pollscheudling_id' : {
        required : "Seleccione una encuesta."
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[answer_type]' : {
        activity_type : "Seleccione el tipo de respuesta."
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[duration]' : {
        required : 'Ingrese el número de duración.',
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}.")
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[period]' : {
        required : 'Seleccione el periodo de duración.'
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[datein]' : {
        required : 'Escoja la fecha de ingreso.',
        regular_expressions : 'Sólo se permite formato de fecha.'
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[dateout]' : {
        regular_expressions : 'Sólo se permite formato de fecha.',
        required : 'Escoja la fecha de fin.'
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_workshopbundle_workshopschedulingactivitytype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_workshopbundle_workshopschedulingpeopletype[profile]' : {
        required : "Seleccione el perfil."
      },
      'fishman_workshopbundle_workshopschedulingpeopletype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_pollbundle_benchmarkingtype[name]' : {
        required : 'Ingrese el nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfanuméricos.'
      },
      'fishman_pollbundle_benchmarkingtype[period]' : {
        required : 'Ingrese el periodo.',
        regular_expressions : 'Sólo se permiten números y \"-\".',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_pollbundle_benchmarkingtype[status]' : {
        required : "Seleccione el estado."
      },
      'poll_categories' : {
        required : "Seleccione la categoría."
      },
      'poll_polls' : {
        required : "Seleccione la encuesta."
      },
      'fishman_pollbundle_entitypolltype[duration]' : {
        required : 'Ingrese el número de duración.',
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}.")
      },
      'fishman_pollbundle_entitypolltype[period]' : {
        required : 'Seleccione el periodo de duración.'
      },
      'fishman_pollbundle_entitypolltype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_pollbundle_entitypolltype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_pollbundle_polltype[title]' : {
        required : 'Ingrese el titulo.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_pollbundle_polltype[version]' : {
        required : 'Ingrese una versión.',
        regular_expressions : 'Ingrese una versión válida.'
      },
      'fishman_pollbundle_polltype[description]' : {
        required : 'Ingrese una descripción.'
      },
      'fishman_pollbundle_polltype[category]' : {
        required : "Seleccione una categoría."
      },
      'fishman_pollbundle_polltype[type]' : {
        required : "Seleccione la tipo de evaluación."
      },
      'fishman_pollbundle_polltype[duration]' : {
        required : 'Ingrese el número de duración.',
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}.")
      },
      'fishman_pollbundle_polltype[period]' : {
        required : 'Seleccione el periodo de duración.'
      },
      'fishman_pollbundle_polltype[level]' : {
        required : "Seleccione la sección."
      },
      'fishman_pollbundle_polltype[sequence_question]' : {
        required : "Seleccione orden de preguntas."
      },
      'fishman_pollbundle_polltype[divide]' : {
        required : "Seleccione la divisón."
      },
      'fishman_pollbundle_polltype[number_question]' : {
        required : 'Ingrese el número divisor.',
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}.")
      },
      'fishman_pollbundle_polltype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_pollbundle_polltype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_pollbundle_pollsectiontype[name]' : {
        required : 'Ingrese el Nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_pollbundle_pollsectiontype[type]' : {
        required : "Seleccione el tipo."
      },
      'fishman_pollbundle_pollsectiontype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_pollbundle_pollsectiontype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_pollbundle_pollquestiontype[question]' : {
        required : 'Ingrese la Pregunta.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_pollbundle_pollquestiontype[pollsection_id]' : {
        required : 'Seleccione la sección.'
      },
      'fishman_pollbundle_pollquestiontype[type]' : {
        required : 'Seleccione el tipo de respuesta.'
      },
      'fishman_pollbundle_pollquestiontype[pollsection]' : {
        required : "Seleccione la sección."
      },
      'fishman_pollbundle_pollquestiontype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_pollbundle_pollquestiontype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_pollbundle_pollschedulingtype[custom_header_title_text]' : {
        required_select : "Ingrese el título de cabecera."
      },
      'fishman_pollbundle_pollschedulingtype[custom_header_title_description]' : {
        required_select : "Ingrese la descripción de cabecera."
      },
      'fishman_pollbundle_pollschedulingtype[description]' : {
        required : "Ingrese una descripción."
      },
      'fishman_pollbundle_pollschedulingtype[company_id]' : {
        required : "Seleccione una empresa."
      },
      'fishman_pollbundle_pollschedulingtype[type]' : {
        required : "Seleccione el tipo de evaluación."
      },
      'fishman_pollbundle_pollschedulingtype[multiple_evaluation]' : {
        required : "Seleccione una opción."
      },
      'fishman_pollbundle_pollschedulingtype[pollscheduling_period]' : {
        required : "El periodo es obligatorio.",
        regular_expressions : 'Sólo se permiten números y \"-\".',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_pollbundle_pollschedulingtype[duration]' : {
        required : 'Ingrese el número de duración.',
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}.")
      },
      'fishman_pollbundle_pollschedulingtype[period]' : {
        required : "Seleccione el periodo de duración."
      },
      'fishman_pollbundle_pollschedulingtype[initdate]' : {
        required : 'Escoja la fecha de ingreso.',
        regular_expressions : "Sólo se permite formato de fecha."
      },
      'fishman_pollbundle_pollschedulingtype[enddate]' : {
        required : 'Escoja la fecha de fin.',
        regular_expressions : "Sólo se permite formato de fecha."
      },
      'fishman_pollbundle_pollschedulingtype[message_gratitude]' : {
        required : "Ingrese un mensaje de agradecimiento."
      },
      'fishman_pollbundle_pollschedulingtype[message_end]' : {
        required : "Ingrese un mensaje de fin de encuesta."
      },
      'fishman_pollbundle_pollschedulingtype[message_inactive]' : {
        required : "Ingrese un mensaje de encuesta inactiva."
      },
      'fishman_pollbundle_pollschedulingtype[access_view_number]' : {
        required : 'Ingrese un número.',
        regular_expressions : 'Sólo se permiten números.',
        min : $.validator.format("Escribe un número mayor o igual a {0}."),
      },
      'fishman_pollbundle_pollschedulingtype[pollscheduling_anonymous]' : {
        required : "Seleccione una opción."
      },
      'fishman_pollbundle_pollschedulingtype[status_bar]' : {
        required : "Seleccione una opción."
      },
      'fishman_pollbundle_pollschedulingtype[counselor_report]' : {
        required : "Seleccione una opción."
      },
      'fishman_pollbundle_pollschedulingtype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_pollbundle_pollschedulingtype[status]' : {
        required : "Seleccione el estado."
      },
      'options_integrant' : {
        required : "Seleccione el participante."
      },
      'options_company' : {
        required : "Seleccione la empresa."
      },
      'options_poll' : {
        required : "Seleccione la encuesta."
      },
      'options_evaluated' : {
        required : "Seleccione al evaluado."
      },
      'options_level' : {
        required : "Seleccione el nivel de agrupación."
      },
      'fishman_pollbundle_pollschedulingsectiontype[name]' : {
        required : 'Ingrese el Nombre.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_pollbundle_pollschedulingsectiontype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_pollbundle_pollschedulingsectiontype[status]' : {
        required : "Seleccione el estado."
      },
      'fishman_pollbundle_pollschedulingquestiontype[question]' : {
        required : 'Ingrese la Pregunta.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Debe ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Debe ingresar {0} caracteres como máximo.")
      },
      'fishman_pollbundle_pollschedulingquestiontype[pollschedulingsection_id]' : {
        required : 'Seleccione la sección.'
      },
      'fishman_pollbundle_pollschedulingquestiontype[type]' : {
        required : 'Seleccione el tipo de respuesta.'
      },
      'fishman_pollbundle_pollschedulingquestiontype[sequence]' : {
        required : "Seleccione el orden."
      },
      'fishman_pollbundle_pollschedulingquestiontype[status]' : {
        required : "Seleccione el estado."
      },
    }
  });
  
  $(".formUser, .crudUser").validate({
    rules : {
      '_username' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      '_password' : {
        required : true,
        minlength : 4,
        maxlength : 16
      },
      'username' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      'fos_user_profile_form[username]' : {
        required : true,
        regular_expressions : a_formats.alphanum,
        minlength : 3,
        maxlength : 60
      },
      'fos_user_profile_form[email]' : {
        required : true,
        regular_expressions : a_formats.email,
        minlength : 6,
        maxlength : 60
      },
      'fos_user_profile_form[current_password]' : {
        required : true,
        minlength : 4,
        maxlength : 16
      },
      'fos_user_resetting_form[new][first]' : {
        required : true,
        minlength : 4,
        maxlength : 16
      },
      'fos_user_resetting_form[new][second]' : {
        required : true,
        minlength : 4,
        maxlength : 16
      },
    },
    messages : {
      '_username' : {
        required : 'Ingrese el nombre de usuario.',
        regular_expressions : 'Sólo caracteres alfa numéricos.',
        minlength : $.validator.format("Ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Ingresar {0} caracteres como máximo.")
      },
      '_password' : {
        required : 'Ingrese la contraseña.',
        minlength : $.validator.format("Ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Ingresar {0} caracteres como máximo.")
      },
      'username' : {
        required : 'Ingrese nombre o correo electrónico.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Ingresar {0} caracteres como máximo.")
      },
      'fos_user_profile_form[username]' : {
        required : 'Ingrese el nombre de usuario.',
        regular_expressions : 'Sólo se permiten caracteres alfa numéricos.',
        minlength : $.validator.format("Ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Ingresar {0} caracteres como máximo.")
      },
      'fos_user_profile_form[current_password]' : {
        required : 'Ingrese la contraseña.',
        minlength : $.validator.format("Ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Ingresar {0} caracteres como máximo.")
      },
      'fos_user_profile_form[email]' : {
        required : 'Ingrese su correo electrónico.',
        email : 'Ingrese un correo electrónico válido.',
        minlength : $.validator.format("Ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Ingresar {0} caracteres como máximo.")
      },
      'fos_user_resetting_form[new][first]' : {
        required : 'Ingrese la nueva contraseña.',
        minlength : $.validator.format("Ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Ingresar {0} caracteres como máximo.")
      },
      'fos_user_resetting_form[new][second]' : {
        required : 'Ingrese confirmación de contraseña.',
        minlength : $.validator.format("Ingresar {0} caracteres como mínimo."),
        maxlength : $.validator.format("Ingresar {0} caracteres como máximo.")
      }
    }
  });
  
  $(".formImport").validate({
    rules : {
      'fishman_authbundle_peopleimporttype[file]' : {
        required : true
      },
      'fishman_authbundle_peopleimporttype[type]' : {
        required : true
      },
      'fishman_entitybundle_companyorganizationalunitimporttype[file]' : {
        required : true
      },
      'fishman_entitybundle_companycourseimporttype[file]' : {
        required : true
      },
      'fishman_entitybundle_companychargeimporttype[file]' : {
        required : true
      },
      'fishman_Pollbundle_Pollschedulingpeopleimporttype[file]' : {
        required : true
      },
      'fishman_pollbundle_pollevaluatorimporttype[file]' : {
        required : true
      },
      'fishman_workshopbundle_workshopschedulingpeopleimporttype[file]' : {
        required : true
      },
      'fishman_workshopbundle_workshoppollevaluatorimporttype[file]' : {
        required : true
      }
    },
    messages : {
      'fishman_authbundle_peopleimporttype[file]' : {
        required : "El archivo es requerido."
      },
      'fishman_authbundle_peopleimporttype[type]' : {
        required : "El tipo es requerido."
      },
      'fishman_entitybundle_companyorganizationalunitimporttype[file]' : {
        required : "El archivo es requerido."
      },
      'fishman_entitybundle_companycourseimporttype[file]' : {
        required : "El archivo es requerido."
      },
      'fishman_entitybundle_companychargeimporttype[file]' : {
        required : "El archivo es requerido."
      },
      'fishman_Pollbundle_Pollschedulingpeopleimporttype[file]' : {
        required : "El archivo es requerido."
      },
      'fishman_pollbundle_pollevaluatorimporttype[file]' : {
        required : "El archivo es requerido."
      },
      'fishman_workshopbundle_workshopschedulingpeopleimporttype[file]' : {
        required : "El archivo es requerido."
      },
      'fishman_workshopbundle_workshoppollevaluatorimporttype[file]' : {
        required : "El archivo es requerido."
      }
    }
  });
  
  $(".formReport").validate({
    rules : {
      'options_integrant' : {
        required : true,
      }
    },
    messages : {
      'options_integrant' : {
        required : 'requiere.',
      }
    }
  });
    
  //Validation in Save Button
  
  $('form').submit(function () {
    var status;
    status = $(".formControl").valid(); //Validate again

    if(status==true) { 
      $('button[type$="submit"]').attr("disabled", "disabled");
    }

  });

})(jQuery);
