$('.schoolselect').change( function()  {
    $.ajax({
        type: "GET",
        data: {company_id : $(this).val()},
        url: globals.basepath + "/admin/schoolinformation/ajax/formelementsbyschool",
        success: function(response_data){
            if (response_data != ''){
                
                var object_data = eval('('+response_data+')');
                
                $('.select_headquarter').html(object_data.select_headquarter).show();
                $('#fishman_authbundle_workinginformationtype_headquarter_id').val('');

                $.uniform.update();
                $('.headquarter').uniform();
                
            }
            else{
                $('.select_headquarter').html('<em>No se encontro información para esta Empresa</em>');
            }
            
            $('.headquarter').change( function() {
                $('#fishman_authbundle_workinginformationtype_headquarter_id').val($(this).val());
            });
       }
   });
});

$('.headquarter').change( function() {
    $('#fishman_authbundle_workinginformationtype_headquarter_id').val($(this).val());
});
