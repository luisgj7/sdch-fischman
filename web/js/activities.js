$('.show-workbook-fancybox').fancybox({
  'width'         : '10',
  'height'        : '6',
  'autoScale'     : false,
  'transitionIn'  : 'none',
  'transitionOut' : 'none',
  'type'          : 'iframe',
  'titleShow'     : false,
  'onClosed'      : function() {
                      window.location.reload(true);
                    }

});

$('.edit-myers-briggs').fancybox({
  'width'         : '6',
  'height'        : '3',
  'autoScale'     : false,
  'transitionIn'  : 'none',
  'transitionOut' : 'none',
  'type'          : 'iframe',
  'titleShow'     : false,
  'onClosed'      : function() {
                      window.location.reload(true);
                    }
});

$('.edit-more-talents').fancybox({
  'width'         : '8',
  'height'        : '4',
  'autoScale'     : false,
  'transitionIn'  : 'none',
  'transitionOut' : 'none',
  'type'          : 'iframe',
  'titleShow'     : false,
  'onClosed'      : function() {
                      window.location.reload(true);
                    }
});

$('.edit-less-talents').fancybox({
  'width'         : '8',
  'height'        : '4',
  'autoScale'     : false,
  'transitionIn'  : 'none',
  'transitionOut' : 'none',
  'type'          : 'iframe',
  'titleShow'     : false,
  'onClosed'      : function() {
                      window.location.reload(true);
                    }
});

$('.edit-group-plangoal').fancybox({
  'width'         : '65%',
  'height'        : '85%',
  'autoScale'     : false,
  'transitionIn'  : 'none',
  'transitionOut' : 'none',
  'type'          : 'iframe',
  'titleShow'     : false,
  'onClosed'      : function() {
                      window.location.reload(true);
                    }
});

$('.edit-personal-plangoal').fancybox({
  'width'         : '65%',
  'height'        : '85%',
  'autoScale'     : false,
  'transitionIn'  : 'none',
  'transitionOut' : 'none',
  'type'          : 'iframe',
  'titleShow'     : false,
  'onClosed'      : function() {
                      window.location.reload(true);
                    }
});

$('.close-button').click(function(){
    parent.$.fancybox.close();
});
