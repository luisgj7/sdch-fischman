$('#options_company').change( function() {
    companyid = $('#options_company').val();
    $.ajax({
        type: "GET",
        data: { 'workshopschedulingid': workshopschedulingid, 'companyid': companyid, 'ids': ids },
        url: globals.basepath + "/dashboard/report/result/pollsbycompany",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollsByCompany').html(object_data.output);
                $('#options_pollscheduling option').remove();
                $('#options_pollscheduling').append(new Option('SELECCIONE UNA OPCIÓN', '', true, true));
                $('#options_evaluated option').remove();
                $('#options_evaluated').append(new Option('SELECCIONE UNA OPCIÓN', '', true, true));
                $.uniform.update();
                $('#options_poll').uniform();
                
                $('#options_poll').change( function() {
                    update_pollschedulings_by_poll(companyid, $(this).val());
                });
                  
            }
        }
    });
});

$('#options_poll').change( function() {
    update_pollschedulings_by_poll(companyid, $(this).val());
});

$('#options_pollscheduling').change( function() {
    update_evaluateds_by_pollschedulings(companyid, $(this).val())
});

function update_pollschedulings_by_poll(companyid, pollid) {
    $.ajax({
        type: "GET",
        data: { 'workshopschedulingid': workshopschedulingid, 'pollid': pollid, 'companyid': companyid, 'ids': ids },
        url: globals.basepath + "/dashboard/report/result/pollschedulingsOnlybypoll",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollschedulingsByPoll').html(object_data.output);
                $('#options_evaluated option').remove();
                $('#options_evaluated').append(new Option('SELECCIONE UNA OPCIÓN', '', true, true));
                $.uniform.update();
                $('#options_pollscheduling').uniform();

                $('#options_pollscheduling').change( function() {
                    update_evaluateds_by_pollschedulings(companyid, $(this).val());
                });
            }
        }
    });
}

function update_evaluateds_by_pollschedulings(companyid, pollschedulingid) {
    
    if (workshopschedulingid != '') {
        parameters = "workshopschedulingid=" + workshopschedulingid + "&pollschedulingids=" + pollschedulingid + "&companyid=" + companyid + "&ids=" + ids;
    }
    else {
        parameters = "pollschedulingids=" + pollschedulingid + "&companyid=" + companyid + "&ids=" + ids;
    }
    
    $.ajax({
        type: "GET",
        data: parameters,
        url:  globals.basepath + "/dashboard/report/result/evaluatedsbypollschedulings",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.evaluatedsByPollscheduling').html(object_data.output);
                $.uniform.update();
                $('#options_evaluated').uniform();
            }
        }
    });
}