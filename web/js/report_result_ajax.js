$('#options_company').change( function() {
    companyid = $('#options_company').val();
    $.ajax({
        type: "GET",
        data: { 'workshopschedulingid': workshopschedulingid, 'companyid': companyid, 'ids': ids },
        url: globals.basepath + "/dashboard/report/result/pollsbycompany",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollsByCompany').html(object_data.output);
                $('.pollschedulingsByPoll .selectOptionsData1 option').remove();
                $('.pollschedulingsByPoll .selectOptionsData2 option').remove();
                $('#options_evaluated option').remove();
                $('#options_evaluated').append(new Option('SELECCIONE UNA OPCIÓN', '', true, true));
                $.uniform.update();
                $('#options_poll').uniform();
                
                $('#options_poll').change( function() {
                    update_pollschedulings_by_poll(companyid, $(this).val());
                });
                  
            }
            else{
                $('.pollsByCompany').html('<em>No hay encuestas para esta empresa</em>');
            }
        }
    });
});

function update_pollschedulings_by_poll(companyid, pollid) {
    $.ajax({
        type: "GET",
        data: { 'workshopschedulingid': workshopschedulingid, 'pollid': pollid, 'companyid': companyid, 'ids': ids },
        url: globals.basepath + "/dashboard/report/result/pollschedulingsbypoll",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollschedulingsByPoll .leftBox select').html(object_data.output);
                $('.pollschedulingsByPoll .selectOptionsData2 option').remove();
                $('#options_evaluated option').remove();
                $('#options_evaluated').append(new Option('SELECCIONE UNA OPCIÓN', '', true, true));
            }
            else{
                $('.pollschedulingsByPoll').html('<em>No hay encuestas programadas para esta encuesta</em>');
            }
        }
    });
}

// Select options for a move to another
 
$("#moveToRightOptions").click( function (){
    update_evaluateds_by_pollschedulings();
});

$("#moveToLeftOptions").click( function (){
    update_evaluateds_by_pollschedulings();
});

$("#moveToAllRightOptions").click( function (){
    update_evaluateds_by_pollschedulings();
});

$("#moveToAllLeftOptions").click( function (){
    update_evaluateds_by_pollschedulings();
});

$(".selectOptionsData1").bind("dblclick", function(){
    update_evaluateds_by_pollschedulings();
});

$(".selectOptionsData2").bind("dblclick", function(){
    update_evaluateds_by_pollschedulings();
});

$('#options_poll').change( function() {
    update_pollschedulings_by_poll(companyid, $(this).val());
});

function update_evaluateds_by_pollschedulings() {
    
    var pollschedulingids = new Array();
    var i = 0;
    var parameters = '';
    
    $('.selectOptionsData2 option').each(function(){
        if ($(this).length) {
            if (i == 0) {
                pollschedulingids = $(this).val();
            }
            else {
                pollschedulingids += ',' + $(this).val();
            }
            i++;
        }
    });
    
    if (workshopschedulingid != '') {
        parameters = "workshopschedulingid=" + workshopschedulingid + "&pollschedulingids=" + pollschedulingids + "&companyid=" + companyid + "&ids=" + ids;
    }
    else {
        parameters = "pollschedulingids=" + pollschedulingids + "&companyid=" + companyid + "&ids=" + ids;
    }
    
    $.ajax({
        type: "GET",
        data: parameters,
        url:  globals.basepath + "/dashboard/report/result/evaluatedsbypollschedulings",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.evaluatedsByPollschedulings').html(object_data.output);
                $.uniform.update();
                $('#options_evaluated').uniform();
            }
            else{
                $('#options_evaluated').html('<em>No hay evaluadores en las encuestas programadas</em>');
            }
        }
    });
}