(function ($) {
  
  // Select options for a move to another
 
  $("#moveToRightOptions").click( function (){
    $('.selectOptionsData1 option:selected').appendTo(".selectOptionsData2");
  });
  
  $("#moveToLeftOptions").click( function (){
    $('.selectOptionsData2 option:selected').appendTo(".selectOptionsData1");
  });
  
  $("#moveToAllRightOptions").click( function (){
    $('.selectOptionsData1 option').appendTo(".selectOptionsData2");
    $(".selectOptionsData2 option").attr("selected", true);
  });
  
  $("#moveToAllLeftOptions").click( function (){
    $('.selectOptionsData2 option').appendTo(".selectOptionsData1");
    $(".selectOptionsData1 option").attr("selected", true);
  });
  
  $(".selectOptionsData1").bind("dblclick", function(){
    $('.selectOptionsData1 option:selected').each(function(){
      var text = $(this).text();
      var value = $(this).val();
      if (text != '') {
        $(".selectOptionsData2").append(new Option(text, value, true, true));
        $(".selectOptionsData1 option:selected").remove();
      }
    });
  });
  
  $(".selectOptionsData2").bind("dblclick", function(){
    $('.selectOptionsData2 option:selected').each(function(){
      var text = $(this).text();
      var value = $(this).val();
      if (text != '') {
        $(".selectOptionsData1").append(new Option(text, value, true, true));
        $(".selectOptionsData2 option:selected").remove();
      }
    });
  });

})(jQuery);