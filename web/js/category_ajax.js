$('.allCategories').change( function() {
    $.ajax({
        type: "GET",
        data: "category_id=" + $(this).val(),
        url: globals.basepath + "/admin/poll/choicebycategory",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                tinymce.activeEditor.setContent('');
                $('.pollCategory').html(object_data.combo).show();
                $('.pollType').val('');
                $('.pollDuration').val('');
                $('.pollPeriod').val('');
                $('.pollId_id').val($('.polls').val());
                $.uniform.update();
                $('.polls').uniform();
            }
            else{
                $('.pollCategory').html('<em>No hay encuestas para esta categoría</em>');
            }

            $('.polls').change( function() {
                update_poll_information($(this).val());
            });

        }
    });
});

$('.polls').change( function() {
    tinymce.get('fishman_pollbundle_pollschedulingtype_description').focus();
    update_poll_information($(this).val());
});

function update_poll_information(poll_id){
    $.ajax({
        type: "GET",
        data: "poll_id=" + poll_id,
        url: globals.basepath + "/admin/poll/json",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                tinymce.execCommand('mceSetContent',false , object_data.description);
                $('.pollDescription').val(object_data.description);
                $('.pollType').val(object_data.type);
                $('.pollDuration').val(object_data.duration);
                $('.pollPeriod').val(object_data.period);
                $('.pollId').val($('.polls').val());
  
                if (object_data.type != '') {
                  var et_option = object_data.type;
                  if (et_option == 'self_evaluation') {
                    $('.formMultipleEvaluation').addClass('formActiveOff');
                    $('.formPollschedulingRelations').removeClass('formActiveOff');
                  }
                  else {
                    $('.formMultipleEvaluation').removeClass('formActiveOff');
                    $('.formPollschedulingRelations').addClass('formActiveOff');
                    $(".pollschedulingRelations option[value='']").attr("selected",true);
                  }
                }
                
                $.uniform.update();
                
            }
            else{
                $('.pollCategory').html('<em>No hay encuestas para esta categoría</em>');
            }
        }
    });
}

$('.pollId').val($('.polls').val());

$('.allCompanies').change( function() {
    $.ajax({
        type: "GET",
        data: "company_id=" + $(this).val() + "&pollscheduling_id=" + pollschedulingid + 
              "&entity_type=" + entitytype + "&entity_id=" + entityid,
        url: globals.basepath + "/admin/pollscheduling/choicebycompany",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.allPollschedulingRelations').html(object_data.combo).show();
                $('.pollschedulingRelationId').val($('.pollschedulingRelations').val());
                $.uniform.update();
                $('.pollschedulingRelations').uniform();
            }
            else{
                $('.pollschedulingRelations').html('<em>No hay encuestas programadas para esta empresa</em>');
            }

            $('.pollschedulingRelations').change( function() {
                $('.pollschedulingRelationId').val($(this).val());
            });

        }
    });
});

$('.pollschedulingRelationId').val($('.pollschedulingRelations').val());

$('.allBenchmarkings').change( function() {
    update_benchmarking_information($(this).val());
    $('.benchmarkingId').val($(this).val());
});

function update_benchmarking_information(benchmarking_id){
    $.ajax({
        type: "GET",
        data: "benchmarking_id=" + benchmarking_id,
        url: globals.basepath + "/admin/benchmarking/json",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollPollschedulingPeriod').val(object_data.period);
            }
            else{
                $('.pollPollschedulingPeriod').html('<em>No hay periodo para este Benchmarking</em>');
            }
        }
    });
}

update_benchmarking_information($('.allBenchmarkings').val());

$('.pollschedulingRelations').change( function() {
    $('.pollschedulingRelationId').val($(this).val());
});
