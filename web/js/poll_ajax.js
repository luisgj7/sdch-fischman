$('.allCategories').change( function() {
    $.ajax({
        type: "GET",
        data: "category_id=" + $(this).val(),
        url: globals.basepath + "/admin/poll/choicebycategory",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollCategory').html(object_data.combo).show();
                $('.pollDuration').val('');
                $('.pollPeriod').val('');
                $('.pollId').val('');
                $.uniform.update();
                $('.polls').uniform();
            }
            else{
                $('.pollCategory').html('<em>No hay encuestas para esta categoría</em>');
            }
            
            $('.polls').change( function() {
                update_poll_information($(this).val());
            });
            
        }
    });
});

$('.polls').change( function() {
    update_poll_information($(this).val());
});

function update_poll_information(poll_id){
    $.ajax({
        type: "GET",
        data: "poll_id=" + poll_id,
        url: globals.basepath + "/admin/poll/json",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollDuration').val(object_data.duration);
                $('.pollPeriod').val(object_data.period);
                $('.pollId').val($('.polls').val());
                
                $.uniform.update();
                
            }
            else{
                $('.pollCategory').html('<em>No hay encuestas para esta categoría</em>');
            }
        }
    });
}

$('.pollId').val($('.polls').val());
