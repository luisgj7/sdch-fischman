(function ($) {
  
  // Click glass to form submit
  
  $('.dataTables_filter .srch').click(function() {
    if ($('.dataTables_filter .formRowWord input').val() == 'buscar') {
      $('.dataTables_filter .formRowWord input').val('');
    }
  });
  
  $('.dataTables_filter .buttonSearch').click(function() {
    if ($('.dataTables_filter .formRowWord input').val() == 'buscar') {
      $('.dataTables_filter .formRowWord input').val('');
    }
  });
  
  // Select checks
  $("input[name='form_deletes[check_all]']").change(function(){
    $('table.check_all tbody tr td input[type=checkbox]').each( function() {
      if ($("input[name='form_deletes[check_all]']:checked").length == 1) {
        this.checked = true;
      }
      else {
        this.checked = false;
      }
    });
    $.uniform.update();
  });
  
  // Datepicker
  
  if ($("input.datepicker").length) {
    $("input.datepicker").get().type = 'text';
  }
  
  // Workshopscheduling form
  
  if ($('.formCrudWorkshopscheduling').length) {
    $('.workshopDescription').focus();
  }
  
  // Reset form search
  
  $('button.buttonResetPeoples').click(function () {
    $('#form_code').attr('value', '');
    $('#form_surname').attr('value', '');
    $('#form_lastname').attr('value', '');
    $('#form_names').attr('value', '');
    $('#form_initdate').attr('value', '');
    $("#form_organizationalunit option[value=]").attr("selected",true);
    $("#form_charge option[value=]").attr("selected",true);
    $.uniform.update();
  });
  
  $('button.buttonResetEvaluators').click(function () {
    $('#form_evaluated').attr('value', '');
    $('#form_code').attr('value', '');
    $('#form_surname').attr('value', '');
    $('#form_lastname').attr('value', '');
    $('#form_names').attr('value', '');
    $('#form_initdate').attr('value', '');
    $('#form_enddate').attr('value', '');
    $('#form_study_year').attr('value', '');
    $("#form_type option[value=]").attr("selected",true);
    $("#form_organizationalunit option[value=]").attr("selected",true);
    $("#form_charge option[value=]").attr("selected",true);
    $("#form_grade option[value=]").attr("selected",true);
    $.uniform.update();
  });
  
  $('button.buttonResetSelf').click(function () {
    $('#form_evaluated').attr('value', '');
    $.uniform.update();
  });
  
  // Evaluateds Poll Dashboard
  
  $('button.buttonWI').click(function () {
    $('#wi').val($(this).attr('id'));
  });
  
  // Ubigeo
  
  if ($('select.countryselect').length) {
    
    if ($('.crud').parent().hasClass('formCrudCreate')) {
      $('.countryselect option[value="1"]').attr('selected', 'selected');
      $('.departmentvalue').val(1392);
      $('.provincevalue').val(1393);
    }
    
    var default_option = $('.countryselect :selected').val();
    if (default_option != '' && default_option == 1) {
      $('.formDepartment').removeClass('formActiveOff');
      $('.formCity').addClass('formActiveOff');
      $('.formCity input').val('');
    }
    else if (default_option != '') {
      $('.formDepartment').addClass('formActiveOff');
      $('.formCity').removeClass('formActiveOff');
    }
    else {
      $('.formDepartment').addClass('formActiveOff');
      $('.formCity').addClass('formActiveOff');
      $('.formCity input').val('');
    }
    
    $('select.countryselect').change(function () {
      var option = $(this).val();
      if (option != '' && option == 1) {
        $('.formDepartment').removeClass('formActiveOff');
        $('.formCity').addClass('formActiveOff');
      }
      else if (option != '') {
        $('.formDepartment').addClass('formActiveOff');
        $('.formCity').removeClass('formActiveOff');
      }
      else {
        $('.formDepartment').addClass('formActiveOff');
        $('.formCity').addClass('formActiveOff');
      }
      $('.formProvince').addClass('formActiveOff');
      $('.formDistrict').addClass('formActiveOff');
      $('.formCity input').val('');
  
      $('.provincevalue').val('');
      $('.districtvalue').val('');
    });
    
  }
  
  if ($('select.departmentselect').length) {
    var default_option = $('.departmentselect :selected').val();
    if (default_option != '') {
      $('.formProvince').removeClass('formActiveOff');
    }
    else {
      $('.formProvince').addClass('formActiveOff');
    }
  }
  
  if ($('select.provinceselect').length) {
    var default_option = $('.provinceselect :selected').val();
    if (default_option != '') {
      $('.formDistrict').removeClass('formActiveOff');
    }
    else {
      $('.formDistrict').addClass('formActiveOff');
    }
  }
  
  // Company
  
  if ($('select.natureFieldset').length) {
    var default_option = $('.natureFieldset :selected').val();
    if (default_option.length) {
      if (nature[default_option]['type'] == 'school') {
        $('.formFieldsetSchool').removeClass('formActiveOff');
        $('.formFieldsetUniversity').addClass('formActiveOff');
      }
      else if (nature[default_option]['type'] == 'university') {
        $('.formFieldsetUniversity').removeClass('formActiveOff');
        $('.formFieldsetSchool').addClass('formActiveOff');
      }
      else {
        $('.formFieldsetUniversity').addClass('formActiveOff');
        $('.formFieldsetSchool').addClass('formActiveOff');
      }
    }
    else {
      $('.formFieldsetSchool').addClass('formActiveOff');
      $('.formFieldsetUniversity').addClass('formActiveOff');
    }
    $('select.natureFieldset').change(function () {
      var option = $(this).val();
      if (option.length) {
        if (nature[option]['type'] == 'school') {
          $('.formFieldsetSchool').removeClass('formActiveOff');
          $('.formFieldsetUniversity').addClass('formActiveOff');
        }
        else if (nature[option]['type'] == 'university') {
          $('.formFieldsetUniversity').removeClass('formActiveOff');
          $('.formFieldsetSchool').addClass('formActiveOff');
        }
        else {
          $('.formFieldsetUniversity').addClass('formActiveOff');
          $('.formFieldsetSchool').addClass('formActiveOff');
        }
      }
      else {
        $('.formFieldsetSchool').addClass('formActiveOff');
        $('.formFieldsetUniversity').addClass('formActiveOff');
      }
    });
  }
  
  if ($('select.religiousAffiliationOn').length) {
    var default_option = $('.religiousAffiliationOn :selected').val();
    if (default_option == 1) {
      $('.formReligiousAffiliation').removeClass('formActiveOff');
    }
    else {
      $('.formReligiousAffiliation').addClass('formActiveOff');
    }
    $('select.religiousAffiliationOn').change(function () {
      var option = $(this).val();
      if (option == 1) {
        $('.formReligiousAffiliation').removeClass('formActiveOff');
      }
      else {
        $('.formReligiousAffiliation').addClass('formActiveOff');
      }
    });
  }
  
  // Validation in Select Multiple for Pollquestion
  
  $('button.buttonOptionActions').click(function () {
    var answer_type = $('select.answerType :selected').val();
    if (answer_type == 'unique_option' || answer_type == 'multiple_option' || answer_type == 'selection_option') {
      
      var options = '';
      var max = $(".selectOptions option").length;
      var i = 0;
      
      $("select.selectOptions option").attr("selected", true);
      
      $(".selectOptions option").each(function () {
        i++;
        options += $(this).val() + '::' + $(this).text();
        if (i < max) {
          options += '%%';
        }
      });
      
      $('input.hideOptions').val(options);
    }
  });
  
  // Validation in Select Multiple
  
  $('button.autoselectOptionData2').click(function () {
    $("select.selectOptionsData2 option").attr("selected", true);
  });
  
  // Save and Close
  
  $('button.buttonSaveAndClose').click(function () {
    var status;
    status = $(".formControl").valid(); //Validate again

    if(status==true) { 
        $("input.saveAndClose").val(1);
    }
  });
  
  // Others
  
  if ($('select.documentType').length) {
    var default_option = $('.documentType :selected').val();
    if (default_option != '') {
      switch (default_option) {
        case 'video':
          $('.formTypeVideo').removeClass('formActiveOff');
          $('.formTypeDocument').addClass('formActiveOff');
          $('.formTypeWebsite').addClass('formActiveOff');
          $('.formTypeEmbed').addClass('formActiveOff');
          break;
        case 'document':
          $('.formTypeVideo').addClass('formActiveOff');
          $('.formTypeDocument').removeClass('formActiveOff');
          $('.formTypeWebsite').addClass('formActiveOff');
          $('.formTypeEmbed').addClass('formActiveOff');
          break;
        case 'website':
          $('.formTypeVideo').addClass('formActiveOff');
          $('.formTypeDocument').addClass('formActiveOff');
          $('.formTypeWebsite').removeClass('formActiveOff');
          $('.formTypeEmbed').addClass('formActiveOff');
          break;
        case 'embed':
          $('.formTypeVideo').addClass('formActiveOff');
          $('.formTypeDocument').addClass('formActiveOff');
          $('.formTypeWebsite').addClass('formActiveOff');
          $('.formTypeEmbed').removeClass('formActiveOff');
          break;
      }
    }
    $('select.documentType').change(function () {
      var option = $(this).val();
      switch (option) {
        case 'video':
          $('.formTypeVideo').removeClass('formActiveOff');
          $('.formTypeDocument').addClass('formActiveOff');
          $('.formTypeWebsite').addClass('formActiveOff');
          $('.formTypeEmbed').addClass('formActiveOff');
          break;
        case 'document':
          $('.formTypeVideo').addClass('formActiveOff');
          $('.formTypeDocument').removeClass('formActiveOff');
          $('.formTypeWebsite').addClass('formActiveOff');
          $('.formTypeEmbed').addClass('formActiveOff');
          break;
        case 'website':
          $('.formTypeVideo').addClass('formActiveOff');
          $('.formTypeDocument').addClass('formActiveOff');
          $('.formTypeWebsite').removeClass('formActiveOff');
          $('.formTypeEmbed').addClass('formActiveOff');
          break;
        case 'embed':
          $('.formTypeVideo').addClass('formActiveOff');
          $('.formTypeDocument').addClass('formActiveOff');
          $('.formTypeWebsite').addClass('formActiveOff');
          $('.formTypeEmbed').removeClass('formActiveOff');
          break;
      }
    });
  }
  
  if ($('select.activityType').length) {
    var default_option = $('.activityType :selected').val();
    if (default_option != '') {
      if (default_option == 1 || default_option == 2 || default_option == 3) {
        if (default_option == 1) {
          $('.formPollschedulingsSelect div label').html('Reporte de Encuesta');
        }
        else {
          $('.formPollschedulingsSelect div label').html('Encuesta');
        }
        $('.formPollschedulingsSelect').removeClass('formActiveOff');
      }
      else {
        $('.formPollschedulingsSelect').addClass('formActiveOff');
      }
      if (default_option == 4) {
        $('.formTypeAnswer').removeClass('formActiveOff');
        $('.formNameActivityLabel').html('Pregunta<span class="req">*</span>');
      }
      else {
        $('.formTypeAnswer').addClass('formActiveOff');
        $('.formOptionsSelect').addClass('formActiveOff');
        $('.formNameActivityLabel').html('Nombre<span class="req">*</span>');
      }
    }
    $('select.activityType').change(function () {
      var option = $(this).val();
      if (option == 1 || option == 2 || option == 3) {
        if (option == 1) {
          $('.formPollschedulingsSelect div label').html('Reporte de Encuesta');
        }
        else {
          $('.formPollschedulingsSelect div label').html('Encuesta');
        }
        $('.formPollschedulingsSelect').removeClass('formActiveOff');
      }
      else {
        $('.formPollschedulingsSelect').addClass('formActiveOff');
      }
      if (option == 4) {
        $('.formTypeAnswer').removeClass('formActiveOff');
        $('.formNameActivityLabel').html('Pregunta<span class="req">*</span>');
      }
      else {
        $('.formTypeAnswer').addClass('formActiveOff');
        $('.formOptionsSelect').addClass('formActiveOff');
        $('.formNameActivityLabel').html('Nombre<span class="req">*</span>');
      }
    });
  }
  
  if ($('select.levelPoll').length) {
    var default_option = $('.levelPoll :selected').val();
    if (default_option == '0') {
      $(".dividePoll option[value='sections_show']").attr('disabled','disabled');
    }
    else {
      $(".dividePoll option[value='sections_show']").removeAttr('disabled');
    }
    $('select.levelPoll').change(function () {
      var option = $(this).val();
      if (option == '0') {
        $(".dividePoll option[value='sections_show']").attr('disabled','disabled');
      }
      else {
        $(".dividePoll option[value='sections_show']").removeAttr('disabled');
      }
    });
  }
  
  if ($('select.dividePoll').length) {
    var default_option = $('.dividePoll :selected').val();
    if (default_option == 'number_questions') {
      $('.formNumberQuestion').removeClass('formActiveOff');
    }
    else {
      $('.formNumberQuestion').addClass('formActiveOff');
    }
    $('select.dividePoll').change(function () {
      var option = $(this).val();
      if (option == 'number_questions') {
        $('.formNumberQuestion').removeClass('formActiveOff');
      }
      else {
        $('.formNumberQuestion').addClass('formActiveOff');
      }
    });
  }

  //Notification type
  
  if ($('select.notificationType').length) {
    var default_option = $('.notificationType :selected').val();
    if (default_option == 'message') {
        $('.formNotificationTypeActivity').addClass('formActiveOff');
        $('.formNotificationTypeStatus').addClass('formActiveOff');
        $('.formNotificationOnePerEvaluator').addClass('formActiveOff');
        $('.formSelectPerfil').removeClass('formActiveOff');
        $("#fishman_notificationbundle_notificationtype_one_per_evaluator_0").removeAttr("checked");
        $('#fishman_notificationbundle_notificationtype_notification_type_id').next().attr({'style': 'display: none'});
    } else {
        if (default_option == 'poll') {
            $('.formNotificationTypeActivity').removeClass('formActiveOff');
            $('.formNotificationTypeStatus').removeClass('formActiveOff');
            $('.formNotificationOnePerEvaluator').removeClass('formActiveOff');
            $('.typeNotificationCurrent').html('Encuesta<span class="req">*</span>');
            $('.formNotificationTypeStatusLabel').html('Estado de Encuesta<span class="req">*</span>');
        }
        else if (default_option == 'activity'){
            $('.formNotificationTypeActivity').removeClass('formActiveOff');
            $('.formNotificationTypeStatus').removeClass('formActiveOff');
            $('.formNotificationOnePerEvaluator').addClass('formActiveOff');
            $('.typeNotificationCurrent').html('Actividad<span class="req">*</span>');
            $('.formNotificationTypeStatusLabel').html('Estado de Actividad<span class="req">*</span>');
            $('.formSelectPerfil').removeClass('formActiveOff');
            $("#fishman_notificationbundle_notificationtype_one_per_evaluator_0").removeAttr("checked");
        };
    }
    $('select.notificationType').change(function () {
        var option = $(this).val();
        if (option == 'message') {
            $('.formNotificationTypeActivity').addClass('formActiveOff');
            $('.formNotificationTypeStatus').addClass('formActiveOff');
            $('.formNotificationOnePerEvaluator').addClass('formActiveOff');
            $('.formSelectPerfil').removeClass('formActiveOff');
            $("#fishman_notificationbundle_notificationtype_one_per_evaluator_0").removeAttr("checked");
            $('#fishman_notificationbundle_notificationtype_notification_type_id').next().attr({'style': 'display: none'});
        } else {
            if (option == 'poll') {
                $('.formNotificationTypeActivity').addClass('formActiveOff');
                $('.formNotificationTypeStatus').removeClass('formActiveOff');
                $('.formNotificationOnePerEvaluator').removeClass('formActiveOff');
                $('.typeNotificationCurrent').html('Encuesta<span class="req">*</span>');
                $('.formNotificationTypeStatusLabel').html('Estado de Encuesta<span class="req">*</span>');
            }
            else if (option == 'activity') {
                $('.formNotificationTypeActivity').addClass('formActiveOff');
                $('.formNotificationTypeStatus').removeClass('formActiveOff');
                $('.formNotificationOnePerEvaluator').addClass('formActiveOff');
                $('.typeNotificationCurrent').html('Actividad<span class="req">*</span>');
                $('.formNotificationTypeStatusLabel').html('Estado de Actividad<span class="req">*</span>');
                $('.formSelectPerfil').removeClass('formActiveOff');
                $("#fishman_notificationbundle_notificationtype_one_per_evaluator_0").removeAttr("checked");
            };
        $('.formNotificationTypeActivity').removeClass('formActiveOff');
        $('.formNotificationTypeStatus').removeClass('formActiveOff');
        }
    });
  }
  
  $('.type_notification').change( function() {
      $('#fishman_notificationbundle_notificationtype_notification_type_id').val($(this).val());
  });

  $('.type_na').change( function() {
      $('#fishman_notificationbundle_notificationschedulingtype_notification_type_id').val($(this).val());
  });

  if ($('select.ejecutionType').length) {
    var default_option = $('.ejecutionType :selected').val();
    var nt_option = $('.notificationType :selected').val();
    if (default_option == 'automatic') {
      $('.formPredecessor').removeClass('formActiveOff');
      $('.formEjecutionSince').removeClass('formActiveOff');
      $('.formEjecutionReplay').removeClass('formActiveOff');
      $('.formSelectedTrigger').addClass('formActiveOff');
      if (nt_option != 'message' && nt_option != '') {
        $('.formNotificationTypeStatus').removeClass('formActiveOff');
      }
    }
    else if (default_option == 'manual') {
      $('.formPredecessor').addClass('formActiveOff');
      $('.formEjecutionSince').addClass('formActiveOff');
      $('.formEjecutionReplay').addClass('formActiveOff');
      $('.formSelectedTrigger').addClass('formActiveOff');
      if (nt_option != 'message') {
        $('.formNotificationTypeStatus').removeClass('formActiveOff');
      }
    }
    else if (default_option == 'trigger') {
      $('.formPredecessor').addClass('formActiveOff');
      $('.formEjecutionSince').addClass('formActiveOff');
      $('.formEjecutionReplay').addClass('formActiveOff');
      $('.formSelectedTrigger').removeClass('formActiveOff');
      $('.formNotificationTypeStatus').addClass('formActiveOff');
    }
    $('select.ejecutionType').change(function () {
      var option = $(this).val();
      var nt_option = $('.notificationType :selected').val();
      if (option == 'automatic') {
        $('.formPredecessor').removeClass('formActiveOff');
        $('.formEjecutionSince').removeClass('formActiveOff');
        $('.formEjecutionReplay').removeClass('formActiveOff');
        $('.formSelectedTrigger').addClass('formActiveOff');
        if (nt_option != 'message') {
          $('.formNotificationTypeStatus').removeClass('formActiveOff');
        }
      }
      else if (option == 'manual') {
        $('.formPredecessor').addClass('formActiveOff');
        $('.formEjecutionSince').addClass('formActiveOff');
        $('.formEjecutionReplay').addClass('formActiveOff');
        $('.formSelectedTrigger').addClass('formActiveOff');
        if (nt_option != 'message') {
          $('.formNotificationTypeStatus').removeClass('formActiveOff');
        }
      }
      else if (option == 'trigger') {
        $('.formPredecessor').addClass('formActiveOff');
        $('.formEjecutionSince').addClass('formActiveOff');
        $('.formEjecutionReplay').addClass('formActiveOff');
        $('.formSelectedTrigger').removeClass('formActiveOff');
        $('.formNotificationTypeStatus').addClass('formActiveOff');
      }
    });
  }
  
  if ($('select.replayFieldset').length) {
    var default_option = $('.replayFieldset :selected').val();
    if (default_option == 1) {
      $('.formReplayFieldset').removeClass('formActiveOff');
    }
    else {
      $('.formReplayFieldset').addClass('formActiveOff');
    }
    $('select.replayFieldset').change(function () {
      var option = $(this).val();
      if (option == 1) {
        $('.formReplayFieldset').removeClass('formActiveOff');
      }
      else {
        $('.formReplayFieldset').addClass('formActiveOff');
      }
    });
  }
  
  if ($('.repetitionNumber input:checked').length) {
    var default_option = $('.repetitionNumber input:checked').val();
    if (default_option == 'after') {
      $('.formRepetitionNumber').removeClass('formActiveOff');
    }
    else {
      $('.formRepetitionNumber').addClass('formActiveOff');
    }
    $('.repetitionNumber input').change(function () {
      var option = $(this).val();
      if (option == 'after') {
        $('.formRepetitionNumber').removeClass('formActiveOff');
      }
      else {
        $('.formRepetitionNumber').addClass('formActiveOff');
      }
    });
  }
  
  if ($('select.answerType').length) {
    var default_option = $('.answerType').val();
    if (default_option == 'unique_option' || default_option == 'multiple_option' || default_option == 'selection_option') {
      if (default_option == 'selection_option') {
        $('.formAlignment').addClass('formActiveOff');
      }
      else {
        $('.formAlignment').removeClass('formActiveOff');
      }
      $('.formOptionsSelect').removeClass('formActiveOff');
    }
    else {
      $('.formAlignment').addClass('formActiveOff');
      $('.formOptionsSelect').addClass('formActiveOff');
    }
    $('select.answerType').change(function () {
      var option = $(this).val();
      if (option == 'unique_option' || option == 'multiple_option' || option == 'selection_option') {
        if (option == 'selection_option') {
          $('.formAlignment').addClass('formActiveOff');
        }
        else {
          $('.formAlignment').removeClass('formActiveOff');
        }
        $('.formOptionsSelect').removeClass('formActiveOff');
      }
      else {
        $('.formAlignment').addClass('formActiveOff');
        $('.formOptionsSelect').addClass('formActiveOff');
      }
    });
  }
  
  if ($('select.evaluatorType').length) {
    var default_option = $('.evaluatorType :selected').val();
    if (default_option == '360') {
      $('.formEvaluator360').removeClass('formActiveOff');
      $('.formEvaluator1to1').addClass('formActiveOff2');
      $('.buttonSaveEva').addClass('formActiveOff2');
      $('.buttonFindEva').removeClass('formActiveOff2');
      $('.menuFilter').removeClass('formActiveOff2');
    }
    else {
      $('.formEvaluator360').addClass('formActiveOff');
      if (default_option == 'one') {
        $('.formEvaluator1to1').removeClass('formActiveOff2');
        $('.menuFilter').addClass('formActiveOff2');
        $('.buttonSaveEva').removeClass('formActiveOff2');
        $('.buttonFindEva').addClass('formActiveOff2');
      }else{
        $('.buttonSaveEva').addClass('formActiveOff2');
        $('.buttonFindEva').removeClass('formActiveOff2');
        $('.formEvaluator1to1').addClass('formActiveOff2');
        $('.menuFilter').removeClass('formActiveOff2');
      } 
    }
    $('select.evaluatorType').change(function () {
      $('#form_type_value').val($(this).val())
      var option = $(this).val();
      if (option == '360') {
        $('.formEvaluator360').removeClass('formActiveOff');
        $('.formEvaluator1to1').addClass('formActiveOff2');
        $('.menuFilter').removeClass('formActiveOff2');
        $('.buttonSaveEva').addClass('formActiveOff2');
        $('.buttonFindEva').removeClass('formActiveOff2');
      }
      else {
        if (option == 'one') {
          $('.formEvaluator1to1').removeClass('formActiveOff2');
          $('.menuFilter').addClass('formActiveOff2');
          $('.buttonSaveEva').removeClass('formActiveOff2');
          $('.buttonFindEva').addClass('formActiveOff2');
        } else {
          $('.formEvaluator1to1').addClass('formActiveOff2');
          $('.menuFilter').removeClass('formActiveOff2');
          $('.buttonSaveEva').addClass('formActiveOff2');
          $('.buttonFindEva').removeClass('formActiveOff2');
        }
        $('.formEvaluator360').addClass('formActiveOff');
      }
    });
  }
  
  // Function for control of aditional  attributes in PollschedulingEvaluator
  
  if ($('#form_attrib').length) {
    var default_option = $('#form_attrib :selected').val();
    if (default_option == 1) {
      $('.additionalAttributes').removeClass('formActiveOff');
    }
    else {
      $('.additionalAttributes').addClass('formActiveOff');
    }
    $('#form_attrib').change(function () {
      var option = $(this).val();
      if (option == 1) {
        $('.additionalAttributes').removeClass('formActiveOff');
      }
      else {
        $('.additionalAttributes').addClass('formActiveOff');
      }
    });
  }

  //Function filter for data type
  
  if ($('#form_data_type').length) {
    var default_option = $('#form_data_type :selected').val();
    if (default_option == 'working') {
      $('.workingFields').removeClass('formActiveOff');
      $('.schoolFields').addClass('formActiveOff');
    }
    else if (default_option == 'school') {
      $('.workingFields').addClass('formActiveOff');
      $('.schoolFields').removeClass('formActiveOff');
    }
    else if (default_option == 'university') {
      $('.workingFields').addClass('formActiveOff');
      $('.schoolFields').addClass('formActiveOff');
    }
    else {
      $('.workingFields').addClass('formActiveOff');
      $('.schoolFields').addClass('formActiveOff');
    }
    $('#form_data_type').change(function () {
      var option = $(this).val();
      if (option == 'working') {
        $('.workingFields').removeClass('formActiveOff');
        $('.schoolFields').addClass('formActiveOff');
      }
      else if (option == 'school'){
        $('.workingFields').addClass('formActiveOff');
        $('.schoolFields').removeClass('formActiveOff');
      } 
      else if (option == 'university') {
        $('.workingFields').addClass('formActiveOff');
        $('.schoolFields').addClass('formActiveOff');
      } else if (option == ''){
        $('.workingFields').addClass('formActiveOff');
        $('.schoolFields').addClass('formActiveOff');
      }
    });
  }

  $('#fishman_notificationbundle_notificationtype_one_per_evaluator, #fishman_notificationbundle_notificationschedulingtype_one_per_evaluator').click(function(){
    if($(this).is(':checked')){
      activateOnePerEvaluator();
    }
    else{
      deactivateOnePerEvaluator();
    }
  });

  function deactivateOnePerEvaluator(){
    //Limpia y desbloquea el control de perfil
    $('.selectOptionsData2 option').appendTo(".selectOptionsData1");
    $('.formSelectPerfil').removeClass('formActiveOff');
  }

  /**
   * Al activar esta opción, en el perfil debe seleccionar solo Evaluator
   * Bloquea el control para que no pueda editarse
   */
  function activateOnePerEvaluator(){
    $('.selectOptionsData2 option').appendTo(".selectOptionsData1");
    $('.selectOptionsData1 option[value="evaluator"]').appendTo(".selectOptionsData2");
    $('.formSelectPerfil').addClass('formActiveOff');
  }
  
})(jQuery);
