(function ($) {
  
  // Select options for a move to another
  
  $(".moveToRightOptions").click( function (){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsCopy1 option:selected').each( function (){
      var text = $(this).text();
      var value = $(this).val();
      if ($('#' + id + ' .selectOptionsCopy2 option').length > 0) {
        notAdd = false;
        $('#' + id + ' .selectOptionsCopy2 option').each( function (){
          if (value == $(this).val()) {
            notAdd = true;
          }
        });
        if (!notAdd) {
          $('#' + id + ' .selectOptionsCopy2').append(new Option(text, value, true, true));
        }
      }
      else {
        $('#' + id + ' .selectOptionsCopy2').append(new Option(text, value, true, true));
      }
      $(this).attr('selected', false);
      $(this).attr('disabled', true);
    });
    
    $('#' + id + ' .selectOptionsCopy2 option').each( function (){
      var text = $(this).text().split("--- ").pop();
      $(this).text(text);
    });
  });
  
  $(".moveToLeftOptions").click( function (){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsCopy2 option:selected').each( function (){
      var value = $(this).val();
      $('#' + id + ' .selectOptionsCopy1 option:disabled').each( function (){
        if (value == $(this).val()) {
          $(this).attr('disabled', false);
          $(this).attr('selected', true);
        }
      });
      $(this).remove();
    });
  });
  
  $(".moveToAllRightOptions").click( function (){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsCopy1 option').each( function (){
      if (!$(this).attr('disabled')) {
        var text = $(this).text();
        var value = $(this).val();
        if ($('#' + id + ' .selectOptionsCopy2 option').length > 0) {
          notAdd = false;
          $('#' + id + ' .selectOptionsCopy2 option').each( function (){
            if (value == $(this).val()) {
              notAdd = true;
            }
          });
          if (!notAdd) {
            $('#' + id + ' .selectOptionsCopy2').append(new Option(text, value, true, true));
          }
        }
        else {
          $('#' + id + ' .selectOptionsCopy2').append(new Option(text, value, true, true));
        }
        $(this).attr('selected', false);
        $(this).attr('disabled', true);
      }
    });
    
    $('#' + id + ' .selectOptionsCopy2 option').each( function (){
      var text = $(this).text().split("--- ").pop();
      $(this).text(text);
    });
  });
  
  $(".moveToAllLeftOptions").click( function (){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsCopy2 option').each( function (){
      var value = $(this).val();
      $('#' + id + ' .selectOptionsCopy1 option').each( function (){
        if (value == $(this).val()) {
          $(this).attr('disabled', false);
          $(this).attr('selected', true);
        }
      });
      $(this).remove();
    });
  });
  
  $(".selectOptionsCopy1").bind("dblclick", function(){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsCopy1 option:selected').each(function(){
      if ($(this).text() != '') {
        $('#' + id + ' .selectOptionsCopy2').append(new Option($(this).text(), $(this).val(), true, true));
        $(this).attr('selected', false);
        $(this).attr('disabled', true);
      }
    });
    
    $('#' + id + ' .selectOptionsCopy2 option').each( function (){
      var text = $(this).text().split("--- ").pop();
      $(this).text(text);
    });
  });
  
  $(".selectOptionsCopy2").bind("dblclick", function(){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsCopy2 option:selected').each(function(){
      var value = $(this).val();
      $('#' + id + ' .selectOptionsCopy1 option').each( function (){
        if (value == $(this).val()) {
          $(this).attr('disabled', false);
          $(this).attr('selected', true);
        }
      });
      $(this).remove();
    });
  });

})(jQuery);