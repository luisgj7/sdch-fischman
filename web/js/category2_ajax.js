$('.allCategories').change( function() {
    $.ajax({
        type: "GET",
        data: "category_id=" + $(this).val(),
        url: globals.basepath + "/admin/workshop/choicebycategory",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                tinyMCE.activeEditor.setContent('');
                $('.workshopCategory').html(object_data.combo).show();
                $('.workshopName').val('');
                $('.workshopId').val($('.workshops').val());
                $.uniform.update();
                $('.workshops').uniform();
            }
            else{
                $('.workshopCategory').html('<em>No hay talleres para esta categoría</em>');
            }

            $('.workshops').change( function() {
                update_workshop_information($(this).val());
            });

        }
    });
});

$('.workshops').change( function() {
    tinymce.get('fishman_workshopbundle_workshopschedulingtype_description').focus();
    update_workshop_information($(this).val());
});

function update_workshop_information(workshop){
    $.ajax({
        type: "GET",
        data: "workshop_id=" + workshop,
        url: globals.basepath + "/admin/workshop/json",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                tinymce.execCommand('mceSetContent',false , object_data.description);
                $('.workshopDescription').val(object_data.description);
                $('.workshopName').val(object_data.name);
                $('.workshopId').val($('.workshops').val());
                $.uniform.update();
            }
            else{
                $('.workshopCategory').html('<em>No hay talleres para esta categoría</em>');
            }
        }
    });
}

$('.workshopId').val($('.workshops').val());
update_workshop_information($('.workshops').val());
