paicontinue = 0;
updateprogressbar();

$('#pai-pending').click(function(){
    paicontinue = 1;
    paiImportStateButtons();
    paiPreimportevaluation(0, true);
    //Al terminar todo se da un mensaje con los resultados
    return false;
});


$('#pai-stop').click(function(){
    paicontinue = 0;
    $(this).attr("disabled", "disabled");
    return false;
});

function paiImportStateButtons(){
    $('#pai-pending').attr("disabled", "disabled");
    $('#pai-stop').removeAttr("disabled", "disabled");
    $('#pai-discard').css("display", "none");
    $('.pai-importbutton a').css("display", "none");
    $('.loader').css("display", "block");
}

function paiStopStateButtons(){
    $('#pai-pending').removeAttr("disabled", "disabled");
    $('#pai-stop').attr("disabled", "disabled");
    $('#pai-discard').css("display", "block");
    $('.pai-importbutton a').css("display", "block");
    $('.loader').css("display", "none");
}

function paiPreimportevaluation(paiindex, recursive){
    
    if(paiindex >= paicount){
        paiStopStateButtons();
        return;
    }

    if(recursive & (paicontinue == 0) ){
        paiStopStateButtons();
        return;
    }

    pai = pollapplicationimports[paiindex];
    
    if(pai.status == 1){
        if(recursive) paiPreimportevaluation(paiindex + 1, true);
        return;
    }
    
    paiImportEvaluation(paiindex, recursive);
}

function paiImportEvaluation(paiindex, recursive){
    pai = pollapplicationimports[paiindex];
    $.ajax({
      type: "POST",
      url: globals.basepath + "/admin/pollapplication/" + pai.pollscheduling_id + "/import/" + pai.id + '/evaluation',
      settings: {async: false},
      success: function(data, textStatus, jqXHR){
        var object_data = eval('('+data+')');
        if (object_data.pai == 1) {
            //Actualiza la base de datos, el arreglo, la lista html y el indicador de progreso
            pai.status = 1;
            paiok++;
            updateprogressbar();
            $('#pai-' + pai.id + ' .pai-status').html('Listo');
            $('#pai-' + pai.id + ' .pai-importbutton').html('Importado');
            
            if(recursive) paiPreimportevaluation(paiindex + 1, true);
            else paiStopStateButtons();
        }
        else{
            if(recursive) paiPreimportevaluation(paiindex + 1, true);
            else paiStopStateButtons();
        }
      },
      error: function(jqXHR, textStatus, errorThrown){
        if(recursive) paiPreimportevaluation(paiindex + 1, true);
        else paiStopStateButtons();
      }
    });
}

function updateprogressbar(){
    $('.countText').html(paiok + ' de ' + paicount + ' evaluaciones importadas');

    var percentaje = 0;
    if(paiok > 0 && paicount > 0){
        percentaje = (paiok / paicount) * 100;
    }

    $('.ui-progressbar-value').css('width', percentaje + '%');
}
