$('.companyselect').change( function()  {
    $.ajax({
        type: "GET",
        data: {company_id : $(this).val(), unittype : $('.unittypeselect').val(), ignore_user : $('#user_id').val()},
        url: globals.basepath + "/admin/workinginformation/ajax/formelementsbycompany",
        success: function(response_data){
            if (response_data != ''){
                
                var object_data = eval('('+response_data+')');
                
                $('.select_boss').html(object_data.select_boss).show();
                $('#fishman_authbundle_workinginformationtype_boss_id').val('');
                
                $('.select_companyorganizationalunit').html(object_data.select_companyorganizationalunit).show();
                $('#fishman_authbundle_workinginformationtype_companyorganizationalunit_id').val('');
                
                $('.select_companycharge').html(object_data.select_companycharge).show();
                $('#fishman_authbundle_workinginformationtype_companycharge_id').val('');
                
                $('.select_headquarter').html(object_data.select_headquarter).show();
                $('#fishman_authbundle_workinginformationtype_headquarter_id').val('');

                $.uniform.update();
                $('.boss').uniform();
                if (object_data.select_companyorganizationalunit) {
                    $('.companyorganizationalunit').uniform();
                }
                $('.companycharge').uniform();
                $('.headquarter').uniform();
                
            }
            else{
                $('.select_boss').html('<em>No se encontro información para esta Empresa</em>');
                $('.select_companycharge').html('<em>No se encontro información para esta Empresa</em>');
                $('.select_companyorganizationalunit').html('<em>No se encontro información para esta Empresa</em>');
                $('.select_headquarter').html('<em>No se encontro información para esta Empresa</em>');
            }
            
            $('.boss').change( function() {
                $('#fishman_authbundle_workinginformationtype_boss').val($(this).val());
            });
            
            $('.companyorganizationalunit').change( function() {
                $('#fishman_authbundle_workinginformationtype_companyorganizationalunit_id').val($(this).val());
            });
            
            $('.companycharge').change( function() {
                $('#fishman_authbundle_workinginformationtype_companycharge_id').val($(this).val());
            });
            
            $('.headquarter').change( function() {
                $('#fishman_authbundle_workinginformationtype_headquarter_id').val($(this).val());
            });
       }
   });
});

$('.unittypeselect').change( function()  {
    $.ajax({
        type: "GET",
        data: {company_id : $('.companyselect').val(), unittype : $(this).val()},
        url: globals.basepath + "/admin/workinginformation/ajax/formelementsbycompanybyunittype",
        success: function(response_data){
            if (response_data != ''){
                
                var object_data = eval('('+response_data+')');
                
                $('.select_companyorganizationalunit').html(object_data.select_companyorganizationalunit).show();
                $('#fishman_authbundle_workinginformationtype_companyorganizationalunit_id').val('');
                
                $.uniform.update();
                $('.companyorganizationalunit').uniform();
                
            }
            else{
                $('.select_companyorganizationalunit').html('<em>No se encontro información para esta Empresa</em>');
            }
            
            $('.companyorganizationalunit').change( function() {
                $('#fishman_authbundle_workinginformationtype_companyorganizationalunit_id').val($(this).val());
            });
       }
   });
});

$('.boss').change( function() {
    $('#fishman_authbundle_workinginformationtype_boss').val($(this).val());
});

$('.companyorganizationalunit').change( function() {
    $('#fishman_authbundle_workinginformationtype_companyorganizationalunit_id').val($(this).val());
});

$('.companycharge').change( function() {
    $('#fishman_authbundle_workinginformationtype_companycharge_id').val($(this).val());
});

$('.headquarter').change( function() {
    $('#fishman_authbundle_workinginformationtype_headquarter_id').val($(this).val());
});
