$('.allCategories').change( function() {
    $.ajax({
        type: "GET",
        data: "category_id=" + $(this).val(),
        url: globals.basepath + "/admin/workshop/choicebycategory",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                tinyMCE.activeEditor.setContent('');
                $('.workshopCategory').html(object_data.combo).show();
                $('.workshopName').val('');
                $('.workshopId').val($('.workshops').val());
                $.uniform.update();
                $('.workshops').uniform();
            }
            else{
                $('.workshopCategory').html('<em>No hay talleres para esta categoría</em>');
            }

            $('.workshops').change( function() {
                tinymce.get('fishman_workshopbundle_workshopschedulingtype_description').focus();
                update_workshop_information($(this).val());
            });

        }
    });
});

$('.workshops').change( function() {
    tinymce.get('fishman_workshopbundle_workshopschedulingtype_description').focus();
    update_workshop_information($(this).val());
});

function update_workshop_information(workshop){
    $.ajax({
        type: "GET",
        data: "workshop_id=" + workshop,
        url: globals.basepath + "/admin/workshop/json",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                tinymce.execCommand('mceSetContent',false , object_data.description);
                $('.workshopDescription').val(object_data.description);
                $('.workshopName').val(object_data.name);
                $('.workshopId').val($('.workshops').val());
                $.uniform.update();
            }
            else{
                $('.workshopCategory').html('<em>No hay talleres para esta categoría</em>');
            }
        }
    });
}

$('.workshopId').val($('.workshops').val());
update_workshop_information($('.workshops').val());

// Selected Custom Header Title

if ($('select.customHeaderTitle').length) {
    var option = $('select.customHeaderTitle :selected').val();
    if (option != '' && option != 0) {
        $('.formCustomHeaderTitle').removeClass('formActiveOff');
    }
    else {
        $('.formCustomHeaderTitle').addClass('formActiveOff');
    }
    $('select.customHeaderTitle').change(function () {
        var option = $('select.customHeaderTitle :selected').val();
        if (option != '' && option != 0) {
            $('.formCustomHeaderTitle').removeClass('formActiveOff');
        }
        else {
            $('.formCustomHeaderTitle').addClass('formActiveOff');
        }
    });
}

// Function for update information in custom header of pollscheduling 

$('#titleCompanyCustom').change(function () {
    var option = $(this).val();
    $('#fishman_workshopbundle_workshopschedulingtype_custom_header_title').val(option);
});
$('#logoCompanyCustom').change(function () {
    var option = $(this).val();
    $('#fishman_workshopbundle_workshopschedulingtype_custom_header_logo').val(option);
});
