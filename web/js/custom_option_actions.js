var option_default_value = 0;

function optionAdd () {
  
  var option_text = $('input.optionText').val();
  var option_score = $('input.optionValue').val();
  
  if (option_text != '' && option_score != '') {
    
    if (option_score.match(/^\d+$/)) {
      
      var deny_text = false;
      
      $(".selectOptions option").each(function () {
        if (option_text == $(this).text()) {
          deny_text = true;
        }
      });
      
      if (!deny_text) {
        $(".selectOptions option").each(function () {
          if (option_default_value < $(this).val()) {
            option_default_value = $(this).val();
          }
        });
        option_default_value++;
        default_value = option_default_value;
        $('.errorOptionsAddUpdate').html('');
        $('.errorOptionsAddUpdate').attr('style','display: none;');
        $('.formOptionsSelectMultiple label.error').attr('style','display: none;');
        $('select.selectOptions').append(new Option(option_score + ' | ' + option_text, default_value, true, true));
        $('input.optionText').val('');
        $('input.optionValue').val('');
        $('input.optionText').focus();
        $("select.selectOptions option").attr("selected", true);
      }
      else {
        $('.errorOptionsAddUpdate').attr('style','display: block;');
        $('.errorOptionsAddUpdate').html('La Opción ya existe.');
      }
      
    }
    else {
      $('.errorOptionsAddUpdate').attr('style','display: block;');
      $('.errorOptionsAddUpdate').html('Sólo números en el Valor.');
    }
    
  }
  else {
    $('.errorOptionsAddUpdate').attr('style','display: block;');
    $('.errorOptionsAddUpdate').html('Ingrese la Opción y su Valor.');
  }
}

function optionUpdate () {
  
  var option_text = $('input.optionText').val();
  var option_score = $('input.optionValue').val();

  if (option_text != '' && option_score != '') {
    
    var deny_text = false;
    var default_value = $('.selectOptions option:selected').val();
    var option = $('.selectOptions option:selected').text().split(' | ');
    var default_text = option[1];
      
    $(".selectOptions option").each(function () {
      var text = $(this).text().split(' | ');
      
      if ((option_text == text[1]) && (option_text != default_text)) {
        deny_text = true;
      }
    });

    if (!deny_text) {
      $('.errorOptionsAddUpdate').attr('style','display: none;');
      $('.errorOptionsAddUpdate').html('');
      $('input.optionText').val('');
      $('input.optionValue').val('');
      $('a#optionAdd').attr('style', 'display: block;');
      $('a#optionUpdate').attr('style', 'display: none;');
      
      // Update option in select
      $(".selectOptions option[value=" + default_value + "]").text(option_score + ' | ' + option_text);
    }
    else {
      $('.errorOptionsAddUpdate').attr('style','display: block;');
      $('.errorOptionsAddUpdate').html('La Opción ya existe.');
    }
    
  }
  else {
    $('.errorOptionsAddUpdate').attr('style','display: block;');
    $('.errorOptionsAddUpdate').html('Ingrese la Opción.');
  }
}

(function ($) {
  
  $('input.optionText').keypress(function (e) {
    if (e.keyCode == 13) {
      return false; 
    }
  });
  
  $('input.optionValue').keypress(function (e) {    
    if (e.keyCode == 13) {
      return false; 
    }
  });
  
  // Add / MoveUp / MoveDown / Update / Delete -> option(s) in Select Multiple
  
  $('a#optionAdd').click(function () {
    optionAdd();
  });
  
  $('a#optionUpdate').click(function () {
    optionUpdate();
  });

  $('#optionUp').click(function () {
    $('.selectOptions option:selected').each(function() {
      $(this).insertBefore($(this).prev());
    });
  });

  $('#optionDown').click(function () {
    $('.selectOptions option:selected').each(function() {
      $(this).insertAfter($(this).next());
    });
  });
  
  $('a#optionModify').click(function () {
    if ($('.selectOptions option:selected').length > 0) {
      var option = $('.selectOptions option:selected').text().split(' | ');
      var default_score = option[0];
      var default_text = option[1];
      $('input.optionText').val(default_text);
      $('input.optionValue').val(default_score);
      $('a#optionAdd').attr('style', 'display: none;');
      $('a#optionUpdate').attr('style', 'display: block;');
      $('input.optionText').select();
    }
  });
  
  $('a#optionDelete').click(function () {
    $('.selectOptions option:selected').remove();
    $('input.optionText').val('');
    $('input.optionValue').val('');
    $('input.optionValue').attr('style','display: block;');
    $('.errorOptionsAddUpdate').attr('style','display: none;');
    $('.errorOptionsAddUpdate').html('');
    $('.optionValueDiv').attr('style','display: none;');
    $('.optionValueDiv').html('');
    $('a#optionAdd').attr('style', 'display: block;');
    $('a#optionUpdate').attr('style', 'display: none;');
  });

})(jQuery);