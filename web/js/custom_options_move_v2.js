(function ($) {
  
  // Select options for a move to another
 
  $(".moveToRightOptions").click( function (){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsData1 option:selected').appendTo("#" + id + " .selectOptionsData2");
  });
  
  $(".moveToLeftOptions").click( function (){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsData2 option:selected').appendTo("#" + id + " .selectOptionsData1");
  });
  
  $(".moveToAllRightOptions").click( function (){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsData1 option').appendTo("#" + id + " .selectOptionsData2");
    $('#' + id + ' .selectOptionsData2 option').attr("selected", true);
  });
  
  $(".moveToAllLeftOptions").click( function (){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsData2 option').appendTo("#" + id + " .selectOptionsData1");
    $('#' + id + ' .selectOptionsData1 option').attr("selected", true);
  });
  
  $(".selectOptionsData1").bind("dblclick", function(){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsData1 option:selected').each(function(){
      var text = $(this).text();
      var value = $(this).val();
      if (text != '') {
        $("#" + id + " .selectOptionsData2").append(new Option(text, value, true, true));
        $("#" + id + " .selectOptionsData1 option:selected").remove();
      }
    });
  });
  
  $(".selectOptionsData2").bind("dblclick", function(){
    var id = $(this).parent().parent().attr('id');
    $('#' + id + ' .selectOptionsData2 option:selected').each(function(){
      var text = $(this).text();
      var value = $(this).val();
      if (text != '') {
        $("#" + id + " .selectOptionsData1").append(new Option(text, value, true, true));
        $("#" + id + " .selectOptionsData2 option:selected").remove();
      }
    });
  });

})(jQuery);