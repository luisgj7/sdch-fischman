$('#options_company').change( function() {
    companyid = $('#options_company').val();
    $.ajax({
        type: "GET",
        data: { 'companyid': companyid, 'benchmarking': 'external' },
        url: globals.basepath + "/dashboard/report/result/elementsbycompany",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                
                $('#options_pollscheduling option').remove();
                $('#options_pollscheduling').append(new Option('SELECCIONE UNA OPCIÓN', '', true, true));
                
                $('.pollsByCompany').html(object_data.polls);
                
                $('.formFilterOptions').addClass('formActiveOff');
                
                $.uniform.update();
                $('#options_poll').uniform();
                
                $('#options_poll').change( function() {
                    update_pollschedulings_by_poll(companyid, $(this).val());
                });
                
            }
            else{
                $('.pollsByCompany').html('<em>No hay encuestas para esta empresa</em>');
            }
        }
    });
});

function update_pollschedulings_by_poll(companyid, pollid) {
    $.ajax({
        type: "GET",
        data: { 'companyid': companyid, 'pollid': pollid, 'benchmarking': 'external' },
        url: globals.basepath + "/dashboard/report/result/pollschedulingperiodsbypoll",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                
                $('.pollschedulingsByPoll').html(object_data.periods);
                
                $('.formFilterOptions').addClass('formActiveOff');
                
                $.uniform.update();
                $('#options_pollscheduling').uniform();
            }
            else{
                $('#options_pollscheduling').html('<em>No hay periodos para esta encuesta</em>');
            }
        }
    });
}

$('#options_poll').change( function() {
    update_pollschedulings_by_poll(companyid, $(this).val());
});

// Active formRows by Nature

if ($('#options_company').length) {
    var text = $('select#options_company :selected').text();
    $('#companyname').val(text);
    $('#options_company').change(function () {
        var text = $('select#options_company option:selected').text();
        $('#companyname').val(text);
    });
}

if ($('#options_filters\\[nature\\]').length) {
    var option = $('select#options_filters\\[nature\\] :selected').val();
    if (option.length) {
        $('#companytype').val(nature[option]['type']);
        if (nature[option]['type'] == 'school') {
            $('.formGroupSchool').removeClass('formActiveOff');
            $('.formGroupUniversity').addClass('formActiveOff');
        }
        else if (nature[option]['type'] == 'university') {
            $('.formGroupUniversity').removeClass('formActiveOff');
            $('.formGroupSchool').addClass('formActiveOff');
        }
        else {
            $('.formGroupSchool').addClass('formActiveOff');
            $('.formGroupUniversity').addClass('formActiveOff');
        }
    }
    else {
        $('#companytype').val('');
        $('.formGroupSchool').addClass('formActiveOff');
        $('.formGroupUniversity').addClass('formActiveOff');
    }
    $('#options_filters\\[nature\\]').change(function () {
        var option = $(this).val();
        if (option.length) {
            $('#companytype').val(nature[option]['type']);
            if (nature[option]['type'] == 'school') {
                $('.formGroupSchool').removeClass('formActiveOff');
                $('.formGroupUniversity').addClass('formActiveOff');
            }
            else if (nature[option]['type'] == 'university') {
                $('.formGroupUniversity').removeClass('formActiveOff');
                $('.formGroupSchool').addClass('formActiveOff');
            }
        }
        else {
            $('#companytype').val('');
            $('.formGroupSchool').addClass('formActiveOff');
            $('.formGroupUniversity').addClass('formActiveOff');
        }
    });
}

if ($('#options_filters\\[religiousaffiliationon\\]').length) {
    var option = $('select#options_filters\\[religiousaffiliationon\\] :selected').val();
    if (option == 1) {
        $('.formReligiousAffiliation').removeClass('formActiveOff');
    }
    else {
        $('.formReligiousAffiliation').addClass('formActiveOff');
    }
    $('#options_filters\\[religiousaffiliationon\\]').change(function () {
        var option = $(this).val();
        if (option == 1) {
            $('.formReligiousAffiliation').removeClass('formActiveOff');
        }
        else {
            $('.formReligiousAffiliation').addClass('formActiveOff');
        }
    });
}

// Validation in select multiple

$('button.autoselectOptionData2').click(function () {
    $("select.selectOptionsData2 option").attr("selected", true);
    $("select.selectOptionsCopy2 option").attr("selected", true);
});

// Validation form Benchmarking Internal

$(".crudBenchmarking").validate({
    rules : {
        'options_company' : 'required',
        'options_poll' : 'required',
        'options_level' : 'required',
        'options_pollscheduling' : 'required'
    },
    messages : {
        'options_company' : {
            required : "Seleccione la empresa."
        },
        'options_poll' : {
            required : "Seleccione la encuesta."
        },
        'options_level' : {
            required : "Seleccione el nivel de agrupación."
        },
        'options_pollscheduling' : {
            required : "Seleccione el periodo."
        }
    }
});

//Validation in Save Button

$('form').submit(function () {
    var status;
    status = $(".formControl").valid(); //Validate again

    if(status==true) { 
        $('button[type$="submit"]').attr("disabled", "disabled");
    }
    
});
