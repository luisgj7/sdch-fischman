nmdcontinue = 0;
updateprogressbar();

// Select checks
$("input[name='form_deletes[check_all]']").change(function() {
    $('table.check_all tbody tr td input[type=checkbox]').each( function() {
        if ($("input[name='form_deletes[check_all]']:checked").length == 1) {
            this.checked = true;
        }
        else {
            this.checked = false;
        }
    });
    $.uniform.update();
});

// Select checks
$("input[name='form_view[pending]']").change(function() {
    $('table.check_all tbody tr td.nmd-status').each( function() {
        var check = $(this).parent('tr').children("td").children("div").children("span").children("input[type=checkbox]").prop("checked");
        if ($("input[name='form_view[pending]']:checked").length == 1) {
            if ($(this).text() == 'Pendiente') {
                if (!check) {
                    $(this).parent('tr').attr('style', 'display: table-row');
                }
            }
        }
        else {
            if ($(this).text() == 'Pendiente') {
                if (!check) {
                    $(this).parent('tr').attr('style', 'display: none');
                }
            }
        }
    });
    $.uniform.update();
});

// Select checks
$("input[name='form_view[send]']").change(function() {
    $('table.check_all tbody tr td.nmd-status').each( function() {
        var check = $(this).parent('tr').children("td").children("div").children("span").children("input[type=checkbox]").prop("checked");
        if ($("input[name='form_view[send]']:checked").length == 1) {
            if ($(this).text() == 'Enviado') {
                if (!check) {
                    $(this).parent('tr').attr('style', 'display: table-row');
                }
            }
        }
        else {
            if ($(this).text() == 'Enviado') {
                if (!check) {
                    $(this).parent('tr').attr('style', 'display: none');
                }
            }
        }
    });
    $.uniform.update();
});

$('#send-pending').click(function() {
    nmdcontinue = 1;
    sendingStateButtons();
    presendnotificationmanual(0, true, true);
    //Al terminar todo se da un mensaje con los resultados
    return false;
});

$('#nmd-stop').click(function() {
    nmdcontinue = 0;
    $(this).attr("disabled", "disabled");
    return false;
});

function sendingStateButtons() {
    $('#send-pending').attr("disabled", "disabled");
    $('#nmd-stop').removeAttr("disabled", "disabled");
    $('.loader').css("display", "block");
    $('#nmd-discard').css("display", "none");
    $('.nmd-sendbutton a').css("display", "none");
    $('#form_deletes_check_all').attr("disabled", "disabled");
    $('#form_view_check_pending').attr("disabled", "disabled");
    $('#form_view_check_send').attr("disabled", "disabled");
}

function stopStateButtons() {
    $('#send-pending').removeAttr("disabled", "disabled");
    $('#nmd-stop').attr("disabled", "disabled");
    $('.loader').css("display", "none");
    $('.nmd-sendbutton a').css("display", "block");
    $('#nmd-discard').css("display", "block");
    $('#form_deletes_check_all').attr("disabled", false);
    $('#form_view_check_pending').attr("disabled", false);
    $('#form_view_check_send').attr("disabled", false);
}

/**
 * Envoltorio para enviar notificationmanualsend.
 * Antes de enviar verifica algunas condiciones:
 * - El indice pasado no es mayor al tamaño de la colección
 * - La nmd esta con estado pending
 * - Si es recursiva, la variable nmdcontinue es 1
 */
function presendnotificationmanual(nmdindex, recursive, status) {
    if (nmdindex >= nmdcount) {
        stopStateButtons();
        return;
    }

    if (recursive & (nmdcontinue == 0) ) {
        stopStateButtons();
        return;
    }

    nmd = notificationmanualdetails[nmdindex];
    
    if (nmd.status != 'pending') {
        if(recursive) presendnotificationmanual(nmdindex + 1, true, false);
  return;
    }
    
    if ($("#form_notification_sends_" + nmd.id).is(':checked')) {
        status = 1;
    }
    else {
        status = 0;
    }
    
    sendnotificationmanual(nmdindex, recursive, status);
}

function sendnotificationmanual(nmdindex, recursive, status) {
    nmd = notificationmanualdetails[nmdindex];
    $.ajax({
      type: "POST",
      url: globals.basepath + "/admin/notificationexecution/"+ nmd.nxid +"/send/"+ nmd.id +"/"+ status,
      settings: {async: false},
      success: function(data, textStatus, jqXHR){
        var object_data = eval('('+data+')');
        if (object_data.nxid == 1) {
            
            //Actualiza la base de datos, el arreglo, la lista html y el indicador de progreso
            nmd.status = 'sent';
            nmdsent++;
            updateprogressbar();
            $('#nmd-' + nmd.id + ' .nmd-status').html('Enviado');
            $('#nmd-' + nmd.id + ' .nmd-sendbutton').html('');

            if(recursive) presendnotificationmanual(nmdindex + 1, true, false);
            else stopStateButtons();
        }
        else{
            if(recursive) presendnotificationmanual(nmdindex + 1, true, false);
            else stopStateButtons();
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(recursive) presendnotificationmanual(nmdindex + 1, true);
        else stopStateButtons();
      }
    });
}

function updateprogressbar() {
    $('.countText').html(nmdsent + ' de ' + nmdcount + ' notificaciones enviadas');

    var percentaje = 0;
    if (nmdsent > 0 && nmdcount > 0) {
        percentaje = (nmdsent / nmdcount) * 100;
    }

    $('.ui-progressbar-value').css('width', percentaje + '%');
}
