// Recover polls options and filters options by type company

$('.permission_companyselect').change( function() {
    $.ajax({
        type: "GET",
        data: { companyid : $(this).val(), userid: userid, permissionid : permissionid },
        url: globals.basepath + "/admin/permission/ajax/formelementsbycompany",
        success: function(response_data){
            if (response_data != ''){
                
                var object_data = eval('('+response_data+')');
                $('.pollsByCompany').html(object_data.polls);
                $('.pollschedulingsByPoll .selectOptionsData1 option').remove();
                $('.pollschedulingsByPoll .selectOptionsData2 option').remove();
                $('.filtersByCompany .leftBox select').html(object_data.filters);
                $('.filtersByCompany .selectOptionsData2 option').remove();
                $('.pollId').val('');
                $.uniform.update();
                $('.permission_pollselect').uniform();
                $('.pollschedulingsByPoll').uniform();
                $('.filtersByCompany').uniform();

                $('.permission_pollselect').change( function() {
                    pollschedulings_options($(this).val(), $('.permission_companyselect').val(), userid, permissionid);
                });
                $('.formBenchmarkingExternal').removeClass('formActiveOff');
                
            }
            else{
                $('.pollsByCompany').html('<em>No se encontraron encuestas Benchmarking de la Empresa</em>');
            }
       }
   });
});

// Option to change the function pollschedulings_options()

$('.permission_pollselect').change( function() {
    pollschedulings_options($(this).val(), $('.permission_companyselect').val(), userid, permissionid);
});

// Function ajax recover pollschedulings options

function pollschedulings_options(pollid, companyid, userid, permissionid) {
    $.ajax({
        type: "GET",
        data: { companyid : companyid, pollid : pollid, userid : userid, permissionid : permissionid },
        url: globals.basepath + "/admin/permission/ajax/formelementsbypoll",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollschedulingsByPoll .leftBox select').html(object_data.pollschedulings);
                $('.pollschedulingsByPoll .selectOptionsData2 option').remove();
                $('.pollId').val($('.permission_pollselect').val());
                $.uniform.update();
                $('.pollschedulingsByPoll').uniform();
                
                $('.formBenchmarkingExternal').removeClass('formActiveOff');
            }
            else{
                $('.pollschedulingsByPoll').html('<em>No hay encuestas programadas Benchmarking para esta encuesta</em>');
            }
        }
    });
}

// Select options for a move to another
 
$(".moveToRightOptions").click( function (){
    update_elements_by_pollschedulings();
});

$(".moveToLeftOptions").click( function (){
    update_elements_by_pollschedulings();
});

$(".moveToAllRightOptions").click( function (){
    update_elements_by_pollschedulings();
});

$(".moveToAllLeftOptions").click( function (){
    update_elements_by_pollschedulings();
});

$(".selectOptionsData1").bind("dblclick", function(){
    update_elements_by_pollschedulings();
});

$(".selectOptionsData2").bind("dblclick", function(){
    update_elements_by_pollschedulings();
});

function update_elements_by_pollschedulings() {
    
    var pollschedulingids = '';
    var i = 0;
    
    $('.selectOptionsData2 option').each(function(){
        if ($(this).length) {
            if (i == 0) {
                pollschedulingids = $(this).val();
            }
            else {
                pollschedulingids += ',' + $(this).val();
            }
            i++;
        }
    });
    
    $.ajax({
        type: "GET",
        data: { 'pollschedulingids': pollschedulingids },
        url: globals.basepath + "/admin/permission/ajax/formelementsbypollschedulings",
        error: function(jqXHR, textStatus, errorThrown){
            console.debug(jqXHR);
        },
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                if (object_data.value == 1) {
                    $('.formBenchmarkingExternal').removeClass('formActiveOff');
                    if ($('.permission_beselect').val() == 1) {
                        $('.formFilters').removeClass('formActiveOff');
                    }
                }
                else {
                    $('.formBenchmarkingExternal').addClass('formActiveOff');
                    $('.formFilters').addClass('formActiveOff');
                }
            }
        }
    });
}

// recover poll_id of permission_pollselect in pollId

$('.pollId').val($('.permission_pollselect').val());

// conditional visibility field Benchmarking External

update_elements_by_pollschedulings();

// If benchmarking external is "SI" => 1 then remove class formActiveOff in formFilters

if ($('select.permission_beselect').length) {
    var default_option = $('.permission_beselect :selected').val();
    if (default_option == '1') {
        $('.formFilters').removeClass('formActiveOff');
    }
    else {
        $('.formFilters').addClass('formActiveOff');
    }
    $('select.permission_beselect').change(function () {
        var option = $(this).val();
        if (option == '1') {
            $('.formFilters').removeClass('formActiveOff');
        }
        else {
            $('.formFilters').addClass('formActiveOff');
        }
    });
}
  
// Validation in select multiple for Permission

$('button.autoselectOptionData2').click(function () {
  $("select.selectOptionsData2 option").attr("selected", true);
});

// Validation form Permission

$(".crudPermission").validate({
    rules : {
        'fishman_authbundle_permissiontype[company_id]' : 'required',
        'permission_polls': 'required',
        'pollschedulings_options[data2][]' : 'options_select_multiple',
        'fishman_authbundle_permissiontype[benchmarking_internal]' : 'required',
        'fishman_authbundle_permissiontype[benchmarking_external]' : 'required',
        'fishman_authbundle_permissiontype[initdate]' : {
            required : true,
            regular_expressions : a_formats.dates
        },
        'fishman_authbundle_permissiontype[enddate]' : {
            required : true,
            regular_expressions : a_formats.dates
        },
        'fishman_authbundle_permissiontype[status]' : 'required',
    },
    messages : {
        'fishman_authbundle_permissiontype[company_id]' : {
            required : "Seleccione la empresa."
        },
        'permission_polls' : {
            required : "Seleccione la encuesta."
        },
        'pollschedulings_options[data2][]' : {
            options_select_multiple : "Agregue uno o más periodos."
        },
        'fishman_authbundle_permissiontype[benchmarking_internal]': {
            required : "Seleccione una opción."
        },
        'fishman_authbundle_permissiontype[benchmarking_external]': {
            required : "Seleccione una opción."
        },
        'fishman_authbundle_permissiontype[initdate]' : {
            required : 'Escoja la fecha de inicio.',
            regular_expressions : 'Sólo se permite formato de fecha.'
        },
        'fishman_authbundle_permissiontype[enddate]' : {
            required : 'Escoja la fecha de fin.',
            regular_expressions : 'Sólo se permite formato de fecha.'
        },
        'fishman_authbundle_permissiontype[status]' : {
            required : "Seleccione el estado."
        }
    }
});
    
//Validation in Save Button

$('form').submit(function () {
    var status;
    status = $(".formControl").valid(); //Validate again

    if(status==true) { 
        $('button[type$="submit"]').attr("disabled", "disabled");
    }
    
});