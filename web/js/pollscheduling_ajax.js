$('.allCategories').change( function() {
    $.ajax({
        type: "GET",
        data: "category_id=" + $(this).val(),
        url: globals.basepath + "/admin/poll/choicebycategory",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                tinymce.activeEditor.setContent('');
                $('.pollCategory').html(object_data.combo).show();
                $('.pollType').val('');
                $('.pollDuration').val('');
                $('.pollPeriod').val('');
                $('.pollAlignment').val('');
                $('.pollId').val('');
                $.uniform.update();
                $('.polls').uniform();
            }
            else{
                $('.pollCategory').html('<em>No hay encuestas para esta categoría</em>');
            }
            
            $('.polls').change( function() {
                tinymce.get('fishman_pollbundle_pollschedulingtype_description').focus();
                update_poll_information($(this).val());
            });
            
        }
    });
});

$('.polls').change( function() {
    tinymce.get('fishman_pollbundle_pollschedulingtype_description').focus();
    update_poll_information($(this).val());
});

function update_poll_information(poll_id){
    $.ajax({
        type: "GET",
        data: "poll_id=" + poll_id,
        url: globals.basepath + "/admin/poll/json",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                tinymce.execCommand('mceSetContent',false , object_data.description);
                $('.pollDescription').val(object_data.description);
                $('.pollDuration').val(object_data.duration);
                $('.pollPeriod').val(object_data.period);
                $('.pollAlignment').val(object_data.alignment);
                $('.pollId').val($('.polls').val());
                
                if (object_data.type != '') {
                    var option = object_data.type;
                    var pa_option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[pollscheduling_anonymous]"]:checked').val();
                    if (option == 'self_evaluation' || pa_option == 1) {
                        $('.formMultipleEvaluation').addClass('formActiveOff');
                    }
                    else {
                        $('.formMultipleEvaluation').removeClass('formActiveOff');
                    }
                }
                
                $.uniform.update();
                
            }
            else{
                $('.pollCategory').html('<em>No hay encuestas para esta categoría</em>');
            }
        }
    });
}

$('.pollId').val($('.polls').val());

$('.allCompanies').change( function() {
    $.ajax({
        type: "GET",
        data: "company_id=" + $(this).val() + "&pollscheduling_id=" + pollschedulingid + 
              "&entity_type=" + entitytype + "&entity_id=" + entityid,
        url: globals.basepath + "/admin/pollscheduling/choicebycompany",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.allPollschedulingRelations').html(object_data.combo).show();
                $('.pollschedulingRelationId').val($('.pollschedulingRelations').val());
                $.uniform.update();
                $('.pollschedulingRelations').uniform();
            }
            else{
                $('.pollschedulingRelations').html('<em>No hay encuestas programadas para esta empresa</em>');
            }

            $('.pollschedulingRelations').change( function() {
                $('.pollschedulingRelationId').val($(this).val());
            });

        }
    });
});

$('.pollschedulingRelationId').val($('.pollschedulingRelations').val());

$('.allBenchmarkings').change( function() {
    update_benchmarking_information($(this).val());
    $('.benchmarkingId').val($(this).val());
});

function update_benchmarking_information(benchmarking_id){
    $.ajax({
        type: "GET",
        data: "benchmarking_id=" + benchmarking_id,
        url: globals.basepath + "/admin/benchmarking/json",
        success: function(response_data){
            if (response_data != ''){
                var object_data = eval('('+response_data+')');
                $('.pollPollschedulingPeriod').val(object_data.period);
            }
            else{
                $('.pollPollschedulingPeriod').html('<em>No hay periodo para este Benchmarking</em>');
            }
        }
    });
}

$('.pollschedulingRelations').change( function() {
    $('.pollschedulingRelationId').val($(this).val());
});
  
/*
 * Conditional fields
 */

if ($('.formCrudPollscheduling').length) {
    $('.pollDescription').focus();
}

// Select values
  
if ($('select.polls').length) {
    $('.pollId').val($('select.polls :selected').val());
}

if ($('select.allBenchmarkings').length) {
    $('.benchmarkingId').val($('select.allBenchmarkings :selected').val());
}

if ($('select.pollschedulingRelations').length) {
    $('.pollschedulingrelationId').val($('select.pollschedulingRelations :selected').val());
}

// Selected Custom Header Title

if ($('select.customHeaderTitle').length) {
    var option = $('select.customHeaderTitle :selected').val();
    if (option != '' && option != 0) {
        $('.formCustomHeaderTitle').removeClass('formActiveOff');
    }
    else {
        $('.formCustomHeaderTitle').addClass('formActiveOff');
    }
    $('select.customHeaderTitle').change(function () {
        var option = $('select.customHeaderTitle :selected').val();
        if (option != '' && option != 0) {
            $('.formCustomHeaderTitle').removeClass('formActiveOff');
        }
        else {
            $('.formCustomHeaderTitle').addClass('formActiveOff');
        }
    });
}

// Selected Polltype

if ($('select.pollType').length) {
    var option = $('.pollType :selected').val();
    var pa_option = 0;
    if ($('.pollAnonymous').length) {
        pa_option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[pollscheduling_anonymous]"]:checked').val();
    }
    var mp_option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[multiple_evaluation]"]:checked').val();
    if (option == 'self_evaluation') {
        $('.formCounselorReport').removeClass('formActiveOff');
    }
    else {
        $('.formCounselorReport').addClass('formActiveOff');
    }
    if (option == 'self_evaluation' || option == 'not_evaluateds') {
        $('.formMultipleEvaluation').addClass('formActiveOff');
        $('.formAccessViewNumber').addClass('formActiveOff');
        $('.formCounselorReport').removeClass('formActiveOff');
    }
    else {
        $('.formMultipleEvaluation').removeClass('formActiveOff');
        $('.formAccessViewNumber').removeClass('formActiveOff');
        $('.formPollschedulingRelations').removeClass('formActiveOff');
    }
    if (option == 'not_evaluateds') {
        $('.formPollschedulingRelations').addClass('formActiveOff');
    }
    if (pa_option == 1) {
        $('.formMultipleEvaluation').addClass('formActiveOff');
        $('.formAccessViewNumber').addClass('formActiveOff');
        $('.formPollschedulingRelations').addClass('formActiveOff');
    }
    if (mp_option == 1) {
        $('.formAccessViewNumber').addClass('formActiveOff');
        $('.formPollschedulingRelations').addClass('formActiveOff');
    }
    $('select.pollType').change(function () {
        var option = $(this).val();
        var mp_option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[multiple_evaluation]"]:checked').val();
        if (option == 'self_evaluation') {
            $('.formCounselorReport').removeClass('formActiveOff');
        }
        else {
            $('.formCounselorReport').addClass('formActiveOff');
        }
        if (option == 'self_evaluation' || option == 'not_evaluateds') {
            $('.formMultipleEvaluation').addClass('formActiveOff');
            $('.formAccessViewNumber').addClass('formActiveOff');
        }
        else {
            $('.formMultipleEvaluation').removeClass('formActiveOff');
            $('.formAccessViewNumber').removeClass('formActiveOff');
            $('.formPollschedulingRelations').removeClass('formActiveOff');
            $.uniform.update();
        }
        if (option == 'not_evaluateds') {
            $('.formPollschedulingRelations').addClass('formActiveOff');
        }
        if (pa_option == 1) {
            $('.formCounselorReport').addClass('formActiveOff');
            $('.formMultipleEvaluation').addClass('formActiveOff');
            $('.formAccessViewNumber').addClass('formActiveOff');
            $('.formPollschedulingRelations').addClass('formActiveOff');
        }
        if (mp_option == 1) {
            $('.formAccessViewNumber').addClass('formActiveOff');
            $('.formPollschedulingRelations').addClass('formActiveOff');
        }
    });
}

// Selected PollmultipleEvaluation

if ($('.pollMultipleEvaluation').length) {
    var option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[multiple_evaluation]"]:checked').val();
    var pt_option = $('.pollType :selected').val();
    var pa_option = 0;
    if ($('.pollAnonymous').length) {
        pa_option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[pollscheduling_anonymous]"]:checked').val();
    }
    if (option == 1) {
        $('.formStatusBar').addClass('formActiveOff');
        $('.formCounselorReport').addClass('formActiveOff');
        $('.formPollschedulingRelations').addClass('formActiveOff');
        $('.formAccessViewNumber').addClass('formActiveOff');
        $('.formPollschedulingAlignment').addClass('formActiveOff');
    }
    else {
        $('.formStatusBar').removeClass('formActiveOff');
        $('.formCounselorReport').removeClass('formActiveOff');
        $('.formPollschedulingRelations').removeClass('formActiveOff');
        if (pt_option != 'self_evaluation' || pt_option != 'not_evaluateds') {
            $('.formAccessViewNumber').removeClass('formActiveOff');
        }
        $('.formPollschedulingAlignment').removeClass('formActiveOff');
    }
    if (pa_option == 1) {
        $('.formCounselorReport').addClass('formActiveOff');
        $('.formPollschedulingRelations').addClass('formActiveOff');
        $('.formAccessViewNumber').addClass('formActiveOff');
        $('.formPollschedulingAlignment').addClass('formActiveOff');
    }
    if (pt_option == 'self_evaluation' || pt_option == 'not_evaluateds') {
        $('.formMultipleEvaluation').addClass('formActiveOff');
        $('.formAccessViewNumber').addClass('formActiveOff');
        $('.formPollschedulingAlignment').removeClass('formActiveOff');
    }
    if (pt_option == 'not_evaluateds') {
        $('.formPollschedulingRelations').addClass('formActiveOff');
    }
    $('input:radio[name="fishman_pollbundle_pollschedulingtype[multiple_evaluation]"]').click(function(){
        var option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[multiple_evaluation]"]:checked').val();
        if (option == 0) {
            $('.formStatusBar').removeClass('formActiveOff');
            $('.formCounselorReport').removeClass('formActiveOff');
            $('.formPollschedulingRelations').removeClass('formActiveOff');
            if (pt_option == 'self_evaluation' && pt_option == 'not_evaluateds') {
                $('.formAccessViewNumber').removeClass('formActiveOff');
            }
            $('.formPollschedulingAlignment').removeClass('formActiveOff');
        }
        else {
            $('.formStatusBar').addClass('formActiveOff');
            $('.formCounselorReport').addClass('formActiveOff');
            $('.formPollschedulingRelations').addClass('formActiveOff');
            $('.formAccessViewNumber').addClass('formActiveOff');
            $('.formPollschedulingAlignment').addClass('formActiveOff');
            $(".pollschedulingRelations option[value='']").attr("selected",true);
            $(".formAccessViewNumber input").val('');
            $.uniform.update();
        }
    });
}

// Selected PollAnonymous

if ($('.pollAnonymous').length) {
    var option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[pollscheduling_anonymous]"]:checked').val();
    var me_option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[multiple_evaluation]"]:checked').val();
    var pt_option = $('.pollType :selected').val();
    if (option == 0 || option == null) {
        $('.formCompany').removeClass('formActiveOff');
        $('.formTitleCustom').removeClass('formActiveOff');
        $('.formLogoCompanyCustom').removeClass('formActiveOff');
        $('.formEvaluationType').removeClass('formActiveOff');
        if (pt_option == 'self_evaluation') {
            $('.formMultipleEvaluation').addClass('formActiveOff');
        }
        $('.formBenchmarking').removeClass('formActiveOff');
        if (me_option == 0) {
            $('.formCounselorReport').removeClass('formActiveOff');
        }
        $('.formPollschedulingRelations').removeClass('formActiveOff');
        if (pt_option != 'self_evaluation' && pt_option != 'not_evaluateds') {
            $('.formAccessViewNumber').removeClass('formActiveOff');
        }
    }
    else {
        $('.formCompany').addClass('formActiveOff');
        $('.formTitleCustom').addClass('formActiveOff');
        $('.formLogoCompanyCustom').addClass('formActiveOff');
        $('.formEvaluationType').addClass('formActiveOff');
        $('.formMultipleEvaluation').addClass('formActiveOff');
        $('.formBenchmarking').addClass('formActiveOff');
        $('.formCounselorReport').addClass('formActiveOff');
        $('.formPollschedulingRelations').addClass('formActiveOff');
        $('.formAccessViewNumber').addClass('formActiveOff');
        $('.formCounselorReport').addClass('formActiveOff');
    }
    if (me_option == 1 || pt_option == 'not_evaluateds') {
        $('.formPollschedulingRelations').addClass('formActiveOff');
        $('.formAccessViewNumber').addClass('formActiveOff');
    }
    else {
        $('.formPollschedulingRelations').removeClass('formActiveOff');
        if (pt_option != 'self_evaluation') {
            $('.formAccessViewNumber').removeClass('formActiveOff');
        }
    }
    $('input:radio[name="fishman_pollbundle_pollschedulingtype[pollscheduling_anonymous]"]').click(function(){
        var option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[pollscheduling_anonymous]"]:checked').val();
        var me_option = $('input:radio[name="fishman_pollbundle_pollschedulingtype[multiple_evaluation]"]:checked').val();
        var pt_option = $('.pollType :selected').val();
        if (option == 0) {
            $('.formCompany').removeClass('formActiveOff');
            $('.formTitleCustom').removeClass('formActiveOff');
            $('.formLogoCompanyCustom').removeClass('formActiveOff');
            $('.formEvaluationType').removeClass('formActiveOff');
            if (pt_option == 'self_evaluation') {
                $('.formMultipleEvaluation').addClass('formActiveOff');
            }
            $('.formBenchmarking').removeClass('formActiveOff');
            $('.formCounselorReport').removeClass('formActiveOff');
            $('.formCounselorReport').removeClass('formActiveOff');
            if (me_option == 0 || pt_option != 'not_evaluateds') {
                $('.formPollschedulingRelations').removeClass('formActiveOff');
                if (pt_option != 'self_evaluation') {
                    $('.formAccessViewNumber').removeClass('formActiveOff');
                }
            }
        }
        else {
            $('.formCompany').addClass('formActiveOff');
            $('.formTitleCustom').addClass('formActiveOff');
            $('.formLogoCompanyCustom').addClass('formActiveOff');
            $('.formEvaluationType').addClass('formActiveOff');
            $('.formMultipleEvaluation').addClass('formActiveOff');
            $('.formBenchmarking').addClass('formActiveOff');
            $('.formCounselorReport').addClass('formActiveOff');
            $('.formPollschedulingRelations').addClass('formActiveOff');
            $('.formAccessViewNumber').addClass('formActiveOff');
            $('.formCounselorReport').addClass('formActiveOff');
        }
    });
}

// Selected Benchmarking

if ($('select.allBenchmarkings').length) {
    var option = $('select.allBenchmarkings :selected').val();
    if (option != '') {
        $('.pollPollschedulingPeriod').attr('readonly', true);
    }
    else {
        $('.pollPollschedulingPeriod').attr('readonly', false);
    }
    $('select.allBenchmarkings').change(function () {
        var option = $('select.allBenchmarkings :selected').val();
        if (option != '') {
            $('.pollPollschedulingPeriod').attr('readonly', true);
        }
        else {
            $('.pollPollschedulingPeriod').attr('readonly', false);
        }
    });
}

// Function for update information in custom header of pollscheduling 

$('#titleCompanyCustom').change(function () {
    var option = $(this).val();
    $('#fishman_pollbundle_pollschedulingtype_custom_header_title').val(option);
});
$('#logoCompanyCustom').change(function () {
    var option = $(this).val();
    $('#fishman_pollbundle_pollschedulingtype_custom_header_logo').val(option);
});
