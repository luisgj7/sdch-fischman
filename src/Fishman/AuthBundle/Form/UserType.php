<?php

namespace Fishman\AuthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext as CoreSecurityContext;

class UserType extends AbstractType
{
    private $rol;

    public function __construct($rol)
    {
        $this->rol = $rol;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('username')
           // ->add('password', 'password')
            ->add('surname')
            ->add('lastname', 'text', array(
                'required' => false
            ))
            ->add('names')
            ->add('identity', 'choice', array(
                'choices' => array(
                    'dni' => 'DNI',
                    'license' => 'Carnet de Extranjería',  
                    'passport' => 'Pasaporte'
                ), 
                'empty_value' => 'Choose an option', 
             ))
            ->add('numberidentity')
            ->add('birthday', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy', 
                'required' => FALSE
            ))
            ->add('birthplace', 'text', array( 
                'required' => FALSE
            ))
            ->add('sex', 'choice', array(
                'choices' => array(
                    'm' => 'Masculino', 
                    'f' => 'Femenino'
                ), 
                'empty_value' => 'Choose an option', 
            ))
            ->add('marital_status', 'choice', array(
                'choices' => array(
                    'single' => 'Soltero',
                    'married' => 'Casado',
                    'widower' => 'Viudo',
                    'divorced' => 'Divorciado',
                    'cohabitant' => 'Conviviente'
                ), 
                'empty_value' => 'Choose an option', 
                'required' => FALSE
            ))
            ->add('email')
            ->add('phone', 'text', array(
                'max_length' => 32,
                'required' => FALSE
            ))
            ->add('enabled', 'choice', array(
                'choices'   => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option' 
            ))
            ;

        //TODO: We need to research if it's possible to jump the security restriction
        // even the options or all field (sending options or input no presents in form definition)
        if ($this->rol == 'ROLE_SUPER_ADMIN') {
            $roleChoices['ROLE_SUPER_ADMIN'] = 'Super Administrador';
            $roleChoices['ROLE_ADMIN'] = 'Administrador';
            $roleChoices['ROLE_TEACHER'] = 'Profesor';
        }
        if ($this->rol == 'ROLE_ADMIN') {
            $roleChoices['ROLE_TEACHER'] = 'Profesor';
        }
        if (count($roleChoices) > 0) {
            $builder
                ->add('roles', 'choice', array(
                    'choices' => $roleChoices,
                    'multiple' => true,
                    'expanded' => true,
                ));

        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\AuthBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'fishman_authbundle_usertype';
    }
}
