<?php

namespace Fishman\AuthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\AuthBundle\Entity\User;
use Fishman\AuthBundle\Entity\Workinginformation;

use Fishman\EntityBundle\Entity\Company;

class WorkinginformationType extends AbstractType
{
    private $workinginformation;
    private $type;
    private $doctrine;

    public function __construct(Workinginformation $workinginformation, $type, Registry $doctrine)
    {
        $this->workinginformation = $workinginformation;
        $this->type = $type;
        $this->doctrine = $doctrine;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defVal = '';
        $companyType = '';
        $headquarterid = '';
        
        if(is_object($this->workinginformation->getCompany())){
            $defVal = $this->workinginformation->getCompany()->getId();
        }
        
        if (is_object($this->workinginformation->getHeadquarter())) {
            $headquarterid = $this->workinginformation->getHeadquarter()->getId();
        }
        
        if($this->type) {
            $companyType = $this->type;
            switch ($companyType) {
              case 'workinginformation':
                $type = 'working';
                break;
              case 'schoolinformation':
                $type = 'school';
                break;
              case 'universityinformation':
                $type = 'university';
                break;
            }
        }
        
        $companies_options = array();
        $repository = $this->doctrine->getRepository('FishmanEntityBundle:Company');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->where('c.status = 1 
                     AND c.id <> 2')
            ->orderBy('c.name', 'ASC');
        
        if ($type != 'working') {
            $queryBuilder
                ->andWhere('c.company_type = :type')
                ->setParameter('type', $type);
        }
        
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $companies_options[$r['id']] = $r['name'];
        }
        
        switch ($this->type) {
            
            case 'workinginformation':
                
                $companyorganizationalunitid = '';
                $companychargeid = '';
                
                if (is_object($this->workinginformation->getCompanyorganizationalunit())) {
                    $companyorganizationalunitid = $this->workinginformation->getCompanyorganizationalunit()->getId();
                }
                
                if (is_object($this->workinginformation->getCompanycharge())) {
                    $companychargeid = $this->workinginformation->getCompanycharge()->getId();
                }
                
                $builder
                    ->add('companyid', 'choice', array(
                        'choices' => $companies_options,  
                        'empty_value' => 'Choose an option', 
                        'mapped' => FALSE, 
                        'required' => false,
                        'data' => $defVal 
                    ))
                    ->add('code')
                    ->add('datein', 'date', array(
                        'input'  => 'datetime',
                        'widget' => 'single_text',
                        'format' => 'dd-MM-yyyy',
                        'required' => false
                    ))
                    ->add('dateout', 'date', array(
                        'input'  => 'datetime',
                        'widget' => 'single_text',
                        'format' => 'dd-MM-yyyy',
                        'required' => false
                    ))
                    ->add('email')
                    ->add('boss', 'hidden')
                    ->add('unit_type', 'choice', array(
                        'choices' => array(
                            'administrative' => 'Administrativa', 
                            'academic' => 'Academica'
                        ), 
                        'empty_value' => 'Choose an option',
                        'required' => FALSE
                    ))
                    ->add('companyorganizationalunit_id', 'hidden', array('data' => $companyorganizationalunitid,
                        'mapped' => false))
                    ->add('companycharge_id', 'hidden', array('data' => $companychargeid,
                        'mapped' => false))
                    ->add('headquarter_id', 'hidden', array('data' => $headquarterid,
                        'mapped' => false))
                    ->add('status', 'choice', array(
                        'choices' => array(
                            1 => 'Activo', 
                            0 => 'Desactivo'
                        ),
                        'empty_value' => 'choose an option'
                    ));
                break;
                
            case 'schoolinformation':
                
                $builder
                    ->add('companyid', 'choice', array(
                        'choices' => $companies_options,  
                        'empty_value' => 'Choose an option', 
                        'mapped' => FALSE, 
                        'required' => false,
                        'data' => $defVal 
                    ))
                    ->add('code')
                    ->add('email_school', 'text', array(
                        'required' => false, 
                        'mapped' => false
                    ))
                    ->add('grade', 'choice', array(
                        'choices' => array(
                            '1p' => '1ro de primaria', 
                            '2p' => '2do de primaria', 
                            '3p' => '3ro de primaria', 
                            '4p' => '4to de primaria', 
                            '5p' => '5to de primaria', 
                            '6p' => '6to de primaria', 
                            '1s' => '1ro de secundaria', 
                            '2s' => '2do de secundaria', 
                            '3s' => '3ro de secundaria', 
                            '4s' => '4to de secundaria', 
                            '5s' => '5to de secundaria'
                        ),
                        'empty_value' => 'Choose an option'
                    )) 
                    ->add('study_year')
                    ->add('headquarter_id', 'hidden', array('data' => $headquarterid,
                        'mapped' => false))
                    ->add('status', 'choice', array(
                        'choices' => array(
                            1 => 'Activo', 
                            0 => 'Desactivo'
                        ),
                        'empty_value' => 'choose an option'
                    ));
                break;
            
            case 'universityinformation':
                
                $companyfacultyid = '';
                $companycareerid = '';
                
                if (is_object($this->workinginformation->getCompanyfaculty())) {
                    $companyfacultyid = $this->workinginformation->getCompanyfaculty()->getId();
                }
                
                if (is_object($this->workinginformation->getCompanycareer())) {
                    $companycareerid = $this->workinginformation->getCompanycareer()->getId();
                }
                
                $builder
                    ->add('companyid', 'choice', array(
                        'choices' => $companies_options,  
                        'empty_value' => 'Choose an option', 
                        'mapped' => FALSE, 
                        'required' => false,
                        'data' => $defVal 
                    ))
                    ->add('email')
                    ->add('companyfaculty_id', 'hidden', array('data' => $companyfacultyid,
                        'mapped' => false))
                    ->add('companycareer_id', 'hidden', array('data' => $companycareerid,
                        'mapped' => false))
                    ->add('code')
                    ->add('academic_year')
                    ->add('headquarter_id', 'hidden', array('data' => $headquarterid,
                        'mapped' => false))
                    ->add('status', 'choice', array(
                        'choices' => array(
                            1 => 'Activo', 
                            0 => 'Desactivo'
                        ),
                        'empty_value' => 'choose an option'
                    ));
                break;
                
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\AuthBundle\Entity\Workinginformation'
        ));
    }

    public function getName()
    {
        return 'fishman_authbundle_workinginformationtype';
    }
}
