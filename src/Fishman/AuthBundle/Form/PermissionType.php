<?php

namespace Fishman\AuthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\AuthBundle\Entity\Permission;

class PermissionType extends AbstractType
{
    private $permission;
    private $doctrine;

    public function __construct(Permission $permission, Registry $doctrine)
    {
        $this->permission = $permission;
        $this->doctrine = $doctrine;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        // Recover Pollscheduling where permissions assigned to the user for the company
        $pscIds = Permission::getPollschedulingPermissionAssignedsOfPermissionId(
            $this->doctrine,
            $this->permission->getUser()->getId(),
            $this->permission
        );
        
        // Recover -companies
        $companies_options = array();
        $repository = $this->doctrine->getRepository('FishmanEntityBundle:Company');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->innerJoin('FishmanPollBundle:Pollscheduling', 'psc', 'WITH', 'c.id = psc.company_id')
            ->where('c.status = 1  
                    AND c.id <> 2')
            ->orderBy('c.name', 'ASC')
            ->groupBy('c.id');
        /*
        $queryBuilder
            ->andWhere('psc.benchmarking <> :benchmarking')
            ->setParameter('benchmarking', '');
        */
        if ($pscIds != '') {
            $queryBuilder
                ->andWhere('psc.id NOT IN(' . $pscIds . ')');
        }
        
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $companies_options[$r['id']] = $r['name'];
        }
        
        $pollid = '';
        
        if ($this->permission->getPollId() != '') {
            $pollid = $this->permission->getPollId();
        }
        
        $builder
            ->add('company_id', 'choice', array(
                'choices' => $companies_options,  
                'empty_value' => 'Choose an option'
            ))
            ->add('poll_id', 'hidden', array(
                'data' => $pollid
            ))
            ->add('benchmarking_internal', 'choice', array(
                'choices' => array(
                    1 => 'Si', 
                    0 => 'No'
                ),
                'empty_value' => 'Choose an option'
            ))
            ->add('benchmarking_external', 'choice', array(
                'choices' => array(
                    1 => 'Si', 
                    0 => 'No'
                ),
                'empty_value' => 'Choose an option'
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\AuthBundle\Entity\Permission'
        ));
    }

    public function getName()
    {
        return 'fishman_authbundle_permissiontype';
    }
}
