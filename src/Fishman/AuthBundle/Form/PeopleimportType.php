<?php

namespace Fishman\AuthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PeopleimportType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      /*
        TODO : Falta validación del campo File con los siguientes propiedades

          'maxsize' => '2M',
          'mimeTypes' => array('application/xsl'),
          'mimeTypesMessage' => 'Porfavor subir un archivo XSL valido' 

       */

        $builder
          ->add('file', 'file', array( 
              'required' => TRUE 
            ))
          ->add('type', 'choice', array(
              'choices' => array(
                  'working' => 'Laboral', 
                  'school' => 'Colegio', 
                  'university' => 'Universidad'
              ),
              'empty_value' => 'Choose an option', 
              'required' => TRUE
          )); 
    }

    public function getName()
    {
        return 'fishman_authbundle_peopleimporttype';
    }
}
