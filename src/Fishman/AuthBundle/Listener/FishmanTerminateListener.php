<?php

namespace Fishman\AuthBundle\Listener;

use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;

class FishmanTerminateListener
{
    /**
     * In certain routes, we need to finish session
     */
    public function onKernelTerminate(PostResponseEvent $event) {
        $request = $event->getRequest();

        if($request->get('_route') == 'user_message'){
            $session = $request->getSession();
            $session->clear();
            $session->save();
        }
    }
}
