<?php

namespace Fishman\AuthBundle\Listener;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Doctrine\Bundle\DoctrineBundle\Registry;

use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\WorkshopBundle\Entity\Workshopapplication;
use Fishman\PollBundle\Entity\Pollapplication;

class FishmanAccessListener
{
    protected $securityContext;
    protected $router;
    protected $doctrine;

    public function __construct(SecurityContextInterface $securityContext, Router $router, Registry $doctrine)
    {
        $this->securityContext = $securityContext;
        $this->router = $router;
        $this->doctrine = $doctrine;
    }

    /**
     * The application intercept the Kernel Request event in order to 
     * force user to select wich rol use, wich working information and
     * default workshop, when he have more than one
     */
    public function onKernelRequest(GetResponseEvent $event) {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        /*
        $request = $event->getRequest();
        $session = $request->getSession();
        if(($request->get('_route') == 'fos_user_security_login') && $session->has('loginmessage')){
            $session->getFlashBag()->add('alert', 'Su usuario no tiene ningún perfil activo');
            return;
        }
        */

        $request = $event->getRequest();

        //If user is not current logged in don't need other access control
        //TODO: Maybe it's not necesary because the security config intercept the restriction early
        if(!$this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')
           || $request->get('_route') == 'user_message') {
            return;
        }

        //TODO: Verificar si es que al enviar login_check, se tiene que hacer return sin comprobaciones
        //      para permitir el login sin problemas
   
        $session = $request->getSession();
        $user = $this->securityContext->getToken()->getUser();
        $roles = $user->getRoles();
        $workinginformations = null;
        $workshopapplications = null;

        //ROL SETTING
        //The user is trying to choose the rol
        if($request->get('_route') == 'fishman_front_end_chooserol'){
            if(count($roles) == 1){
                $this->setCurrentRol($session, $roles[0]);
            }
            else{
                //If send parameter, set it
                if(isset($_GET['rol'])){
                    if(!$this->setCurrentRol($session, $_GET['rol'])){
                        return;
                    }
                    else if($session->get('currentrol') != 'ROLE_USER'){
                        $event->setResponse(
                                new RedirectResponse($this->getRouter()->generate('fishman_front_end_homepage')));
                        return;
                    }
                }
                else{
                    //he can go to URL
                    return;            
                }
            }
        }
        
        //If user have more than one role, he must specified one to use as current rol
        //Check if user have current rol
        if(!$session->has('currentrol')){
            //If user has only one role, select this as current rol, else redirect to select rol
            //Only is possible if the user have more than one rol
            if(count($roles) == 1){
                $this->setCurrentRol($session, $roles[0]);

                //When set currentrol session variable, if rol is diffent than ROLE_USER redirect somewhere else
                if($session->get('currentrol') != 'ROLE_USER'){
                    $event->setResponse(new RedirectResponse($this->getRouter()->generate('fishman_front_end_homepage')));
                    return;
                }
            }
            else{
                //Redirect to choose the rol
                $event->setResponse(new RedirectResponse($this->getRouter()->generate('fishman_front_end_chooserol')));
                return;
            }
        }

        //WORKING_INFORMATION SETTING
        //The user is trying to choose the workinginformation
        if($request->get('_route') == 'fishman_front_end_chooseworkinginformation'){
            $workinginformations = Workinginformation::getListActiveWorkinginformations($this->getDoctrine(), $user->getId());
            if (count($workinginformations) == 1) { //If only have one, set it
                $session->set('workinginformation', $workinginformations[0]);
                $event->setResponse(new RedirectResponse($this->getRouter()->generate('fishman_front_end_homepage')));
                return;
            }
            else{
                if(isset($_GET['wi'])){  //If send parameter set it
                    $workinginformation = Workinginformation::getCurrentWorkinginformation($this->getDoctrine(), $_GET['wi']);

                    // Verify if the Workinginformation there
                    if (!empty($workinginformation)) {
                        $session->set('workinginformation', $workinginformation[0]);
                        $event->setResponse(new RedirectResponse($this->getRouter()->generate('fishman_front_end_homepage')));
                        return;
                    }
                    else{
                        //Redirect to message and logout
                        $event->setResponse(new RedirectResponse($this->getRouter()
                            ->generate('user_message',  array('id' => '1'))));
                        return;
                    }
                }
                else{
                   //he can go to the URL
                   return;
                }
            }
        }

        //Check if the user not has working information set
        if(!$session->has('workinginformation')){
            if($workinginformations == null){
                $workinginformations = Workinginformation::getListActiveWorkinginformations(
                                           $this->getDoctrine(), $user->getId());
            }

            //TODO: If the person don't have active workinginformation: set message and finish session.
            if (count($workinginformations) == 0){
                $event->setResponse(new RedirectResponse($this->getRouter()->generate('user_message',  array('id' => '1'))));
                return;

                //$event->setResponse(new RedirectResponse($this->getRouter()
                //->generate('fos_user_security_logout')));
                //TODO: Set a message
                //TODO: Redirect to some URL

                /*
                $session->getFlashBag()->add('NO TIENE INFORMACIÓN LABORAL ACTIVA',
                        'Su usuario no tiene ninguna información laboral activa. Consulte con su administrador');
                
                $event->setResponse(new RedirectResponse($this->getRouter()
                    ->generate('fos_user_security_logout')));
                
                $session->setFlash('NO TIENE INFORMACIÓN LABORAL ACTIVA',
                                   'Su usuario no tiene ninguna información laboral activa. Consulte con su administrador');
                */
                return;                
            }
            else if (count($workinginformations) == 1){ //If only have one, set it as session variable
                $session->set('workinginformation', $workinginformations[0]);
                $rol = $session->get('currentrol');
                if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                    $event->setResponse(new RedirectResponse($this->getRouter()
                        ->generate('fishman_front_end_homepage')));
                    return;
                }
                else {
                    $event->setResponse(new RedirectResponse($this->getRouter()
                        ->generate('fishman_front_end_workshop')));
                    return;
                }
            }
            else{
                //Else redirect to choose workinginformation
                $event->setResponse(new RedirectResponse($this->getRouter()
                    ->generate('fishman_front_end_chooseworkinginformation')));
                return;
            }
        }

        // Ask if is the user have ROLE_ADMIN, ROLE_SUPER_ADMIN or ROLE_TEACHER maybe it's not
        // necesary to select Workinginformation neither Workshopapplication
        if ($session->get('currentrol') != 'ROLE_USER') {
            return;
        }
        
        if ($request->get('_route') == 'fishman_front_end_workshops') {
            return;
        }
        
        //TODO: La logica completa debe considerar
            // Las encuestas programadas activas
            // Los talleres programados activos
        // Si tiene solo una encuesta, y no tiene taller activo muestra el dashboard de la encuesta.
        // Si tiene solo un taller y no tiene encuestas activas, muestra el dashboard del taller
        

        //WORKSHOP_APPLICACION
        //The user is trying to choose the workshopapplication
        $workshoprouting = substr($request->get('_route'), 0, 26);
        $workshoproutingig = substr($request->get('_route'), 0, 27);
        if( ($workshoprouting == 'fishman_front_end_workshop')  && ($workshoproutingig != 'fishman_front_end_workshop_') ){
            if($session->has('workshopapplication') && (!isset($_GET['wa'])) ) return;
          
            $wi = $session->get('workinginformation');
            
            $workshopapplications = Workshopapplication::getListActiveWorshopapplications($this->getDoctrine(), $wi['id']);
            $pollapplications = Pollapplication::getListActivePollapplications($this->getDoctrine(), $wi['id']);

            //Lista de encuestas activas
            if(count($pollapplications) == 0 && count($workshopapplications) == 0){
                $event->setResponse(new RedirectResponse($this->getRouter()->generate('user_message',  array('id' => '2'))));
                return;                
            }
            if (count($workshopapplications) == 1 && count($pollapplications) == 0) { //If only have one, set it
                $session->set('workshopapplication', $workshopapplications[0]);
                return;
            }
            if(count($workshopapplications) == 0 && count($pollapplications) == 1){
                //Envia al dashboard de la encuesta
                $event->setResponse(new RedirectResponse($this->getRouter()
                                    ->generate('fishman_front_end_poll', array('pollid' => $pollapplications[0]['psid']))));
                return;
            }

            //If send parameter set it
            if(isset($_GET['wa'])){
                // Recover current Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplication($this->getDoctrine(), $_GET['wa']);
                  
                // Verify if the Workshopapplication there
                if (!empty($workshopapplication)) {
                    $session->set('workshopapplication', $workshopapplication[0]);
                    return;
                }
                else{
                    $event->setResponse(new RedirectResponse(
                        $this->getRouter()->generate('fishman_front_end_homepage')));
                    return;
                }
            }
            else{
                //
                $event->setResponse(new RedirectResponse($this->getRouter()->generate('fishman_front_end_homepage')));
                return;
            }

        }
    }

    protected function setCurrentRol($session, $rol)
    {
        switch($rol){
            case 'ROLE_USER':
            case 'ROLE_TEACHER':
            case 'ROLE_ADMIN':
            case 'ROLE_SUPER_ADMIN':
                $session->set('currentrol', $rol);
                return true;
        }
        return false;
    }

    protected function getDoctrine()
    {
        return $this->doctrine;
    }

    protected function getRouter()
    {
        return $this->router;
    }
}
