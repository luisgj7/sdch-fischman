<?php

namespace Fishman\AuthBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\AuthBundle\Entity\User;
use Fishman\AuthBundle\Form\UserType;
use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\EntityBundle\Entity\Company;
use Fishman\EntityBundle\Entity\Organizationalunit;
use Fishman\EntityBundle\Entity\Charge;
use Fishman\AuthBundle\Form\PeopleimportType;

use Fishman\ImportExportBundle\Entity\Temporalimportdata;
use Ideup\SimplePaginatorBundle\Paginator;
use FOS\UserBundle\Model\UserManager;
use Symfony\Component\HttpFoundation\Response;
use PHPExcel_IOFactory;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $company_options = Company::getListCompanyOptions($this->getDoctrine());
        $organizationalunit_options = Organizationalunit::getListOrganizationalunitOptions($this->getDoctrine());
        $charge_options = Charge::getListChargeOptions($this->getDoctrine());
        $rol_options = User::getListRolOptions();
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',
            'company' => '', 
            'organizationalunit' => '', 
            'charge' => '', 
            'rol' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('company', 'choice', array(
                'choices' => $company_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('organizationalunit', 'choice', array(
                'choices' => $organizationalunit_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('charge', 'choice', array(
                'choices' => $charge_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('rol', 'choice', array(
                'choices' => $rol_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',
            'company' => '', 
            'organizationalunit' => '', 
            'charge' => '', 
            'rol' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
        $queryBuilder = $repository->createQueryBuilder('u')
            ->select('u.charge_id, u.id, u.username username, u.names usernames, u.surname usersurname, 
                      u.lastname userlastname, u.email, c.name company, cou.name organizationalunit, 
                      cch.name charge, u.roles, u.changed, u.enabled, uu.names, uu.surname, uu.lastname')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'u.id = wi.user')
            ->leftJoin('wi.company', 'c')
            ->leftJoin('wi.companycharge', 'cch')
            ->leftJoin('wi.companyorganizationalunit', 'cou')
            ->leftJoin('cou.organizationalunit', 'ou')
            ->leftJoin('cch.charge', 'ch')
            ->innerJoin('FishmanAuthBundle:User', 'uu', 'WITH', 'u.modified_by = uu.id')
            ->Where('u.id LIKE :id 
                    OR u.surname LIKE :surname 
                    OR u.lastname LIKE :lastname  
                    OR u.names LIKE :names 
                    OR u.email LIKE :email')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('surname', '%' . $data['word'] . '%')
            ->setParameter('lastname', '%' . $data['word'] . '%')
            ->setParameter('names', '%' . $data['word'] . '%')
            ->setParameter('email', '%' . $data['word'] . '%')
            ->groupBy('u.id')
            ->orderBy('u.id', 'ASC');
        
        // Add arguments
        
        $queryBuilder
            ->andWhere('u.id <> 2');
        
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('wi.company = :company')
                ->setParameter('company', $data['company']);
        }
        if ($data['organizationalunit'] != '') {
            $queryBuilder
                ->andWhere('ou.id = :organizationalunit')
                ->setParameter('organizationalunit', $data['organizationalunit']);
        }
        if ($data['charge'] != '') {
            $queryBuilder
                ->andWhere('ch.id = :charge')
                ->setParameter('charge', $data['charge']);
        }
        if (!in_array($data['rol'], array('', 'ROLE_USER'))) {
            $queryBuilder
                ->andWhere('u.roles LIKE :rol')
                ->setParameter('rol', '%' . $data['rol'] . '%');
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('u.enabled = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'user');
        $paginator->setMaxPagerItems(5, 'user');
        $entities = $paginator->paginate($query, 'user')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('user');
        $endPageItem = $paginator->getEndPageItem('user');
        $totalItems = $paginator->getTotalItems('user');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanAuthBundle:User:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanAuthBundle:User')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        return $this->render('FishmanAuthBundle:User:show.html.twig', array(
            'entity'  => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        $entity = new User();
        $entity->setEnabled(1);
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        $form   = $this->createForm(new UserType($rol), $entity);
        
        return $this->render('FishmanAuthBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $entity  = $userManager->createUser();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        $form = $this->createForm(new UserType($rol), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            $birthday_on = FALSE;
          
            if ($entity->getBirthday() != '') {
                // Recover years
                $years= intval((strtotime("now")-strtotime($entity->getBirthday()->format('Y/m/d')))/31536000);
                $birthday_on = TRUE;
            }
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
            $uNumberIdentity = $repository->createQueryBuilder('u')
                ->select('count(u.id)')
                ->where('u.identity = :identity 
                        AND u.numberidentity = :numberidentity')
                ->setParameter('identity', $entity->getIdentity())
                ->setParameter('numberidentity', $entity->getNumberidentity())
                ->groupBy('u.id')
                ->getQuery()
                ->getResult();
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
            $uEmail = $repository->createQueryBuilder('u')
                ->select('count(u.id)')
                ->where('u.email = :email')
                ->setParameter('email', $entity->getEmail())
                ->groupBy('u.id')
                ->getQuery()
                ->getResult();
            
            // set flash messages
            $session = $this->getRequest()->getSession();
            
            if ((!$birthday_on || $years > 10) && current($uNumberIdentity) == 0 && current($uEmail) == 0) {
            
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                $entity->setCreatedBy($userBy->getId());
                $entity->setModifiedBy($userBy->getId());
                $entity->setUsername($entity->autoCreateUserName());
                $entity->setPassword(User::generateRandomPassword());
                $entity->setCreated(new \DateTime());
                $entity->setChanged(new \DateTime());
                $entity->setPlainPassword($entity->getPassword());
    
                $userManager->updateUser($entity, false);
                $this->getDoctrine()->getManager()->flush();
                
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('user_show', array(
                    'id' => $entity->getId()
                )));
                
            }
            else {
                $message = '';
              
                if ($birthday_on && $years <= 10) {
                    $message .= "La edad debe ser mayor a 10 años." . '</br>';
                }
                
                if (current($uNumberIdentity) > 0) {
                    $message .= "Ya existe un usuario con el mismo documento de identidad." . '</br>';
                }
                
                if (current($uEmail) > 0) {
                    $message .= "Ya existe un usuario con el mismo correo electónico.";
                }
                
                $session->getFlashBag()->add('error', $message);
            }
        }

        return $this->render('FishmanAuthBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');

        $entity = $em->getRepository('FishmanAuthBundle:User')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new UserType($rol), $entity);

        return $this->render('FishmanAuthBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');

        $entity = $em->getRepository('FishmanAuthBundle:User')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }

        // Old roles
        if($entity->hasRole('ROLE_SUPER_ADMIN')){
            $superAdmin = TRUE;
        }
        if($entity->hasRole('ROLE_ADMIN')){
            $admin = TRUE;
        }       

        $editForm = $this->createForm(new UserType($rol), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            $birthday_on = FALSE;
          
            if ($entity->getBirthday() != '') {
                // Recover years
                $years= intval((strtotime("now")-strtotime($entity->getBirthday()->format('Y/m/d')))/31536000);
                $birthday_on = TRUE;
            }
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
            $uNumberIdentity = $repository->createQueryBuilder('u')
                ->select('count(u.id)')
                ->where('u.identity = :identity 
                        AND u.numberidentity = :numberidentity 
                        AND u.id <> :id')
                ->setParameter('identity', $entity->getIdentity())
                ->setParameter('numberidentity', $entity->getNumberidentity())
                ->setParameter('id', $entity->getId())
                ->groupBy('u.id')
                ->getQuery()
                ->getResult();
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
            $uEmail = $repository->createQueryBuilder('u')
                ->select('count(u.id)')
                ->where('u.email = :email 
                        AND u.id <> :id')
                ->setParameter('email', $entity->getEmail())
                ->setParameter('id', $entity->getId())
                ->groupBy('u.id')
                ->getQuery()
                ->getResult();
            
            if ((!$birthday_on || $years > 10) && current($uNumberIdentity) == 0 && current($uEmail) == 0) {
          
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                $entity->setModifiedBy($modifiedBy->getId());
                $entity->setUsername($entity->autoCreateUserName());
                $entity->setChanged(new \DateTime());
    
                if($modifiedBy->hasRole('ROLE_SUPER_ADMIN')){
                    //No se puede quitar el rol SUPER ADMIN a si mismo
                    if($modifiedBy->getId() == $entity->getId()){
                        $entity->addRole('ROLE_SUPER_ADMIN');
                    }
                }
                else {
                    //El usuario solo puede modificar segun su nivel.
                    //El ROL ADMIN y SUPER_ADMIN solo 
                    if(isset($superAdmin) && $superAdmin){
                        $entity->addRole('ROLE_SUPER_ADMIN');
                    }
                    if(isset($admin) && $admin){
                        $entity->addRole('ROLE_ADMIN');
                    }
                }
    
                $em->persist($entity);
                $em->flush();
                                                             
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('user_show', array(
                    'id' => $id
                )));
                
            }
            else {
                $message = '';
              
                if ($birthday_on && $years <= 10) {
                    $message .= "La edad debe ser mayor a 10 años." . '</br>';
                }
                
                if (current($uNumberIdentity) > 0) {
                    $message .= "Ya existe un usuario con el mismo documento de identidad." . '</br>';
                }
                
                if (current($uEmail) > 0) {
                    $message .= "Ya existe un usuario con el mismo correo electónico.";
                }
                
                $session->getFlashBag()->add('error', $message);
            }
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanAuthBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));

    }

    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanAuthBundle:User')->find($id);
            if (!$entity || $id == 2) {
                $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
                return $this->redirect($this->generateUrl('user'));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $wi = $repository->createQueryBuilder('wi')
                ->select('count(wi.user)')
                ->where('wi.user = :user')
                ->setParameter('user', $entity->getId())
                ->groupBy('wi.user')
                ->getQuery()
                ->getResult();
            
            if (current($wi) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'El Usuario están en uso.');
            }

        }

        return $this->redirect($this->generateUrl('user'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an User entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanAuthBundle:User')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanAuthBundle:User:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Import a User with working information entity.
     *
     */
    public function importAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $defaultData = array();
        $form = $this->createForm(new PeopleimportType(), $defaultData);
        
        $form2 = false;
        $data = '';
        $people = false;
        $numberRegister = 0;
        $message = '';
        $messageError = '';
        $existingwiemail = '';

        $disabledValue = 1;
        
        //Mensaje de que no hay registros
        $session = $this->getRequest()->getSession();
        
        if ($request->isMethod('POST')) {
            
            //The user is confirmed to persist data as User entity
            $importFormData = $request->request->get('form', false);
            
            if($importFormData){
                $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                     ->find($importFormData['temporalimportdata_id']);
                
                if(!$temporalentity){
                    return $this->render('FishmanEntityBundle:User:import.html.twig', array(
                           'form' => $form->createView(),
                           'form2' => $form2,
                           'peoples' => $people,
                    ));
                }
                
                $people = $temporalentity->getData();
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                
                    foreach ($people as $peoplearray) {
                        
                        // Sólo se graban los registros que aparecen como validos
                        
                        if (!$peoplearray['valid']) continue;
                        
                        // Return / Create -> User
                        
                        if ($peoplearray['new_user']) {
                            
                            // Si el número de documento de identidad o correo existen, el registro no será guardado
                            
                            $userNI = $em->getRepository('FishmanAuthBundle:User')->findOneBy(array('numberidentity' => $peoplearray['numberidentity']));
                            if ($userNI) {
                                $messageError .= 'El usuario: ' . 
                                            $userNI->getSurname() . ' ' . $userNI->getLastname() . ' ' . $userNI->getNames() . 
                                            ' está usando el mismo número de documento de identidad.</br>';
                                            
                                continue;
                            }
        
                            $userEmail = $em->getRepository('FishmanAuthBundle:User')->findOneBy(array('email' => $peoplearray['email']));
                            if ($userEmail) {
                                $messageError .= 'El usuario: ' . 
                                            $userEmail->getSurname() . ' ' . $userEmail->getLastname() . ' ' . $userEmail->getNames() . 
                                            ' está usando el mismo correo personal.</br>';
                                
                                continue;
                            }
                            
                            $peopleUser = new User();
                            $peopleUser->setCreatedBy($userBy->getId());
                            $peopleUser->setCreated(new \DateTime());
                        }
                        else {
                            $peopleUser = $em->getRepository('FishmanAuthBundle:User')->find($peoplearray['user_id']);
                        }
                        
                        // User
                        
                        $peopleUser->setSurname($peoplearray['surname']);
                        if ($peoplearray['lastname']) {
                            $peopleUser->setLastname($peoplearray['lastname']);
                        }
                        else {
                            $peopleUser->setLastname('Extranjero');
                        }
                        $peopleUser->setNames($peoplearray['names']);
                        $peopleUser->setIdentity($peoplearray['identity']);
                        $peopleUser->setNumberidentity($peoplearray['numberidentity']);
                        $peopleUser->setEmail($peoplearray['email']);
                        $peopleUser->setSex($peoplearray['sex']);
                        if ($peoplearray['birthday']) {
                            $peopleUser->setBirthday($peoplearray['birthday']);
                        }
                        else {
                            $peopleUser->setBirthday(NULL);
                        }
                        $peopleUser->setUsername($peoplearray['username']);
                        $peopleUser->setPassword($peoplearray['password']);
                        $peopleUser->setEnabled($peoplearray['enabled']);
                        $peopleUser->setModifiedBy($userBy->getId());
                        $peopleUser->setChanged(new \DateTime());
                        
                        if ($peoplearray['valid_information'] && $peoplearray['wi_type'] == 'working') {
                            $peopleUser->setCompanyId($peoplearray['company_id']);
                            $peopleUser->setOrganizationalunitId($peoplearray['ou_id']);
                            $peopleUser->setChargeId($peoplearray['charge_id']);
                        }
                        
                        //Save user
                        $em->persist($peopleUser);
                        $em->flush();
                        
                        $numberRegister++;
                        
                        // Sólo se graban los registros que aparecen como validos
                        
                        if (!$peoplearray['valid_information']) continue;
                        
                        // Return / Create -> Workinginformation
                        
                        if ($peoplearray['new_wi']) {
                            $wiUser = new Workinginformation($this->getDoctrine());
                            $wiUser->setCreatedBy($userBy->getId());
                            $wiUser->setCreated(new \DateTime());
                        }
                        else{
                            $wiUser = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($peoplearray['wi_id']);
                        }
                        
                        $wiUser->setUser($peopleUser);
                        $wiUser->setCode($peoplearray['numberidentity']);
                        $wiUser->setCompany($em->getRepository('FishmanEntityBundle:Company')->find($peoplearray['company_id']));
                        $wiUser->setHeadquarter($em->getRepository('FishmanEntityBundle:Headquarter')->find($peoplearray['headquarter_id']));
                        $wiUser->setStatus($peoplearray['status']);
                        $wiUser->setModifiedBy($userBy->getId());
                        $wiUser->setChanged(new \DateTime());
                        
                        if ($peoplearray['wi_type'] == 'workinginformation') {
                            
                            // Working information
                            
                            $wiUser->setType('workinginformation');
                            $wiUser->setEmail($peoplearray['email_information']);
                            $wiUser->setUnitType($peoplearray['ou_unit_type']);
                            $wiUser->setCompanyorganizationalunit($em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->find($peoplearray['ou_id']));
                            $wiUser->setCompanycharge($em->getRepository('FishmanEntityBundle:Companycharge')->find($peoplearray['charge_id']));
                            
                        }
                        elseif ($peoplearray['wi_type'] == 'schoolinformation') {
                            
                            // School information
                            
                            $wiUser->setType('schoolinformation');
                            $wiUser->setGrade($peoplearray['grade_code']);
                            $wiUser->setStudyYear($peoplearray['study_year']);
                            
                        }
                        elseif ($peoplearray['wi_type'] == 'universityinformation') {
                            
                            // University information
                            
                            $wiUser->setType('universityinformation');
                            $wiUser->setCompanyfaculty($em->getRepository('FishmanEntityBundle:Companyfaculty')->find($peoplearray['faculty_id']));
                            $wiUser->setCompanycareer($em->getRepository('FishmanEntityBundle:Companycareer')->find($peoplearray['career_id']));
                            $wiUser->setEmail($peoplearray['email_information']);
                            $wiUser->setAcademicYear($peoplearray['academic_year']);
                            
                        }
                        
                        //Save information 
                        $em->persist($wiUser);
                        $em->flush();
                        
                    } // end foreach
    
                    // Delete temporal data
                    $em->remove($temporalentity);
                    $em->flush();
                
                    // Delete temporal files
                    User::deleteTemporalFiles('../tmp/');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }

                if ($numberRegister > 0) {
                    $session->getFlashBag()->add('status', 'Se grabó la importación de ' . $numberRegister . ' Persona(s)');
                    if ($messageError != '') {
                        $session->getFlashBag()->add('error', $messageError);
                    }
                }
                else {
                    $session->getFlashBag()->add('error', 'No se logró importar ninguna Persona.');
                }
                if($request->request->get('saveandclose', true)){
                    return $this->redirect($this->generateUrl('user'));
                }

                $people = false;

            } //End have temporarlimportdata input
            else {
                $form->bind($request);
                $data = $form->getData();
                
                // We need to move the file in order to use it
                $randomName = rand(1, 10000000000);
                $randomName = $randomName . '.xlsx';
                $directory = __DIR__.'/../../../../tmp';
                $data['file']->move($directory, $randomName);

                if (null !== $data['file']) {
                    $fileName = $data['file']->getClientOriginalName();
                    $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                }

                if ($data['file']->document == 'xlsx') {
                    $inputFileName = $directory . '/' . $randomName;
                    $phpExcel = PHPExcel_IOFactory::load($inputFileName);
                    
                    $worksheet = $phpExcel->getSheet(0);
                    
                    $fielTypeExcel = trim($worksheet->getCellByColumnAndRow(1, 2)->getValue());
                    
                    if (in_array($fielTypeExcel, array('working', 'school', 'university'))) {
                      
                        if ($fielTypeExcel == $data['type']) {
                            
                            $typeWIName = User::typeInformationName($data['type']);
                            
                            switch ($data['type']) {
                                case 'working':
                                    $wiType = 'workinginformation';
                                    break;
                                case 'school':
                                    $wiType = 'schoolinformation';
                                    break;
                                case 'university':
                                    $wiType = 'universityinformation';
                                    break;
                            }
                            
                            // Data defaults
                            
                            $typeIdentity = '';
                            $companyId = '';
                            $birthday = '';
                            
                            $i = 0;
                            $row = 5;
                            $people = array();
                            
                            do {
                                
                                // Data User
                                
                                $surname = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                                $lastname = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                                $names = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                                $typeIdentityRow = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                                if (in_array($typeIdentityRow, array('d', 'e', 'p'))) {
                                    $typeIdentity = User::getDocumentTypeByEquivalent($typeIdentityRow);
                                }
                                $numberIdentity = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                                $birthdayRow = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                                if ($birthdayRow) {
                                    $birthday = \DateTime::createFromFormat('d/m/Y', $birthdayRow);
                                }
                                $sex = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
                                $email = trim($worksheet->getCellByColumnAndRow(7, $row)->getValue());
                                
                                $code = $numberIdentity;
                                $newUser = 0;
                                $userId = 0;
                                $enabled = 0;
                                $createdBy = '';

                                // Generate Username
                                $username = User::createUserName($typeIdentity, $numberIdentity);
                                // Generate Password
                                $password = User::generateRandomPassword();
                                
                                $companyCode = trim($worksheet->getCellByColumnAndRow(8, $row)->getValue());
                                $headquarterCode = trim($worksheet->getCellByColumnAndRow(9, $row)->getValue());
                                $wiId = 0;
                                $companyName = '';
                                $headquarterId = '';
                                $headquarterName = '';
                                $status = 0;
                                
                                if ($data['type'] == 'working') {
                                
                                    // Data Working
                                    
                                    $ouCode = trim($worksheet->getCellByColumnAndRow(10, $row)->getValue());
                                    $chargeCode = trim($worksheet->getCellByColumnAndRow(11, $row)->getValue());
                                    $emailInformation = trim($worksheet->getCellByColumnAndRow(12, $row)->getValue());
                                    $newWI = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                    
                                    $ouId = '';
                                    $ouName = '';
                                    $ouUnitType = '';
                                    $chargeId = '';
                                    $chargeName = '';
                                    $bossId = 0;
                                    
                                    // Finish do while
                                    
                                    if ($code == '' && $names == '' && $companyCode == '' && $ouCode == '' && $chargeCode == '' 
                                        && $email == '' && $emailInformation == ''){
                                        break;
                                    }
                                        
                                }
                                elseif ($data['type'] == 'school') {
                                    
                                    // Data School
                                    
                                    $gradeCode = trim($worksheet->getCellByColumnAndRow(10, $row)->getValue());
                                    $studyYear = trim($worksheet->getCellByColumnAndRow(11, $row)->getValue());
                                    $newWI = trim($worksheet->getCellByColumnAndRow(12, $row)->getValue());
                                    
                                    // Finish do while
                                    
                                    if ($code == '' && $names == '' && $companyCode == '' && $gradeCode == '' && $studyYear == ''){
                                        break;
                                    }
                                    
                                }
                                elseif ($data['type'] == 'university') {
                                    
                                    // Data University
                                    
                                    $facultyCode = trim($worksheet->getCellByColumnAndRow(10, $row)->getValue());
                                    $careerCode = trim($worksheet->getCellByColumnAndRow(11, $row)->getValue());
                                    $emailInformation = trim($worksheet->getCellByColumnAndRow(12, $row)->getValue());
                                    $academicYear = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                    $newWI = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                    
                                    $facultyId = '';
                                    $facultyName = '';
                                    $careerId = '';
                                    $careerName = '';
                                    
                                    // Finish do while
                                    
                                    if ($code == '' && $names == '' && $companyCode == '' && $facultyCode == '' && 
                                        $emailInformation == '' && $academicYear == ''){
                                        break;
                                    }
                                    
                                }
                                
                                // Data validation
                                
                                $message = '';
                                $validPeople = 1;
                                $validInformation = 1;
                                
                                $row++;
                                
                                if (!preg_match('/^\d+$/',$newWI)) {
                                    $newWI = 0;
                                }
        
                                if ($names == '') {
                                    $message .= '<p>Falta el nombre de la persona.</p>';
                                    $validPeople = 0;
                                }
                                
                                if (!$typeIdentity) {
                                    $message .= '<p>El tipo de documento de identidad no es correcto, 
                                                    debe colocar d, e, p. d = dni, e = carné de extranjería, p = pasaporte.</p>';
                                    $validPeople = 0;
                                }
                                
                                if (!$companyCode) {
                                    $message .= '<p>Falta el código de la empresa.</p>';
                                    $validInformation = 0;
                                }
                                else {
                                    $company = $em->getRepository('FishmanEntityBundle:Company')->findOneBy(
                                        array('code' => $companyCode));
                                    if ($company) {
                                        $companyId = $company->getId();
                                        $companyName = $company->getName();
                                    }
                                    else {
                                        $message .= '<p>No existe una Empresa con este código.</p>';
                                        $validInformation = 0;
                                    }
                                }
                                    
                                if ($headquarterCode == '') {
                                    $message .= '<p>Falta el código de la sede.</p>';
                                    $validInformation = 0;
                                }
                                else {
                                    $h = $em->getRepository('FishmanEntityBundle:Headquarter')->findOneBy(
                                        array( 'company' => $companyId , 'code' => $headquarterCode , 'status' => 1));
                                    if ($h) {
                                        $headquarterId = $h->getId();
                                        $headquarterName = $h->getName();
                                    }
                                    else {
                                        $message .= '<p>El código de Sede no existe o está inactivo.</p>';
                                        $validInformation = 0;
                                    }
                                }
                                
                                // Validate Information
                                
                                if ($data['type'] == 'working') {
                                    
                                    // Validate Working
                                    
                                    if ($ouCode == '') {
                                        $message .= '<p>Falta el código de la unidad organizativa.</p>';
                                        $validInformation = 0;
                                    }
                                    else {
                                        $ou = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->findOneBy(
                                            array('company' => $companyId , 'code' => $ouCode , 'status' => 1));
                                        if ($ou) {
                                            $ouId = $ou->getId();
                                            $ouName = $ou->getName();
                                            $ouUnitType = $ou->getUnitType();
                                        }
                                        else {
                                            $message .= '<p>El código de la Unidad no existe o está inactivo.</p>';
                                            $validInformation = 0;
                                        }
                                    }
                                    
                                    if ($chargeCode == '') {
                                        $message .= '<p>Falta el código del cargo.</p>';
                                        $validInformation = 0;
                                    }
                                    else{
                                        $charge = $em->getRepository('FishmanEntityBundle:Companycharge')->findOneBy(
                                            array('company' => $companyId , 'code' => $chargeCode , 'status' => 1));
                                        if ($charge) {
                                            $chargeId = $charge->getId();
                                            $chargeName = $charge->getName();
                                        }
                                        else {
                                            $message .= '<p>El código del Cargo no existe o está inactivo.</p>';
                                            $validInformation = 0;
                                        }
                                    }
                                    
                                    if ($emailInformation == '') {
                                        $message .= '<p>Falta el correo laboral.</p>';
                                        $validInformation = 0;
                                    }
                                    
                                }
                                elseif ($data['type'] == 'school') {
                                    
                                    // Validate School
                                    
                                    if ($gradeCode == '') {
                                        $message .= '<p>Falta el código de grado.</p>';
                                        $validInformation = 0;
                                    }
                                    
                                    if ($studyYear == '') {
                                        $message .= '<p>Falta el año de estudio.</p>';
                                        $validInformation = 0;
                                    }
                                
                                }
                                elseif ($data['type'] == 'university') {
                                    
                                    // Validatie University
                                    
                                    if ($facultyCode == '') {
                                        $message .= '<p>Falta el código de la facultad.</p>';
                                        $validInformation = 0;
                                    }
                                    else {
                                        $faculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->findOneBy(
                                            array( 'company' => $companyId , 'code' => $facultyCode , 'status' => 1));
                                        if ($faculty) {
                                            $facultyId = $faculty->getId();
                                            $facultyName = $faculty->getName();
                                        }
                                        else {
                                            $message .= '<p>El código de Facultad no existe o está inactivo.</p>';
                                            $validInformation = 0;
                                        }
                                    }
                                    
                                    if ($careerCode == '') {
                                        $message .= '<p>Falta el código de la carrera.</p>';
                                        $validInformation = 0;
                                    }
                                    else {
                                        $career = $em->getRepository('FishmanEntityBundle:Companycareer')->findOneBy(
                                            array( 'company' => $companyId , 'code' => $careerCode , 'status' => 1));
                                        if ($career) {
                                            $careerId = $career->getId();
                                            $careerName = $career->getName();
                                        }
                                        else {
                                            $message .= '<p>El código de Carrera no existe o está inactivo.</p>';
                                            $validInformation = 0;
                                        }
                                    }
                                    
                                    if ($emailInformation == '') {
                                        $message .= '<p>Falta el correo universitario.</p>';
                                        $validInformation = 0;
                                    }
                                    
                                    if ($academicYear == '') {
                                        $message .= '<p>Falta el ciclo académico.</p>';
                                        $validInformation = 0;
                                    }
                                    
                                }
                                
                                //El identificador del usuario es su username
                                $result = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->findByUsername($username);
                                
                                if ($result) {
                                    $peopleUser = current($result);
                                    $userId = $peopleUser->getId();
                                    if ($newWI == 0) {
                                        
                                        //Si se indica que hay que actualizar la informacion laboral
                                        //Busca la información laboral por el código, dentro de la empresa
                                        //Si no lo encuentra, emite un mensaje e indica que se creará uno nuevo
                                        
                                        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
                                        $queryBuilder = $repository->createQueryBuilder('wi')
                                             ->select('wi.id, wi.status')
                                             ->where('wi.code = :code AND 
                                                      wi.type = :type AND
                                                      wi.company = :company_id AND
                                                      wi.user = :user_id AND
                                                      wi.status = :status')
                                             ->setParameter('code', trim($code))
                                             ->setParameter('type', $wiType)
                                             ->setParameter('company_id', $companyId)
                                             ->setParameter('user_id', $peopleUser->getId())
                                             ->setParameter('status', 1)
                                             ->orderBy('wi.id', 'DESC')
                                             ->setMaxResults(1);
                                        $wis = $queryBuilder->getQuery()->getResult();
                                        
                                        //Si hay varios intenta actualizar el activo (solo debería haber 1 activo)                            
                                        //Si encuentra registros, de ahi copia el status y el wiId
                                        
                                        if ($wis) {
                                            $wi = current($wis);
                                            $status = $wi['status'];
                                            $wiId = $wi['id'];
                                        }
                                        else {
                                            //Sino, intenta obtener con status = 0
                                            //En este caso emite un mensaje, pero deja grabar los datos
                                            $queryBuilder->setParameter('status', 0);
                                            $wis = $queryBuilder->getQuery()->getResult();
                                            if ($wis) {
                                                $wi = current($wis);
                                                $status = $wi['status'];
                                                $wiId = $wi['id'];
                                                $message .= '<p>La información ' . $typeWIName . ' de este usuario está desactiva. 
                                                                Para activarla, hacerlo a través de la interfaz administrativa.</p>';
                                            }
                                            else {
                                                $status = 1;
                                                $newWI = 1;
                                                if (!$validInformation) {
                                                    $message .= '<p>No se encontró información ' . $typeWIName . ' de este usuario.</p>';
                                                }
                                                else {
                                                    $message .= '<p>No se encontró información ' . $typeWIName . ' de este usuario. Se creará una nueva información laboral.</p>';
                                                }
                                            }
                                        }
                                        $enabled = 1;
                                    }
                                    else {
                                        $newWI = 1;
                                        $status = 1;
                                        $enabled = 1;
                                    }
        
                                    //Verifica el correo coorporativo, para emitir el mensaje
                                    //Esta validacion si impide que se guarde el registro
                                    
                                    if ($data['type'] != 'school') {
                                        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
                                        $queryBuilder = $repository->createQueryBuilder('wi')
                                                         ->select('wi.email')
                                                         ->where('wi.email = :email')
                                                         ->andWhere('wi.user <> :user_id')
                                                         ->setParameter('email', $emailInformation)
                                                         ->setParameter('user_id', $userId);
                                        $existinwiemail = $queryBuilder->getQuery()->getResult();
                                        if ($existingwiemail) {
                                            $message .= '<p>El correo pertenece a la información ' . $typeWIName . ' de otro usuario</p>';       
                                            $validInformation = 0;
                                        }
                                    }
        
                                }
                                else {
                                
                                    if ($numberIdentity == '') {
                                        $message .= '<p>Falta el número de documento de identidad.</p>';
                                        $validPeople = 0;
                                    }
                                    else {
                                        $ni = $em->getRepository('FishmanAuthBundle:User')->findOneBy(array('numberidentity' => $numberIdentity));
                                        if ($ni) {
                                            $message .= '<p>El número de documento de identidad esta siendo usado.</p>';
                                            $validPeople = 0;
                                        }
                                    }
            
                                    if ($email == '') {
                                        $message .= '<p>Falta el correo personal.</p>';
                                        $validPeople = 0;
                                    }
                                    else {
                                        $e = $em->getRepository('FishmanAuthBundle:User')->findOneBy(array('email' => $email));
                                        if ($e) {
                                            $message .= '<p>El correo personal está siendo usado.</p>';
                                            $validPeople = 0;
                                        }
                                    }
                                    
                                    //Tiene que crear el usuario y la informacion laboral
                                    
                                    $newUser = 1;
                                    $enabled = 1;
                                    $status = 1;
                                    $newWI = 1;
        
                                    //Verifica que nadie tenga como correo personal el que viene en el archivo de import.
                                    //Esta validacion si impide que se guarde el registro
                                    
                                    $stmt = $this->getDoctrine()->getConnection()
                                                 ->prepare("SELECT count(u.id) as count
                                                            FROM Authuser u 
                                                            WHERE u.email_canonical = :email");
                                    $stmt->bindValue('email', $email);
                                    $stmt->execute();
                                    $result = $stmt->fetchAll();
                                    $existingpersonalemail = $result[0]['count'];
        
                                    if ($existingpersonalemail > 0) {
                                        $message .= '<p>El correo pertenece a otro usuario como correo personal</p>';
                                        $validPeople = 0;
                                    }
                                    else{
                                        //Verifica el correo coorporativo, para emitir el mensaje
                                        //Esta validacion no impide que se guarde el dato
                                        if ($data['type'] != 'school') {
                                            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
                                            $queryBuilder = $repository->createQueryBuilder('wi')
                                                             ->select('wi.email')
                                                             ->where('wi.email = :email')
                                                             ->setParameter('email', $emailInformation);
                                            $existingwiemail = $queryBuilder->getQuery()->getResult();
                                            if ($existingwiemail) {
                                                $message .= '<p>El correo pertenece a la información ' . $typeWIName . ' de otro usuario</p>';
                                                $validInformation = 0;
                                            }
                                        }
                                    }
                                    
                                }
        
                                // Validation check
                                
                                $disabledValue = $validPeople;
        
                                if ($validPeople && $validPeople && $message == '') {
                                    $message = 'OK';
                                }
                                
                                if ($validPeople && !$validInformation) {
                                    $message .= '<p><strong>No se creará la información ' . $typeWIName . '</strong></p>';
                                }
                                
                                // Array people
                                    
                                $people[$i] = array(
                                                'user_id' => $userId,
                                                'code' => $code,
                                                'surname' => $surname,
                                                'lastname' => $lastname,
                                                'names' => $names,
                                                'sex' => $sex,
                                                'identity' => $typeIdentity,
                                                'numberidentity' => $numberIdentity,
                                                'birthday' => $birthday,
                                                'email' => $email,
                                                'enabled' => $enabled,
                                                'password' => $password,
                                                'username' => $username,
                                                'company_id' => $companyId,
                                                'company_code' => $companyCode,
                                                'company_name' => $companyName,
                                                'headquarter_id' => $headquarterId,
                                                'headquarter_name' => $headquarterName,
                                                'status' => $status,
                                                'created_by' => $createdBy,
                                                'new_user' => $newUser,
                                                'new_wi' => $newWI,
                                                'wi_id' => $wiId,  
                                                'message' => $message,
                                                'valid' => $validPeople,
                                                'valid_information' => $validInformation
                                            );
                                
                                if ($data['type'] == 'working') {
                                    
                                    // People Working
                                    
                                    $people[$i]['wi_type'] = 'workinginformation';
                                    $people[$i]['ou_id'] = $ouId;
                                    $people[$i]['ou_name'] = $ouName;
                                    $people[$i]['ou_unit_type'] = $ouUnitType;
                                    $people[$i]['charge_id'] = $chargeId;
                                    $people[$i]['charge_name'] = $chargeName;
                                    $people[$i]['email_information'] = $emailInformation;
                                                
                                }
                                elseif ($data['type'] == 'school') {
                                             
                                    // People School
                                    
                                    $people[$i]['wi_type'] = 'schoolinformation';
                                    $people[$i]['grade_code'] = $gradeCode;
                                    $people[$i]['study_year'] = $studyYear;
                                    
                                }
                                elseif ($data['type'] == 'university') {
                                    
                                    // People University
                                    
                                    $people[$i]['wi_type'] = 'universityinformation';
                                    $people[$i]['faculty_id'] = $facultyId;
                                    $people[$i]['faculty_name'] = $facultyName;
                                    $people[$i]['career_id'] = $careerId;
                                    $people[$i]['career_name'] = $careerName;
                                    $people[$i]['email_information'] = $emailInformation;
                                    $people[$i]['academic_year'] = $academicYear;
                                    
                                }
                                
                                $i++;
                                    
                            } while (true);
                            
                        }
                        else {
                            $session->getFlashBag()->add('error', 'El archivo a importar debe ser del mismo tipo de archivo que ha seleccionado.');
                        }
                    }
                    else {
                        $session->getFlashBag()->add('error', 'El tipo de información es erroneo debe ser "working" => laboral, 
                                                               "school" => colegio, "university" => universidad.');
                    }
                    
                }
                else {
                    $session->getFlashBag()->add('error', 'El archivo a importar debe ser de formato excel.');
                }
                
                if(count($people) > 0 and is_array($people)){
                    $temporal = new Temporalimportdata();
                    $temporal->setData($people);
                    $temporal->setEntity('user');
                    $temporal->setDatetime(new \DateTime());

                    $em->persist($temporal);
                    $em->flush();

                    // Secondary form
                    $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                    $form2 = $this->createFormBuilder($defaultData2)
                        ->add('temporalimportdata_id',
                              'integer', array (
                                       'required' => true 
                             ))
                        ->getForm();
                    $form2 = $form2->createView();
                }
                else {
                    $session->getFlashBag()->add('error', 'No hay personas para importar.');
                }
                
                //Delete temporal file
                if ($data['file']->document == 'xlsx') {
                    unlink($inputFileName);
                }
            }
        } //End is method post

        return $this->render('FishmanAuthBundle:User:import.html.twig', array(
            'form' => $form->createView(),
            'form2' => $form2,
            'people' => $people,
            'disabled_valid' => $disabledValue,
            'data' => $data
        ));
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $type = $_GET['type'];
        $filename = '';
        
        switch ($type) {
            case 'working':
                $filename = 'importar-personas-laboral.xlsx';
                break;
            case 'school':
                $filename = 'importar-personas-colegio.xlsx';
                break;
            case 'university':
                $filename = 'importar-personas-universidad.xlsx';
                break;
        }
        
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"'
        );
        return new Response(file_get_contents( __DIR__.'/../../../../templates/AuthBundle/User/' . $filename), 200, $headers);
    }
    
    /**
     * 
     */
    public function resettingpasswordAction($username)
    {
        /**** Basado en UserBundle:RessetingController->sendEmailAction ****/

        //Solo el rol ADMIN puede resetear un password
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);
        $session = $this->getRequest()->getSession();

        if (null === $user) {
            $session->getFlashBag()->add('status', 'No se encontró el usuario para resetear su clave');
            return $this->redirect($this->generateUrl('user'));            
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $obfuscatedEmail = $user->getEmail();
        if (false !== $pos = strpos($obfuscatedEmail, '@')) {
            $obfuscatedEmail = '...' . substr($obfuscatedEmail, $pos);
        }

        $session->set('fos_user_send_resetting_email/email', $obfuscatedEmail);
        $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);

        $session->getFlashBag()->add('status', 'Se envió el enlace para cambio de clave al usuario');
        return $this->redirect($this->generateUrl('user'));
    }
}
