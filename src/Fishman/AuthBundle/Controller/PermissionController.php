<?php

namespace Fishman\AuthBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Company;
use Fishman\PollBundle\Entity\Poll;
use Fishman\PollBundle\Entity\Pollscheduling;

use Fishman\AuthBundle\Entity\Permission;
use Fishman\AuthBundle\Form\PermissionType;

/**
 * Permission controller.
 *
 */
class PermissionController extends Controller
{
    /**
     * Lists all Permission entities.
     *
     */
    public function indexAction(Request $request, $userid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // User Info
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recovering data
        
        $company_options = Company::getListCompanyOptions($this->getDoctrine());
        $poll_options = Poll::getListPollOptions($this->getDoctrine());
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'company' => '', 
            'poll' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('company', 'choice', array(
                'choices' => $company_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('poll', 'choice', array(
                'choices' => $poll_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'company' => '', 
            'poll' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Permission');
        $queryBuilder = $repository->createQueryBuilder('pr')
            ->select('pr.id, c.name company, pr.company_id, p.title poll, pr.poll_id, pr.initdate, pr.enddate, 
                      pr.changed, pr.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'pr.company_id = c.id')
            ->innerJoin('FishmanPollBundle:Poll', 'p', 'WITH', 'pr.poll_id = p.id')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pr.modified_by = u.id')
            ->where('pr.user = :user')
            ->andWhere('pr.id LIKE :id')
            ->setParameter('user', $userid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->orderBy('pr.id', 'ASC');
        
        // Add arguments
        
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('pr.company_id = :company')
                ->setParameter('company', $data['company']);
        }
        if ($data['poll'] != '') {
            $queryBuilder
                ->andWhere('pr.poll_id = :poll')
                ->setParameter('poll', $data['poll']);
        }
        if (!empty($data['initdate'])) {
            $queryBuilder
                ->andWhere('pr.initdate = :initdate')
                ->setParameter('initdate', $data['initdate']);
        }
        if (!empty($data['enddate'])) {
            $queryBuilder
                ->andWhere('pr.enddate = :enddate')
                ->setParameter('enddate', $data['enddate']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('pr.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'permission');
        $paginator->setMaxPagerItems(5, 'permission');
        $entities = $paginator->paginate($query, 'permission')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('permission');
        $endPageItem = $paginator->getEndPageItem('permission');
        $totalItems = $paginator->getTotalItems('permission');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanAuthBundle:Permission:index.html.twig', array(
            'entities' => $entities,
            'user' => $user,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Permission entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Permission
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Permission');
        $result = $repository->createQueryBuilder('pr')
            ->select('pr.id, c.name company, p.title poll, pr.pollschedulings, pr.benchmarking_internal, 
                      pr.benchmarking_external, pr.filters, pr.initdate, pr.enddate, u.id user_id, u.names, u.surname, u.lastname, 
                      pr.status, pr.created, pr.changed, pr.created_by, pr.modified_by')
            ->innerJoin('pr.user', 'u')
            ->leftJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'pr.company_id = c.id')
            ->leftJoin('FishmanPollBundle:Poll', 'p', 'WITH', 'pr.poll_id = p.id')
            ->where('pr.id = :permission')
            ->setParameter('permission', $id)
            ->getQuery()
            ->getResult();
        $entity = current($result);
        
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar los permisos.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recover Ids of Pollscheduling Period
        $pscIds = '';
        foreach ($entity['pollschedulings'] as $psc) {
            if ($pscIds == '') {
                $pscIds = $psc;
            }
            else {
                $pscIds .= ', ' . $psc;
            } 
        }
        
        // Recover Period of Pollschedulings
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $pollschedulings = $repository->createQueryBuilder('psc')
            ->select('psc.id, psc.pollscheduling_period psc_period')
            ->where('psc.id IN(' . $pscIds . ')')
            ->getQuery()
            ->getResult();
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanAuthBundle:Permission:show.html.twig', array(
            'entity' => $entity,
            'periods' => $pollschedulings,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Permission entity.
     *
     */
    public function newAction($userid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover User
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        $entity = new Permission();
        $entity->setUser($user);
        $entity->setStatus(1);
        $form   = $this->createForm(new PermissionType($entity, $this->getDoctrine()), $entity);
        
        // Polls
        $selectPolls = Poll::getWidgetPollsByCompany(
            $this->getDoctrine(),
            FALSE,
            FALSE,
            $userid,
            $entity
        );
        
        // Pollschedulings
        $selectPollschedulings = Pollscheduling::getWidgetPollschedulingsByPoll(
            $this->getDoctrine(),
            FALSE,
            FALSE,
            FALSE,
            $userid,
            $entity
        );
        
        // Filters
        $selectFilters = Company::getWidgetFiltersByTypeCompany();
        
        return $this->render('FishmanAuthBundle:Permission:new.html.twig', array(
            'entity' => $entity,
            'user' => $user,
            'form' => $form->createView(),
            'selectPolls' => $selectPolls,
            'selectPollschedulings' => $selectPollschedulings,
            'selectFilters' => $selectFilters
        ));
    }

    /**
     * Creates a new Permission entity.
     *
     */
    public function createAction(Request $request, $userid)
    {
        $entity  = new Permission();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover User
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recover ṕstData of Pollschedulings and Filters
           
        $pollschedulings = $request->request->get('pollschedulings_options', null);
        $filters = $request->request->get('filters_options', null);
        $postDataEntity = $request->request->get('fishman_authbundle_permissiontype', null);
        
        $entity->setUser($user);
        $form = $this->createForm(new PermissionType($entity, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            // Recover Initdate and Enddate
            $initdate = strtotime($entity->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getEnddate()->format('Y/m/d'));
            
            if ($initdate < $enddate) {
                
                $pscIds = '';
                
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                // Check Pollschedulings benchmarking
                $i = 0;
                if (!empty($pollschedulings['data2'])) {
                    foreach ($pollschedulings['data2'] as $pscId) {
                        if ($i == 0) {
                            $pscIds = $pscId;
                        }
                        else {
                            $pscIds .= ',' . $pscId;
                        }
                        $i++;
                    }
                }
                
                // Recovers value
                $value = Pollscheduling::getValueBenchmarkingByPollschedulings($this->getDoctrine(), $pscIds);
                
                // Set Data
                
                $entity->setPollschedulings($pollschedulings['data2']);
                if (!$value) {
                    $entity->setBenchmarkingExternal(0);
                    $entity->setFilters(NULL);
                }
                else {
                    $entity->setFilters($filters['data2']);
                }
                $entity->setCreated(new \DateTime());
                $entity->setChanged(new \DateTime());
                $entity->setCreatedBy($userBy->getId());
                $entity->setModifiedBy($userBy->getId());
                
                // Save Data
                $em->persist($entity);
                $em->flush();
                
                // Message
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
                
                return $this->redirect($this->generateUrl('permission_show', array(
                    'id' => $entity->getId()
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
            
        }
        
        // Polls
        $selectPolls = Poll::getWidgetPollsByCompany(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getPollId(),
            $userid,
            $entity
        );
        
        // Pollschedulings
        $selectPollschedulings = Pollscheduling::getWidgetPollschedulingsByPoll(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getPollId(),
            $pollschedulings['data2'],
            $userid,
            $entity
        );
        
        // Filters
        $selectFilters = Company::getWidgetFiltersByTypeCompany(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $filters['data2']
        );
        
        return $this->render('FishmanAuthBundle:Permission:new.html.twig', array(
            'entity' => $entity,
            'user' => $user,
            'form' => $form->createView(),
            'selectPolls' => $selectPolls,
            'selectPollschedulings' => $selectPollschedulings,
            'selectFilters' => $selectFilters
        ));
    }

    /**
     * Displays a form to edit an existing Permission entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Permission
        $entity = $em->getRepository('FishmanAuthBundle:Permission')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar los permisos.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recover Permission
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($entity->getCompanyId());
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('permission', array(
                'userid' => $entity->getUser()->getId()
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new PermissionType($entity, $this->getDoctrine()), $entity);
        
        // Polls
        $selectPolls = Poll::getWidgetPollsByCompany(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getPollId(),
            $entity->getUser()->getId(),
            $entity
        );
        
        // Pollschedulings
        $selectPollschedulings = Pollscheduling::getWidgetPollschedulingsByPoll(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getPollId(),
            $entity->getPollschedulings(),
            $entity->getUser()->getId(),
            $entity
        );
        
        // Filters
        $selectFilters = Company::getWidgetFiltersByTypeCompany(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getFilters()
        );
        
        return $this->render('FishmanAuthBundle:Permission:edit.html.twig', array(
            'entity' => $entity,
            'user' => $entity->getUser(),
            'company' => $company,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView(),
            'selectPolls' => $selectPolls,
            'selectPollschedulings' => $selectPollschedulings,
            'selectFilters' => $selectFilters
        ));
    }

    /**
     * Edits an existing Permission entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanAuthBundle:Permission')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar los permisos.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recover ṕstData of Pollschedulings and Filters
        
        $pollschedulings = $request->request->get('pollschedulings_options', null);
        $filters = $request->request->get('filters_options', null);
        $postDataEntity = $request->request->get('fishman_authbundle_permissiontype', null);
        
        $editForm = $this->createForm(new PermissionType($entity, $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // Recover Initdate and Enddate
            $initdate = strtotime($entity->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getEnddate()->format('Y/m/d'));
            
            if ($initdate < $enddate) {
              
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                // Check Pollschedulings benchmarking
                $i = 0;
                foreach ($pollschedulings['data2'] as $pscId) {
                    if ($i == 0) {
                        $pscIds = $pscId;
                    }
                    else {
                        $pscIds .= ',' . $pscId;
                    }
                    $i++;
                }
                // Recovers value
                $value = Pollscheduling::getValueBenchmarkingByPollschedulings($this->getDoctrine(), $pscIds);
                
                // Set Data
                
                $entity->setPollschedulings($pollschedulings['data2']);
                if (!$value) {
                    $entity->setBenchmarkingExternal(0);
                    $entity->setFilters(NULL);
                }
                else {
                    $entity->setFilters($filters['data2']);
                }
                $entity->setChanged(new \DateTime());
                $entity->setModifiedBy($userBy->getId());
                // Save Data
                $em->persist($entity);
                $em->flush();
                
                // Message
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
                
                return $this->redirect($this->generateUrl('permission_show', array(
                    'id' => $entity->getId()
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }
        
        // Recover Permission
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($entity->getCompanyId());
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('permission', array(
                'userid' => $entity->getUser()->getId()
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        // Polls
        $selectPolls = Poll::getWidgetPollsByCompany(
            $this->getDoctrine(), 
            $entity->getCompanyId(), 
            $entity->getPollId(), 
            $entity->getUser()->getId(), 
            $entity
        );
        
        // Pollschedulings
        $selectPollschedulings = Pollscheduling::getWidgetPollschedulingsByPoll(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getPollId(),
            $pollschedulings['data2'],
            $entity->getUser()->getId(),
            $entity
        );
        
        // Filters
        $selectFilters = Company::getWidgetFiltersByTypeCompany(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $filters['data2']
        );
        
        return $this->render('FishmanAuthBundle:Permission:edit.html.twig', array(
            'entity' => $entity,
            'user' => $entity->getUser(),
            'company' => $company,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView(),
            'selectPolls' => $selectPolls,
            'selectPollschedulings' => $selectPollschedulings,
            'selectFilters' => $selectFilters
        ));
    }

    /**
     * Drop Permission entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Permission
        $entity = $em->getRepository('FishmanAuthBundle:Permission')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar los permisos.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recover Permission
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($entity->getCompanyId());
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('permission', array(
                'userid' => $entity->getUser()->getId()
            )));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanAuthBundle:Permission:drop.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a Permission entity.
     *
     */
    public function deleteAction(Request $request, $id, $userid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            // Recover Permission
            $entity = $em->getRepository('FishmanAuthBundle:Permission')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar los permisos.');
                return $this->redirect($this->generateUrl('permission', array(
                    'userid' => $userid
                )));
            }

            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('permission', array(
          'userid' => $userid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Return polls select list with some companyid
     * TODO: Change this as ajaxFormElementsByCompanyAction
     */
    public function ajaxFormElementsByCompanyAction()
    {
        $permission = FALSE;
        $userId = '';
        $companyId = '';
        $select_polls = '';
        $select_filters = '';
        
        if (isset($_GET['companyid'])) {
            $companyId = $_GET['companyid'];
        }
        if (isset($_GET['userid'])) {
            $userId = $_GET['userid'];
        }
        if (isset($_GET['permissionid'])) {
            // Recover Permission
            $permission = $this->getDoctrine()->getManager()->getRepository('FishmanAuthBundle:Permission')->find($_GET['permissionid']);
        }
        
        // Recovers Polls
        $result_polls = Poll::getListBenchmarkingPollsByCompany($this->getDoctrine(), $companyId, $userId, $permission);
        
        $select_polls .= '<select name="permission_polls" id="permission_polls" class="permission_pollselect" required >';
        $select_polls .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        if (!empty($result_polls)) {
            foreach ($result_polls as $entity_poll) {
                $select_polls .= '<option value="' . $entity_poll['id'] . '">' . $entity_poll['title'] . '</option>';
            }
        }
        $select_polls .= '</select>';
        
        // Recovers Filters by Type Company
        $result_filters = Company::getListFiltersByTypeCompany($this->getDoctrine(), $companyId);
        
        if (!empty($result_filters)) {
            foreach ($result_filters as $filter_key => $filter_value) {
                $select_filters .= '<option value="' . $filter_key . '">' . $filter_value . '</option>';
            }
        }
        
        $response = new Response(json_encode(
            array('polls' => $select_polls, 'filters' => $select_filters)
        ));
        
        return $response;
    }

    /**
     * Return pollschedulings select list with some pollid
     * TODO: Change this as ajaxFormElementsByPollAction
     */
    public function ajaxFormElementsByPollAction()
    {
        $permission = FALSE;
        $userId = '';
        $pollId = '';
        $companyId = '';
        $select_pollschedulings = '';
        
        if (isset($_GET['companyid'])) {
            $companyId = $_GET['companyid'];
        }
        if (isset($_GET['pollid'])) {
            $pollId = $_GET['pollid'];
        }
        if (isset($_GET['userid'])) {
            $userId = $_GET['userid'];
        }
        if (isset($_GET['permissionid'])) {
            // Recover Permission
            $permission = $this->getDoctrine()->getManager()->getRepository('FishmanAuthBundle:Permission')->find($_GET['permissionid']);
        }
        
        // Recovers Pollschedulings
        $result_pscs = Pollscheduling::getListBenchmarkingPollschedulingsByPoll($this->getDoctrine(), $companyId, $pollId, $userId, $permission);
        
        if (!empty($result_pscs)) {
            foreach ($result_pscs as $entity_psc) {
                $select_pollschedulings .= '<option value="' . $entity_psc['id'] . '">';
                $select_pollschedulings .= $entity_psc['key'] . '. ' . $entity_psc['psc_period'];
                $select_pollschedulings .= '</option>';
            }
        }
        
        $response = new Response(json_encode(
            array('pollschedulings' => $select_pollschedulings)
        ));
        
        return $response;
    }

    /**
     * Return conditional fields with some pollschedulingids
     * TODO: Change this as ajaxFormElementsByPollschedulingsAction
     */
    public function ajaxFormElementsByPollschedulingsAction()
    {
        $pscIds = 0;
        
        if ($_GET['pollschedulingids'] != '') {
            $pscIds = $_GET['pollschedulingids'];
        }
        
        // Recovers value
        $value = Pollscheduling::getValueBenchmarkingByPollschedulings($this->getDoctrine(), $pscIds);
        
        $response = new Response(json_encode(
            array('value' => $value)
        ));
        
        return $response;
    }

}
