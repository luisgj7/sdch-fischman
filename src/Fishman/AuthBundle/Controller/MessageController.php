<?php

namespace Fishman\AuthBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MessageController extends Controller
{
    public function indexAction($id)
    {

        switch($id){
            case 1:
                $message_title = 'SIN INFORMACIÓN LABORAL';
                $message = 'Actualmente no tiene una información laboral activa. Consulte con su administrador';                
                break;
            case 2:
                $message_title = 'SIN TALLER ACTIVO';
                $message = 'Actualmente no tiene un taller activo. Consulte con su administrador';
                break;
        }

        return $this->render('FishmanAuthBundle:Message:index.html.twig', array(
            'message_title' => $message_title,
            'message' => $message
        ));
    }
}
