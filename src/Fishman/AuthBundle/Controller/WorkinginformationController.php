<?php

namespace Fishman\AuthBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\AuthBundle\Form\WorkinginformationType;
use Fishman\AuthBundle\Entity\User;
use Fishman\EntityBundle\Entity\Company;
use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\EntityBundle\Entity\Companycharge;
use Fishman\EntityBundle\Entity\Companyfaculty;
use Fishman\EntityBundle\Entity\Companycareer;
use Fishman\EntityBundle\Entity\Headquarter;
use Fishman\EntityBundle\Entity\Organizationalunit;
use Fishman\EntityBundle\Entity\Charge;
use Fishman\EntityBundle\Entity\Faculty;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Workinginformation controller.
 *
 */
class WorkinginformationController extends Controller
{
    /**
     * Lists all Workinginformation entities.
     *
     */
    public function indexAction(Request $request, $userid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // User Info
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recovering data
        
        $company_options = Company::getListCompanyOptions($this->getDoctrine(), 'working');
        $organizationalunit_options = Organizationalunit::getListOrganizationalunitOptions($this->getDoctrine());
        $charge_options = Charge::getListChargeOptions($this->getDoctrine());
        $boss_options = Workinginformation::getListBossOptions($this->getDoctrine(), 'workinginformation');
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'company' => '', 
            'organizationalunit' => '', 
            'charge' => '', 
            'boss' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('company', 'choice', array(
                'choices' => $company_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('organizationalunit', 'choice', array(
                'choices' => $organizationalunit_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('charge', 'choice', array(
                'choices' => $charge_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('boss', 'choice', array(
                'choices' => $boss_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'company' => '', 
            'organizationalunit' => '', 
            'charge' => '', 
            'boss' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('wi.id, wi.code, c.name company, c.id companyid, cou.name companyorganizationalunit, cch.name companycharge, 
                      h.name headquarter, b.names boss_names, b.surname boss_surname, b.lastname boss_lastname, 
                      wi.changed, wi.status, u.names, u.surname, u.lastname')
            ->innerJoin('wi.company', 'c')
            ->leftJoin('wi.companyorganizationalunit', 'cou')
            ->leftJoin('wi.companycharge', 'cch')
            ->leftJoin('cou.organizationalunit', 'ou')
            ->leftJoin('cch.charge', 'ch')
            ->leftJoin('wi.headquarter', 'h')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wi.modified_by = u.id')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wb', 'WITH', 'wi.boss = wb.id')
            ->leftJoin('FishmanAuthBundle:User', 'b', 'WITH', 'wb.user = b.id')
            ->where('wi.user = :user 
                    AND wi.type = :type')
            ->andWhere('wi.id LIKE :id 
                        OR wi.code LIKE :code')
            ->setParameter('user', $userid)
            ->setParameter('type', 'workinginformation')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->orderBy('wi.id', 'ASC');
        
        // Add arguments
        
        $queryBuilder
            ->andWhere('wi.id <> 2');
        
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('wi.company = :company')
                ->setParameter('company', $data['company']);
        }
        if ($data['organizationalunit'] != '') {
            $queryBuilder
                ->andWhere('ou.id = :organizationalunit')
                ->setParameter('organizationalunit', $data['organizationalunit']);
        }
        if ($data['charge'] != '') {
            $queryBuilder
                ->andWhere('ch.id = :charge')
                ->setParameter('charge', $data['charge']);
        }
        if ($data['boss'] != '') {
            $queryBuilder
                ->andWhere('b.id = :boss')
                ->setParameter('boss', $data['boss']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('wi.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workinginformation');
        $paginator->setMaxPagerItems(5, 'workinginformation');
        $entities = $paginator->paginate($query, 'workinginformation')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workinginformation');
        $endPageItem = $paginator->getEndPageItem('workinginformation');
        $totalItems = $paginator->getTotalItems('workinginformation');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanAuthBundle:Workinginformation:index.html.twig', array(
            'entities' => $entities,
            'user' => $user,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Workinginformation entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('wi.id, c.name company, wi.code, wi.datein, wi.dateout, wi.email, wi.unit_type, co.name companyorganizationalunit, 
                      cc.name companycharge, h.name headquarter, u.id user_id, u.names, u.surname, u.lastname, b.names boss_names, 
                      b.surname boss_surname, b.lastname boss_lastname, wi.status, wi.created, wi.changed, wi.created_by, wi.modified_by')
            ->leftJoin('wi.companyorganizationalunit', 'co')
            ->leftJoin('wi.companycharge', 'cc')
            ->innerJoin('wi.company', 'c')
            ->leftJoin('wi.headquarter', 'h')
            ->innerJoin('wi.user', 'u')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wb', 'WITH', 'wi.boss = wb.id')
            ->leftJoin('FishmanAuthBundle:User', 'b', 'WITH', 'wb.user = b.id')
            ->where('wi.id = :workinginformation')
            ->setParameter('workinginformation', $id);
        
        $queryBuilder
            ->andWhere('wi.id <> 2');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información laboral.');
            return $this->redirect($this->generateUrl('user'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanAuthBundle:Workinginformation:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Workinginformation entity.
     *
     */
    public function newAction($userid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        $entity = new Workinginformation($this->getDoctrine());
        
        $entity->setType('workinginformation');
        $entity->setUser($user);
        $entity->setStatus(1);
        $form = $this->createForm(new WorkinginformationType($entity, 'workinginformation', $this->getDoctrine()));

        $selectBoss = '
            <select class="boss" name="workinginformation_boss">
                <option value="">NINGUNO</option>
            </select>';

        $selectCompanyOrganizationalUnit = '
            <select class="companyorganizationalunit" name="company_organization_unit">
                <option value="">SELECCIONE UNA OPCIÓN</option>
            </select>';
 
        $selectCompanyCharge = '
            <select class="companycharge" name="company_charge">
                <option value="">SELECCIONE UNA OPCIÓN</option>
            </select>';
 
        $selectHeadquarter = '
            <select class="headquarter" name="headquarter">
                <option value="">SELECCIONE UNA OPCIÓN</option>
            </select>';

        return $this->render('FishmanAuthBundle:Workinginformation:new.html.twig', array(
            'entity' => $entity,
            'user' => $user,
            'form' => $form->createView(),
            'selectBoss' => $selectBoss,
            'selectCompanyOrganizationalUnit' => $selectCompanyOrganizationalUnit,
            'selectCompanyCharge' => $selectCompanyCharge,
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Creates a new Workinginformation entity.
     *
     */
    public function createAction(Request $request, $userid)
    {   
        $entity  = new Workinginformation($this->getDoctrine());

        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }

        $postData = $request->request->get('fishman_authbundle_workinginformationtype', null);

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($postData['companyid']);
        $entity->setCompany($company);
        
        $form = $this->createForm(new WorkinginformationType($entity, 'workinginformation', $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            // Recover Datein and Dateout
            $datein = 0;
            $dateout = 0;
            if ($entity->getDateout() != '') {
                $datein = strtotime($entity->getDatein()->format('Y/m/d'));
            }
            if ($entity->getDateout() != '') {
                $dateout = strtotime($entity->getDateout()->format('Y/m/d'));
            }
            
            // get Code
            
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $resultCode = $repository->createQueryBuilder('wi')
                ->select('wi.code')
                ->where('wi.code = :code 
                        AND wi.company = :company 
                        AND wi.user <> :user')
                ->setParameter('code', $entity->getCode())
                ->setParameter('company', $entity->getCompany())
                ->setParameter('user', $userid)
                ->getQuery()
                ->getResult();
            
            $recordCode = current($resultCode);
            
            if (($datein < $dateout || $dateout == 0) && empty($recordCode)) {
              
                // suspend auto-commit
                $em->getConnection()->beginTransaction();

                // Try and make the transaction
                try {
                  
                    //Company Organizational Unit
                    $cou = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')
                              ->find($postData['companyorganizationalunit_id']);
                    if (!$cou) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar la unidad organizativa de la empresa.');
                        return $this->redirect($this->generateUrl('workinginformation', array(
                            'userid' => $userid
                        )));
                    }
                    $entity->setCompanyorganizationalunit($cou);
    
                    //Company Charge
                    $cc = $em->getRepository('FishmanEntityBundle:Companycharge')
                              ->find($postData['companycharge_id']);
                    if (!$cc) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar el cargo de la empresa.');
                        return $this->redirect($this->generateUrl('workinginformation', array(
                            'userid' => $userid
                        )));
                    }
                    $entity->setCompanycharge($cc);
    
                    //Headquarter
                    $h = $em->getRepository('FishmanEntityBundle:Headquarter')
                              ->find($postData['headquarter_id']);
                    if (!$h) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
                        return $this->redirect($this->generateUrl('workinginformation', array(
                            'userid' => $userid
                        )));
                    }
                    $entity->setHeadquarter($h);
    
                    //Boss property is in form element (Workinginformation)
    
                    // User
                    $userBy = $this->get('security.context')->getToken()->getUser();
                    $entity->setCreatedBy($userBy->getId());
                    $entity->setModifiedBy($userBy->getId());
                    
                    $entity->setType('workinginformation');
                    $entity->setUser($user);
                    $entity->setCreated(new \DateTime());
                    $entity->setChanged(new \DateTime());
                    
                    $em->persist($entity);
                    $em->flush();
                    
                    /*
                     * TODO: Quitar esta actualización en la tabla Authuser y quitar los campos de:
                     *  - company_id, 
                     *  - organizationalunid_id, 
                     *  - charge_id
                     */ 
                     
                    $query = $em->createQueryBuilder()
                        ->update('FishmanAuthBundle:User u')
                        ->set('u.company_id', ':company')
                        ->set('u.organizationalunit_id', ':organizationalunit')
                        ->set('u.charge_id', ':charge')
                        ->where('u.id = :user')
                        ->setParameter('company', $entity->getCompany())
                        ->setParameter('organizationalunit', $entity->getCompanyorganizationalunit())
                        ->setParameter('charge', $entity->getCompanycharge())
                        ->setParameter('user', $userid)
                        ->getQuery()
                        ->execute();
                    
                    //TODO: Make this a reuse code (we try it in WorkinginformationRepository but it doesn't work)
                    $em->createQueryBuilder()
                       ->update('FishmanAuthBundle:Workinginformation wi')
                       ->set('wi.status', 0)
                       ->where('wi.company = :company
                              AND wi.type = :type
                              AND wi.user = :user
                              AND wi.id <> :id')
                       ->setParameter('company', $entity->getCompany())
                       ->setParameter('type', 'workinginformation')
                       ->setParameter('user', $entity->getUser())
                       ->setParameter('id', $entity->getId())
                       ->getQuery()
                       ->execute();
    
                    // set flash messages
                    $session = $this->getRequest()->getSession();
                    $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('workinginformation_show', array(
                    'id' => $entity->getId()
                )));
            
            }
            elseif (empty($recordCode)) {
                $session->getFlashBag()->add('error', 'La Fecha de Ingreso debe ser menor a la Fecha de Cese.');
            }
        }
        
        $companyId = $entity->getCompany()->getId();
        
        //Boss
        $selectBoss = Workinginformation::getSelectUser(
            $this->getDoctrine(),
            $companyId,
            $user->getId(),
            $entity->getBoss()
        );
        
        //Company Organizational Unit
        $selectCompanyOrganizationalUnit = Companyorganizationalunit::selectByCompanyWidget(
            $this->getDoctrine(),
            $companyId,
            $entity->getUnitType(),
            $ignoreId = false,
            $postData['companyorganizationalunit_id']
        );
        
        //Company Charges
        $selectCompanyCharge = Companycharge::getSelectCompanycharge(
            $this->getDoctrine(),
            $companyId,
            $postData['companycharge_id']
        );
        
        //Headquarter
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $companyId,
            $postData['headquarter_id']
        );
        
        return $this->render('FishmanAuthBundle:Workinginformation:new.html.twig', array(
            'entity' => $entity,
            'user'   => $user,
            'form'   => $form->createView(),
            'selectBoss' => $selectBoss,
            'selectCompanyOrganizationalUnit' => $selectCompanyOrganizationalUnit,
            'selectCompanyCharge' => $selectCompanyCharge,
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Displays a form to edit an existing Workinginformation entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información laboral.');
            return $this->redirect($this->generateUrl('user'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new WorkinginformationType($entity, 'workinginformation', $this->getDoctrine()), $entity);
        $companyId = $entity->getCompany()->getId();

        //Boss
        
        $selectBoss = Workinginformation::getSelectUser(
            $this->getDoctrine(),
            $companyId,
            $entity->getUser()->getId(),
            $entity->getBoss()
        );
        
        //Company Organizational Unit
        
        if ($entity->getCompanyorganizationalunit()) {
            $ouId = $entity->getCompanyorganizationalunit()->getId();
        }
        else {
            $ouId = FALSE;
        }
        $selectCompanyOrganizationalUnit = Companyorganizationalunit::selectByCompanyWidget(
            $this->getDoctrine(),
            $companyId,
            $entity->getUnitType(),
            $ignoreId = false,
            $ouId
        );
        
        //Company Charges
        
        if ($entity->getCompanycharge()) {
            $chargeId = $entity->getCompanycharge()->getId();
        }
        else {
            $chargeId = FALSE;
        }
        $selectCompanyCharge = Companycharge::getSelectCompanycharge(
            $this->getDoctrine(),
            $companyId,
            $chargeId
        );
        
        //Headquarters
        
        if ($entity->getHeadquarter()) {
            $headquarterId = $entity->getHeadquarter()->getId();
        }
        else {
            $headquarterId = FALSE;
        }
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $companyId,
            $headquarterId
        );

        return $this->render('FishmanAuthBundle:Workinginformation:edit.html.twig', array(
            'entity' => $entity,
            'user' => $entity->getUser(),
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView(),
            'selectBoss' => $selectBoss,
            'selectCompanyOrganizationalUnit' => $selectCompanyOrganizationalUnit,
            'selectCompanyCharge' => $selectCompanyCharge,
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Edits an existing Workinginformation entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        $entity->setDoctrineRegistry($this->getDoctrine());

        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información laboral.');
            return $this->redirect($this->generateUrl('user'));
        }

        $postData = $request->request->get('fishman_authbundle_workinginformationtype', null);

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($postData['companyid']);
        $entity->setCompany($company);
        
        $editForm = $this->createForm(new WorkinginformationType($entity, 'workinginformation', $this->getDoctrine()), $entity);
        $editForm->bind($request);
                                           
        if ($editForm->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $entity->setCompany($company);
        
            // suspend auto-commit
            $em->getConnection()->beginTransaction();

            // Recover Datein and Dateout
            $datein = 0;
            $dateout = 0;
            if ($entity->getDateout() != '') {
                $datein = strtotime($entity->getDatein()->format('Y/m/d'));
            }
            if ($entity->getDateout() != '') {
                $dateout = strtotime($entity->getDateout()->format('Y/m/d'));
            }

            // set flash messages
            $session = $this->getRequest()->getSession();
            
            if ($datein < $dateout || $dateout == 0) {

                // Try and make the transaction
                try {
                    $postData = $request->request->get('fishman_authbundle_workinginformationtype', null);                
                    
                    //Company Organizational Unit
                    
                    $cou = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')
                              ->find($postData['companyorganizationalunit_id']);
                    if (!$cou) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar la unidad organizativa de la empresa.');
                        return $this->redirect($this->generateUrl('workinginformation', array(
                            'userid' => $userid
                        )));
                    }
                    $entity->setCompanyorganizationalunit($cou);
                    
                    //Company Charge
                    
                    $cc = $em->getRepository('FishmanEntityBundle:Companycharge')
                              ->find($postData['companycharge_id']);
                    if (!$cc) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar el cargo de la empresa.');
                        return $this->redirect($this->generateUrl('workinginformation', array(
                            'userid' => $userid
                        )));
                    }
                    $entity->setCompanycharge($cc);
                    
                    //Headquarter
                    
                    $h = $em->getRepository('FishmanEntityBundle:Headquarter')
                              ->find($postData['headquarter_id']);
                    if (!$h) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
                        return $this->redirect($this->generateUrl('workinginformation', array(
                            'userid' => $userid
                        )));
                    }
                    $entity->setHeadquarter($h);
    
                    //Boss property is in form element (workinginformation)
    
                    // User
                    $modifiedBy = $this->get('security.context')->getToken()->getUser();
                    $entity->setModifiedBy($modifiedBy->getId());
                    
                    $entity->setChanged(new \DateTime());
                    $entity->setType('workinginformation');
                    $em->persist($entity);
                    $em->flush();
                    
                    $query = $em->createQueryBuilder()
                        ->update('FishmanAuthBundle:User u')
                        ->set('u.company_id', ':company')
                        ->set('u.organizationalunit_id', ':organizationalunit')
                        ->set('u.charge_id', ':charge')
                        ->where('u.id = :user')
                        ->setParameter('company', $entity->getCompany())
                        ->setParameter('organizationalunit', $entity->getCompanyorganizationalunit())
                        ->setParameter('charge', $entity->getCompanycharge())
                        ->setParameter('user', $entity->getUser())
                        ->getQuery()
                        ->execute();
    
                    //TODO: Make this a reuse code (we try it in WorkinginformationRepository but it doesn't work)
                    $em->createQueryBuilder()
                       ->update('FishmanAuthBundle:Workinginformation wi')
                       ->set('wi.status', 0)
                       ->where('wi.company = :company
                              AND wi.type = :type
                              AND wi.user = :user
                              AND wi.id <> :id')
                       ->setParameter('company', $entity->getCompany())
                       ->setParameter('type', 'workinginformation')
                       ->setParameter('user', $entity->getUser())
                       ->setParameter('id', $entity->getId())
                       ->getQuery()
                       ->execute();
                    
                    // set flash messages
                    $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                return $this->redirect($this->generateUrl('workinginformation_show', array(
                    'id' => $id
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Ingreso debe ser menor a la Fecha de Cese.');
            }
        }

        $companyId = $entity->getCompany()->getId();
        
        //Boss
        $selectBoss = Workinginformation::getSelectUser(
            $this->getDoctrine(),
            $companyId,
            $entity->getUser()->getId(),
            $entity->getBoss()
        );

        //Company Organizational Unit
        $selectCompanyOrganizationalUnit = Companyorganizationalunit::selectByCompanyWidget(
            $this->getDoctrine(),
            $companyId,
            $entity->getUnitType(),
            $ignoreId = false,
            $entity->getCompanyorganizationalunit()->getId()
        );

        //Company Charges
        $selectCompanyCharge = Companycharge::getSelectCompanycharge(
            $this->getDoctrine(),
            $companyId,
            $entity->getCompanycharge()->getId()
        );
        
        //Headquarter
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $companyId,
            $postData['headquarter_id']
        );

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanAuthBundle:Workinginformation:edit.html.twig', array(
            'entity' => $entity,
            'user' => $entity->getUser(),
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView(),
            'selectBoss' => $selectBoss,
            'selectCompanyOrganizationalUnit' => $selectCompanyOrganizationalUnit,
            'selectCompanyCharge' => $selectCompanyCharge,
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Deletes a Workinginformation entity.
     *
     */
    public function deleteAction(Request $request, $id, $userid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
          
            $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
            if (!$entity || $userid == 2 || $id == 2) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la información laboral.');
                return $this->redirect($this->generateUrl('user'));
            }
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
            $psp = $repository->createQueryBuilder('psp')
                ->select('count(psp.workinginformation)')
                ->where('psp.workinginformation = :workinginformation')
                ->setParameter('workinginformation', $entity->getId())
                ->groupBy('psp.workinginformation')
                ->getQuery()
                ->getResult();
            
            if (current($psp) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La información laboral está en uso.');
            }
        }

        return $this->redirect($this->generateUrl('workinginformation', array(
          'userid' => $userid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop Workinginformation entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información laboral.');
            return $this->redirect($this->generateUrl('user'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanAuthBundle:Workinginformation:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    public function ajaxChoiceByCompanyAction()
    {
        $companyId = $_GET['company_id'];
        $ignoreUser = $_GET['ignore_user'];

        $repository = $this->getDoctrine()->getManager()->getRepository('FishmanAuthBundle:Workinginformation');
        $workinginformations = $repository->findActiveByCompany($companyId, $ignoreUser);

        $selectBoss = '<select class="boss" name="workinginformation_boss">';
        $selectBoss .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        foreach($workinginformations as $wi){
            $selectBoss .= '<option value="' . $wi['id'] . '">' . 
                               $wi['names'] . ' '  . $wi['surname']  . ' ' . $wi['lastname'] . 
                           '</option>';
        }
        $selectBoss .= '</select>';

        $response = new Response(json_encode(
            array('select_boss' => $selectBoss)));  
        return $response;
    }

    /**
     *
     * This information needs to refresh like select control.
     * This action get a json with the select controls (all in one request) in order to update form elements
     */
    public function ajaxFormElementsByCompanyAction()
    {
        $companyId = $_GET['company_id'];
        $unitType = $_GET['unittype'];
        $ignoreUser = false;
        
        if(isset($_GET['ignore_user'])){
            $ignoreUser = $_GET['ignore_user'];
        }
        
        //Boss
        $selectBoss = Workinginformation::getSelectUser($this->getDoctrine(), $companyId, $ignoreUser);
        
        if ($unitType) {
            //Company Organizational Unit
            $selectCompanyOrganizationalUnit = Companyorganizationalunit::selectByCompanyWidget($this->getDoctrine(), $companyId, $unitType, $ignoreId = false);
        }
        
        //Company Charges
        $selectCompanyCharge = Companycharge::getSelectCompanycharge($this->getDoctrine(), $companyId);
        
        //Headquarters
        $selectHeadquarter = Headquarter::getSelectHeadquarter($this->getDoctrine(), $companyId);
        
        //Array parameteres
        $array_parameters = array(
            'select_boss' => $selectBoss,
            'select_companycharge' => $selectCompanyCharge,
            'select_headquarter' => $selectHeadquarter
        );
        
        if ($unitType) {
            $array_parameters['select_companyorganizationalunit'] = $selectCompanyOrganizationalUnit;
        }
        
        $response = new Response(json_encode($array_parameters));
        
        return $response;
    }

    /**
     * 
     * This information needs to refresh like select control.
     * This action get a json with the select controls (all in one request) in order to update form elements by unittype
     */
    public function ajaxFormElementsByCompanyByUnitTypeAction()
    {
        $companyId = $_GET['company_id'];
        $unitType = $_GET['unittype'];
        
        //Company Organizational Unit
        $selectCompanyOrganizationalUnit = Companyorganizationalunit::selectByCompanyWidget($this->getDoctrine(), $companyId, $unitType, $ignoreId = false);
        
        $response = new Response(json_encode(
            array('select_companyorganizationalunit' => $selectCompanyOrganizationalUnit)
        ));  
        return $response;
    }

    /**
     * @return json Response with workinginformation from some company.
     *         It have label, id ... fields
     *         Consider only active workinginformations
     */
    public function ajaxJsonByCompanyAction()
    {
        $companyId = $_GET['company_id'];
        $ignoreUser = false;

        if(isset($_GET['ignore_user'])){
            $ignoreUser = $_GET['ignore_user'];
        }

        $repository = $this->getDoctrine()->getManager()->getRepository('FishmanAuthBundle:Workinginformation');
        $workinginformations = $repository->findActiveByCompany($companyId, $ignoreUser);
        $response = new Response(json_encode($workinginformations));  
        return $response;
    }

    /**
     * Los siguientes son metodos para Schoolinformation
     **/
    public function indexSchoolinformationAction(Request $request, $userid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // User Info
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recovering data
        
        $company_options = Company::getListCompanyOptions($this->getDoctrine(), 'school');
        $grade_options = Workinginformation::getListGradeOptions();
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'company' => '', 
            'grade' => '', 
            'study_year' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('company', 'choice', array(
                'choices' => $company_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('grade', 'choice', array(
                'choices' => $grade_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('study_year', 'text', array(
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'company' => '', 
            'grade' => '', 
            'study_year' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('wi.id, wi.code, c.name company, c.id companyid, wi.grade, wi.study_year, h.name headquarter, 
                      wi.changed, wi.status, u.names, u.surname, u.lastname')
            ->innerJoin('wi.company', 'c')
            ->leftJoin('wi.headquarter', 'h')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wi.modified_by = u.id')
            ->where('wi.user = :user 
                    AND wi.type = :type')
            ->andWhere('wi.id LIKE :id 
                        OR wi.code LIKE :code')
            ->setParameter('user', $userid)
            ->setParameter('type', 'schoolinformation')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->orderBy('wi.id', 'ASC');
        
        // Add arguments
        
        $queryBuilder
            ->andWhere('wi.id <> 2');
        
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('wi.company = :company')
                ->setParameter('company', $data['company']);
        }
        if ($data['grade'] != '') {
            $queryBuilder
                ->andWhere('wi.grade = :grade')
                ->setParameter('grade', $data['grade']);
        }
        if ($data['study_year'] != '') {
            $queryBuilder
                ->andWhere('wi.study_year LIKE :study_year')
                ->setParameter('study_year', '%' . $data['study_year'] . '%');
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('wi.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'schoolinformation');
        $paginator->setMaxPagerItems(5, 'schoolinformation');
        $entities = $paginator->paginate($query, 'schoolinformation')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('schoolinformation');
        $endPageItem = $paginator->getEndPageItem('schoolinformation');
        $totalItems = $paginator->getTotalItems('schoolinformation');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanAuthBundle:Schoolinformation:index.html.twig', array(
            'entities' => $entities,
            'user' => $user,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * 
     * Finds and displays a Schoolinformation entity.
     */
    public function showSchoolinformationAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('wi.id, c.name company, wi.code, wi.email, wi.grade, wi.study_year, h.name headquarter, u.id user_id, 
                      u.names, u.surname, u.lastname,  wi.status, wi.created, wi.changed, wi.created_by, wi.modified_by')
            ->innerJoin('wi.company', 'c')
            ->leftJoin('wi.headquarter', 'h')
            ->innerJoin('wi.user', 'u')
            ->where('wi.id = :workinginformation')
            ->setParameter('workinginformation', $id);
        
        $queryBuilder
            ->andWhere('wi.id <> 2');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información de colegio.');
            return $this->redirect($this->generateUrl('user'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanAuthBundle:Schoolinformation:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Schoolinformation entity.
     *
     */
    public function newSchoolinformationAction($userid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        $entity = new Workinginformation($this->getDoctrine());
        $entity->setType('schoolinformation');
        $entity->setUser($user);
        $entity->setStatus(1);
        $form = $this->createForm(new WorkinginformationType($entity, 'schoolinformation', $this->getDoctrine() ));
 
        $selectHeadquarter = '
            <select class="headquarter" name="headquarter">
                <option value="">SELECCIONE UNA OPCIÓN</option>
            </select>';

        return $this->render('FishmanAuthBundle:Schoolinformation:new.html.twig', array(
            'entity' => $entity,
            'user' => $user,
            'form' => $form->createView(),
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Creates a new Schoolinformation entity.
     *
     */
    public function createSchoolinformationAction(Request $request, $userid)
    {   
        $entity  = new Workinginformation($this->getDoctrine());
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        $postData = $request->request->get('fishman_authbundle_workinginformationtype', null);
                        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($postData['companyid']);
        $entity->setCompany($company);
        
        $form = $this->createForm(new WorkinginformationType($entity, 'schoolinformation', $this->getDoctrine()), $entity);
        $form->bind($request);
                    
        if ($form->isValid()) {
            
            //Headquarter
            $h = $em->getRepository('FishmanEntityBundle:Headquarter')
                      ->find($postData['headquarter_id']);
            if (!$h) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
                return $this->redirect($this->generateUrl('schoolinformation', array(
                    'userid' => $userid
                )));
            }
            $entity->setHeadquarter($h);

            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setCompany($company);
            $entity->setType('schoolinformation');
            if ($postData['school_email'] != '') {
                $entity->setEmail($postData['school_email']);
            }
            $entity->setUser($user);
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
                    
            //TODO: Make this a reuse code (we try it in WorkinginformationRepository but it doesn't work)
            $em->createQueryBuilder()
               ->update('FishmanAuthBundle:Workinginformation wi')
               ->set('wi.status', 0)
               ->where('wi.company = :company
                      AND wi.type = :type
                      AND wi.user = :user
                      AND wi.id <> :id')
               ->setParameter('company', $entity->getCompany())
               ->setParameter('type', 'schoolinformation')
               ->setParameter('user', $entity->getUser())
               ->setParameter('id', $entity->getId())
               ->getQuery()
               ->execute();
          
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('schoolinformation_show', array(
                'id' => $entity->getId()
            )));
            
        }
        
        $companyId = $entity->getCompany()->getId();
        
        //Headquarter
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $companyId,
            $postData['headquarter_id']
        );

        return $this->render('FishmanAuthBundle:Schoolinformation:new.html.twig', array(
            'entity' => $entity,
            'user'   => $user,
            'form'   => $form->createView(),
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Displays a form to edit an existing Schoolinformation entity.
     *
     */
    public function editSchoolinformationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información de colegio.');
            return $this->redirect($this->generateUrl('user'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new WorkinginformationType($entity, 'schoolinformation', $this->getDoctrine()), $entity);
        
        $companyId = $entity->getCompany()->getId();
        
        // Headquarters
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $companyId,
            $entity->getHeadquarter()->getId()
        );

        return $this->render('FishmanAuthBundle:Schoolinformation:edit.html.twig', array(
            'entity' => $entity,
            'user' => $entity->getUser(),
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName, 
            'edit_form'   => $editForm->createView(),
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Edits an existing Schoolinformation entity.
     *
     */
    public function updateSchoolinformationAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        $entity->setDoctrineRegistry($this->getDoctrine());
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información de colegio.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        $postData = $request->request->get('fishman_authbundle_workinginformationtype', null);
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($postData['companyid']);
        $entity->setCompany($company);
        
        $editForm = $this->createForm(new WorkinginformationType($entity, 'schoolinformation', $this->getDoctrine()), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            
            // suspend auto-commit
            $em->getConnection()->beginTransaction();
            
            // set flash messages
            $session = $this->getRequest()->getSession();
            
            // Try and make the transaction
            try {
                
                //Headquarter
                $h = $em->getRepository('FishmanEntityBundle:Headquarter')
                          ->find($postData['headquarter_id']);
                if (!$h) {
                    $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
                    return $this->redirect($this->generateUrl('schoolinformation', array(
                        'userid' => $userid
                    )));
                }
                $entity->setHeadquarter($h);              
                
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                $entity->setCompany($company);
                $entity->setType('schoolinformation');
                $entity->setEmail($postData['school_email']);
                $entity->setModifiedBy($modifiedBy->getId());
                $entity->setChanged(new \DateTime());
                $em->persist($entity);
                $em->flush();
                
                //TODO: Make this a reuse code (we try it in WorkinginformationRepository but it doesn't work)     
                $em->createQueryBuilder()
                   ->update('FishmanAuthBundle:Workinginformation wi')
                   ->set('wi.status', 0)
                   ->where('wi.company = :company
                          AND wi.type = :type
                          AND wi.user = :user
                          AND wi.id <> :id')
                   ->setParameter('company', $entity->getCompany())
                   ->setParameter('type', 'schoolinformation')
                   ->setParameter('user', $entity->getUser())
                   ->setParameter('id', $entity->getId())
                   ->getQuery()
                   ->execute();
                
                // set flash messages
                $session = $this->getRequest()->getSession();
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
                
                // Try and commit the transactioncurrentversioncurrentversion
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                throw $e;
            }
                                         
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('schoolinformation_show', array(
                'id' => $id
            )));
            
        }

        $companyId = $entity->getCompany()->getId();
        
        //Headquarter
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $companyId,
            $postData['headquarter_id']
        );

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanAuthBundle:Schoolinformation:edit.html.twig', array(
            'entity' => $entity,
            'user' => $entity->getUser(),
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView(),
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Deletes a Schoolinformation entity.
     *
     */
    public function deleteSchoolinformationAction(Request $request, $id, $userid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
            if (!$entity || $userid == 2 || $id == 2) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la información de colegio.');
                return $this->redirect($this->generateUrl('schoolinformation', array(
                    'userid' => $userid
                )));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
            $psp = $repository->createQueryBuilder('psp')
                ->select('count(psp.workinginformation)')
                ->where('psp.workinginformation = :workinginformation')
                ->setParameter('workinginformation', $entity->getId())
                ->groupBy('psp.workinginformation')
                ->getQuery()
                ->getResult();
            
            if (current($psp) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La información colegio está en uso.');
            }
        }

        return $this->redirect($this->generateUrl('schoolinformation', array(
          'userid' => $userid,
        )));
    }

    /**
     * Drop Schoolinformation entity.
     *
     */
    public function dropSchoolinformationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información de colegio.');
            return $this->redirect($this->generateUrl('user'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanAuthBundle:Schoolinformation:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     *
     * This information needs to refresh like select control.
     * This action get a json with the select controls (all in one request) in order to update form elements
     */
    public function ajaxFormElementsBySchoolAction()
    {
        $companyId = $_GET['company_id'];
        
        //Headquarters
        $selectHeadquarter = Headquarter::getSelectHeadquarter($this->getDoctrine(), $companyId);
        
        $response = new Response(json_encode(array(
            'select_headquarter' => $selectHeadquarter
        )));
        
        return $response;
    }

    /**
     * Los siguientes son metodos para Universityinformation
     **/
    public function indexUniversityinformationAction(Request $request, $userid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // User Info
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        // Recovering data
        
        $company_options = Company::getListCompanyOptions($this->getDoctrine(), 'university');
        $faculty_options = Faculty::getListFacultyOptions($this->getDoctrine());
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'company' => '', 
            'faculty' => '', 
            'companycareer' => '', 
            'academic_year', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('company', 'choice', array(
                'choices' => $company_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('faculty', 'choice', array(
                'choices' => $faculty_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('companycareer', 'text', array(
                'required' => FALSE
            ))
            ->add('academic_year', 'text', array(
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'company' => '', 
            'faculty' => '', 
            'companycareer' => '', 
            'academic_year', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('wi.id, wi.code, c.name company, c.id companyid, cf.name companyfaculty, ccr.name companycareer, 
                      wi.academic_year, h.name headquarter, wi.changed, wi.status, u.names, u.surname, u.lastname')
            ->innerJoin('wi.company', 'c')
            ->leftJoin('wi.companyfaculty', 'cf')
            ->leftJoin('wi.companycareer', 'ccr')
            ->leftJoin('wi.headquarter', 'h')
            ->leftJoin('cf.faculty', 'f')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wi.modified_by = u.id')
            ->where('wi.user = :user 
                    AND wi.type = :type')
            ->andWhere('wi.id LIKE :id 
                        OR wi.code LIKE :code')
            ->setParameter('user', $userid)
            ->setParameter('type', 'universityinformation')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->orderBy('wi.id', 'ASC');
        
        // Add arguments;
        
        $queryBuilder
            ->andWhere('wi.id <> 2');
        
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('wi.company = :company')
                ->setParameter('company', $data['company']);
        }
        if ($data['faculty'] != '') {
            $queryBuilder
                ->andWhere('f.id = :faculty')
                ->setParameter('faculty', $data['faculty']);
        }
        if ($data['companycareer'] != '') {
            $queryBuilder
                ->andWhere('ccr.name LIKE :companycareer')
                ->setParameter('companycareer', '%' . $data['companycareer'] . '%');
        }
        if ($data['academic_year'] != '') {
            $queryBuilder
                ->andWhere('wi.academic_year LIKE :academic_year')
                ->setParameter('academic_year', '%' . $data['academic_year'] . '%');
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('wi.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'universityinformation');
        $paginator->setMaxPagerItems(5, 'universityinformation');
        $entities = $paginator->paginate($query, 'universityinformation')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('universityinformation');
        $endPageItem = $paginator->getEndPageItem('universityinformation');
        $totalItems = $paginator->getTotalItems('universityinformation');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanAuthBundle:Universityinformation:index.html.twig', array(
            'entities' => $entities,
            'user' => $user,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * 
     * Finds and displays a Universityinformation entity.
     */
    public function showUniversityinformationAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('wi.id, c.name company, wi.code, wi.email, cf.name companyfaculty, ccr.name companycareer, 
                      wi.academic_year, h.name headquarter, u.id user_id, u.names, u.surname, u.lastname, 
                      wi.status, wi.created, wi.changed, wi.created_by, wi.modified_by')
            ->innerJoin('wi.company', 'c')
            ->leftJoin('wi.companyfaculty', 'cf')
            ->leftJoin('wi.companycareer', 'ccr')
            ->leftJoin('wi.headquarter', 'h')
            ->innerJoin('wi.user', 'u')
            ->where('wi.id = :workinginformation')
            ->setParameter('workinginformation', $id);;
        
        $queryBuilder
            ->andWhere('wi.id <> 2');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información de universidad.');
            return $this->redirect($this->generateUrl('user'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanAuthBundle:Universityinformation:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Universityinformation entity.
     *
     */
    public function newUniversityinformationAction($userid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        $entity = new Workinginformation($this->getDoctrine());
        $entity->setType('universityinformation');
        $entity->setUser($user);
        $entity->setStatus(1);
        $form = $this->createForm(new WorkinginformationType($entity, 'universityinformation', $this->getDoctrine()));
        
        $selectCompanyfaculty = '
            <select class="companyfaculty" name="company_faculty">
                <option value="">SELECCIONE UNA OPCIÓN</option>
            </select>';
        
        $selectCompanycareer = '
            <select class="companycareer" name="company_career">
                <option value="">SELECCIONE UNA OPCIÓN</option>
            </select>';
        
        $selectHeadquarter = '
            <select class="headquarter" name="headquarter">
                <option value="">SELECCIONE UNA OPCIÓN</option>
            </select>';

        return $this->render('FishmanAuthBundle:Universityinformation:new.html.twig', array(
            'entity' => $entity,
            'user' => $user,
            'form' => $form->createView(),
            'selectCompanyfaculty' => $selectCompanyfaculty,
            'selectCompanycareer' => $selectCompanycareer,
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Creates a new Universityinformation entity.
     *
     */
    public function createUniversityinformationAction(Request $request, $userid)
    {   
        $entity  = new Workinginformation($this->getDoctrine());
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $user = $em->getRepository('FishmanAuthBundle:User')->find($userid);
        if (!$user || $userid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar al usuario.');
            return $this->redirect($this->generateUrl('user'));
        }
        
        $postData = $request->request->get('fishman_authbundle_workinginformationtype', null);
                        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($postData['companyid']);
        $entity->setCompany($company);
        
        $form = $this->createForm(new WorkinginformationType($entity, 'universityinformation', $this->getDoctrine()), $entity);
        $form->bind($request);
                    
        if ($form->isValid()) {
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            // Companyfaculty
            $cf = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($postData['companyfaculty_id']);
            if (!$cf) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la facultad de la empresa.');
                return $this->redirect($this->generateUrl('universityinformation', array(
                    'userid' => $userid
                )));
            }
            $entity->setCompanyfaculty($cf);
            
            // Companycareer
            $ccr = $em->getRepository('FishmanEntityBundle:Companycareer')->find($postData['companycareer_id']);
            if (!$ccr) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la carrera de la empresa.');
                return $this->redirect($this->generateUrl('universityinformation', array(
                    'userid' => $userid
                )));
            }
            $entity->setCompanycareer($ccr);
            
            // Headquarter
            $h = $em->getRepository('FishmanEntityBundle:Headquarter')->find($postData['headquarter_id']);
            if (!$h) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
                return $this->redirect($this->generateUrl('universityinformation', array(
                    'userid' => $userid
                )));
            }
            $entity->setHeadquarter($h);
            
            $entity->setUser($user);
            $entity->setCompany($company);
            $entity->setType('universityinformation');
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $em->createQueryBuilder()
               ->update('FishmanAuthBundle:Workinginformation wi')
               ->set('wi.status', 0)
               ->where('wi.company = :company
                      AND wi.type = :type
                      AND wi.user = :user
                      AND wi.id <> :id')
               ->setParameter('company', $entity->getCompany())
               ->setParameter('type', 'universityinformation')
               ->setParameter('user', $entity->getUser())
               ->setParameter('id', $entity->getId())
               ->getQuery()
               ->execute();
          
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('universityinformation_show', array(
                'id' => $entity->getId()
            )));
            
        }
        
        // Companyfaculty
        $selectCompanyfaculty = Companyfaculty::getSelectCompanyfaculty(
            $this->getDoctrine(),
            $postData['companyid'],
            $postData['companyfaculty_id']
        );
        
        // Companycareer
        $selectCompanycareer = Companycareer::getSelectCompanycareer(
            $this->getDoctrine(),
            $postData['companyfaculty_id'],
            $postData['companycareer_id']
        );
        
        // Headquarter
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $postData['companyid'],
            $postData['headquarter_id']
        );

        return $this->render('FishmanAuthBundle:Universityinformation:new.html.twig', array(
            'entity' => $entity,
            'user' => $user,
            'form' => $form->createView(),
            'selectCompanyfaculty' => $selectCompanyfaculty,
            'selectCompanycareer' => $selectCompanycareer,
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Displays a form to edit an existing Universityinformation entity.
     *
     */
    public function editUniversityinformationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información de universidad.');
            return $this->redirect($this->generateUrl('user'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new WorkinginformationType($entity, 'universityinformation', $this->getDoctrine()), $entity);
        
        $companyId = $entity->getCompany()->getId();
        
        // Companyfaculty
        $selectCompanyfaculty = Companyfaculty::getSelectCompanyfaculty(
            $this->getDoctrine(),
            $companyId,
            $entity->getCompanyfaculty()->getId()
        );
        
        // Companycareer
        $selectCompanycareer = Companycareer::getSelectCompanycareer(
            $this->getDoctrine(),
            $entity->getCompanyfaculty()->getId(),
            $entity->getCompanycareer()->getId()
        );
        
        // Headquarter
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $companyId,
            $entity->getHeadquarter()->getId()
        );

        return $this->render('FishmanAuthBundle:Universityinformation:edit.html.twig', array(
            'entity' => $entity,
            'user' => $entity->getUser(),
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView(),
            'selectCompanyfaculty' => $selectCompanyfaculty,
            'selectCompanycareer' => $selectCompanycareer,
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Edits an existing Universityinformation entity.
     *
     */
    public function updateUniversityinformationAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        $entity->setDoctrineRegistry($this->getDoctrine());
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información de universidad.');
            return $this->redirect($this->generateUrl('user'));
        }

        $postData = $request->request->get('fishman_authbundle_workinginformationtype', null);

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($postData['companyid']);
        
        $entity->setCompany($company);
        
        $editForm = $this->createForm(new WorkinginformationType($entity, 'universityinformation', $this->getDoctrine()), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            
            // suspend auto-commit
            $em->getConnection()->beginTransaction();
            
            // Try and make the transaction
            try {
                
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                // Companyfaculty
                $cf = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($postData['companyfaculty_id']);
                if (!$cf) {
                    $session->getFlashBag()->add('error', 'No se puede encontrar la facultad de la empresa.');
                    return $this->redirect($this->generateUrl('universityinformation', array(
                        'userid' => $userid
                    )));
                }
                $entity->setCompanyfaculty($cf);
                
                // Companycareer
                $ccr = $em->getRepository('FishmanEntityBundle:Companycareer')->find($postData['companycareer_id']);
                if (!$ccr) {
                    $session->getFlashBag()->add('error', 'No se puede encontrar la carrera de la empresa.');
                    return $this->redirect($this->generateUrl('universityinformation', array(
                        'userid' => $userid
                    )));
                }
                $entity->setCompanycareer($ccr);
                
                // Headquarter
                $h = $em->getRepository('FishmanEntityBundle:Headquarter')->find($postData['headquarter_id']);
                if (!$h) {
                    $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
                    return $this->redirect($this->generateUrl('universityinformation', array(
                        'userid' => $userid
                    )));
                }
                $entity->setHeadquarter($h);  
                
                $entity->setCompany($company);
                $entity->setType('universityinformation');
                $entity->setModifiedBy($modifiedBy->getId());
                $entity->setChanged(new \DateTime());
                
                $em->persist($entity);
                $em->flush();
                
                //TODO: Make this a reuse code (we try it in WorkinginformationRepository but it doesn't work)     
                $em->createQueryBuilder()
                   ->update('FishmanAuthBundle:Workinginformation wi')
                   ->set('wi.status', 0)
                   ->where('wi.company = :company
                          AND wi.type = :type
                          AND wi.user = :user
                          AND wi.id <> :id')
                   ->setParameter('company', $entity->getCompany())
                   ->setParameter('type', 'universityinformation')
                   ->setParameter('user', $entity->getUser())
                   ->setParameter('id', $entity->getId())
                   ->getQuery()
                   ->execute();
                
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
                
                // Try and commit the transactioncurrentversioncurrentversion
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                throw $e;
            }

            return $this->redirect($this->generateUrl('universityinformation_show', array(
                'id' => $id
            )));
            
        }
        
        // Companyfaculty
        $selectCompanyfaculty = Companyfaculty::getSelectCompanyfaculty(
            $this->getDoctrine(),
            $postData['companyid'],
            $postData['companyfaculty_id']
        );
        
        // Companycareer
        $selectCompanycareer = Companycareer::getSelectCompanycareer(
            $this->getDoctrine(),
            $postData['companyfaculty_id'],
            $postData['companycareer_id']
        );
        
        // Headquarter
        $selectHeadquarter = Headquarter::getSelectHeadquarter(
            $this->getDoctrine(),
            $postData['companyid'],
            $postData['headquarter_id']
        );

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanAuthBundle:Universityinformation:edit.html.twig', array(
            'entity' => $entity,
            'user' => $entity->getUser(),
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView(),
            'selectCompanyfaculty' => $selectCompanyfaculty,
            'selectCompanycareer' => $selectCompanycareer,
            'selectHeadquarter' => $selectHeadquarter
        ));
    }

    /**
     * Deletes a Universityinformation entity.
     *
     */
    public function deleteUniversityinformationAction(Request $request, $id, $userid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
            if (!$entity || $userid == 2 || $id == 2) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la información de universidad.');
                return $this->redirect($this->generateUrl('universityinformation', array(
                    'userid' => $userid
                )));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
            $psp = $repository->createQueryBuilder('psp')
                ->select('count(psp.workinginformation)')
                ->where('psp.workinginformation = :workinginformation')
                ->setParameter('workinginformation', $entity->getId())
                ->groupBy('psp.workinginformation')
                ->getQuery()
                ->getResult();
            
            if (current($psp) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La información colegio está en uso.');
            }
        }

        return $this->redirect($this->generateUrl('universityinformation', array(
          'userid' => $userid,
        )));
    }

    /**
     * Drop Universityinformation entity.
     *
     */
    public function dropUniversityinformationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información de universidad.');
            return $this->redirect($this->generateUrl('user'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanAuthBundle:Universityinformation:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     *
     * This information needs to refresh like select control.
     * This action get a json with the select controls (all in one request) in order to update form elements
     */
    public function ajaxFormElementsByUniversityAction()
    {
        $companyId = $_GET['company_id'];
        
        // Companyfaculties
        $selectCompanyfaculty = Companyfaculty::getSelectCompanyfaculty($this->getDoctrine(), $companyId);
        
        // Companycareers
        $selectCompanycareer = '
            <select class="companycareer" name="company_career">
                <option value="">SELECCIONE UNA OPCIÓN</option>
            </select>';
        
        // Headquarters
        $selectHeadquarter = Headquarter::getSelectHeadquarter($this->getDoctrine(), $companyId);
        
        $response = new Response(json_encode(array(
            'select_companyfaculty' => $selectCompanyfaculty,
            'select_companycareer' => $selectCompanycareer, 
            'select_headquarter' => $selectHeadquarter
        )));
        
        return $response;
    }

    /**
     *
     * This information needs to refresh like select control.
     * This action get a json with the select controls (all in one request) in order to update form elements
     */
    public function ajaxFormElementsByCompanyfacultyAction()
    {
        $facultyId = $_GET['faculty_id'];
        
        // Companycareers
        $selectCompanycareer = Companycareer::getSelectCompanycareer($this->getDoctrine(), $facultyId);
        
        $response = new Response(json_encode(array(
            'select_companycareer' => $selectCompanycareer
        )));
        
        return $response;
    }

}
