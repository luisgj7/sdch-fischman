<?php

namespace Fishman\AuthBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\AuthBundle\Entity\Permission
 */
class Permission
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $company_id
     */
    private $company_id;

    /**
     * @var integer $poll_id
     */
    private $poll_id;

    /**
     * @var array $pollschedulings
     */
    private $pollschedulings;

    /**
     * @var boolean $benchmarking_internal
     */
    private $benchmarking_internal;

    /**
     * @var boolean $benchmarking_external
     */
    private $benchmarking_external;

    /**
     * @var array $filters
     */
    private $filters;

    /**
     * @var \DateTime $initdate
     */
    private $initdate;

    /**
     * @var \DateTime $enddate
     */
    private $enddate;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\AuthBundle\Entity\User", inversedBy="permissions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    public function __toString()
    {
         return $this->company_id;
    } 


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company_id
     *
     * @param integer $companyId
     * @return Permission
     */
    public function setCompanyId($companyId)
    {
        $this->company_id = $companyId;
    
        return $this;
    }

    /**
     * Get company_id
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * Set poll_id
     *
     * @param integer $pollId
     * @return Permission
     */
    public function setPollId($pollId)
    {
        $this->poll_id = $pollId;
    
        return $this;
    }

    /**
     * Get poll_id
     *
     * @return integer 
     */
    public function getPollId()
    {
        return $this->poll_id;
    }

    /**
     * Set pollschedulings
     *
     * @param array $pollschedulings
     * @return Permission
     */
    public function setPollschedulings($pollschedulings)
    {
        $this->pollschedulings = $pollschedulings;
    
        return $this;
    }

    /**
     * Get pollschedulings
     *
     * @return array 
     */
    public function getPollschedulings()
    {
        return $this->pollschedulings;
    }

    /**
     * Set benchmarking_internal
     *
     * @param boolean $benchmarkingInternal
     * @return Permission
     */
    public function setBenchmarkingInternal($benchmarkingInternal)
    {
        $this->benchmarking_internal = $benchmarkingInternal;
    
        return $this;
    }

    /**
     * Get benchmarking_internal
     *
     * @return boolean 
     */
    public function getBenchmarkingInternal()
    {
        return $this->benchmarking_internal;
    }

    /**
     * Set benchmarking_external
     *
     * @param boolean $benchmarkingExternal
     * @return Permission
     */
    public function setBenchmarkingExternal($benchmarkingExternal)
    {
        $this->benchmarking_external = $benchmarkingExternal;
    
        return $this;
    }

    /**
     * Get benchmarking_external
     *
     * @return boolean 
     */
    public function getBenchmarkingExternal()
    {
        return $this->benchmarking_external;
    }

    /**
     * Set filters
     *
     * @param array $filters
     * @return Permission
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    
        return $this;
    }

    /**
     * Get filters
     *
     * @return array 
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Set initdate
     *
     * @param \DateTime $initdate
     * @return Permission
     */
    public function setInitdate($initdate)
    {
        $this->initdate = $initdate;
    
        return $this;
    }

    /**
     * Get initdate
     *
     * @return \DateTime 
     */
    public function getInitdate()
    {
        return $this->initdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return Permission
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Permission
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Permission
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Permission
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Permission
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Permission
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set user
     *
     * @param Fishman\AuthBundle\Entity\User $user
     * @return Permission
     */
    public function setUser(\Fishman\AuthBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return Fishman\AuthBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get Pollschedulings Permission Assigneds User by Company
     */
    public static function getPollschedulingPermissionAssignedsOfPermissionId(DoctrineRegistry $doctrineRegistry, $userId, $permission)
    {
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanAuthBundle:Permission');
        $result = $repository->createQueryBuilder('pr')
            ->select('pr.pollschedulings')
            ->innerJoin('pr.user', 'u')
            ->where('pr.user = :user 
                    AND pr.status  = 1')
            ->setParameter('user', $userId)
            ->orderBy('pr.id', 'ASC')
            ->getquery()
            ->getResult();
        
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $record) {
                foreach ($record['pollschedulings'] as $psc_key => $psc_value) {
                    if ($permission) {
                        if ($permission->getId() != '') {
                            foreach ($permission->getPollschedulings() as $pscId) {
                                if ($pscId == $psc_value) {
                                    unset($record['pollschedulings'][$psc_key]);
                                }
                            }
                        }
                    }
                }
                foreach ($record['pollschedulings'] as $psc_key => $psc_value) {
                    if ($i == 0) {
                        $output = $psc_value;
                    }
                    else {
                        $output .= ', ' . $psc_value;
                    }
                    $i++;
                }
            }
        }
        
        return $output;
        
    }

    /**
     * Check Permission to Pollscheduling
     */
    public static function checkPermissionToPollschedulingBenchmarking(DoctrineRegistry $doctrineRegistry, $userId, $typeBenchmarking, $date)
    {
        $output = FALSE;
        
        $repository = $doctrineRegistry->getRepository('FishmanAuthBundle:Permission');
        $queryBuilder = $repository->createQueryBuilder('pe')
            ->select('pe.id, pe.initdate, pe.enddate')
            ->where("pe.user = :user
                    AND pe.initdate <= :datetoinit 
                    AND DATE_ADD(pe.enddate, 1, 'day') >= :datetoend
                    AND pe.status = 1")
            ->setParameter('user', $userId)
            ->setParameter('datetoinit', $date)
            ->setParameter('datetoend', $date);
            
        if ($typeBenchmarking == 'internal') {
            $queryBuilder
                ->andWhere('pe.benchmarking_internal = 1');
        }
        elseif ($typeBenchmarking == 'external') {
            $queryBuilder
                ->andWhere('pe.benchmarking_external = 1');
        }
        
        $query = $queryBuilder->getQuery()->getResult();
        
        if (!empty($query)) {
            $output = TRUE;
        }
        
        return $output;
    }
}