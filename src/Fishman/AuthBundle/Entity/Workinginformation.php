<?php

namespace Fishman\AuthBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Symfony\Component\Validator\ExecutionContext;

/**
 * Fishman\AuthBundle\Entity\Workinginformation
 */
class Workinginformation
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var \DateTime $datein
     */
    private $datein;

    /**
     * @var \DateTime $dateout
     */
    private $dateout;

    /**
     * @var string $email
     */
    private $email;

    /**
     * @var string $unit_type
     */
    private $unit_type;

    /**
     * @var integer $boss
     */
    private $boss;

    /**
     * @var string $grade
     */
    private $grade;

    /**
     * @var string $study_year
     */
    private $study_year;

    /**
     * @var string $academic_year
     */
    private $academic_year;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Companycharge", inversedBy="workinginformations")
     * @ORM\JoinColumn(name="companycharge_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Company", inversedBy="workinginformations")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Companyorganizationalunit", inversedBy="workinginformations")
     * @ORM\JoinColumn(name="companyorganizationalunit_id", referencedColumnName="id")
     */
    protected $companyorganizationalunit;
    
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Companycharge", inversedBy="workinginformations")
     * @ORM\JoinColumn(name="companycharge_id", referencedColumnName="id")
     */
    protected $companycharge;
    
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Companyfaculty", inversedBy="workinginformations")
     * @ORM\JoinColumn(name="companyfaculty_id", referencedColumnName="id")
     */
    protected $companyfaculty;
    
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Companycareer", inversedBy="workinginformations")
     * @ORM\JoinColumn(name="companycareer_id", referencedColumnName="id")
     */
    protected $companycareer;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\AuthBundle\Entity\Headquarter", inversedBy="workinginformations")
     * @ORM\JoinColumn(name="headquarter_id", referencedColumnName="id")
     */
    protected $headquarter;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopapplication", mappedBy="workinginformation")
     */
    protected $worshopapplications; 

    protected $doctrineRegistry;

    protected $pollschedulingpeoples; 

    /**
     * Constructor
     */
    public function __construct(DoctrineRegistry $doctrineRegistry)
    {
        $this->doctrineRegistry = $doctrineRegistry;
        $this->workshopapplications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Workinginformation
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set datein
     *
     * @param \DateTime $datein
     * @return Workinginformation
     */
    public function setDatein($datein)
    {
        $this->datein = $datein;
    
        return $this;
    }

    /**
     * Get datein
     *
     * @return \DateTime 
     */
    public function getDatein()
    {
        return $this->datein;
    }

    /**
     * Set dateout
     *
     * @param \DateTime $dateout
     * @return Workinginformation
     */
    public function setDateout($dateout)
    {
        $this->dateout = $dateout;
    
        return $this;
    }

    /**
     * Get dateout
     *
     * @return \DateTime 
     */
    public function getDateout()
    {
        return $this->dateout;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Workinginformation
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set unit_type
     *
     * @param string $unit_type
     * @return Companyorganizationalunit
     */
    public function setUnitType($unit_type)
    {
        $this->unit_type = $unit_type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getUnitType()
    {
        return $this->unit_type;
    }

    /**
     * Set boss
     *
     * @param integer $boss
     * @return Workinginformation
     */
    public function setBoss($boss)
    {
        $this->boss = $boss;
    
        return $this;
    }

    /**
     * Get boss
     *
     * @return integer 
     */
    public function getBoss()
    {
        return $this->boss;
    }

    /**
     * Set type 
     *
     * @param string $type
     * @return Workinginformation
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type 
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set grade 
     *
     * @param string $grade
     * @return Workinginformation
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    
        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set study_year 
     *
     * @param string $studyYear
     * @return Workinginformation
     */
    public function setStudyYear($studyYear)
    {
        $this->study_year = $studyYear;
    
        return $this;
    }

    /**
     * Get study_year 
     *
     * @return string 
     */
    public function getStudyYear()
    {
        return $this->study_year;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Workinginformation
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workinginformation
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workinginformation
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return User
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return User
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   

    /**
     * Set company
     *
     * @param Fishman\EntityBundle\Entity\Company $company
     * @return Workinginformation
     */
    public function setCompany(\Fishman\EntityBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return Fishman\EntityBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set companycharge
     *
     * @param Fishman\EntityBundle\Entity\Companycharge $companycharge
     * @return Workinginformation
     */
    public function setCompanycharge(\Fishman\EntityBundle\Entity\Companycharge $companycharge = null)
    {
        $this->companycharge = $companycharge;
    
        return $this;
    }

    /**
     * Get companycharge
     *
     * @return Fishman\EntityBundle\Entity\Companycharge 
     */
    public function getCompanycharge()
    {
        return $this->companycharge;
    }

    /**
     * Set user
     *
     * @param Fishman\AuthBundle\Entity\User $user
     * @return Workinginformation
     */
    public function setUser(\Fishman\AuthBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return Fishman\AuthBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set companyorganizationalunit
     *
     * @param Fishman\EntityBundle\Entity\Companyorganizationalunit $companyorganizationalunit
     * @return Workinginformation
     */
    public function setCompanyorganizationalunit(\Fishman\EntityBundle\Entity\Companyorganizationalunit $companyorganizationalunit = null)
    {
        $this->companyorganizationalunit = $companyorganizationalunit;
    
        return $this;
    }

    /**
     * Get companyorganizationalunit
     *
     * @return Fishman\EntityBundle\Entity\Companyorganizationalunit 
     */
    public function getCompanyorganizationalunit()
    {
        return $this->companyorganizationalunit;
    }

    public function __toString()
    {
         return $this->code;
    } 

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $workshopapplications;

    /**
     * Add workshopapplications
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplications
     * @return Workinginformation
     */
    public function addWorkshopapplication(\Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplications)
    {
        $this->workshopapplications[] = $workshopapplications;
    
        return $this;
    }

    /**
     * Remove workshopapplications
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplications
     */
    public function removeWorkshopapplication(\Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplications)
    {
        $this->workshopapplications->removeElement($workshopapplications);
    }

    /**
     * Get workshopapplications
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopapplications()
    {
        return $this->workshopapplications;
    }

    public function setDoctrineRegistry(DoctrineRegistry $doctrineRegistry)
    {
        $this->doctrineRegistry = $doctrineRegistry;
    }

    public function uniqueEmployeeCode(ExecutionContext $context)
    {
        $repository = $this->doctrineRegistry->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('count(wi.id)')
            ->innerJoin('wi.company', 'c')
            ->where('wi.code = :code AND
                     wi.type = :type AND
                     wi.status = 1 AND
                     c.id = :companyid')
            ->setParameter('code', $this->getCode())
            ->setParameter('type', $this->getType())
            ->setParameter('companyid', $this->getCompany()->getId());

        //TODO: This should be the user.id and not the wi.id
        if($this->getId() > 0){
            $queryBuilder->andWhere('wi.id <> :id');
            $queryBuilder->setParameter('id', $this->getId());
        }
        $query = $queryBuilder->getQuery();

        try{
            $count = $query->getSingleResult();
            if($count[1] > 0){
                $context->addViolationAtSubPath('code', 'El código debe ser único por información de empresa', array(), null);
            }
        }
        catch(\Doctrine\Orm\NoResultException $e){
            //TODO: What we should do?
        }
    }

    /**
     * Get current workinginformation 
     */
    public static function getCurrentWorkinginformation(DoctrineRegistry $doctrineRegistry, $wiId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanAuthBundle:Workinginformation');
        $query = $repository->createQueryBuilder('wi')
            ->select('wi.id, wi.code, wi.email, wi.datein, wi.dateout, c.id company_id, c.name company_name')
            ->innerJoin('wi.company', 'c')
            ->where('wi.id = :workinginformation 
                    AND wi.status = 1')
            ->setParameter('workinginformation', $wiId)
            ->getQuery();
        $output = $query->getResult();
        
        return $output;
    }

    /**
     * Get active workinginformations
     */
    public static function getListActiveWorkinginformations(DoctrineRegistry $doctrineRegistry, $uId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanAuthBundle:Workinginformation');
        $query = $repository->createQueryBuilder('wi')
            ->select('wi.id, wi.type, wi.code, wi.email, wi.datein, wi.dateout, c.id company_id, c.name company_name')
            ->innerJoin('wi.company', 'c')
            ->where('wi.user = :uid 
                    AND wi.status = 1')
            ->orderBy('wi.id', 'DESC')
            ->setParameter('uid', $uId)
            ->getQuery();
        $output = $query->getResult();
        
        return $output;
    }

    /**
     * Get a select control with the users of the company.
     */
    public static function getSelectUser(DoctrineRegistry $doctrineRegistry, $companyId, $ignoreUser, $selectedId = false){
        $repository = $doctrineRegistry->getManager()->getRepository('FishmanAuthBundle:Workinginformation');
        $workinginformations = $repository->createQueryBuilder('wi')
            ->select('wi.id, u.names, u.surname, u.lastname')
            ->innerJoin('wi.user', 'u')
            ->where('wi.company = :companyid 
                    AND u.id <> :userid
                    AND wi.status = 1')
            ->setParameter('userid', $ignoreUser)
            ->setParameter('companyid', $companyId)
            ->orderBy('u.surname', 'ASC', 'u.lastname', 'ASC', 'u.names', 'ASC')
            ->getQuery()
            ->getResult();

        $selectBoss = '<select class="boss" name="workinginformation_boss">';
        $selectBoss .= '<option value="">NINGUNO</option>';
        foreach($workinginformations as $wi){
            $selected = '';
            if($selectedId == $wi['id']){
                $selected = ' selected ';
            }

            //TODO: Verificar si esta devolviendo los datos correspondientes
            $selectBoss .= '<option value="' . $wi['id'] . '"'. $selected .'>' . 
                                $wi['surname'] . ' '  . $wi['lastname']  . ' ' . $wi['names'] . 
                           '</option>';
        }
        $selectBoss .= '</select>';
        return $selectBoss;
    }

    /**
     * Devuelve la lista de colaboradores (personas a cargo) y el responsable mismo, como un array donde la llave es el wiId y
     * el nombre son los datos de nombres y apellidos de las personas.
     **/
    public static function getListWorkshopapplicationToCollaborators(DoctrineRegistry $doctrineRegistry, $wsId, $wiId, $includeItself)
    {
        $optionsArray = array();
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $result = $repository->createQueryBuilder('wa')
            ->select('wi.id, u.names, u.lastname, u.surname')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('wi.user', 'u')
            ->where('wa.workshopscheduling = :workshopscheduling 
                    AND wa.deleted = 0')
            ->andWhere('wi.boss = :boss 
                    OR wi.id = :workinginformation')
            ->andWhere('wi.status = 1')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('boss', $wiId)
            ->setParameter('workinginformation', $wiId)
            ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC')
            ->getQuery()
            ->getResult();

        if($result){
            foreach ($result as $r) {
                $optionsArray[$r['id']] = $r['names'] . ' ' . $r['surname'] . ' ' . $r['lastname'];
            }
        }

        return $optionsArray;
    }

    /**
     * Get workinginformations employees 
     */
    public static function getIdWorkinginformationEmployees(DoctrineRegistry $doctrineRegistry, $wiId, $recursive = FALSE, &$output = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanAuthBundle:Workinginformation');
        $result = $repository->createQueryBuilder('wi')
            ->select('wi.id')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'wi.id = pa.evaluated_id')
            ->where('wi.boss = :boss 
                    AND wi.status = 1')
            ->setParameter('boss', $wiId)
            ->orderBy('wi.id')
            ->groupBy('wi.id')
            ->getQuery()
            ->getResult();
        
        if (!empty($result)) {
            foreach ($result as $record) {
                if ($output == '') {
                    $output = $record['id'];
                }
                else {
                    $output .= ',' . $record['id'];
                }
                if ($recursive) {
                    self::getIdWorkinginformationEmployees($doctrineRegistry, $record['id'], TRUE, $output);
                }
            }
        }
        
        return $output;
    }


    /**
     * Get workinginformations employees to report result activities 
     */
    public static function getIdWorkinginformationEmployeesReportActivities(DoctrineRegistry $doctrineRegistry, $wiId, $recursive = FALSE, &$output = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanAuthBundle:Workinginformation');
        $result = $repository->createQueryBuilder('wi')
            ->select('wi.id')
            ->where('wi.boss = :boss 
                    AND wi.status = 1')
            ->setParameter('boss', $wiId)
            ->orderBy('wi.id', 'ASC')
            ->groupBy('wi.id')
            ->getQuery()
            ->getResult();
        

        if (!empty($result)) {
            foreach ($result as $record) {
                if ($output == '') {
                    $output = $record['id'];
                }
                else {
                    $output .= ',' . $record['id'];
                }
                if ($recursive) {
                    self::getIdWorkinginformationEmployeesReportActivities($doctrineRegistry, $record['id'], TRUE, $output);
                }
            }
        }
        
        return $output;
    }


    /**
     * Add pollschedulingpeoples
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingpeople $pollschedulingpeoples
     * @return Workinginformation
     */
    public function addPollschedulingpeople(\Fishman\PollBundle\Entity\Pollschedulingpeople $pollschedulingpeoples)
    {
        $this->pollschedulingpeoples[] = $pollschedulingpeoples;
    
        return $this;
    }

    /**
     * Remove pollschedulingpeoples
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingpeople $pollschedulingpeoples
     */
    public function removePollschedulingpeople(\Fishman\PollBundle\Entity\Pollschedulingpeople $pollschedulingpeoples)
    {
        $this->pollschedulingpeoples->removeElement($pollschedulingpeoples);
    }

    /**
     * Get pollschedulingpeoples
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulingpeoples()
    {
        return $this->pollschedulingpeoples;
    }

    /**
     * Set headquarter
     *
     * @param Fishman\EntityBundle\Entity\Headquarter $headquarter
     * @return Workinginformation
     */
    public function setHeadquarter(\Fishman\EntityBundle\Entity\Headquarter $headquarter = null)
    {
        $this->headquarter = $headquarter;
    
        return $this;
    }

    /**
     * Get headquarter
     *
     * @return Fishman\EntityBundle\Entity\Headquarter 
     */
    public function getHeadquarter()
    {
        return $this->headquarter;
    }

    /**
     * Set academic_year
     *
     * @param string $academicYear
     * @return Workinginformation
     */
    public function setAcademicYear($academicYear)
    {
        $this->academic_year = $academicYear;
    
        return $this;
    }

    /**
     * Get academic_year
     *
     * @return string 
     */
    public function getAcademicYear()
    {
        return $this->academic_year;
    }

    /**
     * Set companyfaculty
     *
     * @param Fishman\EntityBundle\Entity\Companyfaculty $companyfaculty
     * @return Workinginformation
     */
    public function setCompanyfaculty(\Fishman\EntityBundle\Entity\Companyfaculty $companyfaculty = null)
    {
        $this->companyfaculty = $companyfaculty;
    
        return $this;
    }

    /**
     * Get companyfaculty
     *
     * @return Fishman\EntityBundle\Entity\Companyfaculty 
     */
    public function getCompanyfaculty()
    {
        return $this->companyfaculty;
    }

    /**
     * Set companycareer
     *
     * @param Fishman\EntityBundle\Entity\Companycareer $companycareer
     * @return Workinginformation
     */
    public function setCompanycareer(\Fishman\EntityBundle\Entity\Companycareer $companycareer = null)
    {
        $this->companycareer = $companycareer;
    
        return $this;
    }

    /**
     * Get companycareer
     *
     * @return Fishman\EntityBundle\Entity\Companycareer 
     */
    public function getCompanycareer()
    {
        return $this->companycareer;
    }

    /**
     * Get list boss options
     * 
     */
    public static function getListBossOptions(DoctrineRegistry $doctrine, $type) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('u.id, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wib', 'WITH', 'wi.boss = wib.id')
            ->innerJoin('wib.user', 'u')
            ->where('wi.boss <> :boss 
                    AND wi.type = :type
                    AND wi.status = 1')
            ->setParameter('boss', '')
            ->setParameter('type', $type)
            ->groupBy('u.id')
            ->orderBy('u.surname', 'ASC', 'u.lastname', 'ASC', 'u.names', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $output[$r['id']] = $r['surname'] . ' ' . $r['lastname'] . ' ' . $r['names'];
        }
        
        return $output;
    }

    /**
     * Get list grade options
     * 
     */
    public static function getListGradeOptions() 
    {
        $output = array(
            '1p' => '1ro de primaria', 
            '2p' => '2do de primaria', 
            '3p' => '3ro de primaria', 
            '4p' => '4to de primaria', 
            '5p' => '5to de primaria', 
            '6p' => '6to de primaria', 
            '1s' => '1ro de secundaria', 
            '2s' => '2do de secundaria', 
            '3s' => '3ro de secundaria', 
            '4s' => '4to de secundaria', 
            '5s' => '5to de secundaria'
        );
        
        return $output;
    }

    /**
     * Get grade
     * 
     */
    public static function getGradeName($id) 
    {
        $output = '';
        
        switch($id){
            case '1p':
                $output = '1ro de Primaria';
                break;
            case '2p':
                $output = '2do de Primaria';
                break;
            case '3p':
                $output = '3ro de Primaria';
                break;
            case '4p':
                $output = '4to de Primaria';
                break;
            case '5p':
                $output = '5to de Primaria';
                break;
            case '6p':
                $output = '6to de Primaria';
                break;
            case '1s':
                $output = '1ro de Secundaria';
                break;
            case '2s':
                $output = '2do de Secundaria';
                break;
            case '3s':
                $output = '3ro de Secundaria';
                break;
            case '4s':
                $output = '4to de Secundaria';
                break;
            case '5s':
                $output = '5to de Secundaria';
                break;
        }
        
        return $output;
    }

    /**
     * Get list person options by pollscheduling
     * 
     */
    public static function getListEvaluatedsByPollscheduling(DoctrineRegistry $doctrine, $pscId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanAuthBundle:Workinginformation');
        $queryBuilder = $repository->createQueryBuilder('wi')
            ->select('wi.id, u.names, u.surname, u.lastname')
            ->innerJoin('wi.user', 'u')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'wi.id = pa.evaluated_id')
            ->innerJoin('pa.pollscheduling', 'psc')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.status = 1 
                    AND pa.deleted = 0 
                    AND pa.finished = 1 
                    AND psc.status = 1 
                    AND psc.deleted = 0 
                    AND wi.status = 1')
            ->setParameter('pollscheduling', $pscId)
            ->groupBy('wi.id')
            ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $output[$r['id']] = $r['surname'] . ' ' . $r['lastname'] . ' ' . $r['names'];
        }
        
        return $output;
    }
    
    /**
     * Get Workinginformation Type Name
     * 
     */
    public static function getTypeName($value)
    {
        $name = '';
        switch ($value) {
            case 'workinginformation':
                $name = 'Trabajador';
                break;
            case 'schoolinformation':
                $name = 'Estudiante';
                break;
            case 'universityinformation':
                $name = 'Universitario';
                break;
        }
        return $name;
    }
    
    /**
     * Get Workinginformation Type Company Code
     * 
     */
    public static function getTypeCompanyCode($value)
    {
        $name = '';
        switch ($value) {
            case 'working':
                $name = 'workinginformation';
                break;
            case 'school':
                $name = 'achoolinformation';
                break;
            case 'university':
                $name = 'universityinformation';
                break;
        }
        return $name;
    }
    
    /**
     * Add woringinformation to evaluation
     * 
     */
    public static function addWorkinginformationToEvaluation(DoctrineRegistry $doctrine, $peopleUser, $wiArray, $userBy) {
        
        $em = $doctrine->getManager();
        
        $wiUser = $em->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(array(
            'code' => $wiArray->getNumberidentity(), 
            'type' => $wiArray->getType(), 
            'company' => $wiArray->getCompanyId()
        ));
        if (!$wiUser) {
            $wiUser = new Workinginformation($doctrine);
            $wiUser->setCreatedBy($userBy->getId());
            $wiUser->setCreated(new \DateTime());
        }
        
        $wiUser->setUser($peopleUser);
        $wiUser->setCode($wiArray->getCode());
        $wiUser->setCompany($em->getRepository('FishmanEntityBundle:Company')->find($wiArray->getCompanyId()));
        $wiUser->setHeadquarter($em->getRepository('FishmanEntityBundle:Headquarter')->find($wiArray->getHeadquarterId()));
        $wiUser->setEmail($wiArray->getEmailInformation());
        $wiUser->setStatus(TRUE);
        $wiUser->setModifiedBy($userBy->getId());
        $wiUser->setChanged(new \DateTime());
        
        if ($wiArray->getType() == 'workinginformation') {
            
            $wiUser->setType($wiArray->getType());
            $organizationalunit = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->find($wiArray->getOrganizationalunitId());
            $wiUser->setUnitType($wiArray->getType($organizationalunit->getType()));
            $wiUser->setCompanyorganizationalunit($organizationalunit);
            $wiUser->setCompanycharge($em->getRepository('FishmanEntityBundle:Companycharge')->find($wiArray->getChargeId()));
            
        }
        elseif ($wiArray->getType() == 'schoolinformation') {
            
            $wiUser->setType($wiArray->getType());
            $wiUser->setGrade($wiArray->getGrade());
            $wiUser->setStudyYear($wiArray->getStudyYear());
            
        }
        elseif ($wiArray->getType() == 'universityinformation') {
            
            $wiUser->setType($wiArray->getType());
            $wiUser->setCompanyfaculty($em->getRepository('FishmanEntityBundle:Companyfaculty')->find($wiArray->getFacultyId()));
            $wiUser->setCompanycareer($em->getRepository('FishmanEntityBundle:Companycareer')->find($wiArray->getCareerId()));
            $wiUser->setAcademicYear($wiArray->getAcademicYear());
            
        }
        
        $em->persist($wiUser);
        $em->flush();
        
        return $wiUser; 
    }
    
}