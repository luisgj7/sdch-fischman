<?php

namespace Fishman\AuthBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\AuthBundle\Entity\User
 * @ORM\Table(name="Authuser")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $surname
     */
    private $surname;

    /**
     * @var string $lastname
     */
    private $lastname;

    /**
     * @var string $names
     */
    private $names;

    /**
     * @var \DateTime $birthday
     */
    private $birthday;

    /**
     * @var string $birthplace
     */
    private $birthplace;

    /**
     * @var boolean $sex
     */
    private $sex;

    /**
     * @var string $identity
     */
    private $identity;

    /**
     * @var string $numberidentity
     */
    private $numberidentity;

    /**
     * @var string $marital_status
     */
    private $marital_status;

    /**
     * @var string $phone
     */
    private $phone;

    /**
     * @var integer $comapny_id
     */
    private $company_id;

    /**
     * @var integer $organizationalunit_id
     */
    private $organizationalunit_id;

    /**
     * @var integer $charge_id
     */
    private $charge_id;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    //Muestra los equivalentes por tipo de documento: dni, license (carné de extranjería), 
    private static $documentTypeEquivalents = array('dni' => 'd', 'license' => 'e', 'passport' => 'p');
    
    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Workinginformation", mappedBy="user")
     */
    protected $workinginformations;
    
    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Permission", mappedBy="user")
     */
    protected $permissions;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set names
     *
     * @param string $names
     * @return User
     */
    public function setNames($names)
    {
        $this->names = $names;
    
        return $this;
    }

    /**
     * Get names
     *
     * @return string 
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * Set identity
     *
     * @param string $identity
     * @return User
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;
    
        return $this;
    }

    /**
     * Get identity
     *
     * @return string 
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set numberidentity
     *
     * @param string $numberidentity
     * @return User
     */
    public function setNumberidentity($numberidentity)
    {
        $this->numberidentity = $numberidentity;
    
        return $this;
    }

    /**
     * Get numberidentity
     *
     * @return string 
     */
    public function getNumberidentity()
    {
        return $this->numberidentity;
    }

    /**
     * Set birthday
     *
     * @param \Date $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \Date 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }
    
    /**
     * Set birthplace
     *
     * @param string $birthplace
     * @return User
     */
    public function setBirthplace($birthplace)
    {
        $this->birthplace = $birthplace;
    
        return $this;
    }

    /**
     * Get birthplace
     *
     * @return string 
     */
    public function getBirthplace()
    {
        return $this->birthplace;
    }

    /**
     * Set sex
     *
     * @param boolean $sex
     * @return User
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    
        return $this;
    }

    /**
     * Get sex
     *
     * @return boolean 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set marital_status
     *
     * @param string $maritalStatus
     * @return User
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->marital_status = $maritalStatus;
    
        return $this;
    }

    /**
     * Get marital_status
     *
     * @return string 
     */
    public function getMaritalStatus()
    {
        return $this->marital_status;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return User
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return User
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return User
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   
    /**
     * Set id
     *
     * @param integer $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Add workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     * @return User
     */
    public function addWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations[] = $workinginformations;
    
        return $this;
    }

    /**
     * Remove workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     */
    public function removeWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations->removeElement($workinginformations);
    }

    /**
     * Get workinginformations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkinginformations()
    {
        return $this->workinginformations;
    }

    /**
     * Set company_id
     *
     * @param integer $companyId
     * @return User
     */
    public function setCompanyId($companyId)
    {
        $this->company_id = $companyId;
    
        return $this;
    }

    /**
     * Get company_id
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * Set organizationalunit_id
     *
     * @param integer $organizationalunitId
     * @return User
     */
    public function setOrganizationalunitId($organizationalunitId)
    {
        $this->organizationalunit_id = $organizationalunitId;
    
        return $this;
    }

    /**
     * Get organizationalunit_id
     *
     * @return integer 
     */
    public function getOrganizationalunitId()
    {
        return $this->organizationalunit_id;
    }

    /**
     * Set charge_id
     *
     * @param integer $chargeId
     * @return User
     */
    public function setChargeId($chargeId)
    {
        $this->charge_id = $chargeId;
    
        return $this;
    }

    /**
     * Get charge_id
     *
     * @return integer 
     */
    public function getChargeId()
    {
        return $this->charge_id;
    }

    public static function getDocumentTypeEquivalents()
    {
        return self::$documentTypeEquivalents;
    }

    public static function getDocumentTypeEquivalent($documentType)
    {
        return self::$documentTypeEquivalents[$documentType];
    }

    public static function createUsername($documentType, $numberIdentity)
    {
         return self::getDocumentTypeEquivalent($documentType) . $numberIdentity;
    }

    public static function getDocumentTypeByEquivalent($equivalent)
    {
        return array_search($equivalent, self::$documentTypeEquivalents);
    }

    //TODO: Esto podria llamarse automaticamente antes de guardar
    public function autoCreateUsername()
    {
         return self::createUsername($this->getIdentity(), $this->getNumberidentity());
    }

    public static function generateRandomPassword()
    {
        return rand(1234343434343, 999993434343434);
    }

    /**
     * Add permissions
     *
     * @param Fishman\AuthBundle\Entity\Permission $permissions
     * @return User
     */
    public function addPermission(\Fishman\AuthBundle\Entity\Permission $permissions)
    {
        $this->permissions[] = $permissions;
    
        return $this;
    }

    /**
     * Remove permissions
     *
     * @param Fishman\AuthBundle\Entity\Permission $permissions
     */
    public function removePermission(\Fishman\AuthBundle\Entity\Permission $permissions)
    {
        $this->permissions->removeElement($permissions);
    }

    /**
     * Get permissions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
    
    /**
     * Get list rol options
     * 
     */
    public static function getListRolOptions() 
    {
        $output = array(
            'ROLE_SUPER_ADMIN' => 'Super Administrador',
            'ROLE_ADMIN' => 'Administrador',
            'ROLE_TEACHER' => 'Profesor',
            'ROLE_USER' => 'Usuario'
        );
        
        return $output;
    }
    
    /**
     * Get list rol options
     * 
     */
    public static function typeInformationName($type)
    {
        $output = '';
        
        switch ($type) {
            case 'working':
                $output = 'laboral';
                break;
            case 'school':
                $output = 'colegio';
                break;
            case 'university':
                $output = 'universidad';
                break;
        }
        
        return $output;
    }
    
    /**
     * Delete temporal files
     * 
     */
    public static function deleteTemporalFiles($directory)
    {
        $dir = opendir($directory);
        while ($file = readdir($dir)) {
            if (is_file($file)) {
                unlink($directory . $file);
            }
        }
    }
    
    /**
     * Add user to evaluation
     * 
     */
    public static function addUserToEvaluation(DoctrineRegistry $doctrine, $peopleArray, $userBy) {
        
        $em = $doctrine->getManager();
        
        $peopleUser = $em->getRepository('FishmanAuthBundle:User')->findOneBy(array('numberidentity' => $peopleArray->getNumberidentity()));
        if (!$peopleUser) {
            $peopleUser = new User();
            $peopleUser->setCreatedBy($userBy->getId());
            $peopleUser->setCreated(new \DateTime());
        }
        
        $peopleUser->setSurname($peopleArray->getSurname());
        $peopleUser->setLastname($peopleArray->getLastname());
        $peopleUser->setNames($peopleArray->getNames());
        $peopleUser->setIdentity($peopleArray->getIdentity());
        $peopleUser->setNumberidentity($peopleArray->getNumberidentity());
        $peopleUser->setEmail($peopleArray->getEmail());
        $peopleUser->setSex($peopleArray->getSex());
        $peopleUser->setBirthday($peopleArray->getBirthday());
        $peopleUser->setUsername($peopleArray->getNumberidentity());
        $peopleUser->setPassword(User::generateRandomPassword());
        $peopleUser->setEnabled(TRUE);
        $peopleUser->setModifiedBy($userBy->getId());
        $peopleUser->setChanged(new \DateTime());
        
        $peopleUser->setCompanyId($peopleArray->getCompanyId());
        
        if ($peopleArray->getType() == 'workinginformation') {
            $peopleUser->setOrganizationalunitId($peopleArray->getOrganizationalunitId());
            $peopleUser->setChargeId($peopleArray->getChargeId());
        }
        
        $em->persist($peopleUser);
        $em->flush();
        
        return $peopleUser; 
    }
    
}