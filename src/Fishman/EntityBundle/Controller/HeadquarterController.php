<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Headquarter;
use Fishman\EntityBundle\Entity\Ubigeo;
use Fishman\EntityBundle\Entity\Country;
use Fishman\EntityBundle\Form\HeadquarterType;

/**
 * Headquarter controller.
 *
 */
class HeadquarterController extends Controller
{
    /**
     * Lists all Headquarter entities.
     *
     */
    public function indexAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Company Info
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company || $companyid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        // Recovering data
        
        $via_options = Headquarter::getListViaOptions();
        $country_options = Country::getListCountryOptions($this->getDoctrine());
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'via' => '', 
            'address' => '', 
            'country' => '', 
            'department' => '', 
            'province' => '', 
            'district' => '', 
            'city' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('via', 'choice', array(
                'choices' => $via_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('address', 'text', array(
                'required' => FALSE
            ))
            ->add('country', 'choice', array(
                'choices' => $country_options, 
                'empty_value' => 'Choose an option', 
                'attr' => array(
                    'class' => 'countryselect'
                ),
                'required' => FALSE
            ))
            ->add('department', 'hidden', array( 
                'attr' => array(
                    'class' => 'departmentvalue'
                ),
                'required' => FALSE
            ))
            ->add('province', 'hidden', array( 
                'attr' => array(
                    'class' => 'provincevalue'
                ),
                'required' => FALSE
            ))
            ->add('district', 'hidden', array( 
                'attr' => array(
                    'class' => 'districtvalue'
                ),
                'required' => FALSE
            ))
            ->add('city', 'text', array(
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'via' => '', 
            'address' => '', 
            'country' => '', 
            'department' => '', 
            'province' => '', 
            'district' => '',
            'city' => '',  
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Headquarter');
        $queryBuilder = $repository->createQueryBuilder('h')
            ->select('h.id, h.code, h.name, h.via, h.address, c.id country_id, c.name country, ud.id department_id, 
                      ud.name department, up.id province_id, up.name province, udt.id district_id, udt.name district, 
                      h.city, h.status, h.changed, u.names, u.surname, u.lastname')
            ->innerJoin('h.country', 'c')
            ->leftJoin('FishmanEntityBundle:Ubigeo', 'ud', 'WITH', 'h.department = ud.id')
            ->leftJoin('FishmanEntityBundle:Ubigeo', 'up', 'WITH', 'h.province = up.id')
            ->leftJoin('FishmanEntityBundle:Ubigeo', 'udt', 'WITH', 'h.district = udt.id')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'h.modified_by = u.id')
            ->where('h.company = :company')
            ->andWhere('h.id LIKE :id 
                        OR h.code LIKE :code 
                        OR h.name LIKE :name')
            ->setParameter('company', $companyid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('h.id', 'ASC');
        
        // Add arguments
        
        if ($data['via'] != '') {
            $queryBuilder
                ->andWhere('h.via = :via')
                ->setParameter('via', $data['via']);
        }
        if ($data['address'] != '') {
            $queryBuilder
                ->andWhere('h.address LIKE :address')
                ->setParameter('address', '%' . $data['address'] . '%');
        }
        if ($data['country'] != '') {
            $queryBuilder
                ->andWhere('h.country = :country')
                ->setParameter('country', $data['country']);
        }
        if ($data['department'] != '') {
            $queryBuilder
                ->andWhere('h.department = :department')
                ->setParameter('department', $data['department']);
        }
        if ($data['province'] != '') {
            $queryBuilder
                ->andWhere('h.province = :province')
                ->setParameter('province', $data['province']);
        }
        if ($data['district'] != '') {
            $queryBuilder
                ->andWhere('h.district = :district')
                ->setParameter('district', $data['district']);
        }
        if ($data['city'] != '') {
            $queryBuilder
                ->andWhere('h.city LIKE :city')
                ->setParameter('city', '%' . $data['city'] . '%');
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('h.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'headquarter');
        $paginator->setMaxPagerItems(5, 'headquarter');
        $entities = $paginator->paginate($query, 'headquarter')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('headquarter');
        $endPageItem = $paginator->getEndPageItem('headquarter');
        $totalItems = $paginator->getTotalItems('headquarter');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        $selectDepartment = '';
        $selectProvince = '';
        $selectDistrict = '';
        
        // Department
        if ($data['department']) {
            $selectDepartment = Ubigeo::getSelectUbigeo($this->getDoctrine(), $data['country'], 'department', $data['department']);
        }
        elseif ($data['country']) {
            $selectDepartment = Ubigeo::getSelectUbigeo($this->getDoctrine(), $data['country'], 'department');
        }
        
        // Province
        if ($data['province']) {
            $selectProvince = Ubigeo::getSelectUbigeo($this->getDoctrine(), $data['department'], 'province', $data['province']);
        }
        elseif ($data['department']) {
            $selectProvince = Ubigeo::getSelectUbigeo($this->getDoctrine(), $data['department'], 'province');
        }
        
        // District
        if ($data['district']) {
            $selectDistrict = Ubigeo::getSelectUbigeo($this->getDoctrine(), $data['province'], 'district', $data['district']);
        }
        elseif ($data['province']) {
            $selectDistrict = Ubigeo::getSelectUbigeo($this->getDoctrine(), $data['province'], 'district');
        }
        
        return $this->render('FishmanEntityBundle:Headquarter:index.html.twig', array(
            'entities' => $entities,
            'company' => $company,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'selectDepartment' => $selectDepartment,
            'selectProvince' => $selectProvince,
            'selectDistrict' => $selectDistrict,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Headquarter entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Headquarter');
        $result = $repository->createQueryBuilder('h')
            ->select('h.id, h.code, h.name, c.name company, c.company_type, c.id company_id, h.via, h.address, h.urbanization, 
                      ct.name country, ct.id country_id, ud.name department, ud.id department_id, up.name province, 
                      up.id province_id, udt.name district, udt.id district_id, h.city, h.phone_station, h.status, 
                      h.created, h.changed, h.created_by, h.modified_by')
            ->innerJoin('h.company', 'c')
            ->innerJoin('h.country', 'ct')
            ->leftJoin('FishmanEntityBundle:Ubigeo', 'ud', 'WITH', 'h.department = ud.id')
            ->leftJoin('FishmanEntityBundle:Ubigeo', 'up', 'WITH', 'h.province = up.id')
            ->leftJoin('FishmanEntityBundle:Ubigeo', 'udt', 'WITH', 'h.district = udt.id')
            ->where('h.id = :headquarter')
            ->setParameter('headquarter', $id)
            ->getQuery()
            ->getResult();
        
        $entity = current($result);
        
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        return $this->render('FishmanEntityBundle:Headquarter:show.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Headquarter entity.
     *
     */
    public function newAction($companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $country = $em->getRepository('FishmanEntityBundle:Country')->find(1);
        if (!$country) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el país.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $entity = new Headquarter($this->getDoctrine());
        
        /*
         * TODO: Cambiar los vbalore sde País(country), Departamento(department), Provincia(province), 
         * por, los ids de Perú, Lima, Lima
         */
        
        $entity->setCompany($company);
        $entity->setStatus(1);
        $form = $this->createForm(new HeadquarterType($entity, $this->getDoctrine()));
        
        // Department
        $selectDepartment = Ubigeo::getSelectUbigeo($this->getDoctrine(), 1, 'department', 1392);
        // Province
        $selectProvince = Ubigeo::getSelectUbigeo($this->getDoctrine(), 1392, 'province', 1393);
        // District
        $selectDistrict = Ubigeo::getSelectUbigeo($this->getDoctrine(), 1393, 'district');

        return $this->render('FishmanEntityBundle:Headquarter:new.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'form' => $form->createView(),
            'selectDepartment' => $selectDepartment,
            'selectProvince' => $selectProvince,
            'selectDistrict' => $selectDistrict
        ));
    }

    /**
     * Creates a new Headquarter entity.
     *
     */
    public function createAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();

        // set flash messages
        $session = $this->getRequest()->getSession();

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $postData = $request->request->get('fishman_entitybundle_headquartertype', null);
        
        $entity  = new Headquarter($this->getDoctrine());
        $form = $this->createForm(new HeadquarterType($entity, $this->getDoctrine()), $entity);
        $form->bind($request);
        
        if ($form->isValid()) {
    
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            if ($entity->getCountry()->getId() == 1) {
                $entity->setCity('');
                $entity->setDepartment($postData['department']);
                $entity->setProvince($postData['province']);
                $entity->setDistrict($postData['district']);
            }
            else {
                $entity->setDepartment('');
                $entity->setProvince('');
                $entity->setDistrict('');
            }
            $entity->setCompany($company);
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
          
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('headquarter_show', array(
                'id' => $entity->getId()
            )));
        }

        $selectDepartment = '';
        $selectProvince = '';
        $selectDistrict = '';
        
        if ($entity->getCountry()->getId() == 1) {
            // Department
            $selectDepartment = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getCountry()->getId(), 'department', $entity->getDepartment());
            // Province
            $selectProvince = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getDepartment(), 'province', $entity->getProvince());
            // District
            $selectDistrict = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getProvince(), 'district', $entity->getDistrict());
        }

        return $this->render('FishmanEntityBundle:Headquarter:new.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'form'   => $form->createView(),
            'selectDepartment' => $selectDepartment,
            'selectProvince' => $selectProvince,
            'selectDistrict' => $selectDistrict
        ));
    }

    /**
     * Displays a form to edit an existing Headquarter entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Headquarter')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new HeadquarterType($entity, $this->getDoctrine()), $entity);
        
        $selectDepartment = '';
        $selectProvince = '';
        $selectDistrict = '';
        
        if ($entity->getCountry()->getId() == 1) {
            
            // Department
            if ($entity->getDepartment()) {
                $selectDepartment = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getCountry()->getId(), 'department', $entity->getDepartment());
            }
            else {
                $selectDepartment = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getCountry()->getId(), 'department');
            }
            
            // Province
            if ($entity->getProvince()) {
                $selectProvince = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getDepartment(), 'province', $entity->getProvince());
            }
            elseif ($entity->getDepartment()) {
                $selectProvince = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getDepartment(), 'province');
            }
            
            // District
            if ($entity->getDistrict()) {
                $selectDistrict = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getProvince(), 'district', $entity->getDistrict());
            }
            elseif ($entity->getProvince()) {
                $selectDistrict = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getProvince(), 'district');
            }
            
        }

        return $this->render('FishmanEntityBundle:Headquarter:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'selectDepartment' => $selectDepartment,
            'selectProvince' => $selectProvince,
            'selectDistrict' => $selectDistrict
        ));
    }

    /**
     * Edits an existing Headquarter entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Headquarter')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $postData = $request->request->get('fishman_entitybundle_headquartertype', null);
        
        $form = $this->createForm(new HeadquarterType($entity, $this->getDoctrine()), $entity);
        $form->bind($request);
        
        if ($form->isValid()) {
    
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            if ($entity->getCountry()->getId() == 1) {
                $entity->setCity('');
                $entity->setDepartment($postData['department']);
                $entity->setProvince($postData['province']);
                $entity->setDistrict($postData['district']);
            }
            else {
                $entity->setDepartment('');
                $entity->setProvince('');
                $entity->setDistrict('');
            }
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
          
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('headquarter_show', array(
                'id' => $id
            )));
        }

        $selectDepartment = '';
        $selectProvince = '';
        $selectDistrict = '';
        
        if ($entity->getCountry()->getId() == 1) {
            // Department
            $selectDepartment = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getCountry()->getId(), 'department', $entity->getDepartment());
            // Province
            $selectProvince = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getDepartment(), 'province', $entity->getProvince());
            // District
            $selectDistrict = Ubigeo::getSelectUbigeo($this->getDoctrine(), $entity->getProvince(), 'district', $entity->getDistrict());
        }

        return $this->render('FishmanEntityBundle:Headquarter:edit.html.twig', array(
            'entity' => $entity,
            'company' => $entity->getCompany(),
            'edit_form'   => $form->createView(),
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'selectDepartment' => $selectDepartment,
            'selectProvince' => $selectProvince,
            'selectDistrict' => $selectDistrict
        ));
    }

    /**
     * Deletes a Headquarter entity.
     *
     */
    public function deleteAction(Request $request, $id, $companyid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanEntityBundle:Headquarter')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
                return $this->redirect($this->generateUrl('headquarter', array(
                    'companyid' => $companyid
                )));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $wi = $repository->createQueryBuilder('wi')
                ->select('count(wi.headquarter)')
                ->where('wi.headquarter = :headquarter')
                ->setParameter('headquarter', $entity->getId())
                ->groupBy('wi.headquarter')
                ->getQuery()
                ->getResult();
            
            if (current($wi) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La Sede está en uso.');
            }
        }

        return $this->redirect($this->generateUrl('headquarter', array(
            'companyid' => $companyid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop Headquarter entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Headquarter')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sede de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Headquarter:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     *
     * This information needs to refresh like select control.
     * This action get a json with the select controls (all in one request) in order to update form elements
     */
    public function ajaxFormElementsByUbigeoAction()
    {
        $ubigeoId = $_GET['ubigeo_id'];
        $ubigeoType = $_GET['ubigeo_type'];

        // Ubigeo
        $selectUbigeo = Ubigeo::getSelectUbigeo($this->getDoctrine(), $ubigeoId, $ubigeoType);
        
        $response = new Response(json_encode(
            array('select_ubigeo' => $selectUbigeo)
        ));
        
        return $response;
    }
    
}
