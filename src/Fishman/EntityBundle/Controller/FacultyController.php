<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Faculty;
use Fishman\EntityBundle\Form\FacultyType;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Faculty controller.
 *
 */
class FacultyController extends Controller
{
    /**
     * Lists all Faculty entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Faculty');
        $queryBuilder = $repository->createQueryBuilder('f')
            ->select('f.id, f.name, f.changed, f.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'f.modified_by = u.id')
            ->where('f.id LIKE :id 
                    OR f.name LIKE :name')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('f.id', 'ASC');
        
        // Add arguments
        
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('f.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'faculty');
        $paginator->setMaxPagerItems(5, 'faculty');
        $entities = $paginator->paginate($query, 'faculty')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('faculty');
        $endPageItem = $paginator->getEndPageItem('faculty');
        $totalItems = $paginator->getTotalItems('faculty');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanEntityBundle:Faculty:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Faculty entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Faculty')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la facultad.');
            return $this->redirect($this->generateUrl('faculty'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        return $this->render('FishmanEntityBundle:Faculty:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Faculty entity.
     *
     */
    public function newAction()
    {
        $entity = new Faculty();
        $entity->setStatus(1);
        $form   = $this->createForm(new FacultyType(), $entity);

        return $this->render('FishmanEntityBundle:Faculty:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Faculty entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Faculty();
        $form = $this->createForm(new FacultyType(), $entity);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('faculty_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanEntityBundle:Faculty:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Faculty entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Faculty')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la facultad.');
            return $this->redirect($this->generateUrl('faculty'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new FacultyType(), $entity);

        return $this->render('FishmanEntityBundle:Faculty:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Faculty entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Faculty')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la facultad.');
            return $this->redirect($this->generateUrl('faculty'));
        }
        
        $editForm = $this->createForm(new FacultyType(), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
            
            return $this->redirect($this->generateUrl('faculty_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanEntityBundle:Faculty:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Faculty entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanEntityBundle:Faculty')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la facultad.');
                return $this->redirect($this->generateUrl('faculty'));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyfaculty');
            $cf = $repository->createQueryBuilder('cf')
                ->select('count(cf.faculty)')
                ->where('cf.faculty = :faculty')
                ->setParameter('faculty', $entity->getId())
                ->groupBy('cf.faculty')
                ->getQuery()
                ->getResult();
            
            if (current($cf) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La Facultad está en uso.');
            }
        }
        
        return $this->redirect($this->generateUrl('faculty'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Faculty entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Faculty')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la facultad.');
            return $this->redirect($this->generateUrl('faculty'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Faculty:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
}
