<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Companycareer;
use Fishman\EntityBundle\Form\CompanycareerType;
use Fishman\EntityBundle\Form\CompanycareerimportType;
use Fishman\EntityBundle\Entity\Company;
use Fishman\EntityBundle\Entity\Companyfaculty;
use Fishman\ImportExportBundle\Entity\Temporalimportdata;
use Ideup\SimplePaginatorBundle\Paginator;
use Symfony\Component\HttpFoundation\Response;

use PHPExcel_IOFactory;

/**
 * Companycareer controller.
 *
 */
class CompanycareerController extends Controller
{
    /**
     * Lists all Companycareer entities.
     *
     */
    public function indexAction(Request $request, $companyid)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Company Info
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company || $companyid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        // Recovering data
        
        $companyfaculty_options = Companyfaculty::getListCompanyfacultyOptions($this->getDoctrine(), $companyid);
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'companyfaculty' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('companyfaculty', 'choice', array(
                'choices' => $companyfaculty_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',  
            'companyfaculty' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycareer');
        $queryBuilder = $repository->createQueryBuilder('ccr')
            ->select('ccr.id, ccr.code, ccr.name, cf.name companyfaculty, ccr.changed, ccr.status, 
                      u.names, u.surname, u.lastname')
            ->leftJoin('ccr.companyfaculty', 'cf')
            ->innerJoin('FishmanAuthBundle:User', 'u')
            ->where('ccr.modified_by = u.id')
            ->andWhere('ccr.company = :company')
            ->andWhere('ccr.id LIKE :id 
                    OR ccr.code LIKE :code 
                    OR ccr.name LIKE :name')
            ->setParameter('company', $companyid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('ccr.id', 'ASC');
        
        // Add arguments
        
        if ($data['companyfaculty'] != '') {
            $queryBuilder
                ->andWhere('ccr.id = :companyfaculty')
                ->setParameter('companyfaculty', $data['companyfaculty']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('cr.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'companycareer');
        $paginator->setMaxPagerItems(5, 'companycareer');
        $entities = $paginator->paginate($query, 'companycareer')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('companycareer');
        $endPageItem = $paginator->getEndPageItem('companycareer');
        $totalItems = $paginator->getTotalItems('companycareer');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanEntityBundle:Companycareer:index.html.twig', array(
            'entities' => $entities,
            'company' => $company,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Companycareer entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanEntityBundle:Companycareer')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la carrera de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanEntityBundle:Companycareer:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Companycareer entity.
     *
     */
    public function newAction($companyid)
    {
        $entity = new Companycareer();
        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la carrera de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $entity->setCompany($company);
        $entity->setStatus(1);
        $form = $this->createForm(new CompanycareerType($company, '', $this->getDoctrine()), $entity);

        return $this->render('FishmanEntityBundle:Companycareer:new.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new Companycareer entity.
     *
     */
    public function createAction(Request $request, $companyid)
    {
        $entity  = new Companycareer();
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $postData = $request->request->get('fishman_entitybundle_companycareertype');
        
        $form = $this->createForm(new CompanycareerType($company, $postData['companyfaculty_id'], $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            // Faculty
            $companyFaculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($postData['companyfaculty_id']);
            
            $entity->setCompanyfaculty($companyFaculty);
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCompany($company);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('companycareer_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanEntityBundle:Companycareer:new.html.twig', array(
            'entity' => $entity,
            'companyid' => $companyid,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Companycareer entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycareer')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la carrera de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new CompanycareerType($entity->getCompany(), $entity->getCompanyfaculty()->getId(), $this->getDoctrine()), $entity);

        return $this->render('FishmanEntityBundle:Companycareer:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Companycareer entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycareer')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la carrera de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $postData = $request->request->get('fishman_entitybundle_companycareertype');
        
        $editForm = $this->createForm(new CompanycareerType($entity->getCompany(), $postData['companyfaculty_id'], $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            // Faculty
            $companyFaculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($postData['companyfaculty_id']);
            
            $entity->setCompanyfaculty($companyFaculty);
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            $em->persist($entity);
            $em->flush();
                                                        
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('companycareer_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanEntityBundle:Companycareer:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Companycareer entity.
     *
     */
    public function deleteAction(Request $request, $id, $companyid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $entity = $em->getRepository('FishmanEntityBundle:Companycareer')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la carrera de la empresa.');
                return $this->redirect($this->generateUrl('companycareer', array(
                    'companyid' => $companyid
                )));
            }
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $wi = $repository->createQueryBuilder('wi')
                ->select('count(wi.companycareer)')
                ->where('wi.companycareer = :companycareer')
                ->setParameter('companycareer', $entity->getId())
                ->groupBy('wi.companycareer')
                ->getQuery()
                ->getResult();
            
            if (current($wi) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La Carrera de la empresa está en uso.');
            }

        }

        return $this->redirect($this->generateUrl('companycareer', array(
          'companyid' => $companyid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Companycareer entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycareer')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la carrera de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Companycareer:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Import a Companycareer entity.
     *
     */
    public function importAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Mensaje de que no hay registros
        $session = $this->getRequest()->getSession();
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $defaultData = array();
        $form = $this->createForm(new CompanycareerimportType(), $defaultData);

        $form2 = false;
        $data = '';
        $careers = false;
        $numberRegister = 0;

        if ($request->isMethod('POST')) {
            //The user is confirmed to persist data as Companycareer entity
            $importFormData = $request->request->get('form', false);
            
            if ($importFormData) {
                $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                     ->find($importFormData['temporalimportdata_id']);
                if (!$temporalentity) {
                    return $this->render('FishmanEntityBundle:Companycareer:import.html.twig', array(
                         'company' => $company,
                         'form' => $form->createView(),
                         'form2' => $form2,
                         'careers' => $careers,
                    ));
                }
                
                $careers = $temporalentity->getData();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    foreach ($careers as $careerarray) {
                        //If the companyfacultycode exists update this
                        $query = $em->createQuery(
                            'SELECT ccr FROM FishmanEntityBundle:Companycareer ccr 
                             WHERE ccr.company = :company
                               AND ccr.code = :code')
                             ->setParameter('company', $company)
                             ->setParameter('code', $careerarray['code']);
    
                        try {
                            $companycareer = $query->getSingleResult();
                        } catch (\Doctrine\Orm\NoResultException $e) {
                            $companycareer = false;
                        }
                        
                        $userBy = $this->get('security.context')->getToken()->getUser();
                        if (!$companycareer) {
                            $companycareer = new Companycareer();
                            $companycareer->setCreatedBy($userBy->getId());
                            $companycareer->setCreated(new \DateTime());
                        }
                        
                        //TODO: When create companycareer manual, then use the same logic
    
                        $companycareer->setCode($careerarray['code']);
                        $companycareer->setName($careerarray['name']);
                        $companycareer->setStatus($careerarray['status']);
                        $companyfaculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($careerarray['companyfaculty_id']);
                        $companycareer->setCompanyfaculty($companyfaculty);
    
                        // User
                        $companycareer->setModifiedBy($userBy->getId());
                        $companycareer->setCompany($company);
                        $companycareer->setChanged(new \DateTime());
                
                        $em->persist($companycareer);
                        $em->flush();
                        if ($careerarray['valid'] != 0) {
                          $numberRegister = $numberRegister + 1;
                        }
                    } //end foreach
    
                    //Delete temporal data
                    $em->remove($temporalentity);
                    $em->flush();
                    
                    // Delete temporal files
                    Company::deleteTemporalFiles('../tmp/');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                if ($numberRegister > 0) {
                    $session->getFlashBag()->add('status', 'Se grabó la importación de '.$numberRegister.' Careera(s).');
                }
                else {
                    $session->getFlashBag()->add('error', 'No se logró importar ninguna Carrera.');
                }
                
                if($request->request->get('saveandclose', true)){
                    return $this->redirect($this->generateUrl('companycareer', array(
                        'companyid' => $companyid
                    )));
                }
                
                $faculties = false;
                
            } //End have temporarlimportdata input
            else {
                $form->bind($request);
                $data = $form->getData();
                
                // We need to move the file in order to use it
                $randomName = rand(1, 10000000000);
                $randomName = $randomName . '.xlsx';
                $directory = __DIR__.'/../../../../tmp';
                $data['file']->move($directory, $randomName);
                
                if (null !== $data['file']) {
                    $fileName = $data['file']->getClientOriginalName();
                    $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                }
                
                if ($data['file']->document == 'xlsx') { 
                    $inputFileName = $directory . '/' . $randomName;
                    $phpExcel = PHPExcel_IOFactory::load($inputFileName);
                    
                    $worksheet = $phpExcel->getSheet(0);
                    
                    $row = 2;
                    $careers = array();
                    
                    do {
                        $code = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                        $name = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                        $companyfaculty_code = trim($worksheet->getCellByColumnAndRow(2, $row++)->getValue());
                        $message = '';
                        $valid = 1;
                        $companyfaculty_id = '';
                        $companyfaculty_name = '';
                        
                        if($code == '' && $name == '' && $companyfaculty_code == '') break;
                        if($code == ''){
                            $message .= '<p>Falta el código de la carrera</p>';
                            $valid = 0;
                        }
                        else {
                            // Check if you have legacy
                            $cf_code = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycareer')->findOneBy(
                                array(
                                    'code' => $code,
                                    'company' => $companyid
                                )
                            );
                            if (!empty($cf_code)) {
                                $message .= '<p>El código de la careera ya está siendo usado en otra carrera de la empresa</p>';
                                $valid = 0;
                            }
                        }
                        
                        if($name == ''){
                            $message .= '<p>Falta el nombre de la carrera</p>';
                            $valid = 0;
                        }
  
                        if($companyfaculty_code == ''){
                            $message .= '<p>Falta el código de la faculta</p>';
                            $valid = 0;
                        }
                        else{
                            $companyfaculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->findOneBy(
                                array(
                                    'code' => $companyfaculty_code,
                                    'company' => $companyid
                                )
                            );
                            if(!empty($companyfaculty)){
                                $companyfaculty_id = $companyfaculty->getId();
                                $companyfaculty_name = $companyfaculty->getName();
                            }
                            else {
                                $message .= '<p>El código de la facultad no existe en la empresa actual</p>';
                                $valid = 0;
                            }
                        }
  
                        if($valid && $message == ''){
                            $message = 'OK';
                        }
                        
                        $careers[] = array('code' => $code,
                                           'name' => $name,
                                           'companyfaculty_code' => $companyfaculty_code,
                                           'companyfaculty_id' => $companyfaculty_id,
                                           'companyfaculty_name' => $companyfaculty_name,
                                           'valid' => $valid,
                                           'status' => 1,
                                           'message' => $message);
                    } while (true);
                    
                }
                else {
                    $session->getFlashBag()->add('status', 'el archivo a importar debe ser de formato excel');
                }
                
                if(count($careers) > 0 and is_array($careers)){
                    $temporal = new Temporalimportdata();
                    $temporal->setData($careers);
                    $temporal->setEntity('companyfaculty');
                    $temporal->setDatetime(new \DateTime());
                    $em->persist($temporal);
                    $em->flush();

                    // Secondary form
                    $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                    $form2 = $this->createFormBuilder($defaultData2)
                        ->add('temporalimportdata_id',
                              'integer', array (
                                       'required' => true 
                             ))
                        ->getForm();
                    $form2 = $form2->createView();
                }
                else {
                    $session->getFlashBag()->add('status', 'No hay carreras para importar.');
                }
                
                //Borrar archivo temporal
                if ($data['file']->document == 'xlsx') {
                    unlink($inputFileName);
                }
                
            }
        } //End is method post
        
        return $this->render('FishmanEntityBundle:Companycareer:import.html.twig', array(
            'company' => $company,
            'form' => $form->createView(),
            'form2' => $form2,
            'careers' => $careers,
        ));
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="importar-empresa-carreras.xlsx'.'"'
        );
        return new Response(file_get_contents(
            __DIR__.'/../../../../templates/EntityBundle/Company/importar-empresa-carreras.xlsx'), 200, $headers);
    }
    
}
