<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Category;
use Fishman\EntityBundle\Form\CategoryType;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Category controller.
 *
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.name, c.changed, c.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'c.modified_by = u.id')
            ->where('c.id LIKE :id 
                    OR c.name LIKE :name')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('c.id', 'ASC');
        
        // Add arguments
        
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('c.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
            
        // Paginator
        
    	  $paginator = $this->get('ideup.simple_paginator');
	      $paginator->setItemsPerPage(20, 'category');
	      $paginator->setMaxPagerItems(5, 'category');
        $entities = $paginator->paginate($query, 'category')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('category');
        $endPageItem = $paginator->getEndPageItem('category');
        $totalItems = $paginator->getTotalItems('category');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
	      return $this->render('FishmanEntityBundle:Category:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
	          'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
	      ));
    }

    /**
     * Finds and displays a Category entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Category')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la categoría.');
            return $this->redirect($this->generateUrl('category'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        return $this->render('FishmanEntityBundle:Category:show.html.twig', array(
            'entity'  => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Category entity.
     *
     */
    public function newAction()
    {
        $entity = new Category();
        $entity->setStatus(1);
        $form   = $this->createForm(new CategoryType(), $entity);

        return $this->render('FishmanEntityBundle:Category:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Category entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Category();
        $form = $this->createForm(new CategoryType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('category_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanEntityBundle:Category:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Category')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la categoría.');
            return $this->redirect($this->generateUrl('category'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new CategoryType(), $entity);

        return $this->render('FishmanEntityBundle:Category:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Category entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Category')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la categoría.');
            return $this->redirect($this->generateUrl('category'));
        }

        $editForm = $this->createForm(new CategoryType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            $entity->setModifiedBy($modifiedBy->getId());
            
            $entity->setChanged(new \DateTime());
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('category_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanEntityBundle:Category:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Category entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
          
            $entity = $em->getRepository('FishmanEntityBundle:Category')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la categoría.');
                return $this->redirect($this->generateUrl('category'));
            }

            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('category'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Category entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Category')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la categoría.');
            return $this->redirect($this->generateUrl('category'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Category:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
}
