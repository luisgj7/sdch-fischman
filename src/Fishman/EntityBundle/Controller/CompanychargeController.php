<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Companycharge;
use Fishman\EntityBundle\Form\CompanychargeType;
use Fishman\EntityBundle\Form\CompanychargeimportType;
use Fishman\EntityBundle\Entity\Company;
use Fishman\ImportExportBundle\Entity\Temporalimportdata;
use Ideup\SimplePaginatorBundle\Paginator;
use Symfony\Component\HttpFoundation\Response;

use PHPExcel_IOFactory;

/**
 * Companycharge controller.
 *
 */
class CompanychargeController extends Controller
{
    /**
     * Lists all Companycharge entities.
     *
     */
    public function indexAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Company Info
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company || $companyid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
           
        // Recovering data
        
        $charge_options = Companycharge::getListChargeOptions($this->getDoctrine(), $companyid);
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'charge' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('charge', 'choice', array(
                'choices' => $charge_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',  
            'charge' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycharge');
        $queryBuilder = $repository->createQueryBuilder('cch')
            ->select('cch.id, cch.code, cch.name, ch.name charge, cch.changed, cch.status, u.names, u.surname, u.lastname')
            ->leftJoin('cch.charge', 'ch')
            ->innerJoin('FishmanAuthBundle:User', 'u')
            ->where('cch.modified_by = u.id')
            ->andWhere('cch.company = :company')
            ->andWhere('cch.id LIKE :id 
                    OR cch.code LIKE :code 
                    OR cch.name LIKE :name')
            ->setParameter('company', $companyid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('cch.id', 'ASC');
        
        // Add arguments
        
        if ($data['charge'] != '') {
            $queryBuilder
                ->andWhere('ch.id = :charge')
                ->setParameter('charge', $data['charge']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('cch.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'companycharge');
        $paginator->setMaxPagerItems(5, 'companycharge');
        $entities = $paginator->paginate($query, 'companycharge')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('companycharge');
        $endPageItem = $paginator->getEndPageItem('companycharge');
        $totalItems = $paginator->getTotalItems('companycharge');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanEntityBundle:Companycharge:index.html.twig', array(
            'entities' => $entities,
            'company' => $company,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Companycharge entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycharge')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el cargo de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanEntityBundle:Companycharge:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Companycharge entity.
     *
     */
    public function newAction($companyid)
    {
        $entity = new Companycharge();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $entity->setCompany($company);
        $entity->setStatus(1);
        $form   = $this->createForm(new CompanychargeType(), $entity);

        return $this->render('FishmanEntityBundle:Companycharge:new.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new Companycharge entity.
     *Companycharge
     */
    public function createAction(Request $request, $companyid)
    {
        $entity  = new Companycharge();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $form = $this->createForm(new CompanychargeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $entity->setCompany($company);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();

            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('companycharge_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanEntityBundle:Companycharge:new.html.twig', array(
            'entity' => $entity,
            'companyid' => $companyid,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Companycharge entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycharge')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el cargo de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new CompanychargeType(), $entity);

        return $this->render('FishmanEntityBundle:Companycharge:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Companycharge entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycharge')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el cargo de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $editForm = $this->createForm(new CompanychargeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            $entity->setModifiedBy($modifiedBy->getId());
            
            $entity->setChanged(new \DateTime());
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('companycharge_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanEntityBundle:Companycharge:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Companycharge entity.
     *
     */
    public function deleteAction(Request $request, $id, $companyid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanEntityBundle:Companycharge')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el cargo de la empresa.');
                return $this->redirect($this->generateUrl('companycharge', array(
                    'companyid' => $companyid
                )));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $wi = $repository->createQueryBuilder('wi')
                ->select('count(wi.companycharge)')
                ->where('wi.companycharge = :companycharge')
                ->setParameter('companycharge', $entity->getId())
                ->groupBy('wi.companycharge')
                ->getQuery()
                ->getResult();
            
            if (current($wi) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'El Cargo de la empresa está en uso.');
            }

        }

        return $this->redirect($this->generateUrl('companycharge', array(
          'companyid' => $companyid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Companycharge entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycharge')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el cargo de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Companycharge:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Import a  Companycharge entity.
     * 
     */
    public function importAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $defaultData = array();
        $form = $this->createForm(new CompanychargeimportType(), $defaultData);

        $form2 = false;
        $data = '';
        $charges = false;
        $numberRegister = 0;

        if ($request->isMethod('POST')) {
            //The user is confirmed to persist data as Companycharge entity
            $importFormData = $request->request->get('form', false);

            if($importFormData){
                $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                     ->find($importFormData['temporalimportdata_id']);
                if(!$temporalentity){
                  return $this->render('FishmanEntityBundle:Companycharge:import.html.twig', array(
                         'company' => $company,
                         'form' => $form->createView(),
                         'form2' => $form2,
                         'charges' => $charges,
                  ));
                }

                $charges = $temporalentity->getData();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    foreach ($charges as $chargearray){
                        //If the companychargecode exists update this
                        $query = $em->createQuery(
                            'SELECT c FROM FishmanEntityBundle:Companycharge c 
                             WHERE c.company = :company
                               AND c.code = :code')
                             ->setParameter('company', $company)
                             ->setParameter('code', $chargearray['code']);
    
                        try {
                            $companycharge = $query->getSingleResult();
                        } catch (\Doctrine\Orm\NoResultException $e) {
                            $companycharge = false;
                        }
    
                        $userBy = $this->get('security.context')->getToken()->getUser();
                        if(!$companycharge){
                            $companycharge = new Companycharge();
                            $companycharge->setCreatedBy($userBy->getId());
                            $companycharge->setCreated(new \DateTime());
                        }
    
                        //TODO: When create companycharge manual, then use the same logic
    
                        $companycharge->setCode($chargearray['code']);
                        $companycharge->setName($chargearray['name']);
                        $companycharge->setStatus($chargearray['status']);
                        $charge = $em->getRepository('FishmanEntityBundle:Charge')->find($chargearray['charge_id']);
                        $companycharge->setCharge($charge);
    
                        // User
                        $companycharge->setModifiedBy($userBy->getId());
                        $companycharge->setCompany($company);
                        $companycharge->setChanged(new \DateTime());
                
                        $em->persist($companycharge);
                        $em->flush();
                        if ($chargearray['valid'] != 0) {
                          $numberRegister = $numberRegister + 1;
                        }
                    } //end foreach
    
                    //Delete temporal data
                    $em->remove($temporalentity);
                    $em->flush();
                    
                    // Delete temporal files
                    Company::deleteTemporalFiles('../tmp/');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                if ($numberRegister > 0) {
                    $session->getFlashBag()->add('status', 'Se grabó la importación de '.$numberRegister.' Cargo(s).');
                }
                else {
                    $session->getFlashBag()->add('error', 'No se logró importar ningun Cargo.');
                }
                
                if($request->request->get('saveandclose', true)){
                    return $this->redirect($this->generateUrl('companycharge', array('companyid' => $companyid)));
                }

                $charges = false;

            } //End have temporarlimportdata input
            else{
                $form->bind($request);
                $data = $form->getData();

                // We need to move the file in order to use it
                $randomName = rand(1, 10000000000);
                $randomName = $randomName . '.xlsx';
                $directory = __DIR__.'/../../../../tmp';
                $data['file']->move($directory, $randomName);

                if (null !== $data['file']) {
                    $fileName = $data['file']->getClientOriginalName();
                    $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                }

                if ($data['file']->document == 'xlsx') { 
                    $inputFileName = $directory . '/' . $randomName;
                    $phpExcel = PHPExcel_IOFactory::load($inputFileName);
  
                    $worksheet = $phpExcel->getSheet(0);
  
                    $row = 2;
                    $charges = array();
                    do {
                        $code = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                        $name = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                        $charge_id = trim($worksheet->getCellByColumnAndRow(2, $row++)->getValue());
                        $message = '';
                        $valid = 1;
                        $charge_name = '';
  
                        if($code == '' && $name == '' && $charge_id == '') break;
                        if($code == ''){
                            $message .= '<p>Falta el código del cargo</p>';
                            $valid = 0;
                        }
                        else {
                            // Check if you have legacy
                            $cch_code = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycharge')->findOneBy(
                                array(
                                    'code' => $code,
                                    'company' => $companyid
                                )
                            );
                            if (!empty($cch_code)) {
                                $message .= '<p>El código del cargo ya está siendo usado en otro cargo de la empresa</p>';
                                $valid = 0;
                            }
                        }
  
                        if($name == ''){
                            $message .= '<p>Falta el nombre del cargo</p>';
                            $valid = 0;
                        }
  
                        if($charge_id == ''){
                            $message .= '<p>Falta el id del nombre global</p>';
                        }
                        elseif(!preg_match('/^\d+$/',$charge_id)) { //Only digits
                            $message .= '<p>El id del nombre global debe ser un número entero</p>';
                        }
                        else{
                            //TODO: Change query in order to find only the name ()
                            $charge = $em->getRepository('FishmanEntityBundle:Charge')->find($charge_id);
                            if($charge){
                                $charge_name = $charge->getName();
                            }
                        }
  
                        if($valid && $message == ''){
                            $message = 'OK';
                        }
  
                        $charges[] = array('code' => $code,
                                           'name' => $name,
                                           'charge_id' => $charge_id,
                                           'charge_name' => $charge_name,
                                           'valid' => $valid,
                                           'status' => 1,
                                           'message' => $message);
                    } while (true);
                }else {
                    //Mensaje de que no hay registros
                    $session = $this->getRequest()->getSession();
                    $session->getFlashBag()->add('status', 'el archivo a importar debe ser de formato excel');
                }

                if(count($charges) > 0 and is_array($charges)){
                    $temporal = new Temporalimportdata();
                    $temporal->setData($charges);
                    $temporal->setEntity('companycharge');
                    $temporal->setDatetime(new \DateTime());
                    $em->persist($temporal);
                    $em->flush();

                    // Secondary form
                    $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                    $form2 = $this->createFormBuilder($defaultData2)
                        ->add('temporalimportdata_id',
                              'integer', array (
                                       'required' => true 
                             ))
                        ->getForm();
                    $form2 = $form2->createView();
                }
                else{
                    //Mensaje de que no hay registros
                    $session = $this->getRequest()->getSession();
                    $session->getFlashBag()->add('status', 'No hay cargos para importar.');
                }
                //Borrar archivo temporal
                if ($data['file']->document == 'xlsx') {
                    unlink($inputFileName);
                }
            }
        } //End is method post

        return $this->render('FishmanEntityBundle:Companycharge:import.html.twig', array(
            'company' => $company,
            'form' => $form->createView(),
            'form2' => $form2,
            'charges' => $charges,
        ));
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="importar-empresa-cargos.xlsx'.'"'
        );
        return new Response(file_get_contents(
            __DIR__.'/../../../../templates/EntityBundle/Company/importar-empresa-cargos.xlsx'), 200, $headers);
    }

    public function ajaxChoiceByCompanyAction()
    {
        $companyId = $_GET['company_id'];

        $repository = $this->getDoctrine()->getManager()->getRepository('FishmanEntityBundle:Companycharge');
        $ccs = $repository->findActiveByCompany($companyId);

        $selectCompanyCharge = '<select class="companycharge" name="company_charge">';
        $selectCompanyCharge .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        if($ccs){
            foreach($ccs as $cc){
                $selectCompanyCharge .= '<option value="' . $cc['id'] . '">' .  $cc['name'] . '</option>';
            }
        }
        $selectCompanyCharge .= '</select>';

        $response = new Response(json_encode(
            array('select_companycharge' => $selectCompanyCharge)));  
        return $response;
    }

}
