<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Company;
use Fishman\EntityBundle\Entity\Companycourse;
use Fishman\EntityBundle\Form\CompanycourseType;
use Fishman\EntityBundle\Form\CompanycourseimportType;
use Fishman\ImportExportBundle\Entity\Temporalimportdata;

use Ideup\SimplePaginatorBundle\Paginator;

use Symfony\Component\HttpFoundation\Response;

use PHPExcel_IOFactory;

/**
 * Companycourse controller.
 *
 */
class CompanycourseController extends Controller
{
    /**
     * Lists all Companycourse entities.
     *
     */
    public function indexAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Company Info
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company || $companyid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        // Recovering data
        
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        $typeCompany =$company->getCompanyType();

        if ($typeCompany == 'working') {
            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'No puede ingresar a los cursos de esta empresa.');

            return $this->redirect($this->generateUrl('company', array(
              'companyid' => $company->getId(),
            )));
        }
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',  
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycourse');
        $queryBuilder = $repository->createQueryBuilder('cc')
            ->select('cc.id, cc.code, cc.name, cc.changed, cc.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:User', 'u')
            ->where('cc.modified_by = u.id')
            ->andWhere('cc.company = :company')
            ->andWhere('cc.id LIKE :id 
                    OR cc.code LIKE :code 
                    OR cc.name LIKE :name')
            ->setParameter('company', $companyid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('cc.id', 'ASC');
        
        // Add arguments
        
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('cc.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'companycourse');
        $paginator->setMaxPagerItems(5, 'companycourse');
        $entities = $paginator->paginate($query, 'companycourse')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('companycourse');
        $endPageItem = $paginator->getEndPageItem('companycourse');
        $totalItems = $paginator->getTotalItems('companycourse');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanEntityBundle:Companycourse:index.html.twig', array(
            'entities' => $entities,
            'company' => $company,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Companycourse entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycourse')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el curso de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $typeCompany =$entity->getCompany()->getCompanyType();

        if ($typeCompany == 'working') {
            $session->getFlashBag()->add('status', 'No puede ingresar a los cursos de esta empresa.');

            return $this->redirect($this->generateUrl('company', array(
              'companyid' => $entity->getCompany()->getId(),
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanEntityBundle:Companycourse:show.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Companycourse entity.
     *
     */
    public function newAction($companyid)
    {
        $entity = new Companycourse();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $typeCompany = $company->getCompanyType();

        if ($typeCompany == 'working') {
            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'No puede ingresar a los cursos de esta empresa.');

            return $this->redirect($this->generateUrl('company', array(
              'companyid' => $company->getId(),
            )));
        }
        
        $entity->setCompany($company);
        $entity->setStatus(1);
        $form   = $this->createForm(new CompanycourseType(), $entity);

        return $this->render('FishmanEntityBundle:Companycourse:new.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Companycourse entity.
     *
     */
    public function createAction(Request $request, $companyid)
    {
        $entity  = new Companycourse();
        $cem = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $company = $cem->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $typeCompany = $company->getCompanyType();

        if ($typeCompany == 'working') {
            $session->getFlashBag()->add('status', 'No puede ingresar a los cursos de esta empresa.');

            return $this->redirect($this->generateUrl('company', array(
              'companyid' => $company->getId(),
            )));
        }
        
        $form = $this->createForm(new CompanycourseType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $entity->setCompany($company);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('companycourse_show', array('id' => $entity->getId())));
        }

        return $this->render('FishmanEntityBundle:Companycourse:new.html.twig', array(
            'entity' => $entity,
            'companyid' => $companyid,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Companycourse entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycourse')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el curso de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $typeCompany = $entity->getCompany()->getCompanyType();

        if ($typeCompany == 'working') {
            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'No puede ingresar a los cursos de esta empresa.');

            return $this->redirect($this->generateUrl('company', array(
              'companyid' => $entity->getCompany()->getId(),
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new CompanycourseType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Companycourse:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Companycourse entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycourse')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el curso de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $typeCompany = $entity->getCompany()->getCompanyType();

        if ($typeCompany == 'working') {
            $session->getFlashBag()->add('status', 'No puede ingresar a los cursos de esta empresa.');

            return $this->redirect($this->generateUrl('company', array(
              'companyid' => $entity->getCompany()->getId(),
            )));
        }

        $editForm = $this->createForm(new CompanycourseType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            $entity->setModifiedBy($modifiedBy->getId());
            
            $entity->setChanged(new \DateTime());
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('companycourse_show', array('id' => $id)));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanEntityBundle:Companycourse:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    public function importAction(Request $request, $companyid)
    {
        set_time_limit(120);
        ini_set('memory_limit','512M');
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $em = $this->getDoctrine()->getManager();
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $defaultData = array();
        $form = $this->createForm(new CompanycourseimportType(), $defaultData);

        $form2 = false;
        $data = '';
        $courses = false;
        $numberRegister = 0;

        if ($request->isMethod('POST')) {
            //The user is confirmed to persist data as Companycharge entity
            $importFormData = $request->request->get('form', false);

            if($importFormData){
                $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                     ->find($importFormData['temporalimportdata_id']);
                if(!$temporalentity){
                  return $this->render('FishmanEntityBundle:Companycourse:import.html.twig', array(
                         'company' => $company,
                         'form' => $form->createView(),
                         'form2' => $form2,
                         'courses' => $courses,
                  ));
                }

                $courses = $temporalentity->getData();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    foreach ($courses as $coursearray){
                        //If the companycoursecode exists update this
                        $query = $em->createQuery(
                            'SELECT c FROM FishmanEntityBundle:Companycourse c 
                             WHERE c.company = :company
                               AND c.code = :code')
                             ->setParameter('company', $company)
                             ->setParameter('code', $coursearray['code']);
    
                        try {
                            $companycourse = $query->getSingleResult();
                        } catch (\Doctrine\Orm\NoResultException $e) {
                            $companycourse = false;
                        }
    
                        $userBy = $this->get('security.context')->getToken()->getUser();
                        if(!$companycourse){
                            $companycourse = new Companycourse();
                            $companycourse->setCreatedBy($userBy->getId());
                            $companycourse->setCreated(new \DateTime());
                        }
    
                        //TODO: When create companycourse manual, then use the same logic
    
                        $companycourse->setCode($coursearray['code']);
                        $companycourse->setName($coursearray['name']);
                        $companycourse->setStatus($coursearray['status']);
    
                        // User
                        $companycourse->setModifiedBy($userBy->getId());
                        $companycourse->setCompany($company);
                        $companycourse->setChanged(new \DateTime());
                
                        $em->persist($companycourse);
                        $em->flush();
                        if ($coursearray['valid'] != 0) {
                          $numberRegister = $numberRegister + 1;
                        }
                    } //end foreach
    
                    //Delete temporal data
                    $em->remove($temporalentity);
                    $em->flush();
                    
                    // Delete temporal files
                    Company::deleteTemporalFiles('../tmp/');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                if ($numberRegister > 0) {
                    $session->getFlashBag()->add('status', 'Se grabó la importación de '.$numberRegister.' Curso(s).');
                }
                else {
                    $session->getFlashBag()->add('error', 'No se logró importar ningun Curso.');
                }
                                       
                if($request->request->get('saveandclose', true)){
                    return $this->redirect($this->generateUrl('companycourse', array('companyid' => $companyid)));
                }

                $courses = false;

            } //End have temporarlimportdata input
            else{
                $form->bind($request);
                $data = $form->getData();

                // We need to move the file in order to use it
                $randomName = rand(1, 10000000000);
                $randomName = $randomName . '.xlsx';
                $directory = __DIR__.'/../../../../tmp';
                $data['file']->move($directory, $randomName);

                if (null !== $data['file']) {
                    $fileName = $data['file']->getClientOriginalName();
                    $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                }

                if ($data['file']->document == 'xlsx') { 
                    $inputFileName = $directory . '/' . $randomName;
                    $phpExcel = PHPExcel_IOFactory::load($inputFileName);
  
                    $worksheet = $phpExcel->getSheet(0);
  
                    $row = 2;
                    $courses = array();
                    do {
                        $code = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                        $name = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                        $course_id = trim($worksheet->getCellByColumnAndRow(2, $row++)->getValue());
                        $message = '';
                        $valido = 1;
                        $course_name = '';
  
                        if($code == '' && $name == '' && $course_id == '') break;
                        if($code == ''){
                            $message .= '<p>Falta el código del curso</p>';
                            $valido = 0;
                        }
                        else {
                            // Check if you have legacy
                            $cch_code = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                array(
                                    'code' => $code,
                                    'company' => $companyid
                                )
                            );
                            if (!empty($cch_code)) {
                                $message .= '<p>El código del curso ya está siendo usado en otro curso de la empresa</p>';
                                $valido = 0;
                            }
                        }
  
                        if($name == ''){
                            $message .= '<p>Falta el nombre del curso</p>';
                            $valido = 0;
                        }
  
                        if($valido && $message == ''){
                            $message = 'OK';
                        }
  
                        $courses[] = array('code' => $code,
                                           'name' => $name,
                                           'course_name' => $course_name,
                                           'valid' => $valido,
                                           'status' => 1,
                                           'message' => $message);
                    } while (true);
                }else {
                    $session->getFlashBag()->add('status', 'el archivo a importar debe ser de formato excel');
                }

                if(count($courses) > 0 and is_array($courses)){
                    $temporal = new Temporalimportdata();
                    $temporal->setData($courses);
                    $temporal->setEntity('companycourse');
                    $temporal->setDatetime(new \DateTime());
                    $em->persist($temporal);
                    $em->flush();

                    // Secondary form
                    $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                    $form2 = $this->createFormBuilder($defaultData2)
                        ->add('temporalimportdata_id',
                              'integer', array (
                                       'required' => true 
                             ))
                        ->getForm();
                    $form2 = $form2->createView();
                }
                else{
                    $session->getFlashBag()->add('status', 'No hay cursos para importar.');
                }
                //Borrar archivo temporal
                if ($data['file']->document == 'xlsx') {
                    unlink($inputFileName);
                }
            }
        } //End is method post

        return $this->render('FishmanEntityBundle:Companycourse:import.html.twig', array(
            'company' => $company,
            'form' => $form->createView(),
            'form2' => $form2,
            'courses' => $courses,
        ));
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="importar-empresa-cursos.xlsx'.'"'
        );
        return new Response(file_get_contents(
            __DIR__.'/../../../../templates/EntityBundle/Company/importar-empresa-cursos.xlsx'), 200, $headers);
    }


    /**
     * Deletes a Companycourse entity.
     *
     */
    public function deleteAction(Request $request, $id, $companyid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanEntityBundle:Companycourse')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el curso de la empresa.');
                return $this->redirect($this->generateUrl('companycourse', array(
                    'companyid' => $companyid
                )));
            }

            $typeCompany = $entity->getCompany()->getCompanyType();

            if ($typeCompany == 'working') {
                $session->getFlashBag()->add('status', 'No puede ingresar a los cursos de esta empresa.');

                return $this->redirect($this->generateUrl('company', array(
                  'companyid' => $entity->getCompany()->getId(),
                )));
            }

            // set flash messages
            $session = $this->getRequest()->getSession();

            $em->remove($entity);
            $em->flush();
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('companycourse', array(
          'companyid' => $companyid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Companycourse entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companycourse')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el curso de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $typeCompany = $entity->getCompany()->getCompanyType();

        if ($typeCompany == 'working') {
            $session->getFlashBag()->add('status', 'No puede ingresar a los cursos de esta empresa.');

            return $this->redirect($this->generateUrl('company', array(
              'companyid' => $entity->getCompany()->getId(),
            )));
        }
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Companycourse:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }
}

