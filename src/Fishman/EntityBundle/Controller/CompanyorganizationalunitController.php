<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\EntityBundle\Form\CompanyorganizationalunitType;
use Fishman\EntityBundle\Form\CompanyorganizationalunitimportType;
use Fishman\EntityBundle\Entity\Company;
use Fishman\ImportExportBundle\Entity\Temporalimportdata;
use Ideup\SimplePaginatorBundle\Paginator;
use Symfony\Component\HttpFoundation\Response;

use PHPExcel_IOFactory;

/**
 * Companyorganizationalunit controller.
 *
 */
class CompanyorganizationalunitController extends Controller
{
    /**
     * Lists all Companyorganizationalunit entities.
     *
     */
    public function indexAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Company Info
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company || $companyid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        // Recovering data
        
        $organizationalunit_options = Companyorganizationalunit::getListOrganizationalunitOptions($this->getDoctrine(), $companyid);
        $boss_options = Companyorganizationalunit::getListBossOptions($this->getDoctrine(), $companyid);
        $parent_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $companyid);
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'organizationalunit' => '', 
            'boss' => '', 
            'parent' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('organizationalunit', 'choice', array(
                'choices' => $organizationalunit_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('boss', 'choice', array(
                'choices' => $boss_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('unit_type', 'choice', array(
                'choices' => array(
                    'administrative' => 'Administrativa', 
                    'academic' => 'Academica'
                ), 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('parent', 'choice', array(
                'choices' => $parent_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',  
            'organizationalunit' => '', 
            'boss' => '', 
            'parent' => '', 
            'unit_type' => '',
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $queryBuilder = $repository->createQueryBuilder('cou')
            ->select('cou.id, cou.code, cou.name, ou.name organizationalunit, b.names boss_names, b.surname boss_surname, 
                      b.lastname boss_lastname, coup.name parent, cou.unit_type, cou.changed, cou.status, u.names, u.surname, u.lastname')
            ->leftJoin('cou.organizationalunit', 'ou')
            ->leftJoin('FishmanEntityBundle:Companyorganizationalunit', 'coup', 'WITH', 'cou.parent_id = coup.id')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'cou.boss = wi.id')
            ->leftJoin('FishmanAuthBundle:User', 'b', 'WITH', 'wi.user = b.id')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'cou.modified_by = u.id')
            ->where('cou.company = :company')
            ->andWhere('cou.id LIKE :id 
                    OR cou.code LIKE :code 
                    OR cou.name LIKE :name')
            ->setParameter('company', $companyid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('cou.id', 'ASC');
        
        // Add arguments
        
        if ($data['organizationalunit'] != '') {
            $queryBuilder
                ->andWhere('ou.id = :organizationalunit')
                ->setParameter('organizationalunit', $data['organizationalunit']);
        }
        if ($data['boss'] != '') {
            $queryBuilder
                ->andWhere('cou.boss = :boss')
                ->setParameter('boss', $data['boss']);
        }
        if ($data['parent'] != '') {
            $queryBuilder
                ->andWhere('cou.parent_id = :parent')
                ->setParameter('parent', $data['parent']);
        }
        if ($data['unit_type'] != '') {
            $queryBuilder
                ->andWhere('cou.unit_type = :unit_type')
                ->setParameter('unit_type', $data['unit_type']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('cou.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
            
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'companyorganizationalunit');
        $paginator->setMaxPagerItems(5, 'companyorganizationalunit');
        $entities = $paginator->paginate($query, 'companyorganizationalunit')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('companyorganizationalunit');
        $endPageItem = $paginator->getEndPageItem('companyorganizationalunit');
        $totalItems = $paginator->getTotalItems('companyorganizationalunit');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanEntityBundle:Companyorganizationalunit:index.html.twig', array(
            'entities' => $entities,
            'company' => $company,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Companyorganizationalunit entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $result = $repository->createQueryBuilder('co')
            ->select('co.id, co.code, co.name, c.id company_id, c.company_type, c.name company, o.name organizationalunit, co.type, co.unit_type, cop.name parent, 
                      b.names boss_names, b.surname boss_surname, b.lastname boss_lastname, co.status, co.created, co.changed, co.created_by, co.modified_by')
            ->leftJoin('co.organizationalunit', 'o')
            ->innerJoin('co.company', 'c')
            ->leftJoin('FishmanEntityBundle:Companyorganizationalunit', 'cop', 'WITH', 'co.parent_id = cop.id')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'co.boss = wi.id')
            ->leftJoin('FishmanAuthBundle:User', 'b', 'WITH', 'wi.user = b.id')
            ->where('co.id = :companyorganizationalunit')
            ->setParameter('companyorganizationalunit', $id)
            ->getQuery()
            ->getResult();
        
        $entity = current($result);
        
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la unidad organizativa de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        return $this->render('FishmanEntityBundle:Companyorganizationalunit:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Companyorganizationalunit entity.
     *
     */
    public function newAction($companyid)
    {
        $em = $this->getDoctrine()->getManager();        
        $entity = new Companyorganizationalunit();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $entity->setCompany($company);
        $entity->setStatus(1);
        $form   = $this->createForm(new CompanyorganizationalunitType($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanEntityBundle:Companyorganizationalunit:new.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Companyorganizationalunit entity.
     *
     */
    public function createAction(Request $request, $companyid)
    {
        $entity  = new Companyorganizationalunit();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $entity->setCompany($company);
        $form = $this->createForm(new CompanyorganizationalunitType($entity, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
            $coCode = $repository->createQueryBuilder('co')
                ->select('count(co.id)')
                ->where('co.company = :company 
                        AND co.code = :code')
                ->setParameter('company', $companyid)
                ->setParameter('code', $entity->getCode())
                ->groupBy('co.id')
                ->getQuery()
                ->getResult();
                
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
            $coName = $repository->createQueryBuilder('co')
                ->select('count(co.id)')
                ->where('co.company = :company 
                        AND co.name = :name')
                ->setParameter('company', $companyid)
                ->setParameter('name', $entity->getName())
                ->groupBy('co.id')
                ->getQuery()
                ->getResult();
            
            if (current($coCode) == 0 && current($coName) == 0) {
            
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                $entity->setCreatedBy($userBy->getId());
                $entity->setModifiedBy($userBy->getId());
                $entity->setCreated(new \DateTime());
                $entity->setChanged(new \DateTime());
    
                $em->persist($entity);
                $em->flush();
    
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('companyorganizationalunit_show', array(
                    'id' => $entity->getId()
                )));
                
            }
            else {
                $message = '';
                
                if (current($coCode) > 0) {
                    $message .= "El Código para Unidad Organizativa de la empresa, ya existe." . '</br>';
                }
                
                if (current($coName) > 0) {
                    $message .= "El Nombre para Unidad Organizativa de la empresa, ya existe.";
                }
                
                $session->getFlashBag()->add('error', $message);
                
            }
        }

        return $this->render('FishmanEntityBundle:Companyorganizationalunit:new.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Companyorganizationalunit entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la unidad organizativa de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        $editForm = $this->createForm(new CompanyorganizationalunitType
                    ($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanEntityBundle:Companyorganizationalunit:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Companyorganizationalunit entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la unidad organizativa de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $editForm = $this->createForm(new CompanyorganizationalunitType($entity, $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
            $coCode = $repository->createQueryBuilder('co')
                ->select('count(co.id)')
                ->where('co.company = :company 
                        AND co.code = :code 
                        AND co.id <> :id')
                ->setParameter('company', $entity->getCompany()->getId())
                ->setParameter('code', $entity->getCode())
                ->setParameter('id', $entity->getId())
                ->groupBy('co.id')
                ->getQuery()
                ->getResult();
                
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
            $coName = $repository->createQueryBuilder('co')
                ->select('count(co.id)')
                ->where('co.company = :company 
                        AND co.name = :name 
                        AND co.id <> :id')
                ->setParameter('company', $entity->getCompany()->getId())
                ->setParameter('name', $entity->getName())
                ->setParameter('id', $entity->getId())
                ->groupBy('co.id')
                ->getQuery()
                ->getResult();
            
            if (current($coCode) == 0 && current($coName) == 0) {
          
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                $entity->setModifiedBy($modifiedBy->getId());
                $entity->setChanged(new \DateTime());
    
                $em->persist($entity);
                $em->flush();
                                                    
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('companyorganizationalunit_show', array(
                    'id' => $id
                )));
                
            }
            else {
                $message = '';
                
                if (current($coCode) > 0) {
                    $message .= "El Código para Unidad Organizativa de la empresa, ya existe." . '</br>';
                }
                
                if (current($coName) > 0) {
                    $message .= "El Nombre para Unidad Organizativa de la empresa, ya existe.";
                }
                
                $session->getFlashBag()->add('error', $message);
                
            }
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanEntityBundle:Companyorganizationalunit:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Companyorganizationalunit entity.
     *
     */
    public function deleteAction(Request $request, $id, $companyid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la unidad organizativa de la empresa.');
                return $this->redirect($this->generateUrl('companyorganizationalunit', array(
                    'companyid' => $companyid
                )));
            }
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $wi = $repository->createQueryBuilder('wi')
                ->select('count(wi.companyorganizationalunit)')
                ->where('wi.companyorganizationalunit = :companyorganizationalunit')
                ->setParameter('companyorganizationalunit', $entity->getId())
                ->groupBy('wi.companyorganizationalunit')
                ->getQuery()
                ->getResult();
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
            $cou = $repository->createQueryBuilder('cou')
                ->select('count(cou.parent_id)')
                ->where('cou.parent_id = :companyorganizationalunit')
                ->setParameter('companyorganizationalunit', $entity->getId())
                ->groupBy('cou.parent_id')
                ->getQuery()
                ->getResult();
            
            if (current($wi) == 0 && current($cou) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La Unidad Organizativa de la empresa está en uso.');
            }

        }

        return $this->redirect($this->generateUrl('companyorganizationalunit', array(
          'companyid' => $companyid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Companyorganizationalunit entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la unidad organizativa de la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Companyorganizationalunit:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Import a company Organizationalunit entity.
     *
     */
    public function importAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $defaultData = array();
        $form = $this->createForm(new CompanyorganizationalunitimportType(), $defaultData);

        $form2 = false;
        $data = '';
        $ous = false;
        $numberRegister = 0;

        if ($request->isMethod('POST')) {
            //The user is confirmed to persist data as Companyorganizationalunit entity
            $importFormData = $request->request->get('form', false);

            if($importFormData){
                $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                     ->find($importFormData['temporalimportdata_id']);
                if(!$temporalentity){
                  return $this->render('FishmanEntityBundle:Companyorganizationalunit:import.html.twig', array(
                         'company' => $company,
                         'form' => $form->createView(),
                         'form2' => $form2,
                         'ous' => $ous,
                  ));
                }

                $ous = $temporalentity->getData();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    foreach ($ous as $ouarray){
                        //If the companyorganizationalunitcode exists update this
                        $query = $em->createQuery(
                            'SELECT c FROM FishmanEntityBundle:Companyorganizationalunit c 
                             WHERE c.company = :company
                               AND c.code = :code')
                             ->setParameter('company', $company)
                             ->setParameter('code', $ouarray['code']);
    
                        try {
                            $companyou = $query->getSingleResult();
                        } catch (\Doctrine\Orm\NoResultException $e) {
                            $companyou = false;
                        }
    
                        $userBy = $this->get('security.context')->getToken()->getUser();
                        if(!$companyou){
                            $companyou = new Companyorganizationalunit();
                            $companyou->setCreatedBy($userBy->getId());
                            $companyou->setCreated(new \DateTime());
                        }
    
    
                        //TODO: When create companyou manual, then use the same logic
    
                        $companyou->setCode($ouarray['code']);
                        $companyou->setName($ouarray['name']);
                        $companyou->setStatus($ouarray['status']);
                        $ou = $em->getRepository('FishmanEntityBundle:Organizationalunit')->find($ouarray['organizationalunit_id']);
                        $companyou->setOrganizationalunit($ou);
                        $companyou->setType($ouarray['type']);
                        if ($ouarray['unit_type'] != '') {
                            $companyou->setUnitType($ouarray['unit_type']);
                        }
                        else {
                            $companyou->setUnitType('administrative');
                        }
                        $companyou->setBoss($ouarray['ou_boss_id']);
                        $companyou->setParentId($ouarray['ou_parent_id']);
    
                        // User
                        $companyou->setModifiedBy($userBy->getId());
                        $companyou->setCompany($company);
                        $companyou->setCreated(new \DateTime());
                        $companyou->setChanged(new \DateTime());
                
                        $em->persist($companyou);
                        $em->flush();
    
                        if ($ouarray['valid'] != 0) {
                          $numberRegister = $numberRegister + 1;
                        }
    
                    } //end foreach
    
                    //Delete temporal data
                    $em->remove($temporalentity);
                    $em->flush();
                    
                    // Delete temporal files
                    Company::deleteTemporalFiles('../tmp/');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }

                $session = $this->getRequest()->getSession();
                if ($numberRegister > 0) {
                    $session->getFlashBag()->add('status', 'Se grabó la importación de '.$numberRegister.' Unidad(es) Organizativa(s).');
                }
                else {
                    $session->getFlashBag()->add('error', 'No se logró importar ninguna Unidad Organizativa.');
                }

                if($request->request->get('saveandclose', true)){
                    return $this->redirect($this->generateUrl('companyorganizationalunit', array('companyid' => $companyid)));
                }

                $ous = false;

            } //End have temporarlimportdata input
            else{
                $form->bind($request);
                $data = $form->getData();

                // We need to move the file in order to use it
                $randomName = rand(1, 10000000000);
                $randomName = $randomName . '.xlsx';
                $directory = __DIR__.'/../../../../tmp';
                $data['file']->move($directory, $randomName);

                if (null !== $data['file']) {
                    $fileName = $data['file']->getClientOriginalName();
                    $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                }

                if ($data['file']->document == 'xlsx') { 

                    $inputFileName = $directory . '/' . $randomName;
                    $phpExcel = PHPExcel_IOFactory::load($inputFileName);
    
                    $worksheet = $phpExcel->getSheet(0);
    
                    $row = 2;
                    $ous = array();
                    do {
                        $code = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                        $name = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                        $ou_id = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                        $boss_id = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                        $parent_id = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                        $type = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
                        $unit_type = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
                        $message = '';
                        $valido = 1;
                        $ou_name = '';
                        $ouBossId = '';
                        $ou_parent_id = '';
                        $ou_parent_name = '';
                        $ouBossName = '';
                        $row++;
                        
                        if ($code == '' && $name == '' && $ou_id == '' && $boss_id == '' && $parent_id == '') break;
                        if ($code == '') {
                            $message .= '<p>Falta el código de la unidad organizativa</p>';
                            $valido = 0;
                        }
                        else {
                            // Check if you have legacy
                            $cou_code = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit')->findOneBy(
                                array(
                                    'code' => $code,
                                    'company' => $companyid
                                )
                            );
                            if (!empty($cou_code)) {
                                $message .= '<p>El código de la unidad organizativa ya está siendo usado en otra unidad de la empresa</p>';
                                $valid = 0;
                            }
                        }
    
                        if ($name == '') {
                            $message .= '<p>Falta el nombre de la unidad organizativa</p>';
                            $valido = 0;
                        }
                        
                        if ($ou_id == '') {
                            $message .= '<p>Falta el id del nombre global</p>';
                        }
                        elseif (!preg_match('/^\d+$/',$ou_id)) { //Only digits
                            $message .= '<p>El id del nombre global debe ser un número entero</p>';           
                            //$valido = 0;
                        }
                        else {
                            //TODO: Change query in order to find only the name ()
                            $ou = $em->getRepository('FishmanEntityBundle:Organizationalunit')->find($ou_id);
                            if($ou){
                                $ou_name = $ou->getName();
                            }
                        }
    
                        if ($type == '') {
                            $message .= '<p>Falta el tipo de nivel de la unidad organizativa</p>';
                        }
    
                        if ($unit_type == '') {
                            $message .= '<p>Falta el tipo de unidad de la unidad organizativa, por defectose guardara como "Administrativo"</p>';
                        }
                        else {
                            if (!in_array($unit_type, array('academic', 'administrative'))) {
                                $message .= '<p>El tipo de unidad es inválido, por defectose guardara como "Administrativo"</p>';
                            }
                        }
    
                        if ($boss_id == '') {
                            $message .= '<p>Falta el responsable de la unidad organizativa</p>';
                        }
                        else {
                            //TODO: Change query in order to find only the name ()
                            //TODO: Boss id should be working information id and not user id
                            try{
                                $ouboss = $em->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(
                                    array('code' => $boss_id , 'company' => $companyid , 'status' => 1));
                            } catch (\Doctrine\Orm\NoResultException $e){
                                $ouboss = false; 
                            }
                            
                            if ($ouboss) {
                                $ouBossId = $ouboss->getId();
                                $ouBossName = $ouboss->getUser()->getNames() .' '. $ouboss->getUser()->getSurname() .' '. $ouboss->getUser()->getlastName();
                            }
                            else {
                                $message .= '<p>El código de responsable pertenece a otra empresa</p>';
                            }
    
                        }
                        
                        if ($parent_id == '') {
                            $ou_parent_id = -1;
                            $ou_parent_name = '';
                        }
                        else {
                            // Check if you have legacy
                            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
                            $result = $repository->createQueryBuilder('cou')
                                ->where('cou.code = :code 
                                        AND cou.company = :company')
                                ->setParameter('code', $parent_id)
                                ->setParameter('company', $companyid)
                                ->getQuery()
                                ->getResult();
                                
                            $ouparent = current($result);
                            if ($ouparent) {
                                $ou_parent_id = $ouparent->getId();
                                $ou_parent_name = $ouparent->getName();
                            }
                            else {
                                $message .= '<p>La unidad organizativa padre no existe o no pertenece a esta empresa</p>';
                            }
                        }
    
                        if($valido && $message == ''){
                            $message = 'OK';
                        }
    
                        $ous[] = array('code' => $code,
                                       'name' => $name,
                                       'ou_id' => $ou_id,
                                       'ou_name' => $ou_name,
                                       'organizationalunit_id' => $ou_id,
                                       'ou_boss_id' => $ouBossId,
                                       'ou_boss_name' => $ouBossName,
                                       'type' => $type,
                                       'unit_type' => $unit_type,
                                       'ou_parent_id' => $ou_parent_id,
                                       'ou_parent_name' => $ou_parent_name,
                                       'valid' => $valido,
                                       'status' => 1,
                                       'message' => $message);
                    } while (true);
                }
                else {
                    $session->getFlashBag()->add('status', 'el archivo a importar debe ser de formato excel');
                }


                if(count($ous) > 0 and is_array($ous)){
                    $temporal = new Temporalimportdata();
                    $temporal->setData($ous);
                    $temporal->setEntity('companyorganizationalunit');
                    $temporal->setDatetime(new \DateTime());
                    $em->persist($temporal);
                    $em->flush();

                    // Secondary form
                    $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                    $form2 = $this->createFormBuilder($defaultData2)
                        ->add('temporalimportdata_id',
                              'integer', array (
                                       'required' => true 
                             ))
                        ->getForm();
                    $form2 = $form2->createView();
                }
                else{
                    //Mensaje de que no hay registros
                    $session = $this->getRequest()->getSession();
                    $session->getFlashBag()->add('status', 'No hay cargos para importar.');
                }
                //Borrar archivo temporal
                if ($data['file']->document == 'xlsx') {
                    unlink($inputFileName);
                }
            }
        } //End is method post

        return $this->render('FishmanEntityBundle:Companyorganizationalunit:import.html.twig', array(
            'company' => $company,
            'form' => $form->createView(),
            'form2' => $form2,
            'ous' => $ous,
        ));
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="importar-empresa-unidades-organizativas.xlsx'.'"'
        );
        return new Response(file_get_contents( __DIR__.'/../../../../templates/EntityBundle/Company/importar-empresa-unidades-organizativas.xlsx'), 200, $headers);
    }

    public function ajaxChoiceByCompanyAction()
    {
        $companyId = $_GET['company_id'];

        $repository = $this->getDoctrine()->getManager()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $cous = $repository->findActiveByCompany($companyId);

        $selectCompanyOrganizationalUnit = '<select class="companyorganizationalunit" name="company_organization_unit">';
        $selectCompanyOrganizationalUnit .= '<option value="">SELECCIONE UNA OPCIÓN</option>';

        if($cous){
            foreach($cous as $cou){
                $selectCompanyOrganizationalUnit .= '<option value="' . $cou['id'] . '">' .  $cou['name'] . '</option>';
            }
        }
        $selectCompanyOrganizationalUnit .= '</select>';

        $response = new Response(json_encode(
            array('select_companyorganizationalunit' => $selectCompanyOrganizationalUnit)));  
        return $response;
    }
                   
    public function ajaxChoiceByparentAction()
    {
        $parentId = $_GET['parent_id'];
        $repository = $this->getDoctrine()->getManager()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $cous = $repository->findActiveByParent($parentId);
        
        $selectCompanyOrganizationalUnit = '<select class="companyorganizationalunit" name="company_organization_unit">';
        $selectCompanyOrganizationalUnit .= '<option value="">SELECCIONE UNA OPCIÓN</option>';

        if($cous){
            foreach($cous as $cou){
                $selectCompanyOrganizationalUnit .= '<option value="' . $cou['id'] . '">' .  $cou['name'] . '</option>';
            }
        }
        $selectCompanyOrganizationalUnit .= '</select>';

        $response = new Response(json_encode(
            array('select_companyorganizationalunit' => $selectCompanyOrganizationalUnit)));  
        return $response;
    }

    public function ajaxChoiceByparent2Action()
    {
        $parentId = $_GET['parent_id'];
        $unitTypeDefault = '';
        $em = $this->getDoctrine()->getManager();

        $cou = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->findOneBy(
          array('id' => $parentId));

        if ($cou) {
            $selectUnitType = '<select class="unitTypeDefault" disabled name="">';
            $selectUnitType .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
            
            if ($cou->getUnitType()) {
                $unitTypeDefault = $cou->getUnitType();
                switch ($unitTypeDefault) {
                    case 'academic':
                        $unitType = 'Academica';
                        break;
                    case 'administrative':
                        $unitType = 'Administrativa';
                        break;
                }
                $selectUnitType .= '<option selected value="' . $cou->getUnitType() . '">' .  $unitType . '</option>';
            }
            else {
                $unitTypeDefault = 'administrative';
                $selectUnitType .= '<option selected value="administrative">Administrativa</option>';
            }

        }
        else {
            $selectUnitType = '<select class="unitTypeDefault" name="">';
            $selectUnitType .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
            $selectUnitType .= '<option value="administrative">Administrativa</option>';
            $selectUnitType .= '<option value="academic">Academica</option>';
        }
        $selectUnitType .= '</select>';

        $response = new Response(json_encode(
            array('select_unittype' => $selectUnitType, 'unittype' => $unitTypeDefault )));  
        return $response;
    }
}
