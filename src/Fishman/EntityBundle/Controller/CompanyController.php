<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Company;
use Fishman\EntityBundle\Entity\Nature;
use Fishman\EntityBundle\Form\CompanyType;
use Ideup\SimplePaginatorBundle\Paginator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

/**
 * Company controller.
 *
 */
class CompanyController extends Controller
{
    /**
     * Lists all Company entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $status_options = array('ruc' => 'Desactivo', 1 => 'Activo');
        $nature_options = Nature::getListNatureOptions($this->getDoctrine());
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'nature' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('nature', 'choice', array(
                'choices' => $nature_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'nature' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.code, c.name, c.ruc, n.name nature, c.contact, c.phone, c.email, 
                      c.changed, c.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'c.modified_by = u.id')
            ->leftJoin('c.nature', 'n')
            ->where('c.id LIKE :id 
                    OR c.code LIKE :code 
                    OR c.name LIKE :name 
                    OR c.ruc LIKE :ruc 
                    OR c.contact LIKE :contact 
                    OR c.phone LIKE :phone 
                    OR c.email LIKE :email')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->setParameter('ruc', '%' . $data['word'] . '%')
            ->setParameter('contact', '%' . $data['word'] . '%')
            ->setParameter('phone', '%' . $data['word'] . '%')
            ->setParameter('email', '%' . $data['word'] . '%')
            ->orderBy('c.id', 'ASC');
        
        // Add arguments
        
        $queryBuilder
            ->andWhere('c.id <> 2');
        
        if ($data['nature'] != '') {
            $queryBuilder
                ->andWhere('c.nature = :nature')
                ->setParameter('nature', $data['nature']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('c.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'company');
        $paginator->setMaxPagerItems(5, 'company');
        $entities = $paginator->paginate($query, 'company')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('company');
        $endPageItem = $paginator->getEndPageItem('company');
        $totalItems = $paginator->getTotalItems('company');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanEntityBundle:Company:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Company entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.code, c.company_type, c.name, c.ruc, i.name industry, n.name nature, c.contact, c.phone, c.annex, c.celphone, c.email, 
                      vb.rank volume_business, ne.rank number_employees, rp.rank range_pension, c.type, c.gender, 
                      ns.rank number_students, ra.name religious_affiliation, c.institutional_affiliation, c.education_system, 
                      c.internal_school, c.status, c.created, c.changed, c.created_by, c.modified_by, c.image')
            ->innerJoin('c.industry', 'i')
            ->innerJoin('c.nature', 'n')
            ->leftJoin('FishmanEntityBundle:Volumebusiness', 'vb', 'WITH', 'c.volume_business = vb.id')
            ->leftJoin('FishmanEntityBundle:Numberpeople', 'ne', 'WITH', 'c.number_employees = ne.id')
            ->leftJoin('FishmanEntityBundle:Rangepension', 'rp', 'WITH', 'c.range_pension = rp.id')
            ->leftJoin('FishmanEntityBundle:Numberpeople', 'ns', 'WITH', 'c.number_students = ns.id')
            ->leftJoin('FishmanEntityBundle:Religiousaffiliation', 'ra', 'WITH', 'c.religious_affiliation = ra.id')
            ->where('c.id = :id')
            ->setParameter('id', $id);
        
        $queryBuilder
            ->andWhere('c.id <> 2');
        
        $result = $queryBuilder->getQuery()->getResult();
            
        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        if (!empty($entity['institutional_affiliation'])) {
            $i = 0;
            foreach ($entity['institutional_affiliation'] as $r) {
                if ($i == 0) {
                    $iaIds = $r;
                }
                else {
                    $iaIds .= ', ' . $r;
                }
                $i++;
            }
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Institutionalaffiliation');
            $query = $repository->createQueryBuilder('ia')
                ->select('ia.id, ia.name')
                ->where('ia.id IN(' . $iaIds . ')')
                ->orderBy('ia.id')
                ->getQuery()
                ->getResult();
            $entity['institutional_affiliation'] = $query;
        }

       // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanEntityBundle:Company:show.html.twig', array(
            'entity'  => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Company entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $nature_array = Nature::getListNature($this->getDoctrine());
        $json_nature = json_encode($nature_array);
        
        // Range pension
        $pensionsSchool = $em->getRepository('FishmanEntityBundle:Rangepension')->findByType('school');
        $pensionsUniversity = $em->getRepository('FishmanEntityBundle:Rangepension')->findByType('university');
        
        // Type
        $typesSchool = array('particular' => 'Particular', 'national' => 'Nacional');
        $typesUniversity = array('particular' => 'Particular', 'national' => 'Nacional');
        
        // Number Students
        $numberstudentsSchool = $em->getRepository('FishmanEntityBundle:Numberpeople')->findByType('school');
        $numberstudentsUniversity = $em->getRepository('FishmanEntityBundle:Numberpeople')->findByType('university');
        
        $entity = new Company();
        $entity->setReligiousAffiliationOn(1);
        $entity->setInternalSchool(1);
        $entity->setStatus(1);
        $form   = $this->createForm(new CompanyType($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanEntityBundle:Company:new.html.twig', array(
            'json_nature' => $json_nature,
            'entity' => $entity,
            'pensionsSchool' => $pensionsSchool, 
            'pensionsUniversity' => $pensionsUniversity, 
            'typesSchool' => $typesSchool, 
            'typesUniversity' => $typesUniversity, 
            'numberstudentsSchool' => $numberstudentsSchool, 
            'numberstudentsUniversity' => $numberstudentsUniversity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Company entity.
     *
     */
    public function createAction(Request $request)
    {
        $nature_array = Nature::getListNature($this->getDoctrine());
        $companyData = $request->request->get('company_data');
        
        $entity  = new Company();
        
        $form = $this->createForm(new CompanyType($entity, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            // set flash messages
            $session = $this->getRequest()->getSession();
            
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company');
            $code = $repository->createQueryBuilder('c')
                ->where('c.code = :code 
                        AND c.id <> 2')
                ->setParameter('code', $entity->getCode())
                ->getQuery()
                ->getResult();
            
            if (!$code) {
                
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                $typeEntity = $nature_array[$entity->getNature()->getId()]['type'];
                if ($typeEntity == 'working') {
                    $entity->setCompanyType('working');
                    $entity->setRangePension(NULL);
                    $entity->setType(NULL);
                    $entity->setGender(NULL);
                    $entity->setNumberStudents(NULL);
                    $entity->setReligiousAffiliationOn(NULL);
                    $entity->setReligiousAffiliation(NULL);
                    $entity->setInstitutionalAffiliation(NULL);
                    $entity->setEducationSystem(NULL);
                    $entity->setInternalSchool(NULL);
                }
                elseif ($typeEntity == 'school') {
                    $entity->setCompanyType('school');
                    $entity->setRangePension($companyData['range_pension_school']);
                    $entity->setType($companyData['type_school']);
                    $entity->setNumberStudents($companyData['number_students_school']);
                }
                elseif ($typeEntity == 'university') {
                    $entity->setCompanyType('university');
                    $entity->setRangePension($companyData['range_pension_university']);
                    $entity->setType($companyData['type_university']);
                    $entity->setNumberStudents($companyData['number_students_university']);
                    $entity->setGender(NULL);
                    $entity->setReligiousAffiliationOn(NULL);
                    $entity->setReligiousAffiliation(NULL);
                    $entity->setInstitutionalAffiliation(NULL);
                    $entity->setEducationSystem(NULL);
                    $entity->setInternalSchool(NULL);
                }
                $entity->setCreatedBy($userBy->getId());
                $entity->setModifiedBy($userBy->getId());
                $entity->setCreated(new \DateTime());
                $entity->setChanged(new \DateTime());
                
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
                
                return $this->redirect($this->generateUrl('company_show', array(
                    'id' => $entity->getId()
                )));
                
            }
            else {
                $session->getFlashBag()->add('error', 'El código pertenece a otra empresa.');
            }
        }
        
        $json_nature = json_encode($nature_array);
        
        // Range pension
        $pensionsSchool = $em->getRepository('FishmanEntityBundle:Rangepension')->findByType('school');
        $pensionsUniversity = $em->getRepository('FishmanEntityBundle:Rangepension')->findByType('university');
        
        // Type
        $typesSchool = array('particular' => 'Particular', 'national' => 'Nacional');
        $typesUniversity = array('particular' => 'Particular', 'national' => 'Nacional');
        
        // Number Students
        $numberstudentsSchool = $em->getRepository('FishmanEntityBundle:Numberpeople')->findByType('school');
        $numberstudentsUniversity = $em->getRepository('FishmanEntityBundle:Numberpeople')->findByType('university');
        
        $entity = new Company();
        $entity->setReligiousAffiliationOn(1);
        $entity->setInternalSchool(1);
        $entity->setStatus(1);
        $form   = $this->createForm(new CompanyType($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanEntityBundle:Company:new.html.twig', array(
            'json_nature' => $json_nature,
            'entity' => $entity,
            'pensionsSchool' => $pensionsSchool, 
            'pensionsUniversity' => $pensionsUniversity, 
            'typesSchool' => $typesSchool, 
            'typesUniversity' => $typesUniversity, 
            'numberstudentsSchool' => $numberstudentsSchool, 
            'numberstudentsUniversity' => $numberstudentsUniversity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Company entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $nature_array = Nature::getListNature($this->getDoctrine());
        $json_nature = json_encode($nature_array);

        $entity = $em->getRepository('FishmanEntityBundle:Company')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        // Range pension
        $pensionsSchool = $em->getRepository('FishmanEntityBundle:Rangepension')->findByType('school');
        $pensionsUniversity = $em->getRepository('FishmanEntityBundle:Rangepension')->findByType('university');
        $current_pension = $entity->getRangePension();
        
        // Type
        $typesSchool = array('particular' => 'Particular', 'national' => 'Nacional');
        $typesUniversity = array('particular' => 'Particular', 'national' => 'Nacional');
        $current_type = $entity->getType();
        
        // Number Students
        $numberstudentsSchool = $em->getRepository('FishmanEntityBundle:Numberpeople')->findByType('school');
        $numberstudentsUniversity = $em->getRepository('FishmanEntityBundle:Numberpeople')->findByType('university');
        $current_numberstudents = $entity->getNumberStudents();

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $entity->setReligiousAffiliationOn(1);
        $entity->setInternalSchool(1);
        $editForm = $this->createForm(new CompanyType($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanEntityBundle:Company:edit.html.twig', array(
            'json_nature' => $json_nature,
            'entity' => $entity,
            'pensionsSchool' => $pensionsSchool, 
            'pensionsUniversity' => $pensionsUniversity, 
            'current_pension' => $current_pension, 
            'typesSchool' => $typesSchool, 
            'typesUniversity' => $typesUniversity, 
            'current_type' => $current_type, 
            'numberstudentsSchool' => $numberstudentsSchool, 
            'numberstudentsUniversity' => $numberstudentsUniversity, 
            'current_numberstudents' => $current_numberstudents, 
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Company entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $nature_array = Nature::getListNature($this->getDoctrine());
        $companyData = $request->request->get('company_data');
        
        $entity = $em->getRepository('FishmanEntityBundle:Company')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $editForm = $this->createForm(new CompanyType($entity, $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // set flash messages
            $session = $this->getRequest()->getSession();
            
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company');
            $code = $repository->createQueryBuilder('c')
                ->where('c.code = :code 
                        AND c.id NOT IN(2, :id)')
                ->setParameter('code', $entity->getCode())
                ->setParameter('id', $entity->getId())
                ->getQuery()
                ->getResult();
            
            if (!$code) {
                
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                $typeEntity = $nature_array[$entity->getNature()->getId()]['type'];
                if ($typeEntity == 'working') {
                    $entity->setCompanyType('working');
                    $entity->setRangePension(NULL);
                    $entity->setType(NULL);
                    $entity->setGender(NULL);
                    $entity->setNumberStudents(NULL);
                    $entity->setReligiousAffiliationOn(NULL);
                    $entity->setReligiousAffiliation(NULL);
                    $entity->setInstitutionalAffiliation(NULL);
                    $entity->setEducationSystem(NULL);
                    $entity->setInternalSchool(NULL);
                }
                elseif ($typeEntity == 'school') {
                    $entity->setCompanyType('school');
                    $entity->setRangePension($companyData['range_pension_school']);
                    $entity->setType($companyData['type_school']);
                    $entity->setNumberStudents($companyData['number_students_school']);
                }
                elseif ($typeEntity == 'university') {
                    $entity->setCompanyType('university');
                    $entity->setRangePension($companyData['range_pension_university']);
                    $entity->setType($companyData['type_university']);
                    $entity->setNumberStudents($companyData['number_students_university']);
                    $entity->setGender(NULL);
                    $entity->setReligiousAffiliationOn(NULL);
                    $entity->setReligiousAffiliation(NULL);
                    $entity->setInstitutionalAffiliation(NULL);
                    $entity->setEducationSystem(NULL);
                    $entity->setInternalSchool(NULL);
                }
                $entity->setModifiedBy($modifiedBy->getId());
                $entity->setChanged(new \DateTime());
                
                $em->persist($entity);
                $em->flush();
                
                // set flash messages
                $session = $this->getRequest()->getSession();                                                
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
      
                return $this->redirect($this->generateUrl('company_show', array(
                    'id' => $id
                )));
                
            }
            else {
                $session->getFlashBag()->add('error', 'El código pertenece a otra empresa.');
            }
        }
        
        $json_nature = json_encode($nature_array);
        
        // Range pension
        $pensionsSchool = $em->getRepository('FishmanEntityBundle:Rangepension')->findByType('school');
        $pensionsUniversity = $em->getRepository('FishmanEntityBundle:Rangepension')->findByType('university');
        $current_pension = $entity->getRangePension();
        
        // Type
        $typesSchool = array('particular' => 'Particular', 'national' => 'Nacional');
        $typesUniversity = array('particular' => 'Particular', 'national' => 'Nacional');
        $current_type = $entity->getType();
        
        // Number Students
        $numberstudentsSchool = $em->getRepository('FishmanEntityBundle:Numberpeople')->findByType('school');
        $numberstudentsUniversity = $em->getRepository('FishmanEntityBundle:Numberpeople')->findByType('university');
        $current_numberstudents = $entity->getNumberStudents();

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $entity->setReligiousAffiliationOn(1);
        $entity->setInternalSchool(1);
        $editForm = $this->createForm(new CompanyType($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanEntityBundle:Company:edit.html.twig', array(
            'json_nature' => $json_nature,
            'entity' => $entity,
            'pensionsSchool' => $pensionsSchool, 
            'pensionsUniversity' => $pensionsUniversity, 
            'current_pension' => $current_pension, 
            'typesSchool' => $typesSchool, 
            'typesUniversity' => $typesUniversity, 
            'current_type' => $current_type, 
            'numberstudentsSchool' => $numberstudentsSchool, 
            'numberstudentsUniversity' => $numberstudentsUniversity, 
            'current_numberstudents' => $current_numberstudents, 
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Company entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanEntityBundle:Company')->find($id);
            if (!$entity || $id == 2) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
                return $this->redirect($this->generateUrl('company'));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycharge');
            $cc = $repository->createQueryBuilder('cc')
                ->select('count(cc.company)')
                ->where('cc.company = :company')
                ->setParameter('company', $entity->getId())
                ->groupBy('cc.company')
                ->getQuery()
                ->getResult();

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyorganizationalunit');
            $co = $repository->createQueryBuilder('co')
                ->select('count(co.company)')
                ->where('co.company = :company')
                ->setParameter('company', $entity->getId())
                ->groupBy('co.company')
                ->getQuery()
                ->getResult();
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyfaculty');
            $cf = $repository->createQueryBuilder('cf')
                ->select('count(cf.company)')
                ->where('cf.company = :company')
                ->setParameter('company', $entity->getId())
                ->groupBy('cf.company')
                ->getQuery()
                ->getResult();
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycareer');
            $ccr = $repository->createQueryBuilder('ccr')
                ->select('count(ccr.company)')
                ->where('ccr.company = :company')
                ->setParameter('company', $entity->getId())
                ->groupBy('ccr.company')
                ->getQuery()
                ->getResult();
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycourse');
            $cco = $repository->createQueryBuilder('cco')
                ->select('count(cco.company)')
                ->where('cco.company = :company')
                ->setParameter('company', $entity->getId())
                ->groupBy('cco.company')
                ->getQuery()
                ->getResult();
            
            // set flash messages
            $session = $this->getRequest()->getSession();
            
            if (current($cc) == 0 && current($co) == 0 && current($cf) == 0 && current($ccr) == 0 && current($cco) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'El Cargo o la Unidad Organizativa de la empresa están en uso.');
            }

        }

        return $this->redirect($this->generateUrl('company'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Company entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanEntityBundle:Company')->find($id);
        if (!$entity || $id == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Company:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Download document.
     * TODO: Access control. The user can access actual Workshpapplication documents
     */
    public function downloadAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanEntityBundle:Company')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se encontró el documento buscado.');
            return $this->redirect($this->generateUrl('company'));
        }

        $file = new File($entity->getAbsolutePath());
        $headers = array(
            'Content-Type' => $file->getMimeType(),
            'Content-Disposition' => 'attachment; filename="'.$entity->getName().'.'.$file->getExtension().'"'
        );
        return new Response(file_get_contents($entity->getAbsolutePath()), 200, $headers);
    }
    
    /**
     * Export Result of the Report.
     *
     */
    public function exportAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        
        // Recover company
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($id);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('company'));
        }

        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
        $workinginformations = $repository->createQueryBuilder('wi')
            ->select('wi.id, wi.type, u.username nick, u.surname, u.lastname, u.names, u.sex, u.identity, u.numberidentity, 
                    u.birthday, u.birthplace, u.marital_status, u.phone, u.email, h.name headquarter, 
                    wi.boss, b.surname b_surname, b.lastname b_lastname, b.names b_names, cch.name companycharge, 
                    cou.name companyou, cou.type companyou_type, cf.name companyfaculty, cc.name companycareer, 
                    wi.academic_year, wi.grade, wi.study_year')
            ->innerJoin('wi.user', 'u')
            ->innerJoin('wi.headquarter', 'h')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'bwi', 'WITH', 'wi.boss = bwi.id')
            ->leftJoin('bwi.user', 'b')
            ->leftJoin('wi.companycharge', 'cch')
            ->leftJoin('wi.companyorganizationalunit', 'cou')
            ->leftJoin('wi.companyfaculty', 'cf')
            ->leftJoin('wi.companycareer', 'cc')
            ->where('wi.company = :company')
            ->setParameter('company', $id)
            ->orderBy('u.surname', 'ASC', 'u.lastname', 'ASC', 'u.names', 'ASC')
            ->getQuery()
            ->getResult();

        //$workinginformations = $em->getRepository('FishmanAuthBundle:Workinginformation')->findByCompany($id);
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Empresa");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta la lista de personas de la entidad (Empresa / Colegio / Universidad");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('EMPRESA');

        $worksheet = $phpExcel->getSheet(0);

        $worksheet->getCellByColumnAndRow(1, 2)->setValue('EMPRESA');
        $worksheet->getCellByColumnAndRow(1, 4)->setValue($company->getName());

        if ($workinginformations) {
            
            $r = 7;

            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
            $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
            $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
            $phpExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
            $phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(24);
            $phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
            $phpExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
            $phpExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
            $phpExcel->getActiveSheet()->getColumnDimension('M')->setWidth(40);
            $phpExcel->getActiveSheet()->getColumnDimension('N')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('O')->setWidth(18);
            $phpExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
            if ($company->getCompanyType() == 'university') {
                $phpExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
                $phpExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
                $phpExcel->getActiveSheet()->getColumnDimension('T')->setWidth(18);
            }
            elseif ($company->getCompanyType() == 'school') {
                $phpExcel->getActiveSheet()->getColumnDimension('R')->setWidth(18);
                $phpExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
            }

            $worksheet->getCellByColumnAndRow(1, 6)->setValue('ID');
            $worksheet->getCellByColumnAndRow(2, 6)->setValue('NICK');
            $worksheet->getCellByColumnAndRow(3, 6)->setValue('NOMBRES Y APELLIDOS');
            $worksheet->getCellByColumnAndRow(4, 6)->setValue('SEXO');
            $worksheet->getCellByColumnAndRow(5, 6)->setValue('TIPO DOCUMENTO');
            $worksheet->getCellByColumnAndRow(6, 6)->setValue('NRO DOCUMENTO');
            $worksheet->getCellByColumnAndRow(7, 6)->setValue('FECHA NACIMIENTO');
            $worksheet->getCellByColumnAndRow(8, 6)->setValue('LUGAR NACIMIENTO');
            $worksheet->getCellByColumnAndRow(9, 6)->setValue('ESTADO CIVIL');
            $worksheet->getCellByColumnAndRow(10, 6)->setValue('TELÉFONO');
            $worksheet->getCellByColumnAndRow(11, 6)->setValue('CORREO');
            $worksheet->getCellByColumnAndRow(12, 6)->setValue('SEDE');
            $worksheet->getCellByColumnAndRow(13, 6)->setValue('JEFE');
            $worksheet->getCellByColumnAndRow(14, 6)->setValue('CARGO');
            $worksheet->getCellByColumnAndRow(15, 6)->setValue('UNIDAD ORGANIZATIVA');
            $worksheet->getCellByColumnAndRow(16, 6)->setValue('TIPO UNIDAD');
            if ($company->getCompanyType() == 'university') {
                $worksheet->getCellByColumnAndRow(17, 6)->setValue('FACULTAD');
                $worksheet->getCellByColumnAndRow(18, 6)->setValue('CARRERA');
                $worksheet->getCellByColumnAndRow(20, 6)->setValue('CICLO ACADÉMICO');
            }
            elseif ($company->getCompanyType() == 'school') {
                $worksheet->getCellByColumnAndRow(17, 6)->setValue('GRADO');
                $worksheet->getCellByColumnAndRow(18, 6)->setValue('AÑO DE ESTUDIO');
            }

            foreach ($workinginformations as $wi) {

                $i = 1;
                $name = $wi['surname'] . ' ' . $wi['lastname'] . ' ' . $wi['names'];
                $birthday = '';
                if (!is_null($wi['birthday'])) {
                    $birthday = date('Y-m-d', strtotime($wi['birthday']->format('d/m/Y')));
                }

                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['id']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['nick']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($name);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['sex']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['identity']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['numberidentity']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($birthday);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['birthplace']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['marital_status']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['phone']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['email']);
                $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['headquarter']);
                if ($wi['boss'] && !is_null($wi['boss'])) {
                    $boss_name = $wi['b_surname'] . ' ' . $wi['b_lastname'] . ' ' . $wi['b_names'];
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($boss_name);
                }
                else {
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue('');
                }
                if ($wi['type'] == 'workinginformation') {
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['companycharge']);
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['companyou']);
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['companyou_type']);
                }
                else {
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue('');
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue('');
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue('');
                }
                if ($wi['type'] == 'universityinformation') {
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['companyfaculty']);
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['companycareer']);
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['academic_year']);
                }
                elseif ($wi['type'] == 'schoolinformation') {
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['grade']);
                    $worksheet->getCellByColumnAndRow($i++, $r)->setValue($wi['study_year']);
                }

                $r++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 6)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="empresa-personas.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }

}
