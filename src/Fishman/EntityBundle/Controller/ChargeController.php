<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Charge;
use Fishman\EntityBundle\Form\ChargeType;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Charge controller.
 *
 */
class ChargeController extends Controller
{
    /**
     * Lists all Charge entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Charge');
        $queryBuilder = $repository->createQueryBuilder('ch')
            ->select('ch.id, ch.name, ch.changed, ch.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'ch.modified_by = u.id')
            ->where('ch.id LIKE :id 
                    OR ch.name LIKE :name')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('ch.id', 'ASC');
        
        // Add arguments
        
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('ch.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'charge');
        $paginator->setMaxPagerItems(5, 'charge');
        $entities = $paginator->paginate($query, 'charge')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('charge');
        $endPageItem = $paginator->getEndPageItem('charge');
        $totalItems = $paginator->getTotalItems('charge');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanEntityBundle:Charge:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Charge entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Charge')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el cargo.');
            return $this->redirect($this->generateUrl('charge'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        return $this->render('FishmanEntityBundle:Charge:show.html.twig', array(
            'entity'  => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Charge entity.
     *
     */
    public function newAction()
    {
        $entity = new Charge();
        $entity->setStatus(1);
        $form   = $this->createForm(new ChargeType(), $entity);

        return $this->render('FishmanEntityBundle:Charge:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Charge entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Charge();
        $form = $this->createForm(new ChargeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();

            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('charge_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanEntityBundle:Charge:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Charge entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Charge')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el cargo.');
            return $this->redirect($this->generateUrl('charge'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new ChargeType(), $entity);

        return $this->render('FishmanEntityBundle:Charge:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Charge entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Charge')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el cargo.');
            return $this->redirect($this->generateUrl('charge'));
        }

        $editForm = $this->createForm(new ChargeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            $entity->setModifiedBy($modifiedBy->getId());
            
            $entity->setChanged(new \DateTime());
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('charge_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollquestion:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Charge entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanEntityBundle:Charge')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el cargo.');
                return $this->redirect($this->generateUrl('charge'));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycharge');
            $cc = $repository->createQueryBuilder('cc')
                ->select('count(cc.charge)')
                ->where('cc.charge = :charge')
                ->setParameter('charge', $entity->getId())
                ->groupBy('cc.charge')
                ->getQuery()
                ->getResult();
            
            if (current($cc) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'El Cargo está en uso.');
            }

        }

        return $this->redirect($this->generateUrl('charge'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Charge entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Charge')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el cargo.');
            return $this->redirect($this->generateUrl('charge'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Charge:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }
}
