<?php

namespace Fishman\EntityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Companyfaculty;
use Fishman\EntityBundle\Form\CompanyfacultyType;
use Fishman\EntityBundle\Form\CompanyfacultyimportType;
use Fishman\EntityBundle\Entity\Company;
use Fishman\ImportExportBundle\Entity\Temporalimportdata;
use Ideup\SimplePaginatorBundle\Paginator;
use Symfony\Component\HttpFoundation\Response;

use PHPExcel_IOFactory;

/**
 * Companyfaculty controller.
 *
 */
class CompanyfacultyController extends Controller
{
    /**
     * Lists all Companyfaculty entities.
     *
     */
    public function indexAction(Request $request, $companyid)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Company Info
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company || $companyid == 2) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la universidad.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        // Recovering data
        
        $faculty_options = Companyfaculty::getListFacultyOptions($this->getDoctrine(), $companyid);
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'faculty' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('faculty', 'choice', array(
                'choices' => $faculty_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',  
            'faculty' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyfaculty');
        $queryBuilder = $repository->createQueryBuilder('cf')
            ->select('cf.id, cf.code, cf.name, f.name faculty, cf.changed, cf.status, u.names, u.surname, u.lastname')
            ->leftJoin('cf.faculty', 'f')
            ->innerJoin('FishmanAuthBundle:User', 'u')
            ->where('cf.modified_by = u.id')
            ->andWhere('cf.company = :company')
            ->andWhere('cf.id LIKE :id 
                    OR cf.code LIKE :code 
                    OR cf.name LIKE :name')
            ->setParameter('company', $companyid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('cf.id', 'ASC');
        
        // Add arguments
        
        if ($data['faculty'] != '') {
            $queryBuilder
                ->andWhere('f.id = :faculty')
                ->setParameter('faculty', $data['faculty']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('f.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'companyfaculty');
        $paginator->setMaxPagerItems(5, 'companyfaculty');
        $entities = $paginator->paginate($query, 'companyfaculty')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('companyfaculty');
        $endPageItem = $paginator->getEndPageItem('companyfaculty');
        $totalItems = $paginator->getTotalItems('companyfaculty');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanEntityBundle:Companyfaculty:index.html.twig', array(
            'entities' => $entities,
            'company' => $company,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Companyfaculty entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la facultad de universidad.');
            return $this->redirect($this->generateUrl('company'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanEntityBundle:Companyfaculty:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Companyfaculty entity.
     *
     */
    public function newAction($companyid)
    {
        $entity = new Companyfaculty();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la universidad.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $entity->setCompany($company);
        $entity->setStatus(1);
        $form = $this->createForm(new CompanyfacultyType(), $entity);

        return $this->render('FishmanEntityBundle:Companyfaculty:new.html.twig', array(
            'entity' => $entity,
            'company' => $company,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new Companyfaculty entity.
     *
     */
    public function createAction(Request $request, $companyid)
    {
        $entity  = new Companyfaculty();
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la universidad.');
            return $this->redirect($this->generateUrl('company'));
        }
        
        $form = $this->createForm(new CompanyfacultyType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCompany($company);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('companyfaculty_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanEntityBundle:Companyfaculty:new.html.twig', array(
            'entity' => $entity,
            'companyid' => $companyid,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Companyfaculty entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la facultad de universidad.');
            return $this->redirect($this->generateUrl('company'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new CompanyfacultyType(), $entity);

        return $this->render('FishmanEntityBundle:Companyfaculty:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Companyfaculty entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la facultad de universidad.');
            return $this->redirect($this->generateUrl('company'));
        }

        $editForm = $this->createForm(new CompanyfacultyType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            $em->persist($entity);
            $em->flush();
                                                        
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('companyfaculty_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanEntityBundle:Companyfaculty:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Companyfaculty entity.
     *
     */
    public function deleteAction(Request $request, $id, $companyid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $entity = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la facultad de universidad.');
                return $this->redirect($this->generateUrl('companyfaculty', array(
                    'company' => $companyid
                )));
            }
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycareer');
            $ccr = $repository->createQueryBuilder('ccr')
                ->select('count(ccr.companyfaculty)')
                ->where('ccr.companyfaculty = :companyfaculty')
                ->setParameter('companyfaculty', $entity->getId())
                ->groupBy('ccr.companyfaculty')
                ->getQuery()
                ->getResult();
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $wi = $repository->createQueryBuilder('wi')
                ->select('count(wi.companyfaculty)')
                ->where('wi.companyfaculty = :companyfaculty')
                ->setParameter('companyfaculty', $entity->getId())
                ->groupBy('wi.companyfaculty')
                ->getQuery()
                ->getResult();
            
            if (current($ccr) == 0 && current($wi) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La Facultad de la empresa está en uso.');
            }

        }

        return $this->redirect($this->generateUrl('companyfaculty', array(
          'companyid' => $companyid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Companyfaculty entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la facultad de universidad.');
            return $this->redirect($this->generateUrl('company'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanEntityBundle:Companyfaculty:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Import a Companyfaculty entity.
     *
     */
    public function importAction(Request $request, $companyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Mensaje de que no hay registros
        $session = $this->getRequest()->getSession();
        
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($companyid);
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la universidad.');
            return $this->redirect($this->generateUrl('company'));
        }

        $defaultData = array();
        $form = $this->createForm(new CompanyfacultyimportType(), $defaultData);

        $form2 = false;
        $data = '';
        $faculties = false;
        $numberRegister = 0;

        if ($request->isMethod('POST')) {
            //The user is confirmed to persist data as Companyfaculty entity
            $importFormData = $request->request->get('form', false);
            
            if ($importFormData) {
                $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                     ->find($importFormData['temporalimportdata_id']);
                if (!$temporalentity) {
                    return $this->render('FishmanEntityBundle:Companyfaculty:import.html.twig', array(
                         'company' => $company,
                         'form' => $form->createView(),
                         'form2' => $form2,
                         'faculties' => $faculties,
                    ));
                }
                
                $faculties = $temporalentity->getData();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    foreach ($faculties as $facultyarray) {
                        //If the companyfacultycode exists update this
                        $query = $em->createQuery(
                            'SELECT cf FROM FishmanEntityBundle:Companyfaculty cf 
                             WHERE cf.company = :company
                               AND cf.code = :code')
                             ->setParameter('company', $company)
                             ->setParameter('code', $facultyarray['code']);
    
                        try {
                            $companyfaculty = $query->getSingleResult();
                        } catch (\Doctrine\Orm\NoResultException $e) {
                            $companyfaculty = false;
                        }
                        
                        $userBy = $this->get('security.context')->getToken()->getUser();
                        if (!$companyfaculty) {
                            $companyfaculty = new Companyfaculty();
                            $companyfaculty->setCreatedBy($userBy->getId());
                            $companyfaculty->setCreated(new \DateTime());
                        }
                        
                        //TODO: When create companyfaculty manual, then use the same logic
    
                        $companyfaculty->setCode($facultyarray['code']);
                        $companyfaculty->setName($facultyarray['name']);
                        $companyfaculty->setStatus($facultyarray['status']);
                        $faculty = $em->getRepository('FishmanEntityBundle:Faculty')->find($facultyarray['faculty_id']);
                        $companyfaculty->setFaculty($faculty);
    
                        // User
                        $companyfaculty->setModifiedBy($userBy->getId());
                        $companyfaculty->setCompany($company);
                        $companyfaculty->setChanged(new \DateTime());
                
                        $em->persist($companyfaculty);
                        $em->flush();
                        if ($facultyarray['valid'] != 0) {
                          $numberRegister = $numberRegister + 1;
                        }
                    } //end foreach
    
                    //Delete temporal data
                    $em->remove($temporalentity);
                    $em->flush();
                    
                    // Delete temporal files
                    Company::deleteTemporalFiles('../tmp/');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                if ($numberRegister > 0) {
                    $session->getFlashBag()->add('status', 'Se grabó la importación de '.$numberRegister.' Facultad(es).');
                }
                else {
                    $session->getFlashBag()->add('error', 'No se logró importar ninguna Facultad.');
                }
                
                if($request->request->get('saveandclose', true)){
                    return $this->redirect($this->generateUrl('companyfaculty', array(
                        'companyid' => $companyid
                    )));
                }
                
                $faculties = false;
                
            } //End have temporarlimportdata input
            else{
                $form->bind($request);
                $data = $form->getData();
                
                // We need to move the file in order to use it
                $randomName = rand(1, 10000000000);
                $randomName = $randomName . '.xlsx';
                $directory = __DIR__.'/../../../../tmp';
                $data['file']->move($directory, $randomName);
                
                if (null !== $data['file']) {
                    $fileName = $data['file']->getClientOriginalName();
                    $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                }
                
                if ($data['file']->document == 'xlsx') { 
                    $inputFileName = $directory . '/' . $randomName;
                    $phpExcel = PHPExcel_IOFactory::load($inputFileName);
                    
                    $worksheet = $phpExcel->getSheet(0);
                    
                    $row = 2;
                    $faculties = array();
                    
                    do {
                        $code = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                        $name = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                        $faculty_id = trim($worksheet->getCellByColumnAndRow(2, $row++)->getValue());
                        $message = '';
                        $valid = 1;
                        $faculty_name = '';
  
                        if($code == '' && $name == '' && $faculty_id == '') break;
                        if($code == ''){
                            $message .= '<p>Falta el código de la facultad</p>';
                            $valid = 0;
                        }
                        else {
                            // Check if you have legacy
                            $cf_code = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companyfaculty')->findOneBy(
                                array(
                                    'code' => $code,
                                    'company' => $companyid
                                )
                            );
                            if (!empty($cf_code)) {
                                $message .= '<p>El código de la facultad ya está siendo usado en otra facultad de la empresa</p>';
                                $valid = 0;
                            }
                        }
  
                        if($name == ''){
                            $message .= '<p>Falta el nombre de la facultad</p>';
                            $valid = 0;
                        }
  
                        if($faculty_id == ''){
                            $message .= '<p>Falta el id del nombre global</p>';
                        }
                        elseif(!preg_match('/^\d+$/',$faculty_id)) { //Only digits
                            $message .= '<p>El id del nombre global debe ser un número entero</p>';
                        }
                        else{
                            $faculty = $em->getRepository('FishmanEntityBundle:Faculty')->find($faculty_id);
                            if($faculty){
                                $faculty_name = $faculty->getName();
                            }
                        }
  
                        if($valid && $message == ''){
                            $message = 'OK';
                        }
  
                        $faculties[] = array('code' => $code,
                                           'name' => $name,
                                           'faculty_id' => $faculty_id,
                                           'faculty_name' => $faculty_name,
                                           'valid' => $valid,
                                           'status' => 1,
                                           'message' => $message);
                    } while (true);
                    
                }
                else {
                    $session->getFlashBag()->add('status', 'el archivo a importar debe ser de formato excel');
                }

                if(count($faculties) > 0 and is_array($faculties)){
                    $temporal = new Temporalimportdata();
                    $temporal->setData($faculties);
                    $temporal->setEntity('companyfaculty');
                    $temporal->setDatetime(new \DateTime());
                    $em->persist($temporal);
                    $em->flush();

                    // Secondary form
                    $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                    $form2 = $this->createFormBuilder($defaultData2)
                        ->add('temporalimportdata_id',
                              'integer', array (
                                       'required' => true 
                             ))
                        ->getForm();
                    $form2 = $form2->createView();
                }
                else {
                    $session->getFlashBag()->add('status', 'No hay facultades para importar.');
                }
                
                //Borrar archivo temporal
                if ($data['file']->document == 'xlsx') {
                    unlink($inputFileName);
                }
                
            }
        } //End is method post
        
        return $this->render('FishmanEntityBundle:Companyfaculty:import.html.twig', array(
            'company' => $company,
            'form' => $form->createView(),
            'form2' => $form2,
            'faculties' => $faculties,
        ));
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="importar-empresa-facultades.xlsx'.'"'
        );
        return new Response(file_get_contents(
            __DIR__.'/../../../../templates/EntityBundle/Company/importar-empresa-facultades.xlsx'), 200, $headers);
    }

}
