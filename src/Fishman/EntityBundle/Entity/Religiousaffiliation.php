<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Religiousaffiliation
 */
class Religiousaffiliation
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Religiousaffiliation
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
        

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get list religious affiliation options
     * 
     */
    public static function getListReligiousAffiliationOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Religiousaffiliation');
        $queryBuilder = $repository->createQueryBuilder('ra')
            ->select('ra.id, ra.name')
            ->orderBy('ra.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }
    
}