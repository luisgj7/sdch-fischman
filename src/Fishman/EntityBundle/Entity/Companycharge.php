<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Companycharge
 */
class Companycharge
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;


    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /*
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Charge", inversedBy="companycharges")
     * @ORM\JoinColumn(name="charge_id", referencedColumnName="id")
     */
    protected $charge;
  
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Company", inversedBy="companycharges")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Workinginformation", mappedBy="companycharge")
     */
    protected $workinginformations;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Companycharge
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Companycharge
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Companycharge
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Companycharge
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Companycharge
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Companycharge
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Companycharge
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   


    /**
     * Set charge
     *
     * @param Fishman\EntityBundle\Entity\Charge $charge
     * @return Companycharge
     */
    public function setCharge(\Fishman\EntityBundle\Entity\Charge $charge = null)
    {
        $this->charge = $charge;
    
        return $this;
    }

    /**
     * Get charge
     *
     * @return Fishman\EntityBundle\Entity\Charge 
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * Set company
     *
     * @param Fishman\EntityBundle\Entity\Company $company
     * @return Companycharge
     */
    public function setCompany(\Fishman\EntityBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return Fishman\EntityBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
   
    public function __toString()
    {
         return $this->name;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workinginformations = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     * @return Companycharge
     */
    public function addWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations[] = $workinginformations;
    
        return $this;
    }

    /**
     * Remove workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     */
    public function removeWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations->removeElement($workinginformations);
    }

    /**
     * Get workinginformations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkinginformations()
    {
        return $this->workinginformations;
    }

    public static function getSelectCompanycharge(DoctrineRegistry $doctrineRegistry, $companyId, $selectedId = false)
    {
        $repository = $doctrineRegistry->getManager()->getRepository('FishmanEntityBundle:Companycharge');
        $ccs = $repository->createQueryBuilder('cc')
            ->select('cc.id, cc.name')
            ->where('cc.company = :companyid 
                    AND cc.status = 1')
            ->setParameter('companyid', $companyId)
            ->orderBy('cc.name', 'ASC')
            ->getQuery()
            ->getResult();

        $selectCompanyCharge = '<select class="companycharge" name="company_charge">';
        $selectCompanyCharge .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        if($ccs){
            foreach($ccs as $cc){
                $selected = '';
                if($selectedId == $cc['id']){
                    $selected = ' selected ';
                }
                $selectCompanyCharge .= '<option value="' . $cc['id'] . '"'. $selected .'>' .  $cc['name'] . '</option>';
            }
        }
        $selectCompanyCharge .= '</select>';
        
        return $selectCompanyCharge;
    }

    /**
     * Get list company charge options
     * 
     */
    public static function getListCompanychargeOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companycharge');
        $queryBuilder = $repository->createQueryBuilder('cch')
            ->select('cch.id, cch.name')
            ->where('cch.company = :company 
                    AND cch.status = 1')
            ->setParameter('company', $companyId)
            ->orderBy('cch.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }

    /**
     * Get list charge to company options
     * 
     */
    public static function getListChargeOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companycharge');
        $queryBuilder = $repository->createQueryBuilder('cch')
            ->select('ch.id, ch.name')
            ->innerJoin('cch.charge', 'ch')
            ->where('cch.company = :company 
                    AND ch.status = 1')
            ->setParameter('company', $companyId)
            ->groupBy('ch.id')
            ->orderBy('ch.name', 'ASC');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }

}