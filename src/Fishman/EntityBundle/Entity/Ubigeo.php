<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Ubigeo
 */
class Ubigeo
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $code_parent
     */
    private $code_parent;

    /**
     * @var string $type
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Ubigeo
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Ubigeo
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code_parent
     *
     * @param string $codeParent
     * @return Ubigeo
     */
    public function setCodeParent($codeParent)
    {
        $this->code_parent = $codeParent;
    
        return $this;
    }

    /**
     * Get code_parent
     *
     * @return string 
     */
    public function getCodeParent()
    {
        return $this->code_parent;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Ubigeo
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    public static function getSelectUbigeo(DoctrineRegistry $doctrineRegistry, $ubigeoId, $ubigeoType, $selectedId = FALSE)
    {
        if ($ubigeoType != 'department') {
            $repository = $doctrineRegistry->getRepository('FishmanEntityBundle:Ubigeo');
            $query = $repository->createQueryBuilder('u')
                ->select('u.code')
                ->where('u.id = :ubigeo')
                ->setParameter('ubigeo', $ubigeoId)
                ->getQuery()
                ->getResult();
            
            $ubigeo_parent = $query[0]['code'];
        }
        
        switch ($ubigeoType) {
            case 'department': 
                $class = 'department departmentselect';
                $name = 'ubigeo_department';
                break;
            case 'province': 
                $class = 'province provinceselect';
                $name = 'ubigeo_province';
                break;
            case 'district': 
                $class = 'district districtselect';
                $name = 'ubigeo_district';
                break;
        }
        
        $repository = $doctrineRegistry->getManager()->getRepository('FishmanEntityBundle:Ubigeo');
        $queryBuilder = $repository->createQueryBuilder('u')
            ->select('u.id, u.name')
            ->orderBy('u.name', 'ASC');
            
        if ($ubigeoType == 'department') {
            $queryBuilder
                ->where('u.code_parent IS NULL');
        }
        else {
            $queryBuilder
                ->where('u.code_parent = :ubigeo')
                ->setParameter('ubigeo', $ubigeo_parent);
        }
        
        $ubigeos = $queryBuilder->getQuery()->getResult();
        $selected = '';
        if ($selectedId) {
            $selected = ' selected ';
        }
        
        $selectUbigeo = '<select class="' . $class . '" name="' . $name . '">';
        $selectUbigeo .= '<option value="" ' . $selected . '>SELECCIONE UNA OPCIÓN</option>';
        if($ubigeos){
            foreach($ubigeos as $ubigeo){
                $selected = '';
                if($selectedId == $ubigeo['id']){
                    $selected = ' selected ';
                }
                $selectUbigeo .= '<option value="' . $ubigeo['id'] . '"'. $selected .'>' .  $ubigeo['name'] . '</option>';
            }
        }
        $selectUbigeo .= '</select>';
        
        return $selectUbigeo;
    }
}