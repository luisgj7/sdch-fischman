<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Fishman\EntityBundle\Entity\Talent
 */
class Talent
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $talent
     */
    private $talent;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workbooktalent", mappedBy="talent")
     */
    protected $workbooktalents; 


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set talent
     *
     * @param string $talent
     * @return Talent
     */
    public function setTalent($talent)
    {
        $this->talent = $talent;
    
        return $this;
    }

    /**
     * Get talent
     *
     * @return string 
     */
    public function getTalent()
    {
        return $this->talent;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Talent
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Talent
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Talent
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Talent
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Talent
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   
    public function __toString()
    {
         return $this->talent;
    }  

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workbooktalents = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add workbooktalents
     *
     * @param Fishman\WorkshopBundle\Entity\Workbooktalent $workbooktalents
     * @return Talent
     */
    public function addWorkbooktalent(\Fishman\WorkshopBundle\Entity\Workbooktalent $workbooktalents)
    {
        $this->workbooktalents[] = $workbooktalents;
    
        return $this;
    }

    /**
     * Remove workbooktalents
     *
     * @param Fishman\WorkshopBundle\Entity\Workbooktalent $workbooktalents
     */
    public function removeWorkbooktalent(\Fishman\WorkshopBundle\Entity\Workbooktalent $workbooktalents)
    {
        $this->workbooktalents->removeElement($workbooktalents);
    }

    /**
     * Get workbooktalents
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkbooktalents()
    {
        return $this->workbooktalents;
    }
}