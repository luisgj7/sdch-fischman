<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Faculty
 */
class Faculty
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;
    
    
    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Companyfaculty", mappedBy="faculty")
     */
    protected $companyfacultys; 
    
    
    public function __toString()
    {
         return $this->name;
    }
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Faculty
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Faculty
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Faculty
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Faculty
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Faculty
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Faculty
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companyfacultys = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add companyfacultys
     *
     * @param Fishman\EntityBundle\Entity\Companyfaculty $companyfacultys
     * @return Faculty
     */
    public function addCompanyfaculty(\Fishman\EntityBundle\Entity\Companyfaculty $companyfacultys)
    {
        $this->companyfacultys[] = $companyfacultys;
    
        return $this;
    }

    /**
     * Remove companyfacultys
     *
     * @param Fishman\EntityBundle\Entity\Companyfaculty $companyfacultys
     */
    public function removeCompanyfaculty(\Fishman\EntityBundle\Entity\Companyfaculty $companyfacultys)
    {
        $this->companyfacultys->removeElement($companyfacultys);
    }

    /**
     * Get companyfacultys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanyfacultys()
    {
        return $this->companyfacultys;
    }

    /**
     * Get list faculty options
     * 
     */
    public static function getListFacultyOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Faculty');
        $queryBuilder = $repository->createQueryBuilder('f')
            ->select('f.id, f.name')
            ->where('f.status = 1')
            ->orderBy('f.name', 'ASC');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }
    
}