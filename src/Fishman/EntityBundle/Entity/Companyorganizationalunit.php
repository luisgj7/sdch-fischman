<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DoctrineExtensions\NestedSet\MultipleRootNode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Companyorganizationalunit
 */
class Companyorganizationalunit
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $unit_type
     */
    private $unit_type;

    /**
     * @var integer $boss
     */
    private $boss;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /*
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Organizationalunit", inversedBy="companyorganizationalunits")
     * @ORM\JoinColumn(name="organizationalunit_id", referencedColumnName="id")
     */
    protected $organizationalunit;

    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Company", inversedBy="companyorganizationalunits")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Workinginformation", mappedBy="companyorganizationalunit")
     */
    protected $workinginformations;
    
    private $parent_id;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workinginformation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Companyorganizationalunit
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Companyorganizationalunit
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Companyorganizationalunit
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set unit_type
     *
     * @param string $unit_type
     * @return Companyorganizationalunit
     */
    public function setUnitType($unit_type)
    {
        $this->unit_type = $unit_type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getUnitType()
    {
        return $this->unit_type;
    }

    /**
     * Set boss
     *
     * @param integer $boss
     * @return Companyorganizationalunit
     */
    public function setBoss($boss)
    {
        $this->boss = $boss;
    
        return $this;
    }

    /**
     * Get boss
     *
     * @return integer 
     */
    public function getBoss()
    {
        return $this->boss;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Companyorganizationalunit
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Companyorganizationalunit
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Companyorganizationalunit
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Companyorganizationalunit
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Companyorganizationalunit
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set organizationalunit
     *
     * @param Fishman\EntityBundle\Entity\Organizationalunit $organizationalunit
     * @return Companyorganizationalunit
     */
    public function setOrganizationalunit(\Fishman\EntityBundle\Entity\Organizationalunit $organizationalunit = null)
    {
        $this->organizationalunit = $organizationalunit;
    
        return $this;
    }

    /**
     * Get organizationalunit
     *
     * @return Fishman\EntityBundle\Entity\Organizationalunit 
     */
    public function getOrganizationalunit()
    {
        return $this->organizationalunit;
    }

    /**
     * Set company
     *
     * @param Fishman\EntityBundle\Entity\Company $company
     * @return Companyorganizationalunit
     */
    public function setCompany(\Fishman\EntityBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return Fishman\EntityBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    public function __toString()
    {
        return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $workinginformation;
    
    /**
     * Add workinginformation
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformation
     * @return Companyorganizationalunit
     */
    public function addWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformation)
    {
        $this->workinginformation[] = $workinginformation;
    
        return $this;
    }

    /**
     * Remove workinginformation
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformation
     */
    public function removeWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformation)
    {
        $this->workinginformation->removeElement($workinginformation);
    }

    /**
     * Get workinginformation
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkinginformation()
    {
        return $this->workinginformation;
    }

    /**
     * Get workinginformations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkinginformations()
    {
        return $this->workinginformations;
    }

    /**
     * Get the parent id.
     * @return integer the parent id if the node not have parent then return -1
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Set de parent of the node. If 
     * @param integer $parent_id.
     */
    public function setParentId($parentId)
    {
        $this->parent_id = $parentId;
    }

    /**
     * @return array with all organizational units of this company
     */
    public function getParentOptions(DoctrineRegistry $doctrine, $companyId, $withoutparentoption = true)
    {
        $options = array();
        if($withoutparentoption){
            $options = array(-1 => 'NINGUNO');
        }

        self::addChoiceOptions($doctrine, $this->getRootParents($doctrine, $companyId), 0, $options, $this->getId());
        return $options;
    }
    
    public static function addChoiceOptions(DoctrineRegistry $doctrine, $nodes, $level, &$options, $ignore_id)
    {
        $levelIndicator = str_pad('', $level, '---' , STR_PAD_LEFT);
        foreach($nodes as $node){
            if($node['id'] != $ignore_id){
                $options[$node['id']] = $levelIndicator . ' ' . $node['name'];
                self::addChoiceOptions($doctrine, self::getChildren($doctrine, $node['id']), $level + 3, $options, $ignore_id);
            }
        }
    }
    
    public static function getChildren(DoctrineRegistry $doctrine, $parent_id)
    {
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $query = $repository->createQueryBuilder('co')
            ->select('co.id, co.name')
            ->where('co.parent_id = :parent_id')
            ->setParameter('parent_id', $parent_id)
            ->orderBy('co.name', 'ASC')
            ->getQuery();
        return $query->getResult();        
    }

    public static function getRootParents(DoctrineRegistry $doctrine, $companyId, $unitType = FALSE)
    {
        // Get all root CompanyOrganizationaUnit From this Company
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $queryBuilder = $repository->createQueryBuilder('co')
            ->select('co.id, co.name')
            ->where('co.company = :company
                     AND co.parent_id = :parent_id')
            ->setParameter('company', $companyId)
            ->setParameter('parent_id', -1)
            ->orderBy('co.name', 'ASC');
        
        if ($unitType) {
            $queryBuilder
                ->andWhere('co.unit_type = :unittype')
                ->setParameter('unittype', $unitType);
        }
        
        $query = $queryBuilder->getQuery()->getResult();
        
        return $query;
    }

    /**
     * Similar to getParentOptions, but with two differences:
     *   - Is static
     *   - Include all COU of some Company
     */
    public static function getCOUOptions(DoctrineRegistry $doctrine, $companyId)
    {
        $options = array();

        //Ignore_id argument = -1, because we want all options
        self::addChoiceOptions($doctrine, self::getRootParents($doctrine, $companyId), 0, $options, -1);
        return $options;
    }

    /**
     * Get company cou select control
     * @param integer $companyId. Return only this company companyorganizationalunits
     * @param mixed $ignoredId, if integer then the cou with this id, and all it's decendants) will not include
     * @return string with a select control. Every option will be a selected company cou
     */
    public static function selectByCompanyWidget(DoctrineRegistry $doctrine, $companyId, $unitType = false, $ignoreId = false, $selectedId = false)
    {
        $options = array();
        self::addChoiceOptions($doctrine, self::getRootParents($doctrine, $companyId, $unitType), 0, $options, $ignoreId);

        $selectCompanyOrganizationalUnit = '<select class="companyorganizationalunit" name="company_organization_unit">';
        $selectCompanyOrganizationalUnit .= '<option value="">SELECCIONE UNA OPCIÓN</option>';

        foreach($options as $key => $op){
            $selected = '';
            if($selectedId == $key){
                $selected = ' selected ';
            }

            $selectCompanyOrganizationalUnit .= '<option value="' . $key . '"'. $selected .'>' .  $op . '</option>';
        }
        $selectCompanyOrganizationalUnit .= '</select>';      

        return $selectCompanyOrganizationalUnit;
    }

    /**
     * Get list company organizational unit options
     * 
     */
    public static function getListCompanyorganizationalunitOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $queryBuilder = $repository->createQueryBuilder('cou')
            ->select('cou.id, cou.name')
            ->where('cou.company = :company 
                    AND cou.status = 1')
            ->setParameter('company', $companyId)
            ->orderBy('cou.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }

    /**
     * Get list organizationalunit to company options
     * 
     */
    public static function getListOrganizationalunitOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $queryBuilder = $repository->createQueryBuilder('cou')
            ->select('ou.id, ou.name')
            ->innerJoin('cou.organizationalunit', 'ou')
            ->where('cou.company = :company 
                    AND ou.status = 1')
            ->setParameter('company', $companyId)
            ->groupBy('ou.id')
            ->orderBy('ou.name', 'ASC');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }

    /**
     * Get list boss options
     * 
     */
    public static function getListBossOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companyorganizationalunit');
        $queryBuilder = $repository->createQueryBuilder('cou')
            ->select('wi.id, u.names, u.surname, u.lastname')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'cou.boss = wi.id')
            ->leftJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wi.user = u.id')
            ->where('cou.company = :company 
                    AND wi.status = 1')
            ->setParameter('company', $companyId)
            ->groupBy('wi.id')
            ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['names'] . ' ' . $r['surname'] . ', ' . $r['lastname'];
            }
        }
        
        return $output;
    }
}