<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Companycareer
 */
class Companycareer
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /*
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Companyfaculty", inversedBy="companycareers")
     * @ORM\JoinColumn(name="companyfaculty_id", referencedColumnName="id")
     */
    protected $companyfaculty;
  
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Company", inversedBy="companycareers")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Workinginformation", mappedBy="companycareer")
     */
    protected $workinginformations;

   
    public function __toString()
    {
         return $this->name;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Companycareer
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Companycareer
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Companycareer
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Companycareer
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Companycareer
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Companycareer
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Companycareer
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set companyfaculty
     *
     * @param Fishman\EntityBundle\Entity\Companyfaculty $companyfaculty
     * @return Companycareer
     */
    public function setCompanyfaculty(\Fishman\EntityBundle\Entity\Companyfaculty $companyfaculty = null)
    {
        $this->companyfaculty = $companyfaculty;
    
        return $this;
    }

    /**
     * Get companyfaculty
     *
     * @return Fishman\EntityBundle\Entity\Companyfaculty 
     */
    public function getCompanyfaculty()
    {
        return $this->companyfaculty;
    }

    /**
     * Set company
     *
     * @param Fishman\EntityBundle\Entity\Company $company
     * @return Companycareer
     */
    public function setCompany(\Fishman\EntityBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return Fishman\EntityBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workinginformations = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     * @return Companycareer
     */
    public function addWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations[] = $workinginformations;
    
        return $this;
    }

    /**
     * Remove workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     */
    public function removeWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations->removeElement($workinginformations);
    }

    /**
     * Get workinginformations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkinginformations()
    {
        return $this->workinginformations;
    }

    /**
     * Get list companycareer to company options
     * 
     */
    public static function getListCompanycareerOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companycareer');
        $queryBuilder = $repository->createQueryBuilder('ccr')
            ->select('ccr.id, ccr.name')
            ->where('ccr.company = :company 
                    AND ccr.status = 1')
            ->setParameter('company', $companyId)
            ->orderBy('ccr.name', 'ASC');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }

    public static function getSelectCompanycareer(DoctrineRegistry $doctrineRegistry, $facultyId, $selectedId = false)
    {
        $repository = $doctrineRegistry->getManager()->getRepository('FishmanEntityBundle:Companycareer');
        $hs = $repository->createQueryBuilder('ccr')
            ->select('ccr.id, ccr.name')
            ->where('ccr.companyfaculty = :facultyid 
                    AND ccr.status = 1')
            ->setParameter('facultyid', $facultyId)
            ->orderBy('ccr.name', 'ASC')
            ->getQuery()
            ->getResult();

        $selectCompanycareer = '<select class="companycareer" name="company_career">';
        $selectCompanycareer .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        if($hs){
            foreach($hs as $h){
                $selected = '';
                if($selectedId == $h['id']){
                    $selected = ' selected ';
                }
                $selectCompanycareer .= '<option value="' . $h['id'] . '"'. $selected .'>' .  $h['name'] . '</option>';
            }
        }
        $selectCompanycareer .= '</select>';
        
        return $selectCompanycareer;
    }

}