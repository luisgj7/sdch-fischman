<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Country
 */
class Country
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Headquarter", mappedBy="country")
     */
    protected $headquarters; 


    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get list country options
     * 
     */
    public static function getListCountryOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Country');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->orderBy('c.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $output[$r['id']] = $r['name'];
        }
        
        return $output;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->headquarters = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add headquarters
     *
     * @param Fishman\EntityBundle\Entity\Headquarter $headquarters
     * @return Country
     */
    public function addHeadquarter(\Fishman\EntityBundle\Entity\Headquarter $headquarters)
    {
        $this->headquarters[] = $headquarters;
    
        return $this;
    }

    /**
     * Remove headquarters
     *
     * @param Fishman\EntityBundle\Entity\Headquarter $headquarters
     */
    public function removeHeadquarter(\Fishman\EntityBundle\Entity\Headquarter $headquarters)
    {
        $this->headquarters->removeElement($headquarters);
    }

    /**
     * Get headquarters
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getHeadquarters()
    {
        return $this->headquarters;
    }
}