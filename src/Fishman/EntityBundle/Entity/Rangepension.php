<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Rangepension
 */
class Rangepension
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $rank
     */
    private $rank;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Rangepension
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set rank
     *
     * @param string $rank
     * @return Rangepension
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    
        return $this;
    }

    /**
     * Get rank
     *
     * @return string 
     */
    public function getRank()
    {
        return $this->rank;
    }
        

    public function __toString()
    {
        return $this->rank;
    }

    /**
     * Get list range pensions options
     * 
     */
    public static function getListRangePensionOptions(DoctrineRegistry $doctrine, $type) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Rangepension');
        $queryBuilder = $repository->createQueryBuilder('rp')
            ->select('rp.id, rp.rank')
            ->where('rp.type = :type')
            ->setParameter('type', $type)
            ->orderBy('rp.rank', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['rank'];
            }
        }
        
        return $output;
    }
    
}