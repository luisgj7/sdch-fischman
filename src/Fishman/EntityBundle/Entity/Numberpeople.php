<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Numberpeople
 */
class Numberpeople
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $rank
     */
    private $rank;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Numberpeople
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set rank
     *
     * @param string $rank
     * @return Numberpeople
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    
        return $this;
    }

    /**
     * Get rank
     *
     * @return string 
     */
    public function getRank()
    {
        return $this->rank;
    }
    

    public function __toString()
    {
        return $this->rank;
    }

    /**
     * Get list number peoples options
     * 
     */
    public static function getListNumberPeopleOptions(DoctrineRegistry $doctrine, $type = 'working') 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Numberpeople');
        $queryBuilder = $repository->createQueryBuilder('np')
            ->select('np.id, np.rank')
            ->where('np.type = :type')
            ->setParameter('type', $type)
            ->orderBy('np.rank', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['rank'];
            }
        }
        
        return $output;
    }
    
}