<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TalentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TalentRepository extends EntityRepository
{
}
