<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Category
 */
class Category
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;


    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;



    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $workshops;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Poll", mappedBy="category")
     */
    protected $polls; 

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopdocument", mappedBy="category")
     */
    protected $workshopdocuments;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopactivity", mappedBy="category")
     */
    protected $workshopactivitys;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopschedulingdocument", mappedBy="category")
     */
    protected $workshopschedulingdocuments;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopschedulingactivity", mappedBy="category")
     */
    protected $workshopschedulingactivitys;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workshops = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Category
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Category
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Category
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }
    
    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Category
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Category
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   


    /**
     * Add workshops
     *
     * @param Fishman\WorkshopBundle\Entity\Workshop $workshops
     * @return Category
     */
    public function addWorkshop(\Fishman\WorkshopBundle\Entity\Workshop $workshops)
    {
        $this->workshops[] = $workshops;
    
        return $this;
    }

    /**
     * Remove workshops
     *
     * @param Fishman\WorkshopBundle\Entity\Workshop $workshops
     */
    public function removeWorkshop(\Fishman\WorkshopBundle\Entity\Workshop $workshops)
    {
        $this->workshops->removeElement($workshops);
    }

    /**
     * Get workshops
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshops()
    {
        return $this->workshops;
    }

    public function __toString()
    {
         return $this->name;
    }


    /**
     * Add polls
     *
     * @param Fishman\PollBundle\Entity\Poll $polls
     * @return Category
     */
    public function addPoll(\Fishman\PollBundle\Entity\Poll $polls)
    {
        $this->polls[] = $polls;
    
        return $this;
    }

    /**
     * Remove polls
     *
     * @param Fishman\PollBundle\Entity\Poll $polls
     */
    public function removePoll(\Fishman\PollBundle\Entity\Poll $polls)
    {
        $this->polls->removeElement($polls);
    }

    /**
     * Get polls
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPolls()
    {
        return $this->polls;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $workshopdocumets;


    /**
     * Add workshopdocumets
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopdocument $workshopdocumets
     * @return Category
     */
    public function addWorkshopdocumet(\Fishman\WorkshopBundle\Entity\Workshopdocument $workshopdocumets)
    {
        $this->workshopdocumets[] = $workshopdocumets;
    
        return $this;
    }

    /**
     * Remove workshopdocumets
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopdocument $workshopdocumets
     */
    public function removeWorkshopdocumet(\Fishman\WorkshopBundle\Entity\Workshopdocument $workshopdocumets)
    {
        $this->workshopdocumets->removeElement($workshopdocumets);
    }

    /**
     * Get workshopdocumets
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopdocumets()
    {
        return $this->workshopdocumets;
    }

    /**
     * Add workshopactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys
     * @return Category
     */
    public function addWorkshopactivity(\Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys)
    {
        $this->workshopactivitys[] = $workshopactivitys;
    
        return $this;
    }

    /**
     * Remove workshopactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys
     */
    public function removeWorkshopactivity(\Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys)
    {
        $this->workshopactivitys->removeElement($workshopactivitys);
    }

    /**
     * Get workshopactivitys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopactivitys()
    {
        return $this->workshopactivitys;
    }

    /**
     * Add workshopschedulingdocuments
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingdocument $workshopschedulingdocuments
     * @return Category
     */
    public function addWorkshopschedulingdocument(\Fishman\WorkshopBundle\Entity\Workshopschedulingdocument $workshopschedulingdocuments)
    {
        $this->workshopschedulingdocuments[] = $workshopschedulingdocuments;
    
        return $this;
    }

    /**
     * Remove workshopschedulingdocuments
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingdocument $workshopschedulingdocuments
     */
    public function removeWorkshopschedulingdocument(\Fishman\WorkshopBundle\Entity\Workshopschedulingdocument $workshopschedulingdocuments)
    {
        $this->workshopschedulingdocuments->removeElement($workshopschedulingdocuments);
    }

    /**
     * Get workshopschedulingdocuments
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopschedulingdocuments()
    {
        return $this->workshopschedulingdocuments;
    }

    /**
     * Add workshopschedulingactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys
     * @return Category
     */
    public function addWorkshopschedulingactivity(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys)
    {
        $this->workshopschedulingactivitys[] = $workshopschedulingactivitys;
    
        return $this;
    }

    /**
     * Remove workshopschedulingactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys
     */
    public function removeWorkshopschedulingactivity(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys)
    {
        $this->workshopschedulingactivitys->removeElement($workshopschedulingactivitys);
    }

    /**
     * Get workshopschedulingactivitys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopschedulingactivitys()
    {
        return $this->workshopschedulingactivitys;
    }

    /**
     * Get list category options
     * 
     */
    public static function getListCategoryOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Category');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->where('c.status = 1')
            ->orderBy('c.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }
}