<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Volumebusiness
 */
class Volumebusiness
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $rank
     */
    private $rank;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rank
     *
     * @param string $rank
     * @return Volumebusiness
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    
        return $this;
    }

    /**
     * Get rank
     *
     * @return string 
     */
    public function getRank()
    {
        return $this->rank;
    }
        

    public function __toString()
    {
        return $this->rank;
    }

    /**
     * Get list volume business options
     * 
     */
    public static function getListVolumeBusinessOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Volumebusiness');
        $queryBuilder = $repository->createQueryBuilder('vb')
            ->select('vb.id, vb.rank')
            ->orderBy('vb.rank', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['rank'];
            }
        }
        
        return $output;
    }
    
}