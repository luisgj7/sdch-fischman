<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Headquarter
 */
class Headquarter
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $via
     */
    private $via;

    /**
     * @var string $address
     */
    private $address;

    /**
     * @var string $urbanization
     */
    private $urbanization;

    /**
     * @var string $department
     */
    private $department;

    /**
     * @var string $province
     */
    private $province;

    /**
     * @var string $district
     */
    private $district;

    /**
     * @var string $city
     */
    private $city;

    /**
     * @var string $phone_station
     */
    private $phone_station;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;
    
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Company", inversedBy="headquarters")
     * @ORM\JoinColumn(name="headquarter_id", referencedColumnName="id")
     */
    protected $company;
    
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Country", inversedBy="headquarters")
     * @ORM\JoinColumn(name="headquarter_id", referencedColumnName="id")
     */
    protected $country;
    
    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Workinginformation", mappedBy="headquarter")
     */
    protected $workinginformations; 


    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Headquarter
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Headquarter
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set via
     *
     * @param string $via
     * @return Headquarter
     */
    public function setVia($via)
    {
        $this->via = $via;
    
        return $this;
    }

    /**
     * Get via
     *
     * @return string 
     */
    public function getVia()
    {
        return $this->via;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Headquarter
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set urbanization
     *
     * @param string $urbanization
     * @return Headquarter
     */
    public function setUrbanization($urbanization)
    {
        $this->urbanization = $urbanization;
    
        return $this;
    }

    /**
     * Get urbanization
     *
     * @return string 
     */
    public function getUrbanization()
    {
        return $this->urbanization;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Headquarter
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set department
     *
     * @param string $department
     * @return Headquarter
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    
        return $this;
    }

    /**
     * Get department
     *
     * @return string 
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set province
     *
     * @param string $province
     * @return Headquarter
     */
    public function setProvince($province)
    {
        $this->province = $province;
    
        return $this;
    }

    /**
     * Get province
     *
     * @return string 
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set district
     *
     * @param string $district
     * @return Headquarter
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    
        return $this;
    }

    /**
     * Get district
     *
     * @return string 
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Headquarter
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phone_station
     *
     * @param string $phoneStation
     * @return Headquarter
     */
    public function setPhoneStation($phoneStation)
    {
        $this->phone_station = $phoneStation;
    
        return $this;
    }

    /**
     * Get phone_station
     *
     * @return string 
     */
    public function getPhoneStation()
    {
        return $this->phone_station;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Headquarter
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workinginformations = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Headquarter
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Headquarter
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Headquarter
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Headquarter
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Add workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     * @return Headquarter
     */
    public function addWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations[] = $workinginformations;
    
        return $this;
    }

    /**
     * Remove workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     */
    public function removeWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations->removeElement($workinginformations);
    }

    /**
     * Get workinginformations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkinginformations()
    {
        return $this->workinginformations;
    }

    /**
     * Set company
     *
     * @param Fishman\EntityBundle\Entity\Company $company
     * @return Headquarter
     */
    public function setCompany(\Fishman\EntityBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return Fishman\EntityBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Get list via options
     * 
     */
    public static function getListViaOptions() 
    {
        $output = array(
            'via' => 'Vía', 
            'street' => 'Calle',
            'batch' => 'Lote'
        );
        
        return $output;
    }

    public static function getSelectHeadquarter(DoctrineRegistry $doctrineRegistry, $companyId, $selectedId = false)
    {
        $repository = $doctrineRegistry->getManager()->getRepository('FishmanEntityBundle:Headquarter');
        $hs = $repository->createQueryBuilder('h')
            ->select('h.id, h.name')
            ->where('h.company = :companyid 
                    AND h.status = 1')
            ->setParameter('companyid', $companyId)
            ->orderBy('h.name', 'ASC')
            ->getQuery()
            ->getResult();

        $selectHeadquarter = '<select class="headquarter" name="headquarter">';
        $selectHeadquarter .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        if($hs){
            foreach($hs as $h){
                $selected = '';
                if($selectedId == $h['id']){
                    $selected = ' selected ';
                }
                $selectHeadquarter .= '<option value="' . $h['id'] . '"'. $selected .'>' .  $h['name'] . '</option>';
            }
        }
        $selectHeadquarter .= '</select>';
        
        return $selectHeadquarter;
    }

    /**
     * Get list company headquarter options
     * 
     */
    public static function getListHeadquarterOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Headquarter');
        $queryBuilder = $repository->createQueryBuilder('h')
            ->select('h.id, h.name')
            ->where('h.company = :company 
                    AND h.status = 1')
            ->setParameter('company', $companyId)
            ->orderBy('h.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }
}