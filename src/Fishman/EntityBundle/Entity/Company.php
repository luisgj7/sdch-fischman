<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Fishman\AuthBundle\Entity\Workinginformation;


/**
 * Fishman\EntityBundle\Entity\Company
 */
class Company
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $company_type
     */
    private $company_type;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $document_type
     */
    private $document_type;

    /**
     * @var string $ruc
     */
    private $ruc;

    /**
     * @var string $contact
     */
    private $contact;

    /**
     * @var string $phone
     */
    private $phone;

    /**
     * @var string $annex
     */
    private $annex;

    /**
     * @var string $celphone
     */
    private $celphone;

    /**
     * @var email $email
     */
    private $email;

    /**
     * @var integer $volumen_business
     */
    private $volume_business;

    /**
     * @var integer $number_employees
     */
    private $number_employees;

    /**
     * @var integer $range_pension
     */
    private $range_pension;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $gender
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $image;

    /**
     * @var integer $number_students
     */
    private $number_students;

    /**
     * @var boolean $religious_affiliation_on
     */
    private $religious_affiliation_on;

    /**
     * @var integer $religious_affiliation_on
     */
    private $religious_affiliation;

    /**
     * @var integer $institutional_affiliation
     */
    private $institutional_affiliation;

    /**
     * @var string $education_system
     */
    private $education_system;

    /**
     * @var boolean $internal_school
     */
    private $internal_school;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;
    
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Industry", inversedBy="companys")
     * @ORM\JoinColumn(name="industry_id", referencedColumnName="id")
     */
    protected $industry;
    
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Nature", inversedBy="companys")
     * @ORM\JoinColumn(name="nature_id", referencedColumnName="id")
     */
    protected $nature;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Companyfaculty", mappedBy="company")
     */
    protected $companyfacultys;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Companycareer", mappedBy="company")
     */
    protected $companycareers;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Companycourse", mappedBy="company")
     */
    protected $companycourses;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Companycharge", mappedBy="company")
     */
    protected $companycharges; 

    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Companyorganizationalunit", mappedBy="company")
     */
    protected $companyorganizationalunits;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Workinginformation", mappedBy="company")
     */
    protected $workinginformations; 

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopscheduling", mappedBy="company")
     */
    protected $workshopschedulings;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Headquarter", mappedBy="company")
     */
    protected $headquarters;
    
    /**
     * @Assert\File(maxSize="6000000")
     */
    public $document;

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->id.'.'.$this->image;
    }

    public function getWebPath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../uploads/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'company/logos';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->document) {
                $fileName = $this->document->getClientOriginalName();
                $this->image = substr($fileName, strrpos($fileName, '.') + 1);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
       if (null === $this->document) {
            return;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->document->move($this->getUploadRootDir(), $this->id.'.'.$this->image);

        unset($this->document);
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->filenameForRemove = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->filenameForRemove) {
            $this->filenameForRemove;
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Company
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set company_type
     *
     * @param string $companyType
     * @return Company
     */
    public function setCompanyType($companyType)
    {
        $this->company_type = $companyType;
    
        return $this;
    }

    /**
     * Get comapny_type
     *
     * @return string 
     */
    public function getCompanyType()
    {
        return $this->company_type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ruc
     *
     * @param string $ruc
     * @return Company
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
    
        return $this;
    }

    /**
     * Get ruc
     *
     * @return string 
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Company
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set annex
     *
     * @param string $annex
     * @return Company
     */
    public function setAnnex($annex)
    {
        $this->annex = $annex;
    
        return $this;
    }

    /**
     * Get annex
     *
     * @return string 
     */
    public function getAnnex()
    {
        return $this->annex;
    }

    /**
     * Set celphone
     *
     * @param string $celphone
     * @return Company
     */
    public function setCelphone($celphone)
    {
        $this->celphone = $celphone;
    
        return $this;
    }

    /**
     * Get celphone
     *
     * @return string 
     */
    public function getCelphone()
    {
        return $this->celphone;
    }

    /**
     * Set email
     *
     * @param email $email
     * @return Company
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return email 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Company
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Company
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Company
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }
    

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Company
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Company
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set industry
     *
     * @param Fishman\EntityBundle\Entity\Industry $industry
     * @return Company
     */
    public function setIndustry(\Fishman\EntityBundle\Entity\Industry $industry = null)
    {
        $this->industry = $industry;
    
        return $this;
    }

    /**
     * Get industry
     *
     * @return Fishman\EntityBundle\Entity\Industry 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set nature
     *
     * @param Fishman\EntityBundle\Entity\Nature $nature
     * @return Company
     */
    public function setNature(\Fishman\EntityBundle\Entity\Nature $nature = null)
    {
        $this->nature = $nature;
    
        return $this;
    }

    /**
     * Get nature
     *
     * @return Fishman\EntityBundle\Entity\Nature 
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * Set document_type
     *
     * @param string $documentType
     * @return Company
     */
    public function setDocumentType($documentType)
    {
        $this->document_type = $documentType;
    
        return $this;
    }

    /**
     * Get document_type
     *
     * @return string 
     */
    public function getDocumentType()
    {
        return $this->document_type;
    }

    /**
     * Set volume_business
     *
     * @param integer $volumeBusiness
     * @return Company
     */
    public function setVolumeBusiness($volumeBusiness)
    {
        $this->volume_business = $volumeBusiness;
    
        return $this;
    }

    /**
     * Get volume_business
     *
     * @return integer 
     */
    public function getVolumeBusiness()
    {
        return $this->volume_business;
    }

    /**
     * Set number_employees
     *
     * @param integer $numberEmployees
     * @return Company
     */
    public function setNumberEmployees($numberEmployees)
    {
        $this->number_employees = $numberEmployees;
    
        return $this;
    }

    /**
     * Get number_employees
     *
     * @return integer 
     */
    public function getNumberEmployees()
    {
        return $this->number_employees;
    }

    /**
     * Set range_pension
     *
     * @param integer $rangePension
     * @return Company
     */
    public function setRangePension($rangePension)
    {
        $this->range_pension = $rangePension;
    
        return $this;
    }

    /**
     * Get range_pension
     *
     * @return integer 
     */
    public function getRangePension()
    {
        return $this->range_pension;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Company
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Company
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set document
     *
     * @param file $document
     * @return Workshopdocument
     */
    public function setDocument($document)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return file 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set number_students
     *
     * @param integer $numberStudents
     * @return Company
     */
    public function setNumberStudents($numberStudents)
    {
        $this->number_students = $numberStudents;
    
        return $this;
    }

    /**
     * Get number_students
     *
     * @return integer 
     */
    public function getNumberStudents()
    {
        return $this->number_students;
    }

    /**
     * Set religious_affiliation_on
     *
     * @param boolean $religiousAffiliationOn
     * @return Company
     */
    public function setReligiousAffiliationOn($religiousAffiliationOn)
    {
        $this->religious_affiliation_on = $religiousAffiliationOn;
    
        return $this;
    }

    /**
     * Get religious_affiliation_on
     *
     * @return boolean 
     */
    public function getReligiousAffiliationOn()
    {
        return $this->religious_affiliation_on;
    }

    /**
     * Set religious_affiliation
     *
     * @param integer $religiousAffiliation
     * @return Company
     */
    public function setReligiousAffiliation($religiousAffiliation)
    {
        $this->religious_affiliation = $religiousAffiliation;
    
        return $this;
    }

    /**
     * Get religious_affiliation
     *
     * @return integer 
     */
    public function getReligiousAffiliation()
    {
        return $this->religious_affiliation;
    }

    /**
     * Set institutional_affiliation
     *
     * @param integer $institutionalAffiliation
     * @return Company
     */
    public function setInstitutionalAffiliation($institutionalAffiliation)
    {
        $this->institutional_affiliation = $institutionalAffiliation;
    
        return $this;
    }

    /**
     * Get institutional_affiliation
     *
     * @return integer 
     */
    public function getInstitutionalAffiliation()
    {
        return $this->institutional_affiliation;
    }

    /**
     * Set education_system
     *
     * @param string $educationSystem
     * @return Company
     */
    public function setEducationSystem($educationSystem)
    {
        $this->education_system = $educationSystem;
    
        return $this;
    }

    /**
     * Get education_system
     *
     * @return string 
     */
    public function getEducationSystem()
    {
        return $this->education_system;
    }

    /**
     * Set internal_school
     *
     * @param boolean $internalSchool
     * @return Company
     */
    public function setInternalSchool($internalSchool)
    {
        $this->internal_school = $internalSchool;
    
        return $this;
    }

    /**
     * Get internal_school
     *
     * @return boolean 
     */
    public function getInternalSchool()
    {
        return $this->internal_school;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Company
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add headquarters
     *
     * @param Fishman\EntityBundle\Entity\Headquarter $headquarters
     * @return Company
     */
    public function addHeadquarter(\Fishman\EntityBundle\Entity\Headquarter $headquarters)
    {
        $this->headquarters[] = $headquarters;
    
        return $this;
    }

    /**
     * Remove headquarters
     *
     * @param Fishman\EntityBundle\Entity\Headquarter $headquarters
     */
    public function removeHeadquarter(\Fishman\EntityBundle\Entity\Headquarter $headquarters)
    {
        $this->headquarters->removeElement($headquarters);
    }

    /**
     * Get headquarters
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getHeadquarters()
    {
        return $this->headquarters;
    }


    public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companycharges = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add companycharges
     *
     * @param Fishman\EntityBundle\Entity\Companycharge $companycharges
     * @return Company
     */
    public function addCompanycharge(\Fishman\EntityBundle\Entity\Companycharge $companycharges)
    {
        $this->companycharges[] = $companycharges;
    
        return $this;
    }

    /**
     * Remove companycharges
     *
     * @param Fishman\EntityBundle\Entity\Companycharge $companycharges
     */
    public function removeCompanycharge(\Fishman\EntityBundle\Entity\Companycharge $companycharges)
    {
        $this->companycharges->removeElement($companycharges);
    }

    /**
     * Get companycharges
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanycharges()
    {
        return $this->companycharges;
    }

    /**
     * Add companycourses
     *
     * @param Fishman\EntityBundle\Entity\Companycourse $companycourses
     * @return Company
     */
    public function addCompanycourse(\Fishman\EntityBundle\Entity\Companycourse $companycourses)
    {
        $this->companycourses[] = $companycourses;
    
        return $this;
    }

    /**
     * Remove companycourses
     *
     * @param Fishman\EntityBundle\Entity\Companycourse $companycourses
     */
    public function removeCompanycourse(\Fishman\EntityBundle\Entity\Companycourse $companycourses)
    {
        $this->companycourses->removeElement($companycourses);
    }

    /**
     * Get companycourses
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanycourses()
    {
        return $this->companycourses;
    }

    /**
     * Add companyorganizationalunits
     *
     * @param Fishman\EntityBundle\Entity\Companyorganizationalunit $companyorganizationalunits
     * @return Company
     */
    public function addCompanyorganizationalunit(\Fishman\EntityBundle\Entity\Companyorganizationalunit $companyorganizationalunits)
    {
        $this->companyorganizationalunits[] = $companyorganizationalunits;
    
        return $this;
    }

    /**
     * Remove companyorganizationalunits
     *
     * @param Fishman\EntityBundle\Entity\Companyorganizationalunit $companyorganizationalunits
     */
    public function removeCompanyorganizationalunit(\Fishman\EntityBundle\Entity\Companyorganizationalunit $companyorganizationalunits)
    {
        $this->companyorganizationalunits->removeElement($companyorganizationalunits);
    }

    /**
     * Get companyorganizationalunits
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanyorganizationalunits()
    {
        return $this->companyorganizationalunits;
    }

    /**
     * Add workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     * @return Company
     */
    public function addWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations[] = $workinginformations;
    
        return $this;
    }

    /**
     * Remove workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     */
    public function removeWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations->removeElement($workinginformations);
    }

    /**
     * Get workinginformations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkinginformations()
    {
        return $this->workinginformations;
    }

    /**
     * Add pollschedulings
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollschedulings
     * @return Company
     */
    public function addPollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollschedulings)
    {
        $this->pollschedulings[] = $pollschedulings;
    
        return $this;
    }

    /**
     * Remove pollschedulings
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollschedulings
     */
    public function removePollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollschedulings)
    {
        $this->pollschedulings->removeElement($pollschedulings);
    }

    /**
     * Get pollschedulings
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulings()
    {
        return $this->pollschedulings;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $workshopchedulings;


    /**
     * Add workshopchedulings
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopchedulings
     * @return Company
     */
    public function addWorkshopcheduling(\Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopchedulings)
    {
        $this->workshopchedulings[] = $workshopchedulings;
    
        return $this;
    }

    /**
     * Remove workshopchedulings
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopchedulings
     */
    public function removeWorkshopcheduling(\Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopchedulings)
    {
        $this->workshopchedulings->removeElement($workshopchedulings);
    }

    /**
     * Get workshopchedulings
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopchedulings()
    {
        return $this->workshopchedulings;
    }

    /**
     * Add companyfacultys
     *
     * @param Fishman\EntityBundle\Entity\Companyfaculty $companyfacultys
     * @return Company
     */
    public function addCompanyfaculty(\Fishman\EntityBundle\Entity\Companyfaculty $companyfacultys)
    {
        $this->companyfacultys[] = $companyfacultys;
    
        return $this;
    }

    /**
     * Remove companyfacultys
     *
     * @param Fishman\EntityBundle\Entity\Companyfaculty $companyfacultys
     */
    public function removeCompanyfaculty(\Fishman\EntityBundle\Entity\Companyfaculty $companyfacultys)
    {
        $this->companyfacultys->removeElement($companyfacultys);
    }

    /**
     * Get companyfacultys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanyfacultys()
    {
        return $this->companyfacultys;
    }

    /**
     * Add companycareers
     *
     * @param Fishman\EntityBundle\Entity\Companycareer $companycareers
     * @return Company
     */
    public function addCompanycareer(\Fishman\EntityBundle\Entity\Companycareer $companycareers)
    {
        $this->companycareers[] = $companycareers;
    
        return $this;
    }

    /**
     * Remove companycareers
     *
     * @param Fishman\EntityBundle\Entity\Companycareer $companycareers
     */
    public function removeCompanycareer(\Fishman\EntityBundle\Entity\Companycareer $companycareers)
    {
        $this->companycareers->removeElement($companycareers);
    }

    /**
     * Get companycareers
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanycareers()
    {
        return $this->companycareers;
    }

    /**
     * Get list company options
     * 
     */
    public static function getCompanyCustomHeader(DoctrineRegistry $doctrine, $id = '') 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Company');
        $result = $repository->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->where('c.status = 1 
                    AND c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        
                
        return $output;
    }

    /**
     * Get list company options
     * 
     */
    public static function getListCompanyOptions(DoctrineRegistry $doctrine, $type = '') 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Company');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->where('c.status = 1 
                    AND c.id <> 2')
            ->orderBy('c.name', 'ASC');
        
        if ($type != '' && $type != 'working') {
            $queryBuilder
                ->andWhere('c.company_type = :type')
                ->setParameter('type', $type);
        }
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $output[$r['id']] = $r['name'];
        }
        
        return $output;
    }

    /**
     * Get list company type options
     * 
     */
    public static function getListCompanyTypeOptions() 
    {
        $output = array(
            'working' => 'Laboral',
            'school' => 'Coelgio',
            'university' => 'Universidad'
        );
        
        return $output;
    }

    /**
     * Get list document type options
     * 
     */
    public static function getListDocumentTypeOptions() 
    {
        $output = array(
            'ruc' => 'RUC',
            'nit' => 'NIT'
        );
        
        return $output;
    }

    /**
     * Get List Pollschedulings by Poll
     */
    public static function getListFiltersByTypeCompany(DoctrineRegistry $doctrineRegistry, $companyId = '')
    {
        $em = $doctrineRegistry->getManager();
        
        $entity = $em->getRepository('FishmanEntityBundle:Company')->find($companyId);
        
        $output = array(
            'period' => 'Periodo',
            'industry' => 'Industria',
            'nature' => 'Naturaleza',
            'volume_business' => 'Volumen de Ventas',
            'number_employees' => 'Número de Empleados',
            'ranking_top' => 'Ranking Top'
        );
        
        if ($entity->getCompanyType() == 'school') {
            $output['range_pension'] = 'Rango de Pensión';
            $output['type'] = 'Tipo';
            $output['gender'] = 'Genero';
            $output['number_students'] = 'Número de Alumnos';
            $output['religious_affiliation'] = 'Afiliación Religiosa';
            $output['institutional_affiliation'] = 'Afiliaciones Intitucionales';
            $output['education_system'] = 'Sistema Educativo';
            $output['internal_school'] = 'Bachillerato Interno (IB)';
        }
        elseif ($entity->getCompanyType() == 'school') {
            $output['avarage_pension'] = 'Pensión Promedio';
            $output['type'] = 'Tipo';
            $output['number_students'] = 'Número de Alumnos';
            $output['faculty'] = 'Facultad';
        }
        
        return $output;
    }

    /**
     * Get Widget Pollschedulings By Poll
     */
    public static function getWidgetFiltersByTypeCompany(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $selectedIds = FALSE)
    {
        $leftOptions = '';
        $rightOptions = '';
        
        if ($selectedIds) {
            
            $filters = self::getListFiltersByTypeCompany($doctrineRegistry, $companyId);
            
            if ($filters) {
                foreach ($filters as $filter_key => $filter_value) {
                    $option = FALSE;
                    foreach ($selectedIds as $id_key => $id_value) {
                        if ($id_value == $filter_key && !$option) {
                            $rightOptions .= '<option value="' . $filter_key . '">' . $filter_value . '</option>';
                            $option = TRUE;
                            unset($filters[$filter_key]);
                        }
                    }
                }
                foreach ($filters as $filter_key => $filter_value) {
                    $leftOptions .= '<option value="' . $filter_key . '">' . $filter_value . '</option>';
                }
            }
            
        }
        
        $output = self::getWidgetMultipleSelect('filters', $leftOptions, $rightOptions);
        
        return $output;
    }

    /**
     * Get List Companies Follow
     */
    public static function getListCompaniesFollow(DoctrineRegistry $doctrineRegistry, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanEntityBundle:Company');
        $queryBuilder = $repository->createQueryBuilder('c')
            ->select('c.id, c.name, c.ruc, n.name nature, c.contact, c.phone, c.email')
            ->innerJoin('FishmanPollBundle:Pollscheduling', 'psc', 'WITH', 'c.id = psc.company_id')
            ->leftJoin('c.nature', 'n')
            ->orderBy('c.id', 'ASC')
            ->groupBy('c.id');
        
        // Add arguments
        
        if (!empty($data)) {
            $queryBuilder
                ->where('c.id LIKE :id 
                        OR c.name LIKE :name 
                        OR c.ruc LIKE :ruc 
                        OR c.contact LIKE :contact 
                        OR c.phone LIKE :phone 
                        OR c.email LIKE :email')
                ->setParameter('id', '%' . $data['word'] . '%')
                ->setParameter('name', '%' . $data['word'] . '%')
                ->setParameter('ruc', '%' . $data['word'] . '%')
                ->setParameter('contact', '%' . $data['word'] . '%')
                ->setParameter('phone', '%' . $data['word'] . '%')
                ->setParameter('email', '%' . $data['word'] . '%')
                ->andWhere('c.id <> 2');
            
            if ($data['nature'] != '') {
                $queryBuilder
                    ->andWhere('c.nature = :nature')
                    ->setParameter('nature', $data['nature']);
            }
        }
        
        $query = $queryBuilder->getQuery();
            
        return $query;
    }

    /**
     * Get list grade options
     * 
     */
    public static function getListFilterOptions($typeFiltersCompany, $typeFiltersPsc) 
    {
        $output = array();
        
        if ($typeFiltersPsc != 'not_evaluateds') {
            switch ($typeFiltersCompany) {
                case 'working':
                    $output = array(
                        'organizationalunits' => 'Unidades Organizativas', 
                        'charges' => 'Cargos', 
                        'headquarters' => 'Sedes', 
                        'evaluateds' => 'Evaluados'
                    );
                    break;
                case 'university':
                    $output = array(
                        'organizationalunits' => 'Unidades Organizativas', 
                        'charges' => 'Cargos', 
                        'faculties' => 'Facultades', 
                        'careers' => 'Carreras', 
                        'headquarters' => 'Sedes', 
                        'evaluateds' => 'Evaluados'
                    );
                    break;
                case 'school':
                    $output = array(
                        'organizationalunits' => 'Unidades Organizativas', 
                        'charges' => 'Cargos', 
                        'grades' => 'Grados', 
                        'headquarters' => 'Sedes', 
                        'evaluateds' => 'Evaluados'
                    );
                    break;
            }
        }
        
        return $output;
    }

    /**
     * Get list grade options
     * 
     */
    public static function getListRankingTopOptions() 
    {
        $output = array();
        
        $output = array(
            '10' => '10%', 
            '20' => '20%', 
            '30' => '30%', 
            '40' => '40%', 
            '50' => '50%', 
            '60' => '60%', 
            '70' => '70%', 
            '80' => '80%', 
            '90' => '90%'
        );
        
        return $output;
    }

    /**
     * Get list type options
     * 
     */
    public static function getListTypeOptions() 
    {
        $output = array();
        
        $output = array(
            'particular' => 'Particular', 
            'national' => 'Nacional'
        );
        
        return $output;
    }

    /**
     * Get list gender options
     * 
     */
    public static function getListGenderOptions() 
    {
        $output = array();
        
        $output = array(
            'mixed' => 'Mixto', 
            'male' => 'Masculino', 
            'female' => 'Femenino'
        );
        
        return $output;
    }

    /**
     * Get list religious affiliation on options
     * 
     */
    public static function getListReligiousAffiliationOnOptions() 
    {
        $output = array();
        
        $output = array(
            1 => 'Si', 
            0 => 'No'
        );
        
        return $output;
    }

    /**
     * Get list education system on options
     * 
     */
    public static function getListEducationSystemOptions() 
    {
        $output = array();
        
        $output = array(
            'international' => 'Internacional', 
            'national' => 'Nacional'
        );
        
        return $output;
    }

    /**
     * Get list internal school on options
     * 
     */
    public static function getListInternalSchoolOptions() 
    {
        $output = array();
        
        $output = array(
            1 => 'Si', 
            0 => 'No'
        );
        
        return $output;
    }

    /**
     * Get Widget Multiple Filters Options
     */
    public static function getWidgetFilterOptions(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $pscId = FALSE, $selectedId = FALSE)
    {
        $output = '';
        $typeFiltersCompany = '';
        $typeFiltersPsc = '';
        $entities = array();
        
        if ($companyId != 2) {
            if ($companyId) {
                $company = $doctrineRegistry->getRepository('FishmanEntityBundle:Company')->find($companyId);
                $typeFiltersCompany = $company->getCompanyType();
            }
            
            if ($pscId) {
                $pollscheduling = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling')->find($pscId);
                $typeFiltersPsc = $pollscheduling->getType();
            }
            
            $entities = self::getListFilterOptions($typeFiltersCompany, $typeFiltersPsc);
        }
        
        $output = self::getWidgetSelect('filter', $entities, $selectedId, '');
        
        return $output;
    }

    /**
     * Get Widget Industries
     */
    public static function getWidgetIndustries(DoctrineRegistry $doctrineRegistry = NULL, $selectedId = FALSE)
    {
        $entities = Industry::getListIndustryOptions($doctrineRegistry);
        $output = self::getWidgetSelect('filters[industry]', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Natures
     */
    public static function getWidgetNatures(DoctrineRegistry $doctrineRegistry = NULL, $selectedId = FALSE)
    {
        $entities = Nature::getListNatureOptions($doctrineRegistry);
        $output = self::getWidgetSelect('filters[nature]', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Volume Business
     */
    public static function getWidgetVolumeBusiness(DoctrineRegistry $doctrineRegistry = NULL, $selectedId = FALSE)
    {
        $entities = Volumebusiness::getListVolumeBusinessOptions($doctrineRegistry);
        $output = self::getWidgetSelect('filters[volumebusiness]', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Number Employees
     */
    public static function getWidgetNumberPeoples(DoctrineRegistry $doctrineRegistry = NULL, $type = 'working', $selectedId = FALSE)
    {
        $entities = Numberpeople::getListNumberPeopleOptions($doctrineRegistry, $type);
        $output = self::getWidgetSelect('filters[numberpeople' . $type . ']', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Ranking Tops
     */
    public static function getWidgetRankingTops($selectedId = FALSE)
    {
        $entities = self::getListRankingTopOptions();
        $output = self::getWidgetSelect('filters[rankingtop]', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Range Pensions School
     */
    public static function getWidgetRangePensions(DoctrineRegistry $doctrineRegistry = NULL, $type, $selectedId = FALSE)
    {
        $entities = Rangepension::getListRangePensionOptions($doctrineRegistry, $type);
        $output = self::getWidgetSelect('filters[rangepension' . $type . ']', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Types
     */
    public static function getWidgetTypes($type, $selectedId = FALSE)
    {
        $entities = self::getListTypeOptions();
        $output = self::getWidgetSelect('filters[type' . $type . ']', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Genders
     */
    public static function getWidgetGenders($selectedId = FALSE)
    {
        $entities = self::getListGenderOptions();
        $output = self::getWidgetSelect('filters[gender]', $entities, $selectedId, '');
        return $output;
    }
    
    /**
     * Get Widget Religious Affiliation On
     */
    public static function getWidgetReligiousAffiliationOn($selectedId = FALSE)
    {
        $entities = self::getListReligiousAffiliationOnOptions();
        $output = self::getWidgetSelect('filters[religiousaffiliationon]', $entities, $selectedId, '', TRUE);
        return $output;
    }
    
    /**
     * Get Widget Religious Affiliation
     */
    public static function getWidgetReligiousAffiliations(DoctrineRegistry $doctrineRegistry = NULL, $selectedId = FALSE)
    {
        $entities = Religiousaffiliation::getListReligiousAffiliationOptions($doctrineRegistry);
        $output = self::getWidgetSelect('filters[religiousaffiliation]', $entities, $selectedId, '');
        return $output;
    }
    
    /**
     * Get Widget Institutional Affiliation
     */
    public static function getWidgetInstitutionalAffiliations(DoctrineRegistry $doctrineRegistry = NULL, $selectedId = FALSE)
    {
        $entities = Institutionalaffiliation::getListInstitutionalAffiliationOptions($doctrineRegistry);
        $output = self::getWidgetSelect('filters[institutionalaffiliation]', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Education Systems
     */
    public static function getWidgetEducationSystems($selectedId = FALSE)
    {
        $entities = self::getListEducationSystemOptions();
        $output = self::getWidgetSelect('filters[educationsystem]', $entities, $selectedId, '');
        return $output;
    }

    /**
     * Get Widget Internal Schools
     */
    public static function getWidgetInternalSchools($selectedId = FALSE)
    {
        $entities = self::getListInternalSchoolOptions();
        $output = self::getWidgetSelect('filters[internalschool]', $entities, $selectedId, '', TRUE);
        return $output;
    }

    /**
     * Get Widget Multiple OrganizationalUnits By Company
     */
    public static function getWidgetMultipleOrganizationalUnitsByCompany(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $selectedIds = FALSE)
    {
        $entities = array();
        
        $entities['administrative'] = 'Administrativas';
        Companyorganizationalunit::addChoiceOptions($doctrineRegistry, Companyorganizationalunit::getRootParents($doctrineRegistry, $companyId, 'administrative'), 3, $entities, 0);
        
        $countEntities = count($entities);
        if ($countEntities == 1) {
            unset($entities);
        }
        
        $entities['academic'] = 'Académicas';
        Companyorganizationalunit::addChoiceOptions($doctrineRegistry, Companyorganizationalunit::getRootParents($doctrineRegistry, $companyId, 'academic'), 3, $entities, 0);
        if ($countEntities == (count($entities) - 1)) {
            unset($entities['academic']);
        }
        
        $output = self::getWidgetMultipleSelectCopyBuildOptions('organizationalunits', $entities, $selectedIds, 'selectOptionsCopy');
        
        return $output;
    }

    /**
     * Get Widget Charges By Company
     */
    public static function getWidgetMultipleChargesByCompany(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $selectedIds = FALSE)
    {
        $entities = Companycharge::getListCompanychargeOptions($doctrineRegistry, $companyId);
        $output = self::getWidgetMultipleSelectBuildOptions('charges', $entities, $selectedIds);
        return $output;
    }

    /**
     * Get Widget Headquarters By Company
     */
    public static function getWidgetMultipleHeadquartersByCompany(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $selectedIds = FALSE)
    {
        $entities = Headquarter::getListHeadquarterOptions($doctrineRegistry, $companyId);
        $output = self::getWidgetMultipleSelectBuildOptions('headquarters', $entities, $selectedIds);
        return $output;
    }

    /**
     * Get Widget Faculties By Company
     */
    public static function getWidgetMultipleFacultiesByCompany(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $selectedIds = FALSE)
    {
        $entities = Companyfaculty::getListCompanyfacultyOptions($doctrineRegistry, $companyId);
        $output = self::getWidgetMultipleSelectBuildOptions('faculties', $entities, $selectedIds);
        return $output;
    }

    /**
     * Get Widget Careers By Company
     */
    public static function getWidgetMultipleCareersByCompany(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $selectedIds = FALSE)
    {
        $entities = Companycareer::getListCompanycareerOptions($doctrineRegistry, $companyId);
        $output = self::getWidgetMultipleSelectBuildOptions('careers', $entities, $selectedIds);
        return $output;
    }

    /**
     * Get Widget Grades By Company
     */
    public static function getWidgetMultipleGradesByCompany(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $selectedIds = FALSE)
    {
        $leftOptions = '';
        $rightOptions = '';
        
        $grades = Workinginformation::getListGradeOptions();
            
        if ($grades) {
            foreach ($grades as $grade_key => $grade_value) {
                $option = FALSE;
                if (isset($selectedIds['data2'])) {
                    foreach ($selectedIds['data2'] as $id_key => $id_value) {
                        if ($id_value == $grade_key && !$option) {
                            $rightOptions .= '<option value="' . $grade_key . '">' . $grade_value . '</option>';
                            $option = TRUE;
                            unset($grades[$grade_key]);
                        }
                    }
                }
            }
            foreach ($grades as $grade_key => $grade_value) {
                $leftOptions .= '<option value="' . $grade_key . '">' . $grade_value . '</option>';
            }
        }
        
        $output = self::getWidgetMultipleSelect('grades', $leftOptions, $rightOptions);
        
        return $output;
    }

    /**
     * Get Widget Select
     */
    public static function getWidgetSelect($widgetId, $entities, $selectedId, $required = 'required', $notDefault = FALSE)
    {   
        $output = '<select name="options_' . $widgetId . '" id="options_' . $widgetId . '" ' . $required . ' >';
        if (!$notDefault) {
            $output .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        }
        if (!empty($entities)) {
            foreach ($entities as $key => $value) {
                if ($selectedId != '') {
                    if ($selectedId == $key) {
                        $output .= '<option value="' . $key . '" selected>' . $value . '</option>';
                    }
                    else {
                        $output .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }
                else {
                    $output .= '<option value="' . $key . '">' . $value . '</option>';
                }
            }
        }
        $output .= '</select>';
        
        return $output;
    }

    /**
     * Get Widget Multiple select build Options
     */
    public static function getWidgetMultipleSelectBuildOptions($widgetId, $entities, $selectedIds, $selectClass = '')
    {
        $leftOptions = '';
        $rightOptions = '';
        
        if ($entities) {
            foreach ($entities as $key => $value) {
                $option = FALSE;
                if (isset($selectedIds['data2'])) {
                    foreach ($selectedIds['data2'] as $id) {
                        if ($id == $key && !$option) {
                            $rightOptions .= '<option value="' . $key . '">' . $value . '</option>';
                            $option = TRUE;
                            unset($entities[$key]);
                        }
                    }
                }
            }
            foreach ($entities as $key => $value) {
                if ($widgetId == 'organizationalunits') {
                    if (in_array($key, array('administrative', 'academic'))) {
                        $leftOptions .= '<option value="' . $key . '" disabled>' . $value . '</option>';
                    }
                    else {
                        $leftOptions .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }
                else {
                    $leftOptions .= '<option value="' . $key . '">' . $value . '</option>';
                }
            }
        }
        
        $output = self::getWidgetMultipleSelect($widgetId, $leftOptions, $rightOptions, $selectClass);
        
        return $output;
    }

    /**
     * Get Widget Multiple select Copy build Options
     */
    public static function getWidgetMultipleSelectCopyBuildOptions($widgetId, $entities, $selectedIds, $selectClass = '')
    {
        $leftOptions = '';
        $rightOptions = '';
        
        if ($entities) {
            foreach ($entities as $key => $value) {
                $option = FALSE;
                if (isset($selectedIds['data2'])) {
                    foreach ($selectedIds['data2'] as $id) {
                        if ($id == $key && !$option) {
                            $leftOptions .= '<option value="' . $key . '" disabled>' . $value . '</option>';
                            $option = TRUE;
                        }
                    }
                }
                if ($widgetId == 'organizationalunits') {
                    if (in_array($key, array('administrative', 'academic'))) {
                        $leftOptions .= '<option value="' . $key . '" disabled>' . $value . '</option>';
                    }
                    elseif (!$option) {
                        $leftOptions .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }
                elseif (!$option) {
                    $leftOptions .= '<option value="' . $key . '">' . $value . '</option>';
                }
            }
            foreach ($entities as $key => $value) {
                $option = FALSE;
                if (isset($selectedIds['data2'])) {
                    foreach ($selectedIds['data2'] as $id) {
                        if ($id == $key && !$option) {
                            $rightOptions .= '<option value="' . $key . '">' . str_replace('---', '', $value) . '</option>';
                            $option = TRUE;
                        }
                    }
                }
            }
        }
        
        $output = self::getWidgetMultipleSelect($widgetId, $leftOptions, $rightOptions, $selectClass);
        
        return $output;
    }

    /**
     * Get Widget Multiple select
     */
    public static function getWidgetMultipleSelect($widgetId, $leftOptions, $rightOptions, $selectClass = '')
    {
        if ($selectClass == '') {
            $selectClass = 'selectOptionsData';
        }
        
        $output = '<div class="leftBox">';
        $output .= '<select name="' . $widgetId . '_options[data1][]" class="multiple ' . $selectClass . '1" multiple="multiple">';
        $output .= $leftOptions;
        $output .= '</select>';
        $output .= '</div>';
        $output .= '<div class="dualControl">';
        $output .= '<button type="button" class="moveToRightOptions dualBtn mr5 mb15">&nbsp;&gt;&nbsp;</button>';
        $output .= '<button type="button" class="moveToAllRightOptions dualBtn mr5 mb15">&nbsp;&gt;&gt;&nbsp;</button>';
        $output .= '<button type="button" class="moveToLeftOptions dualBtn mr5 mb15">&nbsp;&lt;&nbsp;</button>';
        $output .= '<button type="button" class="moveToAllLeftOptions dualBtn mr5 mb15">&nbsp;&lt;&lt;&nbsp;</button>';
        $output .= '</div>';
        $output .= '<div class="rightBox">';
        $output .= '<select name="' . $widgetId . '_options[data2][]" class="multiple ' . $selectClass . '2" multiple="multiple">';
        $output .= $rightOptions;
        $output .= '</select>';
        $output .= '</div>';
        
        return $output;
    }
    
    /**
     * Delete temporal files
     * 
     */
    public static function deleteTemporalFiles($directory)
    {
        $dir = opendir($directory);
        while ($file = readdir($dir)) {
            if (is_file($file)) {
                unlink($directory . $file);
            }
        }
    }
}