<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Industry
 */
class Industry
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Company", mappedBy="industry")
     */
    protected $companys;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Industry
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Industry
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    

    public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companys = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add companys
     *
     * @param Fishman\EntityBundle\Entity\Company $companys
     * @return Industry
     */
    public function addCompany(\Fishman\EntityBundle\Entity\Company $companys)
    {
        $this->companys[] = $companys;
    
        return $this;
    }

    /**
     * Remove companys
     *
     * @param Fishman\EntityBundle\Entity\Company $companys
     */
    public function removeCompany(\Fishman\EntityBundle\Entity\Company $companys)
    {
        $this->companys->removeElement($companys);
    }

    /**
     * Get companys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanys()
    {
        return $this->companys;
    }

    /**
     * Get list industry options
     * 
     */
    public static function getListIndustryOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Industry');
        $queryBuilder = $repository->createQueryBuilder('i')
            ->select('i.id, i.name')
            ->orderBy('i.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }
    
}