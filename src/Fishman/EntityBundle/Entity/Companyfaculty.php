<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Companyfaculty
 */
class Companyfaculty
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /*
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Faculty", inversedBy="companyfacultys")
     * @ORM\JoinColumn(name="faculty_id", referencedColumnName="id")
     */
    protected $faculty;
  
    /* 
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Company", inversedBy="companyfacultys")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Companycareer", mappedBy="companyfaculty")
     */
    protected $companycareers;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Workinginformation", mappedBy="companyfaculty")
     */
    protected $workinginformations;

   
    public function __toString()
    {
         return $this->name;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Companyfaculty
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Companyfaculty
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Companyfaculty
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Companyfaculty
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Companyfaculty
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Companyfaculty
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Companyfaculty
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set faculty
     *
     * @param Fishman\EntityBundle\Entity\Faculty $faculty
     * @return Companyfaculty
     */
    public function setFaculty(\Fishman\EntityBundle\Entity\Faculty $faculty = null)
    {
        $this->faculty = $faculty;
    
        return $this;
    }

    /**
     * Get faculty
     *
     * @return Fishman\EntityBundle\Entity\Faculty 
     */
    public function getFaculty()
    {
        return $this->faculty;
    }

    /**
     * Set company
     *
     * @param Fishman\EntityBundle\Entity\Company $company
     * @return Companyfaculty
     */
    public function setCompany(\Fishman\EntityBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return Fishman\EntityBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companycareers = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add companycareers
     *
     * @param Fishman\EntityBundle\Entity\Companycareer $companycareers
     * @return Companyfaculty
     */
    public function addCompanycareer(\Fishman\EntityBundle\Entity\Companycareer $companycareers)
    {
        $this->companycareers[] = $companycareers;
    
        return $this;
    }

    /**
     * Remove companycareers
     *
     * @param Fishman\EntityBundle\Entity\Companycareer $companycareers
     */
    public function removeCompanycareer(\Fishman\EntityBundle\Entity\Companycareer $companycareers)
    {
        $this->companycareers->removeElement($companycareers);
    }

    /**
     * Get companycareers
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanycareers()
    {
        return $this->companycareers;
    }

    /**
     * Add workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     * @return Companyfaculty
     */
    public function addWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations[] = $workinginformations;
    
        return $this;
    }

    /**
     * Remove workinginformations
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformations
     */
    public function removeWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformations)
    {
        $this->workinginformations->removeElement($workinginformations);
    }

    /**
     * Get workinginformations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkinginformations()
    {
        return $this->workinginformations;
    }

    /**
     * Get list faculty to company options
     * 
     */
    public static function getListFacultyOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companyfaculty');
        $queryBuilder = $repository->createQueryBuilder('cf')
            ->select('f.id, f.name')
            ->innerJoin('cf.faculty', 'f')
            ->where('cf.company = :company 
                    AND f.status = 1')
            ->setParameter('company', $companyId)
            ->groupBy('f.id')
            ->orderBy('f.name', 'ASC');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }

    /**
     * Get list companyfaculty to company options
     * 
     */
    public static function getListCompanyfacultyOptions(DoctrineRegistry $doctrine, $companyId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Companyfaculty');
        $queryBuilder = $repository->createQueryBuilder('cf')
            ->select('cf.id, cf.name')
            ->where('cf.company = :company 
                    AND cf.status = 1')
            ->setParameter('company', $companyId)
            ->orderBy('cf.name', 'ASC');
        
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }

    public static function getSelectCompanyfaculty(DoctrineRegistry $doctrineRegistry, $companyId, $selectedId = false)
    {
        $repository = $doctrineRegistry->getManager()->getRepository('FishmanEntityBundle:Companyfaculty');
        $hs = $repository->createQueryBuilder('cf')
            ->select('cf.id, cf.name')
            ->where('cf.company = :companyid 
                    AND cf.status = 1')
            ->setParameter('companyid', $companyId)
            ->orderBy('cf.name', 'ASC')
            ->getQuery()
            ->getResult();

        $selectCompanyfaculty = '<select class="companyfaculty" name="company_faculty">';
        $selectCompanyfaculty .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        if($hs){
            foreach($hs as $h){
                $selected = '';
                if($selectedId == $h['id']){
                    $selected = ' selected ';
                }
                $selectCompanyfaculty .= '<option value="' . $h['id'] . '"'. $selected .'>' .  $h['name'] . '</option>';
            }
        }
        $selectCompanyfaculty .= '</select>';
        
        return $selectCompanyfaculty;
    }
    
}