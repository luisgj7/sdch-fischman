<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Charge
 */
class Charge
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;


    /**
     * @ORM\OneToMany(targetEntity="Fishman\EntityBundle\Entity\Companycharge", mappedBy="charge")
     */
    protected $companycharges; 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Charge
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Charge
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Charge
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Charge
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Charge
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Charge
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   


    
    public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companycharges = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add companycharges
     *
     * @param Fishman\EntityBundle\Entity\Companycharge $companycharges
     * @return Charge
     */
    public function addCompanycharge(\Fishman\EntityBundle\Entity\Companycharge $companycharges)
    {
        $this->companycharges[] = $companycharges;
    
        return $this;
    }

    /**
     * Remove companycharges
     *
     * @param Fishman\EntityBundle\Entity\Companycharge $companycharges
     */
    public function removeCompanycharge(\Fishman\EntityBundle\Entity\Companycharge $companycharges)
    {
        $this->companycharges->removeElement($companycharges);
    }

    /**
     * Get companycharges
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanycharges()
    {
        return $this->companycharges;
    }

    /**
     * Get list charge options
     * 
     */
    public static function getListChargeOptions(DoctrineRegistry $doctrine) 
    {
        $output = '';
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Charge');
        $queryBuilder = $repository->createQueryBuilder('ch')
            ->select('ch.id, ch.name')
            ->where('ch.status = 1')
            ->orderBy('ch.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        else {
            $output = array();
        }
        
        return $output;
    }
}