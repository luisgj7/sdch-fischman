<?php

namespace Fishman\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\EntityBundle\Entity\Nature
 */
class Nature
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $name
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="Fishman\AuthBundle\Entity\Company", mappedBy="nature")
     */
    protected $companys;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Nature
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Nature
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Nature
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    

    public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companys = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add companys
     *
     * @param Fishman\EntityBundle\Entity\Company $companys
     * @return Nature
     */
    public function addCompany(\Fishman\EntityBundle\Entity\Company $companys)
    {
        $this->companys[] = $companys;
    
        return $this;
    }

    /**
     * Remove companys
     *
     * @param Fishman\EntityBundle\Entity\Company $companys
     */
    public function removeCompany(\Fishman\EntityBundle\Entity\Company $companys)
    {
        $this->companys->removeElement($companys);
    }

    /**
     * Get companys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCompanys()
    {
        return $this->companys;
    }

    /**
     * Get list nature
     * 
     */
    public static function getListNature(DoctrineRegistry $doctrine) 
    {
        $output = '';
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Nature');
        $queryBuilder = $repository->createQueryBuilder('n')
            ->orderBy('n.id', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $output[$r->getId()]['type'] = $r->getType();
            $output[$r->getId()]['name'] = $r->getName();
        }
        
        return $output;
    }

    /**
     * Get list nature options
     * 
     */
    public static function getListNatureOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanEntityBundle:Nature');
        $queryBuilder = $repository->createQueryBuilder('n')
            ->select('n.id, n.name')
            ->orderBy('n.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }
}