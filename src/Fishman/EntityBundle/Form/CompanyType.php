<?php

namespace Fishman\EntityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

class CompanyType extends AbstractType
{
    private $company;
    private $doctrine;

    public function __construct($company, Registry $doctrine)
    {
        $this->company = $company;
        $this->doctrine = $doctrine;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Volume Business
        
        $volumebusiness_options = array();
        $repository = $this->doctrine->getRepository('FishmanEntityBundle:Volumebusiness');
        $result = $repository->createQueryBuilder('vb')
            ->select('vb.id, vb.rank')
            ->orderBy('vb.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach($result as $r) {
            $volumebusiness_options[$r['id']] = $r['rank'];
        }
        
        // Number Employees
        
        $numberemployees_options = array();
        $repository = $this->doctrine->getRepository('FishmanEntityBundle:Numberpeople');
        $result = $repository->createQueryBuilder('np')
            ->select('np.id, np.rank')
            ->orderBy('np.id', 'ASC')
            ->where('np.type = :type')
            ->setParameter('type', 'working')
            ->getQuery()
            ->getResult();
        
        foreach($result as $r) {
            $numberemployees_options[$r['id']] = $r['rank'];
        }
        
        // Religious affilitation
        
        $religiousaffiliation_options = array();
        $repository = $this->doctrine->getRepository('FishmanEntityBundle:Religiousaffiliation');
        $result = $repository->createQueryBuilder('ra')
            ->select('ra.id, ra.name')
            ->orderBy('ra.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach($result as $r) {
            $religiousaffiliation_options[$r['id']] = $r['name'];
        }
        
        // Institucional affilitation
        
        $institutionalaffiliation_options = array();
        $repository = $this->doctrine->getRepository('FishmanEntityBundle:Institutionalaffiliation');
        $result = $repository->createQueryBuilder('ia')
            ->select('ia.id, ia.name')
            ->orderBy('ia.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach($result as $r) {
            $institutionalaffiliation_options[$r['id']] = $r['name'];
        }
        
        $builder
            ->add('code', 'text', array(
                'max_length' => 30,
                'invalid_message_parameters' => array('%num%' => 30),
            ))
            ->add('name', 'text', array(
                'max_length' => 60,
                'invalid_message_parameters' => array('%num%' => 60),
            ))
            ->add('document_type', 'choice', array(
                'choices' => array(
                    'ruc' => 'RUC', 
                    'nit' => 'NIT'
                ),
                'empty_value' => 'Choose an option'
            ))
            ->add('ruc', 'text', array(
                'max_length' => 11,
                'invalid_message_parameters' => array('%num%' => 11),
            )) 
            ->add('contact', 'text', array(
                'max_length' => 60,
                'invalid_message_parameters' => array('%num%' => 60),
            ))
            ->add('phone', 'text', array(
                'max_length' => 16,
                'invalid_message_parameters' => array('%num%' => 7),
            ))
            ->add('annex', 'text', array(
                'max_length' => 6,
                'required' => FALSE,
                'invalid_message_parameters' => array('%num%' => 6),
            ))
            ->add('celphone', 'text', array(
                'max_length' => 16,
                'required' => FALSE, 
                'invalid_message_parameters' => array('%num%' => 9),
            ))
            ->add('email', 'email', array(
                'max_length' => 60,
                'invalid_message_parameters' => array('%num%' => 60),
            ))
            ->add('industry', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Industry',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('i')
                      ->orderBy('i.name', 'ASC');
                },
                'empty_value' => 'Choose an option'
            ))
            ->add('nature', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Nature',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('n')
                      ->orderBy('n.name', 'ASC');
                },
                'empty_value' => 'Choose an option'
            ))
            ->add('volume_business', 'choice', array(
                'choices' => $volumebusiness_options,  
                'empty_value' => 'Choose an option', 
                'required' => FALSE
            ))
            ->add('number_employees', 'choice', array(
                'choices' => $numberemployees_options,  
                'empty_value' => 'Choose an option', 
                'required' => FALSE
            ))
            ->add('gender', 'choice', array(
                'choices' => array(
                    'mixed' => 'Mixto', 
                    'male' => 'Másculino', 
                    'female' => 'Femenino'
                ),
                'empty_value' => 'Choose an option', 
                'required' => FALSE 
            ))
            ->add('religious_affiliation_on', 'choice', array(
                'choices' => array(
                    1 => 'Si', 
                    0 => 'No'
                ), 
                'required' => TRUE
            ))
            ->add('religious_affiliation', 'choice', array(
                'choices' => $religiousaffiliation_options,  
                'empty_value' => 'Choose an option', 
                'required' => FALSE
            ))
            ->add('institutional_affiliation', 'choice', array(
                'choices' => $institutionalaffiliation_options, 
                'multiple' => TRUE, 
                'required' => FALSE
            ))
            ->add('education_system', 'choice', array(
                'choices' => array(
                    'international' => 'Internacional', 
                    'national' => 'Nacional'
                ),
                'empty_value' => 'Choose an option', 
                'required' => FALSE
            ))
            ->add('internal_school', 'choice', array(
                'choices' => array(
                    1 => 'Si', 
                    0 => 'No'
                ), 
                'required' => TRUE 
            ))
            ->add('document', 'file', array(
                'required' => false
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option' 
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\EntityBundle\Entity\Company'
        ));
    }

    public function getName()
    {
        return 'fishman_entitybundle_companytype';
    }
}
