<?php

namespace Fishman\EntityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Fishman\EntityBundle\Entity\Faculty;

class CompanyfacultyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company')
            ->add('code', 'text', array(
                'max_length' => 30,
                'invalid_message_parameters' => array('%num%' => 30),
            ))
            ->add('name', 'text', array(
                'max_length' => 60,
                'invalid_message_parameters' => array('%num%' => 60),
            ))
            ->add('faculty', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Faculty',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('f')
                      ->where('f.status = 1')
                      ->orderBy('f.name', 'ASC');
                }, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\EntityBundle\Entity\Companyfaculty'
        ));
    }

    public function getName()
    {
        return 'fishman_entitybundle_companyfacultytype';
    }
}
