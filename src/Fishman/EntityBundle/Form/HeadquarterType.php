<?php

namespace Fishman\EntityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\EntityBundle\Entity\Headquarter;

class HeadquarterType extends AbstractType
{
    private $headquarter;
    private $doctrine;

    public function __construct(Headquarter $headquarter, Registry $doctrine)
    {
        $this->headquarter = $headquarter;
        $this->doctrine = $doctrine;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $departmentid = '';
        $provinceid = '';
        $districtid = '';

        if ($this->headquarter->getDepartment() != '') {
            $departmentid = $this->headquarter->getDepartment();
        }
        if ($this->headquarter->getProvince() != '') {
            $provinceid = $this->headquarter->getProvince();
        }
        if ($this->headquarter->getDistrict() != '') {
            $districtid = $this->headquarter->getDistrict();
        }
        
        $builder
            ->add('code', 'text', array(
                'max_length' => 30,
                'invalid_message_parameters' => array('%num%' => 30),
            ))
            ->add('name', 'text', array(
                'max_length' => 120,
                'invalid_message_parameters' => array('%num%' => 120),
            ))
            ->add('via', 'choice', array(
                'choices' => array(
                    'av' => 'Av.', 
                    'street' => 'Calle',
                    'jr' => 'Jr.',
                    'batch' => 'Lote',
                    'via' => 'Vía'
                ),
                'empty_value' => 'Choose an option'
            ))
            ->add('address', 'text', array(
                'max_length' => 255,
                'invalid_message_parameters' => array('%num%' => 255),
            ))
            ->add('urbanization', 'text', array(
                'max_length' => 255,
                'invalid_message_parameters' => array('%num%' => 255),
                'required' => FALSE
            ))
            ->add('country', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Country',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('c')
                      ->orderBy('c.name', 'ASC');
                },
                'empty_value' => 'Choose an option'
            ))
            ->add('department', 'hidden', array(
                'data' => $departmentid,
                'mapped' => FALSE,
                'required' => FALSE
            ))
            ->add('province', 'hidden', array(
                'data' => $provinceid,
                'mapped' => FALSE,
                'required' => FALSE
            ))
            ->add('district', 'hidden', array(
                'data' => $districtid,
                'mapped' => FALSE,
                'required' => FALSE
            ))
            ->add('city', 'text', array(
                'max_length' => 255,
                'invalid_message_parameters' => array('%num%' => 255),
                'required' => FALSE
            ))
            ->add('phone_station', 'text', array(
                'max_length' => 30,
                'invalid_message_parameters' => array('%num%' => 30),
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\EntityBundle\Entity\Headquarter'
        ));
    }

    public function getName()
    {
        return 'fishman_entitybundle_headquartertype';
    }
}
