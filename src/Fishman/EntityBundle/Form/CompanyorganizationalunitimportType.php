<?php

namespace Fishman\EntityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompanyorganizationalunitimportType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      /*
        TODO : Falta validación del campo File con los siguientes propiedades

          'maxsize' => '2M',
          'mimeTypes' => array('application/xsl'),
          'mimeTypesMessage' => 'Porfavor subir un archivo XSL valido' 

       */

        $builder
          ->add('file', 'file', array( 
              'required' => TRUE
            ));
    }

    public function getName()
    {
        return 'fishman_entitybundle_companyorganizationalunitimporttype';
    }
}
