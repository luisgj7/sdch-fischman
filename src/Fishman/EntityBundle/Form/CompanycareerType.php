<?php

namespace Fishman\EntityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;

class CompanycareerType extends AbstractType
{
    private $company;
    private $faculty;
    private $doctrine;

    public function __construct($company, $faculty, Registry $doctrine)
    {
        $this->company = $company;
        $this->faculty = $faculty;
        $this->doctrine = $doctrine;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $repository = $this->doctrine->getRepository('FishmanEntityBundle:Companyfaculty');
        $result = $repository->createQueryBuilder('cf')
            ->select('cf.id, cf.name')
            ->where('cf.status = 1 
                    AND cf.company = :company')
            ->setParameter('company', $this->company)
            ->orderBy('cf.name', 'ASC')
            ->getQuery()
            ->getResult();

        $faculty_options = array();
        foreach($result as $r) {
            $faculty_options[$r['id']] = $r['name'];
        }
        
        $builder
            ->add('company')
            ->add('code', 'text', array(
                'max_length' => 30,
                'invalid_message_parameters' => array('%num%' => 30),
            ))
            ->add('name', 'text', array(
                'max_length' => 60,
                'invalid_message_parameters' => array('%num%' => 60),
            ))
            ->add('companyfaculty_id', 'choice', array(
                'choices' => $faculty_options, 
                'empty_value' => 'Choose an option', 
                'data' => $this->faculty,
                'mapped' => FALSE, 
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\EntityBundle\Entity\Companycareer'
        ));
    }

    public function getName()
    {
        return 'fishman_entitybundle_companycareertype';
    }
}
