<?php

namespace Fishman\EntityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\EntityBundle\Entity\Organizationalunit;
use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\AuthBundle\Entity\User;
use Fishman\AuthBundle\Entity\Workinginformation;

class CompanyorganizationalunitType extends AbstractType
{
    private $companyOrganizationalUnit;
    private $doctrine;

    public function __construct($companyOrganizationalUnit, Registry $doctrine)
    {
        $this->companyOrganizationalUnit = $companyOrganizationalUnit;
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parent_options = $this->companyOrganizationalUnit->getParentOptions($this->doctrine, $this->companyOrganizationalUnit->getCompany()->getId());

        //TODO: Boss id should be working information id and not user id
        $repository = $this->doctrine->getRepository('FishmanAuthBundle:Workinginformation');
        $result = $repository->createQueryBuilder('wi')
            ->select('wi.id, u.names, u.surname, u.lastname')
            ->innerJoin('wi.user', 'u')
            ->where('u.enabled = 1 
                    AND wi.company = :company')
            ->setParameter(':company', $this->companyOrganizationalUnit->getCompany()->getId())
            ->orderBy('u.surname', 'ASC', 'u.lastname', 'ASC', 'u.names', 'ASC')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();

        $boss_options = array();              
        foreach($result as $r) {
            $boss_options[$r['id']] = $r['surname'] . ' ' . $r['lastname'] . ' ' . $r['names'];
        }

        $builder
            ->add('code', 'text', array(
                'max_length' => 30,
                'invalid_message_parameters' => array('%num%' => 30),
            ))
            ->add('name', 'text', array(
                'max_length' => 60,
                'invalid_message_parameters' => array('%num%' => 60),
            ))
            ->add('organizationalunit', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Organizationalunit',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('o')
                      ->where('o.status = 1')
                      ->orderBy('o.name', 'ASC');
                }, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => array(
                    'administrative' => 'Administrativo', 
                    'directorate' => 'Dirección', 
                    'management' => 'Gerencia',
                    'headship' => 'Jefatura'
                ), 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('unit_type', 'hidden', array(
                'attr' => array('class' => 'utvalue') 
            ))
            ->add('parentid', 'choice', array(
                'choices' => $parent_options
            ))
            ->add('boss', 'choice', array(
                'choices' => $boss_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option' 
            ))
      
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\EntityBundle\Entity\Companyorganizationalunit'
        ));
    }

    public function getName()
    {
        return 'fishman_entitybundle_companyorganizationalunittype';
    }
}
