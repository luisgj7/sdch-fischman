<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Fishman\PollBundle\Entity\Benchmarking
 */
class Benchmarking
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $period
     */
    private $period;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollscheduling", mappedBy="benchmarking")
     */
    protected $pollschedulings;


    public function __toString()
    {
         return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Benchmarking
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set period
     *
     * @param string $period
     * @return Benchmarking
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return string 
     */
    public function getPeriod()
    {
        return $this->period;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollschedulings = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set status
     *
     * @param boolean $status
     * @return Benchmarking
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Benchmarking
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Benchmarking
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Benchmarking
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Benchmarking
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Add pollschedulings
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollschedulings
     * @return Benchmarking
     */
    public function addPollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollschedulings)
    {
        $this->pollschedulings[] = $pollschedulings;
    
        return $this;
    }

    /**
     * Remove pollschedulings
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollschedulings
     */
    public function removePollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollschedulings)
    {
        $this->pollschedulings->removeElement($pollschedulings);
    }

    /**
     * Get pollschedulings
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulings()
    {
        return $this->pollschedulings;
    }

    /**
     * Get list benchmarking options
     * 
     */
    public static function getListBenchmarkingOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanPollBundle:Benchmarking');
        $queryBuilder = $repository->createQueryBuilder('b')
            ->select('b.id, b.name')
            ->where('b.status = 1')
            ->orderBy('b.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['name'];
            }
        }
        
        return $output;
    }
}