<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PollsectionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PollsectionRepository extends EntityRepository
{
}
