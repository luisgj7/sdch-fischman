<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Fishman\PollBundle\Entity\Pollschedulingquestion;

/**
 * Fishman\PollBundle\Entity\Pollapplicationquestion
 */
class Pollapplicationquestion
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $answer
     */
    private $answer;

    /**
     * @var string $answer_options
     */
    private $answer_options;

    /**
     * @var integer $score
     */
    private $score;

    /**
     * @var boolean $finished
     */
    private $finished;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollapplication", inversedBy="pollapplicationquestions")
     * @ORM\JoinColumn(name="pollapplication_id", referencedColumnName="id")
     */
    protected $pollapplication;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollschedulingquestion", inversedBy="pollapplicationquestions")
     * @ORM\JoinColumn(name="pollschedulingquestion_id", referencedColumnName="id")
     */
    protected $pollschedulingquestion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return Pollapplicationquestion
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set answer_options
     *
     * @param boolean $answerOptions
     * @return Pollapplicationquestion
     */
    public function setAnswerOptions($answerOptions)
    {
        $this->answer_options = $answerOptions;
    
        return $this;
    }

    /**
     * Get answer_options
     *
     * @return boolean 
     */
    public function getAnswerOptions()
    {
        return $this->answer_options;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return Pollapplicationquestion
     */
    public function setScore($score)
    {
        $this->score = $score;
    
        return $this;
    }

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set finished
     *
     * @param boolean $finished
     * @return Pollapplicationquestion
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;
    
        return $this;
    }

    /**
     * Get finished
     *
     * @return boolean 
     */
    public function getFinished()
    {
        return $this->finished;
    }

    public function __toString()
    {
         return $this->question;
    }

    /**
     * Set pollapplication
     *
     * @param Fishman\PollBundle\Entity\Pollapplication $pollapplication
     * @return Pollapplicationquestion
     */
    public function setPollapplication(\Fishman\PollBundle\Entity\Pollapplication $pollapplication = null)
    {
        $this->pollapplication = $pollapplication;
    
        return $this;
    }

    /**
     * Get pollapplication
     *
     * @return Fishman\PollBundle\Entity\Pollapplication 
     */
    public function getPollapplication()
    {
        return $this->pollapplication;
    }

    /**
     * Set pollschedulingquestion
     *
     * @param Fishman\PollBundle\Entity\Pollquestion $pollschedulingquestion
     * @return Pollapplicationquestion
     */
    public function setPollschedulingquestion(\Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestion = null)
    {
        $this->pollschedulingquestion = $pollschedulingquestion;
    
        return $this;
    }

    /**
     * Get pollschedulingquestion
     *
     * @return Fishman\PollBundle\Entity\Pollschedulingquestion 
     */
    public function getPollschedulingquestion()
    {
        return $this->pollschedulingquestion;
    }

    /**
     * getArrayQuestions
     *
     */
    public static function getArrayQuestions($array, &$output = '')
    {
        if (!empty($array)) {
            $j = 0;
            foreach ($array as $key_section => $section) {
                if (isset($section['questions'])) {
                    $i = 0;
                    foreach ($section['questions'] as $key_question => $question) {
                        
                        if (isset($question['pollapplications'])) {
                            $output[$i] = array(
                                'pollapplications' => $question['pollapplications'],
                                'question' => $question['question'], 
                                'type' => $question['type'], 
                                'alignment' => $question['alignment'], 
                                'options' => $question['options'], 
                                'mandatory_response' => $question['mandatory_response'], 
                                'sequence' => $question['sequence']
                            );
                            $i++;
                        }
                        else {
                            $output[$question['id']] = array(
                                'id' => $question['id'],
                                'question' => $question['question'], 
                                'type' => $question['type'], 
                                'alignment' => $question['alignment'], 
                                'options' => $question['options'], 
                                'mandatory_response' => $question['mandatory_response'], 
                                'sequence' => $question['sequence'], 
                                'answer' => $question['answer']
                            );
                        }
                        if ($question['mandatory_response']) {
                            $j++;
                        }
                    }
                }
                
                if (isset($section['sections'])) {
                    $output = self::getArrayQuestions($section['sections'], $output);
                }
            }
        }
        
        if (isset($question['pollapplications'])) {
            $j = $j*count($question['pollapplications']);
        }
        $output['questions_mandatory'] = $j;
        
        return $output;
    }

    /**
     * getArrayQuestionAndAnswer
     *
     */
    public static function getArrayQuestionAndAnswer(DoctrineRegistry $doctrineRegistry, $array, $pollapplicationid)
    {
        $output = '';

        if (!empty($array)) {
          
            foreach ($array as $key_section => $section) {
                
                if (isset($section['title'])) {
                    $output[$key_section]['title'] = $section['title'];
                }
                else {
                    $output[$key_section]['title'] = '';
                }
                
                if (isset($section['description'])) {
                    $output[$key_section]['description'] = $section['description'];
                }
                else {
                    $output[$key_section]['description'] = '';
                }
                
                if (isset($section['questions'])) {
                    foreach ($section['questions'] as $key_question => $question) {
                        
                        if (gettype($pollapplicationid) == 'array') {
                            
                            $pollapplications = $pollapplicationid; 
                          
                            $output[$key_section]['questions'][$key_question] = array(
                                'question' => $question['question'], 
                                'type' => $question['type'], 
                                'alignment' => $question['alignment'], 
                                'options' => $question['options'], 
                                'mandatory_response' => $question['mandatory_response'], 
                                'sequence' => $question['sequence'],
                            );
                            
                            $j = 0;
                            
                            foreach ($pollapplications as $pa) {
                                
                                $ps_question = self::getQuestion($doctrineRegistry, $question['type'], $pa['id'], $question['id']);
                              
                                $output[$key_section]['questions'][$key_question]['pollapplications'][$j] = array(
                                    'id' => $ps_question['id'],
                                    'name' => $pa['name'],
                                    'answer' => $ps_question['answer']
                                );
                                $j++;
                            }
                            
                        }
                        else {
                            
                            $ps_question = self::getQuestion($doctrineRegistry, $question['type'], $pollapplicationid, $question['id']);
                            
                            $output[$key_section]['questions'][$key_question] = array(
                                'id' => $ps_question['id'],
                                'question' => $question['question'], 
                                'type' => $question['type'], 
                                'alignment' => $question['alignment'], 
                                'options' => $question['options'], 
                                'mandatory_response' => $question['mandatory_response'], 
                                'sequence' => $question['sequence'],
                                'answer' => $ps_question['answer']
                            );
                        }
                        
                    }
                }

                if (isset($section['sections'])) {
                 
                    $children_sections = self::getArrayQuestionAndAnswer($doctrineRegistry, $section['sections'], $pollapplicationid);
                    
                    if (!empty($children_sections)) {
                        $output[$key_section]['sections'] = $children_sections;
                    }
                }
            }
        }
        
        return $output;
    }

    /**
     * getQuestionAnswer
     */
    public static function getQuestion(DoctrineRegistry $doctrineRegistry, $type, $pollapplicationid, $pollschedulingquestionid)
    {
        $output = array('id' => '', 'answer' => '');
           
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $result = $repository->createQueryBuilder('paq')
            ->where('paq.pollapplication = :pollapplicationid 
                    AND paq.pollschedulingquestion = :pollschedulingquestionid')
            ->setParameter('pollapplicationid', $pollapplicationid)
            ->setParameter('pollschedulingquestionid', $pollschedulingquestionid)
            ->getQuery()
            ->getResult();
       
       if (!empty($result)) {
          $record = current($result);
          
          $output['id'] = $record->getId();
          
          if (in_array($type, array('simple_text', 'multiple_text'))) {
              $output['answer'] = $record->getAnswer();
          }
          else {
              $output['answer'] = $record->getAnswerOptions();
          }
          
       }
       
       return $output;
    }

    /**
     * getPaginator
     */
    public static function getPaginator(DoctrineRegistry $doctrineRegistry, $pollscheduling, $page)
    {
        $output = array('paginator' => '', 'number_pagers' => '');
        $number_pagers = 0;
        $prev = TRUE;
        $next = TRUE;
        
        // Prev
        
        if ($page == 1 || $pollscheduling->getDivide() == 'none') {
            $prev = FALSE;
        }
        
        // Next
        
        if ($pollscheduling->getDivide() == 'sections_show') {
            
            // Recover count sections
            $array = $pollscheduling->getSerialized();
            if (!empty($array)) {
                $number_pagers = count($array);
                if ($number_pagers <= $page) {
                    $next = FALSE;
                }
            }
            
        }
        elseif ($pollscheduling->getDivide() == 'number_questions') {
            
            // Recover number per page
            $number_per_page = $pollscheduling->getNumberQuestion();
          
            // Recover count questions
            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
            $results = $repository->createQueryBuilder('psq')
                ->select('psq.id')
                ->where('psq.pollscheduling = :pollscheduling')
                ->setParameter('pollscheduling', $pollscheduling->getId())
                ->getQuery()
                ->getResult();
            $count_questions = count($results);
            
            if ($number_per_page == 0) {
                $number_pagers = 0;
            }
            else {
                $number_pagers = round($count_questions / $number_per_page);
                $residue = $count_questions % $number_per_page;
                
                if ($residue > 0) {
                    $number_pagers++;
                }
            }

            if ($number_pagers <= $page) {
                $next = FALSE;
            }
        }
        elseif ($pollscheduling->getDivide() == 'none') {
            $next = FALSE;
        }
        
        if ($prev || $next) {
            $output = Poll::getPaginatorLinks($page, $number_pagers, $prev, $next);
        }
        
        return $output;
    }

    /**
     * getUnResolved
     */
    public static function getUnResolved(DoctrineRegistry $doctrineRegistry, $pollapplicationid)
    {
        // Recover count questions
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $results = $repository->createQueryBuilder('paq')
            ->select('paq.id')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->where('paq.pollapplication = :pollapplication 
                    AND paq.finished = 0 
                    AND psq.mandatory_response = 1')
            ->setParameter('pollapplication', $pollapplicationid)
            ->getQuery()
            ->getResult();
        $questions = count($results);
        
        if ($questions > 0) {
            return TRUE;
        }
        else {
            return FALSE;
        }
        
    }
    
    /**
     * getUnResolvedCurrentPage
     */
    public static function getUnResolvedCurrentPage(DoctrineRegistry $doctrineRegistry, $questions)
    {
        $qString = '';
        
        if (empty($questions)) {
            return FALSE;
        }
        
        foreach ($questions as $key => $value) {
            if (!$qString) {
                $qString .= $key;
            }
            else {
                $qString .= ',' . $key;
            }
        }
        
        // Recover count questions
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $results = $repository->createQueryBuilder('paq')
            ->select('paq.id')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->where('paq.id IN(' . $qString . ')  
                    AND paq.finished = 0 
                    AND psq.mandatory_response = 1')
            ->getQuery()
            ->getResult();
        $questions = count($results);
        
        if ($questions > 0) {
            return TRUE;
        }
        
        return FALSE;
    }

    /**
     * Get List Result Pollapplicationquestion to Pollscheduling
     */
    public static function getResultPollapplicationquestionByPollscheduling(DoctrineRegistry $doctrineRegistry, $wiId = '', $pscId, $evaluator = FALSE)
    {
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $queryBuilder = $repository->createQueryBuilder('paq')
            ->select('pq.id, psq.question, SUM(paq.score) / SUM(psq.ceil_score) * 100 as percentage')
            ->innerJoin('paq.pollapplication', 'pa')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->innerJoin('psq.pollquestion', 'pq')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.deleted = 0 
                    AND pa.finished = 1')
            ->setParameter('pollscheduling', $pscId)
            ->orderBy('psq.id', 'ASC')
            ->groupBy('pq.id');
        
        if ($wiId != '') {
            if (!$evaluator) {
                $queryBuilder
                    ->andWhere('pa.evaluated_id = :evaluated')
                    ->setParameter('evaluated', $wiId);
            }
            else {
                $queryBuilder
                    ->andWhere('pa.evaluator_id = :evaluator')
                    ->setParameter('evaluator', $wiId);
            }
        }
            
        $query = $queryBuilder->getQuery()->getResult();
        
        if (!empty($query)) {
            foreach ($query as $r) {
                $output[$r['id']]['percentage'] = $r['percentage'];
            }
        }
        else {
            $output = $query;
        }
        
        return $output;
    }

    /**
     * Get List Result Pollapplicationquestion to Pollscheduling By Filter
     */
    public static function getResultPollapplicationquestionByFilter(DoctrineRegistry $doctrineRegistry, $pscId, $filterId = '', $typeFilter = '')
    {
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $queryBuilder = $repository->createQueryBuilder('paq')
            ->select('pq.id, psq.question, SUM(paq.score) / SUM(psq.ceil_score) * 100 as percentage')
            ->innerJoin('paq.pollapplication', 'pa')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->innerJoin('psq.pollquestion', 'pq')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.deleted = 0 
                    AND pa.finished = 1')
            ->setParameter('pollscheduling', $pscId)
            ->orderBy('psq.id', 'ASC')
            ->groupBy('pq.id');
        
        if ($filterId != '') {
            switch ($typeFilter) {
                case 'evaluateds':
                    $queryBuilder
                        ->andWhere('pa.evaluated_id = :evaluated')
                        ->setParameter('evaluated', $filterId);
                    break;
                case 'organizationalunits':
                    $queryBuilder
                        ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                        ->setParameter('organizationalunit', $filterId);
                    break;
                case 'charges':
                    $queryBuilder
                        ->andWhere('wi.companycharge = :charge')
                        ->setParameter('charge', $filterId);
                    break;
                case 'headquarters':
                    $queryBuilder
                        ->andWhere('wi.headquarter = :headquarter')
                        ->setParameter('headquarter', $filterId);
                    break;
                case 'faculties':
                    $queryBuilder
                        ->andWhere('wi.companyfaculty = :faculty')
                        ->setParameter('faculty', $filterId);
                    break;
                case 'careers':
                    $queryBuilder
                        ->andWhere('wi.companycareer = :career')
                        ->setParameter('career', $filterId);
                    break;
                case 'grades':
                    $queryBuilder
                        ->andWhere('wi.grade = :grade')
                        ->setParameter('grade', $filterId);
                    break;
            }
        }
                
        $query = $queryBuilder->getQuery()->getResult();
        
        if (!empty($query)) {
            foreach ($query as $r) {
                $output[$r['id']]['percentage'] = $r['percentage'];
            }
        }
        else {
            $output = $query;
        }
        
        return $output;
    }

    /**
     * Get List Result Pollapplicationquestion by Pollschedulings by Filters
     */
    public static function getResultPollapplicationquestionByPollschedulingsByFilters(DoctrineRegistry $doctrineRegistry, $pollschedulings, $filters = FALSE, $pscId = FALSE)
    {
        $output = '';
        $pscIds = '';
        
        $i = 0;
        foreach ($pollschedulings as $psc) {
            if ($pscId != $psc['id']) {
                if ($i == 0) {
                    $pscIds = $psc['id'];
                }
                else {
                    $pscIds .= ',' . $psc['id'];
                }
                $i++;
            }
        }
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $queryBuilder = $repository->createQueryBuilder('paq')
            ->select('pq.id, psq.question, SUM(paq.score) / SUM(psq.ceil_score) * 100 as percentage')
            ->innerJoin('paq.pollapplication', 'pa')
            ->innerJoin('pa.pollscheduling', 'psc')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'c.id = psc.company_id')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->innerJoin('psq.pollquestion', 'pq')
            ->where('pa.pollscheduling IN(' . $pscIds . ') 
                    AND pa.deleted = 0 
                    AND pa.finished = 1')
            ->groupBy('pq.id');
        
        if ($filters) {
            if ($filters['industry'] != '') {
                $queryBuilder
                    ->andWhere('c.industry = :industry')
                    ->setParameter('industry', $filters['industry']);
            }
            if ($filters['nature'] != '') {
                $queryBuilder
                    ->andWhere('c.nature = :nature')
                    ->setParameter('nature', $filters['nature']);
            }
            if ($filters['volumebusiness'] != '') {
                $queryBuilder
                    ->andWhere('c.volume_business = :volumebusiness')
                    ->setParameter('volumebusiness', $filters['volumebusiness']);
            }
            if ($filters['numberpeopleworking'] != '') {
                $queryBuilder
                    ->andWhere('c.number_employees = :numberpeopleworking')
                    ->setParameter('numberpeopleworking', $filters['numberpeopleworking']);
            }
            if ($filters['rankingtop'] != '') {
                
                $totalPas = Pollapplication::getNumberPollapplications($doctrineRegistry, $pscIds);
                $numberPas = round(( $totalPas * $filters['rankingtop']) / 100);
                if ($numberPas == 0) {
                    $numberPas = 1;
                }
                $paIds = Pollapplication::getPollapplicationMaxAverageLimit($doctrineRegistry, $numberPas, $pscIds);
                
                $queryBuilder
                    ->andWhere('pa.id IN(' . $paIds . ')');
            }
            if ($filters['companytype'] == 'school') {
                if ($filters['rangepensionschool'] != '') {
                    $queryBuilder
                        ->andWhere('c.range_pension = :rangepensionschool')
                        ->setParameter('rangepensionschool', $filters['rangepensionschool']);
                }
                if ($filters['typeschool'] != '') {
                    $queryBuilder
                        ->andWhere('c.type = :typeschool')
                        ->setParameter('typeschool', $filters['typeschool']);
                }
                if ($filters['gender'] != '') {
                    $queryBuilder
                        ->andWhere('c.gender = :gender')
                        ->setParameter('gender', $filters['gender']);
                }
                if ($filters['numberpeopleschool'] != '') {
                    $queryBuilder
                        ->andWhere('c.number_students = :numberpeopleschool')
                        ->setParameter('numberpeopleschool', $filters['numberpeopleschool']);
                }
                if ($filters['religiousaffiliationon'] != '' || $filters['religiousaffiliationon'] === 0) {
                    $queryBuilder
                        ->andWhere('c.religious_affiliation_on = :religiousaffiliationon')
                        ->setParameter('religiousaffiliationon', $filters['religiousaffiliationon']);
                }
                if ($filters['religiousaffiliation'] != '') {
                    $queryBuilder
                        ->andWhere('c.religious_affiliation = :religiousaffiliation')
                        ->setParameter('religiousaffiliation', $filters['religiousaffiliation']);
                }
                if ($filters['institutionalaffiliation'] != '') {
                    $queryBuilder
                        ->andWhere('c.institutional_affiliation = :institutionalaffiliation')
                        ->setParameter('institutionalaffiliation', $filters['institutionalaffiliation']);
                }
                if ($filters['educationsystem'] != '') {
                    $queryBuilder
                        ->andWhere('c.education_system = :educationsystem')
                        ->setParameter('educationsystem', $filters['educationsystem']);
                }
                if ($filters['internalschool'] != '' || $filters['internalschool'] === 0) {
                    $queryBuilder
                        ->andWhere('c.internal_school = :internalschool')
                        ->setParameter('internalschool', $filters['internalschool']);
                }
            }
            elseif ($filters['companytype'] == 'university') {
                if ($filters['rangepensionuniversity'] != '') {
                    $queryBuilder
                        ->andWhere('c.range_pension = :rangepensionuniversity')
                        ->setParameter('rangepensionuniversity', $filters['rangepensionuniversity']);
                }
                if ($filters['typeuniversity'] != '') {
                    $queryBuilder
                        ->andWhere('c.type = :typeuniversity')
                        ->setParameter('typeuniversity', $filters['typeuniversity']);
                }
                if ($filters['numberpeopleuniversity'] != '') {
                    $queryBuilder
                        ->andWhere('c.number_students = :numberpeopleuniversity')
                        ->setParameter('numberpeopleuniversity', $filters['numberpeopleuniversity']);
                }
            }
        }
            
        $query = $queryBuilder->getQuery()->getResult();
        
        if (!empty($query)) {
            foreach ($query as $r) {
                $output[$r['id']]['percentage'] = $r['percentage'];
            }
        }
        else {
            $output = $query;
        }
        
        return $output;
    }

    /**
     * Get List Result Pollapplicationquestion Top Average
     */
    public static function getResultPollapplicationquestionTopAverage(DoctrineRegistry $doctrineRegistry, $rankingTop, $pscIds, $array)
    {
        $output = '';
        
        $totalPas = Pollapplication::getNumberPollapplications($doctrineRegistry, $pscIds);
        $numberPas = round(( $totalPas * $rankingTop) / 100);
        if ($numberPas == 0) {
            $numberPas = 1;
        }
        $paIds = Pollapplication::getPollapplicationMaxAverageLimit($doctrineRegistry, $numberPas, $pscIds);
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $queryBuilder = $repository->createQueryBuilder('paq')
            ->select('pq.id, SUM(paq.score) / SUM(psq.ceil_score) * 100 as percentage')
            ->innerJoin('paq.pollapplication', 'pa')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->innerJoin('psq.pollquestion', 'pq')
            ->where('pa.pollscheduling IN(' . $pscIds . ') 
                    AND pa.id IN(' . $paIds . ')
                    AND pa.deleted = 0 
                    AND pa.finished = 1')
            ->groupBy('pq.id');
            
        $query = $queryBuilder->getQuery()->getResult();
        
        if (!empty($query)) {
            foreach ($query as $r) {
                $paqs[$r['id']]['percentage'] = $r['percentage'];
            }
        }
        else {
            $paqs = $query;
        }
        
        $report = Pollscheduling::calculatePercentageSection($array, $paqs);
        
        $output = $report['percentage']['consolidate'];
        
        return $output;
    }

    /**
     * Get count resolved Pollapplicationquestion to Pollscheduling
     */
    public static function getCountResolvedPollapplicationquestion(DoctrineRegistry $doctrineRegistry, $pscId)
    {
        $output = '';
        
        // Check if you have legacy
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $output = $repository->createQueryBuilder('paq')
            ->select('count(paq.id)')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->where('psq.pollscheduling = :pollscheduling 
                  AND paq.finished = 1')
            ->setParameter('pollscheduling', $pscId)
            ->groupBy('paq.id')
            ->getQuery()
            ->getResult();
            
         return $output;
    }

    /**
     * Get List Result Pollapplicationquestion to Disaggregated
     */
    public static function getResultPollapplicationquestionDisaggregated(DoctrineRegistry $doctrineRegistry, $pscId, $paId)
    {
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $query = $repository->createQueryBuilder('paq')
            ->select('pq.id, psq.question, SUM(paq.score) as percentage')
            ->innerJoin('paq.pollapplication', 'pa')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->innerJoin('psq.pollquestion', 'pq')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.deleted = 0 
                    AND pa.finished = 1')
            ->andWhere('pa.id = :pollapplication')
            ->setParameter('pollscheduling', $pscId)
            ->setParameter('pollapplication', $paId)
            ->orderBy('psq.id', 'ASC')
            ->groupBy('pq.id')
            ->getQuery()
            ->getResult();
        
        if (!empty($query)) {
            foreach ($query as $r) {
                $output[$r['id']]['percentage'] = $r['percentage'];
            }
        }
        else {
            $output = $query;
        }
        
        return $output;
    }

    /**
     * Get save Pollapplicationquestion in Pollapplicationreport
     */
    public static function savePollapplicationquestionInPollapplicationreport(DoctrineRegistry $doctrineRegistry, $pscId)
    {
        $em = $doctrineRegistry->getManager();
        
        // Check if you have legacy
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $output = $repository->createQueryBuilder('paq')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->where('psq.pollscheduling = :pollscheduling 
                  AND paq.finished = 1')
            ->setParameter('pollscheduling', $pscId)
            ->groupBy('paq.id')
            ->getQuery()
            ->getResult();
         
         if (!empty($output)) {
             foreach ($output as $o) {
                $em->createQueryBuilder()
                    ->update('FishmanPollBundle:Pollapplicationreport par')
                    ->set('par.answer', ':answer')
                    ->where('par.pollapplicationquestion_id = :pollapplicationquestion')
                    ->setParameter('answer', $o->getAnswer())
                    ->setParameter('pollapplicationquestion', $o->getId())
                    ->getQuery()
                    ->execute();
            }
        }
    }

    /**
     * Get Advanced Poll
     */
    public static function getAdvancedPoll(DoctrineRegistry $doctrineRegistry, $bar, $type, $id)
    {
        $output = '';
        $quantity = 0;
        $resolveds = 0;
        $percentage = 0;
        
        if ($type == 'unique') {
            
            // Return quantity questions
            
            if ($bar == 'section') {
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
                $quantity_query = $repository->createQueryBuilder('paq')
                    ->select('count(paq.id) questions, psc.id')
                    ->innerJoin('paq.pollschedulingquestion', 'psq')
                    ->innerJoin('psq.pollschedulingsection', 'psc')
                    ->where('paq.pollapplication = :pollapplication')
                    ->setParameter('pollapplication', $id)
                    ->groupBy('psq.pollschedulingsection')
                    ->getQuery()
                    ->getResult();
                
                if (!empty($quantity_query)) {
                    $quantity = count($quantity_query);
                }
            }
            elseif ($bar == 'question') {
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
                $quantity_query = $repository->createQueryBuilder('paq')
                    ->select('count(paq.id)')
                    ->where('paq.pollapplication = :pollapplication')
                    ->setParameter('pollapplication', $id)
                    ->groupBy('paq.pollapplication')
                    ->getQuery()
                    ->getResult();
                
                if (!empty($quantity_query)) {
                    $quantity = $quantity_query[0][1];
                }
            }
            
            // Return quantity question resolveds
            
            if ($bar == 'section') {
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
                $resolveds_query = $repository->createQueryBuilder('paq')
                    ->select('count(paq.id) questions, psc.id')
                    ->innerJoin('paq.pollschedulingquestion', 'psq')
                    ->innerJoin('psq.pollschedulingsection', 'psc')
                    ->where("paq.pollapplication = :pollapplication 
                          AND (paq.answer IS NOT NULL OR (paq.answer_options <> 'N;' AND paq.answer_options NOT LIKE 's:0%'))")
                    ->setParameter('pollapplication', $id)
                    ->groupBy('psq.pollschedulingsection')
                    ->getQuery()
                    ->getResult();
                
                $resolveds = 0;
                foreach ($resolveds_query as $r) {
                    foreach ($quantity_query as $q) {
                        if ($q['id'] == $r['id']) {
                            if ($q['questions'] == $r['questions']) {
                                $resolveds++;
                            }
                        }
                    }
                }
            }
            else {
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
                $resolveds_query = $repository->createQueryBuilder('paq')
                    ->select('count(paq.id)')
                    ->where("paq.pollapplication = :pollapplication 
                          AND (paq.answer IS NOT NULL OR (paq.answer_options <> 'N;' AND paq.answer_options NOT LIKE 's:0%'))")
                    ->setParameter('pollapplication', $id)
                    ->groupBy('paq.pollapplication')
                    ->getQuery()
                    ->getResult();
                
                if (!empty($resolveds_query)) {
                    $resolveds = $resolveds_query[0][1];
                }
            }
        }
        elseif ('multiple') {
                
            // Return quantity questions
            
            if ($bar == 'section') {
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
                $quantity_query = $repository->createQueryBuilder('paq')
                    ->select('count(paq.id) questions, psc.id')
                    ->innerJoin('paq.pollapplication', 'pa')
                    ->innerJoin('paq.pollschedulingquestion', 'psq')
                    ->innerJoin('psq.pollschedulingsection', 'psc')
                    ->where('pa.entity_application_id = :entity_application')
                    ->setParameter('entity_application', $id)
                    ->groupBy('psq.pollschedulingsection')
                    ->getQuery()
                    ->getResult();
                
                if (!empty($quantity_query)) {
                    $quantity = count($quantity_query);
                }
            }
            else {
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
                $quantity_query = $repository->createQueryBuilder('paq')
                    ->select('count(paq.id)')
                    ->innerJoin('paq.pollapplication', 'pa')
                    ->where('pa.entity_application_id = :entity_application')
                    ->setParameter('entity_application', $id)
                    ->groupBy('pa.entity_application_id')
                    ->getQuery()
                    ->getResult();
                
                if (!empty($quantity_query)) {
                    $quantity = $quantity_query[0][1];
                }
            }
            
            // Return quantity question resolveds
            
            if ($bar == 'section') {
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
                $resolveds_query = $repository->createQueryBuilder('paq')
                    ->select('count(paq.id) questions, psc.id')
                    ->innerJoin('paq.pollapplication', 'pa')
                    ->innerJoin('paq.pollschedulingquestion', 'psq')
                    ->innerJoin('psq.pollschedulingsection', 'psc')
                    ->where("pa.entity_application_id = :entity_application 
                          AND (paq.answer IS NOT NULL OR (paq.answer_options <> 'N;' AND paq.answer_options NOT LIKE 's:0%'))")
                    ->setParameter('entity_application', $id)
                    ->groupBy('psq.pollschedulingsection')
                    ->getQuery()
                    ->getResult();
                
                $resolveds = 0;
                foreach ($resolveds_query as $r) {
                    foreach ($quantity_query as $q) {
                        if ($q['id'] == $r['id']) {
                            if ($q['questions'] == $r['questions']) {
                                $resolveds++;
                            }
                        }
                    }
                }
            }
            elseif ($bar == 'question') {
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
                $resolveds_query = $repository->createQueryBuilder('paq')
                    ->select('count(paq.id)')
                    ->innerJoin('paq.pollapplication', 'pa')
                    ->where("pa.entity_application_id = :entity_application")
                    ->andWhere("paq.answer IS NOT NULL OR (paq.answer_options <> 'N;' AND paq.answer_options NOT LIKE 's:0%')")
                    ->setParameter('entity_application', $id)
                    ->groupBy('pa.entity_application_id')
                    ->getQuery()
                    ->getResult();
                
                if (!empty($resolveds_query)) {
                    $resolveds = $resolveds_query[0][1];
                }
            }
        }
        
        $output['quantity'] = $quantity;
        $output['resolveds'] = $resolveds;
        
        if ($resolveds > 0) {
            $output['percentage'] = floor(($resolveds*100)/$quantity);
        }
        else {
            $output['percentage'] = 0;
        }
        
        return $output;
    }
    
}
