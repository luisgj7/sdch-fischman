<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\PollBundle\Entity\Pollpersonimport
 */
class Pollpersonimport
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $rol
     */
    private $rol;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $names
     */
    private $names;

    /**
     * @var string $surname
     */
    private $surname;

    /**
     * @var string $lastname
     */
    private $lastname;

    /**
     * @var string $sex
     */
    private $sex;

    /**
     * @var string $identity
     */
    private $identity;

    /**
     * @var string $numberidentity
     */
    private $numberidentity;

    /**
     * @var string $email
     */
    private $email;

    /**
     * @var \DateTime $birthday
     */
    private $birthday;

    /**
     * @var integer $company_id
     */
    private $company_id;

    /**
     * @var integer $headquarter_id
     */
    private $headquarter_id;

    /**
     * @var string $email_information
     */
    private $email_information;

    /**
     * @var integer $organizationalunit_id
     */
    private $organizationalunit_id;

    /**
     * @var integer $charge_id
     */
    private $charge_id;

    /**
     * @var integer $faculty_id
     */
    private $faculty_id;

    /**
     * @var integer $career_id
     */
    private $career_id;

    /**
     * @var string $academic_year
     */
    private $academic_year;

    /**
     * @var string $grade
     */
    private $grade;

    /**
     * @var string $study_year
     */
    private $study_year;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rol
     *
     * @param string $rol
     * @return Pollpersonimport
     */
    public function setRol($rol)
    {
        $this->rol = $rol;
    
        return $this;
    }

    /**
     * Get rol
     *
     * @return string 
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Pollpersonimport
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Pollpersonimport
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set names
     *
     * @param string $names
     * @return Pollpersonimport
     */
    public function setNames($names)
    {
        $this->names = $names;
    
        return $this;
    }

    /**
     * Get names
     *
     * @return string 
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Pollpersonimport
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Pollpersonimport
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set sex
     *
     * @param string $sex
     * @return Pollpersonimport
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    
        return $this;
    }

    /**
     * Get sex
     *
     * @return string 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set identity
     *
     * @param string $identity
     * @return Pollpersonimport
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;
    
        return $this;
    }

    /**
     * Get identity
     *
     * @return string 
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set numberidentity
     *
     * @param string $numberidentity
     * @return Pollpersonimport
     */
    public function setNumberidentity($numberidentity)
    {
        $this->numberidentity = $numberidentity;
    
        return $this;
    }

    /**
     * Get numberidentity
     *
     * @return string 
     */
    public function getNumberidentity()
    {
        return $this->numberidentity;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Pollpersonimport
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Pollpersonimport
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set company_id
     *
     * @param integer $companyId
     * @return Pollpersonimport
     */
    public function setCompanyId($companyId)
    {
        $this->company_id = $companyId;
    
        return $this;
    }

    /**
     * Get company_id
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * Set headquarter_id
     *
     * @param integer $headquarterId
     * @return Pollpersonimport
     */
    public function setHeadquarterId($headquarterId)
    {
        $this->headquarter_id = $headquarterId;
    
        return $this;
    }

    /**
     * Get headquarter_id
     *
     * @return integer 
     */
    public function getHeadquarterId()
    {
        return $this->headquarter_id;
    }

    /**
     * Set email_information
     *
     * @param string $emailInformation
     * @return Pollpersonimport
     */
    public function setEmailInformation($emailInformation)
    {
        $this->email_information = $emailInformation;
    
        return $this;
    }

    /**
     * Get email_information
     *
     * @return string 
     */
    public function getEmailInformation()
    {
        return $this->email_information;
    }

    /**
     * Set organizationalunit_id
     *
     * @param integer $organizationalunitId
     * @return Pollpersonimport
     */
    public function setOrganizationalunitId($organizationalunitId)
    {
        $this->organizationalunit_id = $organizationalunitId;
    
        return $this;
    }

    /**
     * Get organizationalunit_id
     *
     * @return integer 
     */
    public function getOrganizationalunitId()
    {
        return $this->organizationalunit_id;
    }

    /**
     * Set charge_id
     *
     * @param integer $chargeId
     * @return Pollpersonimport
     */
    public function setChargeId($chargeId)
    {
        $this->charge_id = $chargeId;
    
        return $this;
    }

    /**
     * Get charge_id
     *
     * @return integer 
     */
    public function getChargeId()
    {
        return $this->charge_id;
    }

    /**
     * Set faculty_id
     *
     * @param integer $facultyId
     * @return Pollpersonimport
     */
    public function setFacultyId($facultyId)
    {
        $this->faculty_id = $facultyId;
    
        return $this;
    }

    /**
     * Get faculty_id
     *
     * @return integer 
     */
    public function getFacultyId()
    {
        return $this->faculty_id;
    }

    /**
     * Set career_id
     *
     * @param integer $careerId
     * @return Pollpersonimport
     */
    public function setCareerId($careerId)
    {
        $this->career_id = $careerId;
    
        return $this;
    }

    /**
     * Get career_id
     *
     * @return integer 
     */
    public function getCareerId()
    {
        return $this->career_id;
    }

    /**
     * Set academic_year
     *
     * @param string $academicYear
     * @return Pollpersonimport
     */
    public function setAcademicYear($academicYear)
    {
        $this->academic_year = $academicYear;
    
        return $this;
    }

    /**
     * Get academic_year
     *
     * @return string 
     */
    public function getAcademicYear()
    {
        return $this->academic_year;
    }

    /**
     * Set grade
     *
     * @param string $grade
     * @return Pollpersonimport
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    
        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set study_year
     *
     * @param string $studyYear
     * @return Pollpersonimport
     */
    public function setStudyYear($studyYear)
    {
        $this->study_year = $studyYear;
    
        return $this;
    }

    /**
     * Get study_year
     *
     * @return string 
     */
    public function getStudyYear()
    {
        return $this->study_year;
    }

    /**
     * Clone Register 
     */
    public static function addRegister(DoctrineRegistry $doctrineRegistry, $person)
    {
        $em = $doctrineRegistry->getManager();
            
        if (!empty($person)) {
            
            $ppi = new Pollpersonimport();
            $ppi->setType(trim($person['wi_type']));
            if (in_array($person['rol'], array('evaluator', 'evaluated'))) {
                $ppi->setRol('integrant');
            }
            else {
                $ppi->setRol(trim($person['rol']));
            }
            $ppi->setCode(trim($person['code']));
            $ppi->setNames(trim($person['names']));
            $ppi->setSurname(trim($person['surname']));
            if ($person['lastname']) {
                $ppi->setLastname(trim($person['lastname']));
            }
            else {
                $ppi->setLastname('Extranjero');
            }
            $ppi->setSex(trim($person['sex']));
            $ppi->setIdentity(trim($person['identity']));
            $ppi->setNumberidentity(trim($person['numberidentity']));
            $ppi->setEmail(trim($person['email']));
            if ($person['birthday']) {
                $ppi->setBirthday($person['birthday']);
            }
            else {
                $ppi->setBirthday(NULL);
            }
            $ppi->setCompanyId($person['company_id']);
            $ppi->setHeadquarterId($person['headquarter_id']);
            $ppi->setEmailInformation($person['email_information']);
            if ($person['wi_type'] == 'workinginformation') {
                $ppi->setOrganizationalunitId($person['ou_id']);
                $ppi->setChargeId($person['charge_id']);
            }
            elseif ($person['wi_type'] == 'schoolinformation') {
                $ppi->setGrade($person['grade_code']);
                $ppi->setStudyYear($person['study_year']);
            }
            elseif ($person['wi_type'] == 'universityinformation') {
                $ppi->setFacultyId($person['faculty_id']);
                $ppi->setCareerId($person['career_id']);
                $ppi->setAcademicYear($person['academic_year']);
            }
            
            $em->persist($ppi);
            $em->flush();
            
            return $ppi;
            
        }
        
    }
}
