<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Doctrine\Common\Collections\ArrayCollection;

use Fishman\NotificationBundle\Entity\Notificationexecution;

/**
 * Fishman\PollBundle\Entity\Pollschedulingpeople
 */
class Pollschedulingpeople
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $profile
     */
    private $profile;

    /**
     * @var string $personaltoken
     */
    private $personaltoken;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    protected $workinginformation;

    protected $pollscheduling;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profile
     *
     * @param string $profile
     * @return Pollschedulingpeople
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    
        return $this;
    }

    /**
     * Get profile
     *
     * @return string 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set personaltoken
     *
     * @param string $personaltoken
     * @return Pollschedulingpeople
     */
    public function setPersonaltoken($personaltoken)
    {
        $this->personaltoken = $personaltoken;
    
        return $this;
    }

    /**
     * Get personaltoken
     *
     * @return string 
     */
    public function getPersonaltoken()
    {
        return $this->personaltoken;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Pollschedulingpeople
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pollschedulingpeople
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Pollschedulingpeople
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Pollschedulingpeople
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Pollschedulingpeople
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Pollapplication
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Set workinginformation
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformation
     * @return Pollschedulingpeople
     */
    public function setWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformation = null)
    {
        $this->workinginformation = $workinginformation;
    
        return $this;
    }

    /**
     * Get workinginformation
     *
     * @return Fishman\AuthBundle\Entity\Workinginformation 
     */
    public function getWorkinginformation()
    {
        return $this->workinginformation;
    }

    /**
     * Set pollscheduling
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollscheduling
     * @return Pollschedulingpeople
     */
    public function setPollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollscheduling = null)
    {
        $this->pollscheduling = $pollscheduling;
    
        return $this;
    }

    /**
     * Get pollscheduling
     *
     * @return Fishman\PollBundle\Entity\Pollscheduling 
     */
    public function getPollscheduling()
    {
        return $this->pollscheduling;
    }

    /**
     * Get Current Pollschedulingpeople
     */
    public static function getCurrentPollschedulingpeople(DoctrineRegistry $doctrineRegistry, $psId, $wsId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingpeople');
        $query = $repository->createQueryBuilder('psp')
            ->where('psp.workinginformation = :workinginformation
                    AND psp.pollscheduling = :pollscheduling')
            ->setParameter('workinginformation', $wsId)
            ->setParameter('pollscheduling', $psId)
            ->getQuery()
            ->getResult();
        $output = current($query);
            
        return $output;
    }

    /**
     * Get list profile options
     * 
     */
    public static function getListProfileOptions() 
    {
        $output = array(
            'collaborator' => 'Colaborador',
            'integrant' => 'Participante',
            'boss' => 'Jefe',
            /*'responsible' => 'Responsable',*/ 
            'company' => 'Empresa'
        );
        
        return $output;
    }
    
    /**
     * Get Pollschedulingpeoples List
     */
    public static function getPollschedulingPeoplesList(DoctrineRegistry $doctrineRegistry, $companyid, $pollschedulingid, $evaluateds = NULL)
    {
        $repository = $doctrineRegistry->getManager()->getRepository('FishmanPollBundle:Pollschedulingpeople');
        $workinginformations = $repository->createQueryBuilder('psp')
            ->select('wi.id, wi.type, wi.code, u.names, u.surname, u.lastname')
            ->where('wi.company = :company
                    AND psp.pollscheduling = :pollscheduling')
            ->innerJoin('psp.workinginformation', 'wi')
            ->innerJoin('wi.user', 'u')
            ->setParameter('company', $companyid)
            ->setParameter('pollscheduling', $pollschedulingid)
            ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC');
        
        if ($evaluateds != NULL ) {
            $workinginformations
                ->andWhere('psp.workinginformation NOT IN (:ids) ')
                ->setParameter('ids', $evaluateds);
        }
        
        $currentQuery = $workinginformations
            ->getQuery()
            ->getResult();
        
        $output = $currentQuery;
        return $output;
    }
    
    /**
     * Add Pollschedulingpeople
     * 
     */
    public static function addRegister(DoctrineRegistry $doctrine, $profile, $workinginformation, $pollscheduling, $userBy)
    {   
        $em = $doctrine->getManager();
        
        $psp_entity = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'workinginformation' => $workinginformation->getId()
        ));
        
        if (!$psp_entity) {
            $psp_entity = new Pollschedulingpeople();
            $token = Pollapplication::generateCode();
            $psp_entity->setPersonaltoken($token);
            $psp_entity->setPollscheduling($pollscheduling);
            $psp_entity->setWorkinginformation($workinginformation);
            $psp_entity->setCreated(new \DateTime());
            $psp_entity->setCreatedBy($userBy->getId());
        }

        $psp_entity->setProfile($profile);
        $psp_entity->setStatus(TRUE);
        $psp_entity->setChanged(new \DateTime());
        $psp_entity->setModifiedBy($userBy->getId());
        
        $em->persist($psp_entity);
        $em->flush();
        
        // Recuperamos roles del usuario
        $urol = $psp_entity->getWorkinginformation()->getUser()->getRoles();
                
        // Verify notifications for profile
        $repository = $doctrine->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $queryNS = $repository->createQueryBuilder('ns')
            ->where('ns.entity_type = :entity_type 
                  AND ns.entity_id = :entity_id 
                  AND ns.asigned LIKE :profile')
            ->setParameter('entity_type', 'pollscheduling')
            ->setParameter('entity_id', $pollscheduling->getId())
            ->setParameter('profile', '%' . $profile . '%')
            ->getQuery()
            ->getResult();
        
        if (!empty($queryNS)) {
            
            foreach ($queryNS as $ns) {
            
                foreach ($ns->getAsigned() as $asigned) {
                    
                    $nsrol = '';
                    
                    if ($asigned == 'responsible') $nsrol = 'ROLE_TEACHER';
                    
                    if ($profile == $asigned || in_array($nsrol, $urol)) {
                        
                        $repository = $em->getRepository('FishmanNotificationBundle:Notificationexecution');
                        $nxTemp = $repository->createQueryBuilder('nx')
                            ->select('nx.entity_id, nx.profile')
                            ->where('nx.notificationscheduling = :nsid 
                                    AND nx.workinginformation_id = :wiid')
                            ->setParameter('nsid', $ns->getId())
                            ->setParameter('wiid', $psp_entity->getWorkinginformation()->getId())
                            ->getQuery()
                            ->getResult();
                        
                        $nx_entity = current($nxTemp);
                        
                        if (!$nx_entity) {
                            
                            switch ($ns->getNotificationType()) {
                                case 'message':
                                    
                                    $nx = Notificationexecution::generateNotificationexecution($ns, $workinginformation->getId(), 
                                              'pollschedulingpeople', $psp_entity->getId(), $ns->getInitdate(), 
                                              $ns->getEnddate(), NULL, 1, $asigned, $doctrine);
                                    $em->persist($nx);
                                    
                                    break;
                                    
                                case 'poll':
                                    
                                    // Recover Pollapplications
                                    $pollapplications = $em->getRepository('FishmanPollBundle:Pollapplication')->findByPollscheduling($pollscheduling->getId());
                                    
                                    if (!empty($pollapplications)) {
                                        foreach ($pollapplications as $pa_entity) {
                                            $nx = Notificationexecution::generateNotificationexecution($ns, $workinginformation->getId(),
                                                      'pollapplication', $pa_entity->getId(), $ns->getInitdate(),
                                                      $ns->getEnddate(), NULL, 1, $asigned, $doctrine);
                                            $em->persist($nx);
                                        }
                                    }
                                    
                                    break;
                            }
                        }
                    }
                }
                
            }
        }
        
        $em->flush();
        
    }
    
    /**
     * Generate Notification executions 
     */
    public static function generateNotificationexecutions(DoctrineRegistry $doctrineRegistry, $entityPSP, $nsId = '')
    {
        $em = $doctrineRegistry->getManager();
        
        $nextNotification = '';
        $nsRepository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $queryBuilder = $nsRepository->createQueryBuilder('ns')
             ->where("ns.entity_type = :entity_type")
             ->setParameter('entity_type', 'pollscheduling');
        
        if ($nsId != '') {
            $queryBuilder
                ->andWhere('ns.id = :notificationscheduling')
                ->setParameter('notificationscheduling', $nsId);
        }
        
        $nss = $queryBuilder->getQuery()->getResult();
        
        foreach ($nss as $ns) {
            
            $predecessor = $ns->getPredecessor();
            $nextNotification = NULL;
            
            if (!empty($predecessor) && $predecessor == '-1') {
                
                $initdate = FALSE;
                
                // get init date pollscheduling
                if (!$ns->getNotificationTypeStatus()) {
                    $entityPS = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entityPSP->getPollcheduling()->getId());
                    $initdate = strtotime($entityPS->getInitdate()->format('Y/m/d'));
                }
                
                if ($initdate) {
                    // case since
                    if ($ns->getSince() > 0 ) {
                        $initdate = $initdate + ($ns->getSince()*24*60*60);
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                    else {
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                }
                
            }
            
            $profiles = $ns->getAsigned();
           
            if (!empty($profiles)) {
                
                foreach ($profiles as $asigned) {
                    
                    Notificationexecution::generateNotificationexecutions(
                        $doctrineRegistry, 
                        $ns, 
                        $nextNotification, 
                        $asigned, 
                        'pollschedulingpeople', 
                        $entityPSP->getId(), 
                        $entityPSP->getWorkinginformation()->getId(),
                        'POLLscheduling', 
                        $entityPSP->getPollscheduling()->getId(), 
                        $entityPSP->getWorkinginformation()->getCompany()->getId()
                    );
                    
                }
            }
        }

    }

}