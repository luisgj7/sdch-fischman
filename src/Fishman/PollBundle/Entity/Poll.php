<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Doctrine\Common\Collections\ArrayCollection;

use Fishman\AuthBundle\Entity\Permission;
use Fishman\PollBundle\Entity\Pollsection;
use Fishman\PollBundle\Entity\Pollschedulingsection;

/**
 * Fishman\PollBundle\Entity\Poll
 */
class Poll 
{

    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $version
     */
    private $version;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var smallint $sequence
     */
    private $level;
    
    /**
     * @var string $alignment
     */
    private $alignment;

    /**
     * @var string $divide
     */
    private $divide;

    /**
     * @var integer $number_question
     */
    private $number_question;

    /**
     * @var integer $duration
     */
    private $duration;

    /**
     * @var string $period
     */
    private $period;

    /**
     * @var boolean $sequence_question
     */
    private $sequence_question;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var boolean $current_version
     */
    private $current_version;

    /**
     * @var integer $original_id
     */
    private $original_id;

    /**
     * @var boolean $modified_on
     */
    private $modified_on;
    
    /**
     * @var string $serialized
     */
    private $serialized;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;


    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Category", inversedBy="polls")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollsection", mappedBy="poll")
     */
    protected $pollsections;  

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollquestion", mappedBy="poll")
     */
    protected $pollquestions;  

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollscheduling", mappedBy="poll")
     */
    protected $pollschedulings;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Entitypoll", mappedBy="poll")
     */
    protected $entitypolls;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Reset id 
     */
    public function resetId()
    {
        unset($this->id);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Poll
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set version
     *
     * @param string $version
     * @return Poll
     */
    public function setVersion($version)
    {
        $this->version = $version;
    
        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Poll
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type 
     *
     * @param string $type
     * @return Poll
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return smallint 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set level
     *
     * @param string $level
     * @return Poll
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set alignment
     *
     * @param string $alignment
     * @return Poll
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
    
        return $this;
    }

    /**
     * Get alignment
     *
     * @return string 
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * Set sequence_question
     *
     * @param string $sequence_question
     * @return Poll
     */
    public function setSequenceQuestion($sequenceQuestion)
    {
        $this->sequence_question = $sequenceQuestion;
    
        return $this;
    }

    /**
     * Get sequence_question
     *
     * @return string 
     */
    public function getSequenceQuestion()
    {
        return $this->sequence_question;
    }

    /**
     * Set divide
     *
     * @param string $divide
     * @return Poll
     */
    public function setDivide($divide)
    {
        $this->divide = $divide;
    
        return $this;
    }

    /**
     * Get divide
     *
     * @return string 
     */
    public function getDivide()
    {
        return $this->divide;
    }

    /**
     * Set number_question
     *
     * @param integer $number_question
     * @return Poll
     */
    public function setNumberQuestion($numberQuestion)
    {
        $this->number_question = $numberQuestion;
    
        return $this;
    }

    /**
     * Get number_question
     *
     * @return integer 
     */
    public function getNumberQuestion()
    {
        return $this->number_question;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Poll
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set period
     *
     * @param string $period
     * @return Poll
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return string 
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Poll
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set current_version
     *
     * @param boolean $currentVersion
     * @return Poll
     */
    public function setCurrentVersion($currentVersion)
    {
        $this->current_version = $currentVersion;
    
        return $this;
    }

    /**
     * Get current_version
     *
     * @return boolean 
     */
    public function getCurrentVersion()
    {
        return $this->current_version;
    }

    /**
     * Set original_id
     *
     * @param integer $originalId
     * @return Poll
     */
    public function setOriginalId($originalId)
    {
        $this->original_id = $originalId;
    
        return $this;
    }

    /**
     * Get original_id
     *
     * @return integer 
     */
    public function getOriginalId()
    {
        return $this->original_id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Poll
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Poll
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Poll
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Poll
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set category
     *
     * @param Fishman\EntityBundle\Entity\Category $category
     * @return Poll
     */
    public function setCategory(\Fishman\EntityBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return Fishman\EntityBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollsections = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add pollsections
     *
     * @param Fishman\PollBundle\Entity\Pollsection $pollsections
     * @return Poll
     */
    public function addPollsection(\Fishman\PollBundle\Entity\Pollsection $pollsections)
    {
        $this->pollsections[] = $pollsections;
    
        return $this;
    }

    /**
     * Remove pollsections
     *
     * @param Fishman\PollBundle\Entity\Pollsection $pollsections
     */
    public function removePollsection(\Fishman\PollBundle\Entity\Pollsection $pollsections)
    {
        $this->pollsections->removeElement($pollsections);
    }

    /**
     * Get pollsections
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollsections()
    {
        return $this->pollsections;
    }
   
    public function __toString()
    {
         return $this->title;
    }        

    /**
     * Add pollquestions
     *
     * @param Fishman\PollBundle\Entity\Pollquestion $pollquestions
     * @return Poll
     */
    public function addPollquestion(\Fishman\PollBundle\Entity\Pollquestion $pollquestions)
    {
        $this->pollquestions[] = $pollquestions;
    
        return $this;
    }

    /**
     * Remove pollquestions
     *
     * @param Fishman\PollBundle\Entity\Pollquestion $pollquestions
     */
    public function removePollquestion(\Fishman\PollBundle\Entity\Pollquestion $pollquestions)
    {
        $this->pollquestions->removeElement($pollquestions);
    }

    /**
     * Get pollquestions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollquestions()
    {
        return $this->pollquestions;
    }

    /**
     * Add pollschedulings
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollschedulings
     * @return Poll
     */
    public function addPollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollschedulings)
    {
        $this->pollschedulings[] = $pollschedulings;
    
        return $this;
    }

    /**
     * Remove pollschedulings
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollschedulings
     */
    public function removePollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollschedulings)
    {
        $this->pollschedulings->removeElement($pollschedulings);
    }

    /**
     * Get pollschedulings
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulings()
    {
        return $this->pollschedulings;
    }

    /**
     * Add entitypolls
     *
     * @param Fishman\PollBundle\Entity\Entitypoll $entitypolls
     * @return Poll
     */
    public function addEntitypoll(\Fishman\PollBundle\Entity\Entitypoll $entitypolls)
    {
        $this->entitypolls[] = $entitypolls;
    
        return $this;
    }

    /**
     * Remove entitypolls
     *
     * @param Fishman\PollBundle\Entity\Entitypoll $entitypolls
     */
    public function removeEntitypoll(\Fishman\PollBundle\Entity\Entitypoll $entitypolls)
    {
        $this->entitypolls->removeElement($entitypolls);
    }

    /**
     * Get entitypolls
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEntitypolls()
    {
        return $this->entitypolls;
    }

    /**
     * Get list type options
     * 
     */
    public static function getListTypeOptions() 
    {
        $output = array(
            'evaluation_collaborators' => 'Evaluación a Colaboradores',
            'evaluation_boss' => 'Evaluación a Jefes',
            'evaluation_360' => 'Evaluación 360',
            'self_evaluation' => 'Autoevaluación'
        );
        
        return $output;
    }

    /**
     * Get list benchmarking options
     * 
     */
    public static function getListPollOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanPollBundle:Poll');
        $queryBuilder = $repository->createQueryBuilder('p')
            ->select('p.id, p.title')
            ->where('p.status = 1')
            ->orderBy('p.title', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach($result as $r) {
                $output[$r['id']] = $r['title'];
            }
        }
        
        return $output;
    }

    /**
     * Get List Polls by Company
     */
    public static function getListBenchmarkingPollsByCompany(DoctrineRegistry $doctrineRegistry, $companyId = '', $userId, $permission)
    {   
        // Recover Pollscheduling where permissions assigned to the user for the company
        $pscIds = Permission::getPollschedulingPermissionAssignedsOfPermissionId($doctrineRegistry, $userId, $permission);
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Poll');
        $queryBuilder = $repository->createQueryBuilder('p')
            ->select('p.id, p.title')
            ->innerJoin('FishmanPollBundle:Pollscheduling', 'psc', 'WITH', 'p.id = psc.poll')
            ->where('psc.status = 1 
                    AND psc.deleted = 0 
                    AND psc.company_id <> 2')
            ->orderBy('p.title', 'ASC')
            ->groupBy('p.id');
        /*
        $queryBuilder
            ->andWhere('psc.benchmarking <> :benchmarking')
            ->setParameter('benchmarking', ''); 
        */
        if ($pscIds != '') {
            $queryBuilder
                ->andWhere('psc.id NOT IN(' . $pscIds . ')');
        }
        if ($companyId != '') {
            $queryBuilder
                ->andWhere('psc.company_id = :company')
                ->setParameter('company', $companyId);
        }

        $output = $queryBuilder
            ->getQuery()
            ->getResult();
            
        return $output;
    }

    /**
     * Get Widget Polls By Company
     */
    public static function getWidgetPollsByCompany(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $selectedId = FALSE, $userId, $permission)
    {
        $output = '<select name="permission_polls" id="permission_polls" class="permission_pollselect">';
        $output .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        
        if ($selectedId) {
            
            $polls = self::getListBenchmarkingPollsByCompany($doctrineRegistry, $companyId, $userId, $permission);
            
            if ($polls) {
                foreach ($polls as $poll) {
                    $selected = '';
                    if ($selectedId == $poll['id']){
                        $selected = ' selected ';
                    }
                    $output .= '<option value="' . $poll['id'] . '"'. $selected .'>' .  $poll['title'] . '</option>';
                }
            }
            
        }
        
        $output .= '</select>';
        
        return $output;
    }

    /**
     * Set modified_on
     *
     * @param boolean $modifiedOn
     * @return Poll
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modified_on = $modifiedOn;
    
        return $this;
    }

    /**
     * Get modified_on
     *
     * @return boolean 
     */
    public function getModifiedOn()
    {
        return $this->modified_on;
    }

    /**
     * Set serialized
     *
     * @param array $serialized
     * @return Poll
     */
    public function setSerialized($serialized)
    {
        $this->serialized = $serialized;
    
        return $this;
    }

    /**
     * Get serialized
     *
     * @return array 
     */
    public function getSerialized()
    {
        return $this->serialized;
    }

    /**
     * Serializaed Poll
     */
    public static function serializedPoll(DoctrineRegistry $doctrineRegistry, $entity)
    {
        $em = $doctrineRegistry->getManager();
        
        // Created array Sections and Questions
        $pollArray['sections'] = Pollsection::arraySectionsAndQuestions($doctrineRegistry, $entity->getId());
        
        if (!empty($pollArray['sections'])) {
            if ($entity->getLevel() != '10') {
                // Group array Sections
                $pollArray['sections'] = Pollsection::arrayGroupQuestions($doctrineRegistry, 0, $entity->getLevel(), $pollArray['sections']);
            }
            
            if (!$entity->getSequenceQuestion()) {
                // Random array Questions
                $pollArray = Pollsection::arrayRandomQuestions($doctrineRegistry, $pollArray);
            }
        }
        
        // Add Serialized
        $entity->setSerialized($pollArray['sections']);
        $entity->setModifiedOn(FALSE);
        $em->persist($entity);
        $em->flush();
    }

    /**
     * getPaginator
     */
    public static function getPaginator(DoctrineRegistry $doctrineRegistry, $poll, $page)
    {
        $output = array('paginator' => '', 'number_pagers' => '');
        $number_pagers = 0;
        $first = TRUE;
        $prev = TRUE;
        $next = TRUE;
        $last = TRUE;
        
        // Prev
        
        if ($page == 1 || $poll->getDivide() == 'none') {
            $first = FALSE;
            $prev = FALSE;
        }
        
        // Next
        
        if ($poll->getDivide() == 'sections_show') {
            
            // Recover count sections
            $array = $poll->getSerialized();
            if (!empty($array)) {
                $number_pagers = count($array);
                if ($number_pagers <= $page) {
                    $next = FALSE;
                    $last = FALSE;
                }
            }
            
        }
        elseif ($poll->getDivide() == 'number_questions') {
            
            // Recover number per page
            $number_per_page = $poll->getNumberQuestion();
          
            // Recover count questions
            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollquestion');
            $results = $repository->createQueryBuilder('pq')
                ->select('pq.id')
                ->where('pq.poll = :poll')
                ->setParameter('poll', $poll->getId())
                ->getQuery()
                ->getResult();
            $count_questions = count($results);
            
            if ($number_per_page == 0) {
                $number_pagers = 0;
            }
            else {
                $number_pagers = round($count_questions / $number_per_page);
                $residue = $count_questions % $number_per_page;
                
                if ($residue < ($number_per_page / 2)) {
                    $number_pagers++;
                }
            }

            if ($number_pagers <= $page) {
                $next = FALSE;
            }
        }
        elseif ($poll->getDivide() == 'none') {
            $next = FALSE;
        }
        
        if ($prev || $next) {
            $output = Poll::getPaginatorLinks($page, $number_pagers, $prev, $next);
        }
        
        return $output;
    }

    /**
     * getPagers
     */
    public static function getPaginatorLinks($page, $numberPagers, $prev, $next)
    {
        $output = array('paginator', 'number_pagers');
        $paginator = '';
        $number_paginator = '';
        $pagerNumbers = '';
        $info = 'Página ' . $page . ' de ' . $numberPagers;
        
        $paginator .= '<div class="grid12">';
        $paginator .= '<div class="widget clearfix">';
        $paginator .= '<div class="grid3 paginator-info">';
        $paginator .= $info;
        $paginator .= '</div>';
        $paginator .= '<div class="grid9">';
        $paginator .= '<ul class="paginator clearfix">';
        
        if ($page > 3) {
            $pagerNumbers .= '<li>...</li>';
        }

        if ($page-2 < 1 || $page-1 < 1) {
            $arrayPagers = array(0, 1, 2, 3, 4, 5);
        }
        else if ($page+2 > $numberPagers || $page+1 > $numberPagers) {
            $arrayPagers = array($page-4, $page-3, $page-2, $page-1, $page);
        }
        else {
            $arrayPagers = array($page-2, $page-1, $page, $page+1, $page+2);
        }
        
        for ($i = 1; $i <= $numberPagers; $i++) {
            if (in_array($i, $arrayPagers)) {
                if ($i == $page) {
                    $pagerNumbers .= '<li><button type="submit" name="page" value="' . $i . '" class="sideB bGreyish mt10">' . $i . '</button></li>';
                }
                else {
                    $pagerNumbers .= '<li><button type="submit" name="page" value="' . $i . '" class="sideB bRed mt10">' . $i . '</button></li>';
                }
            }
        }
        
        if ($page < $numberPagers && $numberPagers > 5) {
            $pagerNumbers .= '<li>...</li>';
        }
        
        if ($prev && $next) {
            $paginator .= '<li><button type="submit" name="first" value="1" class="sideB bRed mt10">&lt;&lt;</button></li>';
            $paginator .= '<li><button type="submit" name="prev" value="1" class="sideB bRed mt10">&lt;</button></li>';
            $paginator .= $pagerNumbers;
            $paginator .= '<li><button type="submit" name="next" value="1" class="sideB bRed mt10">&gt;</button></li>';
            $paginator .= '<li><button type="submit" name="last" value="1" class="sideB bRed mt10">&gt;&gt;</button></li>';
        }
        elseif (!$prev && $next) {
            $paginator .= $pagerNumbers;
            $paginator .= '<li><button type="submit" name="next" value="1" class="sideB bRed mt10">&gt;</button></li>';
            $paginator .= '<li><button type="submit" name="last" value="1" class="sideB bRed mt10">&gt;&gt;</button></li>';
        }
        elseif ($prev && !$next) {
            $paginator .= '<li><button type="submit" name="first" value="1" class="sideB bRed mt10">&lt;&lt;</button></li>';
            $paginator .= '<li><button type="submit" name="prev" value="1" class="sideB bRed mt10">&lt;</button></li>';
            $paginator .= $pagerNumbers;
        }
    
        $paginator .= '</ul>';
        $paginator .= '</div>';
        $paginator .= '</div>';
        $paginator .= '</div>';
        
        $output['paginator'] = $paginator;
        $output['number_pagers'] = $numberPagers;
        
        return $output;
    }

    /**
     * getArraySectionShow
     */
    public static function getArraySectionShow(DoctrineRegistry $doctrineRegistry, $array, $pollid, $page)
    {
        $output = '';
        $page--;
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollsection');
        $result = $repository->createQueryBuilder('ps')
            ->select('ps.id')
            ->where('ps.poll = :pollid 
                    AND ps.parent_id = :parent_id')
            ->setParameter('pollid', $pollid)
            ->setParameter('parent_id', '-1')
            ->orderBy('ps.sequence', 'ASC')
            ->setFirstResult($page)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
            
        $record = current($result);
        
        if (!empty($record)) {
            $output['sections'][$record['id']] = $array['sections'][$record['id']];
        }
        
        return $output;
    }

    /**
     * getArrayNumberQuestions
     */
    public static function getArrayNumberQuestions(DoctrineRegistry $doctrineRegistry, $array, $pollid, $page, $number)
    {
        $output = '';
        $page--;
        $init = $page * $number;
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollquestion');
        $result = $repository->createQueryBuilder('pq')
            ->select('pq.id, ps.l1')
            ->innerJoin('pq.pollsection', 'ps')
            ->where('pq.poll = :pollid')
            ->setParameter('pollid', $pollid)
            ->setFirstResult($init)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
        
        $record = current($result);

        if (!empty($record)) {
            $output = Pollschedulingsection::getArrayNumberQuestionsCurrent($array, $record['l1'], $init, $number);
        }

        return $output;
    }
    
}