<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Fishman\PollBundle\Entity\Pollschedulingsection;

/**
 * Fishman\PollBundle\Entity\Pollschedulingquestion
 */
class Pollschedulingquestion
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $question
     */
    private $question;
    
    /**
     * @var string $type
     */
    private $type;
    
    /**
     * @var string $alignment
     */
    private $alignment;
    
    /**
     * @var string $options
     */
    private $options;

    /**
     * @var boolean $mandatory_response
     */
    private $mandatory_response;

    /**
     * @var integer $level
     */
    private $level;

    /**
     * @var integer $section
     */
    private $section;

    /**
     * @var string $sequence
     */
    private $sequence;

    /**
     * @var string $original_sequence
     */
    private $original_sequence;

    /**
     * @var integer $ceil_score
     */
    private $ceil_score;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollscheduling", inversedBy="pollschedulingquestions")
     * @ORM\JoinColumn(name="pollscheduling_id", referencedColumnName="id")
     */
    protected $pollscheduling;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollquestion", inversedBy="pollschedulingquestions")
     * @ORM\JoinColumn(name="pollquestion_id", referencedColumnName="id")
     */
    protected $pollquestion;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollschedulingsection", inversedBy="pollschedulingquestions")
     * @ORM\JoinColumn(name="pollschedulingsection_id", referencedColumnName="id")
     */
    protected $pollschedulingsection;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollapplicationquestion", mappedBy="pollschedulingquestion")
     */
    protected $pollapplicationquestions;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Pollschedulingquestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Pollschedulingquestion
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set alignment
     *
     * @param string $alignment
     * @return Pollschedulingquestion
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
    
        return $this;
    }

    /**
     * Get alignment
     *
     * @return string 
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return Pollschedulingquestion
     */
    public function setOptions($options)
    {
        $this->options = $options;
    
        return $this;
    }

    /**
     * Get options
     *
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set mandatory_response
     *
     * @param boolean $mandatoryResponse
     * @return Pollschedulingquestion
     */
    public function setMandatoryResponse($mandatoryResponse)
    {
        $this->mandatory_response = $mandatoryResponse;
    
        return $this;
    }

    /**
     * Get mandatory_response
     *
     * @return boolean
     */
    public function getMandatoryResponse()
    {
        return $this->mandatory_response;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Pollschedulingquestion
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set section
     *
     * @param integer $section
     * @return Pollschedulingquestion
     */
    public function setSection($section)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return integer 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set sequence
     *
     * @param string $sequence
     * @return Pollschedulingquestion
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return string
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set original_sequence
     *
     * @param string $originalSequence
     * @return Pollschedulingquestion
     */
    public function setOriginalSequence($originalSequence)
    {
        $this->original_sequence = $originalSequence;
    
        return $this;
    }

    /**
     * Get original_sequence
     *
     * @return string
     */
    public function getOriginalSequence()
    {
        return $this->original_sequence;
    }

    /**
     * Set ceil_score
     *
     * @param integer $ceilScore
     * @return Pollschedulingquestion
     */
    public function setCeilScore($ceilScore)
    {
        $this->ceil_score = $ceilScore;
    
        return $this;
    }

    /**
     * Get ceil_score
     *
     * @return integer 
     */
    public function getCeilScore()
    {
        return $this->ceil_score;
    }
    
    /**
     * Set pollscheduling
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollscheduling
     * @return Pollschedulingquestion
     */
    public function setPollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollscheduling = null)
    {
        $this->pollscheduling = $pollscheduling;
    
        return $this;
    }

    /**
     * Get pollscheduling
     *
     * @return Fishman\PollBundle\Entity\Pollscheduling 
     */
    public function getPollscheduling()
    {
        return $this->pollscheduling;
    }

    /**
     * Set pollquestion
     *
     * @param Fishman\PollBundle\Entity\Pollquestion $pollquestion
     * @return Pollschedulingquestion
     */
    public function setPollquestion(\Fishman\PollBundle\Entity\Pollquestion $pollquestion = null)
    {
        $this->pollquestion = $pollquestion;
    
        return $this;
    }

    /**
     * Get pollquestion
     *
     * @return Fishman\PollBundle\Entity\Pollquestion 
     */
    public function getPollquestion()
    {
        return $this->pollquestion;
    }

    /**
     * Set pollschedulingsection
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsection
     * @return Pollschedulingquestion
     */
    public function setPollschedulingsection(\Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsection = null)
    {
        $this->pollschedulingsection = $pollschedulingsection;
    
        return $this;
    }

    /**
     * Get pollschedulingsection
     *
     * @return Fishman\PollBundle\Entity\Pollschedulingsection 
     */
    public function getPollschedulingsection()
    {
        return $this->pollschedulingsection;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollapplicationquestions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add pollapplicationquestions
     *
     * @param Fishman\PollBundle\Entity\Pollapplicationquestion $pollapplicationquestions
     * @return Pollschedulingquestion
     */
    public function addPollapplicationquestion(\Fishman\PollBundle\Entity\Pollapplicationquestion $pollapplicationquestions)
    {
        $this->pollapplicationquestions[] = $pollapplicationquestions;
    
        return $this;
    }

    /**
     * Remove pollapplicationquestions
     *
     * @param Fishman\PollBundle\Entity\Pollapplicationquestion $pollapplicationquestions
     */
    public function removePollapplicationquestion(\Fishman\PollBundle\Entity\Pollapplicationquestion $pollapplicationquestions)
    {
        $this->pollapplicationquestions->removeElement($pollapplicationquestions);
    }

    /**
     * Get pollapplicationquestions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollapplicationquestions()
    {
        return $this->pollapplicationquestions;
    }

    /**
     * cloneRegisters
     */
    public static function cloneRegisters(DoctrineRegistry $doctrineRegistry, $entity, $array, $level, $userBy)
    {   
        $em = $doctrineRegistry->getManager();
        
        if ($entity->getPoll()->getLevel() === 0) {
            $level = 0;
        }
        
        if (!empty($array)) {
            if (isset($array['sections']) && $array['sections'] != '') {
                foreach ($array['sections'] as $key_section => $section) {
                  
                    $ps_entity = $em->getRepository('FishmanPollBundle:Pollsection')->find($key_section);
                    
                    $pss_entity = new Pollschedulingsection();
                    
                    if ($entity->getPoll()->getLevel() > 0) {
                        $pss_entity->setName($ps_entity->getName());
                        $pss_entity->setDescription($ps_entity->getDescription());
                        $pss_entity->setType($ps_entity->getType());
                        $pss_entity->setSequence($ps_entity->getSequence());
                        if ($ps_entity->getParentId() == '-1') {
                            $pss_entity->setParentId('-1');
                        }
                        else {
                            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingsection');
                            $pss_parent_result = $repository->createQueryBuilder('pss')
                                ->where('pss.pollscheduling = :pollscheduling 
                                        AND pss.pollsection = :pollsection')
                                ->setParameter('pollscheduling', $entity->getId())
                                ->setParameter('pollsection', $ps_entity->getParentId())
                                ->getQuery()
                                ->getResult();
                            $pss_parent_entity = current($pss_parent_result);
                            
                            $pss_entity->setParentId($pss_parent_entity->getId());
                            $pss_entity->setL1($pss_parent_entity->getL1());
                            $pss_entity->setL2($pss_parent_entity->getL2());
                            $pss_entity->setL3($pss_parent_entity->getL3());
                            $pss_entity->setL4($pss_parent_entity->getL4());
                            $pss_entity->setL5($pss_parent_entity->getL5());
                            $pss_entity->setL6($pss_parent_entity->getL6());
                            $pss_entity->setL7($pss_parent_entity->getL7());
                            $pss_entity->setL8($pss_parent_entity->getL8());
                            $pss_entity->setL9($pss_parent_entity->getL9());
                        }
                        $pss_entity->setPollscheduling($entity);
                        $pss_entity->setPollsection($ps_entity);
                        
                        $pss_entity->setStatus(1);
                        $pss_entity->setCreatedBy($userBy->getId());
                        $pss_entity->setModifiedBy($userBy->getId());
                        $pss_entity->setCreated(new \DateTime());
                        $pss_entity->setChanged(new \DateTime());
                    
                        $em->persist($pss_entity);
                        $em->flush();
                        
                        if ($pss_entity->getParentId() == '-1') {
                            $pss_entity->setL1($pss_entity->getId());
                        }
                        else {
                            $level = FALSE;
                          
                            if ($pss_entity->getL2() == '') {
                                $pss_entity->setL2($pss_entity->getId());
                                $level = TRUE;
                            }
                            if ($pss_entity->getL3() == '' && !$level) {
                                $pss_entity->setL3($pss_entity->getId());
                                $level = TRUE;
                            }
                            if ($pss_entity->getL4() == '' && !$level) {
                                $pss_entity->setL4($pss_entity->getId());
                                $level = TRUE;
                            }
                            if ($pss_entity->getL5() == '' && !$level) {
                                $pss_entity->setL5($pss_entity->getId());
                                $level = TRUE;
                            }
                            if ($pss_entity->getL6() == '' && !$level) {
                                $pss_entity->setL6($pss_entity->getId());
                                $level = TRUE;
                            }
                            if ($pss_entity->getL7() == '' && !$level) {
                                $pss_entity->setL7($pss_entity->getId());
                                $level = TRUE;
                            }
                            if ($pss_entity->getL8() == '' && !$level) {
                                $pss_entity->setL8($pss_entity->getId());
                                $level = TRUE;
                            }
                            if ($pss_entity->getL9() == '' && !$level) {
                                $pss_entity->setL9($pss_entity->getId());
                                $level = TRUE;
                            }
                        }
    
                        $em->persist($pss_entity);
                        $em->flush();
                    }
                            
                    if (!empty($section)) {
                        $j = 1;
                        if (isset($section['questions'])) {
                            foreach ($section['questions'] as $key_question => $question) {
                        
                                $pq_entity = $em->getRepository('FishmanPollBundle:Pollquestion')->find($question['id']);
                                
                                $psq_entity = new Pollschedulingquestion();
                                
                                // Total score
                                $ceil_score = 0;
                                    
                                $psq_entity->setQuestion($question['question']);
                                $psq_entity->setType($question['type']);
                                $psq_entity->setAlignment($question['alignment']);
                                $psq_entity->setOptions($question['options']);
                                $psq_entity->setMandatoryResponse($question['mandatory_response']);
                                $psq_entity->setSequence($j);
                                $psq_entity->setOriginalSequence($question['sequence']);
                                $psq_entity->setLevel($level);
                                $psq_entity->setPollscheduling($entity);
                                if ($entity->getPoll()->getLevel() > 0) {
                                    $psq_entity->setPollschedulingsection($pss_entity);
                                }
                                $psq_entity->setPollquestion($pq_entity);
                                
                                if ($question['type'] == 'multiple_option') {
                                    foreach ($question['options'] as $op) {
                                        $ceil_score += $op['score'];
                                    }
                                }
                                elseif (in_array($question['type'], array('unique_option', 'selection_option'))) {
                                    foreach ($question['options'] as $op) {
                                        if ($op['score'] > $ceil_score) {
                                            $ceil_score = $op['score'];
                                        }
                                    }
                                }
                                
                                $psq_entity->setCeilScore($ceil_score);
                                
                                $psq_entity->setStatus(1);
                                $psq_entity->setCreatedBy($userBy->getId());
                                $psq_entity->setModifiedBy($userBy->getId());
                                $psq_entity->setCreated(new \DateTime());
                                $psq_entity->setChanged(new \DateTime());
                            
                                $em->persist($psq_entity);
                                $em->flush();
                                
                                $j++;
                            }
                        }
                        
                        if (isset($section['sections'])) {
                            if ($entity->getPoll()->getLevel() === 0) {
                                $level = 0;
                            }
                            $sublevel = $level;
                            $sublevel++;
                            self::cloneRegisters($doctrineRegistry, $entity, $section, $sublevel, $userBy);
                        }
                    }
    
                }
            }
        }

    }

    /**
     * getArrayQuestions
     *
     */
    public static function getArrayQuestions(DoctrineRegistry $doctrineRegistry, $array, $pollschedulingid)
    {
        $output = '';
        
        if (!empty($array)) {
            foreach ($array as $key_section => $section) {
              
                $key_pss = Pollschedulingsection::getSectionId($doctrineRegistry, $pollschedulingid, $key_section);
                
                if (isset($section['title'])) {
                    $output[$key_pss]['title'] = $section['title'];
                    $output[$key_pss]['description'] = $section['description'];
                }
                
                if (isset($section['questions'])) {
                    foreach ($section['questions'] as $key_question => $question) {
                        
                        $opId = self::getQuestionId($doctrineRegistry, $pollschedulingid, $question['id']);
                      
                        $output[$key_pss]['questions'][$opId] = array(
                            'id' => $opId,
                            'question' => $question['question'], 
                            'type' => $question['type'], 
                            'alignment' => $question['alignment'], 
                            'options' => $question['options'], 
                            'mandatory_response' => $question['mandatory_response'], 
                            'sequence' => $question['sequence']
                        );
                        
                        unset($output[$key_pss]['questions'][$key_question]);
                        
                    }
                }

                if (isset($section['sections'])) {
                    
                    $children_sections = self::getArrayQuestions($doctrineRegistry, $section['sections'], $pollschedulingid);
                    
                    if (!empty($children_sections)) {
                        $output[$key_pss]['sections'] = $children_sections;
                    }
                }
            }
        }

        return $output;
    }

    /**
     * getQuestionId
     */
    public static function getQuestionId(DoctrineRegistry $doctrineRegistry, $pollschedulingid, $pollquestionid)
    {
        $output = '';
           
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
        $result = $repository->createQueryBuilder('psq')
            ->where('psq.pollscheduling = :pollschedulingid 
                    AND psq.pollquestion = :pollquestionid')
            ->setParameter('pollschedulingid', $pollschedulingid)
            ->setParameter('pollquestionid', $pollquestionid)
            ->getQuery()
            ->getResult();
       
       if (!empty($result)) {
          $record = current($result);
          $output = $record->getId();
       }
       
       return $output;
    }
    /**
     * @var boolean $status
     */
    private $status;


    /**
     * Set status
     *
     * @param boolean $status
     * @return Pollschedulingquestion
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pollschedulingquestion
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Pollschedulingquestion
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Pollschedulingquestion
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Pollschedulingquestion
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
}