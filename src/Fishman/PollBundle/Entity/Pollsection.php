<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\PollBundle\Entity\Pollsection
 */
class Pollsection
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var boolean $type
     */
    private $type;

    /**
     * @var integer $parent_id
     */
    private $parent_id;

    /**
     * @var smallint $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var smallint $sequence
     */
    private $l1;

    /**
     * @var smallint $sequence
     */
    private $l2;

    /**
     * @var smallint $sequence
     */
    private $l3;

    /**
     * @var smallint $sequence
     */
    private $l4;

    /**
     * @var smallint $sequence
     */
    private $l5;

    /**
     * @var smallint $sequence
     */
    private $l6;

    /**
     * @var smallint $sequence
     */
    private $l7;

    /**
     * @var smallint $sequence
     */
    private $l8;

    /**
     * @var smallint $sequence
     */
    private $l9;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Poll", inversedBy="pollsections")
     * @ORM\JoinColumn(name="poll_id", referencedColumnName="id")
     */
    protected $poll;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollquestion", mappedBy="pollsection")
     */
    protected $pollquestions;  

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollschedulingsection", mappedBy="pollsection")
     */
    protected $pollschedulingsections; 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Pollsection
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Pollsection
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Pollsection
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the parent id.
     * @return integer the parent id if the node not have parent then return -1
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Set de parent of the node. If 
     * @param integer $parent_id.
     */
    public function setParentId($parentId)
    {
        $this->parent_id = $parentId;
    }

    /**
     * Set sequence
     *
     * @param smallint $sequence
     * @return Pollsection
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return smallint 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Pollsection
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set l1
     *
     * @param string $l1
     * @return Pollquestion
     */
    public function setL1($l1)
    {
        $this->l1 = $l1;
    
        return $this;
    }

    /**
     * Get l1
     *
     * @return string 
     */
    public function getL1()
    {
        return $this->l1;
    }

    /**
     * Set l2
     *
     * @param string $l2
     * @return Pollquestion
     */
    public function setL2($l2)
    {
        $this->l2 = $l2;
    
        return $this;
    }

    /**
     * Get l2
     *
     * @return string 
     */
    public function getL2()
    {
        return $this->l2;
    }

    /**
     * Set l3
     *
     * @param string $l3
     * @return Pollquestion
     */
    public function setL3($l3)
    {
        $this->l3 = $l3;
    
        return $this;
    }

    /**
     * Get l3
     *
     * @return string 
     */
    public function getL3()
    {
        return $this->l3;
    }

    /**
     * Set l4
     *
     * @param string $l4
     * @return Pollquestion
     */
    public function setL4($l4)
    {
        $this->l4 = $l4;
    
        return $this;
    }

    /**
     * Get l4
     *
     * @return string 
     */
    public function getL4()
    {
        return $this->l4;
    }

    /**
     * Set l5
     *
     * @param string $l5
     * @return Pollquestion
     */
    public function setL5($l5)
    {
        $this->l5 = $l5;
    
        return $this;
    }

    /**
     * Get l5
     *
     * @return string 
     */
    public function getL5()
    {
        return $this->l5;
    }

    /**
     * Set l6
     *
     * @param string $l6
     * @return Pollquestion
     */
    public function setL6($l6)
    {
        $this->l6 = $l6;
    
        return $this;
    }

    /**
     * Get l6
     *
     * @return string 
     */
    public function getL6()
    {
        return $this->l6;
    }

    /**
     * Set l7
     *
     * @param string $l7
     * @return Pollquestion
     */
    public function setL7($l7)
    {
        $this->l7 = $l7;
    
        return $this;
    }

    /**
     * Get l7
     *
     * @return string 
     */
    public function getL7()
    {
        return $this->l7;
    }

    /**
     * Set l8
     *
     * @param string $l8
     * @return Pollquestion
     */
    public function setL8($l8)
    {
        $this->l8 = $l8;
    
        return $this;
    }

    /**
     * Get l8
     *
     * @return string 
     */
    public function getL8()
    {
        return $this->l8;
    }

    /**
     * Set l9
     *
     * @param string $l9
     * @return Pollquestion
     */
    public function setL9($l9)
    {
        $this->l9 = $l9;
    
        return $this;
    }

    /**
     * Get l9
     *
     * @return string 
     */
    public function getL9()
    {
        return $this->l9;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pollsection
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Pollsection
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Pollsection
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Pollsection
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   
    /**
     * Set poll
     *
     * @param Fishman\PollBundle\Entity\Poll $poll
     * @return Pollsection
     */
    public function setPoll(\Fishman\PollBundle\Entity\Poll $poll = null)
    {
        $this->poll = $poll;
    
        return $this;
    }

    /**
     * Get poll
     *
     * @return Fishman\PollBundle\Entity\Poll 
     */
    public function getPoll()
    {
        return $this->poll;
    }

    public function __toString()
    {
         return $this->name;
    }  
                  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollquestions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add pollquestions
     *
     * @param Fishman\PollBundle\Entity\Pollquestion $pollquestions
     * @return Pollsection
     */
    public function addPollquestion(\Fishman\PollBundle\Entity\Pollquestion $pollquestions)
    {
        $this->pollquestions[] = $pollquestions;
    
        return $this;
    }

    /**
     * Remove pollquestions
     *
     * @param Fishman\PollBundle\Entity\Pollquestion $pollquestions
     */
    public function removePollquestion(\Fishman\PollBundle\Entity\Pollquestion $pollquestions)
    {
        $this->pollquestions->removeElement($pollquestions);
    }

    /**
     * Get pollquestions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollquestions()
    {
        return $this->pollquestions;
    }
    
    /**
     * Add pollschedulingsections
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsections
     * @return Pollsection
     */
    public function addPollschedulingsection(\Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsections)
    {
        $this->pollschedulingsections[] = $pollschedulingsections;
    
        return $this;
    }

    /**
     * Remove pollschedulingsections
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsections
     */
    public function removePollschedulingsection(\Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsections)
    {
        $this->pollschedulingsections->removeElement($pollschedulingsections);
    }

    /**
     * Get pollschedulingsections
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulingsections()
    {
        return $this->pollschedulingsections;
    }

    /**
     * @return array with all organizational units of this company
     */
    public function getParentOptions(DoctrineRegistry $doctrine, $pollid, $withoutparentoption = true)
    {
        $options = array();
        if($withoutparentoption){
            $options = array(-1 => 'NINGUNO');
        }

        self::addChoiceOptions($doctrine, $this->getRootParents($doctrine, $pollid), 0, $options, $this->getId());
        return $options;
    }

    public static function addChoiceOptions(DoctrineRegistry $doctrine, $nodes, $level, &$options, $ignore_id)
    {
        $levelIndicator = str_pad('', $level, '---' , STR_PAD_LEFT);
        foreach($nodes as $node){
            if($node['id'] != $ignore_id){
                $options[$node['id']] = $levelIndicator . ' ' . $node['name'];
                self::addChoiceOptions($doctrine, self::getChildren($doctrine, $node['id']), $level + 3, $options, $ignore_id);
            }
        }
    }

    public static function getChildren(DoctrineRegistry $doctrine, $parent_id)
    {
        $repository = $doctrine->getRepository('FishmanPollBundle:Pollsection');
        $query = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.name')
            ->where('ps.parent_id = :parent_id')
            ->setParameter('parent_id', $parent_id)
            ->orderBy('ps.id', 'ASC')
            ->getQuery();
        return $query->getResult();        
    }

    public static function getRootParents(DoctrineRegistry $doctrine, $pollid)
    {
        // Get all root CompanyOrganizationaUnit From this Company
        $repository = $doctrine->getRepository('FishmanPollBundle:Pollsection');
        $query = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.name')
            ->where('ps.poll = :poll
                     AND ps.parent_id = :parent_id
                     AND ps.status = 1')
            ->setParameter('poll', $pollid)
            ->setParameter('parent_id', -1)
            ->orderBy('ps.name', 'ASC')
            ->getQuery();
        return $query->getResult();
    }
    
    /**
     * Similar to getParentOptions, but with two differences:
     *   - Is static
     *   - Include all Poll sections of some poll
     */
    public static function getPollSectionsOptions(DoctrineRegistry $doctrine, $pollid)
    {
        $options = array();

        //Ignore_id argument = -1, because we want all options
        self::addChoiceOptions($doctrine, self::getRootParents($doctrine, $pollid), 0, $options, -1);
        return $options;
    }

    /**
     * updateRegisterParentId
     */
    public static function updateRegisterParentId(DoctrineRegistry $doctrineRegistry, $entity)
    {   
        $em = $doctrineRegistry->getManager();
        
        if ($entity->getParentId() == '-1') {
            $entity->setL1($entity->getId());
            $entity->setL2(NULL);
            $entity->setL3(NULL);
            $entity->setL4(NULL);
            $entity->setL5(NULL);
            $entity->setL6(NULL);
            $entity->setL7(NULL);
            $entity->setL8(NULL);
            $entity->setL9(NULL);
        }
        else {
          
            $level = FALSE;
            
            if ($entity->getL2() == '') {
                $entity->setL2($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL3() == '' && !$level) {
                $entity->setL3($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL4() == '' && !$level) {
                $entity->setL4($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL5() == '' && !$level) {
                $entity->setL5($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL6() == '' && !$level) {
                $entity->setL6($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL7() == '' && !$level) {
                $entity->setL7($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL8() == '' && !$level) {
                $entity->setL8($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL9() == '' && !$level) {
                $entity->setL9($entity->getId());
                $level = TRUE;
            }
        }
        
        // Update Pollsections
        self::updateSections($doctrineRegistry, $entity, $entity->getId());

        $em->persist($entity);
        $em->flush();

    }

    /**
     * updateSections
     */
    public static function updateSections(DoctrineRegistry $doctrineRegistry, $entity, $parentId)
    {   
        $em = $doctrineRegistry->getManager();
            
        // Update Pollsection
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollsection');
        $results = $repository->createQueryBuilder('ps')
            ->where('ps.parent_id = :parent_id 
                    AND ps.poll = :poll')
            ->setParameter('parent_id', $parentId)
            ->setParameter('poll', $entity->getPoll())
            ->orderBy('ps.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        if (!empty($results)) {
            foreach ($results as $r) {
                $level = FALSE;
                if ($entity->getL1() == '') {
                    $r->setL1($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL1($entity->getL1());
                }
                if ($entity->getL2() == '' && !$level) {
                    $r->setL2($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL2($entity->getL2());
                }
                if ($entity->getL3() == '' && !$level) {
                    $r->setL3($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL3($entity->getL3());
                }
                if ($entity->getL4() == '' && !$level) {
                    $r->setL4($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL4($entity->getL4());
                }
                if ($entity->getL5() == '' && !$level) {
                    $r->setL5($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL5($entity->getL5());
                }
                if ($entity->getL6() == '' && !$level) {
                    $r->setL6($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL6($entity->getL6());
                }
                if ($entity->getL7() == '' && !$level) {
                    $r->setL7($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL7($entity->getL7());
                }
                if ($entity->getL8() == '' && !$level) {
                    $r->setL8($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL8($entity->getL8());
                }
                if ($entity->getL9() == '' && !$level) {
                    $r->setL9($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL9($entity->getL9());
                }
            
                $em->persist($r);
                $em->flush();
            
                self::updateSections($doctrineRegistry, $r, $r->getId());
            }
        }
    }

    /**
     * arraySectionsAndQuestions
     */
    public static function arraySectionsAndQuestions(DoctrineRegistry $doctrineRegistry, $pollid, $parent_id = '-1')
    {
        $output = '';
        
        // Recover Pollsections to Pollscheduling
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollsection');
        $sections = $repository->createQueryBuilder('ps')
            ->where('ps.parent_id = :parent_id 
                    AND ps.poll = :poll
                    AND ps.status = 1')
            ->setParameter('parent_id', $parent_id)
            ->setParameter('poll', $pollid)
            ->orderBy('ps.sequence', 'ASC', 'ps.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        if (!empty($sections)) {
            foreach ($sections as $section) {

                $output[$section->getId()]['title'] = $section->getName();
                $output[$section->getId()]['description'] = $section->getDescription();
                    
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollquestion');
                $questions = $repository->createQueryBuilder('pq')
                    ->where('pq.pollsection = :pollsection 
                            AND pq.poll = :poll
                            AND pq.status = 1')
                    ->setParameter('pollsection', $section)
                    ->setParameter('poll', $pollid)
                    ->orderBy('pq.sequence', 'ASC', 'pq.id', 'ASC')
                    ->getQuery()
                    ->getResult();
                
                if (!empty($questions)) {
                    foreach ($questions as $question) {
                        
                        $output[$section->getId()]['questions'][$question->getId()] = array(
                            'id' => $question->getId(),
                            'question' => $question->getQuestion(), 
                            'type' => $question->getType(), 
                            'alignment' => $question->getAlignment(), 
                            'options' => $question->getOptions(), 
                            'mandatory_response' => $question->getMandatoryResponse(), 
                            'sequence' => $question->getSequence(), 
                            'status' => $question->getStatus()
                        );
                        
                    }
                }

                $children_sections = self::arraySectionsAndQuestions($doctrineRegistry, $section->getPoll()->getId(), $section->getId());
                
                if (!empty($children_sections)) {
                    $output[$section->getId()]['sections'] = $children_sections;
                }
            }
        }

        return $output;
    }

    /**
     * arraySectionsAndQuestions
     */
    public static function arraySectionsAndQuestionsPTT(DoctrineRegistry $doctrineRegistry, $pollid, $parent_id = '-1')
    {
        $output = '';
        
        // Recover Pollsections to Pollscheduling
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollsection');
        $sections = $repository->createQueryBuilder('ps')
            ->where('ps.parent_id = :parent_id 
                    AND ps.poll = :poll
                    AND ps.status = 1')
            ->setParameter('parent_id', $parent_id)
            ->setParameter('poll', $pollid)
            ->orderBy('ps.sequence', 'ASC', 'ps.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        if (!empty($sections)) {
            foreach ($sections as $section) {
                                    
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollquestion');
                $questions = $repository->createQueryBuilder('pq')
                    ->where('pq.pollsection = :pollsection 
                            AND pq.poll = :poll
                            AND pq.status = 1')
                    ->setParameter('pollsection', $section)
                    ->setParameter('poll', $pollid)
                    ->orderBy('pq.sequence', 'ASC', 'pq.id', 'ASC')
                    ->getQuery()
                    ->getResult();
                
                if (!empty($questions)) {
                    foreach ($questions as $question) {
                        
                        $output[$section->getId()]['questions'][$question->getId()] = array( 
                            'type' => $question->getType()
                        );
                        
                    }
                }

                $children_sections = self::arraySectionsAndQuestions($doctrineRegistry, $section->getPoll()->getId(), $section->getId());
                
                if (!empty($children_sections)) {
                    $output[$section->getId()]['sections'] = $children_sections;
                }
            }
        }
        
        return $output;
    }

    /**
     * arraySectionsAndQuestionsBySectionIds
     */
    public static function arraySectionsAndQuestionsBySectionIds(DoctrineRegistry $doctrineRegistry, $pollid, $parent_id = '-1', $sectionIds, $continue = FALSE)
    {
        $output = '';
        
        // Recover Pollsections to Pollscheduling
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollsection');
        $sections = $repository->createQueryBuilder('ps')
            ->where('ps.parent_id = :parent_id 
                    AND ps.poll = :poll
                    AND ps.status = 1')
            ->setParameter('parent_id', $parent_id)
            ->setParameter('poll', $pollid)
            ->orderBy('ps.sequence', 'ASC', 'ps.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        if (!empty($sections)) {
            foreach ($sections as $section) {
                
                if (!empty($sectionIds)) {
                    $exit = FALSE;
                    foreach ($sectionIds as $sId) {
                        if ($section->getId() == $sId) {
                            $continue = TRUE;
                            $output[$section->getId()]['check'] = TRUE;
                            $exit = TRUE;
                        }
                        elseif (!$exit) {
                            $output[$section->getId()]['check'] = FALSE;
                        }
                    }
                }

                $output[$section->getId()]['title'] = $section->getName();
                
                if ($continue || empty($sectionIds)) {
                    
                    $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollquestion');
                    $questions = $repository->createQueryBuilder('pq')
                        ->where('pq.pollsection = :pollsection 
                                AND pq.poll = :poll
                                AND pq.status = 1')
                        ->setParameter('pollsection', $section)
                        ->setParameter('poll', $pollid)
                        ->orderBy('pq.sequence', 'ASC', 'pq.id', 'ASC')
                        ->getQuery()
                        ->getResult();
                    
                    if (!empty($questions)) {
                        foreach ($questions as $question) {
                            
                            $output[$section->getId()]['questions'][$question->getId()] = array(
                                'id' => $question->getId(),
                                'question' => $question->getQuestion(), 
                                'type' => $question->getType(), 
                                'alignment' => $question->getAlignment(), 
                                'options' => $question->getOptions(), 
                                'mandatory_response' => $question->getMandatoryResponse(), 
                                'sequence' => $question->getSequence(), 
                                'status' => $question->getStatus()
                            );
                            
                        }
                    }
                }
                else {
                    $continue = FALSE;
                }

                $children_sections = self::arraySectionsAndQuestionsBySectionIds($doctrineRegistry, $section->getPoll()->getId(), $section->getId(), $sectionIds, $continue);
                
                if (!empty($children_sections)) {
                    $output[$section->getId()]['sections'] = $children_sections;
                }
            }
        }

        return $output;
    }

    /**
     * arraySectionsAndQuestionsChecks
     */
    public static function arraySectionsAndQuestionsChecks(DoctrineRegistry $doctrineRegistry, $array, &$output = array())
    {
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                if ($value['check']) {
                    $output[$key] = $value;
                }
        
                if (!empty($value['sections'])) {
                    self::arraySectionsAndQuestionsChecks($doctrineRegistry, $value['sections'], $output);
                }
            }
        }

        return $output;
    }

    /**
     * arrayGroupQuestions
     */
    public static function arrayGroupQuestions(DoctrineRegistry $doctrineRegistry, $current_level, $level, $array)
    {
        $output = '';
        $current_level++;
        
        if ($level == '10') {
            $level = 3;
        }
        
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                
                if ($level > 0 || $level === 'null') {
                    $output[$key]['title'] = $value['title'];
                    $output[$key]['description'] = $value['description'];
                    
                    if (isset($value['questions'])) {
                        $output[$key]['questions'] = $value['questions'];
                    }
                    
                    if (isset($value['sections'])) {
                        $children_sections = self::arrayGroupQuestions($doctrineRegistry, $current_level, $level, $value['sections']);
                        
                        if ($current_level < $level || $level === 'null') {
                            if (!empty($children_sections)) {
                                $output[$key]['sections'] = $children_sections;
                            }
                        }
                        else {
                            if (!empty($children_sections)) {
                                foreach ($children_sections as $section) {
                                    if (isset($section['questions'])) {
                                        foreach ($section['questions'] as $key_question => $question) {
                                            $output[$key]['questions'][$key_question] = $question;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    if (isset($value['questions'])) {
                        foreach ($value['questions'] as $key_question => $question) {
                            $output[0]['questions'][$key_question] = $question;
                        }
                    }
                    
                    if (isset($value['sections'])) {
                        $children_sections = self::arrayGroupQuestions($doctrineRegistry, $current_level, $level, $value['sections']);
                        
                        if (!empty($children_sections)) {
                            foreach ($children_sections as $section) {
                                if (isset($section['questions'])) {
                                    foreach ($section['questions'] as $key_question => $question) {
                                        $output[0]['questions'][$key_question] = $question;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return $output;
    }

    /**
     * arrayRandomQuestions
     */
    public static function arrayRandomQuestions(DoctrineRegistry $doctrineRegistry, $array)
    {
        if (isset($array['sections'])) {
            foreach ($array['sections'] as $key => $value) {

                if (!empty($value['questions'])) {
                    shuffle($value['questions']);
                    $array['sections'][$key]['questions'] = $value['questions'];
                }
              
                if (isset($value['sections'])) {
                    $children_sections = self::arrayRandomQuestions($doctrineRegistry, $value);
                
                    if (!empty($children_sections)) {
                        $array['sections'][$key] = $children_sections;
                    }
                }
                
            }
        }
        
        return $array;
    }
    
    /**
     * Get list pollsection options
     * 
     */
    public static function getListPollsectionOptions(DoctrineRegistry $doctrine, $pollId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanPollBundle:Pollsection');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.name')
            ->where('ps.status = 1 
                    AND ps.poll = :poll')
            ->setParameter('poll', $pollId)
            ->orderBy('ps.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $output[$r['id']] = $r['name'];
        }
        
        return $output;
    }
}