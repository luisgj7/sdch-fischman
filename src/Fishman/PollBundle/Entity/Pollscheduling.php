<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Doctrine\Common\Collections\ArrayCollection;

use Fishman\AuthBundle\Entity\Permission;
use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\PollBundle\Entity\Pollsection;
use Fishman\PollBundle\Entity\Pollschedulingquestion;
use Fishman\PollBundle\Entity\Pollschedulingpeople;
use Fishman\WorkshopBundle\Entity\Workshopapplication;
use Fishman\NotificationBundle\Entity\Notificationscheduling;

/**
 * Fishman\PollBundle\Entity\Pollscheduling
 */
class Pollscheduling
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var float $version
     */
    private $version;

    /**
     * @var text $description
     */
    private $description;

    /**
     * @var string $type
     */
    private $type;
    
    /**
     * @var boolean $multiple_evaluation
     */
    private $multiple_evaluation;

    /**
     * @var string $level
     */
    private $level;
    
    /**
     * @var string $alignment
     */
    private $alignment;

    /**
     * @var string $divide
     */
    private $divide;

    /**
     * @var integer $number_question
     */
    private $number_question;

    /**
     * @var integer $duration
     */
    private $duration;

    /**
     * @var string $period
     */
    private $period;

    /**
     * @var \DateTime $initdate
     */
    private $initdate;

    /**
     * @var text $terms
     */
    private $terms;

    /**
     * @var boolean $use_terms
     */
    private $use_terms;

    /**
     * @var text $message_gratitude
     */
    private $message_gratitude;

    /**
     * @var text $message_end
     */
    private $message_end;

    /**
     * @var text $message_inactive
     */
    private $message_inactive;

    /**
     * @var text $custom_header_title
     */
    private $custom_header_title;

    /**
     * @var string $custom_header_title_text
     */
    private $custom_header_title_text;

    /**
     * @var string $custom_header_title_description
     */
    private $custom_header_title_description;

    /**
     * @var text $custom_header_logo
     */
    private $custom_header_logo;

    /**
     * @var \DateTime $enddate
     */
    private $enddate;

    /**
     * @var boolean $sequence_question
     */
    private $sequence_question;

    /**
     * @var smallint $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var integer $time_number
     */
    private $time_number;

    /**
     * @var integer $access_view_number
     */
    private $access_view_number;
    
    /**
     * @var string $status_bar
     */
    private $status_bar;
    
    /**
     * @var boolean $counselor_report
     */
    private $counselor_report;

    /**
     * @var boolean $current_version
     */
    private $current_version;

    /**
     * @var integer $original_id
     */
    private $original_id;

    /**
     * @var integer $company_id
     */
    private $company_id;

    /**
     * @var string $entity_type
     */
    private $entity_type;

    /**
     * @var integer $entity_id
     */
    private $entity_id;

    /**
     * @var integer $entity_poll_id
     */
    private $entity_poll_id;
    
    /**
     * @var boolean $pollscheduling_anonymous
     */
    private $pollscheduling_anonymous;
    
    /**
     * @var string $pollscheduling_period
     */
    private $pollscheduling_period;
    
    /**
     * @var integer $pollschedulingrelation_id
     */
    private $pollschedulingrelation_id;
    
    /**
     * @var string $serialized
     */
    private $serialized;

    /**
     * @var boolean $own
     */
    private $own;

    /**
     * @var boolean $deleted
     */
    private $deleted;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Poll", inversedBy="pollschedulings")
     * @ORM\JoinColumn(name="poll_id", referencedColumnName="id")
     */
    protected $poll;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Benchmarking", inversedBy="pollschedulings")
     * @ORM\JoinColumn(name="benchmarking_id", referencedColumnName="id")
     */
    protected $benchmarking;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollschedulingsection", mappedBy="pollscheduling")
     */
    protected $pollschedulingsections;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollschedulingquestion", mappedBy="pollscheduling")
     */
    protected $pollschedulingquestions;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollapplication", mappedBy="pollscheduling")
     */
    protected $pollapplications;

    protected $pollschedulingpeoples;
 
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Pollscheduling
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set version
     *
     * @param float $version
     * @return Pollscheduling
     */
    public function setVersion($version)
    {
        $this->version = $version;
    
        return $this;
    }

    /**
     * Get version
     *
     * @return float 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set description
     *
     * @param text $description
     * @return Pollscheduling
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Pollscheduling
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set alignment
     *
     * @param string $alignment
     * @return Pollscheduling
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
    
        return $this;
    }

    /**
     * Get alignment
     *
     * @return string 
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * Set divide
     *
     * @param string $divide
     * @return Pollscheduling
     */
    public function setDivide($divide)
    {
        $this->divide = $divide;
    
        return $this;
    }

    /**
     * Get divide
     *
     * @return string 
     */
    public function getDivide()
    {
        return $this->divide;
    }

    /**
     * Set initdate
     *
     * @param \DateTime $initdate
     * @return Pollscheduling
     */
    public function setInitdate($initdate)
    {
        $this->initdate = $initdate;
    
        return $this;
    }

    /**
     * Get initdate
     *
     * @return \DateTime 
     */
    public function getInitdate()
    {
        return $this->initdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return Pollscheduling
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set sequence
     *
     * @param smallint $sequence
     * @return Pollscheduling
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return smallint 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Pollscheduling
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pollscheduling
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Pollscheduling
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Pollscheduling
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Pollscheduling
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set entity_type
     *
     * @param integer $entityType
     * @return Pollscheduling
     */
    public function setEntityType($entityType)
    {
        $this->entity_type = $entityType;
    
        return $this;
    }

    /**
     * Get entity_type
     *
     * @return integer 
     */
    public function getEntityType()
    {
        return $this->entity_type;
    }

    /**
     * Set entity_id
     *
     * @param integer $entityId
     * @return Pollscheduling
     */
    public function setEntityId($entityId)
    {
        $this->entity_id = $entityId;
    
        return $this;
    }

    /**
     * Get entity_id
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entity_id;
    }

    /**
     * Set entity_poll_id
     *
     * @param integer $entityPollId
     * @return Pollscheduling
     */
    public function setEntityPollId($entityPollId)
    {
        $this->entity_poll_id = $entityPollId;
    
        return $this;
    }

    /**
     * Get entity_poll_id
     *
     * @return integer 
     */
    public function getEntityPollId()
    {
        return $this->entity_poll_id;
    }

    /**
     * Set company_id
     *
     * @param integer $companyId
     * @return Pollscheduling
     */
    public function setCompanyId($companyId)
    {
        $this->company_id = $companyId;
    
        return $this;
    }

    /**
     * Get company_id
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Pollscheduling
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set number_question
     *
     * @param integer $numberQuestion
     * @return Pollscheduling
     */
    public function setNumberQuestion($numberQuestion)
    {
        $this->number_question = $numberQuestion;
    
        return $this;
    }

    /**
     * Get number_question
     *
     * @return integer 
     */
    public function getNumberQuestion()
    {
        return $this->number_question;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Pollscheduling
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set period
     *
     * @param string $period
     * @return Pollscheduling
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return string 
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set sequence_question
     *
     * @param string $sequence_question
     * @return Poll
     */
    public function setSequenceQuestion($sequenceQuestion)
    {
        $this->sequence_question = $sequenceQuestion;
    
        return $this;
    }

    /**
     * Get sequence_question
     *
     * @return string 
     */
    public function getSequenceQuestion()
    {
        return $this->sequence_question;
    }
 
    /**
     * Set terms
     *
     * @param text $terms
     * @return Pollscheduling
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    
        return $this;
    }

    /**
     * Get terms
     *
     * @return text 
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Set use_terms
     *
     * @param boolean $useTerms
     * @return Pollscheduling
     */
    public function setUseTerms($useTerms)
    {
        $this->use_terms = $useTerms;
    
        return $this;
    }

    /**
     * Get use_terms
     *
     * @return integer 
     */
    public function getUseTerms()
    {
        return $this->use_terms;
    }
    
    /**
     * Set message_gratitude
     *
     * @param string $messageGratitude
     * @return Pollscheduling
     */
    public function setMessageGratitude($messageGratitude)
    {
        $this->message_gratitude = $messageGratitude;
    
        return $this;
    }

    /**
     * Get message_gratitude
     *
     * @return string 
     */
    public function getMessageGratitude()
    {
        return $this->message_gratitude;
    }

    /**
     * Set message_end
     *
     * @param string $messageEnd
     * @return Pollscheduling
     */
    public function setMessageEnd($messageEnd)
    {
        $this->message_end = $messageEnd;
    
        return $this;
    }

    /**
     * Get message_end
     *
     * @return string 
     */
    public function getMessageEnd()
    {
        return $this->message_end;
    }

    /**
     * Set message_inactive
     *
     * @param string $messageInactive
     * @return Pollscheduling
     */
    public function setMessageInactive($messageInactive)
    {
        $this->message_inactive = $messageInactive;
    
        return $this;
    }

    /**
     * Get message_inactive
     *
     * @return string 
     */
    public function getMessageInactive()
    {
        return $this->message_inactive;
    }

    /**
     * Set custom_header_title 
     *
     * @param string $custom_header_title
     * @return Pollscheduling
     */
    public function setCustomHeaderTitle($customHeaderTitle)
    {
        $this->custom_header_title = $customHeaderTitle;
    
        return $this;
    }

    /**
     * Get custom_header_title
     *
     * @return string 
     */
    public function getCustomHeaderTitle()
    {
        return $this->custom_header_title;
    }

    /**
     * Set custom_header_title_text
     *
     * @param string $customHeaderTitleText
     * @return Pollscheduling
     */
    public function setCustomHeaderTitleText($customHeaderTitleText)
    {
        $this->custom_header_title_text = $customHeaderTitleText;
    
        return $this;
    }

    /**
     * Get custom_header_title_text
     *
     * @return string 
     */
    public function getCustomHeaderTitleText()
    {
        return $this->custom_header_title_text;
    }

    /**
     * Set custom_header_title_description
     *
     * @param string $customHeaderTitleDescription
     * @return Pollscheduling
     */
    public function setCustomHeaderTitleDescription($customHeaderTitleDescription)
    {
        $this->custom_header_title_description = $customHeaderTitleDescription;
    
        return $this;
    }

    /**
     * Get custom_header_title_description
     *
     * @return string 
     */
    public function getCustomHeaderTitleDescription()
    {
        return $this->custom_header_title_description;
    }

    /**
     * Set custom_header_logo 
     *
     * @param string $custom_header_logo
     * @return Pollscheduling
     */
    public function setCustomHeaderLogo($customHeaderLogo)
    {
        $this->custom_header_logo = $customHeaderLogo;
    
        return $this;
    }

    /**
     * Get custom_header_logo 
     *
     * @return string 
     */
    public function getCustomHeaderLogo()
    {
        return $this->custom_header_logo;
    }

    /**
     * Set time_number
     *
     * @param smallint $timenumber
     * @return Pollscheduling
     */
    public function setTimeNumber($timeNumber)
    {
        $this->time_number = $timeNumber;
    
        return $this;
    }

    /**
     * Get time_number
     *
     * @return smallint 
     */
    public function getTimeNumber()
    {
        return $this->time_number;
    }

    /**
     * Set access_view_number
     *
     * @param int $access_view_number
     * @return Pollscheduling
     */
    public function setAccessViewNumber($access_view_number)
    {
        $this->access_view_number = $access_view_number;
        return $this;
    }

    /**
     * Get access_view_number
     *
     * @return int
     */
    public function getAccessViewNumber()
    {
        return $this->access_view_number;
    }

    /**
     * Set status_bar
     *
     * @param string $status_bar
     * @return Pollscheduling
     */
    public function setStatusBar($statusBar)
    {
        $this->status_bar = $statusBar;
    
        return $this;
    }

    /**
     * Get status_bar
     *
     * @return string 
     */
    public function getStatusBar()
    {
        return $this->status_bar;
    }

    /**
     * Set counselor_report
     *
     * @param boolean $counselorReport
     * @return Pollscheduling
     */
    public function setCounselorReport($counselorReport)
    {
        $this->counselor_report = $counselorReport;
    
        return $this;
    }

    /**
     * Get counselor_report
     *
     * @return boolean 
     */
    public function getCounselorReport()
    {
        return $this->counselor_report;
    }

    /**
     * Set current_version
     *
     * @param boolean $currentVersion
     * @return Pollscheduling
     */
    public function setCurrentVersion($currentVersion)
    {
        $this->current_version = $currentVersion;
    
        return $this;
    }

    /**
     * Get current_version
     *
     * @return boolean 
     */
    public function getCurrentVersion()
    {
        return $this->current_version;
    }

    /**
     * Set original_id
     *
     * @param integer $originalId
     * @return Pollscheduling
     */
    public function setOriginalId($originalId)
    {
        $this->original_id = $originalId;
    
        return $this;
    }

    /**
     * Get original_id
     *
     * @return integer 
     */
    public function getOriginalId()
    {
        return $this->original_id;
    }

    /**
     * Set pollscheduling_anonymous
     *
     * @param boolean $pollschedulingAnonymous
     * @return Pollscheduling
     */
    public function setPollschedulingAnonymous($pollschedulingAnonymous)
    {
        $this->pollscheduling_anonymous = $pollschedulingAnonymous;
    
        return $this;
    }

    /**
     * Get pollscheduling_anonymous
     *
     * @return boolean 
     */
    public function getPollschedulingAnonymous()
    {
        return $this->pollscheduling_anonymous;
    }

    /**
     * Set pollscheduling_period
     *
     * @param string $pollschedulingPeriod
     * @return Pollscheduling
     */
    public function setPollschedulingPeriod($pollschedulingPeriod)
    {
        $this->pollscheduling_period = $pollschedulingPeriod;
    
        return $this;
    }

    /**
     * Get pollscheduling_period
     *
     * @return string 
     */
    public function getPollschedulingPeriod()
    {
        return $this->pollscheduling_period;
    }

    /**
     * Set pollschedulingrelation_id
     *
     * @param integer $pollschedulingrelationId
     * @return Pollscheduling
     */
    public function setPollschedulingrelationId($pollschedulingrelationId)
    {
        $this->pollschedulingrelation_id = $pollschedulingrelationId;
    
        return $this;
    }

    /**
     * Get pollschedulingrelation_id
     *
     * @return integer 
     */
    public function getPollschedulingrelationId()
    {
        return $this->pollschedulingrelation_id;
    }

    /**
     * Set multiple_evaluation
     *
     * @param boolean $multipleEvaluation
     * @return Pollscheduling
     */
    public function setMultipleEvaluation($multipleEvaluation)
    {
        $this->multiple_evaluation = $multipleEvaluation;
    
        return $this;
    }

    /**
     * Get multiple_evaluation
     *
     * @return boolean 
     */
    public function getMultipleEvaluation()
    {
        return $this->multiple_evaluation;
    }

    /**
     * Set serialized
     *
     * @param string $serialized
     * @return Pollscheduling
     */
    public function setSerialized($serialized)
    {
        $this->serialized = $serialized;
    
        return $this;
    }

    /**
     * Get serialized
     *
     * @return string 
     */
    public function getSerialized()
    {
        return $this->serialized;
    }

    /**
     * Set own
     *
     * @param boolean $own
     * @return Pollscheduling
     */
    public function setOwn($own)
    {
        $this->own = $own;
    
        return $this;
    }

    /**
     * Get own
     *
     * @return boolean 
     */
    public function getOwn()
    {
        return $this->own;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Pollscheduling
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    public function __toString()
    {
         return $this->title;
    }


    /**
     * Set poll
     *
     * @param Fishman\PollBundle\Entity\Poll $poll
     * @return Pollscheduling
     */
    public function setPoll(\Fishman\PollBundle\Entity\Poll $poll = null)
    {
        $this->poll = $poll;
    
        return $this;
    }

    /**
     * Get poll
     *
     * @return Fishman\PollBundle\Entity\Poll 
     */
    public function getPoll()
    {
        return $this->poll;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollapplications = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add pollschedulingsections
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsections
     * @return Pollscheduling
     */
    public function addPollschedulingsection(\Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsections)
    {
        $this->pollschedulingsections[] = $pollschedulingsections;
    
        return $this;
    }

    /**
     * Remove pollschedulingsections
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsections
     */
    public function removePollschedulingsection(\Fishman\PollBundle\Entity\Pollschedulingsection $pollschedulingsections)
    {
        $this->pollschedulingsections->removeElement($pollschedulingsections);
    }

    /**
     * Get pollschedulingsections
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulingsections()
    {
        return $this->pollschedulingsections;
    }
    
    /**
     * Add pollschedulingquestions
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollschedulingquestions
     * @return Pollscheduling
     */
    public function addPollschedulingquestion(\Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions)
    {
        $this->pollschedulingquestions[] = $pollschedulingquestions;
    
        return $this;
    }

    /**
     * Remove pollschedulingquestions
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions
     */
    public function removePollschedulingquestion(\Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions)
    {
        $this->pollschedulingquestions->removeElement($pollschedulingquestions);
    }

    /**
     * Get pollschedulingquestions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulingquestions()
    {
        return $this->pollschedulingquestions;
    }
    
    /**
     * Add pollapplications
     *
     * @param Fishman\PollBundle\Entity\Pollapplication $pollapplications
     * @return Pollscheduling
     */
    public function addPollapplication(\Fishman\PollBundle\Entity\Pollapplication $pollapplications)
    {
        $this->pollapplications[] = $pollapplications;
    
        return $this;
    }

    /**
     * Remove pollapplications
     *
     * @param Fishman\PollBundle\Entity\Pollapplication $pollapplications
     */
    public function removePollapplication(\Fishman\PollBundle\Entity\Pollapplication $pollapplications)
    {
        $this->pollapplications->removeElement($pollapplications);
    }

    /**
     * Get pollapplications
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollapplications()
    {
        return $this->pollapplications;
    }

    /**
     * Add pollschedulingpeoples
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingpeople $pollschedulingpeoples
     * @return Pollscheduling
     */
    public function addPollschedulingpeople(\Fishman\PollBundle\Entity\Pollschedulingpeople $pollschedulingpeoples)
    {
        $this->pollschedulingpeoples[] = $pollschedulingpeoples;
    
        return $this;
    }

    /**
     * Remove pollschedulingpeoples
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingpeople $pollschedulingpeoples
     */
    public function removePollschedulingpeople(\Fishman\PollBundle\Entity\Pollschedulingpeople $pollschedulingpeoples)
    {
        $this->pollschedulingpeoples->removeElement($pollschedulingpeoples);
    }

    /**
     * Set benchmarking
     *
     * @param Fishman\PollBundle\Entity\Benchmarking $benchmarking
     * @return Pollscheduling
     */
    public function setBenchmarking(\Fishman\PollBundle\Entity\Benchmarking $benchmarking = null)
    {
        $this->benchmarking = $benchmarking;
    
        return $this;
    }

    /**
     * Get benchmarking
     *
     * @return Fishman\PollBundle\Entity\Benchmarking 
     */
    public function getBenchmarking()
    {
        return $this->benchmarking;
    }

    /**
     * Get pollschedulingpeoples
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulingpeoples()
    {
        return $this->pollschedulingpeoples;
    }

    /**
     * Get Last Five Pollschedulings
     */
    public static function getLastFivePollschedulings(DoctrineRegistry $doctrineRegistry, $rol)
    {   
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.time_number, c.name company, ps.initdate, ps.enddate')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->where('ps.status = 1 
                    AND ps.deleted = :deleted')
            ->setParameter('deleted', FALSE)
            ->orderBy('ps.initdate', 'DESC')
            ->setMaxResults(5);
            
        // Check is ROLE_TEACHER
        
        if ($rol == 'ROLE_TEACHER') {
            $queryBuilder
                ->andWhere('ps.pollscheduling_anonymous = 0
                        OR ps.pollscheduling_anonymous IS NULL');
        }
        
        $output = $queryBuilder->getQuery()->getResult();

        return $output;
    }

    /**
     * Get Last Five PollSchedulings Integrant
     */
    public static function getLastFivePollschedulingsIntegrant(DoctrineRegistry $doctrineRegistry, $wiId)
    {   
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $query = $repository->createQueryBuilder('pa')
            ->select('ps.id, ps.title, ps.time_number, c.name company, ps.initdate, ps.enddate')
            ->innerJoin('pa.pollscheduling', 'ps')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->where('pa.evaluator_id = :evaluator 
                    AND ps.status = 1 
                    AND ps.deleted = :deleted')
            ->setParameter('evaluator', $wiId)
            ->setParameter('deleted', FALSE)
            ->orderBy('pa.initdate', 'DESC')
            ->groupBy('pa.pollscheduling')
            ->setMaxResults(5)
            ->getQuery();
        $output = $query->getResult();

        return $output;
    }

    /**
     * Get List Pollapplications
     */
    public static function getListPollschedulings(DoctrineRegistry $doctrineRegistry, $data = '', $rol)
    {   
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.time_number, c.name company, ps.initdate, ps.enddate, 
                      COUNT(pa.id) total, SUM(pa.finished) quantity')
            ->leftJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->where('ps.status = 1 
                    AND ps.deleted = :deleted')
            ->andWhere('ps.id LIKE :id 
                    OR ps.title LIKE :title')
            ->setParameter('deleted', FALSE)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('title', '%' . $data['word'] . '%')
            ->orderBy('ps.initdate', 'DESC')
            ->groupBy('ps.id');
        
        // Check is ROLE_TEACHER
        
        if ($rol == 'ROLE_TEACHER') {
            $queryBuilder
                ->andWhere('ps.pollscheduling_anonymous = 0
                        OR ps.pollscheduling_anonymous IS NULL');
        }
        
        // Add arguments
        
        if (!empty($data)) {
            if ($data['resolved'] != '' || $data['resolved'] === 0) {
                $queryBuilder
                    ->andWhere('ps.time_number LIKE :resolved')
                    ->setParameter('resolved', '%' . $data['resolved'] . '%');
            }
            if ($data['company'] != '') {
                $queryBuilder
                    ->andWhere('c.name LIKE :company')
                    ->setParameter('company', '%' . $data['company'] . '%');
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('ps.initdate = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('ps.enddate = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
        }
        
        $query = $queryBuilder->getQuery();
            
        return $query;
    }

    /**
     * Get List Pollapplications
     */
    public static function getListCounselorPollschedulings(DoctrineRegistry $doctrineRegistry, $data = '', $rol)
    {   
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, c.name company, ps.initdate, ps.enddate, 
                      COUNT(pa.id) total, SUM(pa.finished) quantity')
            ->leftJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->where('ps.counselor_report = 1 
                    AND ps.status = 1 
                    AND ps.deleted = :deleted')
            ->andWhere('ps.id LIKE :id 
                    OR ps.title LIKE :title')
            ->setParameter('deleted', FALSE)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('title', '%' . $data['word'] . '%')
            ->orderBy('ps.initdate', 'DESC')
            ->groupBy('ps.id');
        
        // Check is ROLE_TEACHER
        
        if ($rol == 'ROLE_TEACHER') {
            $queryBuilder
                ->andWhere('ps.pollscheduling_anonymous = 0
                        OR ps.pollscheduling_anonymous IS NULL');
        }
        
        // Add arguments
        
        if (!empty($data)) {
            if ($data['resolved'] != '' || $data['resolved'] === 0) {
                $queryBuilder
                    ->andWhere('ps.time_number LIKE :resolved')
                    ->setParameter('resolved', '%' . $data['resolved'] . '%');
            }
            if ($data['company'] != '') {
                $queryBuilder
                    ->andWhere('c.name LIKE :company')
                    ->setParameter('company', '%' . $data['company'] . '%');
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('ps.initdate = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('ps.enddate = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
        }
        
        $query = $queryBuilder->getQuery();
            
        return $query;
    }

    /**
     * Get List Pollschedulings Integrant
     */
    public static function getListPollschedulingsIntegrant(DoctrineRegistry $doctrineRegistry, $wiId, $data = '')
    {       
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('ps.id, ps.title, ps.time_number, c.name company, ps.initdate, ps.enddate')
            ->innerJoin('pa.pollscheduling', 'ps')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->where('pa.evaluator_id = :evaluator 
                    AND ps.status = 1 
                    AND ps.deleted = :deleted')
            ->andWhere('ps.id LIKE :id 
                    OR ps.title LIKE :title')
            ->setParameter('evaluator', $wiId)
            ->setParameter('deleted', FALSE)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('title', '%' . $data['word'] . '%')
            ->orderBy('ps.initdate', 'DESC')
            ->groupBy('ps.id');
        
        // Add arguments
        
        if (!empty($data)) {
            if ($data['company'] != '') {
                $queryBuilder
                    ->andWhere('c.name LIKE :company')
                    ->setParameter('company', '%' . $data['company'] . '%');
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('ps.initdate = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('ps.enddate = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get Workshop Last Five Pollschedulings
     */
    public static function getWorkshopLastFivePollschedulings(DoctrineRegistry $doctrineRegistry, $wiId, $waId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $query = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.sequence, ps.time_number, ps.initdate, ps.enddate')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->where('pa.entity_application_id = :workshopapplication 
                    AND pa.entity_application_type = :type 
                    AND pa.evaluator_id = :evaluator
                    AND ps.status = 1 
                    AND ps.deleted = :ps_deleted
                    AND pa.deleted = :pa_deleted')
            ->setParameter('workshopapplication', $waId)
            ->setParameter('type', 'workshopapplication')
            ->setParameter('evaluator', $wiId)
            ->setParameter('ps_deleted', FALSE)
            ->setParameter('pa_deleted', FALSE)
            ->orderBy('pa.finished', 'ASC', 'ps.initdate', 'ASC')
            ->groupBy('ps.id')
            ->setMaxResults(5)
            ->getQuery();
        $output = $query->getResult();

        return $output;
    }

    /**
     * Get Workshop Last Five Pollschedulings Admin
     */
    public static function getWorkshopLastFivePollschedulingsAdmin(DoctrineRegistry $doctrineRegistry, $wsId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $query = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.sequence, ps.time_number, ps.initdate, ps.enddate')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.entity_id = :workshopscheduling 
                    AND ps.entity_type = :type 
                    AND ps.status = 1 
                    AND ps.deleted = :ps_deleted')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('type', 'workshopscheduling')
            ->setParameter('ps_deleted', FALSE)
            ->orderBy('pa.finished', 'ASC', 'ps.initdate', 'ASC')
            ->groupBy('ps.id')
            ->setMaxResults(5)
            ->getQuery();
        $output = $query->getResult();

        return $output;
    }

    /**
     * Get Workshop List Pollschedulings
     */
    public static function getWorkshopListPollschedulings(DoctrineRegistry $doctrineRegistry, $wiId, $waId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.sequence, ps.time_number, ps.initdate, ps.enddate, 
                      ps.duration, ps.period, ps.initdate, ps.enddate')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->where('pa.entity_application_id = :workshopapplication 
                    AND pa.entity_application_type = :type 
                    AND pa.evaluator_id = :evaluator
                    AND ps.status = 1 
                    AND ps.deleted = :ps_deleted
                    AND pa.deleted = :pa_deleted')
            ->andWhere('ps.title LIKE :title')
            ->setParameter('workshopapplication', $waId)
            ->setParameter('type', 'workshopapplication')
            ->setParameter('evaluator', $wiId)
            ->setParameter('ps_deleted', FALSE)
            ->setParameter('pa_deleted', FALSE)
            ->setParameter('title', '%' . $data['word'] . '%')
            ->orderBy('pa.finished', 'ASC', 'ps.initdate', 'ASC')
            ->groupBy('ps.id');
        
        // Add arguments
        
        if (!empty($data)) {
            if ($data['duration'] != '') {
                $queryBuilder
                    ->andWhere('ps.duration LIKE :duration')
                    ->setParameter('duration', '%' . $data['duration'] . '%');
            }
            if ($data['period'] != '') {
                $queryBuilder
                    ->andWhere('ps.period = :period')
                    ->setParameter('period', $data['period']);
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('ps.initdate = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('ps.enddate = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
            if ($data['sequence'] != '') {
                $queryBuilder
                    ->andWhere('ps.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }
        }
        
        $query = $queryBuilder->getQuery();

        return $query;
    }

    /**
     * Get Workshop List Pollschedulings Admin
     */
    public static function getWorkshopListPollschedulingsAdmin(DoctrineRegistry $doctrineRegistry, $wsId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.sequence, ps.time_number, ps.initdate, ps.enddate, 
                      ps.duration, ps.period, ps.initdate, ps.enddate')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.entity_id = :workshopscheduling 
                    AND ps.entity_type = :type 
                    AND ps.status = 1 
                    AND ps.deleted = :ps_deleted')
            ->andWhere('ps.title LIKE :title')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('type', 'workshopscheduling')
            ->setParameter('ps_deleted', FALSE)
            ->setParameter('title', '%' . $data['word'] . '%')
            ->orderBy('pa.finished', 'ASC', 'ps.initdate', 'ASC')
            ->groupBy('ps.id');
        
        // Add arguments
        
        if (!empty($data)) {
            if ($data['duration'] != '') {
                $queryBuilder
                    ->andWhere('ps.duration LIKE :duration')
                    ->setParameter('duration', '%' . $data['duration'] . '%');
            }
            if ($data['period'] != '') {
                $queryBuilder
                    ->andWhere('ps.period = :period')
                    ->setParameter('period', $data['period']);
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('ps.initdate = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('ps.enddate = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
            if ($data['sequence'] != '') {
                $queryBuilder
                    ->andWhere('ps.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }
        }
        
        $query = $queryBuilder->getQuery();

        return $query;
    }

    /**
     * Get Current Pollscheudling Follow
     */
    public static function getCurrentPollschedulingFollow(DoctrineRegistry $doctrineRegistry, $pollId)
    {       
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $query = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.type, ps.pollscheduling_anonymous anonymous, 
                      ps.pollscheduling_period pollschedulingperiod, c.name company, c.id company_id, 
                      c.image company_image, ps.version, ps.time_number timenumber, ps.initdate, ps.enddate, 
                      ps.custom_header_title, ps.custom_header_title_text, ps.custom_header_title_description, 
                      ps.custom_header_logo, COUNT(pa.id) total, SUM(pa.finished) quantity, 
                      ps.entity_type, ps.entity_id, ps.deleted')
            ->leftJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->where('ps.id = :pollschedulingid 
                    AND ps.status = 1 
                    AND ps.deleted = :deleted 
                    AND pa.deleted = 0')
            ->setParameter('pollschedulingid', $pollId)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();
            
        $output = current($query);
        
        return $output;
    }

    /**
     * Get Current Pollscheudling Follow to Workshop
     */
    public static function getWorkshopCurrentPollschedulingFollow(DoctrineRegistry $doctrineRegistry, $wsId, $pollId)
    {       
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $query = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.pollscheduling_anonymous anonymous, ps.pollscheduling_period pollschedulingperiod, 
                      ps.type, ps.initdate, ps.enddate, COUNT(pa.id) total, SUM(pa.finished) quantity, ps.deleted')
            ->leftJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.id = :pollschedulingid 
                    AND ps.entity_type = :entity_type 
                    AND ps.entity_id = :entity_id 
                    AND ps.status = 1 
                    AND ps.deleted = :deleted')
            ->setParameter('pollschedulingid', $pollId)
            ->setParameter('entity_type', 'workshopscheduling')
            ->setParameter('entity_id', $wsId)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();
            
        $output = current($query);
        
        return $output;
    }

    /**
     * Get List Pollschedulings Follow
     */
    public static function getWorkshopListPollschedulingsFollow(DoctrineRegistry $doctrineRegistry, $wsId, $ids = '', $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.sequence, ps.title, ps.time_number, ps.duration, ps.period, 
                      ps.initdate, ps.enddate, SUM(pa.finished) / COUNT(pa.id) * 100 as percentaje')
            ->leftJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.entity_type = :entity_type 
                    AND ps.entity_id = :entity_id 
                    AND ps.status = 1 
                    AND ps.deleted = :deleted')
            ->setParameter('entity_type', 'workshopscheduling')
            ->setParameter('entity_id', $wsId)
            ->setParameter('deleted', FALSE)
            ->orderBy('ps.initdate', 'DESC')
            ->groupBy('ps.id');
            
        // Add arguments
        
        if ($ids != '') {
            $queryBuilder
                ->andWhere('pa.evaluated_id IN(' . $ids . ')');
        }
        if (!empty($data)) {
            if ($data['sequence'] != '' || $data['sequence'] === 0) {
                $queryBuilder
                    ->andWhere('ps.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }   
            if ($data['word'] != '') {
                $queryBuilder
                    ->andWhere('ps.title LIKE :title')
                    ->setParameter('title', '%' . $data['word'] . '%');
            }
            if ($data['resolved'] != '' || $data['resolved'] === 0) {
                $queryBuilder
                    ->andWhere('ps.time_number LIKE :resolved')
                    ->setParameter('resolved', '%' . $data['resolved'] . '%');
            }
            if ($data['duration'] != '') {
                $queryBuilder
                    ->andWhere('ps.duration LIKE :duration')
                    ->setParameter('duration', '%' . $data['duration'] . '%');
            }
            if ($data['period'] != '') {
                $queryBuilder
                    ->andWhere('ps.period = :period')
                    ->setParameter('period', $data['period']);
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('ps.initdate = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('ps.enddate = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
        }
        
        $query = $queryBuilder->getQuery();
            
        return $query;
    }

    /**
     * Get List Pollschedulings by Company Follow
     */
    public static function getListPollschedulingsbyCompanyFollow(DoctrineRegistry $doctrineRegistry, $companyId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.sequence, ps.title, ps.time_number, ps.duration, ps.period, 
                      ps.initdate, ps.enddate, SUM(pa.finished) / COUNT(pa.id) * 100 as percentaje')
            ->leftJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.company_id = :company_id 
                    AND ps.status = 1 
                    AND ps.deleted = 0
                    AND ps.company_id <> 2 
                    AND pa.deleted = 0')
            ->setParameter('company_id', $companyId)
            ->orderBy('ps.initdate', 'DESC')
            ->groupBy('ps.id');
            
        // Add arguments
        
        if (!empty($data)) {
            if ($data['sequence'] != '' || $data['sequence'] === 0) {
                $queryBuilder
                    ->andWhere('ps.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }   
            if ($data['word'] != '') {
                $queryBuilder
                    ->andWhere('ps.title LIKE :title')
                    ->setParameter('title', '%' . $data['word'] . '%');
            }
            if ($data['resolved'] != '' || $data['resolved'] === 0) {
                $queryBuilder
                    ->andWhere('ps.time_number LIKE :resolved')
                    ->setParameter('resolved', '%' . $data['resolved'] . '%');
            }
            if ($data['duration'] != '') {
                $queryBuilder
                    ->andWhere('ps.duration LIKE :duration')
                    ->setParameter('duration', '%' . $data['duration'] . '%');
            }
            if ($data['period'] != '') {
                $queryBuilder
                    ->andWhere('ps.period = :period')
                    ->setParameter('period', $data['period']);
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('ps.initdate = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('ps.enddate = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
        }
        
        $query = $queryBuilder->getQuery();
            
        return $query;
    }

    /**
     * Clone Register 
     */
    public static function cloneRegister(DoctrineRegistry $doctrineRegistry, $entity, $entityId, $userBy)
    {
        $em = $doctrineRegistry->getManager();
      
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Entitypoll');
        $results = $repository->createQueryBuilder('wp')
            ->where('wp.entity_id = :workshopid')
            ->setParameter('workshopid',  $entityId)
            ->getQuery()
            ->getResult();
            
        if (!empty($results)) {
            foreach ($results as $wp) {
                $poll = new Pollscheduling();

                // Total assigned polls to a company
                $pollschedulingCompany = $em->createQuery('SELECT COUNT(psc.id) num 
                        FROM FishmanPollBundle:Pollscheduling psc
                        WHERE psc.poll = :poll 
                        AND psc.company_id = :company')
                    ->setParameter('poll', $wp->getPoll()->getId())
                    ->setParameter('company', $entity->getCompany()->getId())
                    ->getSingleResult();
                $time_number = $pollschedulingCompany['num'] + 1;
                
                $poll->setPoll($wp->getPoll()); 
                $poll->setEntityType('workshopscheduling'); 
                $poll->setEntityId($entity->getId());  
                $poll->setEntityPollId($wp->getId()); 
                $poll->setTitle($wp->getPoll()->getTitle()); 
                $poll->setVersion($wp->getPoll()->getVersion());
                $poll->setType($wp->getPoll()->getType());
                $poll->setMultipleEvaluation(0);
                $poll->setDescription($wp->getPoll()->getDescription()); 
                $poll->setLevel($wp->getPoll()->getLevel()); 
                $poll->setDivide($wp->getPoll()->getDivide());
                $poll->setNumberQuestion($wp->getPoll()->getNumberQuestion());
                $poll->setSequence($wp->getSequence()); 
                $poll->setStatus($wp->getStatus()); 
                $poll->setDuration($wp->getDuration()); 
                $poll->setPeriod($wp->getPeriod()); 
                $poll->setAlignment($wp->getPoll()->getAlignment());
                $poll->setStatusBar('question');
                $poll->setCounselorReport(0);
                $poll->setCompanyId($entity->getCompany()->getId());
                $poll->setSequenceQuestion($wp->getPoll()->getSequenceQuestion()); 
                $poll->setCurrentVersion($wp->getPoll()->getCurrentVersion()); 
                $poll->setTimeNumber($time_number);
                $poll->setUseTerms(FALSE); 
                $poll->setOwn(FALSE); 
                $poll->setDeleted(FALSE);
                $poll->setCreatedBy($userBy->getId());
                $poll->setModifiedBy($userBy->getId());
                $poll->setCreated(new \DateTime());
                $poll->setChanged(new \DateTime());
    
                $em->persist($poll);
                $em->flush();

                // Operations Pollscheduling
                self::operationsPollscheduling($doctrineRegistry, $poll, $userBy);
            
            }
        }
    }

    /**
     * Operations Pollscheduling
     */
    public static function operationsPollscheduling(DoctrineRegistry $doctrineRegistry, $entity, $userBy, $notNotification = FALSE)
    {
        $em = $doctrineRegistry->getManager();
          
        // Created array Sections and Questions
        $pollArray['sections'] = Pollsection::arraySectionsAndQuestions($doctrineRegistry, $entity->getPoll()->getId());
        
        if ($entity->getPoll()->getLevel() != '10') {
            // Group array Sections
            $pollArray['sections'] = Pollsection::arrayGroupQuestions($doctrineRegistry, 0, $entity->getPoll()->getLevel(), $pollArray['sections']);
        }
        
        if (!$entity->getPoll()->getSequenceQuestion()) {
            // Random array Questions
            $pollArray = Pollsection::arrayRandomQuestions($doctrineRegistry, $pollArray);
        }
        
        // Clone Pollquestions
        Pollschedulingquestion::cloneRegisters($doctrineRegistry, $entity, $pollArray, 1, $userBy);
        
        // Clone Notifications
        if (!$notNotification) {
            Notificationscheduling::cloneChildren($doctrineRegistry, -1, -1, $entity, 'pollscheduling', $entity->getPoll()->getId(), 'poll', $userBy);
        }  
        
        // Update array Questions with id pollschedulingquestion
        $pollArray = Pollschedulingquestion::getArrayQuestions($doctrineRegistry, $pollArray['sections'], $entity->getId());
        
        // Add Serialized
        $entity->setSerialized($pollArray);
        $em->persist($entity);
        $em->flush();
    }

    /**
     * Operations Pollscheduling Anonymous
     */
    public static function operationsPollschedulingAnonymous(DoctrineRegistry $doctrineRegistry, $pollscheduling, $userBy)
    {
        $em = $doctrineRegistry->getManager();
        
        $workinginformation = $em->getRepository('FishmanAuthBundle:Workinginformation')->find(2);
        
        // Add Pollschedulingpeople
        
        $psp_entity = new Pollschedulingpeople();
        $token = Pollapplication::generateCode();

        $psp_entity->setProfile('integrant');
        $psp_entity->setStatus(TRUE);
        $psp_entity->setPollscheduling($pollscheduling);
        $psp_entity->setWorkinginformation($workinginformation);
        $psp_entity->setPersonaltoken($token);
        $psp_entity->setCreated(new \DateTime());
        $psp_entity->setChanged(new \DateTime());
        $psp_entity->setCreatedBy($userBy->getId());
        $psp_entity->setModifiedBy($userBy->getId());
    
        $em->persist($psp_entity);                    
        $em->flush();
    }

    /**
     * deleteRegisters
     */
    public static function deleteRegisters(DoctrineRegistry $doctrineRegistry, $type, $entity)
    {   
        $em = $doctrineRegistry->getManager();
            
        // Delete pollschedulings
      
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $results = $repository->createQueryBuilder('ps')
            ->where('ps.entity_type = :entity_type 
                    AND ps.entity_id = :entity_id')
            ->setParameter('entity_type', $type)
            ->setParameter('entity_id', $entity->getId())
            ->getQuery()
            ->getResult();
        
        if (!empty($results)) {
            foreach ($results as $ps_entity) {
              
                $ps_entity->setDeleted(TRUE);
    
                $em->persist($ps_entity);
                $em->flush();
                
                //Delete Pollapplication to Pollscheduling
                Pollapplication::deleteRegisters($doctrineRegistry, $ps_entity);
            }
        }
    }

    /**
     * Get List Polls To Evaluated
     */
    public static function getListCompaniesByProfile(DoctrineRegistry $doctrineRegistry, $wsId = '', $ids = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('c.id, c.name')
            ->innerJoin('ps.poll', 'p')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.status = 1 
                    AND ps.deleted = 0 
                    AND ps.company_id <> 2')
            ->orderBy('c.id', 'ASC')
            ->groupBy('c.id');
            
        if ($wsId != '') {
            $queryBuilder
                ->andWhere('ps.entity_type = :entity_type 
                        AND ps.entity_id = :entity_id')
                ->setParameter('entity_type', 'workshopscheduling')
                ->setParameter('entity_id', $wsId);
        }
            
        if ($ids != '') {
            $queryBuilder
                ->andWhere('pa.evaluated_id IN(' . $ids . ')');
        }
        
        $output = $queryBuilder
            ->getQuery()
            ->getResult();
            
        return $output;
    }

    /**
     * Get List Polls To Evaluated
     */
    public static function getListPollsByProfile(DoctrineRegistry $doctrineRegistry, $wsId = '', $companyId, $ids = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('p.id, p.title')
            ->innerJoin('ps.poll', 'p')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.type <> :type 
                    AND ps.status = 1 
                    AND ps.deleted = 0
                    AND ps.company_id <> 2')
            ->setParameter('type', 'not_evaluateds')
            ->orderBy('p.id', 'ASC')
            ->groupBy('p.id');
            
        if ($wsId != '') {
            $queryBuilder
                ->andWhere('ps.entity_type = :entity_type 
                        AND ps.entity_id = :entity_id')
                ->setParameter('entity_type', 'workshopscheduling')
                ->setParameter('entity_id', $wsId);
        }
            
        if ($ids != '') {
            $queryBuilder
                ->andWhere('pa.evaluated_id IN(' . $ids . ')');
        }
        
        if ($companyId != '') {
            $queryBuilder
                ->andWhere('ps.company_id = :company')
                ->setParameter('company', $companyId);
        }
        
        $output = $queryBuilder
            ->getQuery()
            ->getResult();
            
        return $output;
    }

    /**
     * Get List Pollschedulings To Evaluated
     */
    public static function getListPollschedulingsByProfile(DoctrineRegistry $doctrineRegistry, $wsId = '', $pId, $companyId, $ids = '', $psIds = '')
    {
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.pollscheduling_period period, ps.initdate, ps.enddate')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.poll = :poll 
                    AND ps.type <> :type
                    AND ps.status = 1 
                    AND ps.deleted = 0 
                    AND ps.company_id <> 2')
            ->setParameter('poll', $pId)
            ->setParameter('type', 'not_evaluateds')
            ->orderBy('ps.initdate', 'ASC')
            ->groupBy('ps.id');
            
        if ($wsId != '') {
            $queryBuilder
                ->andWhere('ps.entity_type = :entity_type 
                        AND ps.entity_id = :entity_id')
                ->setParameter('entity_type', 'workshopscheduling')
                ->setParameter('entity_id', $wsId);
        }
            
        if ($ids != '') {
            $queryBuilder
                ->andWhere('pa.evaluated_id IN(' . $ids . ')');
        }
        
        if ($psIds != '') {
            $queryBuilder
                ->andWhere('ps.id IN(' . $psIds . ')');
        }
        
        if ($companyId != '') {
            $queryBuilder
                ->andWhere('ps.company_id = :company')
                ->setParameter('company', $companyId);
        }
        
        $query = $queryBuilder
            ->getQuery()
            ->getResult();

        if (!empty($query)) {
            foreach ($query as $key => $value) {
                $output[$key] = $value;
                $output[$key]['key'] = $key + 1;
            }
        }
        
        return $output;
    }
    
    /**
     * Calcule Percentage Section
     */
    public static function calculatePercentageSection(&$section, $paqs)
    {
        if (!isset($section['percentage']['consolidate'])) {
            $section['percentage']['consolidate'] = '';
        }
        if (!isset($section['percentage']['count'])) {
            $section['percentage']['count'] = '';
        }
        
        if (isset($section['sections'])) {
            $count_parent = 0;
            $consolidate_parent = 0;
            foreach ($section['sections'] as $key_section => $value_section) {
                
                self::calculatePercentageSection($value_section, $paqs);
                
                $consolidate = $value_section['percentage']['consolidate'];
                $count = $value_section['percentage']['count'];
                
                if ($count) {
                    $section['percentage']['consolidate'] = (($consolidate_parent * $count_parent) + ($consolidate * $count)) / ($count_parent + $count);
                }
                else {
                    $section['percentage']['consolidate'] = 0;
                }
                
                $consolidate_parent = $section['percentage']['consolidate'];
                $count_parent += $value_section['percentage']['count'];
                
                $section['percentage']['count'] = $count_parent;
                $section['sections'][$key_section] = $value_section;
            }
        }
        
        if (isset($section['questions'])) {
            foreach ($section['questions'] as $key => $q) {
                $consolidate_parent = $section['percentage']['consolidate'];
                $count_parent = $section['percentage']['count'];
                $section['questions'][$key]['percentage'] = $paqs[$key]['percentage'];
                $section['percentage']['consolidate'] = (($consolidate_parent * $count_parent) + $paqs[$key]['percentage']) / ($count_parent + 1);
                $section['percentage']['count'] = $count_parent + 1;
            }
        }
        
        return $section;
    }

    /**
     * Calcule Percentage Section PTT
     */
    public static function calculatePercentageSectionPTT(&$section, $paqs)
    {
        if (!isset($section['percentage']['consolidate'])) {
            $section['percentage']['consolidate'] = '';
        }
        if (!isset($section['percentage']['count'])) {
            $section['percentage']['count'] = '';
        }
        
        if (isset($section['sections'])) {
            $count_parent = 0;
            $consolidate_parent = 0;
            foreach ($section['sections'] as $key_section => $value_section) {
                
                self::calculatePercentageSectionPTT($value_section, $paqs);
                
                $consolidate = $value_section['percentage']['consolidate'];
                $count = $value_section['percentage']['count'];
                
                if ($count) {
                    $section['percentage']['consolidate'] = (($consolidate_parent * $count_parent) + ($consolidate * $count)) / ($count_parent + $count);
                }
                else {
                    $section['percentage']['consolidate'] = 0;
                }
                
                $consolidate_parent = $section['percentage']['consolidate'];
                $count_parent += $value_section['percentage']['count'];
                
                $section['percentage']['count'] = $count_parent;
                $section['sections'][$key_section] = $value_section;
            }
        }
        
        if (isset($section['questions'])) {
            foreach ($section['questions'] as $key => $q) {
                $consolidate_parent = $section['percentage']['consolidate'];
                $count_parent = $section['percentage']['count'];
                $section['questions'][$key]['percentage'] = $paqs[$key]['percentage'];
                if (in_array($section['questions'][$key]['type'], array('selection_option', 'unique_option'))) {
                    $section['percentage']['consolidate'] = (($consolidate_parent * $count_parent) + $paqs[$key]['percentage']) / ($count_parent + 1);
                    $section['percentage']['count'] = $count_parent + 1;
                }
                else {
                    $section['percentage']['count'] = $count_parent;
                }
            }
        }
        
        return $section;
    }

    /**
     * PercentagePollschedulingWithoutIndentation
     */
    public static function percentagePollschedulingWithoutIndentation($array, $onlySections = FALSE, &$output = '', &$item = 0)
    {   
        if (isset($array)) {
            foreach ($array as $section) {
                
                $output[$item]['percentage'] = $section['percentage']['consolidate'];
                $item++;
                
                if (!$onlySections) {
                    if (isset($section['questions'])) {
                        foreach ($section['questions'] as $question) {
                            $output[$item]['percentage'] = $question['percentage'];
                            $item++;
                        }
                    }
                }
                
                if (isset($section['sections'])) {
                    self::percentagePollschedulingWithoutIndentation($section['sections'], $onlySections, $output, $item);
                }
            }
        }
        
        return $output;
    }

    /**
     * titlePollschedulingWithIndentation
     */
    public static function titlePollschedulingWithIndentation($array, $onlySections = FALSE, &$output = '', &$item = 0, $separator = '')
    {   
        if (isset($array)) {
            foreach ($array as $section) {
                
                $output[$item]['title'] = $separator . '<strong>' . $section['title'] . '</strong>';
                $item++;
                
                if (!$onlySections) {
                    if (isset($section['questions'])) {
                        foreach ($section['questions'] as $question) {
                            $output[$item]['title'] = $separator . '----' . $question['question'];
                            $item++;
                        }
                    }
                }
                
                if (isset($section['sections'])) {
                    $line = $separator . '----';
                    self::TitlePollschedulingWithIndentation($section['sections'], $onlySections, $output, $item, $line);
                }
            }
        }
        
        return $output;
    }

    public static function getPollListOfWorkshop($repository, $workshopscheduling, $first, $typeId)
    {
        $output = '';
        $array_poll = '';
        
        $query = $repository->createQueryBuilder('ps')
         ->where('ps.entity_id = :entity_id
                AND ps.entity_type = :entity_type
                AND ps.initdate IS NOT NULL
                AND ps.enddate IS NOT NULL')
         ->setParameter('entity_type', 'workshopscheduling')
         ->setParameter('entity_id', $workshopscheduling)
         ->orderBy('ps.id', 'ASC')
         ->getQuery();
        $polls = $query->getResult();
        
        $output = '<select class="type_notification type_na" name="fishman_notificationbundle_notificationschedulingtype[notification_type_id]">';
        $output .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        foreach ($polls as $poll) {
            $pid = $poll->getPoll()->getId();
            
            if (!isset($array_poll[$pid])) {
                $array_poll[$pid] = 1;
            }
            else {
                $array_poll[$pid] = $array_poll[$pid] + 1;
            }
            
            if ($first) {
                $name = $poll->getPoll()->getTitle() . ' (' . $array_poll[$pid] . ')';
                $first = false;
            }
            if ($typeId == $poll->getId()) {
                $output .= '<option selected="selected" value="' . $poll->getId() . '">' . $poll->getPoll()->getTitle() . ' (' . $array_poll[$pid] . ')</option>';
            }
            else {
                $output .= '<option value="' . $poll->getId() . '">' . $poll->getPoll()->getTitle() . ' (' . $array_poll[$pid] . ')</option>';
            }
        }
        $output .= '</select>';
  
        return $output;
    }

    /**
     * Get List Pollschedulings by Poll
     */
    public static function getListBenchmarkingPollschedulingsByPoll(DoctrineRegistry $doctrineRegistry, $companyId = '', $pollId = '', $userId, $permission)
    {
        // Recover Pollscheduling where permissions assigned to the user for the company
        $pscIds = Permission::getPollschedulingPermissionAssignedsOfPermissionId($doctrineRegistry, $userId, $permission);
        
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('psc')
            ->select('psc.id, psc.title, psc.pollscheduling_period psc_period')
            ->where('psc.status = 1 
                    AND psc.deleted = 0')
            ->orderBy('psc.pollscheduling_period', 'ASC');
        /*
        $queryBuilder
            ->andWhere('psc.benchmarking <> :benchmarking')
            ->setParameter('benchmarking', '');
        */
        if ($pollId != '') {
            $queryBuilder
                ->andWhere('psc.poll = :poll')
                ->setParameter('poll', $pollId);
        }
        if ($pscIds != '') {
            $queryBuilder
                ->andWhere('psc.id NOT IN(' . $pscIds . ')');
        }
        if ($companyId != '') {
            $queryBuilder
                ->andWhere('psc.company_id = :company')
                ->setParameter('company', $companyId);
        }

        $query = $queryBuilder
            ->getQuery()
            ->getResult();
            
        if (!empty($query)) {
            foreach ($query as $key => $value) {
                $output[$key] = $value;
                $output[$key]['key'] = $key + 1;
            }
        }
        
        return $output;
    }

    /**
     * Get Widget Pollschedulings By Poll
     */
    public static function getWidgetPollschedulingsByPoll(DoctrineRegistry $doctrineRegistry = NULL, $companyId = FALSE, $pollId = FALSE, $selectedIds = FALSE, $userId, $permission)
    {
        $leftOptions = '';
        $rightOptions = '';
        
        if ($selectedIds) {
            
            $pollschedulings = self::getListBenchmarkingPollschedulingsByPoll($doctrineRegistry, $companyId, $pollId, $userId, $permission);
            
            if ($pollschedulings) {
                foreach ($pollschedulings as $psc_key => $psc_value) {
                    $option = FALSE;
                    foreach ($selectedIds as $id) {
                        if ($id == $psc_value['id'] && !$option) {
                            $rightOptions .= '<option value="' . $psc_value['id'] . '">';
                            $rightOptions .= $psc_value['key'] . '. ' . $psc_value['psc_period'];
                            $rightOptions .= '</option>';
                            $option = TRUE;
                            unset($pollschedulings[$psc_key]);
                        }
                    }
                }
                foreach ($pollschedulings as $psc_key => $psc_value) {
                    $leftOptions .= '<option value="' . $psc_value['id'] . '">';
                    $leftOptions .= $psc_value['key'] . '. ' . $psc_value['psc_period'];
                    $leftOptions .= '</option>';
                }
            }
            
        }
        
        $output = self::getWidgetMultipleSelect('pollschedulings', $leftOptions, $rightOptions);
        
        return $output;
    }

    /**
     * Get Widget Pollschedulingrelations By Company
     */
    public static function getListPollschedulingrelationsByCompany(DoctrineRegistry $doctrineRegistry, $companyId, $pollschedulingId = '', $entityType = '', $entityId = '')
    {   
        // Recover Selected Pollscheduling relations
        
        $pscrIds = '';
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilderPscrs = $repository->createQueryBuilder('psc')
            ->select('psc.pollschedulingrelation_id id')
            ->where('psc.company_id = :company
                    AND psc.entity_type = :entity_type 
                    AND psc.pollschedulingrelation_id <> :pollschedulingrelation 
                    AND psc.status = 1')
            ->setParameter('company', $companyId)
            ->setParameter('entity_type', $entityType)
            ->setParameter('pollschedulingrelation', '')
            ->orderBy('psc.id', 'ASC');
            
        if ($pollschedulingId != '') {
            $queryBuilderPscrs
                ->andWhere('psc.id <> :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingId);
        }
        
        $selectedpscrs = $queryBuilderPscrs->getQuery()->getResult();
        
        if (!empty($selectedpscrs)) {
            foreach ($selectedpscrs as $pscr) {
                if ($pscrIds == '') {
                    $pscrIds = $pscr['id'];
                }
                else {
                    $pscrIds .= ', ' . $pscr['id'];
                }
            }
        }
        
        // Recover Pollscheduling relations
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('psc')
            ->select('psc.id, psc.title, psc.pollscheduling_period period')
            ->where('psc.company_id = :company
                    AND psc.entity_type = :entity_type 
                    AND psc.multiple_evaluation = 1 
                    AND psc.status = 1')
            ->setParameter('company', $companyId)
            ->setParameter('entity_type', $entityType)
            ->orderBy('psc.title', 'ASC');
        
        if ($pscrIds != '') {
            $queryBuilder
                ->andWhere('psc.id NOT IN(' . $pscrIds . ')');
        }
        if ($entityId != '') {
            $queryBuilder
                ->andWhere('psc.entity_id = :entity_id')
                ->setParameter('entity_id', $entityId);
        }
        if ($pollschedulingId != '') {
            $queryBuilder
                ->andWhere('psc.id <> :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingId);
        }
        
        $output = $queryBuilder->getQuery()->getResult();
        
        return $output; 
    }

    /**
     * Validate Date in Pollscheduling
     */
    public static function validateDateInPollscheduling(DoctrineRegistry $doctrineRegistry, $initdateObj, $enddateObj, $messageEnd = FALSE)
    {
        $output = array ('valid' => TRUE, 'message' => '');
        
        $now = time();
        $initdate = NULL;
        $enddate = NULL;
        $meesage = '';
        
        if (!is_null($initdateObj)) {
            $initdate = strtotime($initdateObj->format('Y-m-d H:i:s'));
        }

        if (!is_null($enddateObj)) {
            $enddate = strtotime($enddateObj->format('Y-m-d H:i:s')) + (60*60*24);
        }
                
        if ($initdate > $now || $initdate == NULL) {
            $message = 'La encuesta no se ha iniciado.';
        }
        elseif ($enddate < $now) {
            if ($messageEnd) {
                $message = $messageEnd;
            }
            else {
                $message = 'La encuesta ya ha finalizado.';
            }
        }
        
        if (is_null($initdate) || $initdate >= $now || $enddate < $now) {
            $output['valid'] = FALSE;
            $output['message'] = $message;
        }
        
        return $output;
    }

    /**
     * Get Value Benchmarking By Pollschedulings
     */
    public static function getValueBenchmarkingByPollschedulings(DoctrineRegistry $doctrineRegistry, $pscIds)
    {
        $output = TRUE;
        
        if ($pscIds != '') {
            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
            $query = $repository->createQueryBuilder('psc')
                ->where('psc.id IN(' . $pscIds . ') 
                        AND psc.status = 1 
                        AND psc.deleted = 0')
                ->andWhere('psc.benchmarking = :benchmarking 
                        OR psc.benchmarking IS NULL')
                ->setParameter('benchmarking', '')
                ->getQuery()
                ->getResult();
                
            if (!empty($query)) {
                $output = FALSE;
            }
        }
        
        return $output;
    }

    /**
     * Get List Companyes with Pollsxcheduling
     */
    public static function getListCompaniesWithPollscheduling(DoctrineRegistry $doctrineRegistry, $rol, $pscIds = FALSE, $benchmarking = FALSE)
    {
        if (!$pscIds && $benchmarking == 'external') {
            return FALSE;
        }
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('c.id, c.name')
            ->innerJoin('ps.poll', 'p')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.status = 1 
                    AND ps.deleted = 0 
                    AND pa.status = 1
                    AND pa.deleted = 0
                    AND pa.finished = 1')
            ->orderBy('c.id', 'ASC')
            ->groupBy('c.id');
            
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
            $queryBuilder
                ->andWhere('ps.company_id <> 2');
        }

        if ($pscIds != '' || $benchmarking == 'external') {
            $queryBuilder
                ->andWhere('ps.id IN(' . $pscIds . ')');
        }
        
        $output = $queryBuilder->getQuery()->getResult();
            
        return $output;
    }

    /**
     * Get List Polls By company
     */
    public static function getListPollsByCompany(DoctrineRegistry $doctrineRegistry, $rol, $companyId, $pscIds = FALSE)
    {
        $output = array();
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('p.id, p.title')
            ->innerJoin('ps.poll', 'p')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.status = 1 
                    AND ps.deleted = 0
                    AND pa.status = 1
                    AND pa.deleted = 0
                    AND pa.finished = 1
                    AND ps.company_id = :company')
            ->setParameter('company', $companyId)
            ->orderBy('p.id', 'ASC')
            ->groupBy('p.id');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
            $queryBuilder
                ->andWhere('ps.company_id <> 2');
        }

        if ($pscIds != '') {
            $queryBuilder
                ->andWhere('ps.id IN(' . $pscIds . ')');
        }
        
        $output = $queryBuilder->getQuery()->getResult();
            
        return $output;
    }

    /**
     * Get list level options
     * 
     */
    public static function getListLevelOptions() 
    {
        $output = array(
            '1' => 'Nivel 1', 
            '2' => 'Nivel 2', 
            '3' => 'Nivel 3', 
            '4' => 'Nivel 4', 
            '5' => 'Nivel 5', 
            '6' => 'Nivel 6', 
            '7' => 'Nivel 7', 
            '8' => 'Nivel 8', 
            '9' => 'Nivel 9',
            'null' => 'Ninguno' 
        );
        
        return $output;
    }

    /**
     * Get List PollschedulingPeriods By Poll
     */
    public static function getListPollschedulingPeriodsByPoll(DoctrineRegistry $doctrineRegistry, $rol, $pollId, $companyId, $pscIds = FALSE)
    {
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.pollscheduling_period period, ps.initdate, ps.enddate')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'ps.id = pa.pollscheduling')
            ->where('ps.poll = :poll 
                    AND ps.status = 1 
                    AND ps.deleted = 0 
                    AND pa.status = 1
                    AND pa.deleted = 0
                    AND pa.finished = 1
                    AND ps.company_id = :company')
            ->setParameter('poll', $pollId)
            ->setParameter('company', $companyId)
            ->orderBy('ps.pollscheduling_period', 'ASC')
            ->groupBy('ps.id');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
            $queryBuilder
                ->andWhere('ps.company_id <> 2');
        }
        
        if ($pscIds != '') {
            $queryBuilder
                ->andWhere('ps.id IN(' . $pscIds . ')');
        }
        
        $output = $queryBuilder->getQuery()->getResult();
        
        return $output;
    }

    /**
     * Get Widget Polls By Company
     */
    public static function getWidgetCompanies(DoctrineRegistry $doctrineRegistry, $rol, $selectedId = FALSE, $pscIds = FALSE, $benchmarking = FALSE)
    {
        $output = '';
        
        $entities = self::getListCompaniesWithPollscheduling($doctrineRegistry, $rol, $pscIds, $benchmarking);
        
        $output .= '<select name="options_company" id="options_company" required >';
        $output .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                if ($selectedId) {
                    if ($selectedId == $entity['id']) {
                        $output .= '<option value="' . $entity['id'] . '" selected>' . $entity['name'] . '</option>';
                    }
                    else {
                        $output .= '<option value="' . $entity['id'] . '">' . $entity['name'] . '</option>';
                    }
                }
                else {
                    $output .= '<option value="' . $entity['id'] . '">' . $entity['name'] . '</option>';
                }
            }
        }
        $output .= '</select>';
        
        return $output;
    }

    /**
     * Get Widget Polls By Company
     */
    public static function getWidgetPollsByCompany(DoctrineRegistry $doctrineRegistry, $rol, $companyId, $selectedId = FALSE, $pscIds = FALSE)
    {
        $output = '';
        
        $entities = self::getListPollsByCompany($doctrineRegistry, $rol, $companyId, $pscIds);
        
        $output .= '<select name="options_poll" id="options_poll" required >';
        $output .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                if ($selectedId) {
                    if ($selectedId == $entity['id']) {
                        $output .= '<option value="' . $entity['id'] . '" selected>' . $entity['title'] . '</option>';
                    }
                    else {
                        $output .= '<option value="' . $entity['id'] . '">' . $entity['title'] . '</option>';
                    }
                }
                else {
                    $output .= '<option value="' . $entity['id'] . '">' . $entity['title'] . '</option>';
                }
            }
        }
        $output .= '</select>';
        
        return $output;
    }

    /**
     * Get Widget Levels
     */
    public static function getWidgetLevels($selectedId = FALSE)
    {
        $output = '';
        
        $entities = self::getListLevelOptions();
        
        $output .= '<select name="options_level" id="options_level" required >';
        $output .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        if (!empty($entities)) {
            foreach ($entities as $key => $value) {
                if ($selectedId) {
                    if ($selectedId == $key) {
                        $output .= '<option value="' . $key . '" selected>' . $value . '</option>';
                    }
                    else {
                        $output .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }
                else {
                    $output .= '<option value="' . $key . '">' . $value . '</option>';
                }
            }
        }
        $output .= '</select>';
        
        return $output;
    }

    /**
     * Get Widget PollschedulingPeriods By Poll
     */
    public static function getWidgetPollschedulingPeriodsByPoll(DoctrineRegistry $doctrineRegistry, $rol, $pollId, $companyId, $selectedId = FALSE, $pscIds = FALSE)
    {
        $output = '';
        
        $query = self::getListPollschedulingPeriodsByPoll($doctrineRegistry, $rol, $pollId, $companyId, $pscIds);
        
        if (!empty($query)) {
            foreach ($query as $key => $value) {
                $entities[$key] = $value;
                $entities[$key]['key'] = $key + 1;
            }
        }
        
        $output .= '<select name="options_pollscheduling" id="options_pollscheduling" required >';
        $output .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                if ($selectedId) {
                    if ($selectedId == $entity['id']) {
                        $output .= '<option value="' . $entity['id'] . '" selected>' . $entity['key'] . ' - ' . $entity['period'] . '</option>';
                    }
                    else {
                        $output .= '<option value="' . $entity['id'] . '">' . $entity['key'] . ' - ' . $entity['period'] . '</option>';
                    }
                }
                else {
                    $output .= '<option value="' . $entity['id'] . '">' . $entity['key'] . ' - ' . $entity['period'] . '</option>';
                }
            }
        }
        $output .= '</select>';
        
        return $output;
    }

    /**
     * Get Widget Careers By Company
     */
    public static function getWidgetMultipleEvaluatedsByPollscheduling(DoctrineRegistry $doctrineRegistry = NULL, $pscId = FALSE, $selectedIds = FALSE)
    {       
        $entities = Workinginformation::getListEvaluatedsByPollscheduling($doctrineRegistry, $pscId);
        
        $output = self::getWidgetMultipleSelectBuildOptions('evaluateds', $entities, $selectedIds);
        
        return $output;
    }

    /**
     * Get Widget Multiple select build Options
     */
    public static function getWidgetMultipleSelectBuildOptions($widgetId, $entities, $selectedIds)
    {
        $leftOptions = '';
        $rightOptions = '';
        
        if ($entities) {
            foreach ($entities as $key => $value) {
                $option = FALSE;
                if (isset($selectedIds['data2'])) {
                    foreach ($selectedIds['data2'] as $id) {
                        if ($id == $key && !$option) {
                            $rightOptions .= '<option value="' . $key . '">' . $value . '</option>';
                            $option = TRUE;
                            unset($entities[$key]);
                        }
                    }
                }
            }
            foreach ($entities as $key => $value) {
                $leftOptions .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        
        $output = self::getWidgetMultipleSelect($widgetId, $leftOptions, $rightOptions);
        
        return $output;
    }

    /**
     * Get Widget Multiple select
     */
    public static function getWidgetMultipleSelect($widgetId, $leftOptions, $rightOptions)
    {   
        $output = '<div class="leftBox">';
        $output .= '<select name="' . $widgetId . '_options[data1][]" class="multiple selectOptionsData1" multiple="multiple">';
        $output .= $leftOptions;
        $output .= '</select>';
        $output .= '</div>';
        $output .= '<div class="dualControl">';
        $output .= '<button type="button" class="moveToRightOptions dualBtn mr5 mb15">&nbsp;&gt;&nbsp;</button>';
        $output .= '<button type="button" class="moveToAllRightOptions dualBtn mr5 mb15">&nbsp;&gt;&gt;&nbsp;</button>';
        $output .= '<button type="button" class="moveToLeftOptions dualBtn mr5 mb15">&nbsp;&lt;&nbsp;</button>';
        $output .= '<button type="button" class="moveToAllLeftOptions dualBtn mr5 mb15">&nbsp;&lt;&lt;&nbsp;</button>';
        $output .= '</div>';
        $output .= '<div class="rightBox">';
        $output .= '<select name="' . $widgetId . '_options[data2][]" class="multiple selectOptionsData2" multiple="multiple">';
        $output .= $rightOptions;
        $output .= '</select>';
        $output .= '</div>';
        
        return $output;
    }

    /**
     * Get Pollschedulings with Permission by User
     */
    public static function getPollschedulingsWithPermissionByUser(DoctrineRegistry $doctrineRegistry, $userId, $companyId = FALSE, $date = FALSE, $benchmarking = '')
    {
        $output = '';
        
        // Recover Permissions
        $repository = $doctrineRegistry->getRepository('FishmanAuthBundle:Permission');
        $queryBuilder = $repository->createQueryBuilder('pe')
            ->andWhere("pe.user = :user 
                    AND pe.status = 1")
            ->setParameter('user', $userId);
            
        if ($companyId) {
            $queryBuilder
                ->andWhere('pe.company_id = :company')
                ->setParameter('company', $companyId);
        }
        
        if ($date) {
            $queryBuilder
                ->andWhere("pe.initdate <= :datetoinit 
                    AND DATE_ADD(pe.enddate, 1, 'day') >= :datetoend")
                ->setParameter('datetoinit', $date)
                ->setParameter('datetoend', $date);
        }
        
        if ($benchmarking == 'external') { exit;
            $queryBuilder
                ->andWhere('pe.benchmarking_external = 1');
        }
        
        $permissions = $queryBuilder->getQuery()->getResult();
        
        if (!empty($permissions)) {
            $i = 0;
            foreach ($permissions as $pe) {
                foreach ($pe->getPollschedulings() as $psc) {
                    if ($i == 0) {
                        $output = $psc;
                    }
                    else {
                        $output .= ',' . $psc;
                    }
                    $i++;
                }
            }
        }
        
        return $output;
    }

    /**
     * Get Pollschedulings with Benchmarking
     */
    public static function getPollschedulingsWithBenchmarking(DoctrineRegistry $doctrineRegistry)
    {
        $output = '';
        
        // Recover Permissions
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('psc')
            ->andWhere("psc.benchmarking IS NOT NULL 
                    AND psc.deleted = 0 
                    AND psc.status = 1");
                    
        $pollschedulings = $queryBuilder->getQuery()->getResult();
        
        if (!empty($pollschedulings)) {
            $i = 0;
            foreach ($pollschedulings as $psc) {
                if ($i == 0) {
                    $output = $psc->getId();
                }
                else {
                    $output .= ',' . $psc->getId();
                }
                $i++;
            }
        }
        
        return $output;
    }
    
    /**
     * Get Pollschedulings By Benchmarking
     */
    public static function getPollschedulingsByBenchmarking(DoctrineRegistry $doctrineRegistry, $benchmarkingId)
    {
        $output = '';
        
        // Recover Permissions
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('psc')
            ->select('psc.id')
            ->andWhere("psc.benchmarking = :benchmarking 
                    AND psc.deleted = 0 
                    AND psc.status = 1")
            ->setParameter('benchmarking', $benchmarkingId);
                    
        $output = $queryBuilder->getQuery()->getResult();
        
        return $output;
    }
    
    /**
     * Get Report Result Pollschedulings
     */
    public static function getReportResultPollschedulings(DoctrineRegistry $doctrineRegistry, $array, $paqs, $level, $pscId, $pscIds = FALSE)
    {
        $report = self::calculatePercentageSection($array, $paqs);
        
        // Total Rows
        if ($level != 'null') {
            $output['percentages'] = self::percentagePollschedulingWithoutIndentation($report['sections'], TRUE);
        }
        else {
            $output['percentages'] = self::percentagePollschedulingWithoutIndentation($report['sections']);
        }
        
        // Row Results
        $output['average'] = array(
            'percentage' => $report['percentage']['consolidate'], 
            'count' => $report['percentage']['count']
        );
        
        if ($pscIds) {
            $i = 0;
            foreach ($pscIds as $psc) {
                if ($pscId != $psc['id']) {
                    if ($i == 0) {
                        $pscIds = $psc['id'];
                    }
                    else {
                        $pscIds .= ',' . $psc['id'];
                    }
                    $i++;
                }
            }
            
            // Recover Number Evaluators by Evaluated
            $output['number_evaluators'] = Pollapplication::getNumberEvaluatorsByEvaluated($doctrineRegistry, $pscIds, FALSE, FALSE, TRUE);
        }
        else {
            // Recover Number Evaluators by Evaluated
            $output['number_evaluators'] = Pollapplication::getNumberEvaluatorsByEvaluated($doctrineRegistry, $pscId);
        }
        
        // Recover Report
        $output['report'] = $report;
        
        return $output;
    }
    
    /**
     * Get PollschedulingIds By Poll
     */
    public static function getPollschedulingIdsByPoll(DoctrineRegistry $doctrineRegistry, $pId, $companyId)
    {
        $output = '';
        
        // Recover Pollschedulings
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('psc')
            ->select('psc.id')
            ->andWhere("psc.poll = :poll 
                    AND psc.company_id = :company
                    AND psc.deleted = 0 
                    AND psc.status = 1")
            ->setParameter('poll', $pId)
            ->setParameter('company', $companyId);
                    
        $query = $queryBuilder->getQuery()->getResult();
        
        $i = 0;
        foreach ($query as $psc) {
            if ($i == 0) {
                $output = $psc['id'];
            }
            else {
                $output .= ',' . $psc['id'];
            }
            $i++;
        }
        
        return $output;
    }
    
    /**
     * Delete temporal files
     * 
     */
    public static function deleteTemporalFiles($directory)
    {
        $dir = opendir($directory);
        while ($file = readdir($dir)) {
            if (is_file($file)) {
                unlink($directory . $file);
            }
        }
    }

    /**
     * Temporal function to recreate serialized
     */
    public static function recreateSerializedToPollscheduling(DoctrineRegistry $doctrineRegistry, $psId)
    {
        $em = $doctrineRegistry->getManager();
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($psId);
        
        // Created array Sections and Questions
        $pollArray['sections'] = Pollschedulingsection::arraySectionsAndQuestions($doctrineRegistry, $entity->getId());
        
        // Add Serialized
        $entity->setSerialized($pollArray['sections']);
        $em->persist($entity);
        $em->flush();
    }
    
    /**
     * get Poll List Links
     */
    public static function getPollListLinks(DoctrineRegistry $doctrine, $router, $psEntity)
    {
        $poll = array();
        
        if (!$psEntity->getPollschedulingAnonymous()) {
            if( $psEntity->getEntityType() == 'workshopscheduling') {
                $queryBuilder = $doctrine->getRepository('FishmanWorkshopBundle:Workshopapplication')
                                      ->createQueryBuilder('wsa')
                                      ->select('wi.code wi_code, wi.email wi_email, u.email, u.username, 
                                              u.names, u.surname, u.lastname, wsa.personaltoken')
                                      ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'pa.entity_application_id = wsa.id')
                                      ->innerJoin('wsa.workinginformation', 'wi')
                                      ->innerJoin('wi.user', 'u')
                                      ->where('pa.pollscheduling = :pollschedulingid 
                                               AND pa.deleted = 0')
                                      ->setParameter('pollschedulingid', $psEntity->getId())
                                      ->groupBy('wi.id');
            }
            else {
                $queryBuilder = $doctrine->getRepository('FishmanPollBundle:Pollschedulingpeople')
                                      ->createQueryBuilder('psp')
                                      ->select('wi.code wi_code, wi.email wi_email, u.email, u.username, 
                                              u.names, u.surname, u.lastname, psp.personaltoken')
                                      ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'pa.entity_application_id = psp.id')
                                      ->innerJoin('psp.workinginformation', 'wi')
                                      ->innerJoin('wi.user', 'u')
                                      ->where('pa.pollscheduling = :pollschedulingid 
                                               AND pa.deleted = 0')
                                      ->setParameter('pollschedulingid', $psEntity->getId())
                                      ->groupBy('wi.id');
            }
            
            if ($psEntity->getType() != 'not_evaluateds') {
                $queryBuilder
                    ->andWhere('pa.evaluated_id > 0');
            }
            else {
                $queryBuilder
                    ->andWhere('pa.evaluated_id IS NULL');
            }
            
            $people = $queryBuilder->getQuery()->getResult();
            
            if ($people) {
                $poll['anonymous'] = FALSE;
                $i = 0;
                foreach ($people as $p) {
                    $poll['persons'][$i]['user'] = $p['username'];
                    $poll['persons'][$i]['names'] = $p['names'] . ' ' . $p['surname'] . ' ' . $p['lastname'];
                    $poll['persons'][$i]['email'] = $p['email'];
                    $poll['persons'][$i]['work_code'] = $p['wi_code'];
                    $poll['persons'][$i]['work_email'] = $p['wi_email'];
                    $poll['persons'][$i]['poll_link'] = $router->generate('fishman_front_end_poll_evaluateds',
                                    array('pollid' => $psEntity->getId(), 'personaltoken' => $p['personaltoken']), true);
                    $i++;
                }
            }

        }
        else {
          
            $anonymous = $doctrine->getRepository('FishmanPollBundle:Pollschedulingpeople')
                                  ->createQueryBuilder('psp')
                                  ->select('psp.personaltoken')
                                  ->where('psp.pollscheduling = :pollschedulingid')
                                  ->setParameter('pollschedulingid', $psEntity->getId())
                                  ->getQuery()
                                  ->getSingleResult();
            
            if ($anonymous) {
                $poll['anonymous'] = TRUE;
                $poll['persons']['poll_link'] = $router->generate('fishman_front_end_poll_anonymous',
                                array('pollid' => $psEntity->getId(), 'personaltoken' => $anonymous['personaltoken']), true);
            }
            
        }
        
        return $poll;
    }
    
    /*
     * Generate Report Path Evaluations
     * 
     */
    public static function generateReportPathEvaluations($pathEvaluations, &$worksheet, &$phpExcel, $company, $row = FALSE)
    {   
        if (!$row) {
            $row = 6;
        }
        
        if (!empty($pathEvaluations)) {
            
            if (!$pathEvaluations['anonymous']) {
                
                $i = 1;
                
                $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Usuario');
                $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Nombres y apellidos');
                $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Correo personal');
                if ($company->getCompanyType() == 'working') {
                    $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Código Laboral');
                    $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Correo Laboral');
                }
                elseif ($company->getCompanyType() == 'university') {
                    $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Código Universidad');
                    $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Correo Universidad');
                }
                elseif ($company->getCompanyType() == 'school') {
                    $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Código Colegio');
                    $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Correo Colegio');
                }
                $worksheet->getCellByColumnAndRow($i++, $row-1)->setValue('Enlace');
                
                $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
                $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
                $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(130);
                
                foreach ($pathEvaluations['persons'] as $pe) {
                    $i = 1;
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($pe['user']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($pe['names']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($pe['email']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($pe['work_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($pe['work_email']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($pe['poll_link']);
                    $row++;
                }
                
            }
            else {
                $worksheet->getCellByColumnAndRow(1, $row-1)->setValue('Enlace');
                
                $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(130);
                
                if (!$row) {
                    $row = 6;
                }
                    
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($pathEvaluations['poll_link']);
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, $row)->setValue('No hay registros que mostrar');
        }
    }
    
    /**
     * Array Report Percentage
     */
    public static function getArrayReportPercentage(&$section, $paqs)
    {
        if (!isset($section['percentage']['consolidate'])) {
            $section['percentage']['consolidate'] = '';
        }
        if (!isset($section['percentage']['count'])) {
            $section['percentage']['count'] = '';
        }
        
        if (isset($section['sections'])) {
            $count_parent = 0;
            $consolidate_parent = 0;
            foreach ($section['sections'] as $key_section => $value_section) {
                
                self::getArrayReportPercentage($value_section, $paqs);
                
                $consolidate = $value_section['percentage']['consolidate'];
                $count = $value_section['percentage']['count'];
                
                if ($count) {
                    $section['percentage']['consolidate'] = (($consolidate_parent * $count_parent) + ($consolidate * $count)) / ($count_parent + $count);
                }
                else {
                    $section['percentage']['consolidate'] = 0;
                }
                
                $consolidate_parent = $section['percentage']['consolidate'];
                $count_parent += $value_section['percentage']['count'];
                
                $section['percentage']['count'] = $count_parent;
                $section['sections'][$key_section] = $value_section;
            }
        }
        
        if (isset($section['questions'])) {
            foreach ($section['questions'] as $key => $q) {
                $question = Pollapplicationreport::getPollapplicationquestionReportData($paqs, $key);
                unset($question['pq_id']);
                $consolidate_parent = $section['percentage']['consolidate'];
                $count_parent = $section['percentage']['count'];
                $section['questions'][$key] = $question;
                $section['percentage']['consolidate'] = (($consolidate_parent * $count_parent) + $question['result_score']) / ($count_parent + 1);
                $section['percentage']['count'] = $count_parent + 1;
            }
        }
        
        return $section;
    }

    /**
     * getArrayReportCorrelative
     */
    public static function getArrayReportCorrelative($array)
    {
        $output = '';

        if (isset($array['sections'])) {
            $i = 0;
            foreach ($array['sections'] as $section) {
                $value = self::getArrayReportCorrelative($section);
                $output['sections'][$i]['title'] = $section['title'];
                $output['sections'][$i]['description'] = $section['description'];
                if (isset($value['sections'])) {
                    $output['sections'][$i]['sections'] = $value['sections'];
                }
                elseif (isset($value['questions'])) {
                    $output['sections'][$i]['questions'] = $value['questions'];
                }
                $i++;
            }
        }
        
        if (isset($array['questions'])) {
            $j = 0;
            foreach ($array['questions'] as $question) {
                $output['questions'][$j] = $question;
                $j++;
            }
        }
        
        return $output;
    }
    
}