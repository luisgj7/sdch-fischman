<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\PollBundle\Entity\Pollapplicationreport
 */
class Pollapplicationreport
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $pollscheduling_id
     */
    private $pollscheduling_id;

    /**
     * @var integer $pollapplication_id
     */
    private $pollapplication_id;

    /**
     * @var integer $pollapplicationquestion_id
     */
    private $pollapplicationquestion_id;

    /**
     * @var integer $company_id
     */
    private $company_id;

    /**
     * @var string $company
     */
    private $company;

    /**
     * @var integer $headquarter_id
     */
    private $headquarter_id;

    /**
     * @var integer $headquarter_code
     */
    private $headquarter_code;

    /**
     * @var string $headquarter
     */
    private $headquarter;

    /**
     * @var integer $companyorganizationalunit_id
     */
    private $companyorganizationalunit_id;

    /**
     * @var integer $companyorganizationalunit_code
     */
    private $companyorganizationalunit_code;

    /**
     * @var string $companyorganizationalunit
     */
    private $companyorganizationalunit;

    /**
     * @var integer $companyorganizationalunit_parent_id
     */
    private $companyorganizationalunit_parent_id;

    /**
     * @var integer $companyorganizationalunit_parent_code
     */
    private $companyorganizationalunit_parent_code;

    /**
     * @var string $companyorganizationalunit_parent
     */
    private $companyorganizationalunit_parent;

    /**
     * @var integer $companycharge_id
     */
    private $companycharge_id;

    /**
     * @var integer $companycharge_code
     */
    private $companycharge_code;

    /**
     * @var string $companycharge
     */
    private $companycharge;

    /**
     * @var integer $companyfaculty_id
     */
    private $companyfaculty_id;

    /**
     * @var integer $companyfaculty_code
     */
    private $companyfaculty_code;

    /**
     * @var string $companyfaculty
     */
    private $companyfaculty;

    /**
     * @var integer $companycareer_id
     */
    private $companycareer_id;

    /**
     * @var integer $companycareer_code
     */
    private $companycareer_code;

    /**
     * @var string $companycareer
     */
    private $companycareer;

    /**
     * @var integer $companycourse_id
     */
    private $companycourse_id;

    /**
     * @var integer $companycourse_code
     */
    private $companycourse_code;

    /**
     * @var string $companycourse
     */
    private $companycourse;

    /**
     * @var \DateTime $datein
     */
    private $datein;

    /**
     * @var \DateTime $dateout
     */
    private $dateout;

    /**
     * @var string $grade
     */
    private $grade;

    /**
     * @var string $study_year
     */
    private $study_year;

    /**
     * @var string $academic_year
     */
    private $academic_year;

    /**
     * @var integer $study_section
     */
    private $study_section;

    /**
     * @var integer $user_id
     */
    private $user_id;

    /**
     * @var integer $evaluated_id
     */
    private $evaluated_id;

    /**
     * @var integer $evaluated_code
     */
    private $evaluated_code;

    /**
     * @var string $names
     */
    private $names;

    /**
     * @var boolean $sex
     */
    private $sex;

    /**
     * @var \Date $birthday
     */
    private $birthday;

    /**
     * @var string $birthplace
     */
    private $birthplace;

    /**
     * @var string $marital_status
     */
    private $marital_status;

    /**
     * @var integer $evaluator_id
     */
    private $evaluator_id;

    /**
     * @var integer $evaluator_code
     */
    private $evaluator_code;

    /**
     * @var string $evaluator_name
     */
    private $evaluator_name;

    /**
     * @var integer $section_id
     */
    private $section_id;

    /**
     * @var string $section
     */
    private $section;

    /**
     * @var integer $section_parent_id
     */
    private $section_parent_id;

    /**
     * @var string $section_parent
     */
    private $section_parent;

    /**
     * @var integer $question_number
     */
    private $question_number;

    /**
     * @var string $question
     */
    private $question;

    /**
     * @var integer $sequence
     */
    private $sequence;

    /**
     * @var string $answer
     */
    private $answer;

    /**
     * @var integer $score
     */
    private $score;

    /**
     * @var integer $ceil_score
     */
    private $ceil_score;

    /**
     * @var string $result_score
     */
    private $result_score;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pollscheduling_id
     *
     * @param integer $pollschedulingId
     * @return Pollapplicationreport
     */
    public function setPollschedulingId($pollschedulingId)
    {
        $this->pollscheduling_id = $pollschedulingId;
    
        return $this;
    }

    /**
     * Get pollscheduling_id
     *
     * @return integer 
     */
    public function getPollschedulingId()
    {
        return $this->pollscheduling_id;
    }

    /**
     * Set pollapplication_id
     *
     * @param integer $pollapplicationId
     * @return Pollapplicationreport
     */
    public function setPollapplicationId($pollapplicationId)
    {
        $this->pollapplication_id = $pollapplicationId;
    
        return $this;
    }

    /**
     * Get pollapplication_id
     *
     * @return integer 
     */
    public function getPollapplicationId()
    {
        return $this->pollapplication_id;
    }

    /**
     * Set pollapplicationquestion_id
     *
     * @param integer $pollapplicationquestionId
     * @return Pollapplicationreport
     */
    public function setPollapplicationquestionId($pollapplicationquestionId)
    {
        $this->pollapplicationquestion_id = $pollapplicationquestionId;
    
        return $this;
    }

    /**
     * Get pollapplicationquestion_id
     *
     * @return integer 
     */
    public function getPollapplicationquestionId()
    {
        return $this->pollapplicationquestion_id;
    }

    /**
     * Set company_id
     *
     * @param integer $companyId
     * @return Pollapplicationreport
     */
    public function setCompanyId($companyId)
    {
        $this->company_id = $companyId;
    
        return $this;
    }

    /**
     * Get company_id
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Pollapplicationreport
     */
    public function setCompany($company)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set companyorganizationalunit_id
     *
     * @param integer $companyorganizationalunitId
     * @return Pollapplicationreport
     */
    public function setCompanyorganizationalunitId($companyorganizationalunitId)
    {
        $this->companyorganizationalunit_id = $companyorganizationalunitId;
    
        return $this;
    }

    /**
     * Get companyorganizationalunit_id
     *
     * @return integer 
     */
    public function getCompanyorganizationalunitId()
    {
        return $this->companyorganizationalunit_id;
    }

    /**
     * Set companyorganizationalunit
     *
     * @param string $companyorganizationalunit
     * @return Pollapplicationreport
     */
    public function setCompanyorganizationalunit($companyorganizationalunit)
    {
        $this->companyorganizationalunit = $companyorganizationalunit;
    
        return $this;
    }

    /**
     * Get companyorganizationalunit
     *
     * @return string 
     */
    public function getCompanyorganizationalunit()
    {
        return $this->companyorganizationalunit;
    }

    /**
     * Set companyorganizationalunit_parent_id
     *
     * @param integer $companyorganizationalunitParentId
     * @return Pollapplicationreport
     */
    public function setCompanyorganizationalunitParentId($companyorganizationalunitParentId)
    {
        $this->companyorganizationalunit_parent_id = $companyorganizationalunitParentId;
    
        return $this;
    }

    /**
     * Get companyorganizationalunit_parent_id
     *
     * @return integer 
     */
    public function getCompanyorganizationalunitParentId()
    {
        return $this->companyorganizationalunit_parent_id;
    }

    /**
     * Set companyorganizationalunit_parent
     *
     * @param string $companyorganizationalunitParent
     * @return Pollapplicationreport
     */
    public function setCompanyorganizationalunitParent($companyorganizationalunitParent)
    {
        $this->companyorganizationalunit_parent = $companyorganizationalunitParent;
    
        return $this;
    }

    /**
     * Get companyorganizationalunit_parent
     *
     * @return string 
     */
    public function getCompanyorganizationalunitParent()
    {
        return $this->companyorganizationalunit_parent;
    }

    /**
     * Set companycharge_id
     *
     * @param integer $companychargeId
     * @return Pollapplicationreport
     */
    public function setCompanychargeId($companychargeId)
    {
        $this->companycharge_id = $companychargeId;
    
        return $this;
    }

    /**
     * Get companycharge_id
     *
     * @return integer 
     */
    public function getCompanychargeId()
    {
        return $this->companycharge_id;
    }

    /**
     * Set companycharge
     *
     * @param string $companycharge
     * @return Pollapplicationreport
     */
    public function setCompanycharge($companycharge)
    {
        $this->companycharge = $companycharge;
    
        return $this;
    }

    /**
     * Get companycharge
     *
     * @return string 
     */
    public function getCompanycharge()
    {
        return $this->companycharge;
    }

    /**
     * Set companyfaculty_id
     *
     * @param integer $companyfacultyId
     * @return Pollapplicationreport
     */
    public function setCompanyfacultyId($companyfacultyId)
    {
        $this->companyfaculty_id = $companyfacultyId;
    
        return $this;
    }

    /**
     * Get companyfaculty_id
     *
     * @return integer 
     */
    public function getCompanyfacultyId()
    {
        return $this->companyfaculty_id;
    }

    /**
     * Set companyfaculty_code
     *
     * @param string $companyfacultyCode
     * @return Pollapplicationreport
     */
    public function setCompanyfacultyCode($companyfacultyCode)
    {
        $this->companyfaculty_code = $companyfacultyCode;
    
        return $this;
    }

    /**
     * Get companyfaculty_code
     *
     * @return string 
     */
    public function getCompanyfacultyCode()
    {
        return $this->companyfaculty_code;
    }

    /**
     * Set companyfaculty
     *
     * @param string $companyfaculty
     * @return Pollapplicationreport
     */
    public function setCompanyfaculty($companyfaculty)
    {
        $this->companyfaculty = $companyfaculty;
    
        return $this;
    }

    /**
     * Get companyfaculty
     *
     * @return string 
     */
    public function getCompanyfaculty()
    {
        return $this->companyfaculty;
    }

    /**
     * Set companycareer_id
     *
     * @param integer $companycareerId
     * @return Pollapplicationreport
     */
    public function setCompanycareerId($companycareerId)
    {
        $this->companycareer_id = $companycareerId;
    
        return $this;
    }

    /**
     * Get companycareer_id
     *
     * @return integer 
     */
    public function getCompanycareerId()
    {
        return $this->companycareer_id;
    }

    /**
     * Set companycareer_code
     *
     * @param string $companycareerCode
     * @return Pollapplicationreport
     */
    public function setCompanycareerCode($companycareerCode)
    {
        $this->companycareer_code = $companycareerCode;
    
        return $this;
    }

    /**
     * Get companycareer_code
     *
     * @return string 
     */
    public function getCompanycareerCode()
    {
        return $this->companycareer_code;
    }

    /**
     * Set companycareer
     *
     * @param string $companycareer
     * @return Pollapplicationreport
     */
    public function setCompanycareer($companycareer)
    {
        $this->companycareer = $companycareer;
    
        return $this;
    }

    /**
     * Get companycareer
     *
     * @return string 
     */
    public function getCompanycareer()
    {
        return $this->companycareer;
    }

    /**
     * Set companycourse_id
     *
     * @param integer $companycourseId
     * @return Pollapplicationreport
     */
    public function setCompanycourseId($companycourseId)
    {
        $this->companycourse_id = $companycourseId;
    
        return $this;
    }

    /**
     * Get companycourse_id
     *
     * @return integer 
     */
    public function getCompanycourseId()
    {
        return $this->companycourse_id;
    }

    /**
     * Set companycourse_code
     *
     * @param string $companycourseCode
     * @return Pollapplicationreport
     */
    public function setCompanycourseCode($companycourseCode)
    {
        $this->companycourse_code = $companycourseCode;
    
        return $this;
    }

    /**
     * Get companycourse_code
     *
     * @return string 
     */
    public function getCompanycourseCode()
    {
        return $this->companycourse_code;
    }

    /**
     * Set companycourse
     *
     * @param string $companycourse
     * @return Pollapplicationreport
     */
    public function setCompanycourse($companycourse)
    {
        $this->companycourse = $companycourse;
    
        return $this;
    }

    /**
     * Get companycourse
     *
     * @return string 
     */
    public function getCompanycourse()
    {
        return $this->companycourse;
    }

    /**
     * Set datein
     *
     * @param \DateTime $datein
     * @return Pollapplicationreport
     */
    public function setDatein($datein)
    {
        $this->datein = $datein;
    
        return $this;
    }

    /**
     * Get datein
     *
     * @return \DateTime 
     */
    public function getDatein()
    {
        return $this->datein;
    }

    /**
     * Set dateout
     *
     * @param \DateTime $dateout
     * @return Pollapplicationreport
     */
    public function setDateout($dateout)
    {
        $this->dateout = $dateout;
    
        return $this;
    }

    /**
     * Get dateout
     *
     * @return \DateTime 
     */
    public function getDateout()
    {
        return $this->dateout;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return Pollapplicationreport
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
    
        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set evaluated_id
     *
     * @param integer $evaluatedId
     * @return Pollapplicationreport
     */
    public function setEvaluatedId($evaluatedId)
    {
        $this->evaluated_id = $evaluatedId;
    
        return $this;
    }

    /**
     * Get evaluated_id
     *
     * @return integer 
     */
    public function getEvaluatedId()
    {
        return $this->evaluated_id;
    }

    /**
     * Set names
     *
     * @param string $names
     * @return Pollapplicationreport
     */
    public function setNames($names)
    {
        $this->names = $names;
    
        return $this;
    }

    /**
     * Get names
     *
     * @return string 
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * Set sex
     *
     * @param boolean $sex
     * @return Pollapplicationreport
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    
        return $this;
    }

    /**
     * Get sex
     *
     * @return boolean 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set birthday
     *
     * @param \Date $birthday
     * @return Pollapplicationreport
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \Date 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set birthplace
     *
     * @param string $birthplace
     * @return Pollapplicationreport
     */
    public function setBirthplace($birthplace)
    {
        $this->birthplace = $birthplace;
    
        return $this;
    }

    /**
     * Get birthplace
     *
     * @return string 
     */
    public function getBirthplace()
    {
        return $this->birthplace;
    }

    /**
     * Set marital_status
     *
     * @param string $maritalStatus
     * @return Pollapplicationreport
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->marital_status = $maritalStatus;
    
        return $this;
    }

    /**
     * Get marital_status
     *
     * @return string 
     */
    public function getMaritalStatus()
    {
        return $this->marital_status;
    }

    /**
     * Set evaluator_id
     *
     * @param integer $evaluatorId
     * @return Pollapplicationreport
     */
    public function setEvaluatorId($evaluatorId)
    {
        $this->evaluator_id = $evaluatorId;
    
        return $this;
    }

    /**
     * Get evaluator_id
     *
     * @return integer 
     */
    public function getEvaluatorId()
    {
        return $this->evaluator_id;
    }

    /**
     * Set evaluator_name
     *
     * @param string $evaluatorName
     * @return Pollapplicationreport
     */
    public function setEvaluatorName($evaluatorName)
    {
        $this->evaluator_name = $evaluatorName;
    
        return $this;
    }

    /**
     * Get evaluator_name
     *
     * @return string 
     */
    public function getEvaluatorName()
    {
        return $this->evaluator_name;
    }

    /**
     * Set section_id
     *
     * @param integer $sectionId
     * @return Pollapplicationreport
     */
    public function setSectionId($sectionId)
    {
        $this->section_id = $sectionId;
    
        return $this;
    }

    /**
     * Get section_id
     *
     * @return integer 
     */
    public function getSectionId()
    {
        return $this->section_id;
    }

    /**
     * Set section
     *
     * @param string $section
     * @return Pollapplicationreport
     */
    public function setSection($section)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set section_parent_id
     *
     * @param integer $sectionParentId
     * @return Pollapplicationreport
     */
    public function setSectionParentId($sectionParentId)
    {
        $this->section_parent_id = $sectionParentId;
    
        return $this;
    }

    /**
     * Get section_parent_id
     *
     * @return integer 
     */
    public function getSectionParentId()
    {
        return $this->section_parent_id;
    }

    /**
     * Set section_parent
     *
     * @param string $sectionParent
     * @return Pollapplicationreport
     */
    public function setSectionParent($sectionParent)
    {
        $this->section_parent = $sectionParent;
    
        return $this;
    }

    /**
     * Get section_parent
     *
     * @return string 
     */
    public function getSectionParent()
    {
        return $this->section_parent;
    }

    /**
     * Set question_number
     *
     * @param integer $questionNumber
     * @return Pollapplicationreport
     */
    public function setQuestionNumber($questionNumber)
    {
        $this->question_number = $questionNumber;
    
        return $this;
    }

    /**
     * Get question_number
     *
     * @return integer 
     */
    public function getQuestionNumber()
    {
        return $this->question_number;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Pollapplicationreport
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Pollapplicationreport
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return Pollapplicationreport
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return Pollapplicationreport
     */
    public function setScore($score)
    {
        $this->score = $score;
    
        return $this;
    }

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set companyorganizationalunit_code
     *
     * @param string $companyorganizationalunitCode
     * @return Pollapplicationreport
     */
    public function setCompanyorganizationalunitCode($companyorganizationalunitCode)
    {
        $this->companyorganizationalunit_code = $companyorganizationalunitCode;
    
        return $this;
    }

    /**
     * Get companyorganizationalunit_code
     *
     * @return string 
     */
    public function getCompanyorganizationalunitCode()
    {
        return $this->companyorganizationalunit_code;
    }

    /**
     * Set companyorganizationalunit_parent_code
     *
     * @param string $companyorganizationalunitParentCode
     * @return Pollapplicationreport
     */
    public function setCompanyorganizationalunitParentCode($companyorganizationalunitParentCode)
    {
        $this->companyorganizationalunit_parent_code = $companyorganizationalunitParentCode;
    
        return $this;
    }

    /**
     * Get companyorganizationalunit_parent_code
     *
     * @return string 
     */
    public function getCompanyorganizationalunitParentCode()
    {
        return $this->companyorganizationalunit_parent_code;
    }

    /**
     * Set companycharge_code
     *
     * @param string $companychargeCode
     * @return Pollapplicationreport
     */
    public function setCompanychargeCode($companychargeCode)
    {
        $this->companycharge_code = $companychargeCode;
    
        return $this;
    }

    /**
     * Get companycharge_code
     *
     * @return string 
     */
    public function getCompanychargeCode()
    {
        return $this->companycharge_code;
    }

    /**
     * Set evaluated_code
     *
     * @param string $evaluatedCode
     * @return Pollapplicationreport
     */
    public function setEvaluatedCode($evaluatedCode)
    {
        $this->evaluated_code = $evaluatedCode;
    
        return $this;
    }

    /**
     * Get evaluated_code
     *
     * @return string 
     */
    public function getEvaluatedCode()
    {
        return $this->evaluated_code;
    }

    /**
     * Set evaluator_code
     *
     * @param string $evaluatorCode
     * @return Pollapplicationreport
     */
    public function setEvaluatorCode($evaluatorCode)
    {
        $this->evaluator_code = $evaluatorCode;
    
        return $this;
    }

    /**
     * Get evaluator_code
     *
     * @return string 
     */
    public function getEvaluatorCode()
    {
        return $this->evaluator_code;
    }

    /**
     * Set headquarter_id
     *
     * @param integer $headquarterId
     * @return Pollapplicationreport
     */
    public function setHeadquarterId($headquarterId)
    {
        $this->headquarter_id = $headquarterId;
    
        return $this;
    }

    /**
     * Get headquarter_id
     *
     * @return integer 
     */
    public function getHeadquarterId()
    {
        return $this->headquarter_id;
    }

    /**
     * Set headquarter_code
     *
     * @param string $headquarterCode
     * @return Pollapplicationreport
     */
    public function setHeadquarterCode($headquarterCode)
    {
        $this->headquarter_code = $headquarterCode;
    
        return $this;
    }

    /**
     * Get headquarter_code
     *
     * @return string 
     */
    public function getHeadquarterCode()
    {
        return $this->headquarter_code;
    }

    /**
     * Set headquarter
     *
     * @param string $headquarter
     * @return Pollapplicationreport
     */
    public function setHeadquarter($headquarter)
    {
        $this->headquarter = $headquarter;
    
        return $this;
    }

    /**
     * Get headquarter
     *
     * @return string 
     */
    public function getHeadquarter()
    {
        return $this->headquarter;
    }

    /**
     * Set grade
     *
     * @param string $grade
     * @return Pollapplicationreport
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    
        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set study_section
     *
     * @param string $studySection
     * @return Pollapplicationreport
     */
    public function setStudySection($studySection)
    {
        $this->study_section = $studySection;
    
        return $this;
    }

    /**
     * Get study_section
     *
     * @return string 
     */
    public function getStudySection()
    {
        return $this->study_section;
    }

    /**
     * Set study_year
     *
     * @param string $studyYear
     * @return Pollapplicationreport
     */
    public function setStudyYear($studyYear)
    {
        $this->study_year = $studyYear;
    
        return $this;
    }

    /**
     * Get study_year
     *
     * @return string 
     */
    public function getStudyYear()
    {
        return $this->study_year;
    }

    /**
     * Set academic_year
     *
     * @param string $academicYear
     * @return Pollapplicationreport
     */
    public function setAcademicYear($academicYear)
    {
        $this->academic_year = $academicYear;
    
        return $this;
    }

    /**
     * Get academic_year
     *
     * @return string 
     */
    public function getAcademicYear()
    {
        return $this->academic_year;
    }

    /**
     * Set ceil_score
     *
     * @param integer $ceilScore
     * @return Pollapplicationreport
     */
    public function setCeilScore($ceilScore)
    {
        $this->ceil_score = $ceilScore;
    
        return $this;
    }

    /**
     * Get ceil_score
     *
     * @return integer 
     */
    public function getCeilScore()
    {
        return $this->ceil_score;
    }

    /**
     * Set result_score
     *
     * @param string $resultScore
     * @return Pollapplicationreport
     */
    public function setResultScore($resultScore)
    {
        $this->result_score = $resultScore;
    
        return $this;
    }

    /**
     * Get result_score
     *
     * @return string 
     */
    public function getResultScore()
    {
        return $this->result_score;
    }

    /**
     * cloneRegisters
     */
    public static function cloneRegisters(DoctrineRegistry $doctrineRegistry, $pollapplicationid)
    {   
        $em = $doctrineRegistry->getManager();
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $results = $repository->createQueryBuilder('paq')
            ->select('psc.id pollscheduling_id, pa.id pollapplication_id, paq.id pollapplicationquestion_id, 
                      psq.type, psq.options, psc.company_id, c.name company_name, c.company_type, 
                      h.id headquarter_id, h.code headquarter_code, h.name headquarter_name, 
                      co.id companyorganizationalunit_id, co.code companyorganizationalunit_code, co.name companyorganizationalunit_name, 
                      cop.id companyorganizationalunit_parent_id, cop.code companyorganizationalunit_parent_code, cop.name companyorganizationalunit_parent_name, 
                      cch.id companycharge_id, cch.code companycharge_code, cch.name companycharge_name,  
                      cf.id companyfaculty_id, cf.code companyfaculty_code, cf.name companyfaculty_name,  
                      ccr.id companycareer_id, ccr.code companycareer_code, ccr.name companycareer_name,  
                      ccs.id companycourse_id, ccs.code companycourse_code, ccs.name companycourse_name, 
                      u.id user_id, u.sex, u.birthday, u.birthplace, u.marital_status, 
                      wi.datein, wi.dateout, wi.id evaluated_id, wi.grade, wi.study_year, wi.academic_year, pa.section study_section, 
                      wi.code evaluated_code, u.names evaluated_names, u.surname evaluated_surname, u.lastname evaluated_lastname, 
                      wie.id evaluator_id, wie.code evaluator_code, ue.names evaluator_names, ue.surname evaluator_surname, ue.lastname evaluator_lastname, 
                      pss.id section_id, pss.name section_name, pssp.id section_parent_id, pssp.name section_parent_name, 
                      psq.sequence question_number, psq.question, psq.original_sequence sequence, paq.answer, paq.answer_options, 
                      paq.score, psq.ceil_score')
            ->innerJoin('paq.pollapplication', 'pa')
            ->innerJoin('pa.pollscheduling', 'psc')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'psc.company_id = c.id')
            ->leftJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->leftJoin('wi.user', 'u')
            ->leftJoin('wi.headquarter', 'h')
            ->leftJoin('wi.companyorganizationalunit', 'co')
            ->leftJoin('FishmanEntityBundle:Companyorganizationalunit', 'cop', 'WITH', 'co.parent_id = cop.id')
            ->leftJoin('wi.companycharge', 'cch')
            ->leftJoin('wi.companyfaculty', 'cf')
            ->leftJoin('wi.companycareer', 'ccr')
            ->leftJoin('FishmanEntityBundle:Companycourse', 'ccs', 'WITH', 'pa.course = ccs.id')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wie', 'WITH', 'pa.evaluator_id = wie.id')
            ->innerJoin('wie.user', 'ue')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->leftJoin('psq.pollschedulingsection', 'pss')
            ->leftJoin('FishmanPollBundle:Pollschedulingsection', 'pssp', 'WITH', 'pss.parent_id = pssp.id')
            ->where('pa.id = :pollapplication')
            ->setParameter('pollapplication', $pollapplicationid)
            ->getQuery()
            ->getResult();
        
        if (!empty($results)) {
            //print_r($results); exit;
            foreach ($results as $record) {
                if (in_array($record['type'], array('simple_text', 'multiple_text', 'unique_option', 'selection_option')) || empty($record['answer_options'])) {
              
                    $entity = new Pollapplicationreport();
                  
                    $entity->setPollschedulingId($record['pollscheduling_id']);
                    $entity->setPollapplicationId($record['pollapplication_id']);
                    $entity->setPollapplicationquestionId($record['pollapplicationquestion_id']);
                    $entity->setCompanyId($record['company_id']);
                    $entity->setCompany($record['company_name']);
                    if ($record['headquarter_id'] != '') {
                        $entity->setHeadquarterId($record['headquarter_id']);
                        $entity->setHeadquarterCode($record['headquarter_code']);
                        $entity->setHeadquarter($record['headquarter_name']);
                    }
                    if ($record['company_type'] == 'working') {
                        if ($record['companyorganizationalunit_id'] != '') {
                            $entity->setCompanyorganizationalunitId($record['companyorganizationalunit_id']);
                            $entity->setCompanyorganizationalunitCode($record['companyorganizationalunit_code']);
                            $entity->setCompanyorganizationalunit($record['companyorganizationalunit_name']);
                        }
                        if ($record['companyorganizationalunit_parent_id'] != '') {
                            $entity->setCompanyorganizationalunitParentId($record['companyorganizationalunit_parent_id']);
                            $entity->setCompanyorganizationalunitParentCode($record['companyorganizationalunit_parent_code']);
                            $entity->setCompanyorganizationalunitParent($record['companyorganizationalunit_parent_name']);
                        }
                        if ($record['companycharge_id'] != '') {
                            $entity->setCompanychargeId($record['companycharge_id']);
                            $entity->setCompanychargeCode($record['companycharge_code']);
                            $entity->setCompanycharge($record['companycharge_name']);
                        }
                    }
                    elseif ($record['company_type'] == 'university') {
                        if ($record['companyfaculty_id'] != '') {
                            $entity->setCompanyfacultyId($record['companyfaculty_id']);
                            $entity->setCompanyfacultyCode($record['companyfaculty_code']);
                            $entity->setCompanyfaculty($record['companyfaculty_name']);
                        }
                        if ($record['companycareer_id'] != '') {
                            $entity->setCompanycareerId($record['companycareer_id']);
                            $entity->setCompanycareerCode($record['companycareer_code']);
                            $entity->setCompanycareer($record['companycareer_name']);
                        }
                        if ($record['companycourse_id'] != '') {
                            $entity->setCompanycourseId($record['companycourse_id']);
                            $entity->setCompanycourseCode($record['companycourse_code']);
                            $entity->setCompanycourse($record['companycourse_name']);
                        }
                        $entity->setAcademicYear($record['academic_year']);
                    }
                    elseif ($record['company_type'] == 'school') {
                        if ($record['companycourse_id'] != '') {
                            $entity->setCompanycourseId($record['companycourse_id']);
                            $entity->setCompanycourseCode($record['companycourse_code']);
                            $entity->setCompanycourse($record['companycourse_name']);
                        }
                        $entity->setGrade($record['grade']);
                        $entity->setStudySection($record['study_section']);
                        $entity->setStudyYear($record['study_year']);
                    }
                    if ($record['user_id'] != '') {
                        $entity->setDatein($record['datein']);
                        if ($record['dateout'] != NULL) {
                            $entity->setDateout($record['dateout']);
                        }
                        $entity->setUserId($record['user_id']);
                        $entity->setEvaluatedId($record['evaluated_id']);
                        $entity->setEvaluatedCode($record['evaluated_code']);
                        $entity->setNames($record['evaluated_surname'] . ' ' . $record['evaluated_lastname'] . ', ' . $record['evaluated_names']);
                        $entity->setSex(self::getSexName($record['sex']));
                        if ($record['birthday'] != NULL) {
                            $entity->setBirthday($record['birthday']);
                        }
                        if ($record['birthplace'] != '') {
                            $entity->setBirthplace($record['birthplace']);
                        }
                        if ($record['marital_status'] != '') {
                            $entity->setMaritalStatus(self::getMaritalStatusName($record['marital_status']));
                        }
                    }
                    $entity->setEvaluatorId($record['evaluator_id']);
                    $entity->setEvaluatorCode($record['evaluator_code']);
                    $entity->setEvaluatorName($record['evaluator_surname'] . ' ' . $record['evaluator_lastname'] . ', ' . $record['evaluator_names']);
                    $entity->setSectionId($record['section_id']);
                    $entity->setSection($record['section_name']);
                    if ($record['section_parent_id'] != NULL) {
                        $entity->setSectionParentId($record['section_parent_id']);
                        $entity->setSectionParent($record['section_parent_name']);
                    }
                    $entity->setQuestionNumber($record['question_number']);
                    $entity->setQuestion($record['question']);
                    $entity->setSequence($record['sequence']);
                    if (in_array($record['type'], array('simple_text', 'multiple_text'))) {
                        if ($record['answer_options'] != '') {
                            $entity->setAnswer($record['answer']);
                        }
                        $entity->setScore(0);
                    }
                    elseif (in_array($record['type'], array('unique_option', 'multiple_option', 'selection_option'))) {
                        if (!empty($record['answer_options'])) {
                            $entity->setAnswer($record['options'][$record['answer_options']]['text']);
                        }
                        if ($record['score'] != '') {
                            $entity->setScore($record['options'][$record['answer_options']]['score']);
                        }
                        else {
                            $entity->setScore(0);
                        }
                    }
                    $entity->setCeilScore($record['ceil_score']);
                    if ($entity->getScore() > 0) {
                        $entity->setResultScore($entity->getScore()/$record['ceil_score']);
                    }
                    else {
                        $entity->setResultScore(0);
                    }
                
                    $em->persist($entity);
                    $em->flush();
                }
                else {
                    foreach ($record['answer_options'] as $value) {
                      
                        $entity = new Pollapplicationreport();
                  
                        $entity->setPollschedulingId($record['pollscheduling_id']);
                        $entity->setPollapplicationId($record['pollapplication_id']);
                        $entity->setPollapplicationquestionId($record['pollapplicationquestion_id']);
                        $entity->setCompanyId($record['company_id']);
                        $entity->setCompany($record['company_name']);
                        if ($record['companyorganizationalunit_id'] != '') {
                            $entity->setCompanyorganizationalunitId($record['companyorganizationalunit_id']);
                            $entity->setCompanyorganizationalunitCode($record['companyorganizationalunit_code']);
                            $entity->setCompanyorganizationalunit($record['companyorganizationalunit_name']);
                        }
                        if ($record['companyorganizationalunit_parent_id'] != '') {
                            $entity->setCompanyorganizationalunitParentId($record['companyorganizationalunit_parent_id']);
                            $entity->setCompanyorganizationalunitParentCode($record['companyorganizationalunit_parent_code']);
                            $entity->setCompanyorganizationalunitParent($record['companyorganizationalunit_parent_name']);
                        }
                        if ($record['companycharge_id'] != '') {
                            $entity->setCompanychargeId($record['companycharge_id']);
                            $entity->setCompanychargeCode($record['companycharge_code']);
                            $entity->setCompanycharge($record['companycharge_name']);
                        }
                        if ($record['companyfaculty_id'] != '') {
                            $entity->setCompanyfacultyId($record['companyfaculty_id']);
                            $entity->setCompanyfacultyCode($record['companyfaculty_code']);
                            $entity->setCompanyfaculty($record['companyfaculty_name']);
                        }
                        if ($record['companycareer_id'] != '') {
                            $entity->setCompanycareerId($record['companycareer_id']);
                            $entity->setCompanycareerCode($record['companycareer_code']);
                            $entity->setCompanycareer($record['companycareer_name']);
                        }
                        if ($record['companycourse_id'] != '') {
                            $entity->setCompanycourseId($record['companycourse_id']);
                            $entity->setCompanycourseCode($record['companycourse_code']);
                            $entity->setCompanycourse($record['companycourse_name']);
                        }
                        if ($record['user_id'] != '') {
                            $entity->setDatein($record['datein']);
                            if ($record['dateout'] != NULL) {
                                $entity->setDateout($record['dateout']);
                            }
                            $entity->setUserId($record['user_id']);
                            $entity->setEvaluatedId($record['evaluated_id']);
                            $entity->setEvaluatedCode($record['evaluated_code']);
                            $entity->setNames($record['evaluated_surname'] . ' ' . $record['evaluated_lastname'] . ', ' . $record['evaluated_names']);
                            $entity->setSex(self::getSexName($record['sex']));
                            if ($record['birthday'] != NULL) {
                                $entity->setBirthday($record['birthday']);
                            }
                            if ($record['birthplace'] != '') {
                                $entity->setBirthplace($record['birthplace']);
                            }
                            if ($record['marital_status'] != '') {
                                $entity->setMaritalStatus(self::getMaritalStatusName($record['marital_status']));
                            }
                        }
                        $entity->setEvaluatorId($record['evaluator_id']);
                        $entity->setEvaluatorCode($record['evaluator_code']);
                        $entity->setEvaluatorName($record['evaluator_surname'] . ' ' . $record['evaluator_lastname'] . ', ' . $record['evaluator_names']);
                        $entity->setSectionId($record['section_id']);
                        $entity->setSection($record['section_name']);
                        if ($record['section_parent_id'] != NULL) {
                            $entity->setSectionParentId($record['section_parent_id']);
                            $entity->setSectionParent($record['section_parent_name']);
                        }
                        $entity->setQuestionNumber($record['question_number']);
                        $entity->setQuestion($record['question']);
                        $entity->setSequence($record['sequence']);
                        $entity->setAnswer($record['options'][$value]['text']);
                        $entity->setScore($record['options'][$value]['score']);
                        $entity->setCeilScore($record['ceil_score']);
                        if ($entity->getScore() > 0) {
                            $entity->setResultScore($entity->getScore()/$record['ceil_score']);
                        }
                        else {
                            $entity->setResultScore(0);
                        }
                
                        $em->persist($entity);
                        $em->flush();
                    }
                }
            }
        }
        
    }
    
    /*
     * Get sexName
     */
    public static function getSexName($sex)
    {
        $output = '';
        
        if ($sex == 'm') {
            $output = 'Masculino';
        }
        elseif ($sex == 'f') {
            $output = 'Femenino';
        }

        return $output;
    }

    /*
     * Get maritalStatusName
     */
    public static function getMaritalStatusName($maritalStatus)
    {
        $output = '';
      
        if ($maritalStatus == 'single') {
            $output = 'Soltero';
        }
        elseif ($maritalStatus == 'married') {
            $output = 'Casado';
        }
        elseif ($maritalStatus == 'widower') {
            $output = 'Viudo';
        }
        elseif ($maritalStatus == 'divorced') {
            $output = 'Divorciado';
        }
      
        return $output;
    }
    
    /**
     * getListPollapplicationreportResults
     */
    public static function getListPollapplicationreportResults(DoctrineRegistry $doctrineRegistry, $psIds, $evaluatedId = '')
    {  
        $em = $doctrineRegistry->getManager();
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationreport');
        $queryBuilder = $repository->createQueryBuilder('par')
            ->select('par.id, par.company_id, par.company, par.pollapplication_id, par.pollapplicationquestion_id, 
                    par.companyorganizationalunit_id, par.companyorganizationalunit, par.companyorganizationalunit_parent_id, 
                    par.companyorganizationalunit_parent, par.companycharge_id, par.companycharge, par.datein, 
                    par.dateout, par.user_id, par.names, par.sex, par.birthday, par.birthplace, par.marital_status, 
                    par.evaluator_id, par.evaluator_name, par.section_id, par.section, par.section_parent_id, 
                    par.section_parent, par.question_number, par.question, par.sequence, par.score, par.answer, 
                    par.pollscheduling_id, par.evaluated_id, par.companyorganizationalunit_code, 
                    par.companyorganizationalunit_parent_code, par.companycharge_code, par.evaluated_code, par.evaluator_code, 
                    par.headquarter_id, par.headquarter_code, par.headquarter, par.companyfaculty_id, par.companyfaculty_code, 
                    par.companyfaculty, par.companycareer_id, par.companycareer_code, par.companycareer, par.companycourse_id, 
                    par.companycourse_code, par.companycourse, par.grade, par.study_section, par.study_year, 
                    par.academic_year, par.ceil_score, par.result_score')
            ->where('par.pollscheduling_id IN (' . $psIds . ')')
            ->orderBy('par.pollapplication_id', 'ASC')
            ->addOrderBy('par.names', 'ASC')
            ->addOrderBy('par.id', 'ASC')
            ->addOrderBy('par.evaluated_id', 'ASC')
            ->addOrderBy('par.evaluator_id', 'ASC');
            
        if ($evaluatedId != '') {
            $queryBuilder
                ->andWhere('par.evaluated_id = :evaluated')
                ->setParameter('evaluated', $evaluatedId);
        }
        
        $result = $queryBuilder->getQuery()->getResult();
            
        return $result;
    }
    
    /**
     * getListPollapplicationreportByFilters
     */
    public static function getListPollapplicationreportByFilters(DoctrineRegistry $doctrineRegistry, $pscId, $filterIds = '', $typeFilter = '')
    {
        $em = $doctrineRegistry->getManager();
        //jtt
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationreport');
        $queryBuilder = $repository->createQueryBuilder('par')
            ->select('par.id, par.company_id, par.company, par.pollapplication_id, par.pollapplicationquestion_id, 
                    par.companyorganizationalunit_id, par.companyorganizationalunit, par.companyorganizationalunit_parent_id, 
                    par.companyorganizationalunit_parent, par.companycharge_id, par.companycharge, par.datein, 
                    par.dateout, par.user_id, par.names, par.sex, par.birthday, par.birthplace, par.marital_status, 
                    par.evaluator_id, par.evaluator_name, ps.id section_id, ps.name section, psp.id section_parent_id, 
                    psp.name section_parent, par.question_number, par.question, par.sequence, par.score, par.answer, 
                    par.pollscheduling_id, par.evaluated_id, par.companyorganizationalunit_code, 
                    par.companyorganizationalunit_parent_code, par.companycharge_code, par.evaluated_code, par.evaluator_code, 
                    par.headquarter_id, par.headquarter_code, par.headquarter, par.companyfaculty_id, par.companyfaculty_code, 
                    par.companyfaculty, par.companycareer_id, par.companycareer_code, par.companycareer, par.companycourse_id, 
                    par.companycourse_code, par.companycourse, par.grade, par.study_section, par.study_year, 
                    par.academic_year, par.ceil_score, par.result_score')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'par.pollapplication_id = pa.id')
            ->innerJoin('FishmanPollBundle:Pollapplicationquestion', 'paq', 'WITH', 'par.pollapplicationquestion_id = paq.id')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->innerJoin('psq.pollquestion', 'pq')
            ->innerJoin('pq.pollsection', 'ps')
            ->innerJoin('FishmanPollBundle:Pollsection', 'psp', 'WITH', 'ps.parent_id=psp.id')
            ->where('par.pollscheduling_id = :pollscheduling
                    AND pa.finished = 1
                    AND pa.status = 1
                    AND pa.deleted = 0')
            ->setParameter('pollscheduling', $pscId)
            ->orderBy('par.pollapplication_id', 'ASC')
            ->addOrderBy('par.names', 'ASC')
            ->addOrderBy('par.id', 'ASC')
            ->addOrderBy('par.evaluated_id', 'ASC')
            ->addOrderBy('par.evaluator_id', 'ASC');
        
        if ($filterIds != '') {
            switch ($typeFilter) {
                case 'evaluateds':
                    $queryBuilder
                        ->andWhere("par.evaluated_id IN(" . $filterIds . ")");
                    break;
                case 'organizationalunits':
                    $queryBuilder
                        ->andWhere("par.companyorganizationalunit_id IN(" . $filterIds . ")");
                    break;
                case 'charges':
                    $queryBuilder
                        ->andWhere("par.companycharge_id IN(" . $filterIds . ")");
                    break;
                case 'headquarters':
                    $queryBuilder
                        ->andWhere("par.headquarter_id IN(" . $filterIds . ")");
                    break;
                case 'faculties':
                    $queryBuilder
                        ->andWhere("par.companyfaculty_id IN(" . $filterIds . ")");
                    break;
                case 'careers':
                    $queryBuilder
                        ->andWhere("par.companycareer_id IN(" . $filterIds . ")");
                    break;
                case 'grades':
                    $queryBuilder
                        ->andWhere("par.grade IN(" . $filterIds . ")");
                    break;
            }
        }
        
        $results = $queryBuilder->getQuery()->getResult();
        
        return $results;
    }
    
    /*
     * Generate Report Result
     * 
     */
    public static function generateReportResult($pollapplicationreports, &$worksheet, &$phpExcel, $company, $showEvaluator = TRUE, $row = '')
    {
        if (!empty($pollapplicationreports)) {
            
            $i = 1;
            
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Empresa');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Empresa');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Encuesta programada');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Encuesta aplicada');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Pregunta de encuesta aplicada');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Sede');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código Sede');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Sede');
            if ($company->getCompanyType() == 'working') {
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Unidad Organizativa');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código Unidad Organizativa');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Unidad Organizativa');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Unidad Organizativa padre');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código Unidad Organizativa padre');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Unidad Organizativa padre');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id cargo');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código cargo');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Cargo');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Fecha de inicio');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Fecha de fin');
            }
            elseif ($company->getCompanyType() == 'university') {
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id facultad');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código facultad');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Facultad');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id carrera');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código carrera');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Carrera');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id curso');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código curso');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Curso');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Año Académico');
            }
            elseif ($company->getCompanyType() == 'school') {
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id curso');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código curso1');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Curso');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Grado');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Sección');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Año de Estudio');
            }
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Usuario');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Evaluado');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código Evaluado');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Nombres');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Sexo');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Fecha de nacimiento');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Lugar de nacimiento');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Estado civil');
            if ($showEvaluator) {
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id evaluador');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Código evaluador');
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Evaluador');
            }
            else {
                $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Evaluador');
            }
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Sección');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Sección');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Id Sección padre');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Sección padre');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Nro de Pregunta');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Pregunta');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Orden');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Puntos');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Respuesta');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Puntaje Máximo');
            $worksheet->getCellByColumnAndRow($i++, 5)->setValue('Resultado');
            
            if ($showEvaluator) {
                if ($company->getCompanyType() == 'working') {
                    $columns = array(
                        'B' => 8, 'C' => 14, 'D' => 35, 'E' => 20, 'F' => 18, 'G' => 28, 'H' => 20, 'I' => 20, 'J' => 20, 
                        'K' => 20, 'L' => 35, 'M' => 35, 'N' => 25, 'O' => 31, 'P' => 35, 'Q' => 20, 'R' => 20, 'S' => 20, 
                        'T' => 14, 'U' => 14, 'V' => 14, 'W' => 14, 'X' => 14, 'Y' => 35, 'Z' => 18, 'AA' => 18, 'AB' => 35, 
                        'AC' => 14, 'AD' => 14, 'AE' => 15, 'AF' => 35, 'AG' => 14, 'AH' => 25, 'AI' => 15, 'AJ' => 25, 
                        'AK' => 14, 'AL' => 40, 'AM' => 8, 'AN' => 8, 'AO' => 55, 'AP' => 14, 'AQ' => 14
                    );
                }
                elseif ($company->getCompanyType() == 'university') {
                    $columns = array(
                        'B' => 8, 'C' => 14, 'D' => 35, 'E' => 20, 'F' => 18, 'G' => 28, 'H' => 20, 'I' => 20, 'J' => 20, 
                        'K' => 20, 'L' => 20, 'M' => 20, 'N' => 20, 'O' => 20, 'P' => 20, 'Q' => 20, 'R' => 20, 'S' => 20, 
                        'T' => 15, 'U' => 14, 'V' => 14, 'W' => 14, 'X' => 35, 'Y' => 18, 'Z' => 18, 'AA' => 35, 'AB' => 14, 
                        'AC' => 14, 'AD' => 15, 'AE' => 35, 'AF' => 14, 'AG' => 25, 'AH' => 15, 'AI' => 25, 'AJ' => 14, 
                        'AK' => 40, 'AL' => 8, 'AM' => 8, 'AN' => 55, 'AO' => 14, 'AP' => 14, 'AQ' => 9
                    );
                }
                elseif ($company->getCompanyType() == 'school') {
                    $columns = array(
                        'B' => 8, 'C' => 14, 'D' => 35, 'E' => 20, 'F' => 18, 'G' => 28, 'H' => 20, 'I' => 20, 'J' => 20, 
                        'K' => 20, 'L' => 20, 'M' => 20, 'N' => 15, 'O' => 15, 'P' => 15, 'Q' => 14, 'R' => 14, 'S' => 14, 
                        'T' => 35, 'U' => 18, 'V' => 18, 'W' => 35, 'X' => 14, 'Y' => 14, 'Z' => 15, 'AA' => 35, 'AB' => 14, 
                        'AC' => 25, 'AD' => 15, 'AE' => 25, 'AF' => 14, 'AG' => 40, 'AH' => 8, 'AI' => 8, 'AJ' => 55, 
                        'AK' => 14, 'AL' => 14, 'AM' => 9, 'AN' => 9, 'AO' => 9, 'AP' => 9, 'AQ' => 9
                    );
                }
            }
            else {
                if ($company->getCompanyType() == 'working') {
                    $columns = array(
                        'B' => 8, 'C' => 14, 'D' => 35, 'E' => 20, 'F' => 18, 'G' => 28, 'H' => 20, 'I' => 20, 'J' => 20, 
                        'K' => 20, 'L' => 35, 'M' => 35, 'N' => 25, 'O' => 31, 'P' => 35, 'Q' => 20, 'R' => 20, 'S' => 20, 
                        'T' => 14, 'U' => 14, 'V' => 14, 'W' => 14, 'X' => 14, 'Y' => 35, 'Z' => 18, 'AA' => 18, 'AB' => 35, 
                        'AC' => 14, 'AD' => 14, 'AE' => 14, 'AF' => 25, 'AG' => 15, 'AH' => 25, 'AI' => 14, 'AJ' => 40, 
                        'AK' => 8, 'AL' => 8, 'AM' => 55, 'AN' => 14, 'AO' => 14, 'AP' => 9, 'AQ' => 9
                    );
                }
                elseif ($company->getCompanyType() == 'university') {
                    $columns = array(
                        'B' => 8, 'C' => 14, 'D' => 35, 'E' => 20, 'F' => 18, 'G' => 28, 'H' => 20, 'I' => 20, 'J' => 20, 
                        'K' => 20, 'L' => 20, 'M' => 20, 'N' => 20, 'O' => 20, 'P' => 20, 'Q' => 20, 'R' => 20, 'S' => 20, 
                        'T' => 15, 'U' => 14, 'V' => 14, 'W' => 14, 'X' => 35, 'Y' => 18, 'Z' => 18, 'AA' => 35, 'AB' => 14, 
                        'AC' => 14, 'AD' => 14, 'AE' => 25, 'AF' => 15, 'AG' => 25, 'AH' => 14, 'AI' => 40, 'AJ' => 8, 
                        'AK' => 8, 'AL' => 55, 'AM' => 14, 'AN' => 14, 'AO' => 9, 'AP' => 9, 'AQ' => 9
                    );
                }
                elseif ($company->getCompanyType() == 'school') {
                    $columns = array(
                        'B' => 8, 'C' => 14, 'D' => 35, 'E' => 20, 'F' => 18, 'G' => 28, 'H' => 20, 'I' => 20, 'J' => 20, 
                        'K' => 20, 'L' => 20, 'M' => 20, 'N' => 15, 'O' => 15, 'P' => 15, 'Q' => 14, 'R' => 14, 'S' => 14, 
                        'T' => 35, 'U' => 18, 'V' => 18, 'W' => 35, 'X' => 14, 'Y' => 14, 'Z' => 14, 'AA' => 25, 'AB' => 15, 
                        'AC' => 25, 'AD' => 14, 'AE' => 40, 'AF' => 8, 'AG' => 8, 'AH' => 55, 'AI' => 14, 'AJ' => 14, 
                        'AK' => 9, 'AL' => 9, 'AM' => 9, 'AN' => 9, 'AO' => 9, 'AP' => 9, 'AQ' => 9
                    );
                }
            }
            
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth($columns['B']);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth($columns['C']);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth($columns['D']);
            $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth($columns['E']);
            $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth($columns['F']);
            $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth($columns['G']);
            $phpExcel->getActiveSheet()->getColumnDimension('H')->setWidth($columns['H']);
            $phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth($columns['I']);
            $phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth($columns['J']);
            $phpExcel->getActiveSheet()->getColumnDimension('K')->setWidth($columns['K']);
            $phpExcel->getActiveSheet()->getColumnDimension('L')->setWidth($columns['L']);
            $phpExcel->getActiveSheet()->getColumnDimension('M')->setWidth($columns['M']);
            $phpExcel->getActiveSheet()->getColumnDimension('N')->setWidth($columns['N']);
            $phpExcel->getActiveSheet()->getColumnDimension('O')->setWidth($columns['O']);
            $phpExcel->getActiveSheet()->getColumnDimension('P')->setWidth($columns['P']);
            $phpExcel->getActiveSheet()->getColumnDimension('Q')->setWidth($columns['Q']);
            $phpExcel->getActiveSheet()->getColumnDimension('R')->setWidth($columns['R']);
            $phpExcel->getActiveSheet()->getColumnDimension('S')->setWidth($columns['S']);
            $phpExcel->getActiveSheet()->getColumnDimension('T')->setWidth($columns['T']);
            $phpExcel->getActiveSheet()->getColumnDimension('U')->setWidth($columns['U']);
            $phpExcel->getActiveSheet()->getColumnDimension('V')->setWidth($columns['V']);
            $phpExcel->getActiveSheet()->getColumnDimension('W')->setWidth($columns['W']);
            $phpExcel->getActiveSheet()->getColumnDimension('X')->setWidth($columns['X']);
            $phpExcel->getActiveSheet()->getColumnDimension('Y')->setWidth($columns['Y']);
            $phpExcel->getActiveSheet()->getColumnDimension('Z')->setWidth($columns['Z']);
            $phpExcel->getActiveSheet()->getColumnDimension('AA')->setWidth($columns['AA']);
            $phpExcel->getActiveSheet()->getColumnDimension('AB')->setWidth($columns['AB']);
            $phpExcel->getActiveSheet()->getColumnDimension('AC')->setWidth($columns['AC']);
            $phpExcel->getActiveSheet()->getColumnDimension('AD')->setWidth($columns['AD']);
            $phpExcel->getActiveSheet()->getColumnDimension('AE')->setWidth($columns['AE']);
            $phpExcel->getActiveSheet()->getColumnDimension('AF')->setWidth($columns['AF']);
            $phpExcel->getActiveSheet()->getColumnDimension('AG')->setWidth($columns['AG']);
            $phpExcel->getActiveSheet()->getColumnDimension('AH')->setWidth($columns['AH']);
            $phpExcel->getActiveSheet()->getColumnDimension('AI')->setWidth($columns['AI']);
            $phpExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth($columns['AJ']);
            $phpExcel->getActiveSheet()->getColumnDimension('AK')->setWidth($columns['AK']);
            $phpExcel->getActiveSheet()->getColumnDimension('AL')->setWidth($columns['AL']);
            $phpExcel->getActiveSheet()->getColumnDimension('AM')->setWidth($columns['AM']);
            $phpExcel->getActiveSheet()->getColumnDimension('AN')->setWidth($columns['AN']);
            $phpExcel->getActiveSheet()->getColumnDimension('AO')->setWidth($columns['AO']);
            $phpExcel->getActiveSheet()->getColumnDimension('AP')->setWidth($columns['AP']);
            $phpExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth($columns['AQ']);
            
            if ($row == '') {
                $row = 7;
            }
            
            $evNumber = rand(5, 15);
            //print_r($pollapplicationreports);exit;
            foreach ($pollapplicationreports as $par) {
                
                $i = 1;
                
                // Recover format date
                
                $datein = '';
                $dateout = '';
                $birthday = '';
                if (!is_null($par['datein'])) {
                    $datein = date('Y-m-d', strtotime($par['datein']->format('d/m/Y')));
                }
                if (!is_null($par['dateout'])) {
                    $dateout = date('Y-m-d', strtotime($par['dateout']->format('d/m/Y')));
                }
                if (!is_null($par['birthday'])) {
                    $birthday = date('Y-m-d', strtotime($par['birthday']->format('d/m/Y')));
                }
                
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['company_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['company']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['pollscheduling_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['pollapplication_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['pollapplicationquestion_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['headquarter_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['headquarter_code']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['headquarter']);
                if ($company->getCompanyType() == 'working') {
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyorganizationalunit_id']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyorganizationalunit_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyorganizationalunit']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyorganizationalunit_parent_id']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyorganizationalunit_parent_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyorganizationalunit_parent']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycharge_id']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycharge_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycharge']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($datein);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($dateout);
                }
                elseif ($company->getCompanyType() == 'university') {
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyfaculty_id']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyfaculty_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companyfaculty']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycareer_id']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycareer_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycareer']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycourse_id']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycourse_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycourse']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['academic_year']);
                }
                elseif ($company->getCompanyType() == 'school') {
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycourse_id']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycourse_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['companycourse']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['grade']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['study_section']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['study_year']);
                }
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['user_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['evaluated_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['evaluated_code']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['names']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['sex']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($birthday);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['birthplace']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['marital_status']);
                if ($showEvaluator) {
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['evaluator_id']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['evaluator_code']);
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['evaluator_name']);
                }
                else {
                    $worksheet->getCellByColumnAndRow($i++, $row)->setValue('E' . ($par['evaluator_id'] + $evNumber));
                }
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['section_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['section']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['section_parent_id']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['section_parent']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['question_number']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['question']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['sequence']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['score']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['answer']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['ceil_score']);
                $worksheet->getCellByColumnAndRow($i++, $row)->setValue($par['result_score']);
               
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('No hay registros que mostrar');
        }
    }
    
    /*
     * Generate Report Result CSV
     * 
     */
    public static function generateReportResultCSV($pollapplicationreports, $company, $showEvaluator = TRUE)
    {
        $output = '';
        
        if (!empty($pollapplicationreports)) {
          
            $output .= 'Id' . ';' . 'Id Empresa' . ';' . 'Empresa' . ';' . 'Id Encuesta programada' . ';' . 
                       'Id Encuesta aplicada' . ';' . 'Id Pregunta de encuesta aplicada'. ';' . 'Id Sede'. ';' . 
                       'Código Sede'. ';' . 'Sede';
            
            if ($company->getCompanyType() == 'working') {
              
                $output .= ';' . 'Id Unidad Organizativa' . ';' . 'Código Unidad Organizativa' . ';' . 
                           'Unidad Organizativa' . ';' . 'Id Unidad Organizativa padre' . ';' . 
                           'Código Unidad Organizativa padre' . ';' . 'Unidad Organizativa padre' . ';' . 
                           'Id cargo' . ';' . 'Código cargo' . ';' . 'Cargo' . ';' . 'Fecha de inicio' . ';' . 
                           'Fecha de fin';
                           
            }
            elseif ($company->getCompanyType() == 'university') {
              
                $output .= ';' . 'Id facultad' . ';' . 'Código facultad' . ';' . 'Facultad' . ';' . 
                           'Id carrera' . ';' . 'Código carrera' . ';' . 'Carrera' . ';' . 
                           'Id curso' . ';' . 'Código curso' . ';' . 'Curso' . ';' . 'Año Académico';
                           
            }
            elseif ($company->getCompanyType() == 'school') {
                
                $output .= ';' . 'Id curso' . ';' . 'Código curso' . ';' . 'Curso' . ';' . 'Grado' . ';' . 
                           'Sección' . ';' . 'Año de Estudio';
                           
            }
            
            $output .= ';' . 'Id Usuario' . ';' . 'Id Evaluado' . ';' . 'Código Evaluado' . ';' . 'Nombres' . ';' . 
                       'Sexo' . ';' . 'Fecha de nacimiento' . ';' . 'Lugar de nacimiento' . ';' . 'Estado civil';
                       
            if ($showEvaluator) {
                $output .= ';' . 'Id evaluador' . ';' . 'Código evaluador' . ';' . 'Evaluador';
            }
            else {
                $output .= ';' . 'Evaluador';
            }
            
            $output .= ';' . 'Id Sección' . ';' . 'Sección' . ';' . 'Id Sección padre' . ';' . 'Sección padre' . ';' . 
                       'Nro de Pregunta' . ';' . 'Pregunta' . ';' . 'Orden' . ';' . 'Puntos' . ';' . 
                       'Respuesta' . ';' . 'Puntaje Máximo' . ';' . 'Resultado' . "\r\n";
            
            $evNumber = rand(5, 15);
            
            foreach ($pollapplicationreports as $par) {
                
                // Recover format date
                
                $datein = '';
                $dateout = '';
                $birthday = '';
                
                if ($par['datein']) {
                    $datein = date('Y-m-d', strtotime($par['datein']->format('d/m/Y')));
                }
                if ($par['dateout']) {
                    $dateout = date('Y-m-d', strtotime($par['dateout']->format('d/m/Y')));
                }
                if ($par['birthday']) {
                    $birthday = date('Y-m-d', strtotime($par['birthday']->format('d/m/Y')));
                }
                
                $output .= $par['id'] . ';' . $par['company_id'] . ';' . $par['company'] . ';' . 
                           $par['pollscheduling_id'] . ';' . $par['pollapplication_id'] . ';' . 
                           $par['pollapplicationquestion_id'] . ';' . $par['headquarter_id'] . ';' . 
                           $par['headquarter_code'] . ';' . $par['headquarter'];
                if ($company->getCompanyType() == 'working') {
                    $output .= ';' . $par['companyorganizationalunit_id'] . ';' . 
                               $par['companyorganizationalunit_code'] . ';' . 
                               $par['companyorganizationalunit'] . ';' . 
                               $par['companyorganizationalunit_parent_id'] . ';' . 
                               $par['companyorganizationalunit_parent_code'] . ';' . 
                               $par['companyorganizationalunit_parent'] . ';' . 
                               $par['companycharge_id'] . ';' . 
                               $par['companycharge_code'] . ';' . 
                               $par['companycharge'] . ';' . 
                               $datein . ';' . 
                               $dateout;
                }
                elseif ($company->getCompanyType() == 'university') {
                    $output .= ';' . $par['companyfaculty_id'] . ';' . 
                               $par['companyfaculty_code'] . ';' . 
                               $par['companyfaculty'] . ';' . 
                               $par['companycareer_id'] . ';' . 
                               $par['companycareer_code'] . ';' . 
                               $par['companycareer'] . ';' . 
                               $par['companycourse_id'] . ';' . 
                               $par['companycourse_code'] . ';' . 
                               $par['companycourse'] . ';' . 
                               $par['academic_year'];
                }
                elseif ($company->getCompanyType() == 'school') {
                    $output .= ';' . $par['companycourse_id'] . ';' . 
                               $par['companycourse_code'] . ';' . 
                               $par['companycourse'] . ';' . 
                               $par['grade'] . ';' . 
                               $par['study_section'] . ';' . 
                               $par['study_year'];
                }
                $output .= ';' . $par['user_id'] . ';' . 
                           $par['evaluated_id'] . ';' . 
                           $par['evaluated_code'] . ';' . 
                           $par['names'] . ';' . 
                           $par['sex'] . ';' . 
                           $birthday . ';' . 
                           $par['birthplace'] . ';' . 
                           $par['marital_status'];
                if ($showEvaluator) {
                    $output .= ';' . $par['evaluator_id'] . ';' . 
                           $par['evaluator_code'] . ';' . 
                           $par['evaluator_name'];
                }
                else {
                    $output .= ';' . 'E' . ($par['evaluator_id'] + $evNumber);
                }
                $output .= ';' . $par['section_id'] . ';' . 
                           $par['section'] . ';' . 
                           $par['section_parent_id'] . ';' . 
                           $par['section_parent'] . ';' . 
                           $par['question_number'] . ';' . 
                           $par['question'] . ';' . 
                           $par['sequence'] . ';' . 
                           $par['score'] . ';' . 
                           $par['answer'] . ';' . 
                           $par['ceil_score'] . ';' . 
                           $par['result_score'] . "\r\n";
            }
        }
        else {
            $output .= 'No hay registros que mostrar';
        }
        
        return $output;
    }
    
    /*
     * get Pollapplicationquestion Report Data
     * 
     */
    public static function getPollapplicationquestionReportData($paqs, $key)
    {
        foreach ($paqs as $paq) {
            if ($paq['pq_id'] == $key) {
                return $paq;
            }
        }
    }
    
}
