<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Doctrine\Common\Collections\ArrayCollection;

use Fishman\PollBundle\Entity\Pollscheduling;
use Fishman\PollBundle\Entity\Pollapplicationquestion;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Fishman\AuthBundle\Entity\User;

/**
 * Fishman\PollBundle\Entity\Pollapplication
 */
class Pollapplication
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var \DateTime $initdate
     */
    private $initdate;

    /**
     * @var \DateTime $enddate
     */
    private $enddate;

    /**
     * @var boolean $finished
     */
    private $finished;

    /**
     * @var string $entity_application_type
     */
    private $entity_application_type;

    /**
     * @var integer $entity_application_id
     */
    private $entity_application_id;

    /**
     * @var integer $evaluator_id
     */
    private $evaluator_id;

    /**
     * @var integer $evaluated_id
     */
    private $evaluated_id;

    /**
     * @var string $evaluator_rol
     */
    private $evaluator_rol;
    /**
     * @var integer $resolve_count
     */
    private $resolve_count;

    /**
     * @var integer $duration
     */
    private $duration;

    /**
     * @var string $period
     */
    private $period;

    /**
     * @var integer $course
     */
    private $course;

    /**
     * @var integer $section
     */
    private $section;

    /**
     * @var integer $resolve_account
     */
    private $resolve_account;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var decimal $average
     */
    private $average;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @var boolean $deleted
     */
    private $deleted;

    /**
     * @var boolean $term_on
     */
    private $term_on;

    /**
     * @var string $token
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollscheduling", inversedBy="pollapplications")
     * @ORM\JoinColumn(name="pollscheduling_id", referencedColumnName="id")
     */
    protected $pollscheduling;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollapplicationquestion", mappedBy="pollapplication")
     */
    protected $pollapplicationquestions;

    // jtt: agregado el 20150812
    /**
     * @var string $cargo_global
     */
    private $cargo_global;

    // jtt: agregado el 20150812
    /**
     * @var string $nivel_responsabilidad
     */
    private $nivel_responsabilidad;

    protected $doctrine;

    /**
     * Constructor
     */
    public function __construct(DoctrineRegistry $doctrineRegistry)
    {
        $this->pollapplicationquestions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->doctrine = $doctrineRegistry;
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set initdate
     *
     * @param \DateTime $initdate
     * @return Pollapplication
     */
    public function setInitdate($initdate)
    {
        $this->initdate = $initdate;
    
        return $this;
    }

    /**
     * Get initdate
     *
     * @return \DateTime 
     */
    public function getInitdate()
    {
        return $this->initdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return Pollapplication
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set finished
     *
     * @param boolean $finished
     * @return Pollapplication
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;
    
        return $this;
    }

    /**
     * Get finished
     *
     * @return boolean 
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * Set entity_application_type
     *
     * @param string $entityApplicationType
     * @return Pollapplication
     */
    public function setEntityApplicationType($entityApplicationType)
    {
        $this->entity_application_type = $entityApplicationType;
    
        return $this;
    }

    /**
     * Get entity_application_type
     *
     * @return string 
     */
    public function getEntityApplicationType()
    {
        return $this->entity_application_type;
    }

    /**
     * Set entity_application_id
     *
     * @param integer $entityApplicationId
     * @return Pollapplication
     */
    public function setEntityApplicationId($entityApplicationId)
    {
        $this->entity_application_id = $entityApplicationId;
    
        return $this;
    }

    /**
     * Get entity_application_id
     *
     * @return integer 
     */
    public function getEntityApplicationId()
    {
        return $this->entity_application_id;
    }

    /**
     * Set evaluator_id
     *
     * @param integer $evaluatorId
     * @return Pollapplication
     */
    public function setEvaluatorId($evaluatorId)
    {
        $this->evaluator_id = $evaluatorId;
    
        return $this;
    }

    /**
     * Get evaluator_id
     *
     * @return integer 
     */
    public function getEvaluatorId()
    {
        return $this->evaluator_id;
    }

    /**
     * Set evaluated_id
     *
     * @param integer $evaluatedId
     * @return Pollapplication
     */
    public function setEvaluatedId($evaluatedId)
    {
        $this->evaluated_id = $evaluatedId;
    
        return $this;
    }

    /**
     * Get evaluated_id
     *
     * @return integer 
     */
    public function getEvaluatedId()
    {
        return $this->evaluated_id;
    }

    /**
     * Set evaluator_rol
     *
     * @param string $evaluatorRol
     * @return Pollapplication
     */
    public function setEvaluatorRol($evaluatorRol)
    {
        $this->evaluator_rol = $evaluatorRol;
    
        return $this;
    }

    /**
     * Get evaluator_rol
     *
     * @return string 
     */
    public function getEvaluatorRol()
    {
        return $this->evaluator_rol;
    }

    /**
     * Set evaluated_rol
     *
     * @param string $evaluatedRol
     * @return Pollapplication
     */
    public function setEvaluatedRol($evaluatedRol)
    {
        $this->evaluated_rol = $evaluatedRol;
    
        return $this;
    }

    /**
     * Get evaluated_rol
     *
     * @return string 
     */
    public function getEvaluatedRol()
    {
        return $this->evaluated_rol;
    }

    // jtt: agregado el 20150812
    /**
     * Set cargo_global
     *
     * @param string $cargoGlobal
     * @return Pollapplicationimport
     */
    public function setCargoGlobal($cargoGlobal)
    {
        $this->cargo_global = $cargoGlobal;
    
        return $this;
    }

    // jtt: agregado el 20150812
    /**
     * Get cargo_global
     *
     * @return string
     */
    public function getCargoGlobal()
    {
        return $this->cargo_global;
    }

    // jtt: agregado el 20150812
    /**
     * Set nivel_responsabilidad
     *
     * @param string $nivelResponsabilidad
     * @return Pollapplicationimport
     */
    public function setNivelResponsabilidad($nivelResponsabilidad)
    {
        $this->nivel_responsabilidad = $nivelResponsabilidad;
    
        return $this;
    }

    // jtt: agregado el 20150812
    /**
     * Get nivel_responsabilidad
     *
     * @return string
     */
    public function getNivelResponsabilidad()
    {
        return $this->nivel_responsabilidad;
    }

    /**
     * Set resolve_count
     *
     * @param integer $resolveCount
     * @return Pollapplication
     */
    public function setResolveCount($resolveCount)
    {
        $this->resolve_count = $resolveCount;
    
        return $this;
    }

    /**
     * Get resolve_count
     *
     * @return integer 
     */
    public function getResolveCount()
    {
        return $this->resolve_count;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Pollapplication
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set period
     *
     * @param string $period
     * @return Pollapplication
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return string 
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Pollapplication
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pollapplication
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Pollapplication
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Pollapplication
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Pollapplication
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Pollapplication
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    public function __toString()
    {
         return $this->initdate;
    }

    /**
     * Set term_on
     *
     * @param boolean $termOn
     * @return Pollapplication
     */
    public function setTermOn($termOn)
    {
        $this->term_on = $termOn;
    
        return $this;
    }

    /**
     * Get term_on
     *
     * @return boolean 
     */
    public function getTermOn()
    {
        return $this->term_on;
    }


    /**
     * Set token
     *
     * @param string $token
     * @return Pollapplication
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Get course 
     *
     * @return integer 
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set course 
     *
     * @param integer $course
     * @return Pollapplication
     */
    public function setCourse($course)
    {
        $this->course = $course;
    
        return $this;
    }

    /**
     * Get section 
     *
     * @return section 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set section 
     *
     * @param string $section
     * @return Pollapplication
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * Set average
     *
     * @param float $average
     * @return Pollapplication
     */
    public function setAverage($average)
    {
        $this->average = $average;
    
        return $this;
    }

    /**
     * Get average
     *
     * @return float 
     */
    public function getAverage()
    {
        return $this->average;
    }

    /**
     * Set pollscheduling
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollscheduling
     * @return Pollapplication
     */
    public function setPollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollscheduling = null)
    {
        $this->pollscheduling = $pollscheduling;
    
        return $this;
    }

    /**
     * Get pollscheduling
     *
     * @return Fishman\PollBundle\Entity\Pollscheduling 
     */
    public function getPollscheduling()
    {
        return $this->pollscheduling;
    }

    /**
     * Add pollapplicationquestions
     *
     * @param Fishman\PollBundle\Entity\Pollapplicationquestion $pollapplicationquestions
     * @return Pollapplication
     */
    public function addPollapplicationquestion(\Fishman\PollBundle\Entity\Pollapplicationquestion $pollapplicationquestions)
    {
        $this->pollapplicationquestions[] = $pollapplicationquestions;
    
        return $this;
    }

    /**
     * Remove pollapplicationquestions
     *
     * @param Fishman\PollBundle\Entity\Pollapplicationquestion $pollapplicationquestions
     */
    public function removePollapplicationquestion(\Fishman\PollBundle\Entity\Pollapplicationquestion $pollapplicationquestions)
    {
        $this->pollapplicationquestions->removeElement($pollapplicationquestions);
    }
    
    /**
     * Get pollapplicationquestions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollapplicationquestions()
    {
        return $this->pollapplicationquestions;
    }
    
    /**
     * Generate Notification executions 
     */
    public static function generateNotificationexecutions(DoctrineRegistry $doctrineRegistry, $entityPA, $nsId = '')
    {
        $em = $doctrineRegistry->getManager();
        
        if ($entityPA->getEntityApplicationType() == 'pollschedulingpeople') {
            $entityType = 'pollscheduling';
        }
        else {
            $entityType = 'workshopscheduling';
        }
        
        $nextNotification = '';
        $nsRepository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $queryBuilder = $nsRepository->createQueryBuilder('ns')
             ->where("ns.entity_type = :entity_type")
             ->setParameter('entity_type', $entityType);
        
        if ($entityPA->getEntityApplicationType() == 'pollschedulingpeople') {
            $queryBuilder
                 ->andWhere("ns.entity_id = :entity_id")
                 ->setParameter('entity_id', $entityPA->getPollscheduling()->getId());
        }
        else {
            $queryBuilder
                 ->andWhere("ns.notification_type_id = :notification_type_id")
                 ->setParameter('notification_type_id', $entityPA->getPollscheduling()->getId());
        }
        
        if ($nsId != '') {
            $queryBuilder
                ->andWhere('ns.id = :notificationscheduling')
                ->setParameter('notificationscheduling', $nsId);
        }
        
        $nss = $queryBuilder->getQuery()->getResult();
        
        foreach ($nss as $ns) {
            
            $predecessor = $ns->getPredecessor();
            $nextNotification = NULL;
            
            if (!empty($predecessor) && $predecessor == '-1') {
                
                $initdate = FALSE;
                
                // get init date pollscheduling
                if (!$ns->getNotificationTypeStatus()) {
                    $initdate = strtotime($entityPA->getPollscheduling()->getInitdate()->format('Y/m/d'));
                }
                elseif ($entityPA->getFinished()) {
                    $initdate = strtotime($entityPA->getEnddate()->format('Y/m/d'));
                }
                
                if ($initdate) {
                    // case since
                    if ($ns->getSince() > 0 ) {
                        $initdate = $initdate + ($ns->getSince()*24*60*60);
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                    else {
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                }
                
            }
            
            $profiles = $ns->getAsigned();
           
            if (!empty($profiles)) {
                
                foreach ($profiles as $asigned) {
                    
                    if ($asigned == 'evaluated') {
                        $wiId = $entityPA->getEvaluatedId();
                    }
                    elseif ($asigned == 'evaluator') {
                        $wiId = $entityPA->getEvaluatorId();
                    }
                    
                    if ($entityType == 'workshopscheduling') { 
                        $entityId = $entityPA->getPollscheduling()->getEntityId();
                    }
                    elseif ($entityType == 'pollscheduling') {
                        $entityId = $entityPA->getPollscheduling()->getId();
                    }
                    
                    Notificationexecution::generateNotificationexecutions(
                        $doctrineRegistry, 
                        $ns, 
                        $nextNotification, 
                        $asigned, 
                        'pollapplication', 
                        $entityPA->getId(), 
                        $wiId, 
                        $entityType, 
                        $entityId, 
                        $entityPA->getPollscheduling()->getCompanyId()
                    );
                    
                }
            }
        }

    }

    /**
     * Get List Pollapplications Integrants
     */
    public static function getListPollapplicationsIntegrants(DoctrineRegistry $doctrineRegistry, $wiId, $pollId, $ids = '', $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id, wi.id wi_id, wi.code, u.surname, u.lastname, u.names, cch.name charge, cou.name organizationalunit, pa.finished')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->innerJoin('wi.user', 'u')
            ->leftJoin('wi.companycharge', 'cch')
            ->leftJoin('wi.companyorganizationalunit', 'cou')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.status = 1 
                    AND pa.deleted = :pa_deleted')
            ->andWhere('wi.code = :code 
                    OR u.surname LIKE :surname 
                    OR u.lastname LIKE :lastname 
                    OR u.names LIKE :names')
            ->setParameter('pollscheduling', $pollId)
            ->setParameter('pa_deleted', FALSE)
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('surname', '%' . $data['word'] . '%')
            ->setParameter('lastname', '%' . $data['word'] . '%')
            ->setParameter('names', '%' . $data['word'] . '%')
            ->orderBy('pa.finished', 'ASC', 'pa.initdate', 'ASC', 'pa.enddate', 'ASC')
            ->groupBy('wi.id');
        
        if ($ids != '') {
            $queryBuilder
                ->andWhere('pa.evaluated_id IN(' . $ids . ')');
        }
        if (!empty($data)) {
            if ($data['organizationalunit'] != '') {
                $queryBuilder
                    ->andWhere('cou.id = :organizationalunit')
                    ->setParameter('organizationalunit', $data['organizationalunit']);
            }
            if ($data['charge'] != '') {
                $queryBuilder
                    ->andWhere('cch.id = :charge')
                    ->setParameter('charge', $data['charge']);
            }
        }
            
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get List Pollapplications Finished Integrants
     */
    public static function getListPollapplicationsFinishedIntegrants(DoctrineRegistry $doctrineRegistry, $pollId, $notEvaluateds = FALSE, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id, pa.token, psp.id psp_id, wi.id wi_id, wi.code, u.surname, u.lastname, u.names')
            ->innerJoin('FishmanPollBundle:Pollschedulingpeople', 'psp', 'WITH', 'pa.entity_application_id = psp.id')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.status = 1 
                    AND pa.deleted = 0
                    AND pa.finished = 1')
            ->andWhere('wi.code LIKE :code 
                    OR u.surname LIKE :surname 
                    OR u.lastname LIKE :lastname 
                    OR u.names LIKE :names')
            ->setParameter('pollscheduling', $pollId)
            ->setParameter('code', '%' . $data['word'] . '%')
            ->setParameter('surname', '%' . $data['word'] . '%')
            ->setParameter('lastname', '%' . $data['word'] . '%')
            ->setParameter('names', '%' . $data['word'] . '%')
            ->orderBy('pa.finished', 'ASC', 'pa.initdate', 'ASC', 'pa.enddate', 'ASC')
            ->groupBy('u.id');
        
        if (!$notEvaluateds) {
            $queryBuilder
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->innerJoin('wi.user', 'u');
        }
        else {
            $queryBuilder
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                ->innerJoin('wi.user', 'u');
        }
            
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get List Pollapplications Finished
     */
    public static function getListPollapplicationsFinished(DoctrineRegistry $doctrineRegistry, $pollId, $workinginformationId, $notEvaluateds = FALSE)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id, pa.token, wi.id wi_id, wi.code, u.surname, u.lastname, u.names')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.status = 1 
                    AND pa.deleted = 0
                    AND pa.finished = 1')
            ->setParameter('pollscheduling', $pollId)
            ->orderBy('pa.finished', 'ASC', 'pa.initdate', 'ASC', 'pa.enddate', 'ASC');
        
        if (!$notEvaluateds) {
            $queryBuilder
                ->andWhere('pa.evaluated_id = :workinginformationid')
                ->setParameter('workinginformationid', $workinginformationId)
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->innerJoin('wi.user', 'u');
        }
        else {
            $queryBuilder
                ->andWhere('pa.evaluator_id = :workinginformationid')
                ->setParameter('workinginformationid', $workinginformationId)
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                ->innerJoin('wi.user', 'u');
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get List Pollapplications Evaluateds
     */
    public static function getListPollapplicationsEvaluateds(DoctrineRegistry $doctrineRegistry, $wiId, $pollId, $ids = '', $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id, wi.id wi_id, wi.code, u.surname, u.lastname, u.names, cch.name charge, cou.name organizationalunit, 
                      pa.finished, pa.token')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->innerJoin('wi.user', 'u')
            ->leftJoin('wi.companycharge', 'cch')
            ->leftJoin('wi.companyorganizationalunit', 'cou')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.evaluator_id = :evaluator 
                    AND pa.status = 1 
                    AND pa.deleted = :pa_deleted')
            ->setParameter('pollscheduling', $pollId)
            ->setParameter('evaluator', $wiId)
            ->setParameter('pa_deleted', FALSE)
            ->orderBy('pa.finished', 'ASC', 'pa.initdate', 'ASC', 'pa.enddate', 'ASC');
        
        // Add arguments
        
        if ($ids != '') {
            $queryBuilder
                ->andWhere('pa.evaluated_id IN(' . $ids . ')');
        }
        if (!empty($data)) {
            if ($data['word'] != '') {
                $queryBuilder
                    ->andWhere('u.surname LIKE :surname 
                            OR u.lastname LIKE :lastname 
                            OR u.names LIKE :names')
                    ->setParameter('surname', '%' . $data['word'] . '%')
                    ->setParameter('lastname', '%' . $data['word'] . '%')
                    ->setParameter('names', '%' . $data['word'] . '%');
            }
            if ($data['organizationalunit'] != '') {
                $queryBuilder
                    ->andWhere('cou.id = :organizationalunit')
                    ->setParameter('organizationalunit', $data['organizationalunit']);
            }
            if ($data['charge'] != '') {
                $queryBuilder
                    ->andWhere('cch.id = :charge')
                    ->setParameter('charge', $data['charge']);
            }
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get List Pollapplications Follow
     */
    public static function getListPollapplicationsFollow(DoctrineRegistry $doctrineRegistry, $pollId, $ids = '', $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id, wi_one.code evaluated_code, wi_one.email evaluated_email, u_one.surname evaluated_surname, 
                      u_one.lastname evaluated_lastname, u_one.names evaluated_names, wi_two.code evaluator_code, 
                      wi_two.email evaluator_email, u_two.surname evaluator_surname, u_two.lastname evaluator_lastname, 
                      u_two.names evaluator_names, pa.finished')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
            ->innerJoin('wi_one.user', 'u_one')
            ->innerJoin('wi_two.user', 'u_two')
            ->where('pa.pollscheduling = :pollscheduling  
                    AND pa.status = 1 
                    AND pa.deleted = :deleted')
            ->setParameter('pollscheduling', $pollId)
            ->setParameter('deleted', FALSE)
            ->orderBy('wi_one.code', 'ASC');
        
        // Add arguments
        
        if ($ids != '') {
            $queryBuilder
                ->andWhere('wi_one.id IN(' . $ids . ')');
        }
        if (!empty($data)) {
            if ($data['evaluated_code'] != '') {
                $queryBuilder
                    ->andWhere('wi_one.code LIKE :evaluated_code')
                    ->setParameter('evaluated_code', '%' . $data['evaluated_code'] . '%');
            }
            if ($data['evaluated_names'] != '') {
                $queryBuilder
                    ->andWhere('u_one.names LIKE :evaluated_names')
                    ->setParameter('evaluated_names', '%' . $data['evaluated_names'] . '%');
            }
            if ($data['evaluated_surname'] != '') {
                $queryBuilder
                    ->andWhere('u_one.surname LIKE :evaluated_surname')
                    ->setParameter('evaluated_surname', '%' . $data['evaluated_surname'] . '%');
            }
            if ($data['evaluated_lastname'] != '') {
                $queryBuilder
                    ->andWhere('u_one.lastname LIKE :evaluated_lastname')
                    ->setParameter('evaluated_lastname', '%' . $data['evaluated_lastname'] . '%');
            }
            if ($data['evaluator_code'] != '') {
                $queryBuilder
                    ->andWhere('wi_two.code LIKE :evaluator_code')
                    ->setParameter('evaluator_code', '%' . $data['evaluator_code'] . '%');
            }
            if ($data['evaluator_names'] != '') {
                $queryBuilder
                    ->andWhere('u_two.names LIKE :evaluator_names')
                    ->setParameter('evaluator_names', '%' . $data['evaluator_names'] . '%');
            }
            if ($data['evaluator_surname'] != '') {
                $queryBuilder
                    ->andWhere('u_two.surname LIKE :evaluator_surname')
                    ->setParameter('evaluator_surname', '%' . $data['evaluator_surname'] . '%');
            }
            if ($data['evaluator_lastname'] != '') {
                $queryBuilder
                    ->andWhere('u_two.lastname LIKE :evaluator_lastname')
                    ->setParameter('evaluator_lastname', '%' . $data['evaluator_lastname'] . '%');
            }
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get List Pollapplications Follow
     */
    public static function getListPollapplicationsNotEvaluatedsFollow(DoctrineRegistry $doctrineRegistry, $pollId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id, wi.code evaluator_code, wi.email evaluator_email, ue.surname evaluator_surname, 
                      ue.lastname evaluator_lastname, ue.names evaluator_names, pa.finished')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
            ->innerJoin('wi.user', 'ue')
            ->where('pa.pollscheduling = :pollscheduling  
                    AND pa.status = 1 
                    AND pa.deleted = :deleted
                    AND pa.evaluated_id IS NULL')
            ->setParameter('pollscheduling', $pollId)
            ->setParameter('deleted', FALSE)
            ->orderBy('wi.code', 'ASC');
        
        // Add arguments
        
        if (!empty($data)) {
            if ($data['evaluator_code'] != '') {
                $queryBuilder
                    ->andWhere('wi.code LIKE :evaluator_code')
                    ->setParameter('evaluator_code', '%' . $data['evaluator_code'] . '%');
            }
            if ($data['evaluator_names'] != '') {
                $queryBuilder
                    ->andWhere('ue.names LIKE :evaluator_names')
                    ->setParameter('evaluator_names', '%' . $data['evaluator_names'] . '%');
            }
            if ($data['evaluator_surname'] != '') {
                $queryBuilder
                    ->andWhere('ue.surname LIKE :evaluator_surname')
                    ->setParameter('evaluator_surname', '%' . $data['evaluator_surname'] . '%');
            }
            if ($data['evaluator_lastname'] != '') {
                $queryBuilder
                    ->andWhere('ue.lastname LIKE :evaluator_lastname')
                    ->setParameter('evaluator_lastname', '%' . $data['evaluator_lastname'] . '%');
            }
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * addPollapplications
     */
    public static function addPollapplications(DoctrineRegistry $doctrineRegistry, $evaluators, $evaluateds, $typeapplication, $pollscheduling, $userBy, $workshopschedulingid = '', $additionalAttributes = '', $evaluatorRol = '')
    {   
        $em = $doctrineRegistry->getManager();
        
        $rol = '';
        $numberArray = 0;
        $numberRegister = 0;
        $wa_entity = '';
        $psp_entity = '';
        $messageError = '';
        
        // We travel registers
        
        foreach ($evaluators as $r) {
            
            if (gettype($evaluateds) == 'array') {
                $evaluated = $evaluateds[$numberArray];
                $numberArray++;
            }
            else {
                $evaluated = $evaluateds;
            }
            
            if ($typeapplication == 'workshopapplication') {
                
                // Recover Workshopapplication
                $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $wa_result = $repository->createQueryBuilder('wa')
                    ->where('wa.workshopscheduling = :workshopscheduling
                            AND wa.workinginformation = :workinginformation')
                    ->setParameter('workshopscheduling', $workshopschedulingid)
                    ->setParameter('workinginformation', $r)
                    ->getQuery()
                    ->getResult();
                
                $wa_entity = current($wa_result);
            }
            elseif ($typeapplication == 'pollschedulingpeople') {
                
                // Recover Pollschedulingpeople
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $psp_result = $repository->createQueryBuilder('psp')
                    ->where('psp.pollscheduling = :pollscheduling
                            AND psp.workinginformation = :workinginformation')
                    ->setParameter('pollscheduling', $pollscheduling->getId())
                    ->setParameter('workinginformation', $r)
                    ->getQuery()
                    ->getResult();
                
                $psp_entity = current($psp_result);
            }
            
            if (!empty($wa_entity) || !empty($psp_entity)) {
                
                // Checks whether
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
                $queryBuilder = $repository->createQueryBuilder('pa')
                    ->where('pa.pollscheduling = :pollscheduling 
                            AND pa.evaluator_id = :evaluator')
                    ->setParameter('pollscheduling', $pollscheduling->getId())
                    ->setParameter('evaluator', $r);
                    
                if ($pollscheduling->getType() != 'not_evaluateds') {
                    $queryBuilder
                        ->andWhere('pa.evaluated_id = :evaluated')
                        ->setParameter('evaluated', $evaluated);
                }
                else {
                    $queryBuilder
                        ->andWhere('pa.evaluated_id IS NULL');
                }
                
                $pa_result = $queryBuilder->getQuery()->getResult();
                
                $pa_entity = current($pa_result);
                
                // Check if the assessment is self-assessment 
                
                if ($evaluated == $r && $evaluatorRol != 'self') {
                    $messageError .= 'La evaluación entre los usuarios con id de información ' . $evaluated . ' - ' . $r . ' es una autoevaluación, el rol del eveluador ha sido cambiado a "Autoevaluado".</br>';
                    $rol = 'self';
                }
                elseif ($evaluated != $r && $evaluatorRol == 'self') {
                    $messageError .= 'La evaluación entre los usuarios con id de información ' . $evaluated . ' - ' . $r . ' no es una autoevaluación, el rol del eveluador no ha sido asignado.</br>';
                    $rol = '';
                }
                else {
                    $rol = $evaluatorRol;
                }
                
                // Add Pollapplication
                
                if (!empty($pa_entity)) {
                    
                    if ($additionalAttributes != NULL) {
                        if (gettype($additionalAttributes[0]) == 'array') {
                            $course = $additionalAttributes[0][$numberRegister];
                            $section = $additionalAttributes[1][$numberRegister];
                        }
                        else {
                            $course = $additionalAttributes[0];
                            $section = $additionalAttributes[1];
                        }
                        
                        $em->createQueryBuilder()
                            ->update('FishmanPollBundle:Pollapplication pa')
                            ->set('pa.deleted', ':deleted')
                            ->set('pa.course', ':course')
                            ->set('pa.section', ':section')
                            ->set('pa.evaluator_rol', ':evaluator_rol')
                            ->where('pa.id = :pollapplication')
                            ->setParameter('deleted', FALSE)
                            ->setParameter('course', $course)
                            ->setParameter('section', $section)
                            ->setParameter('evaluator_rol', $rol)
                            ->setParameter('pollapplication', $pa_entity->getId())
                            ->getQuery()
                            ->execute();
                    }
                    else {
                        $em->createQueryBuilder()
                            ->update('FishmanPollBundle:Pollapplication pa')
                            ->set('pa.deleted', ':deleted')
                            ->set('pa.course', ':course')
                            ->set('pa.section', ':section')
                            ->set('pa.evaluator_rol', ':evaluator_rol')
                            ->where('pa.id = :pollapplication')
                            ->setParameter('deleted', FALSE)
                            ->setParameter('course', NULL)
                            ->setParameter('section', NULL)
                            ->setParameter('evaluator_rol', $rol)
                            ->setParameter('pollapplication', $pa_entity->getId())
                            ->getQuery()
                            ->execute();
                    }
                    
                    if ($pa_entity->getDeleted()) {
                        self::generateNotificationexecutions($doctrineRegistry, $pa_entity);
                    }
                }
                else {
                    $pa_entity = new Pollapplication($doctrineRegistry);
                    $token = self::generateCode();
                    
                    $pa_entity->setEntityApplicationType($typeapplication);
                    if ($typeapplication == 'workshopapplication') {
                        $pa_entity->setEntityApplicationId($wa_entity->getId());
                    }
                    elseif ($typeapplication == 'pollschedulingpeople') {
                        $pa_entity->setEntityApplicationId($psp_entity->getId());
                    }
                    $pa_entity->setEvaluatedId($evaluated);
                    $pa_entity->setEvaluatorId($r);
                    $pa_entity->setResolveCount(0);
                    $pa_entity->setDuration($pollscheduling->getDuration());
                    $pa_entity->setPeriod($pollscheduling->getPeriod());
                    $pa_entity->setFinished(FALSE);
                    $pa_entity->setStatus(TRUE);
                    $pa_entity->setPollscheduling($pollscheduling);
                    $pa_entity->setEvaluatorRol($rol);
                    $pa_entity->setCreated(new \DateTime());
                    $pa_entity->setChanged(new \DateTime());
                    $pa_entity->setCreatedBy($userBy->getId());
                    $pa_entity->setModifiedBy($userBy->getId());
                    $pa_entity->setDeleted(FALSE);
                    $pa_entity->setToken($token);
               
                    if ($additionalAttributes != NULL) {
                        $pa_entity->setCourse($additionalAttributes[0]);
                        $pa_entity->setSection($additionalAttributes[1]);
                    }
                    
                    if ($additionalAttributes != NULL) {
                        if (gettype($additionalAttributes[0]) == 'array') {
                            $pa_entity->setCourse($additionalAttributes[0][$numberRegister]);
                            $pa_entity->setSection($additionalAttributes[1][$numberRegister]);
                        }
                        else {
                            $pa_entity->setCourse($additionalAttributes[0]);
                            $pa_entity->setSection($additionalAttributes[1]);
                        }
                    }
                    
                    $em->persist($pa_entity);
                    $em->flush();
                      
                    // Recover Pollquestion of Poll
                    $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
                    $psq_result = $repository->createQueryBuilder('psq')
                        ->where('psq.pollscheduling = :pollscheduling')
                        ->setParameter(':pollscheduling', $pollscheduling->getId())
                        ->orderBy('psq.id', 'ASC')
                        ->getQuery()
                        ->getResult();
                        
                    // We travel records notifications no predecessor
                    
                    foreach ($psq_result as $psq_entity) {
    
                        $paq_entity  = new Pollapplicationquestion();
                        
                        $paq_entity->setFinished(FALSE);
                        $paq_entity->setPollapplication($pa_entity);
                        $paq_entity->setPollSchedulingQuestion($psq_entity);
                        $paq_entity->setScore(0);
                
                        $em->persist($paq_entity);
                    }
                    
                    $em->flush();
                    
                    self::generateNotificationexecutions($doctrineRegistry, $pa_entity);
                }
                
                $numberRegister++;
            }
        }
        
        $output['number_register'] = $numberRegister;
        $output['message_error'] = $messageError;
        
        return $output;
    }

    //jtt: se duplico y modifico para recibir el cargo_global y el nivel_responsabilidad
    /**
     * addPollapplications
     */
    public static function addPollapplicationsAdditional(DoctrineRegistry $doctrineRegistry, $evaluators, $evaluateds, $typeapplication, $pollscheduling, $userBy, $workshopschedulingid = '', $additionalAttributes = '', $evaluatorRol = '', $cargoGlobal = '', $nivelResponsabilidad = '')
    {   
        $em = $doctrineRegistry->getManager();
        
        $rol = '';
        $numberArray = 0;
        $numberRegister = 0;
        $wa_entity = '';
        $psp_entity = '';
        $messageError = '';
        $cargo = $cargoGlobal;
        $nivel = $nivelResponsabilidad;
        
        // We travel registers
        
        foreach ($evaluators as $r) {
            
            if (gettype($evaluateds) == 'array') {
                $evaluated = $evaluateds[$numberArray];
                $numberArray++;
            }
            else {
                $evaluated = $evaluateds;
            }
            
            if ($typeapplication == 'workshopapplication') {
                
                // Recover Workshopapplication
                $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $wa_result = $repository->createQueryBuilder('wa')
                    ->where('wa.workshopscheduling = :workshopscheduling
                            AND wa.workinginformation = :workinginformation')
                    ->setParameter('workshopscheduling', $workshopschedulingid)
                    ->setParameter('workinginformation', $r)
                    ->getQuery()
                    ->getResult();
                
                $wa_entity = current($wa_result);
            }
            elseif ($typeapplication == 'pollschedulingpeople') {
                
                // Recover Pollschedulingpeople
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $psp_result = $repository->createQueryBuilder('psp')
                    ->where('psp.pollscheduling = :pollscheduling
                            AND psp.workinginformation = :workinginformation')
                    ->setParameter('pollscheduling', $pollscheduling->getId())
                    ->setParameter('workinginformation', $r)
                    ->getQuery()
                    ->getResult();
                
                $psp_entity = current($psp_result);
            }
            
            if (!empty($wa_entity) || !empty($psp_entity)) {
                
                // Checks whether
                $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
                $queryBuilder = $repository->createQueryBuilder('pa')
                    ->where('pa.pollscheduling = :pollscheduling 
                            AND pa.evaluator_id = :evaluator')
                    ->setParameter('pollscheduling', $pollscheduling->getId())
                    ->setParameter('evaluator', $r);
                    
                if ($pollscheduling->getType() != 'not_evaluateds') {
                    $queryBuilder
                        ->andWhere('pa.evaluated_id = :evaluated')
                        ->setParameter('evaluated', $evaluated);
                }
                else {
                    $queryBuilder
                        ->andWhere('pa.evaluated_id IS NULL');
                }
                
                $pa_result = $queryBuilder->getQuery()->getResult();
                
                $pa_entity = current($pa_result);
                
                // Check if the assessment is self-assessment 
                
                if ($evaluated == $r && $evaluatorRol != 'self') {
                    $messageError .= 'La evaluación entre los usuarios con id de información ' . $evaluated . ' - ' . $r . ' es una autoevaluación, el rol del eveluador ha sido cambiado a "Autoevaluado".</br>';
                    $rol = 'self';
                }
                elseif ($evaluated != $r && $evaluatorRol == 'self') {
                    $messageError .= 'La evaluación entre los usuarios con id de información ' . $evaluated . ' - ' . $r . ' no es una autoevaluación, el rol del eveluador no ha sido asignado.</br>';
                    $rol = '';
                }
                else {
                    $rol = $evaluatorRol;
                }
                
                // Add Pollapplication
                
                if (!empty($pa_entity)) {
                    
                    if ($additionalAttributes != NULL) {
                        if (gettype($additionalAttributes[0]) == 'array') {
                            $course = $additionalAttributes[0][$numberRegister];
                            $section = $additionalAttributes[1][$numberRegister];
                        }
                        else {
                            $course = $additionalAttributes[0];
                            $section = $additionalAttributes[1];
                        }
                        
                        $em->createQueryBuilder()
                            ->update('FishmanPollBundle:Pollapplication pa')
                            ->set('pa.deleted', ':deleted')
                            ->set('pa.course', ':course')
                            ->set('pa.section', ':section')
                            ->set('pa.evaluator_rol', ':evaluator_rol')
                            ->where('pa.id = :pollapplication')
                            ->setParameter('deleted', FALSE)
                            ->setParameter('course', $course)
                            ->setParameter('section', $section)
                            ->setParameter('evaluator_rol', $rol)
                            ->setParameter('pollapplication', $pa_entity->getId())
                            ->getQuery()
                            ->execute();
                    }
                    else {
                        $em->createQueryBuilder()
                            ->update('FishmanPollBundle:Pollapplication pa')
                            ->set('pa.deleted', ':deleted')
                            ->set('pa.course', ':course')
                            ->set('pa.section', ':section')
                            ->set('pa.evaluator_rol', ':evaluator_rol')
                            ->where('pa.id = :pollapplication')
                            ->setParameter('deleted', FALSE)
                            ->setParameter('course', NULL)
                            ->setParameter('section', NULL)
                            ->setParameter('evaluator_rol', $rol)
                            ->setParameter('cargo_global', $cargo)
                            ->setParameter('nivel_responsabilidad', $nivel)
                            ->setParameter('pollapplication', $pa_entity->getId())
                            ->getQuery()
                            ->execute();
                    }
                    
                    if ($pa_entity->getDeleted()) {
                        self::generateNotificationexecutions($doctrineRegistry, $pa_entity);
                    }
                }
                else {
                    $pa_entity = new Pollapplication($doctrineRegistry);
                    $token = self::generateCode();
                    
                    $pa_entity->setEntityApplicationType($typeapplication);
                    if ($typeapplication == 'workshopapplication') {
                        $pa_entity->setEntityApplicationId($wa_entity->getId());
                    }
                    elseif ($typeapplication == 'pollschedulingpeople') {
                        $pa_entity->setEntityApplicationId($psp_entity->getId());
                    }
                    $pa_entity->setEvaluatedId($evaluated);
                    $pa_entity->setEvaluatorId($r);
                    $pa_entity->setResolveCount(0);
                    $pa_entity->setDuration($pollscheduling->getDuration());
                    $pa_entity->setPeriod($pollscheduling->getPeriod());
                    $pa_entity->setFinished(FALSE);
                    $pa_entity->setStatus(TRUE);
                    $pa_entity->setPollscheduling($pollscheduling);
                    $pa_entity->setEvaluatorRol($rol);
                    $pa_entity->setCreated(new \DateTime());
                    $pa_entity->setChanged(new \DateTime());
                    $pa_entity->setCreatedBy($userBy->getId());
                    $pa_entity->setModifiedBy($userBy->getId());
                    $pa_entity->setDeleted(FALSE);
                    $pa_entity->setToken($token);
                    
                    $pa_entity->setCargoGlobal($cargo);
                    $pa_entity->setNivelResponsabilidad($nivel);
               
                    if ($additionalAttributes != NULL) {
                        $pa_entity->setCourse($additionalAttributes[0]);
                        $pa_entity->setSection($additionalAttributes[1]);
                    }
                    
                    if ($additionalAttributes != NULL) {
                        if (gettype($additionalAttributes[0]) == 'array') {
                            $pa_entity->setCourse($additionalAttributes[0][$numberRegister]);
                            $pa_entity->setSection($additionalAttributes[1][$numberRegister]);
                        }
                        else {
                            $pa_entity->setCourse($additionalAttributes[0]);
                            $pa_entity->setSection($additionalAttributes[1]);
                        }
                    }
                    
                    $em->persist($pa_entity);
                    $em->flush();
                      
                    // Recover Pollquestion of Poll
                    $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
                    $psq_result = $repository->createQueryBuilder('psq')
                        ->where('psq.pollscheduling = :pollscheduling')
                        ->setParameter(':pollscheduling', $pollscheduling->getId())
                        ->orderBy('psq.id', 'ASC')
                        ->getQuery()
                        ->getResult();
                        
                    // We travel records notifications no predecessor
                    
                    foreach ($psq_result as $psq_entity) {
    
                        $paq_entity  = new Pollapplicationquestion();
                        
                        $paq_entity->setFinished(FALSE);
                        $paq_entity->setPollapplication($pa_entity);
                        $paq_entity->setPollSchedulingQuestion($psq_entity);
                        $paq_entity->setScore(0);
                
                        $em->persist($paq_entity);
                    }
                    
                    $em->flush();
                    
                    self::generateNotificationexecutions($doctrineRegistry, $pa_entity);
                }
                
                $numberRegister++;
            }
        }
        
        $output['number_register'] = $numberRegister;
        $output['message_error'] = $messageError;
        
        return $output;
    }

    /**
     * addPollapplication
     */
    public static function addPollapplicationToAnonymous(DoctrineRegistry $doctrineRegistry, $anonymous, $pollscheduling)
    {
        $em = $doctrineRegistry->getManager();
        
        $wiId = $anonymous->getWorkinginformation()->getId();
            
        $pa_entity = new Pollapplication($doctrineRegistry);
        $token = self::generateCode();

        $pa_entity->setEntityApplicationType('pollschedulingpeople');
        $pa_entity->setEntityApplicationId($anonymous->getId());
        $pa_entity->setEvaluatedId($wiId);
        $pa_entity->setEvaluatorId($wiId);
        $pa_entity->setResolveCount(0);
        $pa_entity->setDuration($pollscheduling->getDuration());
        $pa_entity->setPeriod($pollscheduling->getPeriod());
        $pa_entity->setFinished(FALSE);
        $pa_entity->setStatus(TRUE);
        $pa_entity->setPollscheduling($pollscheduling);
        $pa_entity->setCreated(new \DateTime());
        $pa_entity->setChanged(new \DateTime());
        $pa_entity->setCreatedBy(2);
        $pa_entity->setModifiedBy(2);
        $pa_entity->setDeleted(FALSE);
        $pa_entity->setToken($token);
        
        $em->persist($pa_entity);
        $em->flush();
          
        // Recover Pollquestion of Poll
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
        $psq_result = $repository->createQueryBuilder('psq')
            ->where('psq.pollscheduling = :pollscheduling')
            ->setParameter(':pollscheduling', $pollscheduling->getId())
            ->orderBy('psq.id', 'ASC')
            ->getQuery()
            ->getResult();
            
        // We travel records notifications no predecessor
        
        foreach ($psq_result as $psq_entity) {
            
            $paq_entity  = new Pollapplicationquestion();
            
            $paq_entity->setFinished(FALSE);
            $paq_entity->setPollapplication($pa_entity);
            $paq_entity->setPollSchedulingQuestion($psq_entity);
            $paq_entity->setScore(0);
    
            $em->persist($paq_entity);
        }
        
        $em->flush();

        return $pa_entity;
    }

    /**
     * deleteRegisters
     */
    public static function deleteRegisters(DoctrineRegistry $doctrineRegistry, $entity)
    {   
        $em = $doctrineRegistry->getManager();
            
        // Delete pollapplications
        
        $query = $em->createQueryBuilder()
            ->update('FishmanPollBundle:Pollapplication pa')
            ->set('pa.deleted', ':deleted')
            ->where('pa.pollscheduling = :pollscheduling')
            ->setParameter('deleted', TRUE)
            ->setParameter('pollscheduling', $entity->getId())
            ->getQuery()
            ->execute();
    }
    
    /**
     * generateCode
     */
    public static function generateCode($length = 64, $uc = TRUE, $n = TRUE, $sc = FALSE)
    {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($n==1) $source .= '1234567890';
        if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        
        if ($length > 0) {
            $rstr = "";
            $source = str_split($source, 1);
            for ($i = 1; $i <= $length; $i++) {
                mt_srand((double)microtime() * 1000000);
                $num = mt_rand(1,count($source));
                $rstr .= $source[$num-1];
            }

        }
        
        return $rstr;
    }
    
    /*
     * pollValidationAccessControl
     */
    public static function pollValidateAccessControl($logged, $token_on, $pollscheduling, $finished, $terms_on, $message = '')
    {   
        $now = time();
        $initdate = NULL;
        $enddate = NULL;
        
        if ($logged || (!$logged && $token_on)) {
          
            if($pollscheduling->getInitdate() != null){
                $initdate = strtotime($pollscheduling->getInitdate()->format('Y-m-d H:i:s'));
            }
    
            if($pollscheduling->getEnddate() != null){
                $enddate = strtotime($pollscheduling->getEnddate()->format('Y-m-d H:i:s'));
                $enddate = $enddate + (24*60*60);
            }
            
            if ($initdate > $now || $initdate == NULL) {
                $message = 1;
            }
            elseif ($enddate < $now) {
                $message = 2;
            }
            elseif ($finished) {
                $message = 3;
            }
            elseif (!$pollscheduling->getStatus()) {
                $message = 4;
            }
            elseif (!$terms_on && $pollscheduling->getUseTerms()) {
                $message = 'off';
            }
        }
        
        return $message;
    }
    
    /*
     * pollMessage
     */
    public static function pollMessage($logged, $token_on, $term_on, $pollscheduling, $message)
    {
        $output = array('title' => '', 'body' => '', 'edit' => FALSE, 'terms' => FALSE);
        
        switch ($message) {
            case 'off':
                $output['terms'] = TRUE;
            case -1:
                if ($logged || $token_on) {
                    $output['edit'] = TRUE;
                }
                else {
                    $output['title'] = 'Error';
                    $output['body'] = 'No existe la encuesta que está solicitando';
                }
                break;
            case 1:
                $output['title'] = 'Inicio de Encuesta';
                $output['body'] = 'La encuesta no se ha iniciado';
                break;
            case 2:
                $output['title'] = 'Fin de Encuesta';
                $output['body'] = $pollscheduling->getMessageEnd();
                break;
            case 3:
                $output['title'] = 'Agradecimiento';
                $output['body'] = $pollscheduling->getMessageGratitude();
                break;
            case 4:
                $output['title'] = 'Encuesta Inactiva';
                $output['body'] = $pollscheduling->getMessageInactive();
                break;
        }

        return $output;
    }

    /**
     * Get List Evaluateds To Evaluated
     */
    public static function getListPollschedulingEvaluatedsByProfile(DoctrineRegistry $doctrineRegistry, $wsId = '', $psIds, $companyId, $ids = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('wi.id, u.surname, u.lastname, u.names')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->innerJoin('wi.user', 'u')
            ->innerJoin('pa.pollscheduling', 'ps')
            ->where('ps.id IN(' . $psIds . ') 
                    AND ps.status = 1 
                    AND ps.deleted = 0
                    AND pa.deleted = 0
                    AND pa.finished = 1')
            ->orderBy('u.surname', 'ASC', 'u.lastname', 'ASC', 'u.names', 'ASC')
            ->groupBy('pa.evaluated_id');
            
        if ($wsId != '') {
            $queryBuilder
                ->andWhere('ps.entity_type = :entity_type 
                        AND ps.entity_id = :entity_id')
                ->setParameter('entity_type', 'workshopscheduling')
                ->setParameter('entity_id', $wsId);
        }
            
        if ($ids != '') {
            $queryBuilder
                ->andWhere('pa.evaluated_id IN(' . $ids . ')');
        }
        
        if ($companyId != '') {
            $queryBuilder
                ->andWhere('ps.company_id = :company')
                ->setParameter('company', $companyId);
        }
        
        $output = $queryBuilder
            ->getQuery()
            ->getResult();
        
        return $output;
    }

    /**
     * Get Current Evaluated / Evaluator
     */
    public static function getCurrentEvaluatedOrEvaluatorByPollscheduling(DoctrineRegistry $doctrineRegistry, $wsId = '', $psIds, $wiId, $evaluator = FALSE)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('wi.id, u.surname, u.lastname, u.names');
            
        if (!$evaluator) {
            $queryBuilder
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->innerJoin('wi.user', 'u')
                ->innerJoin('pa.pollscheduling', 'ps')
                ->where('ps.status = 1 
                        AND ps.deleted = :ps_deleted
                        AND pa.deleted = :pa_deleted')
                ->andWhere('pa.evaluated_id = :evaluated')
                ->setParameter(':ps_deleted', FALSE)
                ->setParameter(':pa_deleted', FALSE)
                ->setParameter(':evaluated', $wiId)
                ->groupBy('pa.evaluated_id');
        }
        else {
            $queryBuilder
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                ->innerJoin('wi.user', 'u')
                ->innerJoin('pa.pollscheduling', 'ps')
                ->where('ps.status = 1 
                        AND ps.deleted = :ps_deleted
                        AND pa.deleted = :pa_deleted')
                ->andWhere('pa.evaluator_id = :evaluator')
                ->setParameter(':ps_deleted', FALSE)
                ->setParameter(':pa_deleted', FALSE)
                ->setParameter(':evaluator', $wiId)
                ->groupBy('pa.evaluator_id');
        }
        
        if ($wsId != '') {
            $queryBuilder
                ->andWhere('ps.entity_type = :entity_type 
                        AND ps.entity_id = :entity_id')
                ->setParameter(':entity_type', 'workshopscheduling')
                ->setParameter(':entity_id', $wsId);
        }
        
        $query = $queryBuilder
            ->getQuery()
            ->getResult();
        
        $output = current($query);
        
        return $output;
    }

    public static function getListActivePollapplications(DoctrineRegistry $doctrineRegistry, $wiId)
    {
        //TODO: Ver si se necesitan mas campos
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $query = $repository->createQueryBuilder('pa')
            ->select('pa.id, ps.id psid')
            ->innerJoin('pa.pollscheduling', 'ps')
            ->where('pa.evaluated_id = :evaluated 
                    OR pa.evaluator_id = :evaluator')
            ->andWhere('pa.status = 1
                    AND pa.deleted = 0')
            ->setParameter('evaluated', $wiId)
            ->setParameter('evaluator', $wiId)
            ->getQuery();
        $output = $query->getResult();
        
        return $output;

    }
    
    /**
     *
     * Verify number of times the assessed evaluated
     */
    public static function getNumberResolvedPolls(DoctrineRegistry $doctrineRegistry, $pscId, $filterId = '', $typeFilter = 'evaluateds')
    {
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('COUNT(pa.id) number_resolved')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->where('pa.pollscheduling = :pollscheduling
                    AND pa.finished = 1
                    AND pa.status = 1
                    AND pa.deleted = 0')
            ->setParameter('pollscheduling', $pscId);
        
        if ($filterId != '') {
            switch ($typeFilter) {
                case 'evaluateds':
                    $queryBuilder
                        ->andWhere('pa.evaluated_id = :evaluated')
                        ->setParameter('evaluated', $filterId);
                    break;
                case 'organizationalunits':
                    $queryBuilder
                        ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                        ->setParameter('organizationalunit', $filterId);
                    break;
                case 'charges':
                    $queryBuilder
                        ->andWhere('wi.companycharge = :charge')
                        ->setParameter('charge', $filterId);
                    break;
                case 'headquarters':
                    $queryBuilder
                        ->andWhere('wi.headquarter = :headquarter')
                        ->setParameter('headquarter', $filterId);
                    break;
                case 'faculties':
                    $queryBuilder
                        ->andWhere('wi.companyfaculty = :faculty')
                        ->setParameter('faculty', $filterId);
                    break;
                case 'careers':
                    $queryBuilder
                        ->andWhere('wi.companycareer = :career')
                        ->setParameter('career', $filterId);
                    break;
                case 'grades':
                    $queryBuilder
                        ->andWhere('wi.grade = :grade')
                        ->setParameter('grade', $filterId);
                    break;
            }
        }
                
        $query = $queryBuilder->getQuery()->getResult();
            
        $output = $query[0]['number_resolved'];
        
        return $output;
    }
    
    /**
     *
     * Check for participating in the survey
     */
    public static function checkParticipatingPoll(DoctrineRegistry $doctrineRegistry, $pscId, $filterId = '', $typeFilter = '')
    {
        $output = FALSE;
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->where('pa.pollscheduling = :pollscheduling
                    AND pa.status = 1
                    AND pa.deleted = 0')
            ->setParameter('pollscheduling', $pscId);
        
        if ($filterId != '') {
            switch ($typeFilter) {
                case 'evaluateds':
                    $queryBuilder
                        ->andWhere('pa.evaluated_id = :evaluated')
                        ->setParameter('evaluated', $filterId);
                    break;
                case 'organizationalunits':
                    $queryBuilder
                        ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                        ->setParameter('organizationalunit', $filterId);
                    break;
                case 'charges':
                    $queryBuilder
                        ->andWhere('wi.companycharge = :charge')
                        ->setParameter('charge', $filterId);
                    break;
                case 'headquarters':
                    $queryBuilder
                        ->andWhere('wi.headquarter = :headquarter')
                        ->setParameter('headquarter', $filterId);
                    break;
                case 'faculties':
                    $queryBuilder
                        ->andWhere('wi.companyfaculty = :faculty')
                        ->setParameter('faculty', $filterId);
                    break;
                case 'careers':
                    $queryBuilder
                        ->andWhere('wi.companycareer = :career')
                        ->setParameter('career', $filterId);
                    break;
                case 'grades':
                    $queryBuilder
                        ->andWhere('wi.grade = :grade')
                        ->setParameter('grade', $filterId);
                    break;
            }
        }
                
        $query = $queryBuilder->getQuery()->getResult();
            
        if (!empty($query)) {
            $output = TRUE;
        }
        
        return $output;
    }
    
    /**
     *
     * Get Number Evaluators by Evaluated
     */
    public static function getNumberEvaluatorsByEvaluated(DoctrineRegistry $doctrineRegistry, $pscId, $filterId = '', $typeFilter = 'evaluateds', $pscIdOn = FALSE)
    {
        $output = 0;
        
        if (!$pscIdOn) {
            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
            $queryBuilder = $repository->createQueryBuilder('pa')
                ->select('count(pa.id) evaluators')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->where('pa.pollscheduling = :pollscheduling
                        AND pa.status = 1
                        AND pa.deleted = 0
                        AND pa.finished = 1')
                ->setParameter('pollscheduling', $pscId);
        }
        else {
            $pscIds = $pscId;
            
            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
            $queryBuilder = $repository->createQueryBuilder('pa')
                ->select('count(pa.id) evaluators')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->where('pa.pollscheduling IN(' . $pscIds . ') 
                        AND pa.status = 1
                        AND pa.deleted = 0
                        AND pa.finished = 1');
        }
        
        if ($filterId != '') {
            switch ($typeFilter) {
                case 'evaluateds':
                    $queryBuilder
                        ->andWhere('pa.evaluated_id = :evaluated')
                        ->setParameter('evaluated', $filterId);
                    break;
                case 'organizationalunits':
                    $queryBuilder
                        ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                        ->setParameter('organizationalunit', $filterId);
                    break;
                case 'charges':
                    $queryBuilder
                        ->andWhere('wi.companycharge = :charge')
                        ->setParameter('charge', $filterId);
                    break;
                case 'headquarters':
                    $queryBuilder
                        ->andWhere('wi.headquarter = :headquarter')
                        ->setParameter('headquarter', $filterId);
                    break;
                case 'faculties':
                    $queryBuilder
                        ->andWhere('wi.companyfaculty = :faculty')
                        ->setParameter('faculty', $filterId);
                    break;
                case 'careers':
                    $queryBuilder
                        ->andWhere('wi.companycareer = :career')
                        ->setParameter('career', $filterId);
                    break;
                case 'grades':
                    $queryBuilder
                        ->andWhere('wi.grade = :grade')
                        ->setParameter('grade', $filterId);
                    break;
            }
        }
                
        $query = $queryBuilder->getQuery()->getResult();
        
        if (!empty($query)) {
            $output = $query[0]['evaluators'];
        }
        
        return $output;
    }

    public static function getArrayPollapplications(DoctrineRegistry $doctrineRegistry, $pscId, $evaluatorId)
    {
        $output = array();
        
        //TODO: Ver si se necesitan mas campos
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $query = $repository->createQueryBuilder('pa')
            ->select('pa.id, pa.initdate, pa.enddate, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
            ->innerJoin('wi.user', 'u')
            ->where('pa.pollscheduling = :pollscheduling 
                    AND pa.evaluator_id = :evaluator 
                    AND pa.finished = 0 
                    AND pa.deleted = 0')
            ->setParameter('pollscheduling', $pscId)
            ->setParameter('evaluator', $evaluatorId)
            ->getQuery();
        $result = $query->getResult();
        
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $record) {
                $output[$i]['id'] = $record['id'];
                $output[$i]['name'] = $record['names'] . ' ' . $record['surname'] . ' ' . $record['lastname'];
                $output[$i]['initdate'] = $record['initdate'];
                $output[$i]['enddate'] = $record['enddate'];
                $i++;
            }
        }
        
        return $output;

    }
    
    /**
     * Get Pollapplication not Average
     */
    public static function getPollapplicationNotAverage(DoctrineRegistry $doctrineRegistry)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id')
            ->where('pa.average IS NULL 
                    AND pa.deleted = 0 
                    AND pa.finished = 1');
            
        $output = $queryBuilder->getQuery()->getResult();
        
        return $output;
    }
    
    /**
     * Get Average by Pollapplication
     */
    public static function getAverageByPollapplication(DoctrineRegistry $doctrineRegistry, $paId)
    {
        $em = $doctrineRegistry->getManager();
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplicationquestion');
        $queryBuilder = $repository->createQueryBuilder('paq')
            ->select('SUM(paq.score) / SUM(psq.ceil_score) * 100 as percentage')
            ->innerJoin('paq.pollapplication', 'pa')
            ->innerJoin('paq.pollschedulingquestion', 'psq')
            ->where('paq.pollapplication = :pollapplication 
                    AND pa.deleted = 0')
            ->setParameter('pollapplication', $paId);
            
        $query = $queryBuilder->getQuery()->getResult();
        $average = current($query);
        
        $pa_entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($paId);
        
        $pa_entity->setAverage($average['percentage']);
        
        $em->persist($pa_entity);
        $em->flush();
    }
    
    /**
     * Get get Number Pollapplications
     */
    public static function getNumberPollapplications(DoctrineRegistry $doctrineRegistry, $pscIds)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('COUNT(pa.id)')
            ->where('pa.pollscheduling IN(' . $pscIds . ') 
                    AND pa.deleted = 0 
                    AND pa.finished = 1');
            
        $query = $queryBuilder->getQuery()->getResult();
        
        $output = current(current($query));
        
        return $output;
    }
    
    /**
     * Get get Pollapplication max Average Limit
     */
    public static function getPollapplicationMaxAverageLimit(DoctrineRegistry $doctrineRegistry, $limit, $pscIds = '')
    {
        $output = '';
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $queryBuilder = $repository->createQueryBuilder('pa')
            ->select('pa.id')
            ->orderBy('pa.average', 'DESC')
            ->setMaxResults($limit);
        
        if ($pscIds != '') {
            $queryBuilder
                ->where('pa.pollscheduling IN(' . $pscIds . ')');
        }
        $pollapplications = $queryBuilder->getQuery()->getResult();
        
        $i = 0;
        foreach ($pollapplications as $pa) {
            if ($i == 0) {
                $output = $pa['id'];
            }
            else {
                $output .= ',' . $pa['id'];
            }
            $i++;
        }
        
        return $output;
    }
    
    /**
     * Get list rol options
     * 
     */
    public static function getListRolOptions() 
    {
        $output = array( 
            'boss' => 'Jefe', 
            'pair' => 'Par',
            'collaborator' => 'Colaborador',
            'self' => 'Autoevaluado'
        );
        
        return $output;
    }

    /**
     * Get List Evaluators By Evaluated
     */
    public static function getEvaluatorsByEvaluated(DoctrineRegistry $doctrineRegistry, $psId, $evaluated)
    {
        $output = '';
        $evaluators = '';

        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollapplication');
        $query = $repository->createQueryBuilder('pa')
            ->select('pa.id, u.surname, u.lastname, u.names, pa.evaluator_rol, pa.finished')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
            ->innerJoin('wi.user', 'u')
            ->innerJoin('pa.pollscheduling', 'ps')
            ->where('ps.id IN(' . $psId . ') 
                    AND pa.evaluated_id = :evaluated 
                    AND ps.status = 1 
                    AND ps.deleted = 0
                    AND pa.deleted = 0
                    AND pa.finished = 1')
            ->setParameter(':evaluated', $evaluated)
            ->orderBy('u.surname', 'ASC', 'u.lastname', 'ASC', 'u.names', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach ($query as $q) {
            if (!$q['evaluator_rol']) {
                $evaluators['others'][$q['id']] = array(
                    'id' => $q['id'],
                    'name' => $q['surname'] . ' ' . $q['lastname'] . ' ' . $q['names'],
                    'rol' => 'others',
                    'finished' => $q['finished']
                );
            }
            else {
                $evaluators[$q['evaluator_rol']][$q['id']] = array(
                    'id' => $q['id'],
                    'name' => $q['surname'] . ' ' . $q['lastname'] . ' ' . $q['names'],
                    'rol' => $q['evaluator_rol'],
                    'finished' => $q['finished']
                );
            }
        }
        
        $roles = array('self', 'boss', 'pair', 'collaborator', 'others');

        if (isset($evaluators['self'])) {
            foreach ($evaluators['self'] as $e) {
                $output[$e['id']] = $e;
            }
        }
        if (isset($evaluators['boss'])) {
            foreach ($evaluators['boss'] as $e) {
                $output[$e['id']] = $e;
            }
        }
        if (isset($evaluators['pair'])) {
            foreach ($evaluators['pair'] as $e) {
                $output[$e['id']] = $e;
            }
        }
        if (isset($evaluators['collaborator'])) {
            foreach ($evaluators['collaborator'] as $e) {
                $output[$e['id']] = $e;
            }
        }
        if (isset($evaluators['others'])) {
            foreach ($evaluators['others'] as $e) {
                $output[$e['id']] = $e;
            }
        }

        return $output;
    }

    /**
     * Get Report Disaggregated
     */
    public static function getReportDisaggregated(DoctrineRegistry $doctrineRegistry, $arrayPoll, $psId, $evaluated, $parametters)
    {
        $output['valid'] = TRUE;
        $messagePollscheduling = '';
        $numEvaluators = 0;
        $roles = array();
        $ttp = array();
        $evaluators = self::getEvaluatorsByEvaluated($doctrineRegistry, $psId, $evaluated);
        
        if (!empty($evaluators)) {
                
            $numRol = 0;
            $numEvaluatorsRol = 0;
            $rol = '';
            $tcp = array();
            
            foreach ($evaluators as $key => $value) {

                // Verify number of times the assessed evaluated

                if (!$value['finished']) {

                    $messagePollscheduling .= $value['name'] . ' (NO DISPONIBLE)' . '</br>';
                    
                    unset($evaluators[$key]);
                }
                else {

                    // Recover Pollapplicationquestions
                    $pollapplicationquestions = Pollapplicationquestion::getResultPollapplicationquestionDisaggregated($doctrineRegistry, $psId, $value['id']);
                    
                    if (!empty($pollapplicationquestions)) {

                        // Percentage Total Rol
                        
                        if ($rol == '') {
                            $rol = $value['rol'];
                        }
                        if ($rol != $value['rol']) {
                            
                            // Count report rol vertical
                            if (!isset($countVertical)) {
                                $countVertical = count($pollschedulingPercentages[0]);
                            }
                            
                            // Total Rol Rows
                            for ($j = 0; $j < $countVertical; $j++) {
                                $percentageRolRows = 0;
                                for ($n = $numEvaluators - $numEvaluatorsRol; $n < $numEvaluators; $n++) {
                                    $percentageRolRows += $pollschedulingPercentages[$n][$j]['percentage'];
                                }
                                $pollschedulingPercentages[$numEvaluators][$j]['percentage'] = $percentageRolRows / $numEvaluatorsRol;
                            }

                            // Total Row Colum
                            $percentageTotalRolRows = 0;
                            for ($n = $numEvaluators - $numEvaluatorsRol; $n < $numEvaluators; $n++) {
                                $percentageTotalRolRows += $pollschedulingTotals[$n]['percentage'];
                            }
                            $pollschedulingTotals[$numEvaluators]['percentage'] = $percentageTotalRolRows / $numEvaluatorsRol;

                            // Guardamos el nro de columna de promedio que no será considerado para el promedio total del total de filas
                            $tcp[] = $numEvaluators;

                            $numRol++;
                            $numEvaluators++;
                            $numEvaluatorsRol = 1;
                            
                            $rol = $value['rol'];

                        }
                        else {
                            $numEvaluatorsRol++;
                        }
                        
                        // Percentage Evaluator

                        $evaluatorsCols[$numEvaluators] = $value['id'];
                        
                        $pollschedulingReport = Pollscheduling::calculatePercentageSection($arrayPoll, $pollapplicationquestions);
                        
                        $pollschedulingPercentages[$numEvaluators] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections']);
                        
                        $pollschedulingTotals[$numEvaluators] = array(
                            'percentage' => $pollschedulingReport['percentage']['consolidate'], 
                            'count' => $pollschedulingReport['percentage']['count']
                        );
                        
                        switch ($value['rol']) {
                            case 'self':
                                $roles['self'][$value['id']] = $value['name'];
                                break;
                            case 'boss':
                                $roles['boss'][$value['id']] = $value['name'];
                                break;
                            case 'pair':
                                $roles['pair'][$value['id']] = $value['name'];
                                break;
                            case 'collaborator':
                                $roles['collaborator'][$value['id']] = $value['name'];
                                break;
                            case 'others':
                                $roles['others'][$value['id']] = $value['name'];
                                break;
                        }
                        if (!$value['rol']) {
                            $roles['others'][$value['id']] = $value['name'];
                        }
                    }

                    $numEvaluators++;
                }

            }

            // Count report vertical
            $countVertical = count($pollschedulingPercentages[0]);
            
            // Last Total Rol Rows
            for ($j = 0; $j < $countVertical; $j++) {
                $percentageRolRows = 0;
                for ($n = $numEvaluators - $numEvaluatorsRol; $n < $numEvaluators; $n++) {
                    $percentageRolRows += $pollschedulingPercentages[$n][$j]['percentage'];
                }
                $pollschedulingPercentages[$numEvaluators][$j]['percentage'] = $percentageRolRows / $numEvaluatorsRol;
            }

            // Last Total Rol Colum
            $percentageTotalRolRows = 0;
            for ($n = $numEvaluators - $numEvaluatorsRol; $n < $numEvaluators; $n++) {
                $percentageTotalRolRows += $pollschedulingTotals[$n]['percentage'];
            }
            $pollschedulingTotals[$numEvaluators]['percentage'] = $percentageTotalRolRows / $numEvaluatorsRol;

            // Guardamos el nro de columna de promedio que no será considerado para el promedio total del total de filas
            $tcp[] = $numEvaluators;

            $numRol++;
            $numEvaluators++;
            
        }
        
        if (!empty($pollapplicationquestions)) {
            
            // Count report horizontal
            $countHorizontal = $numEvaluators;

            // Headers
            $k = 0;
            if (!empty($evaluators)) {
                foreach ($evaluators as $evaluator) {
                    $pollschedulingHeaders[$k] = $evaluator['name'];
                    $k++;
                }
            }
            
            // Titles
            $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections']);
            
            // Roles
            if (!empty($roles)) {
                $r = 0;
                if (isset($roles['self'])) {
                    $pollschedulingRoles[$r++] = array(
                        'title' => 'Autoevaluado',
                        'count' => count($roles['self'])
                    );
                }
                if (isset($roles['boss'])) {
                    $pollschedulingRoles[$r++] = array(
                        'title' => 'Jefes',
                        'count' => count($roles['boss'])
                    );
                }
                if (isset($roles['pair'])) {
                    $pollschedulingRoles[$r++] = array(
                        'title' => 'Pares',
                        'count' => count($roles['pair'])
                    );
                }
                if (isset($roles['collaborator'])) {
                    $pollschedulingRoles[$r++] = array(
                        'title' => 'Colaboradores',
                        'count' => count($roles['collaborator'])
                    );
                }
                if (isset($roles['others'])) {
                    $pollschedulingRoles[$r++] = array(
                        'title' => 'Otros',
                        'count' => count($roles['others'])
                    );
                }
            }
            
            // Total Rows
            for ($j = 0; $j < $countVertical; $j++) {
                $percentageTotalRow = 0;
                for ($n = 0; $n < $countHorizontal; $n++) {
                    if (!in_array($n, $tcp)) {
                        $percentageTotalRow += $pollschedulingPercentages[$n][$j]['percentage'];
                    }
                }
                $pollschedulingPercentageRows[$j] = $percentageTotalRow / ($countHorizontal - $numRol);
            }

            // Row Results
            $row_total = 0;
            $t = 0;
            foreach ($pollschedulingTotals as $total) {
                if (!in_array($t, $tcp)) {
                    $row_total += $total['percentage'];
                }
                $t++;
            }
            $pollschedulingTotalRows = $row_total / ($countHorizontal - $numRol);
            
            $parametters['header_results'] = $pollschedulingHeaders;
            $parametters['title_results'] = $pollschedulingTitles;
            $parametters['rol_results'] = $pollschedulingRoles;
            $parametters['report_results'] = $pollschedulingPercentages;
            $parametters['report_rows'] = $pollschedulingPercentageRows;
            $parametters['total_results'] = $pollschedulingTotals;
            $parametters['row_results'] = $pollschedulingTotalRows;
            $parametters['count_horizontal'] = $countHorizontal;
            $parametters['count_vertical'] = $countVertical;
            $parametters['num_evaluators'] = $numEvaluators;
            $parametters['num_rols'] = $numRol;
            $parametters['item_averages'] = $tcp;
            
        }
        else {
            $output['valid'] = FALSE;
        }
        
        $output['message'] = $messagePollscheduling;
        $output['parametters'] = $parametters;

        return $output;
    }

    /**
     * Get Report PTT
     */
    public static function getReportPTT(DoctrineRegistry $doctrineRegistry, $pId, $psId, $evaluated, $numEvaluators)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
        $query = $repository->createQueryBuilder('psq')
            ->select('pq.id, pq.type, pq.options')
            ->innerJoin('psq.pollquestion', 'pq')
            ->where('psq.pollscheduling = :pollscheduling')
            ->setParameter('pollscheduling', $psId)
            ->orderBy('psq.id', 'ASC')
            ->getQuery()
            ->getResult();

        foreach ($query as $result) {
            if (in_array($result['type'], array('selection_option', 'unique_option'))) {
                $count = count($result['options']);
                $i = 0;
                $j = 0;
                foreach ($result['options'] as $key => $value) {
                    if ($i+2 >= $count) {
                        $psqs[$result['id']][$j] = $key;
                        $j++;
                    }
                    $i++;
                }
            }
            else {
                $psqs[$result['id']] = NULL;
            }
        }
        
        foreach ($psqs as $key => $value) {

            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
            $query = $repository->createQueryBuilder('psq')
                ->select('pq.id, count(paq.score) as ptt')
                ->innerJoin('psq.pollquestion', 'pq')
                ->innerJoin('FishmanPollBundle:Pollapplicationquestion', 'paq', 'WITH', 'paq.pollschedulingquestion = psq.id')
                ->innerJoin('paq.pollapplication', 'pa')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->where('psq.pollscheduling = :pollscheduling 
                        AND pq.id = :pollquestion
                        AND wi.id = :workinginformation
                        AND pa.finished = 1')
                ->andWhere('paq.answer_options LIKE :option_one
                        OR paq.answer_options LIKE :option_two')
                ->setParameter('pollscheduling', $psId)
                ->setParameter('pollquestion', $key)
                ->setParameter('workinginformation', $evaluated)
                ->setParameter('option_one', '%"' . $value[0] . '"%')
                ->setParameter('option_two', '%"' . $value[1] . '"%')
                ->orderBy('psq.id', 'ASC')
                ->getQuery()
                ->getResult();
            
            $paq[$query[0]['id']]['percentage'] = $query[0]['ptt'] / $numEvaluators * 100;

        }
        
        $arrayPoll['sections'] = Pollsection::arraySectionsAndQuestionsPTT($doctrineRegistry, $pId);
        
        $reportPercentagePTT = Pollscheduling::calculatePercentageSectionPTT($arrayPoll, $paq);

        $reportPTT['total'] = $reportPercentagePTT['percentage']['consolidate'];

        $reportPTT['percentages'] = Pollscheduling::percentagePollschedulingWithoutIndentation($reportPercentagePTT['sections']);
        
        return $reportPTT;
    }

}