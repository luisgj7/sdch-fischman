<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fishman\WorkshopBundle\Workshop;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\PollBundle\Entity\Entitypoll
 */
class Entitypoll
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $entity_id
     */
    private $entity_id;

    /**
     * @var string $entity_type
     */
    private $entity_type;

    /**
     * @var integer $duration
     */
    private $duration;

    /**
     * @var string $period
     */
    private $period;

    /**
     * @var integer $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Poll", inversedBy="entitypolls")
     * @ORM\JoinColumn(name="poll_id", referencedColumnName="id")
     */
    protected $poll;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entity_id
     *
     * @param integer $entityId
     * @return Entitypoll
     */
    public function setEntityId($entityId)
    {
        $this->entity_id = $entityId;
    
        return $this;
    }

    /**
     * Get entity_id
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entity_id;
    }

    /**
     * Set entity_type
     *
     * @param string $entityType
     * @return Entitypoll
     */
    public function setEntityType($entityType)
    {
        $this->entity_type = $entityType;
    
        return $this;
    }

    /**
     * Get entity_type
     *
     * @return string 
     */
    public function getEntityType()
    {
        return $this->entity_type;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Entitypoll
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set period
     *
     * @param string $period
     * @return Entitypoll
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return string 
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Entitypoll
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Entitypoll
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Entitypoll
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Entitypoll
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Entitypoll
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Entitypoll
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set poll
     *
     * @param Fishman\PollBundle\Entity\Poll $poll
     * @return Entitypoll
     */
    public function setPoll(\Fishman\PollBundle\Entity\Poll $poll = null)
    {
        $this->poll = $poll;
    
        return $this;
    }

    /**
     * Get poll
     *
     * @return Fishman\PollBundle\Entity\Poll 
     */
    public function getPoll()
    {
        return $this->poll;
    }

    public static function getPollListOfWorkshop($repository, $workshop, $first, $typeId)
    {
        $output = '';
        $array_poll = '';
        
        $query = $repository->createQueryBuilder('ep')
           ->where('ep.entity_id = :poll_id')
           ->setParameter('poll_id', $workshop)
           ->orderBy('ep.sequence', 'ASC', 'ep.id', 'ASC')
           ->getQuery();
        $polls = $query->getResult();
        
        $output = '<select class="type_notification" name="fishman_notificationbundle_notificationtype[notification_type_id]">';
        $output .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        foreach ($polls as $poll) {
            $pid = $poll->getPoll()->getId();
            
            if (!isset($array_poll[$pid])) {
                $array_poll[$pid] = 1;
            }
            else {
                $array_poll[$pid] = $array_poll[$pid] + 1;
            }
            
            if ($first) {
                $name = $poll->getPoll()->getTitle() . ' (' . $array_poll[$pid] . ')';
                $first = false;
            }
            if ($typeId == $poll->getPoll()->getId()) {
                $output .= '<option selected="selected" value="' . $poll->getId() . '">' . $poll->getPoll()->getTitle() . ' (' . $array_poll[$pid] . ')</option>';
            }
            else {
                $output .= '<option value="' . $poll->getId() . '">' . $poll->getPoll()->getTitle() . ' (' . $array_poll[$pid] . ')</option>';
            }
        }
        $output .= '</select>';
  
        return $output;
    }
}