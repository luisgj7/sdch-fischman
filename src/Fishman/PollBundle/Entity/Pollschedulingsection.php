<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\PollBundle\Entity\Pollschedulingsection
 */
class Pollschedulingsection
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var boolean $type
     */
    private $type;

    /**
     * @var integer $parent_id
     */
    private $parent_id;

    /**
     * @var smallint $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var smallint $sequence
     */
    private $l1;

    /**
     * @var smallint $sequence
     */
    private $l2;

    /**
     * @var smallint $sequence
     */
    private $l3;

    /**
     * @var smallint $sequence
     */
    private $l4;

    /**
     * @var smallint $sequence
     */
    private $l5;

    /**
     * @var smallint $sequence
     */
    private $l6;

    /**
     * @var smallint $sequence
     */
    private $l7;

    /**
     * @var smallint $sequence
     */
    private $l8;

    /**
     * @var smallint $sequence
     */
    private $l9;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollsection", inversedBy="pollschedulingsections")
     * @ORM\JoinColumn(name="pollsection_id", referencedColumnName="id")
     */
    protected $pollsection;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollscheduling", inversedBy="pollschedulingsections")
     * @ORM\JoinColumn(name="pollscheduling_id", referencedColumnName="id")
     */
    protected $pollscheduling;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollschedulingquestion", mappedBy="pollschedulingsection")
     */
    protected $pollschedulingquestions; 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Pollschedulingsection
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Pollsection
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Pollsection
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the parent id.
     * @return integer the parent id if the node not have parent then return -1
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Set de parent of the node. If 
     * @param integer $parent_id.
     */
    public function setParentId($parentId)
    {
        $this->parent_id = $parentId;
    }

    /**
     * Set sequence
     *
     * @param smallint $sequence
     * @return Pollsection
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return smallint 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Pollsection
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set l1
     *
     * @param string $l1
     * @return Pollquestion
     */
    public function setL1($l1)
    {
        $this->l1 = $l1;
    
        return $this;
    }

    /**
     * Get l1
     *
     * @return string 
     */
    public function getL1()
    {
        return $this->l1;
    }

    /**
     * Set l2
     *
     * @param string $l2
     * @return Pollquestion
     */
    public function setL2($l2)
    {
        $this->l2 = $l2;
    
        return $this;
    }

    /**
     * Get l2
     *
     * @return string 
     */
    public function getL2()
    {
        return $this->l2;
    }

    /**
     * Set l3
     *
     * @param string $l3
     * @return Pollquestion
     */
    public function setL3($l3)
    {
        $this->l3 = $l3;
    
        return $this;
    }

    /**
     * Get l3
     *
     * @return string 
     */
    public function getL3()
    {
        return $this->l3;
    }

    /**
     * Set l4
     *
     * @param string $l4
     * @return Pollquestion
     */
    public function setL4($l4)
    {
        $this->l4 = $l4;
    
        return $this;
    }

    /**
     * Get l4
     *
     * @return string 
     */
    public function getL4()
    {
        return $this->l4;
    }

    /**
     * Set l5
     *
     * @param string $l5
     * @return Pollquestion
     */
    public function setL5($l5)
    {
        $this->l5 = $l5;
    
        return $this;
    }

    /**
     * Get l5
     *
     * @return string 
     */
    public function getL5()
    {
        return $this->l5;
    }

    /**
     * Set l6
     *
     * @param string $l6
     * @return Pollquestion
     */
    public function setL6($l6)
    {
        $this->l6 = $l6;
    
        return $this;
    }

    /**
     * Get l6
     *
     * @return string 
     */
    public function getL6()
    {
        return $this->l6;
    }

    /**
     * Set l7
     *
     * @param string $l7
     * @return Pollquestion
     */
    public function setL7($l7)
    {
        $this->l7 = $l7;
    
        return $this;
    }

    /**
     * Get l7
     *
     * @return string 
     */
    public function getL7()
    {
        return $this->l7;
    }

    /**
     * Set l8
     *
     * @param string $l8
     * @return Pollquestion
     */
    public function setL8($l8)
    {
        $this->l8 = $l8;
    
        return $this;
    }

    /**
     * Get l8
     *
     * @return string 
     */
    public function getL8()
    {
        return $this->l8;
    }

    /**
     * Set l9
     *
     * @param string $l9
     * @return Pollquestion
     */
    public function setL9($l9)
    {
        $this->l9 = $l9;
    
        return $this;
    }

    /**
     * Get l9
     *
     * @return string 
     */
    public function getL9()
    {
        return $this->l9;
    }
   
    /**
     * Set pollsection
     *
     * @param Fishman\PollBundle\Entity\Pollsection $pollsection
     * @return Pollschedulingsection
     */
    public function setPollsection(\Fishman\PollBundle\Entity\Pollsection $pollsection = null)
    {
        $this->pollsection = $pollsection;
    
        return $this;
    }

    /**
     * Get pollsection
     *
     * @return Fishman\PollBundle\Entity\Pollsection 
     */
    public function getPollsection()
    {
        return $this->pollsection;
    }
   
    /**
     * Set pollscheduling
     *
     * @param Fishman\PollBundle\Entity\Pollscheduling $pollscheduling
     * @return Pollschedulingsection
     */
    public function setPollscheduling(\Fishman\PollBundle\Entity\Pollscheduling $pollscheduling = null)
    {
        $this->pollscheduling = $pollscheduling;
    
        return $this;
    }

    /**
     * Get pollscheduling
     *
     * @return Fishman\PollBundle\Entity\Pollscheduling 
     */
    public function getPollscheduling()
    {
        return $this->pollscheduling;
    }

    public function __toString()
    {
         return $this->name;
    }  
                  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollschedulingquestions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add pollschedulingquestions
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions
     * @return Pollschedulingsection
     */
    public function addPollschedulingquestion(\Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions)
    {
        $this->pollschedulingquestions[] = $pollschedulingquestions;
    
        return $this;
    }

    /**
     * Remove pollschedulingquestions
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions
     */
    public function removePollschedulingquestion(\Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions)
    {
        $this->pollschedulingquestions->removeElement($pollschedulingquestions);
    }

    /**
     * Get pollschedulingquestion
     *
     * @return Fishman\PollBundle\Entity\Pollschedulingquestion 
     */
    public function getPollschedulingquestions()
    {
        return $this->pollschedulingquestions;
    }

    /**
     * getSectionId
     */
    public static function getSectionId(DoctrineRegistry $doctrineRegistry, $pollschedulingid, $pollsectionid)
    {
        $output = '';
           
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingsection');
        $result = $repository->createQueryBuilder('pss')
            ->where('pss.pollscheduling = :pollschedulingid 
                    AND pss.pollsection = :pollsectionid')
            ->setParameter('pollschedulingid', $pollschedulingid)
            ->setParameter('pollsectionid', $pollsectionid)
            ->getQuery()
            ->getResult();
       
       if (!empty($result)) {
          $record = current($result);
          $output = $record->getId();
       }
       
       return $output;
    }

    /**
     * getArraySectionShow
     */
    public static function getArraySectionShow(DoctrineRegistry $doctrineRegistry, $array, $pollschedulingid, $page)
    {
        $output = '';
        $page--;
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingsection');
        $result = $repository->createQueryBuilder('pss')
            ->select('pss.id')
            ->where('pss.pollscheduling = :pollschedulingid 
                    AND pss.parent_id = :parent_id')
            ->setParameter('pollschedulingid', $pollschedulingid)
            ->setParameter('parent_id', '-1')
            ->orderBy('pss.sequence', 'ASC')
            ->setFirstResult($page)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
            
        $record = current($result);
        
        if (!empty($record)) {
            $output['sections'][$record['id']] = $array['sections'][$record['id']];
        }
        
        return $output;
    }

    /**
     * getArrayNumberQuestions
     */
    public static function getArrayNumberQuestions(DoctrineRegistry $doctrineRegistry, $array, $pollschedulingid, $page, $number)
    {
        $em = $doctrineRegistry->getManager();
        
        $output = '';
        $page--;
        $init = $page * $number;
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        
        if ($pollscheduling->getLevel() != 0) {
            
            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
            $result = $repository->createQueryBuilder('psq')
                ->select('psq.id, pss.l1')
                ->innerJoin('psq.pollschedulingsection', 'pss')
                ->where('psq.pollscheduling = :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->setFirstResult($init)
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();
            
            $record = current($result);
            
            if (!empty($record)) {
                $output = self::getArrayNumberQuestionsCurrent($array, $record['l1'], $init, $number);
            }
        }
        else {
            $output = self::getArrayNumberQuestionsCurrentNotSection($array, $init, $number);
        }
        
        return $output;
    }

    /**
     * getArrayNumberQuestionsCurrent
     */
    public static function getArrayNumberQuestionsCurrent($array, $section_level, &$init, &$number, &$set = FALSE)
    {
        $output = '';
        
        if (isset($array)) {
            foreach ($array as $key_section => $section) {
              
                if ($number <= 0) {
                    $set = FALSE;
                }
                
                if ($key_section) {
                    
                    if ($section_level == $key_section) {
                        $set = TRUE;
                    }
                    
                    if (($set || $init <= 0) && $number > 0) {
                        $output['sections'][$key_section]['title'] = $section['title'];
                        if (isset($section['description'])) {
                            $output['sections'][$key_section]['description'] = $section['description'];
                        }
                    }
                    
                    if (isset($section['questions'])) {
                        foreach ($section['questions'] as $key_question => $question) {
                            $init--;
                            if ($init < 0) {
                                $number--;
                                if ($number >= 0) {
                                    $output['sections'][$key_section]['questions'][$key_question] = $question;
                                }
                                else {
                                    $set = FALSE;
                                }
                            }
                        }
                    }
                    
                    if (isset($section['sections'])) {
                        $children_sections = self::getArrayNumberQuestionsCurrent($section['sections'], $section_level, $init, $number, $set);
                        
                        if (!empty($children_sections)) {
                            $output['sections'][$key_section]['sections'] = $children_sections['sections'];
                        }
                    }
                }
                else {
                    if (isset($section['questions'])) {
                        foreach ($section['questions'] as $key_question => $question) {
                            $init--;
                            if ($init < 0) {
                                $number--;
                                if ($number >= 0) {
                                    $output['sections'][$key_section]['questions'][$key_question] = $question;
                                }
                                else {
                                    $set = FALSE;
                                }
                            }
                        }
                    }
                }
                
            }
        }
        
        return $output;
        
    }

    /**
     * getArrayNumberQuestionsCurrentNotSection
     */
    public static function getArrayNumberQuestionsCurrentNotSection($array, &$init, &$number, &$set = FALSE)
    {
        $output = '';
        
        if (isset($array)) {
            foreach ($array as $key_section => $section) {
              
                if ($number <= 0) {
                    $set = FALSE;
                }
                
                if (isset($section['questions'])) {
                    foreach ($section['questions'] as $key_question => $question) {
                        $init--;
                        if ($init < 0) {
                            $number--;
                            if ($number >= 0) {
                                $output['sections'][$key_section]['questions'][$key_question] = $question;
                            }
                            else {
                                $set = FALSE;
                            }
                        }
                    }
                }
                
            }
        }
        
        return $output;
        
    }

    /**
     * @return array with all organizational units of this company
     */
    public function getParentOptions(DoctrineRegistry $doctrine, $psId, $withoutparentoption = true)
    {
        $options = array();
        if($withoutparentoption){
            $options = array(-1 => 'NINGUNO');
        }

        self::addChoiceOptions($doctrine, $this->getRootParents($doctrine, $psId), 0, $options, $this->getId());
        return $options;
    }

    public static function addChoiceOptions(DoctrineRegistry $doctrine, $nodes, $level, &$options, $ignore_id)
    {
        $levelIndicator = str_pad('', $level, '---' , STR_PAD_LEFT);
        foreach($nodes as $node){
            if($node['id'] != $ignore_id){
                $options[$node['id']] = $levelIndicator . ' ' . $node['name'];
                self::addChoiceOptions($doctrine, self::getChildren($doctrine, $node['id']), $level + 3, $options, $ignore_id);
            }
        }
    }

    public static function getChildren(DoctrineRegistry $doctrine, $parent_id)
    {
        $repository = $doctrine->getRepository('FishmanPollBundle:Pollschedulingsection');
        $query = $repository->createQueryBuilder('pss')
            ->select('pss.id, pss.name')
            ->where('pss.parent_id = :parent_id')
            ->setParameter('parent_id', $parent_id)
            ->orderBy('pss.id', 'ASC')
            ->getQuery();
        return $query->getResult();
    }

    public static function getRootParents(DoctrineRegistry $doctrine, $psId)
    {
        // Get all root CompanyOrganizationaUnit From this Company
        $repository = $doctrine->getRepository('FishmanPollBundle:Pollschedulingsection');
        $query = $repository->createQueryBuilder('pss')
            ->select('pss.id, pss.name')
            ->where('pss.pollscheduling = :pollscheduling
                     AND pss.parent_id = :parent_id
                     AND pss.status = 1')
            ->setParameter('pollscheduling', $psId)
            ->setParameter('parent_id', -1)
            ->orderBy('pss.name', 'ASC')
            ->getQuery();
        return $query->getResult();
    }
    
    public static function getPollschedulingSectionsOptions(DoctrineRegistry $doctrine, $psId)
    {
        $options = array();
        
        //Ignore_id argument = -1, because we want all options
        self::addChoiceOptions($doctrine, self::getRootParents($doctrine, $psId), 0, $options, -1);
        return $options;
    }

    /**
     * updateRegisterParentId
     */
    public static function updateRegisterParentId(DoctrineRegistry $doctrineRegistry, $entity)
    {   
        $em = $doctrineRegistry->getManager();
        
        if ($entity->getParentId() == '-1') {
            $entity->setL1($entity->getId());
            $entity->setL2(NULL);
            $entity->setL3(NULL);
            $entity->setL4(NULL);
            $entity->setL5(NULL);
            $entity->setL6(NULL);
            $entity->setL7(NULL);
            $entity->setL8(NULL);
            $entity->setL9(NULL);
        }
        else {
          
            $level = FALSE;
            
            if ($entity->getL2() == '') {
                $entity->setL2($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL3() == '' && !$level) {
                $entity->setL3($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL4() == '' && !$level) {
                $entity->setL4($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL5() == '' && !$level) {
                $entity->setL5($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL6() == '' && !$level) {
                $entity->setL6($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL7() == '' && !$level) {
                $entity->setL7($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL8() == '' && !$level) {
                $entity->setL8($entity->getId());
                $level = TRUE;
            }
            if ($entity->getL9() == '' && !$level) {
                $entity->setL9($entity->getId());
                $level = TRUE;
            }
        }
        
        // Update Pollschedulingsections
        self::updateSections($doctrineRegistry, $entity, $entity->getId());

        $em->persist($entity);
        $em->flush();

    }

    /**
     * updateSections
     */
    public static function updateSections(DoctrineRegistry $doctrineRegistry, $entity, $parentId)
    {   
        $em = $doctrineRegistry->getManager();
            
        // Update Pollsection
        
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingsection');
        $results = $repository->createQueryBuilder('pss')
            ->where('pss.parent_id = :parent_id 
                    AND pss.pollscheduling = :pollscheduling')
            ->setParameter('parent_id', $parentId)
            ->setParameter('pollscheduling', $entity->getPollscheduling())
            ->orderBy('pss.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        if (!empty($results)) {
            foreach ($results as $r) {
                $level = FALSE;
                if ($entity->getL1() == '') {
                    $r->setL1($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL1($entity->getL1());
                }
                if ($entity->getL2() == '' && !$level) {
                    $r->setL2($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL2($entity->getL2());
                }
                if ($entity->getL3() == '' && !$level) {
                    $r->setL3($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL3($entity->getL3());
                }
                if ($entity->getL4() == '' && !$level) {
                    $r->setL4($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL4($entity->getL4());
                }
                if ($entity->getL5() == '' && !$level) {
                    $r->setL5($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL5($entity->getL5());
                }
                if ($entity->getL6() == '' && !$level) {
                    $r->setL6($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL6($entity->getL6());
                }
                if ($entity->getL7() == '' && !$level) {
                    $r->setL7($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL7($entity->getL7());
                }
                if ($entity->getL8() == '' && !$level) {
                    $r->setL8($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL8($entity->getL8());
                }
                if ($entity->getL9() == '' && !$level) {
                    $r->setL9($r->getId());
                    $level = TRUE;
                }
                else {
                    $r->setL9($entity->getL9());
                }
            
                $em->persist($r);
                $em->flush();
            
                self::updateSections($doctrineRegistry, $r, $r->getId());
            }
        }
    }

    /**
     * arraySectionsAndQuestions
     */
    public static function arraySectionsAndQuestions(DoctrineRegistry $doctrineRegistry, $psId, $parent_id = '-1')
    {
        $output = '';
        
        $em = $doctrineRegistry->getManager();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($psId);
        
        if ($pollscheduling->getLevel() > 0) {
            
            // Recover Pollsections to Pollscheduling
            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingsection');
            $sections = $repository->createQueryBuilder('pss')
                ->where('pss.parent_id = :parent_id 
                        AND pss.pollscheduling = :pollscheduling
                        AND pss.status = 1')
                ->setParameter('parent_id', $parent_id)
                ->setParameter('pollscheduling', $psId)
                ->orderBy('pss.sequence', 'ASC', 'pss.id', 'ASC')
                ->getQuery()
                ->getResult();
            
            if (!empty($sections)) {
                foreach ($sections as $section) {
    
                    $output[$section->getId()]['title'] = $section->getName();
                    $output[$section->getId()]['description'] = $section->getDescription();
                        
                    $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
                    $questions = $repository->createQueryBuilder('psq')
                        ->where('psq.pollschedulingsection = :pollschedulingsection 
                                AND psq.pollscheduling = :pollscheduling
                                AND psq.status = 1')
                        ->setParameter('pollschedulingsection', $section)
                        ->setParameter('pollscheduling', $psId)
                        ->orderBy('psq.sequence', 'ASC', 'psq.id', 'ASC')
                        ->getQuery()
                        ->getResult();
                    
                    if (!empty($questions)) {
                        foreach ($questions as $question) {
                            
                            $output[$section->getId()]['questions'][$question->getId()] = array(
                                'id' => $question->getId(),
                                'question' => $question->getQuestion(), 
                                'type' => $question->getType(), 
                                'alignment' => $question->getAlignment(), 
                                'options' => $question->getOptions(), 
                                'mandatory_response' => $question->getMandatoryResponse(), 
                                'sequence' => $question->getSequence(), 
                                'status' => $question->getStatus()
                            );
                            
                        }
                    }
    
                    $children_sections = self::arraySectionsAndQuestions($doctrineRegistry, $section->getPollscheduling()->getId(), $section->getId());
                    
                    if (!empty($children_sections)) {
                        $output[$section->getId()]['sections'] = $children_sections;
                    }
                }
            }
            
        }
        else {
            
            $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollschedulingquestion');
            $questions = $repository->createQueryBuilder('psq')
                ->where('psq.pollscheduling = :pollscheduling
                        AND psq.status = 1')
                ->setParameter('pollscheduling', $psId)
                ->orderBy('psq.sequence', 'ASC', 'psq.id', 'ASC')
                ->getQuery()
                ->getResult();
            
            if (!empty($questions)) {
                foreach ($questions as $question) {
                    
                    $output[0]['questions'][$question->getId()] = array(
                        'id' => $question->getId(),
                        'question' => $question->getQuestion(), 
                        'type' => $question->getType(), 
                        'alignment' => $question->getAlignment(), 
                        'options' => $question->getOptions(), 
                        'mandatory_response' => $question->getMandatoryResponse(), 
                        'sequence' => $question->getSequence(), 
                        'status' => $question->getStatus()
                    );
                    
                }
            }
            
        }
        
        return $output;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pollschedulingsection
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Pollschedulingsection
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Pollschedulingsection
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Pollschedulingsection
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
}