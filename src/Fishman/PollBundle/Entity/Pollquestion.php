<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Fishman\PollBundle\Entity\Pollsection;

/**
 * Fishman\PollBundle\Entity\Pollquestion
 */
class Pollquestion
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $question
     */
    private $question;

    /**
     * @var string $type
     */
    private $type;
    
    /**
     * @var string $alignment
     */
    private $alignment;
    
    /**
     * @var string $options
     */
    private $options;

    /**
     * @var boolean $mandatory_response
     */
    private $mandatory_response;

    /**
     * @var string $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Pollsection", inversedBy="pollquestions")
     * @ORM\JoinColumn(name="pollsection_id", referencedColumnName="id")
     */
    protected $pollsection;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\PollBundle\Entity\Poll", inversedBy="pollquestions")
     * @ORM\JoinColumn(name="poll_id", referencedColumnName="id")
     */
    protected $poll;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\PollBundle\Entity\Pollschedulingquestion", mappedBy="pollquestion")
     */
    protected $pollschedulingquestions; 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question 
     *
     * @param string $question
     * @return Pollquestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question 
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Pollquestion
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set alignment
     *
     * @param string $alignment
     * @return Pollquestion
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
    
        return $this;
    }

    /**
     * Get alignment
     *
     * @return string 
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return Pollquestion
     */
    public function setOptions($options)
    {
        $this->options = $options;
    
        return $this;
    }

    /**
     * Get options
     *
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set mandatory_response
     *
     * @param boolean $mandatoryResponse
     * @return Pollquestion
     */
    public function setMandatoryResponse($mandatoryResponse)
    {
        $this->mandatory_response = $mandatoryResponse;
    
        return $this;
    }

    /**
     * Get mandatory_response
     *
     * @return boolean
     */
    public function getMandatoryResponse()
    {
        return $this->mandatory_response;
    }

    /**
     * Set sequence 
     *
     * @param string $sequence
     * @return Pollquestion
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return string 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Pollquestion
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pollquestion
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Pollquestion
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Pollquestion
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Pollquestion
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    public function __toString()
    {
         return $this->question;
    }
   
    /**
     * Set poll
     *
     * @param Fishman\PollBundle\Entity\Poll $poll
     * @return Pollquestion
     */
    public function setPoll(\Fishman\PollBundle\Entity\Poll $poll = null)
    {
        $this->poll = $poll;
    
        return $this;
    }

    /**
     * Get poll
     *
     * @return Fishman\PollBundle\Entity\Poll 
     */
    public function getPoll()
    {
        return $this->poll;
    }

    /**
     * Set pollsection
     *
     * @param Fishman\PollBundle\Entity\Pollsection $pollsection
     * @return Pollquestion
     */
    public function setPollsection(\Fishman\PollBundle\Entity\Pollsection $pollsection = null)
    {
        $this->pollsection = $pollsection;
    
        return $this;
    }

    /**
     * Get pollsection
     *
     * @return Fishman\PollBundle\Entity\Pollsection 
     */
    public function getPollsection()
    {
        return $this->pollsection;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollschedulingquestions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add pollschedulingquestions
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions
     * @return Pollquestion
     */
    public function addPollschedulingquestion(\Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions)
    {
        $this->pollschedulingquestions[] = $pollschedulingquestions;
    
        return $this;
    }

    /**
     * Remove pollschedulingquestions
     *
     * @param Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions
     */
    public function removePollschedulingquestion(\Fishman\PollBundle\Entity\Pollschedulingquestion $pollschedulingquestions)
    {
        $this->pollschedulingquestions->removeElement($pollschedulingquestions);
    }

    /**
     * Get pollschedulingquestions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPollschedulingquestions()
    {
        return $this->pollschedulingquestions;
    }

    /**
     * Get list type options
     * 
     */
    public static function getListTypeOptions() 
    {
        $output = array(
            'simple_text' => 'Cuadro de texto simple', 
            'unique_option' => 'Opción única', 
            'multiple_option' => 'Opción múltiple', 
            'selection_option' => 'Opción selección', 
            'multiple_text' => 'Cuadro de texto múltiple'
        );
        
        return $output;
    }

    /**
     * cloneRegisters
     */
    public static function cloneRegisters(DoctrineRegistry $doctrineRegistry, $entity, $array, $level, $userBy, $psId = NULL)
    {   
        $em = $doctrineRegistry->getManager();
        
        if ($entity->getLevel() === 0) {
            $level = 0;
        }
        
        if (!empty($array)) {
            if (isset($array['sections']) && $array['sections'] != '') {
                foreach ($array['sections'] as $key_section => $section) {
                  
                    $ps_entity = $em->getRepository('FishmanPollBundle:Pollsection')->find($key_section);
                    
                    $psn_entity = new Pollsection();
                    
                    $psn_entity->setName($ps_entity->getName());
                    $psn_entity->setDescription($ps_entity->getDescription());
                    $psn_entity->setType($ps_entity->getType());
                    $psn_entity->setSequence($ps_entity->getSequence());
                    if ($ps_entity->getParentId() == '-1') {
                        $psn_entity->setParentId('-1');
                    }
                    else {
                        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollsection');
                        $psn_parent_result = $repository->createQueryBuilder('ps')
                            ->where('ps.id = :pollsection')
                            ->setParameter('pollsection', $psId)
                            ->getQuery()
                            ->getResult();
                        $psn_parent_entity = current($psn_parent_result);
                        
                        $psn_entity->setParentId($psn_parent_entity->getId());
                        $psn_entity->setL1($psn_parent_entity->getL1());
                        $psn_entity->setL2($psn_parent_entity->getL2());
                        $psn_entity->setL3($psn_parent_entity->getL3());
                        $psn_entity->setL4($psn_parent_entity->getL4());
                        $psn_entity->setL5($psn_parent_entity->getL5());
                        $psn_entity->setL6($psn_parent_entity->getL6());
                        $psn_entity->setL7($psn_parent_entity->getL7());
                        $psn_entity->setL8($psn_parent_entity->getL8());
                        $psn_entity->setL9($psn_parent_entity->getL9());
                    }
                    $psn_entity->setPoll($entity);
                    $psn_entity->setStatus($ps_entity->getStatus());
                    $psn_entity->setCreatedBy($userBy->getId());
                    $psn_entity->setModifiedBy($userBy->getId());
                    $psn_entity->setCreated(new \DateTime());
                    $psn_entity->setChanged(new \DateTime());
                
                    $em->persist($psn_entity);
                    $em->flush();
                    
                    if ($psn_entity->getParentId() == '-1') {
                        $psn_entity->setL1($psn_entity->getId());
                    }
                    else {
                        $level = FALSE;
                      
                        if ($psn_entity->getL2() == '') {
                            $psn_entity->setL2($psn_entity->getId());
                            $level = TRUE;
                        }
                        if ($psn_entity->getL3() == '' && !$level) {
                            $psn_entity->setL3($psn_entity->getId());
                            $level = TRUE;
                        }
                        if ($psn_entity->getL4() == '' && !$level) {
                            $psn_entity->setL4($psn_entity->getId());
                            $level = TRUE;
                        }
                        if ($psn_entity->getL5() == '' && !$level) {
                            $psn_entity->setL5($psn_entity->getId());
                            $level = TRUE;
                        }
                        if ($psn_entity->getL6() == '' && !$level) {
                            $psn_entity->setL6($psn_entity->getId());
                            $level = TRUE;
                        }
                        if ($psn_entity->getL7() == '' && !$level) {
                            $psn_entity->setL7($psn_entity->getId());
                            $level = TRUE;
                        }
                        if ($psn_entity->getL8() == '' && !$level) {
                            $psn_entity->setL8($psn_entity->getId());
                            $level = TRUE;
                        }
                        if ($psn_entity->getL9() == '' && !$level) {
                            $psn_entity->setL9($psn_entity->getId());
                            $level = TRUE;
                        }
                    }

                    $em->persist($psn_entity);
                    $em->flush();
                            
                    if (!empty($section)) {
                        if (isset($section['questions'])) {
                            foreach ($section['questions'] as $key_question => $question) {
                                
                                $pqn_entity = new Pollquestion();
                                
                                // Total score
                                $ceil_score = 0;
                                    
                                $pqn_entity->setQuestion($question['question']);
                                $pqn_entity->setType($question['type']);
                                $pqn_entity->setAlignment($question['alignment']);
                                $pqn_entity->setOptions($question['options']);
                                $pqn_entity->setMandatoryResponse($question['mandatory_response']);
                                $pqn_entity->setSequence($question['sequence']);
                                $pqn_entity->setPoll($entity);
                                $pqn_entity->setPollsection($psn_entity);
                                $pqn_entity->setStatus($question['status']);
                                $pqn_entity->setCreatedBy($userBy->getId());
                                $pqn_entity->setModifiedBy($userBy->getId());
                                $pqn_entity->setCreated(new \DateTime());
                                $pqn_entity->setChanged(new \DateTime());
                            
                                $em->persist($pqn_entity);
                                $em->flush();
                            }
                        }
                        
                        if (isset($section['sections'])) {
                            if ($entity->getLevel() === 0) {
                                $level = 0;
                            }
                            $sublevel = $level;
                            $sublevel++;
                            self::cloneRegisters($doctrineRegistry, $entity, $section, $sublevel, $userBy, $psn_entity->getId());
                        }
                    }
    
                }
            }
        }

    }
}