<?php

namespace Fishman\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\PollBundle\Entity\Pollapplicationimport
 */
class Pollapplicationimport
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var int $pollscheduling_id
     */
    private $pollscheduling_id;

    /**
     * @var string $evaluated_id
     */
    private $evaluated_id;

    /**
     * @var string $evaluator_id
     */
    private $evaluator_id;

    /**
     * @var string $evaluated_code
     */
    private $evaluated_code;

    /**
     * @var string $evaluator_code
     */
    private $evaluator_code;

    /**
     * @var string $evaluator_rol
     */
    private $evaluator_rol;

    /**
     * @var init $course
     */
    private $course;

    /**
     * @var string $section
     */
    private $section;

    /**
     * @var boolean $status
     */
    private $status;

    // jtt: agregado el 20150812
    /**
     * @var string $cargo_global
     */
    private $cargo_global;
    
    // jtt: agregado el 20150812
    /**
     * @var string $nivel_responsabilidad
     */
    private $nivel_responsabilidad;

    protected $doctrine;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pollscheduling_id
     *
     * @param integer $pollschedulingId
     * @return Pollapplicationimport
     */
    public function setPollschedulingId($pollschedulingId)
    {
        $this->pollscheduling_id = $pollschedulingId;
    
        return $this;
    }

    /**
     * Get pollscheduling_id
     *
     * @return integer 
     */
    public function getPollschedulingId()
    {
        return $this->pollscheduling_id;
    }

    /**
     * Set evaluated_id
     *
     * @param integer $evaluatedId
     * @return Pollapplicationimport
     */
    public function setEvaluatedId($evaluatedId)
    {
        $this->evaluated_id = $evaluatedId;
    
        return $this;
    }

    /**
     * Get evaluated_id
     *
     * @return integer 
     */
    public function getEvaluatedId()
    {
        return $this->evaluated_id;
    }

    /**
     * Set evaluator_id
     *
     * @param integer $evaluatorId
     * @return Pollapplicationimport
     */
    public function setEvaluatorId($evaluatorId)
    {
        $this->evaluator_id = $evaluatorId;
    
        return $this;
    }

    /**
     * Get evaluator_id
     *
     * @return integer
     */
    public function getEvaluatorId()
    {
        return $this->evaluator_id;
    }

    /**
     * Set evaluated_code
     *
     * @param string $evaluatedCode
     * @return Pollapplicationimport
     */
    public function setEvaluatedCode($evaluatedCode)
    {
        $this->evaluated_code = $evaluatedCode;
    
        return $this;
    }

    /**
     * Get evaluated_code
     *
     * @return string 
     */
    public function getEvaluatedCode()
    {
        return $this->evaluated_code;
    }

    /**
     * Set evaluator_code
     *
     * @param string $evaluatorCode
     * @return Pollapplicationimport
     */
    public function setEvaluatorCode($evaluatorCode)
    {
        $this->evaluator_code = $evaluatorCode;
    
        return $this;
    }

    /**
     * Get evaluator_code
     *
     * @return string
     */
    public function getEvaluatorCode()
    {
        return $this->evaluator_code;
    }

    /**
     * Set evaluator_rol
     *
     * @param string $evaluatorRol
     * @return Pollapplicationimport
     */
    public function setEvaluatorRol($evaluatorRol)
    {
        $this->evaluator_rol = $evaluatorRol;
    
        return $this;
    }

    /**
     * Get evaluator_rol
     *
     * @return string
     */
    public function getEvaluatorRol()
    {
        return $this->evaluator_rol;
    }

    // jtt: agregado el 20150812
    /**
     * Set cargo_global
     *
     * @param string $cargoGlobal
     * @return Pollapplicationimport
     */
    public function setCargoGlobal($cargoGlobal)
    {
        $this->cargo_global = $cargoGlobal;
    
        return $this;
    }

    // jtt: agregado el 20150812
    /**
     * Get cargo_global
     *
     * @return string
     */
    public function getCargoGlobal()
    {
        return $this->cargo_global;
    }

    // jtt: agregado el 20150812
    /**
     * Set nivel_responsabilidad
     *
     * @param string $nivelResponsabilidad
     * @return Pollapplicationimport
     */
    public function setNivelResponsabilidad($nivelResponsabilidad)
    {
        $this->nivel_responsabilidad = $nivelResponsabilidad;
    
        return $this;
    }

    // jtt: agregado el 20150812
    /**
     * Get nivel_responsabilidad
     *
     * @return string
     */
    public function getNivelResponsabilidad()
    {
        return $this->nivel_responsabilidad;
    }

    /**
     * Set course
     *
     * @param initeger $course
     * @return Pollapplicationimport
     */
    public function setCourse($course)
    {
        $this->course = $course;
    
        return $this;
    }

    /**
     * Get course
     *
     * @return initeger 
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set section
     *
     * @param string $section
     * @return Pollapplicationimport
     */
    public function setSection($section)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Pollapplicationimport
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
