<?php

namespace Fishman\PollBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Fishman\EntityBundle\Entity\Category;

class PollType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Category',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('c')
                      ->where('c.status = 1')
                      ->orderBy('c.name', 'ASC');
                },
                'empty_value' => 'Choose an option'
            ))
            ->add('title')
            ->add('version')
            ->add('description', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium' // simple, advanced, bbcode
                ),
            ))
            ->add('type', 'choice', array(
                'choices'   => array(
                    'self_evaluation' => 'Autoevaluación', 
                    'evaluation_360' => 'Evaluación 360',
                    'evaluation_collaborators' => 'Evaluación a Colaboradores', 
                    'evaluation_boss' => 'Evaluación a Jefes', 
                    /*'evaluation_teachers' => 'Evaluación a Profesores',*/
                    'not_evaluateds' => 'Sin Evaluados'
                ), 
                'empty_value' => 'Choose an option'
            ))
            ->add('level', 'choice', array(
                'choices'   => array(
                    '0' => 'Ninguno',
                    '1' => 'Nivel 1', 
                    '2' => 'Nivel 2', 
                    '3' => 'Nivel 3', 
                    '4' => 'Nivel 4', 
                    '5' => 'Nivel 5', 
                    '6' => 'Nivel 6', 
                    '7' => 'Nivel 7', 
                    '8' => 'Nivel 8', 
                    '10' => 'Todos'
                ), 
                'empty_value' => 'Choose an option', 
            ))
            ->add('alignment', 'choice', array(
                'choices' => array(
                    'vertical' => 'Vertical', 
                    'horizontal' => 'Horizontal'
                ) 
            ))
            ->add('divide', 'choice', array(
              'choices'   => array(
                'sections_show' => 'Secciones a Mostrar', 
                'number_questions' => 'Cantidad de Preguntas', 
                'none' => 'Ninguna'
              ), 
              'empty_value' => 'Choose an option', 
            ))
            ->add('number_question', 'text')
            ->add('duration', 'text')
            ->add('period', 'choice', array(
                'choices'   => array(
                    'day' => 'Días', 
                    'week' => 'Semanas', 
                    'month' => 'Meses'
                ), 
                'empty_value' => 'Choose an option', 
            ))
            ->add('sequence_question', 'choice', array(
                'choices'   => array(
                    1 => 'Ordenado', 
                    0 => 'Aleatorio'
                ),
                'empty_value' => 'Choose an option' 
            ))
            ->add('status', 'choice', array(
                'choices'   => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'choose an option'
            ))
            ->add('current_version', 'hidden')
            ->add('original_id', 'hidden')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\PollBundle\Entity\Poll'
        ));
    }

    public function getName()
    {
        return 'fishman_pollbundle_polltype';
    }
}
