<?php

namespace Fishman\PollBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\EntityBundle\Entity\Company;

class PollschedulingType extends AbstractType
{
    private $pollScheduling;
    private $doctrine;

    public function __construct($pollScheduling, $disabled, Registry $doctrine)
    {
        $this->pollScheduling = $pollScheduling;
        $this->disabled = $disabled;
        $this->doctrine = $doctrine;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Recover -companies
        $companies_options = array();
        $repository = $this->doctrine->getRepository('FishmanEntityBundle:Company');
        $result = $repository->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->where('c.status = 1
                AND c.id <> 2')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach($result as $r) {
            $companies_options[$r['id']] = $r['name'];
        }
        
        $builder
            ->add('poll_id', 'hidden', array(
                'mapped' => false
            ))
            ->add('description', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium' // simple, advanced, bbcode
                )
            ))
            ->add('company_id', 'choice', array(
                'choices' => $companies_options,  
                'empty_value' => 'Choose an option', 
                'disabled' => $this->disabled
            ))
            ->add('pollscheduling_anonymous', 'choice', array(
                'choices'   => array(
                    1 => 'Si', 
                    0 => 'No'
                ),
                'expanded' => true,
                'empty_value' => 'choose an option',
                'disabled' => $this->disabled
            ))
            ->add('custom_header_title', 'text')
            ->add('custom_header_title_text', 'text', array(
                'required' => FALSE
            ))
            ->add('custom_header_title_description', 'text', array(
                'required' => FALSE
            ))
            ->add('custom_header_logo', 'text')
            ->add('type', 'choice', array(
                'choices'   => array(
                    'self_evaluation' => 'Autoevaluación', 
                    'evaluation_360' => 'Evaluación 360',
                    'evaluation_collaborators' => 'Evaluación a Colaboradores', 
                    'evaluation_boss' => 'Evaluación a Jefes', 
                    /*'evaluation_teachers' => 'Evaluación a Profesores',*/
                    'not_evaluateds' => 'Sin Evaluados'
                ), 
                'empty_value' => 'Choose an option',
                'disabled' => $this->disabled
            ))
            ->add('multiple_evaluation', 'choice', array(
                'choices'   => array(
                    1 => 'Si', 
                    0 => 'No'
                ),
                'expanded' => true,
                'empty_value' => 'choose an option',
                'disabled' => $this->disabled
            ))
            ->add('alignment', 'choice', array(
                'choices' => array(
                    'vertical' => 'Vertical', 
                    'horizontal' => 'Horizontal'
                ) 
            ))
            ->add('benchmarking_id', 'hidden', array(
                'mapped' => false
            ))
            ->add('pollscheduling_period', 'text')
            ->add('duration', 'text')
            ->add('period', 'choice', array(
                'choices'   => array(
                    'day' => 'Días', 
                    'week' => 'Semanas', 
                    'month' => 'Meses'
                ), 
                'empty_value' => 'Choose an option'
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('terms', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium' // simple, advanced, bbcode
                ),
                'required' => FALSE
            ))
            ->add('use_terms', 'choice', array(
                'choices'   => array(
                    1 => 'Si', 
                    0 => 'No'
                ),
                'expanded' => true,
                'empty_value' => 'choose an option',
                'required' => FALSE
            ))
            ->add('message_gratitude', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium'
                )
            ))
            ->add('message_end', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium'
                )
            ))
            ->add('message_inactive', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium'
                )
            ))
            ->add('access_view_number', 'text')
            ->add('status_bar', 'choice', array(
                'choices' => array(
                    'section' => 'Por sección', 
                    'question' => 'Por pregunta'
                ),
                'disabled' => $this->disabled
            ))
            ->add('counselor_report', 'choice', array(
                'choices'   => array(
                    1 => 'Si', 
                    0 => 'No'
                ),
                'expanded' => true,
                'empty_value' => 'choose an option',
                'disabled' => $this->disabled
            ))
            ->add('pollscheduling_anonymous', 'choice', array(
                'choices'   => array(
                    1 => 'Si', 
                    0 => 'No'
                ),
                'expanded' => true,
                'empty_value' => 'choose an option',
                'disabled' => $this->disabled
            ))
            ->add('pollschedulingrelation_id', 'hidden')
            ->add('sequence', 'choice', array(
                'choices' => array(
                    1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 
                    8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 
                    15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20),
                'empty_value' => 'choose an option'
            ))
            ->add('status', 'choice', array(
                'choices'   => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'choose an option'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\PollBundle\Entity\Pollscheduling'
        ));
    }

    public function getName()
    {
        return 'fishman_pollbundle_pollschedulingtype';
    }
}
