<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\PollBundle\Entity\Benchmarking;
use Fishman\PollBundle\Form\BenchmarkingType;

/**
 * Benchmarking controller.
 *
 */
class BenchmarkingController extends Controller
{
    /**
     * Lists all Benchmarking entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
        $queryBuilder = $repository->createQueryBuilder('bm')
            ->select('bm.id, bm.name, bm.period, bm.changed, bm.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'bm.modified_by = u.id')
            ->andWhere('bm.id LIKE :id 
                    OR bm.name LIKE :name 
                    OR bm.period LIKE :period')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->setParameter('period', '%' . $data['word'] . '%')
            ->orderBy('bm.id', 'ASC');
        
        // Add arguments
        
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('bm.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'benchmarking');
        $paginator->setMaxPagerItems(5, 'benchmarking');
        $entities = $paginator->paginate($query, 'benchmarking')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('benchmarking');
        $endPageItem = $paginator->getEndPageItem('benchmarking');
        $totalItems = $paginator->getTotalItems('benchmarking');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanPollBundle:Benchmarking:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Benchmarking entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Benchmarking')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el benchmarking.');
            return $this->redirect($this->generateUrl('benchmarking'));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        return $this->render('FishmanPollBundle:Benchmarking:show.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Benchmarking entity.
     *
     */
    public function newAction()
    {
        $entity = new Benchmarking();
        $entity->setStatus(1);
        $form   = $this->createForm(new BenchmarkingType(), $entity);

        return $this->render('FishmanPollBundle:Benchmarking:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Benchmarking entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Benchmarking();
        $form = $this->createForm(new BenchmarkingType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
                
            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('benchmarking_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanPollBundle:Benchmarking:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Benchmarking entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Benchmarking')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el benchmarking.');
            return $this->redirect($this->generateUrl('benchmarking'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        $editForm = $this->createForm(new BenchmarkingType(), $entity);

        return $this->render('FishmanPollBundle:Benchmarking:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Benchmarking entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Benchmarking')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el benchmarking.');
            return $this->redirect($this->generateUrl('benchmarking'));
        }
        
        $editForm = $this->createForm(new BenchmarkingType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) { 
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
            
            return $this->redirect($this->generateUrl('benchmarking_show', array('id' => $id)));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        return $this->render('FishmanPollBundle:Benchmarking:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Benchmarking entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanPollBundle:Benchmarking')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el benchmarking.');
                return $this->redirect($this->generateUrl('benchmarking'));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
            $psc = $repository->createQueryBuilder('psc')
                ->select('count(psc.benchmarking)')
                ->where('psc.benchmarking = :benchmarking')
                ->setParameter('benchmarking', $entity->getId())
                ->groupBy('psc.benchmarking')
                ->getQuery()
                ->getResult();
            
            if (current($psc) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'No se puede borrar el Benchmarking, existen datos asociados.');
            }
        }

        return $this->redirect($this->generateUrl('benchmarking'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Benchmarking entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Benchmarking')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el benchmarking.');
            return $this->redirect($this->generateUrl('benchmarking'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Benchmarking:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    //TODO: Change name by more descriptive
    function jsonAction()
    {
        $benchmarking_id = $_GET['benchmarking_id'];   
        $benchmarking = $this->getDoctrine()->getManager()->getRepository('FishmanPollBundle:Benchmarking')->find($benchmarking_id);

        $period = '';

        if($benchmarking){
            $period = $benchmarking->getPeriod();
        }

        $response = new Response(json_encode(array('period' => $period))); 
        return $response;
    }
}
