<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\PollBundle\Entity\Pollscheduling;
use Fishman\PollBundle\Entity\Pollsection;
use Fishman\PollBundle\Entity\Pollschedulingsection;
use Fishman\PollBundle\Entity\Pollschedulingquestion;
use Fishman\PollBundle\Entity\Pollschedulingpeople;
use Fishman\PollBundle\Entity\Pollapplication;
use Fishman\PollBundle\Entity\Pollpersonimport;
use Fishman\PollBundle\Entity\Pollapplicationimport;
use Fishman\PollBundle\Entity\Pollapplicationquestion;
use Fishman\PollBundle\Entity\Benchmarking;
use Fishman\AuthBundle\Entity\User;
use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\EntityBundle\Entity\Category;
use Fishman\EntityBundle\Entity\Companycourse;
use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\EntityBundle\Entity\Companycharge;
use Fishman\WorkshopBundle\Entity\Workshopapplication;
use Fishman\NotificationBundle\Entity\Notification;
use Fishman\NotificationBundle\Entity\Notificationscheduling;
use Fishman\NotificationBundle\Entity\Notificationexecution;

use Fishman\PollBundle\Form\PollschedulingType;
use Fishman\PollBundle\Form\PollevaluatorimportType;
use Fishman\NotificationBundle\Form\NotificationschedulingType;
use Ideup\SimplePaginatorBundle\Paginator;

use Fishman\ImportExportBundle\Entity\Temporalimportdata;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

/**
 * Pollscheduling controller.
 *
 */
class PollschedulingController extends Controller
{
    /**
     * Lists all Pollscheduling entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $category_options = Category::getListCategoryOptions($this->getDoctrine());
        $benchmarking_options = Benchmarking::getListBenchmarkingOptions($this->getDoctrine());
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'resolved' => '', 
            'category' => '', 
            'company' => '', 
            'benchmarking' => '', 
            'psc_period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('resolved', 'text', array(
                'required' => FALSE
            ))
            ->add('category', 'choice', array(
                'choices' => $category_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('company', 'text', array(
                'required' => FALSE
            ))
            ->add('benchmarking', 'choice', array(
                'choices' => $benchmarking_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('psc_period', 'text', array(
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'resolved' => '', 
            'category' => '', 
            'company' => '', 
            'benchmarking' => '', 
            'psc_period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
            
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('psc')
            ->select('psc.id, psc.title, psc.version, psc.entity_type, psc.entity_id, psc.time_number, t.name category, 
                      c.name company, psc.initdate, psc.enddate, b.name benchmarking, psc.pollscheduling_period psc_period, 
                      psc.own, psc.changed, psc.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'psc.company_id = c.id')
            ->innerJoin('psc.poll', 'p')
            ->innerJoin('p.category', 't')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'psc.modified_by = u.id')
            ->leftJoin('psc.benchmarking', 'b')
            ->where("psc.deleted = :deleted 
                    AND psc.entity_type = 'pollscheduling'")
            ->andWhere('psc.id LIKE :id 
                    OR psc.title LIKE :title 
                    OR psc.version LIKE :version')
            ->setParameter('deleted', FALSE)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('title', '%' . $data['word'] . '%')
            ->setParameter('version', '%' . $data['word'] . '%')
            ->orderBy('psc.id', 'ASC');
        
        // Add arguments
        
        if ($data['resolved'] != '' || $data['resolved'] === 0) {
            $queryBuilder
                ->andWhere('psc.time_number LIKE :resolved')
                ->setParameter('resolved', '%' . $data['resolved'] . '%');
        }
        if ($data['category'] != '') {
            $queryBuilder
                ->andWhere('t.id = :category')
                ->setParameter('category', $data['category']);
        }
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('c.name LIKE :company')
                ->setParameter('company', '%' . $data['company'] . '%');
        }
        if ($data['benchmarking'] != '') {
            $queryBuilder
                ->andWhere('b.id = :benchmarking')
                ->setParameter('benchmarking', $data['benchmarking']);
        }
        if ($data['psc_period'] != '') {
            $queryBuilder
                ->andWhere('psc.pollscheduling_period LIKE :psc_period')
                ->setParameter('psc_period', '%' . $data['psc_period'] . '%');
        }
        if (!empty($data['initdate'])) {
            $queryBuilder
                ->andWhere('psc.initdate = :initdate')
                ->setParameter('initdate', $data['initdate']);
        }
        if (!empty($data['enddate'])) {
            $queryBuilder
                ->andWhere('psc.enddate = :enddate')
                ->setParameter('enddate', $data['enddate']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('psc.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollscheduling');
        $paginator->setMaxPagerItems(5, 'pollscheduling');
        $entities = $paginator->paginate($query, 'pollscheduling')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollscheduling');
        $endPageItem = $paginator->getEndPageItem('pollscheduling');
        $totalItems = $paginator->getTotalItems('pollscheduling');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanPollBundle:Pollscheduling:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Pollscheduling entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('psc')
            ->select('psc.id, psc.title, psc.entity_type, t.name category, psc.version, psc.description, 
                      psc.custom_header_title, psc.custom_header_title_text, psc.custom_header_title_description, 
                      psc.custom_header_logo, c.name company, psc.type, psc.multiple_evaluation, b.name benchmarking, 
                      psc.pollscheduling_period psc_period, psc.level, psc.alignment, psc.divide, psc.number_question, 
                      psc.sequence_question, psc.duration, psc.period, psc.initdate, psc.enddate, 
                      psc.terms, psc.use_terms, psc.message_gratitude, psc.message_end, 
                      psc.message_inactive, psc.access_view_number, psc.status_bar, psc.counselor_report, 
                      psc.pollscheduling_anonymous anonymous, pscr.title psc_relation, 
                      pscr.pollscheduling_period psc_relation_period, psc.sequence, psc.status, psc.time_number, 
                      psc.own, psc.created, psc.changed, psc.created_by, psc.modified_by')
            ->innerJoin('psc.poll', 'p')
            ->innerJoin('p.category', 't')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'psc.company_id = c.id')
            ->leftJoin('psc.benchmarking', 'b')
            ->leftJoin('FishmanPollBundle:Pollscheduling', 'pscr', 'WITH', 'psc.pollschedulingrelation_id = pscr.id')
            ->where("psc.id = :pollscheduling 
                    AND psc.deleted = :deleted
                    AND psc.entity_type = 'pollscheduling'")
            ->setParameter('pollscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanPollBundle:Pollscheduling:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));

    }

    /**
     * Displays a form to create a new Pollscheduling entity.
     *
     */
    public function newAction()
    {
        $entity = new Pollscheduling();
        
        $em = $this->getDoctrine()->getManager();
        
        // Recover Polls
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
        $polls = $repository->createQueryBuilder('p')
             ->where('p.current_version = :current_version')
             ->setParameter('current_version', 1)
             ->orderBy('p.id', 'ASC')
             ->getQuery()
             ->getResult();
        
        if (!empty($polls)) {
        
            // Recover Categories
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
            $categories = $repository->createQueryBuilder('c')
                 ->where('c.status = 1')
                 ->orderBy('c.name', 'ASC')
                 ->getQuery()
                 ->getResult();
            
            // Recover Benchmarkings
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
            $benchmarkings = $repository->createQueryBuilder('b')
                 ->where('b.status = 1')
                 ->orderBy('b.name', 'ASC')
                 ->getQuery()
                 ->getResult();
            
            $entity->setMultipleEvaluation(0);
            $entity->setPollschedulingAnonymous(0);
            $entity->setStatus(TRUE);
            
            $form = $this->createForm(new PollschedulingType($entity, false, $this->getDoctrine()), $entity);
            
            return $this->render('FishmanPollBundle:Pollscheduling:new.html.twig', array(
                'entity' => $entity,
                'categories' => $categories,
                'benchmarkings' => $benchmarkings,
                'form' => $form->createView(),
            ));
        }
        else {
            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('error', 'Debe crear una encuesta para poder ser programada.');

            return $this->redirect($this->generateUrl('poll', array()));
        }
    }

    /**
     * Creates a new Pollscheduling entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
                
        $entity  = new Pollscheduling();
        
        $form = $this->createForm(new PollschedulingType($entity, false, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            // Recover Datein and Dateout
            $initdate = strtotime($entity->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getEnddate()->format('Y/m/d'));

            // set flash messages
            $session = $this->getRequest()->getSession();
            
            if ($initdate < $enddate) {
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    // PostData
                    $postData = $request->request->get('fishman_pollbundle_pollschedulingtype', 'default value if bar does not exist');
                    
                    // User
                    $userBy = $this->get('security.context')->getToken()->getUser();
                    
                    // Recover Poll
                    $poll = $em->getRepository('FishmanPollBundle:Poll')->find($postData['poll_id']);
                    
                    // Total assigned polls to a company
                    $pollschedulingCompany = $em->createQuery('SELECT COUNT(psc.id) num 
                            FROM FishmanPollBundle:Pollscheduling psc
                            WHERE psc.poll = :poll 
                            AND psc.company_id = :company')
                        ->setParameter('poll', $poll->getId())
                        ->setParameter('company', $entity->getCompanyId())
                        ->getSingleResult();
                    $time_number = $pollschedulingCompany['num'] + 1;
                    $entity->setEntityType('pollscheduling');
                    $entity->setPoll($poll);
                    $entity->setTitle($poll->getTitle());
                    $entity->setVersion($poll->getVersion());
                    $entity->setLevel($poll->getLevel());
                    $entity->setDivide($poll->getDivide());
                    $entity->setNumberQuestion($poll->getNumberQuestion());
                    $entity->setSequenceQuestion($poll->getSequenceQuestion());
                    $entity->setCurrentVersion($poll->getCurrentVersion());
                    $entity->setTimeNumber($time_number);
                    $entity->setOriginalId($poll->getOriginalId());
                    if ($entity->getCustomHeaderTitle() != 1) {
                        $entity->setCustomHeaderTitleText('');
                        $entity->setCustomHeaderTitleDescription('');
                    }
                    if ($entity->getPollschedulingAnonymous()) {
                        $entity->setCompanyId(2);
                        $entity->setCustomHeaderTitle(FALSE);
                        $entity->setCustomHeaderLogo(FALSE);
                        $entity->setType('self_evaluation');
                        $entity->setMultipleEvaluation(FALSE);
                        $entity->setBenchmarking(NULL);
                        $entity->setStatusBar('question');
                        $entity->setCounselorReport(0);
                        $entity->setAccessViewNumber(1);
                        $entity->setPollschedulingrelationId(NULL);
                    }
                    if ($entity->getType() == 'self_evaluation') {
                        $entity->setMultipleEvaluation(FALSE);
                        $entity->setAccessViewNumber(1);
                    }
                    if ($entity->getType() == 'not_evaluateds') {
                        $entity->setMultipleEvaluation(FALSE);
                        $entity->setAccessViewNumber(1);
                        $entity->setPollschedulingrelationId(NULL);
                    }
                    if ($entity->getMultipleEvaluation()) {
                        $entity->setStatusBar('question');
                        $entity->setCounselorReport(0);
                        $entity->setAccessViewNumber(1);
                        $entity->setPollschedulingrelationId('');
                        $entity->setAlignment('vertical');
                    }
                    if ($postData['benchmarking_id'] != '') {
                        // Recover Benchmarking
                        $benchmarking = $em->getRepository('FishmanPollBundle:Benchmarking')->find($postData['benchmarking_id']);
                        $entity->setBenchmarking($benchmarking);
                    }
                    if ($entity->getUseTerms() == NULL) {
                      $entity->setUseTerms(FALSE);
                    }
                    $entity->setDeleted(FALSE);
                    $entity->setOwn(TRUE);
                    $entity->setCreatedBy($userBy->getId());
                    $entity->setModifiedBy($userBy->getId());
                    $entity->setCreated(new \DateTime());
                    $entity->setChanged(new \DateTime());
                    
                    $em->persist($entity);
                    $em->flush();
        
                    $entity->setEntityId($entity->getId());
                    
                    $em->persist($entity);
                    $em->flush();
                    
                    // Operations Pollscheduling
                    if ($entity->getPollschedulingAnonymous()) {
                        Pollscheduling::operationsPollschedulingAnonymous($this->getDoctrine(), $entity, $userBy);
                        Pollscheduling::operationsPollscheduling($this->getDoctrine(), $entity, $userBy, TRUE);
                    }
                    else {
                        Pollscheduling::operationsPollscheduling($this->getDoctrine(), $entity, $userBy);
                    }
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('pollscheduling_show', array(
                    'id' => $entity->getId()
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        
        // Recover Benchmarkings
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
        $benchmarkings = $repository->createQueryBuilder('b')
             ->where('b.status = 1')
             ->orderBy('b.name', 'ASC')
             ->getQuery()
             ->getResult();

        return $this->render('FishmanPollBundle:Pollscheduling:new.html.twig', array(
            'entity' => $entity,
            'categories' => $categories,
            'benchmarkings' => $benchmarkings,
            'form' => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Pollscheduling entity.
     *
     */
    public function editAction($id)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('ps')
            ->where("ps.id = :pollscheduling 
                    AND ps.deleted = :deleted
                    AND ps.entity_type = 'pollscheduling'")
            ->setParameter('pollscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_category = $entity->getPoll()->getCategory();
        
        // Recover polls
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
        $polls = $repository->createQueryBuilder('p')
             ->where('p.category = :category_id 
                    AND p.current_version = :current_version')
             ->setParameter('category_id', $current_category->getId())
             ->setParameter('current_version', 1)
             ->orderBy('p.id', 'ASC')
             ->getQuery()
             ->getResult();
        $current_poll = $entity->getPoll();
        
        // Recover Benchmarkings
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
        $benchmarkings = $repository->createQueryBuilder('b')
             ->where('b.status = 1')
             ->orderBy('b.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_benchmarking = $entity->getBenchmarking();
        
        // Recover Pollscheduling relations
        $pollschedulingrelations = Pollscheduling::getListPollschedulingrelationsByCompany(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getId(),
            'pollscheduling'
        );
        $current_pollschedulingrelation = $entity->getPollschedulingrelationId();
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        $editForm = $this->createForm(new PollschedulingType($entity, true, $this->getDoctrine()), $entity);
        
        if ($entity->getEntitytype() == "pollscheduling") {
            return $this->render('FishmanPollBundle:Pollscheduling:edit.html.twig', array(
                'entity' => $entity, 
                'categories' => $categories,
                'polls' => $polls,
                'benchmarkings' => $benchmarkings,
                'pollschedulingrelations' => $pollschedulingrelations,
                'current_category' => $current_category,
                'current_poll' => $current_poll,
                'current_benchmarking' => $current_benchmarking,
                'current_pollschedulingrelation' => $current_pollschedulingrelation,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'edit_form'   => $editForm->createView()
            ));
        }
        else {
            $session->getFlashBag()->add('error', 'Esta encuesta pertenece a un taller, ingrese a traves de la sección programación de talleres.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }

    }

    /**
     * Edits an existing Pollscheduling entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        $editForm = $this->createForm(new PollschedulingType($entity, true, $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // Recover Datein and Dateout
            $initdate = strtotime($entity->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getEnddate()->format('Y/m/d'));
            
            if ($initdate < $enddate) {
                
                // PostData
                $postData = $request->request->get('fishman_pollbundle_pollschedulingtype', 'default value if bar does not exist');
                
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                if ($entity->getCustomHeaderTitle() != 1) {
                    $entity->setCustomHeaderTitleText('');
                    $entity->setCustomHeaderTitleDescription('');
                }
                if ($entity->getType() == 'self_evaluation') {
                    $entity->setMultipleEvaluation(FALSE);
                    $entity->setAccessViewNumber(1);
                }
                if ($entity->getType() == 'not_evaluateds') {
                    $entity->setMultipleEvaluation(FALSE);
                    $entity->setAccessViewNumber(1);
                    $entity->setPollschedulingrelationId(NULL);
                }
                if ($entity->getMultipleEvaluation()) {
                    $entity->setStatusBar('question');
                    $entity->setCounselorReport(0);
                    $entity->setAccessViewNumber(1);
                    $entity->setPollschedulingrelationId('');
                    $entity->setAlignment('vertical');
                }
                if ($postData['benchmarking_id'] != '') {
                    // Recover Benchmarking
                    $benchmarking = $em->getRepository('FishmanPollBundle:Benchmarking')->find($postData['benchmarking_id']);
                    $entity->setBenchmarking($benchmarking);
                }
                $entity->setModifiedBy($modifiedBy->getId());
                $entity->setChanged(new \DateTime());
                
                $em->persist($entity);
                $em->flush();
                                             
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('pollscheduling_show', array(
                    'id' => $id
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_category = $entity->getPoll()->getCategory();
        
        // Recover Polls
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
        $polls = $repository->createQueryBuilder('p')
             ->where('p.category = :category_id 
                      AND p.current_version = :current_version')
             ->setParameter('category_id', $current_category->getId())
             ->setParameter('current_version', 1)
             ->orderBy('p.id', 'ASC')
             ->getQuery()
             ->getResult();
        $current_poll = $entity->getPoll();
            
        // Recover Benchmarkings
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
        $benchmarkings = $repository->createQueryBuilder('b')
             ->where('b.status = 1')
             ->orderBy('b.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_benchmarking = $entity->getBenchmarking();
        
        // Recover Pollscheduling relations
        $pollschedulingrelations = Pollscheduling::getListPollschedulingrelationsByCompany(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getId(),
            'pollscheduling'
        );
        $current_pollschedulingrelation = $entity->getPollschedulingrelationId();

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollscheduling:edit.html.twig', array(
            'entity' => $entity, 
            'categories' => $categories,
            'polls' => $polls,
            'benchmarkings' => $benchmarkings,
            'pollschedulingrelations' => $pollschedulingrelations,
            'current_category' => $current_category,
            'current_poll' => $current_poll,
            'current_benchmarking' => $current_benchmarking,
            'current_pollschedulingrelation' => $current_pollschedulingrelation,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Pollscheduling entity.
     *
     */
    public function dropAction($id)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('ps')
            ->where("ps.id = :pollscheduling 
                    AND ps.deleted = :deleted
                    AND ps.entity_type = 'pollscheduling'")
            ->setParameter('pollscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
          
        $deleteForm = $this->createDeleteForm($id);

        if ($entity->getEntitytype() == "pollscheduling") {
          return $this->render('FishmanPollBundle:Pollscheduling:drop.html.twig', array(
              'entity'      => $entity,
              'delete_form' => $deleteForm->createView()
          ));
        }
        else {
            $session->getFlashBag()->add('error', 'Esta encuesta pertenece a un taller, ingrese a traves de la sección programación de talleres.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Deletes a Pollscheduling entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {    
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        $em = $this->getDoctrine()->getManager();
            
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('ps')
            ->where("ps.id = :pollscheduling 
                    AND ps.deleted = :deleted
                    AND ps.entity_type = 'pollscheduling'")
            ->setParameter('pollscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
            
        if ($form->isValid()) {
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
            $pscr = $repository->createQueryBuilder('psc')
                ->select('count(psc.pollschedulingrelation_id)')
                ->where('psc.pollschedulingrelation_id = :pollschedulingrelation
                        AND psc.deleted = 0')
                ->setParameter('pollschedulingrelation', $entity->getId())
                ->groupBy('psc.pollschedulingrelation_id')
                ->getQuery()
                ->getResult();
            
            if (count($pscr) == 0) {
                
                //Delete Pollapplication to Pollscheduling
                Pollapplication::deleteRegisters($this->getDoctrine(), $entity);
                
                $entity->setDeleted(TRUE);
    
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La Encuesta está relacionada a otra Programación de Encuesta.');
            }
            
        }

        return $this->redirect($this->generateUrl('pollscheduling'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Return poll select list with some category id
     * TODO: Change this as ajaxChoiceByCategory
     */
    public function choiceByCompanyAction()
    {
        $company_id = $_GET['company_id'];
        $pollscheduling_id = $_GET['pollscheduling_id'];
        $entity_type = $_GET['entity_type'];
        $entity_id = $_GET['entity_id'];
        
        // Recover Pollscheduling relations
        $pscrelations = Pollscheduling::getListPollschedulingrelationsByCompany(
            $this->getDoctrine(),
            $company_id,
            $pollscheduling_id,
            $entity_type,
            $entity_id
        );
        
        $combo = '<select class="pollschedulingRelations" name="poll_pollschedulingrelations">';
        $combo .= '<option value="">Seleccione la encuesta</option>';
        foreach($pscrelations as $pscrelation){
            $combo .= '<option value="' . $pscrelation['id'] . '">' . $pscrelation['title'] . ' - ' . $pscrelation['period'] . '</option>';
        }
        $combo .= '</select>';

        $response = new Response(json_encode(
            array('combo' => $combo)));  
        return $response;
    }

    /**
     * Preview Pollscheduling entity.
     *
     */
    public function previewAction(Request $request, $id, $page)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
        if (!$entity || $entity->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $array_questions = '';
        
        if ($request->request->get('send', false)) {
            return $this->redirect($this->generateUrl('pollscheduling_preview_message', array(
                'id' => $id
            )));
        }
        
        // Paginator

        $array_paginator = Pollapplicationquestion::getPaginator($this->getDoctrine(), $entity, $page);
        
        if ($array_paginator['number_pagers'] > 0 && $page > $array_paginator['number_pagers']) {
            return $this->redirect($this->generateUrl('pollscheduling_preview', array(
                'id' => $id,
                'page' => $array_paginator['number_pagers']
            )));
        }

        // Recover Questions in page current
        
        $array['sections'] = $entity->getSerialized();
        
        if ($entity->getDivide() == 'sections_show') {
            $array_questions = Pollschedulingsection::getArraySectionShow($this->getDoctrine(), $array, $entity->getId(), $page);
        }
        else if ($entity->getDivide() == 'number_questions') {
            $array_questions = Pollschedulingsection::getArrayNumberQuestions($this->getDoctrine(), $array['sections'], $entity->getId(), $page, $entity->getNumberQuestion());
        }
        else if ($entity->getDivide() == 'none') {
            // jtt
            $ids = array(19,26,27); //array con id de encuestas de TEMPERAMENTO
            if (in_array($entity->getPoll()->getId(), $ids)) {
            foreach ($array['sections'] as $llave => $valor) {
                if (array_key_exists('questions', $valor)) {
                    uasort($array['sections'][$llave]['questions'], function($a, $b) {
                        return $a['sequence'] - $b['sequence'];
                    });
                }
            }
            }
            $array_questions = $array;
        }
        
        if ($request->request->get('first', false)) {
            $page = 1;
        }
        if ($request->request->get('prev', false)) {
            $page--;
        }
        if ($request->request->get('page')) {
            $page = $request->request->get('page');
        }
        if ($request->request->get('next', false)) {
            $page++;
        }
        if ($request->request->get('last', false)) {
            $page = $array_paginator['number_pagers'];
        }
        
        if ($request->request->get('page') || $request->request->get('first', false) || $request->request->get('prev', false) || $request->request->get('next', false) || $request->request->get('last', false)) {
            return $this->redirect($this->generateUrl('pollscheduling_preview', array(
                'id' => $id,
                'page' => $page
            )));
        }
        
        // Array Parameters
        
        $array_parameters = array(
            'entity' => $entity, 
            'page' => $page,
            'paginator' => $array_paginator['paginator'],
            'lastpage' => $array_paginator['number_pagers'],
            'alignment' => 'vertical',
            'questions' => $array_questions,
            'preview_multiple' => FALSE
        );
        
        if ($entity->getAlignment() == 'horizontal') {
            $array_parameters['alignment'] = 'horizontal';
        }
        
        if ($entity->getMultipleEvaluation()) {
            $array_parameters['preview_multiple'] = array(
                0 => array('id' => 1, 'name' => 'Evaluado N°1', 'answer' => ''),
                1 => array('id' => 2, 'name' => 'Evaluado N°2', 'answer' => ''),
                2 => array('id' => 3, 'name' => 'Evaluado N°3', 'answer' => '')
            );
        }
        
        return $this->render('FishmanPollBundle:Pollscheduling/Preview:resolved.html.twig', $array_parameters);
    }

    /**
     * Preview Pollscheduling Terms.
     *
     */
    public function previewTermsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
        if (!$entity || $entity->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$entity->getUseTerms()) {
            return $this->redirect($this->generateUrl('pollscheduling_preview', array(
                'id' => $id
            )));
        }
        
        // Array Parameters

        $array_parameters = array(
            'entity' => $entity,
            'terms' => $entity->getTerms()
        );
        
        return $this->render('FishmanPollBundle:Pollscheduling/Preview:terms.html.twig', $array_parameters);
    }

    /**
     * Preview Pollscheduling Message.
     *
     */
    public function previewMessageAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
        if (!$entity || $entity->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Array Parameters

        $array_parameters = array(
            'entity' => $entity,
            'message' => $entity->getMessageGratitude()
        );
        
        return $this->render('FishmanPollBundle:Pollscheduling/Preview:message.html.twig', $array_parameters);
    }

    /**
     * Export Pollscheduling Links.
     *
     */
    public function exportLinksAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
            
            $router = $this->get('router');
            $doctrine = $this->getDoctrine();
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
            if (!$pollscheduling || $pollscheduling->getEntityType() != 'pollscheduling') {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
                return $this->redirect($this->generateUrl('pollscheduling'));
            }
            
            // Recover Company
            $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
            
            $phpExcel = new PHPExcel();
            $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
            $phpExcel->getProperties()->setLastModifiedBy("");
            $phpExcel->getProperties()->setTitle("Rutas de Evaluaciones por Persona");
            $phpExcel->getProperties()->setSubject("");
            $phpExcel->getProperties()->setDescription("Muesta una lista de todas las personas con sus rutas de acceso para la lista de evaluacciones pendientes a resolver.");
    
            $phpExcel->setActiveSheetIndex(0);
            $phpExcel->getActiveSheet()->setTitle('Rutas de Evaluaciones');
    
            $worksheet = $phpExcel->getSheet(0);
            $worksheet->getCellByColumnAndRow(1, 2)->setValue('Rutas de Evaluaciones');
            $worksheet->getCellByColumnAndRow(1, 3)->setValue('Encuesta: ' . $pollscheduling->getTitle() . ' | ' 
                                                                           . $pollscheduling->getVersion() . ' | ' 
                                                                           . $company->getName() . ' | ' 
                                                                           . $pollscheduling->getPollschedulingPeriod());
            
            $pathEvaluations = Pollscheduling::getPollListLinks($doctrine, $router, $pollscheduling);
            
            Pollscheduling::generateReportPathEvaluations($pathEvaluations, $worksheet, $phpExcel, $company);
            
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="encuesta-personas-evaluaciones.xls"');
            header('Cache-Control: max-age=0');
            
            $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
            $writer->save('php://output');
            
        }
        exit;
    }
    
    /**
     * Lists all Pollapplication entities.
     *
     */
    public function indexEvaluatorAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        elseif ($pollscheduling->getType() == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
        
        if (!$pollscheduling->getDeleted()) {
            
            // Recovering data
            
            $rol_options = Pollapplication::getListRolOptions($this->getDoctrine());
            
            // Find Entities
            
            $defaultData = array(
                'evaluated_code' => '', 
                'evaluated_names' => '', 
                'evaluated_surname' => '', 
                'evaluated_lastname' => '', 
                'evaluator_code' => '', 
                'evaluator_names' => '', 
                'evaluator_surname' => '', 
                'evaluator_lastname' => '', 
                'evaluator_rol' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('evaluated_code', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluated_names', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluated_surname', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluated_lastname', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_code', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_names', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_surname', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_lastname', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_rol', 'choice', array(
                    'choices' => $rol_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'evaluated_code' => '', 
                'evaluated_names' => '', 
                'evaluated_surname' => '', 
                'evaluated_lastname' => '', 
                'evaluator_code' => '', 
                'evaluator_names' => '', 
                'evaluator_surname' => '', 
                'evaluator_lastname' => '', 
                'evaluator_rol' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Delete registers
            $deletes_form = $this->createFormBuilder()
                ->add('all_select', 'choice', array(
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => true
                ))
                ->add('registers', 'choice', array(
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => true
                ))
                ->getForm();
                
            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $queryBuilder = $repository->createQueryBuilder('pa')
                ->select('pa.id, wi_one.code evaluated_code, u_one.names evaluated_names, u_one.surname evaluated_surname, 
                          u_one.lastname evaluated_lastname, wi_two.code evaluator_code, u_two.names evaluator_names, 
                          u_two.surname evaluator_surname, u_two.lastname evaluator_lastname, pa.evaluator_rol, 
                          pa.changed, u.names, u.surname, u.lastname')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
                ->innerJoin('wi_one.user', 'u_one')
                ->innerJoin('wi_two.user', 'u_two')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pa.modified_by = u.id')
                ->where('pa.pollscheduling = :pollscheduling 
                        AND pa.deleted = :deleted')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->setParameter('deleted', FALSE)
                ->orderBy('pa.id', 'ASC');
        
            // Add arguments
            
            if ($data['evaluated_code'] != '') {
                $queryBuilder
                    ->andWhere('wi_one.code LIKE :evaluated_code')
                    ->setParameter('evaluated_code', '%' . $data['evaluated_code'] . '%');
            }
            if ($data['evaluated_names'] != '') {
                $queryBuilder
                    ->andWhere('u_one.names LIKE :evaluated_names')
                    ->setParameter('evaluated_names', '%' . $data['evaluated_names'] . '%');
            }
            if ($data['evaluated_surname'] != '') {
                $queryBuilder
                    ->andWhere('u_one.surname LIKE :evaluated_surname')
                    ->setParameter('evaluated_surname', '%' . $data['evaluated_surname'] . '%');
            }
            if ($data['evaluated_lastname'] != '') {
                $queryBuilder
                    ->andWhere('u_one.lastname LIKE :evaluated_lastname')
                    ->setParameter('evaluated_lastname', '%' . $data['evaluated_lastname'] . '%');
            }
            if ($data['evaluator_code'] != '') {
                $queryBuilder
                    ->andWhere('wi_two.code LIKE :evaluator_code')
                    ->setParameter('evaluator_code', '%' . $data['evaluator_code'] . '%');
            }
            if ($data['evaluator_names'] != '') {
                $queryBuilder
                    ->andWhere('u_two.names LIKE :evaluator_names')
                    ->setParameter('evaluator_names', '%' . $data['evaluator_names'] . '%');
            }
            if ($data['evaluator_surname'] != '') {
                $queryBuilder
                    ->andWhere('u_two.surname LIKE :evaluator_surname')
                    ->setParameter('evaluator_surname', '%' . $data['evaluator_surname'] . '%');
            }
            if ($data['evaluator_lastname'] != '') {
                $queryBuilder
                    ->andWhere('u_two.lastname LIKE :evaluator_lastname')
                    ->setParameter('evaluator_lastname', '%' . $data['evaluator_lastname'] . '%');
            }
            if ($data['evaluator_rol'] != '') {
                $queryBuilder
                    ->andWhere('pa.evaluator_rol = :evaluator_rol')
                    ->setParameter('evaluator_rol', $data['evaluator_rol']);
            }
            
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'pollapplication');
            $paginator->setMaxPagerItems(5, 'pollapplication');
            $entities = $paginator->paginate($query, 'pollapplication')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('pollapplication');
            $endPageItem = $paginator->getEndPageItem('pollapplication');
            $totalItems = $paginator->getTotalItems('pollapplication');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }

            // Return Template
            
            return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:index.html.twig', array(
                'entities' => $entities,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'deletes_form' => $deletes_form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');
            
            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Pollapplication entity.
     *
     */
    public function newEvaluatorAction(Request $request, $pollschedulingid)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.entity_type, ps.type, ps.pollscheduling_anonymous anonymous, 
                      ps.type, ps.version, ps.entity_type, ps.entity_id, 
                      c.id company_id, c.name company_name, c.company_type, ps.initdate, ps.enddate, ps.deleted')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->where('ps.id = :pollscheduling 
                    AND ps.status = 1')
            ->setParameter(':pollscheduling', $pollschedulingid)
            ->getQuery()
            ->getResult();
            
        $pollscheduling = current($result);
        
        if (!$pollscheduling || $pollscheduling['anonymous'] || $pollscheduling['entity_type'] != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds_new', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
        
        if (!$pollscheduling['deleted']) {
            
            // Recovering data
            
            $course_options = Companycourse::getListCompanycourseOptions($this->getDoctrine(), $pollscheduling['company_id']);
            $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $pollscheduling['company_id']);
            $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $pollscheduling['company_id']);
            
            if ($pollscheduling['type'] == 'self_evaluation') {
                $dataOp = 'self';
                $statusField = TRUE;
            } else {
                $dataOp = '';
                $statusField = FALSE;
            }
          
            // Find Workinginformations
            
            $defaultData = array(
                'id' => '',
                'evaluated' => '', 
                'type' => '', 
                'evaluator_name' => '', 
                'evaluator_rol' => '', 
                'iden' => '',
                'type_value' => '',
                'attrib' => '', 
                'course' => '', 
                'section' => '', 
                'code' => '', 
                'surname' => '', 
                'lastname' => '', 
                'data_type' => '', 
                'grade' => '', 
                'study_year' => '', 
                'names' => '', 
                'initdate' => NULL, 
                'organizationalunit' => '', 
                'charge' => ''
            );
            $form = $this->createFormBuilder($defaultData)
                ->add('id', 'hidden', array(
                    'required' => TRUE
                ))
                ->add('iden', 'hidden', array(
                    'required' => TRUE
                ))
                ->add('type_value', 'hidden', array(
                    'required' => TRUE,
                    'data' => $dataOp 
                ))
                ->add('evaluated', 'text', array(
                    'required' => TRUE
                ))
                ->add('type', 'choice', array(
                    'choices' => array(
                        'one' => 'Uno a uno',
                        'collaborators' => 'Colaboradores',
                        'boss' => 'Jefe',
                        '360' => '360',
                        'self' => 'Autoevaluación'
                    ), 
                    'empty_value' => 'Choose an option',
                    'required' => TRUE,
                    'disabled' => $statusField,
                    'data' => $dataOp 
                ))
                ->add('evaluator_name', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_rol', 'choice', array(
                    'choices' => array(
                        'boss' => 'Jefe',
                        'pair' => 'Par',
                        'collaborator' => 'Colaborador',
                        'self' => 'Autoevaluado'
                    ), 
                    'empty_value' => 'Choose an option',
                    'required' => TRUE
                ))
                ->add('attrib', 'choice', array(
                    'choices' => array(
                        '0' => 'No',
                        '1' => 'Si',
                    ), 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('course', 'choice', array(
                    'choices' => $course_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('section', 'text', array(
                    'required' => FALSE
                ))
                ->add('code', 'text', array(
                    'required' => FALSE
                ))
                ->add('surname', 'text', array(
                    'required' => FALSE
                ))
                ->add('lastname', 'text', array(
                    'required' => FALSE
                ))
                ->add('names', 'text', array(
                    'required' => FALSE
                ))
                ->add('data_type', 'choice', array(
                    'choices' => array(
                        'working' => 'Laboral',
                        'university' => 'Universidad',
                        'school' => 'Colegio',
                    ), 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('organizationalunit', 'choice', array(
                    'choices' => $organizationalunit_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('charge', 'choice', array(
                    'choices' => $charge_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('grade', 'choice', array(
                    'choices' => array(
                        '1p' => '1ro de primaria', 
                        '2p' => '2do de primaria', 
                        '3p' => '3ro de primaria', 
                        '4p' => '4to de primaria', 
                        '5p' => '5to de primaria', 
                        '6p' => '6to de primaria', 
                        '1s' => '1ro de secundaria', 
                        '2s' => '2do de secundaria', 
                        '3s' => '3ro de secundaria', 
                        '4s' => '4to de secundaria', 
                        '5s' => '5to de secundaria'
                    ), 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('study_year', 'text', array(
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'id' => '',
                'iden' => '',
                'type_value' => '', 
                'evaluated' => '', 
                'type' => '', 
                'evaluator_name' => '', 
                'evaluator_rol' => '', 
                'attrib' => '', 
                'course' => '', 
                'section' => '', 
                'code' => '', 
                'surname' => '', 
                'lastname' => '', 
                'names' => '', 
                'data_type' => '', 
                'grade' => '', 
                'study_year' => '', 
                'initdate' => NULL, 
                'organizationalunit' => '', 
                'charge' => ''
            );
            if($request->getMethod() == 'POST') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            if ($data['type_value'] != '') {
            
                // Recover pollapplications ids
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                $wsp_result = $repository->createQueryBuilder('pa')
                    ->select('wi.id')
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                    ->where('pa.pollscheduling = :pollscheduling 
                            AND pa.evaluated_id = :evaluated 
                            AND wi.company = :company 
                            AND pa.deleted = :deleted')
                    ->setParameter('pollscheduling', $pollscheduling['id'])
                    ->setParameter('evaluated', $data['id'])
                    ->setParameter('company', $pollscheduling['company_id'])
                    ->setParameter('deleted', FALSE)
                    ->orderBy('wi.id', 'ASC')
                    ->groupBy('wi.id')
                    ->getQuery()
                    ->getResult();
                
                $wsp_ids = '';
                
                // We set ids chain pollapplications
                
                if (!empty($wsp_result)) {
                    
                    $wsp_count = count($wsp_result);
                    $i = 1;
                    $wsp_ids = '';
                    
                    foreach ($wsp_result as $wsp) {
                        $wsp_ids .= $wsp['id'];
                        if ($i < $wsp_count) {
                            $wsp_ids .= ',';
                        }
                        $i++;
                    }
                }

                // Query
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $queryBuilder = $repository->createQueryBuilder('psp');
            
                if ($data['type_value'] == 'collaborators') {
                    $queryBuilder
                        ->select('wi.id, wi.code, u.names, u.surname, u.lastname, 
                                  co.name companyorganizationalunit, cc.name companycharge, 
                                  wi.datein, wi.status')
                        ->innerjoin('psp.workinginformation', 'wi')
                        ->leftJoin('wi.companyorganizationalunit', 'co')
                        ->leftJoin('wi.companycharge', 'cc')
                        ->innerJoin('wi.user', 'u')
                        ->where('psp.pollscheduling = :pollscheduling 
                                AND wi.company = :company
                                AND wi.status = 1
                                AND wi.boss = :boss')
                        ->setParameter('boss', $data['id']);
                }
                elseif ($data['type_value'] == 'boss') {
                    $queryBuilder
                        ->select('wib.id, wib.code, u.names, u.surname, u.lastname, 
                                  co.name companyorganizationalunit, cc.name companycharge, 
                                  wib.datein, wib.status')
                        ->innerjoin('psp.workinginformation', 'wi')
                        ->innerJoin('FishmanAuthBundle:Workinginformation', 'wib', 'WITH', 'wi.boss = wib.id')
                        ->leftJoin('wib.companyorganizationalunit', 'co')
                        ->leftJoin('wib.companycharge', 'cc')
                        ->innerJoin('wib.user', 'u')
                        ->where('psp.pollscheduling = :pollscheduling 
                                AND wib.company = :company
                                AND wib.status = 1
                                AND wi.id = :workinginformation')
                        ->setParameter('workinginformation', $data['id']);
                }
                elseif ($data['type_value'] == '360') {
                    $queryBuilder
                        ->select('wi.id, wi.code, u.names, u.surname, u.lastname, 
                                  co.name companyorganizationalunit, cc.name companycharge, 
                                  wi.datein, wi.status, wi.grade, wi.study_year, wi.type')
                        ->innerjoin('psp.workinginformation', 'wi')
                        ->leftJoin('wi.companyorganizationalunit', 'co')
                        ->leftJoin('wi.companycharge', 'cc')
                        ->innerJoin('wi.user', 'u')
                        ->where('psp.pollscheduling = :pollscheduling 
                                AND wi.company = :company
                                AND wi.status = 1');
                }
                elseif ($data['type_value'] == 'self') {
                    $queryBuilder
                        ->select('wi.id, wi.code, u.names, u.surname, u.lastname, 
                                  co.name companyorganizationalunit, cc.name companycharge, 
                                  wi.datein, wi.status')
                        ->innerjoin('psp.workinginformation', 'wi')
                        ->leftJoin('wi.companyorganizationalunit', 'co')
                        ->leftJoin('wi.companycharge', 'cc')
                        ->innerJoin('wi.user', 'u')
                        ->where('psp.pollscheduling = :pollscheduling 
                                AND wi.company = :company
                                AND wi.status = 1
                                AND wi.id = :workinginformation')
                        ->setParameter('workinginformation', $data['id']);
                }elseif ($data['type_value'] == 'one') {
                    $queryBuilder
                        ->select('wi.id, wi.code, wi.grade, wi.study_year, u.names, 
                                  u.surname, u.lastname, co.name companyorganizationalunit, 
                                  cc.name companycharge, wi.datein, wi.status')
                        ->innerjoin('psp.workinginformation', 'wi')
                        ->leftJoin('wi.companyorganizationalunit', 'co')
                        ->leftJoin('wi.companycharge', 'cc')
                        ->innerJoin('wi.user', 'u')
                        ->where('psp.pollscheduling = :pollscheduling 
                                AND wi.company = :company
                                AND wi.status = 1
                                AND wi.id = :workinginformation')
                        ->setParameter('workinginformation', $data['iden']);
                }
                
                $queryBuilder
                    ->setParameter('pollscheduling', $pollschedulingid)
                    ->setParameter('company', $pollscheduling['company_id'])
                    ->orderBy('wi.id', 'ASC');
                    
                // Check if there are people assigned
                
                if ($wsp_ids != '' && $data['type_value'] != 'self') {
                    $queryBuilder
                        ->andWhere('wi.id NOT IN(' . $wsp_ids . ')');
                }
                
                // Add the arguments that are being asked
                if ($data['code'] != '') {
                    $queryBuilder
                        ->andWhere('wi.code LIKE :code')
                        ->setParameter('code', '%' . $data['code'] . '%');
                }
                if (!empty($data['surname'])) {
                    $queryBuilder
                        ->andWhere('u.surname LIKE :surname')
                        ->setParameter('surname', '%' . $data['surname'] . '%');
                }
                if (!empty($data['lastname'])) {
                    $queryBuilder
                        ->andWhere('u.lastname LIKE :lastname')
                        ->setParameter('lastname', '%' . $data['lastname'] . '%');
                }
                if (!empty($data['names'])) {
                    $queryBuilder
                        ->andWhere('u.names LIKE :names')
                        ->setParameter('names', '%' . $data['names'] . '%');
                }
                if ($data['data_type'] == 'school') {

                    if (!empty($data['data_type'])) {
                        $queryBuilder
                            ->andWhere('wi.type LIKE :type')
                            ->setParameter('type', '%schoolinformation%');
                    }

                    if (!empty($data['grade'])) {
                        $queryBuilder
                            ->andWhere('wi.grade LIKE :grade')
                            ->setParameter('grade', '%' . $data['grade'] . '%');
                    }
                    if (!empty($data['study_year'])) {
                        $queryBuilder
                            ->andWhere('wi.study_year LIKE :study_year')
                            ->setParameter('study_year', '%' . $data['study_year'] . '%');
                    }
                }
                if ($data['data_type'] == 'working') {
                    if (!empty($data['data_type'])) {
                        $queryBuilder
                            ->andWhere('wi.type LIKE :type')
                            ->setParameter('type', '%workinginformation%');
                    }

                    if (!empty($data['initdate'])) {
                        $queryBuilder
                            ->andWhere('wi.datein = :initdate')
                            ->setParameter('initdate', $data['initdate']);
                    }

                    if ($data['organizationalunit'] != '') {
                        $queryBuilder
                            ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                            ->setParameter('organizationalunit', $data['organizationalunit']);
                    }

                    if ($data['charge'] != '') {
                        $queryBuilder
                            ->andWhere('wi.companycharge = :charge')
                            ->setParameter('charge', $data['charge']);
                    }
                }
                
                $query = $queryBuilder->getQuery();
    
                if ($data['type_value'] == 'one') {
                    
                    // User
                    $userBy = $this->get('security.context')->getToken()->getUser();
                    
                    $data = $request->request->get('form');
                    
                    $workinginformations = '';
                    $attArray = array();
                    
                    $pollschedulingOb = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollscheduling['id']);
                    
                    $evaluator[] = $data['iden'];
                    $evaluated[] = $data['id'];
                    $attArray[0] = $data['course'];
                    $attArray[1] = $data['section'];
                    
                    // Add Pollapplications
                    $pollapplication = Pollapplication::addPollapplications(
                        $this->getDoctrine(), 
                        $evaluator, 
                        $evaluated, 
                        'pollschedulingpeople', 
                        $pollschedulingOb, 
                        $userBy, 
                        NULL, 
                        $attArray,
                        $data['evaluator_rol']
                    );
                    
                    if ($pollapplication['message_error']) {
                        $session->getFlashBag()->add('error', $pollapplication['message_error']);
                    }

                    $session->getFlashBag()->add('status', 'Se asignaron ' . $pollapplication['number_register'] . ' de Evaluacion(es) a la encuesta satisfactoriamente.');

                    if($request->request->get('saveandclose', true)){
                        return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
                            'pollschedulingid' => $pollschedulingid
                        )));
                    }
                    else {
                        return $this->redirect($this->generateUrl('pollschedulingevaluator_new', array(
                            'pollschedulingid' => $pollschedulingid
                        )));
                    }

                }
                
                // Paginator
                
                $paginator = $this->get('ideup.simple_paginator');
                $paginator->setItemsPerPage(20, 'workinginformationevaluator');
                $paginator->setMaxPagerItems(5, 'workinginformationevaluator');
                $entities = $paginator->paginate($query, 'workinginformationevaluator')->getResult();
                
                $startPageItem = $paginator->getStartPageItem('workinginformationevaluator');
                $endPageItem = $paginator->getEndPageItem('workinginformationevaluator');
                $totalItems = $paginator->getTotalItems('workinginformationevaluator');
        
                if ($totalItems == 0) {
                    $info_paginator = 'No hay registros que mostrar';
                }
                else {
                    $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
                }
                
            }
            else {
                $entities = '';
                $paginator = '';
                $info_paginator = '';
            }
    
            // Asigned registers
            
            $defaultDataRegisters = array(
                'evaluated' => $data['id'],
                'evaluators' => array()
            );
            if ($data['type_value'] == 'self') {
                $defaultDataRegisters['evaluators_rol'] = 'self';
            }
            $asigneds_form = $this->createFormBuilder($defaultDataRegisters)
                ->add('evaluated', 'hidden', array(
                    'required' => true
                ))
                ->add('evaluators', 'choice', array(
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => true
                ))
                ->add('evaluators_rol', 'choice', array(
                    'choices' => array(
                        'boss' => 'Jefe',
                        'pair' => 'Par',
                        'collaborator' => 'Colaborador',
                        'self' => 'Autoevaluado'
                    ),
                    'empty_value' => 'Choose an option',
                    'required' => true
                ))
                ->getForm();
            
            // Recover Persons
            $workinginformations = Pollschedulingpeople::getPollschedulingPeoplesList($this->getDoctrine(), $pollscheduling['company_id'], $pollscheduling['id']);
            
            for ($i = 0; $i < count($workinginformations) ; $i++) {
                $workinginformations[$i]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                                    $workinginformations[$i]['lastname'] . ' ' . 
                                                    $workinginformations[$i]['surname'] . ' (' . 
                                                    Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
            }
            
            $workinginformations = 'var workinginformations = ' . json_encode($workinginformations);

            // Return new.html.twig            
            return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:new.html.twig', array(
                'entities' => $entities,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'asigneds_form' => $asigneds_form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'jsonworkinginformations' => $workinginformations
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     */
    public function choiceByEvaluatedAction(Request $request, $pollschedulingid)
    {
        $evaluated = $_GET['form_id'];
        $em = $this->getDoctrine()->getManager();
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);

        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
        $pollapplication = $repository->createQueryBuilder('pa')
            ->where('pa.evaluated_id = :evaluated ')
            ->setParameter('evaluated', $evaluated)
            ->getQuery()
            ->getResult();
        $evaluatorarray = array();
        
        foreach ($pollapplication as $pa) {
          $evaluatorarray[] = $pa->getEvaluatorId();
        }

        if ($pollscheduling->getEntityType() == 'workshopscheduling') {

            $repository = $this->getDoctrine()->getManager()->getRepository('FishmanWorkshopBundle:Workshopapplication');
            $workinginformations = $repository->createQueryBuilder('wa')
                ->select('wi.id, wi.type, wi.code, u.names, u.surname, u.lastname')
                ->where('wi.company = :company
                        AND wa.workshopscheduling = :workshopscheduling
                        AND wa.deleted = :deleted')
                ->innerJoin('wa.workinginformation', 'wi')
                ->innerJoin('wi.user', 'u')
                ->setParameter('company', $pollscheduling->getCompanyId())
                ->setParameter('workshopscheduling', $pollscheduling->getEntityId())
                ->setParameter('deleted', FALSE)
                ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC')
                ->getQuery()
                ->getResult();
        }
        else {
            $workinginformations = Pollschedulingpeople::getPollschedulingPeoplesList($this->getDoctrine(), $pollscheduling->getCompanyId(), $pollschedulingid);
        }
        
        for ($i = 0; $i < count($workinginformations) ; $i++) {
            $workinginformations[$i]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                                $workinginformations[$i]['surname'] . ' ' . 
                                                $workinginformations[$i]['lastname'] . ' (' . 
                                                Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
        }
        
        $response = new Response(json_encode($workinginformations));  
        return $response;

    }

    /**
     * Creates a new Pollapplication entity.
     *
     */
    public function asignedsEvaluatorAction(Request $request, $pollschedulingid)
    {   
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $data = $request->request->get('form');
        
        if (!empty($data['evaluators'])) {
            
            $em = $this->getDoctrine()->getManager();
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
                $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollscheduling'));
            }
            
            $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
            
            $attArray = array();
            
            $numberRegister = 0;
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            // Recovering data
            
            if (isset($data['iden'])) {
                $evaluator[] = $data['iden'];
            }
            $evaluated = NULL;
            
            // Add Pollapplications
            
            if (in_array($company->getCompanyType(), array('school', 'university'))) {
                if (isset($data['course'])) {
                    $attArray[0]= $data['course'];
                }
                if (isset($data['section'])) {
                    $attArray[1]= $data['section'];
                }
                $pollapplication = Pollapplication::addPollapplications(
                    $this->getDoctrine(), 
                    $data['evaluators'], 
                    $data['evaluated'], 
                    'pollschedulingpeople', 
                    $pollscheduling, 
                    $userBy, 
                    NULL,
                    $attArray,
                    $data['evaluators_rol']
                );
            }
            else {
                $pollapplication = Pollapplication::addPollapplications(
                    $this->getDoctrine(), 
                    $data['evaluators'], 
                    $data['evaluated'], 
                    'pollschedulingpeople', 
                    $pollscheduling, 
                    $userBy, 
                    NULL,
                    NULL,
                    $data['evaluators_rol']
                );
            }
            
            if ($pollapplication['message_error']) {
                $session->getFlashBag()->add('error', $pollapplication['message_error']);
            }
            
            $session->getFlashBag()->add('status', 'Se asignaron ' . $pollapplication['number_register'] . ' Evaluacion(es) a la encuesta satisfactoriamente.');
        }
        else {
            $session->getFlashBag()->add('error', 'No escogio ningún evaluador para asignar a la encuesta.');
        }

        if($request->request->get('saveandclose', true)){
            return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
        else {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_new', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
    }

    /**
     * Displays a form to edit an existing Pollapplication entity.
     *
     */
    public function editEvaluatorAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Pollapplication Info
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
        $query = $repository->createQueryBuilder('pa')
            ->select('pa.id, wi_one.id evaluated_id, wi_one.type evaluated_type, wi_one.code evaluated_code, 
                      u_one.names evaluated_names, u_one.surname evaluated_surname, u_one.lastname evaluated_lastname, 
                      wi_two.id evaluator_id, wi_two.type evaluator_type, wi_two.code evaluator_code, 
                      u_two.names evaluator_names, u_two.surname evaluator_surname, u_two.lastname evaluator_lastname, 
                      pa.evaluator_rol, pa.deleted, pa.course, pa.section, ps.id pollscheduling_id, 
                      ps.title pollscheduling_title, ps.version pollscheduling_version, ps.entity_type, ps.entity_id, 
                      ps.company_id, c.company_type, ps.pollscheduling_anonymous anonymous, ps.type, 
                      ps.deleted pollscheduling_deleted, ps.initdate, ps.enddate, ps.company_id')
            ->innerJoin('pa.pollscheduling', 'ps')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
            ->innerJoin('wi_one.user', 'u_one')
            ->innerJoin('wi_two.user', 'u_two')
            ->where('pa.id = :pollapplication')
            ->setParameter('pollapplication', $id)
            ->getQuery()
            ->getResult();
            
        $entity = current($query);
        
        if (!$entity || $entity['anonymous'] || $entity['entity_type'] != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        elseif ($entity['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                'pollschedulingid' => $entity['pollscheduling_id']
            )));
        }

        $company = $em->getRepository('FishmanEntityBundle:Company')->find($entity['company_id']);

        //Recovery Companycourse
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycourse');
        $result = $repository->createQueryBuilder('cc')
            ->select('cc.id, cc.name')
            ->innerJoin('cc.company', 'c')
            ->where('cc.company = :company 
                    AND cc.status = 1')
            ->setParameter(':company', $entity['company_id'])
            ->orderBy('cc.id', 'ASC')
            ->getQuery()
            ->getResult();

        $courses = array();
                      
        foreach($result as $r) {
            $courses[$r['id']] = $r['name'];
        }
            
        if (!$entity['pollscheduling_deleted']) {
          
            if ($entity['deleted']) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollscheduling'));
            }
            
            // Form Edit Evaluated
            
            $defaultData = array(
                'id' => $entity['evaluator_id'],
                'evaluator' => $entity['evaluator_names'] . ' ' . $entity['evaluator_surname'] . ' ' . $entity['evaluator_lastname'] . ' (' . Workinginformation::getTypeName($entity['evaluator_type']) . ')',
                'evaluator_rol' => $entity['evaluator_rol']
            );
            $edit_form = $this->createFormBuilder($defaultData)
                ->add('id', 'hidden', array(
                    'required'=>true
                ))
                ->add('evaluator', 'text', array(
                    'required'=>true
                ))
                ->add('evaluator_rol', 'choice', array(
                    'choices' => array(
                        'boss' => 'Jefe',
                        'pair' => 'Par',
                        'collaborator' => 'Colaborador',
                        'self' => 'Autoevaluado'
                    ),
                    'empty_value' => 'Choose an option',
                    'required' => false,
                    'data' => $entity['evaluator_rol'] 
                ))
                ->add('course', 'choice', array(
                    'choices' => $courses, 
                    'empty_value' => 'Choose an option',
                    'required' => false,
                    'data' => $entity['course'] 
                ))
                ->add('section', 'text', array(
                    'required' => false,
                    'data' => $entity['section'] 
                ))
                ->getForm();
            
            //Posible evaluated, for the company
            
            $repository = $this->getDoctrine()->getManager()->getRepository('FishmanPollBundle:Pollschedulingpeople');
            $workinginformations = $repository->createQueryBuilder('psp')
                ->select('wi.id, wi.type, wi.code, u.names, u.surname, u.lastname')
                ->where('wi.company = :company
                        AND psp.pollscheduling = :pollscheduling')
                ->innerJoin('psp.workinginformation', 'wi')
                ->innerJoin('wi.user', 'u')
                ->setParameter('company', $entity['company_id'])
                ->setParameter('pollscheduling', $entity['pollscheduling_id'])
                ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC')
                ->getQuery()
                ->getResult();
            
            $count_wis = count($workinginformations);
            $j = 0;
            $wis = array();
            
            //Create
            for ($i = 0; $i < $count_wis; $i++) {
                $query = FALSE; 
                if ($entity['evaluator_id'] != $workinginformations[$i]['id']) {
                    $query = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
                                'evaluated_id' => $entity['evaluated_id'], 
                                'evaluator_id' => $workinginformations[$i]['id'], 
                                'deleted' => FALSE));

                    if (empty($query)) {
                        $wis[$j]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                            $workinginformations[$i]['lastname'] . ' ' . 
                                            $workinginformations[$i]['surname'] . ' (' . 
                                            Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
                        $j++;
                    }
                    else {
                        unset($workinginformations[$i]);
                    }
                    
                }
                else {
                    $wis[$j]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                        $workinginformations[$i]['lastname'] . ' ' . 
                                        $workinginformations[$i]['surname'] . ' (' . 
                                        Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
                    $j++;
                }
            }
            
            $workinginformations = 'var workinginformations = ' . json_encode($wis);
            
            //Return template edit pollscheduling evaluator
            return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:edit.html.twig', array(
                'entity' => $entity,
                'edit_form' => $edit_form->createView(),
                'jsonworkinginformations' => $workinginformations
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Edits an existing Pollapplication entity.
     *
     */
    public function updateEvaluatorAction(Request $request, $id)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollapplication
        
        $entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($id);
        if (!$entity || $entity->getPollscheduling()->getPollschedulingAnonymous() || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $data = $request->request->get('form');
        
        if (!empty($data['id'])) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
                    
            //Remove notification execution
            Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatorId(), 'pollapplication', $entity->getId());
            
            $entity->setEvaluatorId($data['id']);
            if ($entity->getEvaluatedId() == $data['id'] && $data['evaluator_rol'] != 'self') {
                $session->getFlashBag()->add('error', 'Esta es una autoevaluación, el rol del eveluador ha sido cambiado a "Autoevaluado".');
                $entity->setEvaluatorRol('self');
            }
            elseif ($entity->getEvaluatedId() != $data['id'] && $data['evaluator_rol'] == 'self') {
                $session->getFlashBag()->add('error', 'Esta no es una autoevaluación, el rol del eveluador no ha sido asignado.');
                $entity->setEvaluatorRol('');
            }
            else {
                $entity->setEvaluatorRol($data['evaluator_rol']);
            }
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            if (isset($data['attrib'])) {
                if ($data['attrib'] != 0 ) {
                    $entity->setCourse($data['course']);
                    $entity->setSection($data['section']);
                } else {
                    $entity->setCourse(NULL);
                    $entity->setSection(NULL);
                }
            }
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
            'pollschedulingid' => $entity->getPollscheduling()->getId()
        )));
    }

    /**
     * Drop an Pollapplication entity.
     *
     */
    public function dropEvaluatorAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
        $query = $repository->createQueryBuilder('pa')
            ->select('pa.id, wi_one.id evaluated_id, u_one.names evaluated_names, u_one.surname evaluated_surname, 
                      u_one.lastname evaluated_lastname, wi_two.id evaluator_id, u_two.names evaluator_names, 
                      u_two.surname evaluator_surname, u_two.lastname evaluator_lastname, pa.deleted, ps.id pollscheduling_id, 
                      ps.title pollscheduling_title, ps.type, ps.pollscheduling_anonymous anonymous, ps.entity_type, 
                      ps.version pollscheduling_version, ps.deleted pollscheduling_deleted, ps.initdate, ps.enddate')
            ->innerJoin('pa.pollscheduling', 'ps')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
            ->innerJoin('wi_one.user', 'u_one')
            ->innerJoin('wi_two.user', 'u_two')
            ->where('pa.id = :pollapplication')
            ->setParameter('pollapplication', $id)
            ->getQuery()
            ->getResult();
            
        $entity = current($query);
        if (!$entity || $entity['anonymous'] || $entity['entity_type'] != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        elseif ($entity['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds_drop', array(
                'id' => $entity['id']
            )));
        }
        
        if (!$entity['pollscheduling_deleted']) {
          
            if ($entity['deleted']) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollscheduling'));
            }
          
            $deleteForm = $this->createDeleteForm($id);
    
            return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:drop.html.twig', array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Drop multiple an Pollapplication entities.
     *
     */
    public function dropmultipleEvaluatorAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        elseif ($pollscheduling->getType() == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds_dropmultiple', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
        
        if (!$pollscheduling->getDeleted()) {
          
            // Recover register of form
            
            $data = $request->request->get('form_deletes');
            
            if (isset($data['registers'])) {
            
                $registers = '';
                $registers_count = count($data['registers']);
                $i = 1;
                
                if (!empty($data['registers'])) {
                  
                    foreach($data['registers'] as $r) {
                        $registers .= $r;
                        if ($i < $registers_count) {
                            $registers .= ',';
                        }
                        $i++;
                    }
        
                    // Recover pollapplication of pollscheduling
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                    $entities = $repository->createQueryBuilder('pa')
                        ->select('pa.id, wi_one.id evaluated_id, u_one.names evaluated_names, u_one.surname evaluated_surname, u_one.lastname evaluated_lastname, 
                                  wi_two.id evaluator_id, u_two.names evaluator_names, u_two.surname evaluator_surname, u_two.lastname evaluator_lastname')
                        ->innerJoin('pa.pollscheduling', 'ps')
                        ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
                        ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
                        ->innerJoin('wi_one.user', 'u_one')
                        ->innerJoin('wi_two.user', 'u_two')
                        ->where('pa.id IN(' . $registers . ')')
                        ->getQuery()
                        ->getResult();
                        
                    if (!$entities) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la Programación de Encuesta.');
                        return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
                            'pollschedulingid' => $pollschedulingid
                        )));
                    }
                
                    $deleteMultipleForm = $this->createDeleteMultipleForm($registers);
                
                    return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:dropmultiple.html.twig', array(
                        'entities' => $entities,
                        'pollscheduling' => $pollscheduling,
                        'delete_form' => $deleteMultipleForm->createView()
                    ));
                }
            }
            
            $session->getFlashBag()->add('error', 'No se ha seleccionado ningún registro para eliminar.');
            
            return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
                'pollschedulingid' => $pollschedulingid
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
        
    }

    /**
     * Drop all an Pollapplication entities.
     *
     */
    public function dropallEvaluatorAction($pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        elseif ($pollscheduling->getType() == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds_dropall', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
        
        if (!$pollscheduling->getDeleted()) {
            
            $deleteAllForm = $this->createDeleteAllForm();
            
            return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:dropall.html.twig', array(
                'pollscheduling' => $pollscheduling,
                'delete_all_form' => $deleteAllForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
        
    }

    private function createDeleteMultipleForm($registers)
    {
        return $this->createFormBuilder(array('registers' => $registers))
            ->add('registers', 'hidden')
            ->getForm()
        ;
    }

    private function createDeleteAllForm()
    {
        return $this->createFormBuilder(array('delete_all' => TRUE))
            ->add('delete_all', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Delete a Pollapplication entity.
     *
     */
    public function deleteEvaluatorAction(Request $request, $pollschedulingid, $id)
    { 
        $em = $this->getDoctrine()->getManager();
        
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
            
            $entity->setDeleted(TRUE);

            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Todas las evaluaciones han sido eliminadas satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Deletes multiple a Pollapplication entities.
     *
     */
    public function deletemultipleEvaluatorAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $form = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();   
        
        if ($form['registers'] != '') {
            
            // Recover Pollapplications of form
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $entities = $repository->createQueryBuilder('pa')
                ->where('pa.id IN(' . $form['registers'] . ')')
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            foreach($entities as $entity) {
                
                Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
                
                $entity->setDeleted(TRUE);
                
                $em->persist($entity);
                $em->flush();
            }                                             
            $session->getFlashBag()->add('status', 'Los registros fueron eliminados satisfactoriamente.');
            
        }

        return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Deletes all Pollapplication entities.
     *
     */
    public function deleteallEvaluatorAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $form = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();   
        
        if ($form['delete_all']) {
            
            // Recover Pollapplications of form
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $entities = $repository->createQueryBuilder('pa')
                ->where('pa.pollscheduling = :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No existen evaluaciones en esta Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            foreach($entities as $entity) {
                
                Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
                
                $entity->setDeleted(TRUE);
                
                $em->persist($entity);
                $em->flush();
            }
            $session->getFlashBag()->add('status', 'Los registros fueron eliminados satisfactoriamente.');
            
        }
        
        return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Import an Pollapplication entity.
     *
     */
    public function importEvaluatorAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Mensaje de que no hay registros
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        elseif ($pollscheduling->getType() == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds_import', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
                            
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $companyId = $company->getId();
        $companyCode = $company->getCode();
        $companyName = $company->getName();
        $companyType = $company->getCompanyType();
        
        if (!$pollscheduling->getDeleted()) {
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplicationimport');
            $query = $repository->createQueryBuilder('pai')
                ->select('COUNT(pai.id) cpais')
                ->where('pai.pollscheduling_id = :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->groupBy('pai.pollscheduling_id')
                ->getQuery()
                ->getResult();
            
            $paimports = current($query);
            if ($paimports['cpais'] > 0) {
                $session->getFlashBag()->add('status', 'Existen Evaluaciones pendientes a importar.');
                return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:import.html.twig', array(
                    'pollscheduling' => $pollscheduling,
                    'pollapplicationimport' => TRUE,
                    'companytype' => $companyType
                ));
            }
            
            $defaultData = array();
            $form = $this->createForm(new PollevaluatorimportType(), $defaultData);

            $form2 = false;
            $data = '';
            $people = false;
            $evaluations = false;
            $message = '';
            $messageError = '';
            $existingwiemail = '';
            $numberRegister = 0;

            if ($request->isMethod('POST')) {
                
                //The user is confirmed to persist data as 
                $importFormData = $request->request->get('form', false);
    
                if ($importFormData) {
                    $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                         ->find($importFormData['temporalimportdata_id']);
                    if(!$temporalentity){
                        return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:import.html.twig', array(
                            'form' => $form->createView(),
                            'form2' => $form2->createView(),
                            'pollscheduling' => $pollscheduling
                        ));
                    }
                    
                    $evaluations = $temporalentity->getData();
                    $userBy = $this->get('security.context')->getToken()->getUser();
                    
                    // suspend auto-commit
                    $em->getConnection()->beginTransaction();
                    
                    // Try and make the transaction
                    try {
                        
                        if (in_array($companyType, array('working', 'school', 'university'))) {
                            foreach ($evaluations as $evaluation) {
                                if ($evaluation['evaluator']['valid'] && $evaluation['evaluated']['valid']) {
                                    
                                    $evaluator = Pollpersonimport::addRegister($this->getDoctrine(), $evaluation['evaluator']);
                                    $evaluated = Pollpersonimport::addRegister($this->getDoctrine(), $evaluation['evaluated']);
                                    
                                    $pai = new Pollapplicationimport();
                                    $pai->setPollschedulingId($pollschedulingid);
                                    $pai->setEvaluatedId($evaluated->getId());
                                    $pai->setEvaluatorId($evaluator->getId());
                                    $pai->setEvaluatedCode($evaluation['evaluated']['code']);
                                    $pai->setEvaluatorCode($evaluation['evaluator']['code']);
                                    $pai->setEvaluatorRol($evaluation['evaluator']['evaluator_rol']);
                                    $pai->setCourse($evaluation['evaluated']['course_id']);
                                    $pai->setSection($evaluation['evaluated']['section']);
                                    $pai->setStatus(FALSE);
                                    
                                    //jtt: agregado 20150812
                                    $pai->setCargoGlobal($evaluation['evaluator']['cargo_global']);
                                    $pai->setNivelResponsabilidad($evaluation['evaluator']['nivel_responsabilidad']);
                                    
                                    $em->persist($pai);
                                    $em->flush();
                                    
                                    $numberRegister++;
                                }
                            }
                        }
                        
                        //Delete temporal data
                        $em->remove($temporalentity);
                        $em->flush();
                        
                        // Delete temporal files
                        Pollscheduling::deleteTemporalFiles('../tmp/');
                        
                        // Try and commit the transactioncurrentversioncurrentversion
                        $em->getConnection()->commit();
                    } catch (Exception $e) {
                        // Rollback the failed transaction attempt
                        $em->getConnection()->rollback();
                        throw $e;
                    }
                    
                    if ($numberRegister > 0) {
                        $session->getFlashBag()->add('status', 'Existen '.$numberRegister.' Evaluaciones pendientes a importar.');
                        return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:import.html.twig', array(
                            'pollscheduling' => $pollscheduling,
                            'pollapplicationimport' => TRUE,
                            'companytype' => $companyType
                        ));
                    }
                    else {
                        $session->getFlashBag()->add('error', 'No hay Evaluaciones para importar.');
                    }
    
                    $people = false;
    
                } //End have temporarlimportdata input
                else {
                    
                    $form->bind($request);
                    $data = $form->getData();
    
                    // We need to move the file in order to use it
                    $randomName = rand(1, 10000000000);
                    $randomName = $randomName . '.xlsx';
                    $directory = __DIR__.'/../../../../tmp';
                    $data['file']->move($directory, $randomName);
    
                    if (null !== $data['file']) {
                        $fileName = $data['file']->getClientOriginalName();
                        $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                    }
    
                    if ($data['file']->document == 'xlsx') {
                        
                        $inputFileName = $directory . '/' . $randomName;
                        $phpExcel = PHPExcel_IOFactory::load($inputFileName);
      
                        $worksheet = $phpExcel->getSheet(0);
                        
                        $fileEvaluationExcel = trim($worksheet->getCellByColumnAndRow(1, 1)->getValue());
                        $fileTypeExcel = trim($worksheet->getCellByColumnAndRow(1, 2)->getValue());
                        
                        if (in_array($fileEvaluationExcel, array('evaluateds', 'selves'))) {
                            
                            if (in_array($fileTypeExcel, array('working', 'school', 'university'))) {
                                
                                // Data defaults
                                
                                $typeIdentity = '';
                                $birthday = '';
                                
                                $i = 0;
                                $row = 5;
                                $group = -1;
                                $subgroup = '';
                                $people = array();
                                
                                do {
                                    
                                    $field = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                                    
                                    if (in_array($field, array('group', 'GROUP'))) {
                                        $group++;
                                    }
                                    elseif (in_array($field, array('evaluateds', 'EVALUATEDS', 'evaluateds/evaluators', 'EVALUATEDS/EVALUATORS'))) {
                                        $subgroup = 'evaluateds';
                                        $i = 0;
                                    }
                                    elseif (in_array($field, array('evaluators', 'EVALUATORS'))) {
                                        $subgroup = 'evaluators';
                                        $i = 0;
                                    }
                                    else {
                                        
                                        // Data default
                                        
                                        $code = '';
                                        $names = '';
                                        $birthday = '';
                                        $email = '';
                                        $emailInformation = '';
                                        
                                        $headquarterId = '';
                                        $headquarterName = '';
                                        $status = 0;
                                        
                                        $ouId = '';
                                        $ouName = '';
                                        $ouUnitType = '';
                                        $chargeId = '';
                                        $chargeName = '';
                                        
                                        $facultyCode = '';
                                        $gradeCode = '';
                                        $studyYear = '';
                                        $academicYear = '';
                                        
                                        $courseId = '';
                                        $courseCode = '';
                                        $courseName = '';
                                        $section = '';
                                        
                                        // Data validation
                                        
                                        $message = '';
                                        $valid = 1;
                                        
                                        // Get Data
                                        
                                        $type = strtolower(trim($worksheet->getCellByColumnAndRow(0, $row)->getValue()));
                                        $rol = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                                        
                                        // Data User
                                        
                                        $surname = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                                        $lastname = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                                        $names = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                                        $typeIdentityRow = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
                                        if (in_array($typeIdentityRow, array('d', 'e', 'p'))) {
                                            $typeIdentity = User::getDocumentTypeByEquivalent($typeIdentityRow);
                                        }
                                        $numberIdentity = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
                                        $birthdayRow = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                                        if ($birthdayRow) {
                                            $birthday = \DateTime::createFromFormat('d/m/Y', $birthdayRow);
                                        }
                                        $sex = trim($worksheet->getCellByColumnAndRow(8, $row)->getValue());
                                        $email = trim($worksheet->getCellByColumnAndRow(9, $row)->getValue());
                                        
                                        // Generate Code
                                        $code = $numberIdentity;
                                        
                                        // Data Working
                                        
                                        $headquarterCode = trim($worksheet->getCellByColumnAndRow(10, $row)->getValue());
                                        $emailInformation = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                        
                                        if ($type == 'working') {
                                            
                                            $ouCode = trim($worksheet->getCellByColumnAndRow(11, $row)->getValue());
                                            $chargeCode = trim($worksheet->getCellByColumnAndRow(12, $row)->getValue());
                                            if ($fileTypeExcel == 'school') {
                                                $evaluatorRol = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                            }
                                            elseif ($fileTypeExcel == 'university') {
                                                $evaluatorRol = trim($worksheet->getCellByColumnAndRow(19, $row)->getValue());
                                            }
                                            elseif ($fileTypeExcel == 'working') {
                                                $evaluatorRol = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                                //jtt: agregado 20150812
                                                $cargoGlobal = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                                $nivelResponsabilidad = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                            }
                                            
                                        }
                                        elseif ($type == 'school') {
                                            
                                            $gradeCode = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                            $studyYear = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                            $section = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                            $courseCode = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                            $evaluatorRol = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                            
                                        }
                                        elseif ($type == 'university') {
                                            
                                            // Data University
                                            
                                            $emailInformation = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                            $facultyCode = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                            $careerCode = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                            $academicYear = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                            $section = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                            $courseCode = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                            $evaluatorRol = trim($worksheet->getCellByColumnAndRow(19, $row)->getValue());
                                            
                                            $facultyId = '';
                                            $facultyName = '';
                                            $careerId = '';
                                            $careerName = '';
                                            
                                        }
                                        
                                        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
                                        $userEmail = $repository->createQueryBuilder('u')
                                            ->where('u.email = :email 
                                                    AND u.numberidentity <> :numberidentity')
                                            ->setParameter('email', $email)
                                            ->setParameter('numberidentity', $numberIdentity)
                                            ->getQuery()
                                            ->getResult();
                                        if ($userEmail) {
                                            $message .= '<p>El correo lo está usando otro usuario.</p>';
                                            $valid = 0;
                                        }
                                        
                                        // Verify code in pollapllication
                                        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                                        $peaoplepas = $repository->createQueryBuilder('pa')
                                            ->select('COUNT(pa.id) evaluations')
                                            ->where('wiev.code = :evaluated
                                                    OR wier.code = :evaluator')
                                            ->andWhere('pa.pollscheduling <> :pollscheduling')
                                            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wiev', 'WITH', 'pa.evaluated_id = wiev.id')
                                            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wier', 'WITH', 'pa.evaluator_id = wier.id')
                                            ->setParameter('evaluated', $code)
                                            ->setParameter('evaluator', $code)
                                            ->setParameter('pollscheduling', $pollschedulingid)
                                            ->groupBy('pa.id')
                                            ->getQuery()
                                            ->getResult();
                                        
                                        if (!empty($peaoplepas)) {
                                            $message .= '<p>Participa en otra encuesta, se actualizará la información laboral, verificar los datos.</p>';
                                        }
                                        
                                        // Finish do while
                                        
                                        if ($code == '') {
                                            break;
                                        }
                                        
                                        // Validate Register
                                        
                                        if ($code == '' && $subgroup == 'evaluateds') {
                                            $message .= '<p>Falta el código del evaluado</p>';
                                            $valid = 0;
                                        }
                                        
                                        if ($code == '' && $subgroup == 'evaluators') {
                                            $message .= '<p>Falta el código del evaluador</p>';
                                            $valid = 0;
                                        }
                                        
                                        if ($names == '') {
                                            $message .= '<p>Falta el nombre de la persona.</p>';
                                            $valid = 0;
                                        }
                                        
                                        if (!$typeIdentity) {
                                            $message .= '<p>El tipo de documento de identidad no es correcto, 
                                                            debe colocar d, e, p. d = dni, e = carné de extranjería, p = pasaporte.</p>';
                                            $valid = 0;
                                        }
                                        
                                        if ($headquarterCode == '') {
                                            $message .= '<p>Falta el código de la sede.</p>';
                                            $validInformation = 0;
                                        }
                                        else {
                                            $h = $em->getRepository('FishmanEntityBundle:Headquarter')->findOneBy(
                                                array( 'company' => $companyId , 'code' => $headquarterCode , 'status' => 1));
                                            if ($h) {
                                                $headquarterId = $h->getId();
                                                $headquarterName = $h->getName();
                                            }
                                            else {
                                                $message .= '<p>El código de sede no existe o está inactivo.</p>';
                                                $valid = 0;
                                            }
                                        }
                                        
                                        // Validate Information
                                        
                                        if ($type == 'working') {
                                            
                                            if ($emailInformation == '') {
                                                $message .= '<p>Falta el correo laboral.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($ouCode == '') {
                                                $message .= '<p>Falta el código de la unidad organizativa.</p>';
                                                $valid = 0;
                                            }
                                            else {
                                                $ou = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $ouCode , 'status' => 1));
                                                if ($ou) {
                                                    $ouId = $ou->getId();
                                                    $ouName = $ou->getName();
                                                    $ouUnitType = $ou->getUnitType();
                                                }
                                                else {
                                                    $message .= '<p>El código de unidad organizativa no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                            if ($chargeCode == '') {
                                                $message .= '<p>Falta el código del cargo.</p>';
                                                $valid = 0;
                                            }
                                            else {
                                                $charge = $em->getRepository('FishmanEntityBundle:Companycharge')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $chargeCode , 'status' => 1));
                                                if ($charge) {
                                                    $chargeId = $charge->getId();
                                                    $chargeName = $charge->getName();
                                                }
                                                else {
                                                    $message .= '<p>El código de cargo no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                        }
                                        elseif ($type == 'school') {
                                            
                                            if ($gradeCode == '') {
                                                $message .= '<p>Falta el código de grado.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($studyYear == '') {
                                                $message .= '<p>Falta el año de estudio.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($courseCode == '') {
                                                $message .= '<p>Falta el código del curso</p>';
                                            }
                                            else {
                                                $co = $em->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                                    array('code' => $courseCode, 'company' => $pollscheduling->getCompanyId(), 'status' => 1));
                                                if ($co) {
                                                    $courseName = $co->getName();
                                                    $courseId = $co->getId();
                                                }
                                                else {
                                                    $message .= '<p>El código de curso no existe o esta inactivo.</p>';
                                                }
                                            }
                                        
                                        }
                                        elseif ($type == 'university') {
                                            
                                            if ($emailInformation == '') {
                                                $message .= '<p>Falta el correo universitario.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($facultyCode == '') {
                                                $message .= '<p>Falta el código de la facultad.</p>';
                                                $valid = 0;
                                            }
                                            else {
                                                $faculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $facultyCode , 'status' => 1));
                                                if ($faculty) {
                                                    $facultyId = $faculty->getId();
                                                    $facultyName = $faculty->getName();
                                                }
                                                else {
                                                    $message .= '<p>El código de facultad no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                            if ($careerCode == '') {
                                                $message .= '<p>Falta el código de la carrera.</p>';
                                                $valid = 0;
                                            }
                                            else {
                                                $career = $em->getRepository('FishmanEntityBundle:Companycareer')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $careerCode , 'status' => 1));
                                                if ($career) {
                                                    $careerId = $career->getId();
                                                    $careerName = $career->getName();
                                                }
                                                else {
                                                    $message .= '<p>El código de carrera no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                            if ($academicYear == '') {
                                                $message .= '<p>Falta el ciclo académico.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($courseCode == '') {
                                                $message .= '<p>Falta el código del curso</p>';
                                            }
                                            else {
                                                $co = $em->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                                    array('code' => $courseCode, 'company' => $pollscheduling->getCompanyId(), 'status' => 1));
                                                if ($co) {
                                                    $courseName = $co->getName();
                                                    $courseId = $co->getId();
                                                }
                                                else {
                                                    $message .= '<p>El código de curso no existe o esta inactivo.</p>';
                                                }
                                            }
                                            
                                        }
                                        
                                        if ($subgroup == 'evaluators') {
                                            if (!in_array($evaluatorRol, array('boss', 'pair', 'collaborator', 'self'))) {
                                                $message .= '<p>El rol de evaluador no es válido, no se asignará el rol.</p>';
                                            }
                                        }
                                        
                                        if($valid && $message == ''){
                                            $message = 'OK';
                                        }
                                        
                                        // Array evaluations
                                        
                                        $people['groups'][$group][$subgroup][$i] = array(
                                            'rol' => $rol,
                                            'code' => $code,
                                            'surname' => $surname,
                                            'lastname' => $lastname,
                                            'names' => $names,
                                            'sex' => $sex,
                                            'identity' => $typeIdentity,
                                            'numberidentity' => $numberIdentity,
                                            'birthday' => $birthday,
                                            'email' => $email,
                                            'company_id' => $companyId,
                                            'company_code' => $companyCode,
                                            'company_name' => $companyName,
                                            'company_type' => $companyType,
                                            'headquarter_id' => $headquarterId,
                                            'headquarter_name' => $headquarterName,
                                            'email_information' => $emailInformation,
                                            'message' => $message,
                                            'valid' => $valid
                                        );
                                        
                                        if ($type == 'working') {
                                            
                                            $people['groups'][$group][$subgroup][$i]['wi_type'] = 'workinginformation';
                                            $people['groups'][$group][$subgroup][$i]['ou_id'] = $ouId;
                                            $people['groups'][$group][$subgroup][$i]['ou_name'] = $ouName;
                                            $people['groups'][$group][$subgroup][$i]['ou_unit_type'] = $ouUnitType;
                                            $people['groups'][$group][$subgroup][$i]['charge_id'] = $chargeId;
                                            $people['groups'][$group][$subgroup][$i]['charge_name'] = $chargeName;
                                            $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                            $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                            $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                            $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                            //jtt: agregado 20150812
                                            $people['groups'][$group][$subgroup][$i]['cargo_global'] = $cargoGlobal;
                                            $people['groups'][$group][$subgroup][$i]['nivel_responsabilidad'] = $nivelResponsabilidad;
                                            
                                        }
                                        elseif ($type == 'school') {
                                            
                                            $people['groups'][$group][$subgroup][$i]['wi_type'] = 'schoolinformation';
                                            $people['groups'][$group][$subgroup][$i]['grade_code'] = $gradeCode;
                                            $people['groups'][$group][$subgroup][$i]['study_year'] = $studyYear;
                                            $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                            $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                            $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                            $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                            
                                        }
                                        elseif ($type == 'university') {
                                            
                                            $people['groups'][$group][$subgroup][$i]['wi_type'] = 'universityinformation';
                                            $people['groups'][$group][$subgroup][$i]['faculty_id'] = $facultyId;
                                            $people['groups'][$group][$subgroup][$i]['faculty_name'] = $facultyName;
                                            $people['groups'][$group][$subgroup][$i]['career_id'] = $careerId;
                                            $people['groups'][$group][$subgroup][$i]['career_name'] = $careerName;
                                            $people['groups'][$group][$subgroup][$i]['academic_year'] = $academicYear;
                                            $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                            $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                            $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                            $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                            
                                        }
                                        
                                        $people['groups'][$group][$subgroup][$i]['evaluator_rol'] = $evaluatorRol;
                                        
                                        $i++;
                                    }
                                    
                                    $row++;
                                    
                                } while (true);
                                
                                $i = 0;
                                $evaluations = array();
                                
                                if ($fileEvaluationExcel == 'evaluateds') {
                                    foreach ($people['groups'] as $group) {
                                        foreach ($group['evaluators'] as $evaluator) {
                                            foreach ($group['evaluateds'] as $evaluated) {
                                                $evaluations[$i]['evaluator'] = $evaluator;
                                                $evaluations[$i]['evaluated'] = $evaluated;
                                                $i++;
                                            }
                                        }
                                    }
                                }
                                else {
                                    foreach ($people['groups'] as $group) {
                                        foreach ($group['evaluateds'] as $evaluated) {
                                            $evaluations[$i]['evaluator'] = $evaluated;
                                            $evaluations[$i]['evaluated'] = $evaluated;
                                            $i++;
                                        }
                                    }
                                }
                                
                            }
                            else {
                                $session->getFlashBag()->add('error', 'El tipo de empresa de la plantilla es erroneo debe ser "school", "university".');
                            }
                            
                        }
                        else {
                            $session->getFlashBag()->add('error', 'El tipo de evaluación de la plantilla es erroneo debe ser "evaluateds".');
                        }
                        
                    }
                    else {
                        $session->getFlashBag()->add('error', 'El archivo a importar debe ser de formato excel');
                    }
                    
                    if (count($evaluations) > 0 and is_array($evaluations)) {
                        $temporal = new Temporalimportdata();
                        $temporal->setData($evaluations);
                        $temporal->setEntity('evaluations');
                        $temporal->setDatetime(new \DateTime());
                        $em->persist($temporal);
                        $em->flush();
    
                        // Secondary form
                        $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                        $form2 = $this->createFormBuilder($defaultData2)
                            ->add('temporalimportdata_id',
                                  'integer', array (
                                           'required' => true 
                                 ))
                            ->getForm();
                        $form2 = $form2->createView();
                    }
                    else {
                        $session->getFlashBag()->add('error', 'No hay evaluaciones para importar.');
                    }
                    
                    //Delete temporal file
                    if ($data['file']->document == 'xlsx') {
                        unlink($inputFileName);
                    }
                }
            } //End is method post
              
            return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:import.html.twig', array(
                'form' => $form->createView(),
                'form2' => $form2,
                'pollscheduling' => $pollscheduling,
                'evaluations' => $evaluations,
                'pollapplicationimport' => FALSE,
                'companytype' => $companyType
            ));
            
        }
        else {
          $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

          return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $type_evaluation = $_GET['type_evaluation'];
        $type = $_GET['type'];
        $filename = '';
        
        switch ($type_evaluation) {
            case 'regular':
                switch ($type) {
                    case 'working':
                        $filename = 'importar-evaluaciones-laboral.xlsx';
                        break;
                    case 'school':
                        $filename = 'importar-evaluaciones-colegio.xlsx';
                        break;
                    case 'university':
                        $filename = 'importar-evaluaciones-universidad.xlsx';
                        break;
                }
                break;
            case 'self':
                switch ($type) {
                    case 'working':
                        $filename = 'importar-evaluaciones-laboral-autoevaluacion.xlsx';
                        break;
                    case 'school':
                        $filename = 'importar-evaluaciones-colegio-autoevaluacion.xlsx';
                        break;
                    case 'university':
                        $filename = 'importar-evaluaciones-universidad-autoevaluacion.xlsx';
                        break;
                }
                break;
        }
        
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"'
        );
        
        if ($type_evaluation == 'regular') {
            return new Response(file_get_contents( __DIR__.'/../../../../templates/PollBundle/Pollscheduling/' . $filename), 200, $headers);
        }
        else {
            return new Response(file_get_contents( __DIR__.'/../../../../templates/PollBundle/Pollscheduling/SelfEvaluation/' . $filename), 200, $headers);
        }
    }
    
    /**
     * Lists all Pollapplication not evaluateds entities.
     *
     */
    public function indexEvaluatorNotEvaluatedsAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
            
            // Find Entities
            
            $defaultData = array(
                'evaluator_code' => '', 
                'evaluator_names' => '', 
                'evaluator_surname' => '', 
                'evaluator_lastname' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('evaluator_code', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_names', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_surname', 'text', array(
                    'required' => FALSE
                ))
                ->add('evaluator_lastname', 'text', array(
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'evaluator_code' => '', 
                'evaluator_names' => '', 
                'evaluator_surname' => '', 
                'evaluator_lastname' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Delete registers
            
            $deletes_form = $this->createFormBuilder()
                ->add('registers', 'choice', array(
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => true
                ))
                ->getForm();
                
            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $queryBuilder = $repository->createQueryBuilder('pa')
                ->select('pa.id, wi.code evaluator_code, ue.names evaluator_names, ue.surname evaluator_surname, 
                          ue.lastname evaluator_lastname, pa.changed, u.names, u.surname, u.lastname')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                ->innerJoin('wi.user', 'ue')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pa.modified_by = u.id')
                ->where('pa.pollscheduling = :pollscheduling 
                        AND pa.deleted = :deleted')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->setParameter('deleted', FALSE)
                ->orderBy('pa.id', 'ASC');
        
            // Add arguments
            
            if ($data['evaluator_code'] != '') {
                $queryBuilder
                    ->andWhere('wi.code LIKE :evaluator_code')
                    ->setParameter('evaluator_code', '%' . $data['evaluator_code'] . '%');
            }
            if ($data['evaluator_names'] != '') {
                $queryBuilder
                    ->andWhere('ue.names LIKE :evaluator_names')
                    ->setParameter('evaluator_names', '%' . $data['evaluator_names'] . '%');
            }
            if ($data['evaluator_surname'] != '') {
                $queryBuilder
                    ->andWhere('ue.surname LIKE :evaluator_surname')
                    ->setParameter('evaluator_surname', '%' . $data['evaluator_surname'] . '%');
            }
            if ($data['evaluator_lastname'] != '') {
                $queryBuilder
                    ->andWhere('ue.lastname LIKE :evaluator_lastname')
                    ->setParameter('evaluator_lastname', '%' . $data['evaluator_lastname'] . '%');
            }
            
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'pollapplication');
            $paginator->setMaxPagerItems(5, 'pollapplication');
            $entities = $paginator->paginate($query, 'pollapplication')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('pollapplication');
            $endPageItem = $paginator->getEndPageItem('pollapplication');
            $totalItems = $paginator->getTotalItems('pollapplication');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }

            // Return Template
            
            return $this->render('FishmanPollBundle:Pollscheduling/EvaluatorNotEvaluateds:index.html.twig', array(
                'entities' => $entities,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'deletes_form' => $deletes_form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');
            
            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Pollapplication not evaluateds entity.
     *
     */
    public function newEvaluatorNotEvaluatedsAction(Request $request, $pollschedulingid)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.pollscheduling_anonymous anonymous, ps.type, ps.version, 
                      ps.entity_type, ps.entity_id, c.id company_id, c.name company_name, c.company_type, 
                      ps.initdate, ps.enddate, ps.deleted')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
            ->where('ps.id = :pollscheduling 
                    AND ps.status = 1')
            ->setParameter(':pollscheduling', $pollschedulingid)
            ->getQuery()
            ->getResult();
        
        $pollscheduling = current($result);
        
        if (!$pollscheduling || $pollscheduling['anonymous'] || $pollscheduling['entity_type'] != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling['deleted']) {
            
            // Recovering data
            
            $course_options = Companycourse::getListCompanycourseOptions($this->getDoctrine(), $pollscheduling['company_id']);
            
            // Find Workinginformations
            
            $defaultData = array( 
                'iden' => '',
                'evaluator_name' => '',
                'attrib' => '', 
                'course' => '', 
                'section' => ''
            );
            $form = $this->createFormBuilder($defaultData)
                ->add('iden', 'hidden', array(
                    'required'=>true
                ))
                ->add('evaluator_name', 'text', array(
                    'required'=>false
                ))
                ->add('attrib', 'choice', array(
                    'choices' => array(
                        '0' => 'No',
                        '1' => 'Si',
                    ), 
                    'empty_value' => 'Choose an option',
                    'required'=>false
                ))
                ->add('course', 'choice', array(
                    'choices' => $course_options, 
                    'empty_value' => 'Choose an option',
                    'required'=>false
                ))
                ->add('section', 'text', array(
                    'required'=>false
                ))
                ->getForm();
    
            $data = array(
                'iden' => '',
                'evaluator_name' => '', 
                'attrib' => '', 
                'course' => '', 
                'section' => ''
            );
            if($request->getMethod() == 'POST') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Recover Workinginformations
            
            $workinginformations = Pollschedulingpeople::getPollschedulingPeoplesList($this->getDoctrine(), $pollscheduling['company_id'], $pollscheduling['id']);
            
            for ($i=0; $i < count($workinginformations); $i++) {
                $workinginformations[$i]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                                    $workinginformations[$i]['surname'] . ' ' . 
                                                    $workinginformations[$i]['surname'] . ' (' . 
                                                    Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
            }

            $workinginformations = 'var workinginformations = ' . json_encode($workinginformations);
            
            // Return new.html.twig            
            return $this->render('FishmanPollBundle:Pollscheduling/EvaluatorNotEvaluateds:new.html.twig', array(
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'jsonworkinginformations' => $workinginformations,
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Creates a new Pollapplication not evaluateds entity.
     *
     */
    public function asignedsEvaluatorNotEvaluatedsAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        $data = $request->request->get('form');
        
        if ($data['iden'] != '') {
            
            $entityAT = 'pollschedulingpeople';
            $entityId = NULL;
            
            $workinginformations = '';
            $attArray = array();
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            // Recovering data
            
            $evaluator[] = $data['iden'];
            $evaluated = NULL;
            $attArray[0]= $data['course'];
            $attArray[1]= $data['section'];
            
            // Add Pollapplications
            
            if (in_array($company->getCompanyType(), array('school', 'university'))) {
                $numberRegister = Pollapplication::addPollapplications(
                    $this->getDoctrine(), 
                    $evaluator, 
                    $evaluated, 
                    $entityAT, 
                    $pollscheduling, 
                    $userBy, 
                    $entityId,
                    $attArray,
                    NULL
                );
            }
            else {
                $numberRegister = Pollapplication::addPollapplications(
                    $this->getDoctrine(), 
                    $evaluator, 
                    $evaluated, 
                    $entityAT, 
                    $pollscheduling, 
                    $userBy, 
                    $entityId,
                    NULL,
                    NULL
                );
            }
         
            $session->getFlashBag()->add('status', 'El evaluador fue asignado a la encuesta satisfactoriamente.');
        }
        else {
            $session->getFlashBag()->add('error', 'No escogio ningún evaluador para asignar a la encuesta.');
        }
        
        if($request->request->get('saveandclose', true)){
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
        else {
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds_new', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
    }

    /**
     * Drop an Pollapplication not evaluateds entity.
     *
     */
    public function dropEvaluatorNotEvaluatedsAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollapplication
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
        $query = $repository->createQueryBuilder('pa')
            ->select('pa.id, wi.id evaluator_id, ue.names evaluator_names, ue.surname evaluator_surname, 
                      ue.lastname evaluator_lastname, pa.deleted, ps.id pollscheduling_id, ps.title pollscheduling_title, 
                      ps.pollscheduling_anonymous anonymous, ps.version pollscheduling_version, 
                      ps.deleted pollscheduling_deleted, ps.entity_type, ps.initdate, ps.enddate')
            ->innerJoin('pa.pollscheduling', 'ps')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
            ->innerJoin('wi.user', 'ue')
            ->where('pa.id = :pollapplication 
                    AND pa.evaluated_id IS NULL')
            ->setParameter('pollapplication', $id)
            ->getQuery()
            ->getResult();
            
        $entity = current($query);
        if (!$entity || $entity['anonymous'] || $entity['entity_type'] != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$entity['pollscheduling_deleted']) {
            
            if ($entity['deleted']) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la encuesta de la programación de taller.');
                return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                    'pollschedulingid' => $entity['pollscheduling_id']
                )));
            }
          
            $deleteForm = $this->createDeleteForm($id);
    
            return $this->render('FishmanPollBundle:Pollscheduling/EvaluatorNotEvaluateds:drop.html.twig', array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Drop multiple an Pollapplication not evaluateds entities.
     *
     */
    public function dropmultipleEvaluatorNotEvaluatedsAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
          
            // Recover register of form
            
            $data = $request->request->get('form_deletes');
            
            if (isset($data['registers'])) {
            
                $registers = '';
                $registers_count = count($data['registers']);
                $i = 1;
                
                if (!empty($data['registers'])) {
                    
                    foreach($data['registers'] as $r) {
                        $registers .= $r;
                        if ($i < $registers_count) {
                            $registers .= ',';
                        }
                        $i++;
                    }
        
                    // Recover pollapplication of pollscheduling
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                    $entities = $repository->createQueryBuilder('pa')
                        ->select('pa.id, wi.id evaluator_id, ue.names evaluator_names, ue.surname evaluator_surname, 
                                  ue.lastname evaluator_lastname')
                        ->innerJoin('pa.pollscheduling', 'ps')
                        ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                        ->innerJoin('wi.user', 'ue')
                        ->where('pa.id IN(' . $registers . ')
                              AND pa.evaluated_id IS NULL')
                        ->getQuery()
                        ->getResult();
                        
                    if (!$entities) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la Programación de Encuesta.');
                        return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                            'pollschedulingid' => $pollschedulingid
                        )));
                    }
                
                    $deleteMultipleForm = $this->createDeleteMultipleForm($registers);
                
                    return $this->render('FishmanPollBundle:Pollscheduling/EvaluatorNotEvaluateds:dropmultiple.html.twig', array(
                        'entities' => $entities,
                        'pollscheduling' => $pollscheduling,
                        'delete_form' => $deleteMultipleForm->createView()
                    ));
                }
            }
            
            $session->getFlashBag()->add('error', 'No se ha seleccionado ningún registro para eliminar.');
            
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                'pollschedulingid' => $pollschedulingid
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
        
    }

    /**
     * Drop all an Pollapplication not evaluateds entities.
     *
     */
    public function dropallEvaluatorNotEvaluatedsAction($pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
            
            $deleteAllForm = $this->createDeleteAllForm();
            
            return $this->render('FishmanPollBundle:Pollscheduling/EvaluatorNotEvaluateds:dropall.html.twig', array(
                'pollscheduling' => $pollscheduling,
                'delete_all_form' => $deleteAllForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
        
    }

    /**
     * Delete a Pollapplication not evaluateds entity.
     *
     */
    public function deleteEvaluatorNotEvaluatedsAction(Request $request, $pollschedulingid, $id)
    { 
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
          
            $entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar al eveluado asignado a la Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
            
            $entity->setDeleted(TRUE);

            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Deletes multiple a Pollapplication not evaluateds entities.
     *
     */
    public function deletemultipleEvaluatorNotEvaluatedsAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $form = $request->request->get('form');
        
        if ($form['registers'] != '') {
            
            // Recover Pollapplications of form

            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $entities = $repository->createQueryBuilder('pa')
                ->where('pa.id IN(' . $form['registers'] . ')')
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
          
            foreach($entities as $entity) {
            
                Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
              
                $entity->setDeleted(TRUE);
    
                $em->persist($entity);
                $em->flush();
            }
            
            $session->getFlashBag()->add('status', 'Los registros fueron eliminados satisfactoriamente.');

        }

        return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Deletes all Pollapplication not evaluateds entities.
     *
     */
    public function deleteallEvaluatorNotEvaluatedsAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $form = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();   
        
        if ($form['delete_all']) {
            
            // Recover Pollapplications of form
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $entities = $repository->createQueryBuilder('pa')
                ->where('pa.pollscheduling = :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No existen evaluaciones en esta Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            foreach($entities as $entity) {
                
                Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
                
                $entity->setDeleted(TRUE);
                
                $em->persist($entity);
                $em->flush();
            }
            $session->getFlashBag()->add('status', 'Los registros fueron eliminados satisfactoriamente.');
            
        }
        
        return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Import an Pollapplication entity.
     *
     */
    public function importEvaluatorNotEvaluatedsAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Mensaje de que no hay registros
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
                            
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        if (!$company) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
            return $this->redirect($this->generateUrl('pollschedulingevaluator_notevaluateds', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
        
        $companyId = $company->getId();
        $companyCode = $company->getCode();
        $companyName = $company->getName();
        $companyType = $company->getCompanyType();
        
        if (!$pollscheduling->getDeleted()) {
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplicationimport');
            $query = $repository->createQueryBuilder('pai')
                ->select('COUNT(pai.id) cpais')
                ->where('pai.pollscheduling_id = :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->groupBy('pai.pollscheduling_id')
                ->getQuery()
                ->getResult();
            
            $paimports = current($query);
            if ($paimports['cpais'] > 0) {
                $session->getFlashBag()->add('status', 'Existen evaluaciones pendientes a importar.');
                return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:import.html.twig', array(
                    'pollscheduling' => $pollscheduling,
                    'pollapplicationimport' => TRUE,
                    'companytype' => $companyType
                ));
            }
            
            $defaultData = array();
            $form = $this->createForm(new PollevaluatorimportType(), $defaultData);

            $form2 = false;
            $data = '';
            $people = false;
            $evaluations = false;
            $message = '';
            $messageError = '';
            $existingwiemail = '';
            $numberRegister = 0;

            if ($request->isMethod('POST')) {
                
                //The user is confirmed to persist data as 
                $importFormData = $request->request->get('form', false);
    
                if ($importFormData) {
                    
                    $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                         ->find($importFormData['temporalimportdata_id']);
                    if(!$temporalentity){
                      return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:import.html.twig', array(
                          'form' => $form->createView(),
                          'form2' => $form2->createView(),
                          'pollscheduling' => $pollscheduling
                      ));
                    }
                    
                    $evaluations = $temporalentity->getData();
                    $userBy = $this->get('security.context')->getToken()->getUser();
                    
                    // suspend auto-commit
                    $em->getConnection()->beginTransaction();
                    
                    // Try and make the transaction
                    try {
                        
                        if (in_array($companyType, array('working', 'school', 'university'))) {
                            foreach ($evaluations as $evaluation) {
                                if ($evaluation['evaluator']['valid']) {
                                    
                                    $evaluator = Pollpersonimport::addRegister($this->getDoctrine(), $evaluation['evaluator']);
                                    
                                    $pai = new Pollapplicationimport();
                                    $pai->setPollschedulingId($pollschedulingid);
                                    $pai->setEvaluatedId(NULL);
                                    $pai->setEvaluatorId($evaluator->getId());
                                    $pai->setEvaluatedCode(NULL);
                                    $pai->setEvaluatorCode($evaluation['evaluator']['code']);
                                    $pai->setCourse($evaluation['evaluator']['course_id']);
                                    $pai->setSection($evaluation['evaluator']['section']);
                                    $pai->setStatus(FALSE);
                                    
                                    $em->persist($pai);
                                    $em->flush();
                                    
                                    $numberRegister++;
                                }
                            }
                        }
                        
                        //Delete temporal data
                        $em->remove($temporalentity);
                        $em->flush();
                        
                        // Delete temporal files
                        Pollscheduling::deleteTemporalFiles('../tmp/');
                        
                        // Try and commit the transactioncurrentversioncurrentversion
                        $em->getConnection()->commit();
                    } catch (Exception $e) {
                        // Rollback the failed transaction attempt
                        $em->getConnection()->rollback();
                        throw $e;
                    }
                    
                    if ($numberRegister > 0) {
                        $session->getFlashBag()->add('status', 'Existen '.$numberRegister.' evaluaciones pendientes a importar.');
                        return $this->render('FishmanPollBundle:Pollscheduling/EvaluatorNotEvaluateds:import.html.twig', array(
                            'pollscheduling' => $pollscheduling,
                            'pollapplicationimport' => TRUE,
                            'companytype' => $companyType
                        ));
                    }
                    else {
                        $session->getFlashBag()->add('error', 'No hay evaluaciones para importar.');
                    }
                    
                    $people = false;
    
                } //End have temporarlimportdata input
                else {
                    
                    $form->bind($request);
                    $data = $form->getData();
    
                    // We need to move the file in order to use it
                    $randomName = rand(1, 10000000000);
                    $randomName = $randomName . '.xlsx';
                    $directory = __DIR__.'/../../../../tmp';
                    $data['file']->move($directory, $randomName);
    
                    if (null !== $data['file']) {
                        $fileName = $data['file']->getClientOriginalName();
                        $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                    }
    
                    if ($data['file']->document == 'xlsx') {
                        
                        $inputFileName = $directory . '/' . $randomName;
                        $phpExcel = PHPExcel_IOFactory::load($inputFileName);
                        
                        $worksheet = $phpExcel->getSheet(0);
                        
                        $fileEvaluationExcel = trim($worksheet->getCellByColumnAndRow(1, 1)->getValue());
                        $fileTypeExcel = trim($worksheet->getCellByColumnAndRow(1, 2)->getValue());
                        
                        if ($fileEvaluationExcel == 'evaluators') {
                            
                            if (in_array($fileTypeExcel, array('working', 'school', 'university'))) {
                                
                                // Data defaults
                                
                                $typeIdentity = '';
                                $birthday = '';
                                
                                $i = 0;
                                $row = 5;
                                $group = -1;
                                $subgroup = '';
                                $people = array();
                                
                                do {
                                    
                                    $field = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                                    
                                    if (in_array($field, array('group', 'GROUP'))) {
                                        $group++;
                                    }
                                    elseif (in_array($field, array('evaluators', 'EVALUATORS'))) {
                                        $subgroup = 'evaluators';
                                        $i = 0;
                                    }
                                    else {
                                        
                                        // Data default
                                        
                                        $code = '';
                                        $names = '';
                                        $birthday = '';
                                        $email = '';
                                        $emailInformation = '';
                                        
                                        $headquarterId = '';
                                        $headquarterName = '';
                                        $status = 0;
                                        
                                        $ouId = '';
                                        $ouName = '';
                                        $ouUnitType = '';
                                        $chargeId = '';
                                        $chargeName = '';
                                        
                                        $facultyCode = '';
                                        $gradeCode = '';
                                        $studyYear = '';
                                        $academicYear = '';
                                        
                                        $courseId = '';
                                        $courseCode = '';
                                        $courseName = '';
                                        $section = '';
                                        
                                        // Data validation
                                        
                                        $message = '';
                                        $valid = 1;
                                        
                                        // Get Data
                                        
                                        $type = strtolower(trim($worksheet->getCellByColumnAndRow(0, $row)->getValue()));
                                        $rol = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                                        
                                        // Data User
                                        
                                        $surname = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                                        $lastname = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                                        $names = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                                        $typeIdentityRow = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
                                        if (in_array($typeIdentityRow, array('d', 'e', 'p'))) {
                                            $typeIdentity = User::getDocumentTypeByEquivalent($typeIdentityRow);
                                        }
                                        $numberIdentity = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
                                        $birthdayRow = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                                        if ($birthdayRow) {
                                            $birthday = \DateTime::createFromFormat('d/m/Y', $birthdayRow);
                                        }
                                        $sex = trim($worksheet->getCellByColumnAndRow(8, $row)->getValue());
                                        $email = trim($worksheet->getCellByColumnAndRow(9, $row)->getValue());
                                        
                                        // Generate Code
                                        $code = $numberIdentity;
                                        
                                        // Data Working
                                        
                                        $headquarterCode = trim($worksheet->getCellByColumnAndRow(10, $row)->getValue());
                                        $emailInformation = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                        
                                        if ($type == 'working') {
                                            
                                            $ouCode = trim($worksheet->getCellByColumnAndRow(11, $row)->getValue());
                                            $chargeCode = trim($worksheet->getCellByColumnAndRow(12, $row)->getValue());
                                            if ($fileTypeExcel == 'school') {
                                                $newWI = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                            }
                                            elseif ($fileTypeExcel == 'university') {
                                                $newWI = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                            }
                                            
                                        }
                                        elseif ($type == 'school') {
                                            
                                            $gradeCode = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                            $studyYear = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                            $section = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                            $courseCode = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                            $newWI = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                            
                                        }
                                        elseif ($type == 'university') {
                                            
                                            // Data University
                                            
                                            $emailInformation = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                            $facultyCode = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                            $careerCode = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                            $academicYear = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                            $section = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                            $courseCode = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                            $newWI = trim($worksheet->getCellByColumnAndRow(19, $row)->getValue());
                                            
                                            $facultyId = '';
                                            $facultyName = '';
                                            $careerId = '';
                                            $careerName = '';
                                            
                                        }
                                        
                                        $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
                                        $userEmail = $repository->createQueryBuilder('u')
                                            ->where('u.email = :email 
                                                    AND u.numberidentity <> :numberidentity')
                                            ->setParameter('email', $email)
                                            ->setParameter('numberidentity', $numberIdentity)
                                            ->getQuery()
                                            ->getResult();
                                        if ($userEmail) {
                                            $message .= '<p>El correo lo está usando otro usuario.</p>';
                                            $valid = 0;
                                        }
                                        
                                        // Verify code in pollapllication
                                        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                                        $peaoplepas = $repository->createQueryBuilder('pa')
                                            ->select('COUNT(pa.id) evaluations')
                                            ->where('wiev.code = :evaluated
                                                    OR wier.code = :evaluator')
                                            ->andWhere('pa.pollscheduling <> :pollscheduling')
                                            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wiev', 'WITH', 'pa.evaluated_id = wiev.id')
                                            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wier', 'WITH', 'pa.evaluator_id = wier.id')
                                            ->setParameter('evaluated', $code)
                                            ->setParameter('evaluator', $code)
                                            ->setParameter('pollscheduling', $pollschedulingid)
                                            ->groupBy('pa.id')
                                            ->getQuery()
                                            ->getResult();
                                        
                                        if (!empty($peaoplepas)) {
                                            $message .= '<p>Participa en otra encuesta, se actualizará la información laboral, verificar los datos.</p>';
                                        }
                                        
                                        // Finish do while
                                        
                                        if ($code == '') {
                                            break;
                                        }
                                        
                                        // Validate Register
                                        
                                        if ($code == '' && $subgroup == 'evaluateds') {
                                            $message .= '<p>Falta el código del evaluado</p>';
                                            $valid = 0;
                                        }
                                        
                                        if ($code == '' && $subgroup == 'evaluators') {
                                            $message .= '<p>Falta el código del evaluador</p>';
                                            $valid = 0;
                                        }
                                        
                                        if ($names == '') {
                                            $message .= '<p>Falta el nombre de la persona.</p>';
                                            $valid = 0;
                                        }
                                        
                                        if (!$typeIdentity) {
                                            $message .= '<p>El tipo de documento de identidad no es correcto, 
                                                            debe colocar d, e, p. d = dni, e = carné de extranjería, p = pasaporte.</p>';
                                            $valid = 0;
                                        }
                                        
                                        if ($headquarterCode == '') {
                                            $message .= '<p>Falta el código de la sede.</p>';
                                            $validInformation = 0;
                                        }
                                        else {
                                            $h = $em->getRepository('FishmanEntityBundle:Headquarter')->findOneBy(
                                                array( 'company' => $companyId , 'code' => $headquarterCode , 'status' => 1));
                                            if ($h) {
                                                $headquarterId = $h->getId();
                                                $headquarterName = $h->getName();
                                            }
                                            else {
                                                $message .= '<p>El código de sede no existe o está inactivo.</p>';
                                                $valid = 0;
                                            }
                                        }
                                        
                                        // Validate Information
                                        
                                        if ($type == 'working') {
                                            
                                            if ($emailInformation == '') {
                                                $message .= '<p>Falta el correo laboral.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($ouCode == '') {
                                                $message .= '<p>Falta el código de la unidad organizativa.</p>';
                                                $valid = 0;
                                            }
                                            else {
                                                $ou = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $ouCode , 'status' => 1));
                                                if ($ou) {
                                                    $ouId = $ou->getId();
                                                    $ouName = $ou->getName();
                                                    $ouUnitType = $ou->getUnitType();
                                                }
                                                else {
                                                    $message .= '<p>El código de unidad organizativa no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                            if ($chargeCode == '') {
                                                $message .= '<p>Falta el código del cargo.</p>';
                                                $valid = 0;
                                            }
                                            else {
                                                $charge = $em->getRepository('FishmanEntityBundle:Companycharge')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $chargeCode , 'status' => 1));
                                                if ($charge) {
                                                    $chargeId = $charge->getId();
                                                    $chargeName = $charge->getName();
                                                }
                                                else {
                                                    $message .= '<p>El código de cargo no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                        }
                                        elseif ($type == 'school') {
                                            
                                            if ($gradeCode == '') {
                                                $message .= '<p>Falta el código de grado.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($studyYear == '') {
                                                $message .= '<p>Falta el año de estudio.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($courseCode == '') {
                                                $message .= '<p>Falta el código del curso</p>';
                                            }
                                            else {
                                                $co = $em->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                                    array('code' => $courseCode, 'company' => $pollscheduling->getCompanyId(), 'status' => 1));
                                                if ($co) {
                                                    $courseName = $co->getName();
                                                    $courseId = $co->getId();
                                                }
                                                else {
                                                    $message .= '<p>El código de curso no existe o esta inactivo.</p>';
                                                }
                                            }
                                        
                                        }
                                        elseif ($type == 'university') {
                                            
                                            if ($emailInformation == '') {
                                                $message .= '<p>Falta el correo universitario.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($facultyCode == '') {
                                                $message .= '<p>Falta el código de la facultad.</p>';
                                                $valid = 0;
                                            }
                                            else {
                                                $faculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $facultyCode , 'status' => 1));
                                                if ($faculty) {
                                                    $facultyId = $faculty->getId();
                                                    $facultyName = $faculty->getName();
                                                }
                                                else {
                                                    $message .= '<p>El código de facultad no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                            if ($careerCode == '') {
                                                $message .= '<p>Falta el código de la carrera.</p>';
                                                $valid = 0;
                                            }
                                            else {
                                                $career = $em->getRepository('FishmanEntityBundle:Companycareer')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $careerCode , 'status' => 1));
                                                if ($career) {
                                                    $careerId = $career->getId();
                                                    $careerName = $career->getName();
                                                }
                                                else {
                                                    $message .= '<p>El código de carrera no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                            if ($academicYear == '') {
                                                $message .= '<p>Falta el ciclo académico.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($courseCode == '') {
                                                $message .= '<p>Falta el código del curso</p>';
                                            }
                                            else {
                                                $co = $em->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                                    array('code' => $courseCode, 'company' => $pollscheduling->getCompanyId(), 'status' => 1));
                                                if ($co) {
                                                    $courseName = $co->getName();
                                                    $courseId = $co->getId();
                                                }
                                                else {
                                                    $message .= '<p>El código de curso no existe o esta inactivo.</p>';
                                                }
                                            }
                                            
                                        }
                                        
                                        if($valid && $message == ''){
                                            $message = 'OK';
                                        }
                                        
                                        // Array evaluations
                                        
                                        $people['groups'][$group][$subgroup][$i] = array(
                                            'rol' => $rol,
                                            'code' => $code,
                                            'surname' => $surname,
                                            'lastname' => $lastname,
                                            'names' => $names,
                                            'sex' => $sex,
                                            'identity' => $typeIdentity,
                                            'numberidentity' => $numberIdentity,
                                            'birthday' => $birthday,
                                            'email' => $email,
                                            'company_id' => $companyId,
                                            'company_code' => $companyCode,
                                            'company_name' => $companyName,
                                            'company_type' => $companyType,
                                            'headquarter_id' => $headquarterId,
                                            'headquarter_name' => $headquarterName,
                                            'email_information' => $emailInformation,
                                            'message' => $message,
                                            'valid' => $valid
                                        );
                                        
                                        if ($type == 'working') {
                                            
                                            $people['groups'][$group][$subgroup][$i]['wi_type'] = 'workinginformation';
                                            $people['groups'][$group][$subgroup][$i]['ou_id'] = $ouId;
                                            $people['groups'][$group][$subgroup][$i]['ou_name'] = $ouName;
                                            $people['groups'][$group][$subgroup][$i]['ou_unit_type'] = $ouUnitType;
                                            $people['groups'][$group][$subgroup][$i]['charge_id'] = $chargeId;
                                            $people['groups'][$group][$subgroup][$i]['charge_name'] = $chargeName;
                                            $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                            $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                            $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                            $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                            
                                        }
                                        elseif ($type == 'school') {
                                            
                                            $people['groups'][$group][$subgroup][$i]['wi_type'] = 'schoolinformation';
                                            $people['groups'][$group][$subgroup][$i]['grade_code'] = $gradeCode;
                                            $people['groups'][$group][$subgroup][$i]['study_year'] = $studyYear;
                                            $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                            $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                            $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                            $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                            
                                        }
                                        elseif ($type == 'university') {
                                            
                                            $people['groups'][$group][$subgroup][$i]['wi_type'] = 'universityinformation';
                                            $people['groups'][$group][$subgroup][$i]['faculty_id'] = $facultyId;
                                            $people['groups'][$group][$subgroup][$i]['faculty_name'] = $facultyName;
                                            $people['groups'][$group][$subgroup][$i]['career_id'] = $careerId;
                                            $people['groups'][$group][$subgroup][$i]['career_name'] = $careerName;
                                            $people['groups'][$group][$subgroup][$i]['academic_year'] = $academicYear;
                                            $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                            $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                            $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                            $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                            
                                        }
                                        
                                        $i++;
                                    }
                                    
                                    $row++;
                                    
                                } while (true);
                                
                                $i = 0;
                                $evaluations = array();
                                foreach ($people['groups'] as $group) {
                                    foreach ($group['evaluators'] as $evaluator) {
                                        $evaluations[$i]['evaluator'] = $evaluator;
                                        $evaluations[$i]['evaluated'] = $evaluator;
                                        $i++;
                                    }
                                }
                                
                            }
                            else {
                                $session->getFlashBag()->add('error', 'El tipo de empresa de la plantilla es erroneo debe ser "school", "university".');
                            }
                            
                        }
                        else {
                            $session->getFlashBag()->add('error', 'El tipo de evaluación de la plantilla es erroneo debe ser "evaluators".');
                        }
                        
                    }
                    else {
                        $session->getFlashBag()->add('error', 'El archivo a importar debe ser de formato excel');
                    }
                    
                    if(count($evaluations) > 0 and is_array($evaluations)){
                        $temporal = new Temporalimportdata();
                        $temporal->setData($evaluations);
                        $temporal->setEntity('evaluations');
                        $temporal->setDatetime(new \DateTime());
                        $em->persist($temporal);
                        $em->flush();
    
                        // Secondary form
                        $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                        $form2 = $this->createFormBuilder($defaultData2)
                            ->add('temporalimportdata_id',
                                  'integer', array (
                                           'required' => true 
                                 ))
                            ->getForm();
                        $form2 = $form2->createView();
                    }
                    else {
                        $session->getFlashBag()->add('error', 'No hay evaluaciones para importar.');
                    }
                    
                    //Delete temporal file
                    if ($data['file']->document == 'xlsx') {
                        unlink($inputFileName);
                    }
                }
            } //End is method post
              
            return $this->render('FishmanPollBundle:Pollscheduling/EvaluatorNotEvaluateds:import.html.twig', array(
                'form' => $form->createView(),
                'form2' => $form2,
                'pollscheduling' => $pollscheduling,
                'evaluations' => $evaluations,
                'pollapplicationimport' => FALSE,
                'companytype' => $companyType
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');
  
            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Download document not evaluateds.
     * The user can access actual User documents
     */
    public function downloadtemplateNotEvaluatedsAction()
    {
        $type = $_GET['type'];
        $filename = '';
        
        switch ($type) {
            case 'working':
                $filename = 'importar-evaluaciones-laboral-sin-evaluados.xlsx';
                break;
            case 'school':
                $filename = 'importar-evaluaciones-colegio-sin-evaluados.xlsx';
                break;
            case 'university':
                $filename = 'importar-evaluaciones-universidad-sin-evaluados.xlsx';
                break;
        }
        
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"'
        );
        return new Response(file_get_contents( __DIR__.'/../../../../templates/PollBundle/Pollscheduling/NotEvaluateds/' . $filename), 200, $headers);
    }

    /**
     * Lists all Pollschedulingnotification entities.
     *s
     */
    public function indexNotificationAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
            
            // Recovering data
            
            $predecessor_options = Notificationscheduling::getListNotificationschedulingOptions($this->getDoctrine(), 'pollscheduling', $pollschedulingid);
            for ($i = 1; $i<= 20; $i++) {
                $sequence_options[$i] = $i;
            }
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'predecessor' => '', 
                'sequence' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('predecessor', 'choice', array(
                    'choices' => $predecessor_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('sequence', 'choice', array(
                    'choices' => $sequence_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'predecessor' => '', 
                'sequence' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Query
                
            $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationscheduling');
            $queryBuilder = $repository->createQueryBuilder('ns')
                ->select('ns.id, ns.name, ns.notification_type type, ns.notification_type_name type_name, nsp.name predecessor, 
                          ns.sequence, ns.changed, ns.status, ns.own, u.names, u.surname, u.lastname')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'ns.modified_by = u.id')
                ->leftJoin('FishmanNotificationBundle:Notificationscheduling', 'nsp', 'WITH', 'ns.predecessor = nsp.id')
                ->where('ns.entity_id = :pollscheduling 
                        AND ns.entity_type = :entity_type')
                ->andWhere('ns.id LIKE :id 
                        OR ns.name LIKE :name')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->setParameter('entity_type', $pollscheduling->getEntityType())
                ->setParameter('id', '%' . $data['word'] . '%')
                ->setParameter('name', '%' . $data['word'] . '%')
                ->orderBy('ns.id', 'ASC');
        
            // Add arguments
            
            if ($data['predecessor'] != '') {
                $queryBuilder
                    ->andWhere('ns.predecessor = :predecessor')
                    ->setParameter('predecessor', $data['predecessor']);
            }
            if ($data['sequence'] != '') {
                $queryBuilder
                    ->andWhere('ns.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }
            if ($data['status'] != '' || $data['status'] === 0) {
                $queryBuilder
                    ->andWhere('ns.status = :status')
                    ->setParameter('status', $data['status']);
            }
            
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'pollschedulingnotification');
            $paginator->setMaxPagerItems(5, 'pollschedulingnotification');
            $entities = $paginator->paginate($query, 'pollschedulingnotification')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('pollschedulingnotification');
            $endPageItem = $paginator->getEndPageItem('pollschedulingnotification');
            $totalItems = $paginator->getTotalItems('pollschedulingnotification');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
    
            // Return Template
            
            return $this->render('FishmanPollBundle:Pollscheduling/Notification:index.html.twig', array(
                'entities' => $entities,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Finds and displays a Pollschedulingnotification entity.
     *
     */
    public function showNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Notificationscheduling
        $entity = Notificationscheduling::getNotificationscheduling($this->getDoctrine(), $id);
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entity['entity_id']);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
          
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
            
            return $this->render('FishmanPollBundle:Pollscheduling/Notification:show.html.twig', array(
                'entity' => $entity,
                'pollscheduling' => $pollscheduling, 
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Pollschedulingnotification entity.
     *
     */
    public function newNotificationAction($pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
          
            $entity = new Notificationscheduling();
            
            $entity->setFinish('never');
            $entity->setSelectedTrigger('to_send');
            $entity->setStatus(1);
            $form = $this->createForm(new NotificationschedulingType($entity, 'pollscheduling', $pollschedulingid, true, $this->getDoctrine()), $entity);
    
            return $this->render('FishmanPollBundle:Pollscheduling/Notification:new.html.twig', array(
                'entity' => $entity,
                'pollscheduling' => $pollscheduling,
                'form'   => $form->createView(),
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Creates a new Pollschedulingnotification entity.
     *
     */
    public function createNotificationAction(Request $request, $pollschedulingid)
    {
        $entity  = new Notificationscheduling();
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $asigneds = $request->request->get('asigned_options');
        $entity->setNotificationType('poll');
        
        $form = $this->createForm(new NotificationschedulingType($entity, 'pollscheduling', $pollschedulingid, true, $this->getDoctrine()), $entity);
        $form->bind($request);
        
        if ($form->isValid()) {
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setNotificationTypeId($pollschedulingid);
            $entity->setNotificationTypeName($pollscheduling->getTitle());
            if ($entity->getExecutionType() == 'manual' || $entity->getExecutionType() == 'trigger') {
                $entity->setPredecessor(-1);
            }
            if ($entity->getExecutionType() == 'trigger') {
                $entity->setNotificationTypeStatus(TRUE);
            }
            else {
                $entity->setSelectedTrigger('');
            }
            if ($entity->getExecutionType() != 'automatic') {
                $entity->setSince('');
                $entity->setReplay(FALSE);
            }
            if ($entity->getReplay() == 0) {
                $entity->setRepeatPeriod('');
                $entity->setRepeatRange('');
                $entity->setFinish('');
            }
            if ($entity->getFinish() != 'after') {
                $entity->setRepetitionNumber('');
            }
            if ($entity->getPredecessor() == '') {
                $entity->setPredecessor(-1);
            }
            $entity->setAsigned($asigneds['data2']);
            $entity->setEntityType('pollscheduling');
            $entity->setEntityId($pollscheduling->getEntityId());
            $entity->setEntityName($pollscheduling->getTitle());
            $entity->setEntityStatus($pollscheduling->getStatus());
            $entity->setOwn(TRUE);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $em->persist($entity);
            $em->flush();
            
            // Create Notificationexecution
            switch ($entity->getNotificationType()) {
                
                case 'poll':
                    
                    // Recover Pollapplications
                    $pollapplications = $em->getRepository('FishmanPollBundle:Pollapplication')->findBy(array(
                        'pollscheduling' => $pollschedulingid,
                        'deleted' => 0
                    ));
                    
                    if (!empty($pollapplications)) {
                        foreach ($pollapplications as $pa_entity) {
                            Pollapplication::generateNotificationexecutions($this->getDoctrine(), $pa_entity, $entity->getId());
                        }
                    }
                    
                    break;
                    
                case 'message':
                    
                    // Recover Pollschedulingpeoples
                    $pollschedulingpeoples = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findBy(array(
                        'pollscheduling' => $pollschedulingid
                    ));
                    
                    if (!empty($pollschedulingpeoples)) {
                        foreach ($pollschedulingpeoples as $psp_entity) {
                            Pollschedulingpeople::generateNotificationexecutions($this->getDoctrine(), $psp_entity, $entity->getId());
                        }
                    }
                    
                    break;
            }
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('pollschedulingnotification_show', array(
                'id' => $entity->getId()
            )));
        }
        
        return $this->render('FishmanPollBundle:Pollscheduling/Notification:new.html.twig', array(
            'entity' => $entity,
            'pollscheduling' => $pollscheduling,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pollschedulingnotification entity.
     *
     */
    public function editNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Notificationscheduling
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollschedulingnotification', array(
                'pollschedulingid' => $entity->getEntityId()
            )));
        }
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entity->getEntityId());
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
         
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
            
            $entity->setFinish('never');
            $entity->setSelectedTrigger('to_send');
            $editForm = $this->createForm(new NotificationschedulingType($entity, 'pollscheduling', $entity->getEntityId(), $entity->getOwn(), $this->getDoctrine()), $entity);
            
            return $this->render('FishmanPollBundle:Pollscheduling/Notification:edit.html.twig', array(
                'entity' => $entity,
                'pollscheduling' => $pollscheduling, 
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'edit_form' => $editForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Edits an existing Pollschedulingnotification entity.
     *
     */
    public function updateNotificationAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Notificationscheduling
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entity->getEntityId());
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $asigneds = $request->request->get('asigned_options');
        
        $dataEntity = array(
            'notification_type' => $entity->getNotificationType(),
            'notification_type_status' => $entity->getNotificationTypeStatus(),
            'one_per_evaluator' => $entity->getOnePerEvaluator(),
            'execution_type' => $entity->getExecutionType(),
            'predecessor' => $entity->getPredecessor(),
            'since' => $entity->getSince(),
            'selected_trigger' => $entity->getSelectedTrigger(),
            'replay' => $entity->getReplay(),
            'repeat_period' => $entity->getRepeatPeriod(),
            'repeat_range' => $entity->getRepeatRange(),
            'finish' => $entity->getFinish(),
            'repetition_number' => $entity->getRepetitionNumber()
        );
        
        $editForm = $this->createForm(new NotificationschedulingType($entity, 'pollscheduling', $entity->getEntityId(), $entity->getOwn(), $this->getDoctrine()), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setNotificationType($dataEntity['notification_type']);
            $entity->setNotificationTypeStatus($dataEntity['notification_type_status']);
            $entity->setOnePerEvaluator($dataEntity['one_per_evaluator']);
            $entity->setExecutionType($dataEntity['execution_type']);
            $entity->setPredecessor($dataEntity['predecessor']);
            $entity->setSince($dataEntity['since']);
            $entity->setSelectedTrigger($dataEntity['selected_trigger']);
            $entity->setReplay($dataEntity['replay']);
            $entity->setRepeatPeriod($dataEntity['repeat_period']);
            $entity->setRepeatRange($dataEntity['repeat_range']);
            $entity->setFinish($dataEntity['finish']);
            $entity->setRepetitionNumber($dataEntity['repetition_number']);
            
            $entity->setAsigned($asigneds['data2']);
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();

            // set flash messages
            $session = $this->getRequest()->getSession();                                                
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('pollschedulingnotification_show', array(
                'id' => $id
            )));
        }

        $entity->setNotificationType('p');
        $editForm = $this->createForm(new NotificationType($entity, 'pollscheduling', $entity->getEntityId(), $entity->getOwn(), $this->getDoctrine()), $entity);
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollscheduling/Notification:edit.html.twig', array(
            'entity' => $entity,
            'pollscheduling' => $pollscheduling,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Pollschedulingnotification entity.
     *
     */
    public function dropNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Notificationscheduling
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entity->getEntityId());
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
        
            if ($entity->getOwn()) {
    
                $deleteForm = $this->createDeleteForm($id);
        
                return $this->render('FishmanPollBundle:Pollscheduling/Notification:drop.html.twig', array(
                    'entity' => $entity,
                    'pollscheduling' => $pollscheduling, 
                    'delete_form' => $deleteForm->createView()
                ));
            }
                                                               
            $session->getFlashBag()->add('error', 'El registro no puede ser eliminado.');
    
            return $this->redirect($this->generateUrl('pollschedulingnotification', array(
              'pollschedulingid' => $pollscheduling->getId(),
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Deletes a Pollschedulingnotification entity.
     *
     */
    public function deleteNotificationAction(Request $request, $id, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        if ($form->isValid()) {
            
            $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la Programación de Encuesta.');
                return $this->redirect($this->generateUrl('pollschedulingnotification', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $nds = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationsend')
               ->createQueryBuilder('nd')
               ->innerJoin('nd.notificationexecution', 'ne')
               ->where('ne.notificationscheduling = :notificationscheduling')
               ->setParameter('notificationscheduling', $id)
               ->getQuery()
               ->getResult();
            
            if (!empty($nds)) {
                $session->getFlashBag()->add('error', 'El registro no puede ser eliminado, algunas notificaciones han sido enviadas.');
                return $this->redirect($this->generateUrl('pollschedulingnotification', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $nmss = $em->getRepository('FishmanNotificationBundle:Notificationmanualsend')->findBy(array(
                'notificationscheduling' => $id
            ));
            
            if (!empty($nmss)) {
                foreach ($nmss as $nms) {
                    $nmds = $em->getRepository('FishmanNotificationBundle:Notificationmanualdetails')->findBy(array(
                        'notificationmanualsend' => $nms->getId()
                    ));
                    
                    if (!empty($nmds)) {
                        foreach ($nmds as $nmd) {
                            $em->remove($nmd);
                            $em->flush();
                        }
                    }
                    
                    $em->remove($nms);
                    $em->flush();
                }
            }
            
            // Update children predecessor
            $em->createQueryBuilder()
                ->update('FishmanNotificationBundle:Notificationscheduling ns')
                ->set('ns.predecessor', ':predecessor')
                ->where('ns.predecessor = :notificationscheduling')
                ->setParameter('predecessor', '-1')
                ->setParameter('notificationscheduling', $id)
                ->getQuery()
                ->execute();
                
            Notificationexecution::removeNotificationexecutions($this->getDoctrine(), $entity->getId(), $entity->getEntityType(), $entity->getEntityId());
                
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('pollschedulingnotification', array(
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Regenarte Pollscheduling entity.
     *
     */
    public function regenerateAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible workinginformation
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
            
            // suspend auto-commit
            $em->getConnection()->beginTransaction();
            
            // Try and make the transaction
            try {
                Pollscheduling::recreateSerializedToPollscheduling($this->getDoctrine(), $id);
                
                // Try and commit the transactioncurrentversioncurrentversion
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                throw $e;
            }
            
            $session->getFlashBag()->add('status', 'La encuesta ha sido regenerada exitosamente.');
            
            if (isset($_GET['workshopschedulingid'])) {
                return $this->redirect($this->generateUrl('workshopschedulingpoll_show', array(
                    'id' => $id
                )));
            }
            else {
                return $this->redirect($this->generateUrl('pollscheduling_show', array(
                    'id' => $id
                )));
            }
        }
        else {
            $session->getFlashBag()->add('error', 'Usted no cuenta con los permisos necesarios para hacer esta operación.');
        }
    }
}
