<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('FishmanPollBundle:Default:index.html.twig', array('name' => $name));
    }
}
