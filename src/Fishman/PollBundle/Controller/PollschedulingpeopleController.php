<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\PollBundle\Entity\Pollscheduling;
use Fishman\PollBundle\Entity\Pollschedulingpeople;
use Fishman\PollBundle\Entity\Pollapplication;
use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\EntityBundle\Entity\Companycharge;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Fishman\ImportExportBundle\Entity\Temporalimportdata;
use Fishman\PollBundle\Form\PollschedulingpeopleType;
use Fishman\PollBundle\Form\PollschedulingpeopleimportType;

use Symfony\Component\HttpFoundation\Response;

use PHPExcel_IOFactory;

/**
 * Pollschedulingpeople controller.
 *
 */
class PollschedulingpeopleController extends Controller
{
    /**
     * Lists all Pollschedulingpeople entities.
     *
     */
    public function indexAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
          
            // Recovering data
            
            $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $pollscheduling->getCompanyId());
            $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $pollscheduling->getCompanyId());
            $profile_options = Pollschedulingpeople::getListProfileOptions();
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'organizationalunit' => '', 
                'charge' => '', 
                'profile' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('organizationalunit', 'choice', array(
                    'choices' => $organizationalunit_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('charge', 'choice', array(
                    'choices' => $charge_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('profile', 'choice', array(
                    'choices' => $profile_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'organizationalunit' => '', 
                'charge' => '', 
                'profile' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Delete registers
            
            $deletes_form = $this->createFormBuilder()
                ->add('registers', 'choice', array(
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => true
                ))
                ->getForm();
                
            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
            $queryBuilder = $repository->createQueryBuilder('psp')
                ->select('psp.id, wi.code, wi.type, pspu.names pspu_names, pspu.surname pspu_surname, pspu.lastname pspu_lastname, 
                          cou.name organizationalunit, cch.name charge, psp.profile, psp.status, psp.changed, 
                          u.names, u.surname, u.lastname')
                ->innerJoin('psp.workinginformation', 'wi')
                ->leftJoin('wi.companyorganizationalunit', 'cou')
                ->leftJoin('wi.companycharge', 'cch')
                ->innerJoin('wi.user', 'pspu')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'psp.modified_by = u.id')
                ->where('psp.pollscheduling = :pollscheduling
                        AND wi.status = 1')
                ->andWhere('wi.code LIKE :code 
                        OR pspu.names LIKE :names 
                        OR pspu.surname LIKE :surname 
                        OR pspu.lastname LIKE :lastname')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->setParameter('code', '%' . $data['word'] . '%')
                ->setParameter('names', '%' . $data['word'] . '%')
                ->setParameter('surname', '%' . $data['word'] . '%')
                ->setParameter('lastname', '%' . $data['word'] . '%')
                ->orderBy('pspu.surname', 'ASC', 'pspu.lastname', 'ASC', 'pspu.names', 'ASC');
            
            // Add arguments
            
            if ($data['organizationalunit'] != '') {
                $queryBuilder
                    ->andWhere('cou.id = :organizationalunit')
                    ->setParameter('organizationalunit', $data['organizationalunit']);
            }
            if ($data['charge'] != '') {
                $queryBuilder
                    ->andWhere('cch.id = :charge')
                    ->setParameter('charge', $data['charge']);
            }
            if ($data['profile'] != '') {
                $queryBuilder
                    ->andWhere('psp.profile = :profile')
                    ->setParameter('profile', $data['profile']);
            }
            if ($data['status'] != '' || $data['status'] === 0) {
                $queryBuilder
                    ->andWhere('psp.status = :status')
                    ->setParameter('status', $data['status']);
            }
        
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'pollschedulingpeople');
            $paginator->setMaxPagerItems(5, 'pollschedulingpeople');
            $entities = $paginator->paginate($query, 'pollschedulingpeople')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('pollschedulingpeople');
            $endPageItem = $paginator->getEndPageItem('pollschedulingpeople');
            $totalItems = $paginator->getTotalItems('pollschedulingpeople');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
            
            return $this->render('FishmanPollBundle:Pollschedulingpeople:index.html.twig', array(
                'entities' => $entities,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'deletes_form' => $deletes_form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
        }
        else {
            $session->getFlashBag()->add('error', 'La Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Finds and displays a Pollschedulingpeople entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollschedulingpeople
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
        $result = $repository->createQueryBuilder('psp')
            ->select('psp.id, wi.code, wi.type, u.names, u.surname, u.lastname, u.sex, u.email, psp.profile, 
                      psp.status, psp.created, psp.changed, psp.created_by, psp.modified_by, 
                      ps.id pollscheduling_id, ps.title pollscheduling_title, ps.pollscheduling_anonymous anonymous, 
                      ps.level pollscheduling_level, ps.version pollscheduling_version, 
                      ps.deleted pollscheduling_deleted, ps.entity_type')
            ->innerJoin('psp.pollscheduling', 'ps')
            ->innerJoin('psp.workinginformation', 'wi')
            ->innerJoin('wi.user', 'u')
            ->where('psp.id = :pollschedulingpeople')
            ->setParameter('pollschedulingpeople', $id)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        
        if (!$entity || $entity['anonymous'] || $entity['entity_type'] != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la persona asignada a la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        if (!$entity['pollscheduling_deleted']) {
          
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
    
            return $this->render('FishmanPollBundle:Pollschedulingpeople:show.html.twig', array(
                'entity' => $entity,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Pollschedulingpeople entity.
     *
     */
    public function newAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
            
            // Recover Companyorganizationalunits
            $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $pollscheduling->getCompanyId());
            
            // Recover Companycharges
            $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $pollscheduling->getCompanyId());
            
            // Find Workinginformations
            
            $defaultData = array(
                'code' => '', 
                'surname' => '', 
                'lastname' => '', 
                'names' => '', 
                'initdate' => NULL, 
                'organizationalunit' => '', 
                'charge' => ''
            );
            $form = $this->createFormBuilder($defaultData)
                ->add('code', 'text', array(
                    'required'=>false
                ))
                ->add('surname', 'text', array(
                    'required'=>false
                ))
                ->add('lastname', 'text', array(
                    'required'=>false
                ))
                ->add('names', 'text', array(
                    'required'=>false
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => false
                ))
                ->add('organizationalunit', 'choice', array(
                    'choices' => $organizationalunit_options, 
                    'empty_value' => 'Choose an option',
                    'required' => false
                ))
                ->add('charge', 'choice', array(
                    'choices' => $charge_options, 
                    'empty_value' => 'Choose an option',
                    'required' => false
                ))
                ->getForm();
    
            $data = array(
                'code' => '', 
                'surname' => '', 
                'lastname' => '', 
                'names' => '', 
                'initdate' => NULL, 
                'organizationalunit' => '', 
                'charge' => ''
            );
            if($request->getMethod() == 'POST') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Recover Workinginformation ids
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
            $wa_result = $repository->createQueryBuilder('psp')
                ->select('wi.id')
                ->innerJoin('psp.workinginformation', 'wi')
                ->where('psp.pollscheduling = :pollscheduling
                        AND wi.company = :company')
                ->setParameter('pollscheduling', $pollscheduling->getId())
                ->setParameter('company', $pollscheduling->getCompanyId())
                ->orderBy('wi.id', 'ASC')
                ->getQuery()
                ->getResult();
            
            $wi_ids = '';
            
            // We set ids chain Workshopapplications
            
            if (!empty($wa_result)) {
                
                $wa_count = count($wa_result);
                $i = 1;
                $wi_ids = '';
                
                foreach ($wa_result as $wa) {
                    $wi_ids .= $wa['id'];
                    if ($i < $wa_count) {
                        $wi_ids .= ',';
                    }
                    $i++;
                }
            }

            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $queryBuilder = $repository->createQueryBuilder('wi')
                ->select('wi.id, wi.code, wi.type, u.names, u.surname, u.lastname, co.name companyorganizationalunit, 
                          cc.name companycharge, wi.datein, wi.status')
                ->leftJoin('wi.companyorganizationalunit', 'co')
                ->leftJoin('wi.companycharge', 'cc')
                ->innerJoin('wi.user', 'u')
                ->where('wi.company = :company  
                        AND wi.status = 1')
                ->setParameter('company', $pollscheduling->getCompanyId())
                ->orderBy('wi.id', 'ASC');
                        
            // Check if there are people assigned
            
            if ($wi_ids != '') {
                $queryBuilder
                    ->andWhere('wi.id NOT IN(' . $wi_ids . ')');
            }
            
            // Add the arguments that are being asked
            
            if ($data['code'] != '') {
                $queryBuilder
                    ->andWhere('wi.code LIKE :code')
                    ->setParameter('code', '%' . $data['code'] . '%');
            }
            if (!empty($data['surname'])) {
                $queryBuilder
                    ->andWhere('u.surname LIKE :surname')
                    ->setParameter('surname', '%' . $data['surname'] . '%');
            }
            if (!empty($data['lastname'])) {
                $queryBuilder
                    ->andWhere('u.lastname LIKE :lastname')
                    ->setParameter('lastname', '%' . $data['lastname'] . '%');
            }
            if (!empty($data['names'])) {
                $queryBuilder
                    ->andWhere('u.names LIKE :names')
                    ->setParameter('names', '%' . $data['names'] . '%');
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('wi.datein = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            elseif ($data['organizationalunit'] != '') {
                $queryBuilder
                    ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                    ->setParameter('organizationalunit', $data['organizationalunit']);
            }
            elseif ($data['charge'] != '') {
                $queryBuilder
                    ->andWhere('wi.companycharge = :charge')
                    ->setParameter('charge', $data['charge']);
            }
            
            $query = $queryBuilder->getQuery();
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workinginformation');
            $paginator->setMaxPagerItems(5, 'workinginformation');
            $entities = $paginator->paginate($query, 'workinginformation')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workinginformation');
            $endPageItem = $paginator->getEndPageItem('workinginformation');
            $totalItems = $paginator->getTotalItems('workinginformation');
        
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
            
            // Asigned registers
            
            $asigneds_form = $this->createFormBuilder()
                ->add('registers', 'choice', array(
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => true
                ))
                ->add('profile', 'choice', array(
                    'choices' => array(
                        'boss' => 'Jefe',
                        'collaborator' => 'Colaborador',
                        'integrant' => 'Participante',
                        'company' => 'Empresa'/*,
                        'responsible' => 'Responsable'*/
                    ),
                    'empty_value' => 'Choose an option',
                    'required' => false
                ))
                ->getForm();
            
            // Return new.html.twig
            
            return $this->render('FishmanPollBundle:Pollschedulingpeople:new.html.twig', array(
                'entities' => $entities,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'asigneds_form' => $asigneds_form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator
            ));
        }
        else {
            $session->getFlashBag()->add('error', 'La Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Asigneds Pollschedulingpeoples.
     *
     */
    public function asignedsAction(Request $request, $pollschedulingid)
    {   
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $data = $request->request->get('form');

        if (!empty($data['registers'])) {
            
            $em = $this->getDoctrine()->getManager();
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
                $session->getFlashBag()->add('error', 'No se puede encontrar la programación de encuesta.');
                return $this->redirect($this->generateUrl('pollscheduling'));
            }
                
            if (!empty($data['profile'])) {
                
                $numberRegister = 0;
                
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                // We travel registers
                foreach ($data['registers'] as $r) {
                  
                    $workinginformation = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($r);
                    
                    Pollschedulingpeople::addRegister($this->getDoctrine(), $data['profile'], $workinginformation, $pollscheduling, $userBy);
                    
                    $numberRegister++;
                }
                
                $session->getFlashBag()->add('status', 'Se asignaron ' . $numberRegister . ' Persona(s) a la encuesta satisfactoriamente');
            }
        }
        else {
            $session->getFlashBag()->add('error', 'No escogio ninguna persona para asignar a la encuesta.');
        }
        
        if($request->request->get('saveandclose', true)){
            return $this->redirect($this->generateUrl('pollschedulingpeople', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
        else {
            return $this->redirect($this->generateUrl('pollschedulingpeople_new', array(
                'pollschedulingid' => $pollschedulingid
            )));
        }
    }

    /**
     * Displays a form to edit an existing Pollschedulingpeople entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollschedulingpeople
        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->find($id);
        if (!$entity || $entity->getPollscheduling()->getPollschedulingAnonymous() || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la persona asignada a la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$entity->getPollscheduling()->getDeleted()) {

            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
            
            $editForm = $this->createForm(new PollschedulingpeopleType(), $entity);
    
            return $this->render('FishmanPollBundle:Pollschedulingpeople:edit.html.twig', array(
                'entity' => $entity,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'edit_form' => $editForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Edits an existing Pollschedulingpeople entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollschedulingpeople
        
        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->find($id);
        if (!$entity || $entity->getPollscheduling()->getPollschedulingAnonymous() || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la persona asignada a la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        $editForm = $this->createForm(new PollschedulingpeopleType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());

            $em->persist($entity);
            
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('pollschedulingpeople_show', array(
                'id' => $id
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanPollBundle:Pollschedulingpeople:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Drop multiple an Pollschedulingpeoples entities.
     *
     */
    public function dropmultipleAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if (!$pollscheduling->getDeleted()) {
          
            // Recover register of form
            
            $data = $request->request->get('form_deletes');
            
            if (isset($data['registers'])) {
            
                $registers = '';
                $registers_count = count($data['registers']);
                $i = 1;
                
                if (!empty($data['registers'])) {
                    
                    foreach($data['registers'] as $r) {
                        $registers .= $r;
                        if ($i < $registers_count) {
                            $registers .= ',';
                        }
                        $i++;
                    }
        
                    // Recover Pollschedulingpeople of Pollscheduling
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
                    $entities = $repository->createQueryBuilder('psp')
                        ->where('psp.id IN(' . $registers . ')')
                        ->getQuery()
                        ->getResult();
                    if (!$entities) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar la personas asignadas a la programación de encuesta.');
                        return $this->redirect($this->generateUrl('pollschedulingpeople', array(
                            'pollschedulingid' => $pollschedulingid
                        )));
                    }
                
                    $deleteMultipleForm = $this->createDeleteMultipleForm($registers);
                
                    return $this->render('FishmanPollBundle:Pollschedulingpeople:dropmultiple.html.twig', array(
                        'entities' => $entities,
                        'pollscheduling' => $pollscheduling,
                        'delete_form' => $deleteMultipleForm->createView()
                    ));
                }
            }
            
            $session->getFlashBag()->add('error', 'No se ha seleccionado ningún registro para eliminar.');
            
            return $this->redirect($this->generateUrl('pollschedulingpeople', array(
                'pollschedulingid' => $pollschedulingid
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Encuesta ha sido eliminada.');

            return $this->redirect($this->generateUrl('pollscheduling', array()));
        }
    }

    /**
     * Deletes multiple a Workshopapplications entities.
     *
     */
    public function deletemultipleAction(Request $request, $pollschedulingid)
    {   
        $form = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        if ($form['registers'] != '') {
              
            $em = $this->getDoctrine()->getManager();
               
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
                $session->getFlashBag()->add('error', 'No se puede encontrar la programación de encuesta.');
                return $this->redirect($this->generateUrl('pollscheduling'));
            }            
            
            // Recover Pollscheudlingpeoples of form
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
            $entities = $repository->createQueryBuilder('psp')
                ->where('psp.id IN(' . $form['registers'] . ')')
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la personas asignadas a la programación de encuesta.');
                return $this->redirect($this->generateUrl('pollschedulingpeople', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $message = '';
            $messageError = '';
            $messagePeople = '';
            $deleted = 0;
            $noDeleted = 0;
            
            foreach($entities as $entity) {
              
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                $pa = $repository->createQueryBuilder('pa')
                    ->where('pa.evaluated_id = :evaluated 
                            OR pa.evaluator_id = :evaluator')
                    ->andWhere('pa.deleted = 0')
                    ->andWhere('pa.pollscheduling = :pollscheduling')
                    ->setParameter('evaluated', $entity->getWorkinginformation()->getId())
                    ->setParameter('evaluator', $entity->getWorkinginformation()->getId())
                    ->setParameter('pollscheduling', $pollschedulingid)
                    ->getQuery()
                    ->getResult();
                    
                if ($pa) {
                    $upa = $entity->getWorkinginformation()->getUser();
                    if ($noDeleted > 0) {
                        $messagePeople .= '</br>';
                    }
                    $messagePeople .= $upa->getNames() . ' ' . $upa->getSurname() . ' ' . $upa->getLastname();
                    $noDeleted++;
                }
                else {
                    Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getWorkinginformation()->getId());
                    
                    $em->remove($entity);
                    $em->flush();
                    
                    $deleted++;
                }
                
            }
            
            if ($messagePeople != '') {
                if ($deleted > 0) {
                    $message = 'Se eliminaron ' . $deleted . ' Persona(s).';
                    $messageError = 'No se eliminaron ' . $noDeleted . ' Persona(s) porque participan como ' . 
                                    'Evaluado y/o Evaluador:</br>' . $messagePeople;
                }
                else {
                    $messageError = 'No se eliminaron las personas seleccionadas porque participan como Evaluado y/o Evaluador';
                }
            }
            else {
                $message = 'Los registros fueron eliminados satisfactoriamente.';
            }
            
            if ($message) {
                $session->getFlashBag()->add('status', $message);
            }
            if ($messageError) {
                $session->getFlashBag()->add('error', $messageError);
            }
        }

        return $this->redirect($this->generateUrl('pollschedulingpeople', array(
            'pollschedulingid' => $pollschedulingid
        )));
    }

    private function createDeleteMultipleForm($registers)
    {
        return $this->createFormBuilder(array('registers' => $registers))
            ->add('registers', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Import Pollschedulingpeoples entities.
     *
     */
    public function importAction(Request $request, $pollschedulingid)
    {
        set_time_limit(120);
        ini_set('memory_limit','512M');
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
          
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        $defaultData = array();
        $form = $this->createForm(new PollschedulingpeopleimportType(), $defaultData);

        $form2 = false;
        $data = '';
        $wiData = '';
        $people = false;
        $valido = 1;
        $numberRegister = 0;

        if ($request->isMethod('POST')) {
            
            $importFormData = $request->request->get('form', false);

            if ($importFormData) {
                
                $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                     ->find($importFormData['temporalimportdata_id']);
                if(!$temporalentity){
                  return $this->render('FishmanPollBundle:Pollschedulingpeople:import.html.twig', array(
                         'pollscheduling' => $pollscheduling,
                         'form' => $form->createView(),
                         'form2' => $form2,
                         'people' => $people,
                  ));
                }

                $people = $temporalentity->getData();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    foreach ($people as $peoplearray){
    
                        $userBy = $this->get('security.context')->getToken()->getUser();
    
                        if(!$peoplearray['valid']) continue;
                        
                        $workinginformation = $em->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(
                            array( 'code' => $peoplearray['code'] , 'company' => $pollscheduling->getCompanyId()
                        ));
                        
                        Pollschedulingpeople::addRegister($this->getDoctrine(), $peoplearray['role'], $workinginformation, $pollscheduling, $userBy);
                        
                        $numberRegister = $numberRegister + 1;
    
                    } //end foreach
                    
                    //Delete temporal data
                    $em->remove($temporalentity);
                    $em->flush();
                    
                    // Delete temporal files
                    Pollscheduling::deleteTemporalFiles('../tmp/');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                if ($numberRegister > 0) {
                    $session->getFlashBag()->add('status', 'Se grabó la importación de ' . $numberRegister . ' Persona(s)');
                }
                else {
                    $session->getFlashBag()->add('error', 'No se logró importar ninguna Persona.');
                }

                if($request->request->get('saveandclose', TRUE)){
                    return $this->redirect($this->generateUrl('pollschedulingpeople', array('pollschedulingid' => $pollschedulingid)));
                }

                $people = false;

            } //End have temporarlimportdata input
            else {
                $form->bind($request);
                $data = $form->getData();

                // We need to move the file in order to use it
                $randomName = rand(1, 10000000000);
                $randomName = $randomName . '.xlsx';
                $directory = __DIR__.'/../../../../tmp';
                $data['file']->move($directory, $randomName);

                if (null !== $data['file']) {
                    $fileName = $data['file']->getClientOriginalName();
                    $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                }

                if ($data['file']->document == 'xlsx') { 
                    $inputFileName = $directory . '/' . $randomName;
                    $phpExcel = PHPExcel_IOFactory::load($inputFileName);

                    $worksheet = $phpExcel->getSheet(0);
                    $row = 2;
                    $people = array();
                    do {
                        $code = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                        $type = Workinginformation::getTypeCompanyCode(trim($worksheet->getCellByColumnAndRow(1, $row)->getValue()));
                        $role = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                        $message = '';
                        $people_name = '';
                        $people_surname = '';
                        $people_lastname = '';
                        $organizational_unit = '';
                        $valido = 1;
                        $charge = '';
                        $wi_id = '';
                        $company = '';
                        
                        if ($code == '' && $type == '' && $role == '') break;
                        
                        if($type == ''){
                            $message .= '<p>Falta el tipo de la información</p>';
                            $valido = 0;
                        }
                        elseif (!in_array($type, array('workinginformation', 'schoolinformation', 'universityinformation'))) {
                            $message .= '<p>Tipo de información inválido, debe ser: <strong>working</strong>, <strong>school</strong>, <strong>university</strong></p>';
                            $valido = 0;
                        }
                        else {
                            if ($code == ''){
                                $message .= '<p>Falta el código del usuario</p>';
                                $valido = 0;
                            }
                            else {
                                $wiCode = $em->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(
                                    array( 'code' => $code, 'status' => '1')
                                );
                                
                                if (!empty($wiCode)) {
                                    
                                    $wiData = $em->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(
                                        array( 'code' => $code, 'type' => $type, 'status' => '1', 'company' => $pollscheduling->getCompanyId())
                                    );
                                    
                                    if (!empty($wiData)) {
          	                            $code_wi = $wiData->getCode();
          	                            $company = $wiData->getCompany()->getId();
          	                            $user_data = $wiData->getUser();
          	                            $userStatus = $em->getRepository('FishmanAuthBundle:User')->findOneBy(
          	                            array( 'enabled' => '1' , 'id' => $user_data->getId() ));
                                        
          	                            $wi_id = $wiData->getId();
                                        
                                        if ($code_wi == $code  and $company == $pollscheduling->getcompanyId() and !empty($userStatus)){
                                            $people_name = $wiData->getUser()->getNames();
                                            $people_surname = $wiData->getUser()->getSurname();
                                            $people_lastname = $wiData->getUser()->getLastname();
                                            
                                            if($people_surname == ''){
                                                $message .= '<p>Falta el apellido de la persona</p>';
                                                $valido = 0;
                                            }
                                            
                                            if($people_name == ''){
                                                $message .= '<p>Falta el nombre de la persona</p>';
                                                $valido = 0;
                                            }
                                            
                                            if (is_object($wiData->getCompanyorganizationalunit())) {
                                                $organizational_unit = $wiData->getCompanyorganizationalunit()->getName();
                                            }
                                            else {
                                                $organizational_unit = '';
                                            }
                                            
                                            if (is_object($wiData->getCompanycharge())) {
                                                $charge = $wiData->getCompanycharge()->getName();
                                            }
                                            else {
                                                $charge = '';
                                            }
                                            
                                        } elseif (empty($userStatus)) {
                                            $message .= '<p>La persona esta inactiva, no procedera la importación de este registro</p>';
                                            $valido = 0;
                                        }
                                        else {
                                            $message .= '<p>Persona inexistente o su tipo de información no es válido.</p>';
                                            $valido = 0;
                                        }
                                    } else {
                                        $message .= '<p>El código y tipo de información, no pertenece a la empresa de esta programación.</p>';
                                        $valido = 0;
                                    }
                                } else {
                                    $message .= '<p>No existe persona con el código ingresado</p>';
                                    $valido = 0;
                                }
                            }
                        }
                        
                        if($role == ''){
                            $message .= '<p>Falta el perfil de la persona</p>';
                            $valido = 0;
                        }
                        elseif (($role != 'boss') && ($role !='collaborator') && ($role != 'integrant') && ($role !='company')) {
                            $message .= '<p>El perfil ingresado es incorrectoo</p>';
                            $valido = 0;
                        }

                        if($valido){
                            $message = 'OK';
                        }
                        
                        $people[] = array(
                            'code' => $code,
                            'type' => $type,
                            'role' => $role,
                            'surname' => $people_surname,
                            'lastname' => $people_lastname,
                            'name' => $people_name,
                            'organizationalunit' => $organizational_unit,
                            'charge' => $charge,
                            'company_id' => $company,
                            'wiid' => $wi_id,
                            'valid' => $valido,
                            'status' => 1,
                            'message' => $message
                        );
                        
                        $row++;
                        
                    } while (true);
                }
                else {
                    //Mensaje de que no hay registros
                    $session = $this->getRequest()->getSession();
                    $session->getFlashBag()->add('status', 'el archivo a importar debe ser de formato excel');
                }
                
                if(count($people) > 0 and is_array($people)){
                    
                    $temporal = new Temporalimportdata();
                    $temporal->setData($people);
                    $temporal->setEntity('pollschedulingpeople');
                    $temporal->setDatetime(new \DateTime());
                    $em->persist($temporal);
                    $em->flush();

                    // Secondary form
                    $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                    $form2 = $this->createFormBuilder($defaultData2)
                        ->add('temporalimportdata_id',
                              'integer', array (
                                       'required' => true 
                             ))
                        ->getForm();
                    $form2 = $form2->createView();
                }
                else{
                    $session->getFlashBag()->add('status', 'No hay personas para importar.');
                }
                //Borrar archivo temporal
                if ($data['file']->document == 'xlsx') {
                    unlink($inputFileName);
                }
            }
        } //End is method post

        return $this->render('FishmanPollBundle:Pollschedulingpeople:import.html.twig', array(
            'pollscheduling' => $pollscheduling,
            'form' => $form->createView(),
            'form2' => $form2,
            'people' => $people,
        ));
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="importar-programacion-encuesta-personas.xlsx'.'"'
        );
        return new Response(file_get_contents( __DIR__.'/../../../../templates/PollBundle/Pollschedulingpeople/importar-programacion-encuesta-personas.xlsx'), 200, $headers);
    }
}
