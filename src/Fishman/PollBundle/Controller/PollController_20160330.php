<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\PollBundle\Entity\Poll;
use Fishman\PollBundle\Entity\Pollsection;
use Fishman\PollBundle\Entity\Pollquestion;
use Fishman\NotificationBundle\Entity\Notification;
use Fishman\EntityBundle\Entity\Category;
use Fishman\PollBundle\Form\PollType;
use Fishman\NotificationBundle\Form\NotificationType;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Poll controller.
 *
 */
class PollController extends Controller
{
    /**
     * Lists all Poll entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $category_options = Category::getListCategoryOptions($this->getDoctrine());
        $type_options = Poll::getListTypeOptions();
        $active_options = array(1 => 'Si', 0 => 'No');
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'version' => '', 
            'category' => '', 
            'type' => '',  
            'origin' => '', 
            'active' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('version', 'text', array(
                'required' => FALSE
            ))
            ->add('category', 'choice', array(
                'choices' => $category_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('origin', 'text', array(
                'required' => FALSE
            ))
            ->add('active', 'choice', array(
                'choices' => $active_options, 
                'empty_value' => 'Choose an option',
                /*'expanded' => TRUE,*/ 
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'version' => '', 
            'category' => '', 
            'type' => '',  
            'origin' => '', 
            'active' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
        $queryBuilder = $repository->createQueryBuilder('p')
            ->select('p.id, p.title, p.version, c.name category,p.type, p.changed, p.status, p.original_id, 
                      p.current_version, u.names, u.surname, u.lastname')
            ->innerJoin('p.category', 'c')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'p.modified_by = u.id')
            ->andWhere('p.id LIKE :id 
                    OR p.title LIKE :name')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('p.id', 'ASC');
        
        // Add arguments
        
        if ($data['version'] != '') {
            $queryBuilder
                ->andWhere('p.version LIKE :version')
                ->setParameter('version', '%' . $data['version'] . '%');
        }
        if ($data['category'] != '') {
            $queryBuilder
                ->andWhere('p.category = :category')
                ->setParameter('category', $data['category']);
        }
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('p.type = :type')
                ->setParameter('type', $data['type']);
        }
        if ($data['origin'] != '') {
            $queryBuilder
                ->andWhere('p.original_id LIKE :origin')
                ->setParameter('origin', '%' . $data['origin'] . '%');
        }
        if ($data['active'] != '' || $data['active'] === 0) {
            $queryBuilder
                ->andWhere('p.current_version = :active')
                ->setParameter('active', $data['active']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('p.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'poll');
        $paginator->setMaxPagerItems(5, 'poll');
        $entities = $paginator->paginate($query, 'poll')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('poll');
        $endPageItem = $paginator->getEndPageItem('poll');
        $totalItems = $paginator->getTotalItems('poll');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanPollBundle:Poll:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Poll entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Poll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanPollBundle:Poll:show.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Poll entity.
     *
     */
    public function newAction()
    {
        $entity = new Poll();
        $entity->setStatus(1);
        $form   = $this->createForm(new PollType(), $entity);
        
        return $this->render('FishmanPollBundle:Poll:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Poll entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Poll();
        $form = $this->createForm(new PollType(), $entity);
        $form->bind($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // suspend auto-commit
            $em->getConnection()->beginTransaction();
            
            // Try and make the transaction
            try {
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();

                if ($entity->getDivide() != 'number_questions') {
                  $entity->setNumberQuestion('');
                }
                $entity->setCurrentVersion(1);
                $entity->setModifiedOn(1);
                $entity->setCreatedBy($userBy->getId());
                $entity->setModifiedBy($userBy->getId());
                $entity->setCreated(new \DateTime());
                $entity->setChanged(new \DateTime());
                
                $em->persist($entity);
                $em->flush();
                
                $entity->setOriginalId($entity->getId());
                $em->persist($entity);
                $em->flush();
                
                // set flash messages
                $session = $this->getRequest()->getSession();
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
            
                // Try and commit the transaction
                $em->getConnection()->commit();
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $em->getConnection()->rollback();
                throw $e;
            }

            return $this->redirect($this->generateUrl('poll_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanPollBundle:Poll:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Poll entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Poll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new PollType(), $entity);

        return $this->render('FishmanPollBundle:Poll:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Poll entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Poll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        $version = $entity->getVersion();
        $originalId = $entity->getOriginalId();
        
        $editForm = $this->createForm(new PollType(), $entity);
        $editForm->bind($request);

        if ($version != $entity->getVersion()) {
            
            // Verificamos si la versión ya existe
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
            $emVersion = $repository->findOneBy(array(
                                        'original_id' => $entity->getOriginalId(),
                                        'version' => $entity->getVersion()));
                                        
            if (empty($emVersion)) {
            
                $em->detach($entity);
                $newEntity = clone $entity;
    
                $editForm = $this->createForm(new PollType(), $newEntity);
                $editForm->bind($request);
        
                if ($editForm->isValid()) {
                  
                    // suspend auto-commit
                    $em->getConnection()->beginTransaction();
    
                    // Try and make the transaction
                    try {
                        $query = $em->createQueryBuilder()
                            ->update('FishmanPollBundle:Poll p')
                            ->set('p.current_version', ':currentversion')
                            ->where('p.original_id = :id')
                            ->setParameter('currentversion', 0)
                            ->setParameter('id', $originalId)
                            ->getQuery()
                            ->execute();
    
                        $userBy = $this->get('security.context')->getToken()->getUser();

                        if ($entity->getDivide() != 'number_questions') {
                          $entity->setNumberQuestion('');
                        }
                        $newEntity->setCurrentVersion(1);
                        $newEntity->setModifiedOn(1);
                        $newEntity->setCreatedBy($userBy->getId());
                        $newEntity->setModifiedBy($userBy->getId());
                        $newEntity->setCreated(new \DateTime());
                        $newEntity->setChanged(new \DateTime());
    
                        // TODO: que el usuario establezca la version actual en el formulario. En caso afirmativo,
                        // pone todas las demas como false. Problema, si quita todas las versiones actuales, no podria cambiarlo
                        // $newEntity->setCurrentVersion(true);
                        
                        $em->persist($newEntity);
                        $em->flush();
                        
                        $pollArray['sections'] = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $id);
                        Pollquestion::cloneRegisters($this->getDoctrine(), $newEntity, $pollArray, 1, $userBy);
                        
                        $session->getFlashBag()->add('status', 'Se ha creado una nueva versión.');
                        
                        // Try and commit the transaction
                        $em->getConnection()->commit();
                    } catch (Exception $e) {
                        // Rollback the failed transaction attempt
                        $em->getConnection()->rollback();
                        throw $e;
                    }
    
                    return $this->redirect($this->generateUrl('poll_show', array(
                      'id' => $newEntity->getId()
                    )));
                }
            }
            else {
                
                $session->getFlashBag()->add('error', 'La versión ' . $entity->getVersion() . ' ya existe para esta Encuesta.');
                
                // User
                $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
                $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
                $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
                $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
                
                $editForm = $this->createForm(new PollType(), $entity);
        
                return $this->render('FishmanPollBundle:Poll:edit.html.twig', array(
                    'entity'      => $entity,
                    'createdByName' => $createdByName,
                    'modifiedByName' => $modifiedByName,
                    'edit_form'   => $editForm->createView(),
                ));
            }
        }
        else {
            $editForm = $this->createForm(new PollType(), $entity);
            $editForm->bind($request);
    
            if ($editForm->isValid()) {
                  
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                $entity->setModifiedBy($modifiedBy->getId());
                if ($entity->getDivide() != 'number_questions') {
                  $entity->setNumberQuestion('');
                }
                $entity->setChanged(new \DateTime());
                
                $entity->setModifiedOn(1);
                
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('poll_show', array(
                  'id' => $id
                )));
            }
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Poll:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));

    }

    /**
     * Deletes a Poll entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanPollBundle:Poll')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
                return $this->redirect($this->generateUrl('poll'));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollsection');
            $ps = $repository->createQueryBuilder('ps')
                ->select('count(ps.poll)')
                ->where('ps.poll = :poll')
                ->setParameter('poll', $entity->getId())
                ->groupBy('ps.poll')
                ->getQuery()
                ->getResult();
            
            if (current($ps) == 0) {
                $pollschedulings = $em->getRepository('FishmanPollBundle:Pollscheduling')->findByPoll($entity->getId());
                if (!empty($pollschedulings)) {
                    $session->getFlashBag()->add('error', 'La encuesta no puede ser borrada.');
                }
                else {
                    $em->remove($entity);
                    $em->flush();
                    
                    $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
                }
            }
            else {
                $session->getFlashBag()->add('error', 'No se puede borrar la Encuesta, existen datos asociados.');
            }
        }

        return $this->redirect($this->generateUrl('poll'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Poll entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Poll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Poll:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Active Version Poll entity.
     *
     */
    public function activeVersionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanPollBundle:Poll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
          
        // suspend auto-commit
        $em->getConnection()->beginTransaction();
        
        // Try and make the transaction
        try { 
            $query = $em->createQueryBuilder()
                ->update('FishmanPollBundle:Poll p')
                ->set('p.current_version', ':currentversion')
                ->where('p.original_id = :id')
                ->setParameter('currentversion', 0)
                ->setParameter('id', $entity->getOriginalId())
                ->getQuery()
                ->execute();
            
            $entity->setCurrentVersion(1);
            $em->persist($entity);
            $em->flush();
        
            // Try and commit the transaction
            $em->getConnection()->commit();
        } catch (Exception $e) {
            // Rollback the failed transaction attempt
            $em->getConnection()->rollback();
            throw $e;
        }

        return $this->redirect($this->generateUrl('poll'));
    }

    /**
     * Return poll select list with some category id
     * TODO: Change this as ajaxChoiceByCategory
     */
    public function choiceByCategoryAction()
    {
        $category_id = $_GET['category_id'];
        $repository = $this->getDoctrine()->getManager()->getRepository('FishmanPollBundle:Poll');

        $query = $repository->createQueryBuilder('p')
            ->innerJoin('FishmanPollBundle:Pollquestion', 'pq', 'WITH', 'p.id = pq.poll')
            ->where('p.category = :category_id 
                    AND p.current_version = :current_version 
                    AND p.status = 1')
            ->setParameter('category_id', $category_id)
            ->setParameter('current_version', 1)
            ->orderBy('p.id', 'ASC')
            ->getQuery();

        $polls = $query->getResult();

        $first = true;
        $description = '';
        $duration = '';
        $period = '';

        $combo = '<select class="polls" name="poll_polls">';
        $combo .= '<option value="">Seleccione la encuesta</option>';
        foreach($polls as $poll){
            if($first){
                $description = $poll->getDescription();
                $duration = $poll->getDuration();
                $period = $poll->getPeriod();
                $alignment = $poll->getAlignment();
                $first = false;
            }
            $combo .= '<option value="' . $poll->getId() . '">' . $poll->getTitle() . '</option>';
        }
        $combo .= '</select>';

        //$this->render('FishmanPollBundle:Entitypoll:choicebycategory.html.twig', array('polls' => $polls));

        $response = new Response(json_encode(
            array('combo' => $combo, 'description' => $description , 'duration' => $duration, 'period' => $period, 'alignment' => $alignment)));  
        return $response;
    }
    
    function jsonAction()
    {
        $poll_id = $_GET['poll_id'];   
        $poll = $this->getDoctrine()->getManager()->getRepository('FishmanPollBundle:Poll')->find($poll_id);

        $duration = '';
        $period = '';
        $description = '';
        $type = '';

        if($poll){
            $description = $poll->getDescription();
            $type = $poll->getType();
            $duration = $poll->getDuration();
            $period = $poll->getPeriod();
            $alignment = $poll->getAlignment();
        }

        $response = new Response(json_encode(array(
            'description' => $description, 
            'type' => $type, 
            'duration' => $duration, 
            'period' => $period, 
            'alignment' => $alignment
        ))); 
        return $response;
    }

    /**
     * Preview Poll entity.
     *
     */
    public function previewAction(Request $request, $id, $page)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $array_questions = '';

        $entity = $em->getRepository('FishmanPollBundle:Poll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        if (!$entity->getSerialized() || $entity->getModifiedOn()) {
            Poll::serializedPoll($this->getDoctrine(), $entity);
        }
        
        // Paginator
        
        $array_paginator = Poll::getPaginator($this->getDoctrine(), $entity, $page);
        
        if ($array_paginator['number_pagers'] > 0 && $page > $array_paginator['number_pagers']) {
            return $this->redirect($this->generateUrl('poll_preview', array(
                'id' => $id,
                'page' => $page
            )));
        }

        // Recover Questions in page current
        
        $array['sections'] = $entity->getSerialized();
        
        if ($entity->getDivide() == 'sections_show') {
            $array_questions = Poll::getArraySectionShow($this->getDoctrine(), $array, $entity->getId(), $page);
        }
        else if ($entity->getDivide() == 'number_questions') {
            $array_questions = Poll::getArrayNumberQuestions($this->getDoctrine(), $array['sections'], $entity->getId(), $page, $entity->getNumberQuestion());
        }
        else if ($entity->getDivide() == 'none') {
            // jtt
            $ids = array(19,26,27); //array con id de encuestas de TEMPERAMENTO
            if (in_array($entity->getId(), $ids)) {
            foreach ($array['sections'] as $llave => $valor) {
                if (array_key_exists('questions', $valor)) {
                    uasort($array['sections'][$llave]['questions'], function($a, $b) {
                        return $a['sequence'] - $b['sequence'];
                    });
                }
            }
            }
            $array_questions = $array;
        }
        
        if ($request->request->get('first', false)) {
            $page = 1;
        }
        if ($request->request->get('prev', false)) {
            $page--;
        }
        if ($request->request->get('page')) {
            $page = $request->request->get('page');
        }
        if ($request->request->get('next', false)) {
            $page++;
        }
        if ($request->request->get('last', false)) {
            $page = $array_paginator['number_pagers'];
        }
        
        if ($request->request->get('page') || $request->request->get('first', false) || $request->request->get('prev', false) || $request->request->get('next', false) || $request->request->get('last', false)) {
            $array_parameters['page'] = $page;
            return $this->redirect($this->generateUrl('poll_preview', array(
                'id' => $id,
                'page' => $page
            )));
        }
        
        // Array Parameters
        
        $array_parameters = array(
            'entity' => $entity,
            'page' => $page,
            'paginator' => $array_paginator['paginator'],
            'lastpage' => $array_paginator['number_pagers'],
            'alignment' => 'vertical',
            'questions' => $array_questions, 
            'alignment' => 'vertical'
        );
        
        if ($entity->getAlignment() == 'horizontal') {
            $array_parameters['alignment'] = 'horizontal';
        }
        
        return $this->render('FishmanPollBundle:Poll/Preview:resolved.html.twig', $array_parameters);
    }
    
    /**
     * Lists all Pollnotification entities.
     *
     */
    public function indexNotificationAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Poll Info
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        // Recovering data
        
        $predecessor_options = Notification::getListNotificationOptions($this->getDoctrine(), 'poll', $pollid);
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'predecessor' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('predecessor', 'choice', array(
                'choices' => $predecessor_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'predecessor' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
            
        $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notification');
        $queryBuilder = $repository->createQueryBuilder('n')
            ->select('n.id, n.name, n.notification_type type, n.notification_type_name type_name, np.name predecessor, n.sequence, n.changed, n.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'n.modified_by = u.id')
            ->leftJoin('FishmanNotificationBundle:Notification', 'np', 'WITH', 'n.predecessor = np.id')
            ->where('n.entity_id = :poll 
                    AND n.entity_type = :entity_type')
            ->andWhere('n.id LIKE :id 
                    OR n.name LIKE :name')
            ->setParameter('poll', $pollid)
            ->setParameter('entity_type', 'poll')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('n.id', 'ASC');
        
        // Add arguments
        
        if ($data['predecessor'] != '') {
            $queryBuilder
                ->andWhere('n.predecessor = :predecessor')
                ->setParameter('predecessor', $data['predecessor']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('n.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('n.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollnotification');
        $paginator->setMaxPagerItems(5, 'pollnotification');
        $entities = $paginator->paginate($query, 'pollnotification')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollnotification');
        $endPageItem = $paginator->getEndPageItem('pollnotification');
        $totalItems = $paginator->getTotalItems('pollnotification');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanPollBundle:Poll/Notification:index.html.twig', array(
            'entities' => $entities,
            'poll' => $poll,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Workshopnotification entity.
     *
     */
    public function showNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Notification
        $entity = Notification::getNotification($this->getDoctrine(), $id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        $pem = $this->getDoctrine()->getManager();
        $poll = $pem->getRepository('FishmanPollBundle:Poll')->find($entity['entity_id']);
        if (!$poll) {
            throw $this->createNotFoundException('Unable to find Poll entity.');
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanPollBundle:Poll/Notification:show.html.twig', array(
            'entity' => $entity,
            'poll' => $poll, 
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Pollnotification entity.
     *
     */
    public function newNotificationAction($pollid)
    {
        $entity = new Notification();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        $entity->setFinish('never');
        $entity->setSelectedTrigger('to_send');
        $entity->setStatus(1);
        $form = $this->createForm(new NotificationType($entity, 'poll', $pollid, $this->getDoctrine()), $entity);

        return $this->render('FishmanPollBundle:Poll/Notification:new.html.twig', array(
            'entity' => $entity,
            'poll' => $poll,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Pollnotification entity.
     *
     */
    public function createNotificationAction(Request $request, $pollid)
    {
        $entity  = new Notification();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        $asigneds = $request->request->get('asigned_options');
        $entity->setNotificationType('poll');

        $form = $this->createForm(new NotificationType($entity, 'poll', $pollid, $this->getDoctrine()), $entity);
        $form->bind($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setNotificationTypeId($pollid);
            $entity->setNotificationTypeName($poll->getTitle());
            if ($entity->getEjecutionType() == 'manual' || $entity->getEjecutionType() == 'trigger') {
                $entity->setPredecessor(-1);
            }
            if ($entity->getEjecutionType() == 'trigger') {
                $entity->setNotificationTypeStatus(TRUE);
            }
            else {
                $entity->setSelectedTrigger('');
            }
            if ($entity->getEjecutionType() != 'automatic') {
                $entity->setSince('');
                $entity->setReplay(false);
            }
            if ($entity->getReplay() == 0) {
                $entity->setRepeatPeriod('');
                $entity->setRepeatRange('');
                $entity->setFinish('');
            }
            if ($entity->getFinish() != 'after') {
                $entity->setRepetitionNumber('');
            }
            if ($entity->getPredecessor() == '') {
                $entity->setPredecessor(-1);
            }
            $entity->setAsigned($asigneds['data2']);
            $entity->setEntityType('poll');
            $entity->setEntityId($poll->getId());
            $entity->setEntityName($poll->getTitle());
            $entity->setEntityStatus($poll->getStatus());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('pollnotification_show', array(
                'id' => $entity->getId()
            )));
        }

        $entity->setNotificationType('poll');
        $form = $this->createForm(new NotificationType($entity, 'poll', $pollid, $this->getDoctrine()), $entity);
        
        return $this->render('FishmanPollBundle:Poll/Notification:new.html.twig', array(
            'entity' => $entity,
            'poll' => $poll,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Return the list of a notification type 
     */
    public function choiceByTypeNotificationAction($pollid)
    {   
        $typeNotification = $_GET['type'];
        
        //Filter for type Notification
        if ($typeNotification == 'message') {
          $multiSelect = '<option value="boss">Jefe</option>';
          $multiSelect .= '<option value="collaborator">Colaborador</option>';
          $multiSelect .= '<option value="integrant">Participante</option>';
          $multiSelect .= '<option value="company">Empresa</option>';
          $multiSelect .= '<option value="responsible">Responsable</option>';
        } elseif ($typeNotification == 'poll') {
          $multiSelect = '<option value="evaluated">Evaluado</option>';
          $multiSelect .= '<option value="evaluator">Evaluador</option>';
        }
        
        $response = new Response(json_encode(
            array('multi_select' => $multiSelect)));  
        return $response;
    }
    
    /**
     * Displays a form to edit an existing Pollnotification entity.
     *
     */
    public function editNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($entity->getEntityId());
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
         
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $entity->setFinish('never');
        $entity->setSelectedTrigger('to_send');
        $editForm = $this->createForm(new NotificationType($entity, 'poll', $entity->getEntityId(), $this->getDoctrine()), $entity);
        
        return $this->render('FishmanPollBundle:Poll/Notification:edit.html.twig', array(
            'entity' => $entity,
            'poll' => $poll, 
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Pollnotification entity.
     *
     */
    public function updateNotificationAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        $asigneds = $request->request->get('asigned_options');
        $notificationType = $entity->getNotificationType();

        $editForm = $this->createForm(new NotificationType($entity, 'poll', $entity->getEntityId(), $this->getDoctrine()), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setNotificationType($notificationType);
            if ($entity->getEjecutionType() == 'manual' || $entity->getEjecutionType() == 'trigger') {
                $entity->setPredecessor(-1);
            }
            if ($entity->getEjecutionType() == 'trigger') {
                $entity->setNotificationTypeStatus(TRUE);
            }
            else {
                $entity->setSelectedTrigger('');
            }
            if ($entity->getEjecutionType() != 'automatic') {
                $entity->setSince('');
                $entity->setReplay(false);
            }
            if ($entity->getReplay() == 0) {
                $entity->setRepeatPeriod('');
                $entity->setRepeatRange('');
                $entity->setFinish('');
            }
            if ($entity->getFinish() != 'after') {
                $entity->setRepetitionNumber('');
            }
            if ($entity->getPredecessor() == '') {
                $entity->setPredecessor(-1);
            }
            $entity->setAsigned($asigneds['data2']);
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
                                                        
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('pollnotification_show', array(
                'id' => $id
            )));
        }
        
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($entity->getEntityId());
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        $entity->setNotificationType('poll');
        $editForm = $this->createForm(new NotificationType($entity, 'poll', $entity->getEntityId(), $this->getDoctrine()), $entity);
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Poll/Notification:edit.html.twig', array(
            'entity' => $entity,
            'poll' => $poll,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Pollnotification entity.
     *
     */
    public function dropNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($entity->getEntityId());
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        // Check if you have legacy
        $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $pns = $repository->createQueryBuilder('ns')
            ->select('count(ns.notification)')
            ->where('ns.notification = :notification')
            ->setParameter('notification', $entity->getId())
            ->groupBy('ns.notification')
            ->getQuery()
            ->getResult();
        
        if (current($pns) > 0) {
            $session->getFlashBag()->add('error', 'La Notificación está en uso.');
            return $this->redirect($this->generateUrl('pollnotification', array(
                'pollid' => $entity->getPoll()->getId()
            )));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Poll/Notification:drop.html.twig', array(
            'entity' => $entity,
            'poll' => $poll, 
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a Pollnotification entity.
     *
     */
    public function deleteNotificationAction(Request $request, $id, $pollid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $em = $this->getDoctrine()->getManager();
          
            $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la encuesta.');
                return $this->redirect($this->generateUrl('pollnotification', array(
                    'pollid' => $pollid
                )));
            }

            // Update children predecessor
            $em->createQueryBuilder()
                ->update('FishmanNotificationBundle:Notification n')
                ->set('n.predecessor', ':predecessor')
                ->where('n.predecessor = :notification')
                ->setParameter('predecessor', '-1')
                ->setParameter('notification', $id)
                ->getQuery()
                ->execute();
            
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('pollnotification', array(
            'pollid' => $pollid
        )));
    }
}
