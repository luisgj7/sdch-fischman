<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\PollBundle\Entity\Pollschedulingquestion;
use Fishman\PollBundle\Entity\Pollscheduling;
use Fishman\PollBundle\Entity\Pollschedulingsection;
use Fishman\PollBundle\Entity\Pollquestion;
use Fishman\PollBundle\Entity\Pollapplicationquestion;
use Fishman\PollBundle\Form\PollschedulingquestionType;

use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Pollschedulingquestion controller.
 *
 */
class PollschedulingquestionController extends Controller
{
    /**
     * Lists all Pollschedulingquestion entities.
     *
     */
    public function indexAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Pollscheduling Info
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recovering data
        
        $section_options = Pollschedulingsection::getPollschedulingSectionsOptions($this->getDoctrine(), $pollschedulingid);
        $type_options = Pollquestion::getListTypeOptions();
        for ($i = 1; $i <= 50; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',  
            'section' => '', 
            'type' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('section', 'choice', array(
                'choices' => $section_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'section' => '', 
            'type' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingquestion');
        $queryBuilder = $repository->createQueryBuilder('psq')
            ->select('psq.id, psq.question, pss.name pollsection, psq.type, psq.changed, psq.sequence, psq.status, u.names, u.surname, u.lastname')
            ->leftJoin('psq.pollschedulingsection', 'pss')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'psq.modified_by = u.id')
            ->where('psq.pollscheduling = :pollscheduling')
            ->andWhere('psq.id LIKE :id 
                    OR psq.question LIKE :question')
            ->setParameter('pollscheduling', $pollschedulingid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('question', '%' . $data['word'] . '%')
            ->orderBy('psq.id', 'ASC');
        
        // Add arguments
        
        if ($data['section'] != '') {
            $queryBuilder
                ->andWhere('psq.pollschedulingsection = :section')
                ->setParameter('section', $data['section']);
        }
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('psq.type = :type')
                ->setParameter('type', $data['type']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('psq.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('psq.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollschedulingquestion');
        $paginator->setMaxPagerItems(5, 'pollschedulingquestion');
        $entities = $paginator->paginate($query, 'pollschedulingquestion')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollschedulingquestion');
        $endPageItem = $paginator->getEndPageItem('pollschedulingquestion');
        $totalItems = $paginator->getTotalItems('pollschedulingquestion');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanPollBundle:Pollschedulingquestion:index.html.twig', array(
            'entities' => $entities,
            'pollscheduling' => $pollscheduling,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Pollschedulingquestion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
        if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanPollBundle:Pollschedulingquestion:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Pollschedulingquestion entity.
     *
     */
    public function newAction($pollschedulingid)
    {   
        $entity = new Pollschedulingquestion();
        $entity->setStatus(1);
        $em = $this->getDoctrine()->getManager();  
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $pollscheduling->getId());
        if (count($paq) > 0) {
            $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, no se permite crear nuevas preguntas');
            return $this->redirect($this->generateUrl('pollschedulingquestion', array(
                'pollschedulingid' => $pollscheduling->getId()
            )));
        }
        
        $entity->setPollscheduling($pollscheduling);
        $entity->setStatus(1);
        $form   = $this->createForm(new PollschedulingquestionType($pollschedulingid, '', $this->getDoctrine()), $entity);

        return $this->render('FishmanPollBundle:Pollschedulingquestion:new.html.twig', array(
            'entity' => $entity,
            'pollscheduling' => $pollscheduling,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Pollschedulingquestion entity.
     *
     */
    public function createAction(Request $request, $pollschedulingid)
    {
        $entity  = new Pollschedulingquestion();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recover options
        
        $options = array();
        $requestPollschedulingQuestion = $request->request->get('fishman_pollbundle_pollschedulingquestiontype');
        
        if (in_array($requestPollschedulingQuestion['type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestPollschedulingQuestion['options'])) {
                $optionsArray = explode('%%', $requestPollschedulingQuestion['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }
        
        if ($pollscheduling->getLevel() === 0) {
            $form = $this->createForm(new PollschedulingquestionType(
                           $pollschedulingid, null, $this->getDoctrine()), $entity);
        }
        else {
            $form = $this->createForm(new PollschedulingquestionType(
                           $pollschedulingid, $requestPollschedulingQuestion['pollschedulingsection_id'], $this->getDoctrine()), $entity);
        }
        
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $section = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find(
                         $requestPollschedulingQuestion['pollschedulingsection_id']);
            $entity->setPollschedulingsection($section);

            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setPollscheduling($pollscheduling);
            if (in_array($entity->getType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                $entity->setAlignment('');
            }
            if (in_array($entity->getType(), array('multiple_text', 'simple_text'))) {
                $entity->setOptions(array());
            }
            else {
                $entity->setOptions($options);
            }
            $entity->setOriginalSequence($entity->getSequence());
            $entity->setLevel($pollscheduling->getLevel());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.<br/>
                                                    Debe regenerar la Programación de Encuesta para que los 
                                                    cambios sean visibles en las evaluaciones de la misma.');

            return $this->redirect($this->generateUrl('pollschedulingquestion_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanPollBundle:Pollschedulingquestion:new.html.twig', array(
            'entity' => $entity,
            'pollscheduling' => $pollscheduling,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pollschedulingquestion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $disabled = FALSE;
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
        if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $entity->getPollscheduling()->getId());
        if (count($paq) > 0) {
            $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, sólo puede editar el nombre de la pregunta');
            $disabled = TRUE;
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        if ($entity->getPollscheduling()->getLevel() === 0) {
            $editForm = $this->createForm(new PollschedulingquestionType($entity->getPollscheduling()->getId(),
                                          null, $this->getDoctrine(), $disabled), $entity);
        }
        else {
            $editForm = $this->createForm(new PollschedulingquestionType($entity->getPollscheduling()->getId(),
                                          $entity->getPollschedulingsection()->getId(), $this->getDoctrine(), $disabled), $entity);
        }

        return $this->render('FishmanPollBundle:Pollschedulingquestion:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView(), 
            'disabled' => $disabled
        ));
    }

    /**
     * Edits an existing Pollschedulingquestion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
        if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recover options
        
        $options = array();
        $requestPollschedulingQuestion = $request->request->get('fishman_pollbundle_pollschedulingquestiontype');
        
        if (isset($requestPollschedulingQuestion['type'])) {
            if (in_array($requestPollschedulingQuestion['type'], array('unique_option', 'multiple_option', 'selection_option'))) {
                if (!empty($requestPollschedulingQuestion['options'])) {
                    $optionsArray = explode('%%', $requestPollschedulingQuestion['options']);
                    $i = 0;
                    foreach ($optionsArray as $op_text) {
                        $op = explode('::', $op_text);
                        if (!empty($op)) {
                            $option = explode(' | ', $op[1]);
                            $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                        }
                        $i++;
                    }
                }
            }
        }
        else {
            $entityOriginal['type'] = $entity->getType();
            $entityOriginal['alignment'] = $entity->getAlignment();
            $entityOriginal['pollschedulingsection'] = $entity->getPollschedulingsection();
            $entityOriginal['mandatory_response'] = $entity->getMandatoryResponse();
            $entityOriginal['sequence'] = $entity->getSequence();
            $entityOriginal['status'] = $entity->getStatus();
            $entityOriginal['options'] = $entity->getOptions();
        }
        
        if ($entity->getPollscheduling()->getLevel() === 0) {
            $editForm = $this->createForm(new PollschedulingquestionType($entity->getPollscheduling()->getId(),
                                          null, $this->getDoctrine()), $entity);
        }
        else {
            $editForm = $this->createForm(new PollschedulingquestionType($entity->getPollscheduling()->getId(),
                                          $entity->getPollschedulingsection()->getId(), $this->getDoctrine()), $entity);
        }
        
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            if (isset($requestPollschedulingQuestion['type'])) {
                $section = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find(
                             $requestPollschedulingQuestion['pollschedulingsection_id']);
                $entity->setPollschedulingsection($section);
              
                if (in_array($entity->getType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                    $entity->setAlignment('');
                }
                if (in_array($entity->getType(), array('multiple_text', 'simple_text'))) {
                    $entity->setOptions(array());
                }
                else {
                    $entity->setOptions($options);
                }
            }
            else {
                $entity->setType($entityOriginal['type']);
                $entity->setAlignment($entityOriginal['alignment']);
                $entity->setPollschedulingsection($entityOriginal['pollschedulingsection']);
                $entity->setMandatoryResponse($entityOriginal['mandatory_response']);
                $entity->setSequence($entityOriginal['sequence']);
                $entity->setStatus($entityOriginal['status']);
                $entity->setOptions($entityOriginal['options']);
            }
            
            $entity->setChanged(new \DateTime());
            $entity->setModifiedBy($modifiedBy->getId());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.<br/>
                                                    Debe regenerar la Programación de Encuesta para que los 
                                                    cambios sean visibles en las evaluaciones de la misma.');

            return $this->redirect($this->generateUrl('pollschedulingquestion_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollschedulingquestion:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Pollschedulingquestion entity.
     *
     */
    public function deleteAction(Request $request, $id, $pollschedulingid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
            if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
                $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
                return $this->redirect($this->generateUrl('pollschedulingquestion', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.<br/>
                                                    Debe regenerar la Programación de Encuesta para que los 
                                                    cambios sean visibles en las evaluaciones de la misma.');
        }

        return $this->redirect($this->generateUrl('pollschedulingquestion', array(
          'pollschedulingid' => $pollschedulingid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Pollschedulingsection entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
        if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $entity->getPollscheduling()->getId());
        if (count($paq) > 0) {
            $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, no se permite eliminar preguntas');
            return $this->redirect($this->generateUrl('pollschedulingquestion', array(
                'pollschedulingid' => $entity->getPollscheduling()->getId()
            )));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Pollschedulingquestion:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }
}
