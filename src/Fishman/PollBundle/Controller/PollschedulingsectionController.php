<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\PollBundle\Entity\Pollschedulingsection;
use Fishman\PollBundle\Entity\Pollscheduling;
use Fishman\PollBundle\Entity\Pollapplicationquestion;
use Fishman\PollBundle\Form\PollschedulingsectionType;

use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Pollschedulingsection controller.
 *
 */
class PollschedulingsectionController extends Controller
{
    /**
     * Lists all Pollschedulingsection entities.
     *
     */
    public function indexAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Poll Info
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if ($pollscheduling->getLevel() === 0) {
            $session->getFlashBag()->add('error', 'La encuesta ha sido programada sin secciones.');
            return $this->redirect($this->generateUrl('pollscheduling_show', array(
                'id' => $pollschedulingid
            )));
        }
        
        // Recovering data
        
        $parent_options = Pollschedulingsection::getPollschedulingSectionsOptions($this->getDoctrine(), $pollschedulingid);
        for ($i = 1; $i <= 50; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',  
            'parent' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('parent', 'choice', array(
                'choices' => $parent_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',  
            'parent' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingsection');
        $queryBuilder = $repository->createQueryBuilder('pss')
            ->select('pss.id, pss.name, pssp.name parent, pss.sequence, pss.changed, pss.status, 
                      u.names, u.surname, u.lastname')
            ->leftJoin('FishmanPollBundle:Pollschedulingsection', 'pssp', 'WITH', 'pss.parent_id = pssp.id')
            ->leftJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pss.modified_by = u.id')
            ->where('pss.pollscheduling = :pollscheduling')
            ->andWhere('pss.id LIKE :id 
                    OR pss.name LIKE :name')
            ->setParameter('pollscheduling', $pollschedulingid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('pss.id', 'ASC');
        
        // Add arguments
        
        if ($data['parent'] != '') {
            $queryBuilder
                ->andWhere('pss.parent_id = :parent')
                ->setParameter('parent', $data['parent']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('pss.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('pss.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollschedulingsection');
        $paginator->setMaxPagerItems(5, 'pollschedulingsection');
        $entities = $paginator->paginate($query, 'pollschedulingsection')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollschedulingsection');
        $endPageItem = $paginator->getEndPageItem('pollschedulingsection');
        $totalItems = $paginator->getTotalItems('pollschedulingsection');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanPollBundle:Pollschedulingsection:index.html.twig', array(
            'entities' => $entities,
            'pollscheduling' => $pollscheduling,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Pollschedulingsection entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingsection');
        $result = $repository->createQueryBuilder('pss')
            ->select('pss.id, pss.name, ps.id pollscheduling_id, ps.title pollscheduling_title, 
                      ps.entity_type pollscheduling_entitytype, ps.pollscheduling_anonymous, pss.description, 
                      pssp.name parent, pss.sequence, pss.changed, pss.status, pss.created, 
                      pss.changed, pss.created_by, pss.modified_by')
            ->innerJoin('pss.pollscheduling', 'ps')
            ->leftJoin('FishmanPollBundle:Pollschedulingsection', 'pssp', 'WITH', 'pss.parent_id = pssp.id')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pss.modified_by = u.id')
            ->where('pss.id = :pollschedulingsection')
            ->setParameter('pollschedulingsection', $id)
            ->getQuery()
            ->getResult();
        
        $entity = current($result);

        if (!$entity || $entity['pollscheduling_entitytype'] != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanPollBundle:Pollschedulingsection:show.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Pollschedulingsection entity.
     *
     */
    public function newAction($pollschedulingid)
    {
        $entity = new Pollschedulingsection();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        if ($pollscheduling->getLevel() === 0) {
            $session->getFlashBag()->add('error', 'La encuesta ha sido programada sin secciones.');
            return $this->redirect($this->generateUrl('pollscheduling_show', array(
                'id' => $pollschedulingid
            )));
        }
        
        $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $pollscheduling->getId());
        if (count($paq) > 0) {
            $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, no se permite crear nuevas secciones');
            return $this->redirect($this->generateUrl('pollschedulingsection', array(
                'pollschedulingid' => $pollscheduling->getId()
            )));
        }
        
        $entity->setPollscheduling($pollscheduling);
        $entity->setStatus(1);
        $form   = $this->createForm(new PollschedulingsectionType($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanPollBundle:Pollschedulingsection:new.html.twig', array(
            'entity' => $entity,
            'pollscheduling' => $pollscheduling,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Pollschedulingsection entity.
     *
     */
    public function createAction(Request $request, $pollschedulingid)
    {
        $entity  = new Pollschedulingsection();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        $entity->setPollscheduling($pollscheduling);
        $form = $this->createForm(new PollschedulingsectionType($entity, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();

            if ($entity->getParentId() != '-1') {
                $ps_entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($entity->getParentId());
                $entity->setL1($ps_entity->getL1()); 
                $entity->setL2($ps_entity->getL2()); 
                $entity->setL3($ps_entity->getL3()); 
                $entity->setL4($ps_entity->getL4()); 
                $entity->setL5($ps_entity->getL5()); 
                $entity->setL6($ps_entity->getL6()); 
                $entity->setL7($ps_entity->getL7()); 
                $entity->setL8($ps_entity->getL8()); 
                $entity->setL9($ps_entity->getL9());
            }
            $entity->setType(1);
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setPollscheduling($pollscheduling);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            if ($entity->getParentId() == '-1') {
                $entity->setL1($entity->getId());
            }
            else {
                $level = FALSE;
              
                if ($entity->getL2() == '') {
                    $entity->setL2($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL3() == '' && !$level) {
                    $entity->setL3($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL4() == '' && !$level) {
                    $entity->setL4($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL5() == '' && !$level) {
                    $entity->setL5($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL6() == '' && !$level) {
                    $entity->setL6($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL7() == '' && !$level) {
                    $entity->setL7($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL8() == '' && !$level) {
                    $entity->setL8($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL9() == '' && !$level) {
                    $entity->setL9($entity->getId());
                    $level = TRUE;
                }
            }
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.<br/>
                                                    Debe regenerar la Programación de Encuesta para que los 
                                                    cambios sean visibles en las evaluaciones de la misma.');

            return $this->redirect($this->generateUrl('pollschedulingsection_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanPollBundle:Pollschedulingsection:new.html.twig', array(
            'entity' => $entity,
            'pollscheduling' => $pollscheduling,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pollschedulingsection entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $disabled = FALSE;
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($id);
        if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $entity->getPollscheduling()->getId());
        if (count($paq) > 0) {
            $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, sólo puede editar el nombre y/o descripción de la sección');
            $disabled = TRUE;
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new PollschedulingsectionType($entity, $this->getDoctrine(), $disabled), $entity);

        return $this->render('FishmanPollBundle:Pollschedulingsection:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Pollschedulingsection entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($id);
        if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        $parent_id = $entity->getParentId();
        
        $editForm = $this->createForm(new PollschedulingsectionType($entity, $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            if ($entity->getParentId()) {
                if (!in_array($entity->getParentId(), array('-1', $parent_id))) {
                    $ps_entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($entity->getParentId());
                    $entity->setL1($ps_entity->getL1()); 
                    $entity->setL2($ps_entity->getL2()); 
                    $entity->setL3($ps_entity->getL3()); 
                    $entity->setL4($ps_entity->getL4()); 
                    $entity->setL5($ps_entity->getL5()); 
                    $entity->setL6($ps_entity->getL6()); 
                    $entity->setL7($ps_entity->getL7()); 
                    $entity->setL8($ps_entity->getL8()); 
                    $entity->setL9($ps_entity->getL9());
                }
            }
            else {
                $entity->setParentId($parent_id);
            }
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();

            if ($entity->getParentId() != $parent_id) {
                // Update Pollschedulingsection to ParentId
                Pollschedulingsection::updateRegisterParentId($this->getDoctrine(), $entity);
            }
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.<br/>
                                                    Debe regenerar la Programación de Encuesta para que los 
                                                    cambios sean visibles en las evaluaciones de la misma.');

            return $this->redirect($this->generateUrl('pollschedulingsection_show', array(
                'id' => $id
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollschedulingsection:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));

    }

    /**
     * Deletes a Pollschedulingsection entity.
     *
     */
    public function deleteAction(Request $request, $id, $pollschedulingid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $em = $this->getDoctrine()->getManager();
          
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($id);
            if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
                return $this->redirect($this->generateUrl('pollschedulingsection', array(
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.<br/>
                                                    Debe regenerar la Programación de Encuesta para que los 
                                                    cambios sean visibles en las evaluaciones de la misma.');
        }

        return $this->redirect($this->generateUrl('pollschedulingsection', array(
          'pollschedulingid' => $pollschedulingid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Pollschedulingsection entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($id);
        if (!$entity || $entity->getPollscheduling()->getEntityType() != 'pollscheduling') {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $entity->getPollscheduling()->getId());
        if (count($paq) > 0) {
            $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, no se permite eliminar secciones');
            return $this->redirect($this->generateUrl('pollschedulingsection', array(
                'pollschedulingid' => $entity->getPollscheduling()->getId()
            )));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Pollschedulingsection:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }
}
