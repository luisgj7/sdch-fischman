<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\PollBundle\Entity\Pollquestion;
use Fishman\PollBundle\Form\PollquestionType;
use Fishman\PollBundle\Entity\Poll;
use Fishman\PollBundle\Entity\Pollsection;

use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Pollquestion controller.
 *
 */
class PollquestionController extends Controller
{
    /**
     * Lists all Pollquestion entities.
     *
     */
    public function indexAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Poll Info
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        // Recovering data
        
        $section_options = Pollsection::getPollSectionsOptions($this->getDoctrine(), $pollid);
        $type_options = Pollquestion::getListTypeOptions();
        for ($i = 1; $i <= 50; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',  
            'section' => '', 
            'type' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('section', 'choice', array(
                'choices' => $section_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'section' => '', 
            'type' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollquestion');
        $queryBuilder = $repository->createQueryBuilder('pq')
            ->select('pq.id, pq.question, ps.name pollsection, pq.type, pq.changed, pq.sequence, pq.status, u.names, u.surname, u.lastname')
            ->innerJoin('pq.pollsection', 'ps')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pq.modified_by = u.id')
            ->where('pq.poll = :poll')
            ->andWhere('pq.id LIKE :id 
                    OR pq.question LIKE :question')
            ->setParameter('poll', $pollid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('question', '%' . $data['word'] . '%')
            ->orderBy('pq.id', 'ASC');
        
        // Add arguments
        
        if ($data['section'] != '') {
            $queryBuilder
                ->andWhere('pq.pollsection = :section')
                ->setParameter('section', $data['section']);
        }
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('pq.type = :type')
                ->setParameter('type', $data['type']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('pq.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('pq.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollquestion');
        $paginator->setMaxPagerItems(5, 'pollquestion');
        $entities = $paginator->paginate($query, 'pollquestion')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollquestion');
        $endPageItem = $paginator->getEndPageItem('pollquestion');
        $totalItems = $paginator->getTotalItems('pollquestion');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanPollBundle:Pollquestion:index.html.twig', array(
            'entities' => $entities,
            'poll' => $poll,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Pollquestion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollquestion')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanPollBundle:Pollquestion:show.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Pollquestion entity.
     *
     */
    public function newAction($pollid)
    {   
        $entity = new Pollquestion();
        $entity->setStatus(1);
        $em = $this->getDoctrine()->getManager();  
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        $entity->setPoll($poll);
        $entity->setStatus(1);
        $form   = $this->createForm(new PollquestionType($pollid, '', $this->getDoctrine()), $entity);

        return $this->render('FishmanPollBundle:Pollquestion:new.html.twig', array(
            'entity' => $entity,
            'poll' => $poll,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Pollquestion entity.
     *
     */
    public function createAction(Request $request, $pollid)
    {
        $entity  = new Pollquestion();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        // Recover options
        
        $options = array();
        $requestPollQuestion = $request->request->get('fishman_pollbundle_pollquestiontype');
        
        if (in_array($requestPollQuestion['type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestPollQuestion['options'])) {
                $optionsArray = explode('%%', $requestPollQuestion['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }
        
        $form = $this->createForm(new PollquestionType(
                       $pollid, $requestPollQuestion['pollsection_id'], $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $section = $em->getRepository('FishmanPollBundle:Pollsection')->find(
                         $requestPollQuestion['pollsection_id']);
            $entity->setPollsection($section);

            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setPoll($poll);
            if (in_array($entity->getType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                $entity->setAlignment('');
            }
            if (in_array($entity->getType(), array('multiple_text', 'simple_text'))) {
                $entity->setOptions(array());
            }
            else {
                $entity->setOptions($options);
            }
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $poll->setModifiedOn(TRUE);
            $em->persist($poll);
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('pollquestion_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanPollBundle:Pollquestion:new.html.twig', array(
            'entity' => $entity,
            'poll' => $poll,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pollquestion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanPollBundle:Pollquestion')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new PollquestionType($entity->getPoll()->getId(),
                                      $entity->getPollsection()->getId(), $this->getDoctrine()), $entity);

        return $this->render('FishmanPollBundle:Pollquestion:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Pollquestion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanPollBundle:Pollquestion')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($entity->getPoll()->getId());
        
        // Recover options
        
        $options = array();
        $requestPollQuestion = $request->request->get('fishman_pollbundle_pollquestiontype');
        
        if (in_array($requestPollQuestion['type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestPollQuestion['options'])) {
                $optionsArray = explode('%%', $requestPollQuestion['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }
        
        $editForm = $this->createForm(new PollquestionType($entity->getPoll()->getId(),
                                      $entity->getPollsection()->getId(), $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $section = $em->getRepository('FishmanPollBundle:Pollsection')->find(
                         $requestPollQuestion['pollsection_id']);
            $entity->setPollsection($section);
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            if (in_array($entity->getType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                $entity->setAlignment('');
            }
            if (in_array($entity->getType(), array('multiple_text', 'simple_text'))) {
                $entity->setOptions(array());
            }
            else {
                $entity->setOptions($options);
            }
            $entity->setChanged(new \DateTime());
            $entity->setModifiedBy($modifiedBy->getId());
            
            $poll->setModifiedOn(TRUE);
            $em->persist($poll);
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.'); 

            return $this->redirect($this->generateUrl('pollquestion_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollquestion:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Pollquestion entity.
     *
     */
    public function deleteAction(Request $request, $id, $pollid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanPollBundle:Pollquestion')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta.');
                return $this->redirect($this->generateUrl('pollquestion', array(
                    'pollid' => $pollid
                )));
            }
            
            $poll = $em->getRepository('FishmanPollBundle:Poll')->find($entity->getPoll()->getId());
            
            $poll->setModifiedOn(TRUE);
            $em->persist($poll);

            if ($entity instanceof Pollquestion) {
                $entity->setStatus(false);
            }
            $em->persist($entity);
            
            // $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('pollquestion', array(
          'pollid' => $pollid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Pollsection entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollquestion')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
            
        // Check if you have legacy
        /*$repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingquestion');
        $psq = $repository->createQueryBuilder('psq')
            ->select('count(psq.pollquestion)')
            ->where('psq.pollquestion = :pollquestion')
            ->setParameter('pollquestion', $entity->getId())
            ->groupBy('psq.pollquestion')
            ->getQuery()
            ->getResult();
        
        if (count($psq) > 0) {
            $session->getFlashBag()->add('error', 'No se puede borrar la Pregunta, existen datos asociados.');
            return $this->redirect($this->generateUrl('pollquestion', array(
                'pollid' => $entity->getPoll()->getId()
            )));
        }*/

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Pollquestion:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }
}
