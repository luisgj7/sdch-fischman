<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\WorkshopBundle\Entity\Workshopapplication;
use Fishman\PollBundle\Entity\Pollapplication;
use Fishman\PollBundle\Entity\Pollschedulingpeople;
use Fishman\AuthBundle\Entity\User;
use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\PollBundle\Form\PollapplicationType;

/**
 * Pollapplication controller.
 *
 */
class PollapplicationController extends Controller
{
    /**
     * Metodo creado para la importación de evaluaciones una a una
     **/
    public function importEvaluationOnebyoneAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session variable workinginformation
        $session = $this->getRequest()->getSession();
        $winformation = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getDeleted()) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Form
        $form = $this->createFormBuilder(array('id' => $pollschedulingid))
                     ->add('id', 'hidden')
                     ->getForm();
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplicationimport');
        //jtt: 20150814, se agrego , pai.cargo_global er_cargo, pai.nivel_responsabilidad er_responsabilidad
        $pollApplicationImports = $repository->createQueryBuilder('pai')
            ->select('pai.id, pai.pollscheduling_id, pai.evaluated_code ev_code, pai.evaluator_code er_code, 
                      pai.evaluator_rol er_rol, pai.cargo_global er_cargo, pai.nivel_responsabilidad er_responsabilidad, 
                      pai.course course_id, pai.section section_id, pai.status, 
                      ppiev.names ev_names, ppiev.surname ev_surname, ppiev.lastname ev_lastname, 
                      ppier.names er_names, ppier.surname er_surname, ppier.lastname er_lastname')
            ->leftJoin('FishmanPollBundle:Pollpersonimport', 'ppiev', 'WITH', 'pai.evaluated_id = ppiev.id')
            ->leftJoin('FishmanPollBundle:Pollpersonimport', 'ppier', 'WITH', 'pai.evaluator_id = ppier.id')
            ->where('pai.pollscheduling_id = :pollscheduling')
            ->setParameter('pollscheduling', $pollschedulingid)
            ->orderBy('pai.id', 'ASC', 'ppier.code', 'ASC', 'ppiev.code', 'ASC')
            ->getQuery()
            ->getResult();

        $paiOkCount = $repository->createQueryBuilder('pai')
            ->select('count(pai.id)')
            ->where('pai.status = 1 
                   AND pai.pollscheduling_id = :pollscheduling')
            ->setParameter('pollscheduling', $pollschedulingid)
            ->getQuery()
            ->getSingleResult();
        
        return $this->render('FishmanPollBundle:Pollapplication:import_onebyone.html.twig', array(
            'pollscheduling' => $pollscheduling, 
            'poll_application_imports' => $pollApplicationImports, 
            'poll_application_imports_json' => json_encode($pollApplicationImports), 
            'paiok' => $paiOkCount[1],
            'paicount' => count($pollApplicationImports),
            'form'   => $form->createView()
        ));

    }

    /**
     * Mensaje de confirmación de importar evaluación
     */
    public function importEvaluationMessageAction($pollschedulingid, $paiid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //message
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getDeleted()) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recover Pollapplicationimport
        $pai = $em->getRepository('FishmanPollBundle:Pollapplicationimport')->find($paiid);
        
        //Antes de enviar las notificaciones solicita una confirmación
        return $this->render('FishmanPollBundle:Pollapplication:import_evaluation.html.twig', array(
            'pollscheduling' => $pollscheduling, 
            'pai' => $pai
        ));
    }

    /**
     * Importar evaluación
     */
    public function importEvaluationAction($pollschedulingid, $paiid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //message
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getDeleted()) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de encuesta.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Recover Pollapplicationimport
        $pai = $em->getRepository('FishmanPollBundle:Pollapplicationimport')->find($paiid);
        
        if ($pollscheduling && $pai) {
            
            $result = TRUE;
            $messageError = '';
            
            if ($pollscheduling->getEntityType() == 'pollscheduling') {
                $entityAT = 'pollschedulingpeople';
                $entityId = NULL;
            }
            elseif ($pollscheduling->getEntityType() == 'workshopscheduling') {
                $entityAT = 'workshopapplication';
                $entityId = $pollscheduling->getEntityId();
            }
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            if ($pollscheduling->getType() != 'not_evaluateds') {
                
                // Recover Evaluated and Evaluator
                $evaluated = $em->getRepository('FishmanPollBundle:Pollpersonimport')->find($pai->getEvaluatedId());
                $evaluator = $em->getRepository('FishmanPollBundle:Pollpersonimport')->find($pai->getEvaluatorId());
                
                $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
                $evaluatedEmail = $repository->createQueryBuilder('u')
                    ->where('u.email = :email 
                            AND u.numberidentity <> :numberidentity')
                    ->setParameter('email', $evaluated->getEmail())
                    ->setParameter('numberidentity', $evaluated->getNumberidentity())
                    ->getQuery()
                    ->getResult();
                if ($evaluatedEmail) {
                    $messageError .= 'El correo de evaluado lo está usando otro usuario.<br/>';
                }
                
                $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
                $evaluatorEmail = $repository->createQueryBuilder('u')
                    ->where('u.email = :email 
                            AND u.numberidentity <> :numberidentity')
                    ->setParameter('email', $evaluator->getEmail())
                    ->setParameter('numberidentity', $evaluator->getNumberidentity())
                    ->getQuery()
                    ->getResult();
                if ($evaluatorEmail) {
                    $messageError .= 'El correo de evaluador lo está usando otro usuario.<br/>';
                }
                
                if ($evaluatedEmail || $evaluatorEmail) {
                    $session->getFlashBag()->add('error', $messageError);
                    return $this->redirect($this->generateUrl('pollapplication_import_one_by_one', array(
                        'pollschedulingid' => $pollschedulingid
                    )));
                }
                
                $ev_user = User::addUserToEvaluation($this->getDoctrine(), $evaluated, $userBy);
                $ev_wi = Workinginformation::addWorkinginformationToEvaluation($this->getDoctrine(), $ev_user, $evaluated, $userBy);
                if ($pollscheduling->getEntityType() == 'pollscheduling') {
                    $ev_psp = Pollschedulingpeople::addRegister($this->getDoctrine(), $evaluated->getRol(), $ev_wi, $pollscheduling, $userBy);
                }
                elseif ($pollscheduling->getEntityType() == 'workshopscheduling') {
                    $ev_psp = Workshopapplication::addRegister($this->getDoctrine(), $evaluated->getRol(), $ev_wi, $pollscheduling->getEntityId(), $userBy);
                }
                
                $er_user = User::addUserToEvaluation($this->getDoctrine(), $evaluator, $userBy);
                $er_wi = Workinginformation::addWorkinginformationToEvaluation($this->getDoctrine(), $er_user, $evaluator, $userBy);
                if ($pollscheduling->getEntityType() == 'pollscheduling') {
                    $er_psp = Pollschedulingpeople::addRegister($this->getDoctrine(), $evaluator->getRol(), $er_wi, $pollscheduling, $userBy);
                }
                elseif ($pollscheduling->getEntityType() == 'workshopscheduling') {
                    $er_psp = Workshopapplication::addRegister($this->getDoctrine(), $evaluator->getRol(), $er_wi, $pollscheduling->getEntityId(), $userBy);
                } 
                
                $evaluateds[0] = $ev_wi->getId();
                $evaluators[0] = $er_wi->getId();
                
                if (in_array($company->getCompanyType(), array('school', 'university'))) {
                    
                    $courses[0] = $pai->getCourse();
                    $sections[0] = $pai->getSection();
                    
                    Pollapplication::addPollapplications(
                        $this->getDoctrine(), 
                        $evaluators, 
                        $evaluateds, 
                        $entityAT, 
                        $pollscheduling, 
                        $userBy,
                        $entityId,
                        array($courses, $sections),
                        $pai->getEvaluatorRol()
                    );
                }
                else {
                    // Pollapplication::addPollapplications(
                        // $this->getDoctrine(), 
                        // $evaluators, 
                        // $evaluateds, 
                        // $entityAT, 
                        // $pollscheduling, 
                        // $userBy,
                        // $entityId,
                        // NULL,
                        // $pai->getEvaluatorRol()
                    // );
                    Pollapplication::addPollapplicationsAdditional(
                        $this->getDoctrine(), 
                        $evaluators, 
                        $evaluateds, 
                        $entityAT, 
                        $pollscheduling, 
                        $userBy,
                        $entityId,
                        NULL,
                        $pai->getEvaluatorRol(),
                        $pai->getCargoGlobal(),
                        $pai->getNivelResponsabilidad()
                    );
                }
                
            }
            else {
                
                // Recover Evaluated and Evaluator
                $evaluator = $em->getRepository('FishmanPollBundle:Pollpersonimport')->find($pai->getEvaluatorId());
                
                $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
                $evaluatorEmail = $repository->createQueryBuilder('u')
                    ->where('u.email = :email 
                            AND u.numberidentity <> :numberidentity')
                    ->setParameter('email', $evaluator->getEmail())
                    ->setParameter('numberidentity', $evaluator->getNumberidentity())
                    ->getQuery()
                    ->getResult();
                if ($evaluatorEmail) {
                    $messageError .= 'El correo de evaluador lo está usando otro usuario.<br/>';
                }
                
                if ($evaluatorEmail) {
                    $session->getFlashBag()->add('error', $messageError);
                    return $this->redirect($this->generateUrl('pollapplication_import_one_by_one', array(
                        'pollschedulingid' => $pollschedulingid
                    )));
                }
                
                $er_user = User::addUserToEvaluation($this->getDoctrine(), $evaluator, $userBy);
                $er_wi = Workinginformation::addWorkinginformationToEvaluation($this->getDoctrine(), $er_user, $evaluator, $userBy);
                if ($pollscheduling->getEntityType() == 'pollscheduling') {
                    $er_psp = Pollschedulingpeople::addRegister($this->getDoctrine(), $evaluator->getRol(), $er_wi, $pollscheduling, $userBy);
                }
                elseif ($pollscheduling->getEntityType() == 'workshopscheduling') {
                    $er_psp = Workshopapplication::addRegister($this->getDoctrine(), $evaluator->getRol(), $er_wi, $pollscheduling->getEntityId(), $userBy);
                }
                
                $evaluators[0] = $er_wi->getId();
                
                if (in_array($company->getCompanyType(), array('school', 'university'))) {
                    
                    $courses[0] = $pai->getCourse();
                    $sections[0] = $pai->getSection();
                    
                    Pollapplication::addPollapplications(
                        $this->getDoctrine(), 
                        $evaluators, 
                        NULL, 
                        $entityAT, 
                        $pollscheduling, 
                        $userBy,
                        $entityId,
                        array($courses, $sections)
                    );
                }
                else {
                    Pollapplication::addPollapplications(
                        $this->getDoctrine(), 
                        $evaluators, 
                        NULL, 
                        $entityAT, 
                        $pollscheduling, 
                        $userBy,
                        $entityId
                    );
                }
                
            }
            
            $pai->setStatus(TRUE);
            $em->persist($pai);
            $em->flush();
        }
        else {
            $result = FALSE;
        }
        
        if ($this->getRequest()->isXmlHttpRequest()) {
            $response = new Response(json_encode(
                array('pai' => $result)));
            return $response;
        }
        else {
            if(!$result){
                $session->getFlashBag()->add('error', 'La evaluación no pudo ser importada.');
            }
            else {
                $session->getFlashBag()->add('status', 'La evaluación se ha importado exitosamente.');
            }
            
            return $this->redirect($this->generateUrl('pollapplication_import_one_by_one', array(
                'pollschedulingid' => $pollscheduling->getId()
            )));
            
        }
    }

    /**
     * Descartar importación de evaluaciones
     */
    public function importEvaluationDiscardAction(Request $request, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //message
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getDeleted()) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }
        
        // Form
        $form = $this->createFormBuilder(array('id' => $pollschedulingid))
            ->add('id', 'hidden')
            ->getForm();

        if ($request->isMethod('POST')) {
            
            //The user is confirmed to persist data as Companycharge entity
            $discardData = $request->request->get('form', true);
            
            if ($discardData) {
                
                // Recover Pollapplicationimports
                $pollapplicationimports = $em->getRepository('FishmanPollBundle:Pollapplicationimport')->findBy(array(
                    'pollscheduling_id' => $pollschedulingid
                ));
                
                foreach ($pollapplicationimports as $pai) {
                    
                    if ($pollscheduling->getType() != 'not_evaluateds') {
                        $evaluated = $em->getRepository('FishmanPollBundle:Pollpersonimport')->find($pai->getEvaluatedId());
                        $em->remove($evaluated);
                        $em->flush();
                    }
                    
                    $evaluator = $em->getRepository('FishmanPollBundle:Pollpersonimport')->find($pai->getEvaluatorId());
                    $em->remove($evaluator);
                    $em->flush();
                    
                    $em->remove($pai);
                    $em->flush();
                }
                
                if ($pollscheduling->getEntityType() == 'pollscheduling') {
                    return $this->redirect($this->generateUrl('pollschedulingevaluator', array(
                        'pollschedulingid' => $pollschedulingid
                    )));
                }
                else {
                    return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                        'workshopschedulingid' => $pollscheduling->getEntityId(),
                        'pollschedulingid' => $pollschedulingid
                    )));
                }

            }
        }
        
        return $this->render('FishmanPollBundle:Pollapplication:import_discard.html.twig', array(
            'form' => $form->createView(),
            'pollscheduling' => $pollscheduling,
        ));

    }
}
