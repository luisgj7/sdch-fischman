<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\PollBundle\Entity\Pollsection;
use Fishman\PollBundle\Entity\Poll;
use Fishman\PollBundle\Form\PollsectionType;

use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Pollsection controller.
 *
 */
class PollsectionController extends Controller
{
    /**
     * Lists all Pollsection entities.
     *
     */
    public function indexAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Poll Info
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        // Recovering data
        
        $parent_options = Pollsection::getPollSectionsOptions($this->getDoctrine(), $pollid);
        for ($i = 1; $i <= 50; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',  
            'parent' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('parent', 'choice', array(
                'choices' => $parent_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',  
            'parent' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollsection');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.name, psp.name parent, ps.sequence, ps.changed, ps.status, 
                      u.names, u.surname, u.lastname')
            ->leftJoin('FishmanPollBundle:Pollsection', 'psp', 'WITH', 'ps.parent_id = psp.id')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'ps.modified_by = u.id')
            ->where('ps.poll = :poll')
            ->andWhere('ps.id LIKE :id 
                    OR ps.name LIKE :name')
            ->setParameter('poll', $pollid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('ps.id', 'ASC');
        
        // Add arguments
        
        if ($data['parent'] != '') {
            $queryBuilder
                ->andWhere('ps.parent_id = :parent')
                ->setParameter('parent', $data['parent']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('ps.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('ps.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollsection');
        $paginator->setMaxPagerItems(5, 'pollsection');
        $entities = $paginator->paginate($query, 'pollsection')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollsection');
        $endPageItem = $paginator->getEndPageItem('pollsection');
        $totalItems = $paginator->getTotalItems('pollsection');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }

        return $this->render('FishmanPollBundle:Pollsection:index.html.twig', array(
            'entities' => $entities,
            'poll' => $poll,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Pollsection entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollsection');
        $result = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.name, p.id poll_id, p.title poll_title, ps.description, 
                      psp.name parent, ps.sequence, ps.changed, ps.status, ps.created, 
                      ps.changed, ps.created_by, ps.modified_by')
            ->innerJoin('ps.poll', 'p')
            ->leftJoin('FishmanPollBundle:Pollsection', 'psp', 'WITH', 'ps.parent_id = psp.id')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'ps.modified_by = u.id')
            ->where('ps.id = :pollsection')
            ->setParameter('pollsection', $id)
            ->getQuery()
            ->getResult();
        
        $entity = current($result);

        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanPollBundle:Pollsection:show.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Pollsection entity.
     *
     */
    public function newAction($pollid)
    {
        $entity = new Pollsection();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        $entity->setPoll($poll);
        $entity->setStatus(1);
        $form   = $this->createForm(new PollsectionType($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanPollBundle:Pollsection:new.html.twig', array(
            'entity' => $entity,
            'poll' => $poll,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Pollsection entity.
     *
     */
    public function createAction(Request $request,$pollid )
    {
        $entity  = new Pollsection();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($pollid);
        if (!$poll) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        $entity->setPoll($poll);
        $form = $this->createForm(new PollsectionType($entity, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();

            if ($entity->getParentId() != '-1') {
                $ps_entity = $em->getRepository('FishmanPollBundle:Pollsection')->find($entity->getParentId());
                $entity->setL1($ps_entity->getL1()); 
                $entity->setL2($ps_entity->getL2()); 
                $entity->setL3($ps_entity->getL3()); 
                $entity->setL4($ps_entity->getL4()); 
                $entity->setL5($ps_entity->getL5()); 
                $entity->setL6($ps_entity->getL6()); 
                $entity->setL7($ps_entity->getL7()); 
                $entity->setL8($ps_entity->getL8()); 
                $entity->setL9($ps_entity->getL9());
            }
            $entity->setType(1);
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setPoll($poll);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            if ($entity->getParentId() == '-1') {
                $entity->setL1($entity->getId());
            }
            else {
                $level = FALSE;
              
                if ($entity->getL2() == '') {
                    $entity->setL2($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL3() == '' && !$level) {
                    $entity->setL3($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL4() == '' && !$level) {
                    $entity->setL4($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL5() == '' && !$level) {
                    $entity->setL5($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL6() == '' && !$level) {
                    $entity->setL6($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL7() == '' && !$level) {
                    $entity->setL7($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL8() == '' && !$level) {
                    $entity->setL8($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL9() == '' && !$level) {
                    $entity->setL9($entity->getId());
                    $level = TRUE;
                }
            }
            
            $poll->setModifiedOn(TRUE);
            $em->persist($poll);
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('pollsection_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanPollBundle:Pollsection:new.html.twig', array(
            'entity' => $entity,
            'poll' => $poll,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pollsection entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollsection')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new PollsectionType($entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanPollBundle:Pollsection:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Pollsection entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollsection')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }
        
        $poll = $em->getRepository('FishmanPollBundle:Poll')->find($entity->getPoll()->getId());

        $parent_id = $entity->getParentId();
        
        $editForm = $this->createForm(new PollsectionType($entity, $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            if (!in_array($entity->getParentId(), array('-1', $parent_id))) {
                $ps_entity = $em->getRepository('FishmanPollBundle:Pollsection')->find($entity->getParentId());
                $entity->setL1($ps_entity->getL1()); 
                $entity->setL2($ps_entity->getL2()); 
                $entity->setL3($ps_entity->getL3()); 
                $entity->setL4($ps_entity->getL4()); 
                $entity->setL5($ps_entity->getL5()); 
                $entity->setL6($ps_entity->getL6()); 
                $entity->setL7($ps_entity->getL7()); 
                $entity->setL8($ps_entity->getL8()); 
                $entity->setL9($ps_entity->getL9());
            }
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $poll->setModifiedOn(TRUE);
            $em->persist($poll);
            
            $em->persist($entity);
            $em->flush();

            if ($entity->getParentId() != $parent_id) {
                // Update Pollsection to ParentId
                Pollsection::updateRegisterParentId($this->getDoctrine(), $entity);
            }
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('pollsection_show', array(
                'id' => $id
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollsection:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));

    }

    /**
     * Deletes a Pollsection entity.
     *
     */
    public function deleteAction(Request $request, $id, $pollid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $em = $this->getDoctrine()->getManager();
          
            $entity = $em->getRepository('FishmanPollBundle:Pollsection')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta.');
                return $this->redirect($this->generateUrl('pollsection', array(
                    'pollid' => $pollid
                )));
            }
            
            $poll = $em->getRepository('FishmanPollBundle:Poll')->find($entity->getPoll()->getId());
            
            $poll->setModifiedOn(TRUE);
            $em->persist($poll);
            
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('pollsection', array(
          'pollid' => $pollid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Pollsection entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Pollsection')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta.');
            return $this->redirect($this->generateUrl('poll'));
        }

        // Check if you have legacy
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollquestion');
        $pq = $repository->createQueryBuilder('pq')
            ->select('count(pq.pollsection)')
            ->where('pq.pollsection = :pollsection')
            ->setParameter('pollsection', $entity->getId())
            ->groupBy('pq.pollsection')
            ->getQuery()
            ->getResult();

        // Check if you have legacy
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingsection');
        $pss = $repository->createQueryBuilder('pss')
            ->select('count(pss.pollsection)')
            ->where('pss.pollsection = :pollsection')
            ->setParameter('pollsection', $entity->getId())
            ->groupBy('pss.pollsection')
            ->getQuery()
            ->getResult();
        
        if (current($pq) > 0 || current($pss) > 0) {
            $session->getFlashBag()->add('error', 'No se puede borrar la Sección, existen datos asociados.');
            return $this->redirect($this->generateUrl('pollquestion', array(
                'pollid' => $entity->getPoll()->getId()
            )));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Pollsection:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }
}
