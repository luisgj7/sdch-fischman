<?php

namespace Fishman\PollBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\PollBundle\Entity\Entitypoll;
use Fishman\PollBundle\Form\EntitypollType;

/**
 * Entitypoll controller.
 *
 */
class EntitypollController extends Controller
{
    /**
     * Lists all Entitypoll entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('FishmanPollBundle:Entitypoll')->findAll();

        return $this->render('FishmanPollBundle:Entitypoll:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Entitypoll entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Entitypoll entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Entitypoll:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Entitypoll entity.
     *
     */
    public function newAction()
    {
        $entity = new Entitypoll();
        $entity->setStatus(1);
        $form   = $this->createForm(new EntitypollType(), $entity);

        return $this->render('FishmanPollBundle:Entitypoll:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Entitypoll entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Entitypoll();
        $form = $this->createForm(new EntitypollType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entitypoll_show', array('id' => $entity->getId())));
        }

        return $this->render('FishmanPollBundle:Entitypoll:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Entitypoll entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Entitypoll entity.');
        }

        $editForm = $this->createForm(new EntitypollType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanPollBundle:Entitypoll:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Entitypoll entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Entitypoll entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new EntitypollType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entitypoll_edit', array('id' => $id)));
        }

        return $this->render('FishmanPollBundle:Entitypoll:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Entitypoll:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));

    }

    /**
     * Deletes a Entitypoll entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Entitypoll entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('entitypoll'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
