<?php

namespace Fishman\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\WorkshopBundle\Entity\Workshopscheduling;
use Fishman\WorkshopBundle\Entity\Workshopactivity;
use Fishman\WorkshopBundle\Entity\Workshopschedulingactivity;
use Fishman\WorkshopBundle\Entity\Workshopapplicationactivity;
use Fishman\WorkshopBundle\Entity\Workshopapplication;
use Fishman\WorkshopBundle\Form\WorkshopschedulingType;
use Fishman\WorkshopBundle\Form\WorkshoppollevaluatorimportType;
use Fishman\WorkshopBundle\Entity\Workshopdocument;
use Fishman\WorkshopBundle\Entity\Workshopschedulingdocument;
use Fishman\PollBundle\Entity\Entitypoll;
use Fishman\PollBundle\Entity\Pollquestion;
use Fishman\PollBundle\Entity\Pollscheduling;
use Fishman\PollBundle\Entity\Pollschedulingsection;
use Fishman\PollBundle\Entity\Pollschedulingquestion;
use Fishman\PollBundle\Entity\Pollapplication;
use Fishman\PollBundle\Entity\Pollpersonimport;
use Fishman\PollBundle\Entity\Pollapplicationimport;
use Fishman\PollBundle\Entity\Pollapplicationquestion;
use Fishman\PollBundle\Entity\Benchmarking;
use Fishman\AuthBundle\Entity\User;
use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\EntityBundle\Entity\Category;
use Fishman\EntityBundle\Entity\Companycourse;
use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\EntityBundle\Entity\Companycharge;
use Fishman\NotificationBundle\Entity\Notification;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Fishman\NotificationBundle\Entity\Notificationscheduling;
use Fishman\NotificationBundle\Form\NotificationschedulingType;

use Fishman\PollBundle\Form\PollschedulingType;
use Fishman\PollBundle\Form\PollschedulingsectionType;
use Fishman\PollBundle\Form\PollschedulingquestionType;
use Ideup\SimplePaginatorBundle\Paginator;

use Fishman\ImportExportBundle\Entity\Temporalimportdata;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

/**
 * Workshopscheduling controller.
 *
 */
class WorkshopschedulingController extends Controller
{
    /**
     * Lists all Workshopscheduling entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $category_options = Category::getListCategoryOptions($this->getDoctrine());
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'resolved' => '', 
            'category' => '', 
            'company' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('resolved', 'text', array(
                'required' => FALSE
            ))
            ->add('category', 'choice', array(
                'choices' => $category_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('company', 'text', array(
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'resolved' => '', 
            'category' => '', 
            'company' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling');
        $queryBuilder = $repository->createQueryBuilder('ws')
            ->select('ws.id, ws.name, ws.time_number, t.name category, c.name company, ws.initdate, ws.enddate, 
                      ws.enddate, ws.changed, ws.status, u.names, u.surname, u.lastname')
            ->innerJoin('ws.company', 'c')
            ->innerJoin('ws.workshop', 'w')
            ->innerJoin('w.category', 't')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'ws.modified_by = u.id')
            ->where('ws.deleted = :deleted')
            ->andWhere('ws.id LIKE :id 
                    OR ws.name LIKE :name')
            ->setParameter('deleted', FALSE)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('ws.id', 'ASC');
        
        // Add arguments
        
        if ($data['resolved'] != '' || $data['resolved'] === 0) {
            $queryBuilder
                ->andWhere('ws.time_number LIKE :resolved')
                ->setParameter('resolved', '%' . $data['resolved'] . '%');
        }
        if ($data['category'] != '') {
            $queryBuilder
                ->andWhere('t.id = :category')
                ->setParameter('category', $data['category']);
        }
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('c.name LIKE :company')
                ->setParameter('company', '%' . $data['company'] . '%');
        }
        if (!empty($data['initdate'])) {
            $queryBuilder
                ->andWhere('ws.initdate = :initdate')
                ->setParameter('initdate', $data['initdate']);
        }
        if (!empty($data['enddate'])) {
            $queryBuilder
                ->andWhere('ws.enddate = :enddate')
                ->setParameter('enddate', $data['enddate']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('ws.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopscheduling');
        $paginator->setMaxPagerItems(5, 'workshopscheduling');
        $entities = $paginator->paginate($query, 'workshopscheduling')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopscheduling');
        $endPageItem = $paginator->getEndPageItem('workshopscheduling');
        $totalItems = $paginator->getTotalItems('workshopscheduling');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanWorkshopBundle:Workshopscheduling:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Workshopscheduling entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling');
        $result = $repository->createQueryBuilder('ws')
            ->where('ws.id = :workshopscheduling 
                    AND ws.deleted = :deleted')
            ->setParameter('workshopscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanWorkshopBundle:Workshopscheduling:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Displays a form to create a new Workshopscheduling entity.
     *
     */
    public function newAction()
    {
        $entity = new Workshopscheduling();
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();

        //The first time, the poll are from first category
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshop');
        $workshops = $repository->createQueryBuilder('w')
             ->orderBy('w.id', 'ASC')
             ->getQuery()
             ->getResult();
             
        if (!empty($workshops)) {
    
            $entity->setStatus(1);
            $entity->setTimeNumber(0);
            $form   = $this->createForm(new WorkshopschedulingType(), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling:new.html.twig', array(
                'entity' => $entity,
                'categories' => $categories,
                'workshops' => $workshops,
                'form'   => $form->createView(),
            ));
        }
        else {
            $session->getFlashBag()->add('error', 'Debe crear un taller para poder ser programado.');

            return $this->redirect($this->generateUrl('workshop'));
        }
    }

    /**
     * Creates a new Workshopscheduling entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity  = new Workshopscheduling();
        $form = $this->createForm(new WorkshopschedulingType(), $entity);
        $form->bind($request);

        //Cloning of Workshop to Workshopscheduling

        if ($form->isValid()) {

            // Recover Datein and Dateout
            $initdate = strtotime($entity->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getEnddate()->format('Y/m/d'));

            // set flash messages
            $session = $this->getRequest()->getSession();
            
            if ($initdate < $enddate) {
              
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    if ($entity->getCustomHeaderTitle() != 1) {
                        $entity->setCustomHeaderTitleText('');
                        $entity->setCustomHeaderTitleDescription('');
                    }
                    $entity->setTimeNumber(0);
                    $entity->setCreatedBy($userBy->getId());
                    $entity->setModifiedBy($userBy->getId());
                    $entity->setCreated(new \DateTime());
                    $entity->setChanged(new \DateTime());
                    
                    //Workshop
                    $postData = $request->request->get('fishman_workshopbundle_workshopschedulingtype', null);
                    $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($postData['workshop']);
                    if (!$workshop) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
                        return $this->redirect($this->generateUrl('workshopscheduling'));
                    }
                    $entity->setWorkshop($workshop);
                    $entity->setDeleted(false);
        
                    $em->persist($entity);
                    $em->flush();
        
                    //Cloning of WorkshopActivity to Workshopschedulingactivity
                    Workshopschedulingactivity::cloneChildren($this->getDoctrine(), $entity, -1, -1, $workshop->getId(), $userBy);
        
                    //Cloning of WorkshopDocument to Workshopschedulingdocument
                    Workshopschedulingdocument::cloneRegister($this->getDoctrine(),$entity , $entity->getWorkshop()->getId(), $userBy);
        
                    //Cloning of EntityPoll to Pollscheduling 
                    Pollscheduling::cloneRegister($this->getDoctrine(),$entity , $entity->getWorkshop()->getId(), $userBy);
                  
                    //Cloning of Notification to Notificationscheduling 
                    Notificationscheduling::cloneChildren($this->getDoctrine(), -1, -1, $entity, 'workshopscheduling', $workshop->getId(), 'workshop', $userBy);
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('workshopscheduling_show', array(
                    'id' => $entity->getId()
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_category = $categories[0];
        $current_workshop = '';

        //The first time, the poll are from first category
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshop');
        $workshops = $repository->createQueryBuilder('w')
             ->where('w.category = :category_id')
             ->setParameter('category_id', $current_category->getId())
             ->orderBy('w.id', 'ASC')
             ->getQuery()
             ->getResult();
             
        if (!empty($workshops)) {
            $current_workshop = $workshops[0];
        }

        return $this->render('FishmanWorkshopBundle:Workshopscheduling:new.html.twig', array(
            'entity' => $entity,
            'categories' => $categories,
            'workshops' => $workshops,
            'current_category' => $current_category,
            'current_workshop' => $current_workshop,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Workshopscheduling entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling');
        $result = $repository->createQueryBuilder('ws')
            ->where('ws.id = :workshopscheduling 
                    AND ws.deleted = :deleted')
            ->setParameter('workshopscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_category = $entity->getWorkshop()->getCategory();
        
        //The first time, the poll are from first category
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshop');
        $workshops = $repository->createQueryBuilder('w')
             ->where('w.category = :category_id')
             ->setParameter('category_id', $current_category->getId())
             ->orderBy('w.id', 'ASC')
             ->getQuery()
             ->getResult();
        $current_workshop = $entity->getWorkshop();
        $disabled = true;
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new WorkshopschedulingType(), $entity);

        return $this->render('FishmanWorkshopBundle:Workshopscheduling:edit.html.twig', array(
            'entity'      => $entity,
            'categories' => $categories,
            'workshops' => $workshops,
            'disabled' => $disabled,
            'current_workshop' => $current_workshop,
            'current_category' => $current_category,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Workshopscheduling entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        $dataEntity = array(
            'company' => $entity->getCompany()
        );
        
        $editForm = $this->createForm(new WorkshopschedulingType(), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {

            // Recover Datein and Dateout
            $initdate = strtotime($entity->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getEnddate()->format('Y/m/d'));
            
            if ($initdate < $enddate) {
                
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                $entity->setCompany($dataEntity['company']);
                
                if ($entity->getCustomHeaderTitle() != 1) {
                    $entity->setCustomHeaderTitleText('');
                    $entity->setCustomHeaderTitleDescription('');
                }
                $entity->setModifiedBy($modifiedBy->getId());
                $entity->setChanged(new \DateTime());
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('workshopscheduling_show', array(
                    'id' => $id
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_category = $entity->getWorkshop()->getCategory();
        //The first time, the poll are from first category
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshop');
        $workshops = $repository->createQueryBuilder('w')
             ->where('w.category = :category_id')
             ->setParameter('category_id', $current_category->getId())
             ->orderBy('w.id', 'ASC')
             ->getQuery()
             ->getResult();
        $current_workshop = $entity->getWorkshop();
        $disabled = true;
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanWorkshopBundle:Workshopscheduling:edit.html.twig', array(
            'entity'      => $entity,
            'categories' => $categories,
            'workshops' => $workshops,
            'disabled' => $disabled,
            'current_workshop' => $current_workshop,
            'current_category' => $current_category,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Workshopscheduling entity.
     *
     */
    public function dropAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling');
        $result = $repository->createQueryBuilder('ws')
            ->where('ws.id = :workshopscheduling 
                    AND ws.deleted = :deleted')
            ->setParameter('workshopscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
          
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanWorkshopBundle:Workshopscheduling:drop.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a Workshopscheduling entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {    
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        if ($form->isValid()) {
            
            //Delete Workshopapplication to Workshopscheduling
            Workshopapplication::deleteRegisters($this->getDoctrine(), $entity);
            
            //Delete Pollscheduling to Workshopscheduling
            Pollscheduling::deleteRegisters($this->getDoctrine(), 'workshopscheduling', $entity);
            
            $entity->setDeleted(TRUE);

            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');

        }

        return $this->redirect($this->generateUrl('workshopscheduling'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Lists all Workshopschedulingpoll entities.
     *
     */
    public function indexPollAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Recovering data
            
            $category_options = Category::getListCategoryOptions($this->getDoctrine());
            $benchmarking_options = Benchmarking::getListBenchmarkingOptions($this->getDoctrine());
            for ($i = 1; $i<= 20; $i++) {
                $sequence_options[$i] = $i;
            }
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'resolved' => '', 
                'category' => '', 
                'benchmarking' => '', 
                'psc_period' => '', 
                'initdate' => NULL, 
                'enddate' => NULL, 
                'sequence' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('resolved', 'text', array(
                    'required' => FALSE
                ))
                ->add('category', 'choice', array(
                    'choices' => $category_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('benchmarking', 'choice', array(
                    'choices' => $benchmarking_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('psc_period', 'text', array(
                    'required' => FALSE
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('enddate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('sequence', 'choice', array(
                    'choices' => $sequence_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'resolved' => '', 
                'category' => '', 
                'benchmarking' => '', 
                'psc_period' => '', 
                'initdate' => NULL, 
                'enddate' => NULL, 
                'sequence' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Query
                
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
            $queryBuilder = $repository->createQueryBuilder('psc')
                ->select('psc.id, p.id poll, psc.title, psc.version, psc.entity_type, psc.entity_id, psc.time_number, c.name category, 
                          c.name company, psc.initdate, psc.enddate, b.name benchmarking, psc.pollscheduling_period psc_period, 
                          psc.sequence, psc.own, psc.changed, psc.status, u.names, u.surname, u.lastname')
                ->innerJoin('psc.poll', 'p')
                ->innerJoin('p.category', 'c')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'psc.modified_by = u.id')
                ->leftJoin('psc.benchmarking', 'b')
                ->where('psc.entity_type = :entity_type 
                        AND psc.entity_id = :entity_id 
                        AND psc.deleted = :deleted')
                ->andWhere('psc.id LIKE :id 
                        OR psc.title LIKE :title 
                        OR psc.version LIKE :version ')
                ->setParameter('entity_type', 'workshopscheduling')
                ->setParameter('entity_id', $workshopschedulingid)
                ->setParameter('deleted', FALSE)
                ->setParameter('id', '%' . $data['word'] . '%')
                ->setParameter('title', '%' . $data['word'] . '%')
                ->setParameter('version', '%' . $data['word'] . '%')
                ->orderBy('psc.id', 'ASC');
        
        // Add arguments
        
        if ($data['resolved'] != '' || $data['resolved'] === 0) {
            $queryBuilder
                ->andWhere('psc.time_number LIKE :resolved')
                ->setParameter('resolved', '%' . $data['resolved'] . '%');
        }
        if ($data['category'] != '') {
            $queryBuilder
                ->andWhere('c.id = :category')
                ->setParameter('category', $data['category']);
        }
        if ($data['benchmarking'] != '') {
            $queryBuilder
                ->andWhere('b.id = :benchmarking')
                ->setParameter('benchmarking', $data['benchmarking']);
        }
        if ($data['psc_period'] != '') {
            $queryBuilder
                ->andWhere('psc.pollscheduling_period LIKE :psc_period')
                ->setParameter('psc_period', '%' . $data['psc_period'] . '%');
        }
        if (!empty($data['initdate'])) {
            $queryBuilder
                ->andWhere('ps.initdate = :initdate')
                ->setParameter('initdate', $data['initdate']);
        }
        if (!empty($data['enddate'])) {
            $queryBuilder
                ->andWhere('ps.enddate = :enddate')
                ->setParameter('enddate', $data['enddate']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('psc.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('psc.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workshopschedulingpoll');
            $paginator->setMaxPagerItems(5, 'workshopschedulingpoll');
            $entities = $paginator->paginate($query, 'workshopschedulingpoll')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workshopschedulingpoll');
            $endPageItem = $paginator->getEndPageItem('workshopschedulingpoll');
            $totalItems = $paginator->getTotalItems('workshopschedulingpoll');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll:index.html.twig', array(
                'entities' => $entities,
                'workshopscheduling' => $workshopscheduling,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Finds and displays a Workshopschedulingpoll entity.
     *
     */
    public function showPollAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('psc')
            ->select('psc.id, psc.title, psc.custom_header_logo, psc.entity_type, t.name category, psc.version,
                      psc.custom_header_title, psc.custom_header_title_text, psc.custom_header_title_description, 
                      psc.description, c.name company, psc.type, psc.multiple_evaluation, b.name benchmarking, 
                      psc.pollscheduling_period psc_period, psc.level, psc.alignment, psc.divide, psc.number_question, 
                      psc.sequence_question, psc.duration, psc.period, psc.initdate, psc.enddate, 
                      psc.terms, psc.use_terms, psc.message_gratitude, psc.message_end, 
                      psc.message_inactive, psc.access_view_number, psc.status_bar, psc.counselor_report, 
                      psc.pollscheduling_anonymous anonymous, pscr.title psc_relation, 
                      pscr.pollscheduling_period psc_relation_period, psc.sequence, psc.status, psc.time_number, 
                      psc.own, psc.created, psc.changed, psc.created_by, psc.modified_by,  
                      wsc.id workshopscheduling_id, wsc.name workshopscheduling_name, wsc.deleted workshopscheduling_deleted')
            ->innerJoin('psc.poll', 'p')
            ->innerJoin('FishmanWorkshopBundle:Workshopscheduling', 'wsc', 'WITH', 'psc.entity_id = wsc.id')
            ->innerJoin('p.category', 't')
            ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'psc.company_id = c.id')
            ->leftJoin('psc.benchmarking', 'b')
            ->leftJoin('FishmanPollBundle:Pollscheduling', 'pscr', 'WITH', 'psc.pollschedulingrelation_id = pscr.id')
            ->where('psc.id = :pollscheduling 
                    AND psc.deleted = :deleted')
            ->setParameter('pollscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        
        if (!$entity['workshopscheduling_deleted']) {
        
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopscheduling'));
            }
    
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll:show.html.twig', array(
                'entity' => $entity,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Workshopschedulingpoll entity.
     *
     */
    public function newPollAction($workshopschedulingid)
    {
        $entity = new Pollscheduling();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Recover Polls
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
            $polls = $repository->createQueryBuilder('p')
                 ->where('p.current_version = :current_version')
                 ->setParameter('current_version', 1)
                 ->orderBy('p.id', 'ASC')
                 ->getQuery()
                 ->getResult();
            
            if (!empty($polls)) {
                
                // Recover Categories
                $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
                $categories = $repository->createQueryBuilder('c')
                     ->where('c.status = 1')
                     ->orderBy('c.name', 'ASC')
                     ->getQuery()
                     ->getResult();
                
                // Recover Benchmarkings
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
                $benchmarkings = $repository->createQueryBuilder('b')
                     ->where('b.status = 1')
                     ->orderBy('b.name', 'ASC')
                     ->getQuery()
                     ->getResult();
                
                // Recover Pollscheduling relations
                $pollschedulingrelations = Pollscheduling::getListPollschedulingrelationsByCompany(
                    $this->getDoctrine(),
                    $workshopscheduling->getCompany()->getId(),
                    '',
                    'workshopscheduling',
                    $workshopscheduling->getId()
                );
                
                $entity->setMultipleEvaluation(0);
                $entity->setPollschedulingAnonymous(0);
                $entity->setStatus(TRUE);
                
                $form = $this->createForm(new PollschedulingType($entity, false, $this->getDoctrine()), $entity);
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll:new.html.twig', array(
                    'entity' => $entity,
                    'workshopscheduling' => $workshopscheduling,
                    'categories' => $categories,
                    'benchmarkings' => $benchmarkings,
                    'pollschedulingrelations' => $pollschedulingrelations,
                    'form' => $form->createView(),
                ));
            }
            else {
                $session->getFlashBag()->add('error', 'Debe crear una encuesta para poder ser programada.');
    
                return $this->redirect($this->generateUrl('poll', array()));
            }
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Creates a new Workshopschedulingpoll entity.
     *
     */
    public function createPollAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        $entity  = new Pollscheduling();
        
        $form = $this->createForm(new PollschedulingType($entity, false, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            // Recover Initdate and Enddate Workshopscheduling
            $initdateWS = strtotime($workshopscheduling->getInitdate()->format('Y/m/d'));
            $enddateWS = strtotime($workshopscheduling->getEnddate()->format('Y/m/d'));
            
            // Recover Datein and Dateout
            $initdate = strtotime($entity->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getEnddate()->format('Y/m/d'));

            // set flash messages
            $session = $this->getRequest()->getSession();
            
            if ($initdate < $enddate) {
                
                $message = '';
                if ($initdate < $initdateWS) {
                    $message .= 'La Fecha de Inicio de la encuesta no puede ser menor a la Fecha de Inicio de la Programación de Taller.' . "</br>";
                }
                if ($enddate > $enddateWS) {
                    $message .= 'La Fecha de Fin de la encuesta no puede ser mayor a la Fecha de Fin de la Programación de Taller.' . "</br>";
                }
                
                if ($message) {
                    $session->getFlashBag()->add('error', $message);
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_new', array(
                        'workshopschedulingid' => $workshopschedulingid
                    )));
                }
                
                // PostData
                $postData = $request->request->get('fishman_pollbundle_pollschedulingtype', 'default value if bar does not exist');
                
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                // Recover Poll
                $poll = $em->getRepository('FishmanPollBundle:Poll')->find($postData['poll_id']);
    
                // Total assigned polls to a company
                $pollschedulingCompany = $em->createQuery('SELECT COUNT(psc.id) num 
                        FROM FishmanPollBundle:Pollscheduling psc
                        WHERE psc.poll = :poll 
                        AND psc.company_id = :company')
                    ->setParameter('poll', $poll->getId())
                    ->setParameter('company', $entity->getCompanyId())
                    ->getSingleResult();
                $time_number = $pollschedulingCompany['num'] + 1;
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    $entity->setEntityType('workshopscheduling');
                    $entity->setEntityId($workshopscheduling->getId());
                    $entity->setPoll($poll);
                    $entity->setTitle($poll->getTitle());
                    $entity->setVersion($poll->getVersion());
                    $entity->setLevel($poll->getLevel());
                    $entity->setDivide($poll->getDivide());
                    $entity->setNumberQuestion($poll->getNumberQuestion());
                    $entity->setSequenceQuestion($poll->getSequenceQuestion());
                    $entity->setCurrentVersion($poll->getCurrentVersion());
                    $entity->setTimeNumber($time_number);
                    $entity->setOriginalId($poll->getOriginalId());
                    $entity->setCompanyId($workshopscheduling->getCompany()->getId());
                    if ($entity->getType() == 'self_evaluation') {
                        $entity->setMultipleEvaluation(FALSE);
                        $entity->setAccessViewNumber(1);
                    }
                    if ($entity->getType() == 'not_evaluateds') {
                        $entity->setMultipleEvaluation(FALSE);
                        $entity->setAccessViewNumber(1);
                        $entity->setPollschedulingrelationId(NULL);
                    }
                    if ($entity->getMultipleEvaluation()) {
                        $entity->setStatusBar('question');
                        $entity->setCounselorReport(0);
                        $entity->setAccessViewNumber(1);
                        $entity->setPollschedulingrelationId('');
                        $entity->setAlignment('vertical');
                    }
                    if ($postData['benchmarking_id'] != '') {
                        // Recover Benchmarking
                        $benchmarking = $em->getRepository('FishmanPollBundle:Benchmarking')->find($postData['benchmarking_id']);
                        $entity->setBenchmarking($benchmarking);
                    }
                    if ($entity->getUseTerms() == NULL) {
                      $entity->setUseTerms(FALSE);
                    }
                    if ($entity->getCustomHeaderTitle() != 1) {
                        $entity->setCustomHeaderTitleText('');
                        $entity->setCustomHeaderTitleDescription('');
                    }
                    $entity->setOwn(TRUE);
                    $entity->setDeleted(FALSE);
                    $entity->setCreatedBy($userBy->getId());
                    $entity->setModifiedBy($userBy->getId());
                    $entity->setCreated(new \DateTime());
                    $entity->setChanged(new \DateTime());
                    
                    $em->persist($entity);
                    $em->flush();
                
                    // Operations Pollscheduling
                    Pollscheduling::operationsPollscheduling($this->getDoctrine(), $entity, $userBy);
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('workshopschedulingpoll_show', array(
                    'id' => $entity->getId()
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        
        // Recover Benchmarkings
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
        $benchmarkings = $repository->createQueryBuilder('b')
             ->where('b.status = 1')
             ->orderBy('b.name', 'ASC')
             ->getQuery()
             ->getResult();
                
        // Recover Pollscheduling relations
        $pollschedulingrelations = Pollscheduling::getListPollschedulingrelationsByCompany(
            $this->getDoctrine(),
            $workshopscheduling->getCompany()->getId(),
            $entity->getId(),
            'workshopscheduling',
            $workshopscheduling->getId()
        );

        return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll:new.html.twig', array(
            'entity' => $entity,
            'workshopscheduling' => $workshopscheduling,
            'categories' => $categories,
            'benchmarkings' => $benchmarkings,
            'pollschedulingrelations' => $pollschedulingrelations,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Workshopschedulingpoll entity.
     *
     */
    public function editPollAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('ps')
            ->where("ps.id = :pollscheduling 
                    AND ps.deleted = :deleted
                    AND ps.entity_type = 'workshopscheduling'")
            ->setParameter('pollscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            if ($entity->getEntitytype() == "workshopscheduling") {
                
                // Recover Categories
                $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
                $categories = $repository->createQueryBuilder('c')
                     ->where('c.status = 1')
                     ->orderBy('c.name', 'ASC')
                     ->getQuery()
                     ->getResult();
                $current_category = $entity->getPoll()->getCategory();
                
                // Recover polls
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
                $polls = $repository->createQueryBuilder('p')
                     ->where('p.category = :category_id 
                            AND p.current_version = :current_version')
                     ->setParameter('category_id', $current_category->getId())
                     ->setParameter('current_version', 1)
                     ->orderBy('p.id', 'ASC')
                     ->getQuery()
                     ->getResult();
                $current_poll = $entity->getPoll();
                
                // Recover Benchmarkings
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
                $benchmarkings = $repository->createQueryBuilder('b')
                     ->where('b.status = 1')
                     ->orderBy('b.name', 'ASC')
                     ->getQuery()
                     ->getResult();
                $current_benchmarking = $entity->getBenchmarking();
                
                // Recover Pollscheduling relations
                $pollschedulingrelations = Pollscheduling::getListPollschedulingrelationsByCompany(
                    $this->getDoctrine(),
                    $entity->getCompanyId(),
                    $entity->getId(),
                    'workshopscheduling',
                    $workshopscheduling->getId()
                );
                $current_pollschedulingrelation = $entity->getPollschedulingrelationId();
        
                // User
                $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
                $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
                $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
                $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
                
                $editForm = $this->createForm(new PollschedulingType($entity, true, $this->getDoctrine()), $entity);
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll:edit.html.twig', array(
                    'entity' => $entity, 
                    'workshopscheduling' => $workshopscheduling, 
                    'categories' => $categories,
                    'polls' => $polls,
                    'benchmarkings' => $benchmarkings,
                    'pollschedulingrelations' => $pollschedulingrelations,
                    'current_category' => $current_category,
                    'current_poll' => $current_poll,
                    'current_benchmarking' => $current_benchmarking,
                    'current_pollschedulingrelation' => $current_pollschedulingrelation,
                    'createdByName' => $createdByName,
                    'modifiedByName' => $modifiedByName,
                    'edit_form'   => $editForm->createView()
                ));
                
            }
            else {
                $session->getFlashBag()->add('error', 'Esta encuesta no pertenece a un taller, ingrese a traves de la sección programación de encuestas.');
    
                return $this->redirect($this->generateUrl('workshopscheduling'));
            }
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Edits an existing Workshopschedulingpoll entity.
     *
     */
    public function updatePollAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        
        $editForm = $this->createForm(new PollschedulingType($entity, true, $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // Recover Initdate and Enddate Workshopscheduling
            $initdateWS = strtotime($workshopscheduling->getInitdate()->format('Y/m/d'));
            $enddateWS = strtotime($workshopscheduling->getEnddate()->format('Y/m/d'));
            
            // Recover Datein and Dateout
            $initdate = strtotime($entity->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getEnddate()->format('Y/m/d'));
            
            if ($initdate < $enddate) {
                
                $message = '';
                if ($initdate < $initdateWS) {
                    $message .= 'La Fecha de Inicio de la encuesta no puede ser menor a la Fecha de Inicio de la Programación de Taller.' . "</br>";
                }
                if ($enddate > $enddateWS) {
                    $message .= 'La Fecha de Fin de la encuesta no puede ser mayor a la Fecha de Fin de la Programación de Taller.' . "</br>";
                }
                
                if ($message) {
                    $session->getFlashBag()->add('error', $message);
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_edit', array(
                        'id' => $entity->getId()
                    )));
                }
                
                // PostData
                $postData = $request->request->get('fishman_pollbundle_pollschedulingtype', 'default value if bar does not exist');
                
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                if ($entity->getCustomHeaderTitle() != 1) {
                    $entity->setCustomHeaderTitleText('');
                    $entity->setCustomHeaderTitleDescription('');
                }
                if ($entity->getType() == 'self_evaluation') {
                    $entity->setMultipleEvaluation(FALSE);
                    $entity->setAccessViewNumber(0);
                }
                if ($entity->getType() == 'not_evaluateds') {
                    $entity->setMultipleEvaluation(FALSE);
                    $entity->setAccessViewNumber(0);
                    $entity->setPollschedulingrelationId(NULL);
                }
                if ($entity->getMultipleEvaluation()) {
                    $entity->setStatusBar('question');
                    $entity->setCounselorReport(0);
                    $entity->setAccessViewNumber(1);
                    $entity->setPollschedulingrelationId('');
                    $entity->setAlignment('vertical');
                }
                if ($postData['benchmarking_id'] != '') {
                    // Recover Benchmarking
                    $benchmarking = $em->getRepository('FishmanPollBundle:Benchmarking')->find($postData['benchmarking_id']);
                    $entity->setBenchmarking($benchmarking);
                }
                $entity->setModifiedBy($modifiedBy->getId());
                $entity->setChanged(new \DateTime());
                
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('workshopschedulingpoll_show', array(
                    'id' => $id
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_category = $entity->getPoll()->getCategory();
        
        // Recover Polls
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
        $polls = $repository->createQueryBuilder('p')
             ->where('p.category = :category_id 
                      AND p.current_version = :current_version')
             ->setParameter('category_id', $current_category->getId())
             ->setParameter('current_version', 1)
             ->orderBy('p.id', 'ASC')
             ->getQuery()
             ->getResult();
        $current_poll = $entity->getPoll();
        
        // Recover Benchmarkings
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Benchmarking');
        $benchmarkings = $repository->createQueryBuilder('b')
             ->where('b.status = 1')
             ->orderBy('b.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_benchmarking = $entity->getBenchmarking();
        
        // Recover Pollscheduling relations
        $pollschedulingrelations = Pollscheduling::getListPollschedulingrelationsByCompany(
            $this->getDoctrine(),
            $entity->getCompanyId(),
            $entity->getId(),
            'workshopscheduling',
            $workshopscheduling->getId()
        );
        $current_pollschedulingrelation = $entity->getPollschedulingrelationId();

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll:edit.html.twig', array(
            'entity' => $entity, 
            'workshopscheduling' => $workshopscheduling, 
            'categories' => $categories,
            'polls' => $polls,
            'benchmarkings' => $benchmarkings,
            'pollschedulingrelations' => $pollschedulingrelations,
            'current_category' => $current_category,
            'current_poll' => $current_poll,
            'current_benchmarking' => $current_benchmarking,
            'current_pollschedulingrelation' => $current_pollschedulingrelation,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Workshopschedulingpoll entity.
     *
     */
    public function dropPollAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.version, ps.initdate, ps.enddate, ps.own, 
                      ws.id workshopscheduling_id, ws.name workshopscheduling_name, ws.deleted workshopscheduling_deleted')
            ->innerJoin('ps.poll', 'p')
            ->innerJoin('FishmanWorkshopBundle:Workshopscheduling', 'ws', 'WITH', 'ps.entity_id = ws.id')
            ->where('ps.id = :pollscheduling')
            ->setParameter('pollscheduling', $id)
            ->getQuery()
            ->getResult();
            
        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$entity['workshopscheduling_deleted']) {
          
            $deleteForm = $this->createDeleteForm($id);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll:drop.html.twig', array(
                'entity'      => $entity,
                'delete_form' => $deleteForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Deletes a Workshopschedulingpoll entity.
     *
     */
    public function deletePollAction(Request $request, $id)
    {    
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
          
        // Recover Pollscheduling
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
        $result = $repository->createQueryBuilder('ps')
            ->where('ps.id = :pollscheduling 
                    AND ps.deleted = :deleted')
            ->setParameter('pollscheduling', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if ($form->isValid()) {
            
            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
            $pscr = $repository->createQueryBuilder('psc')
                ->select('count(psc.pollschedulingrelation_id)')
                ->where('psc.pollschedulingrelation_id = :pollschedulingrelation')
                ->setParameter('pollschedulingrelation', $entity->getId())
                ->groupBy('psc.pollschedulingrelation_id')
                ->getQuery()
                ->getResult();
            
            if (count($pscr) == 0) {
            
                //Delete Pollapplication to Pollscheduling
                Pollapplication::deleteRegisters($this->getDoctrine(), $entity);
                
                $entity->setDeleted(TRUE);
    
                $em->persist($entity);
                $em->flush();
                                                      
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'La Encuesta está relacionada a otra programación de encuesta.');
            }

        }

        return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
            'workshopschedulingid' => $entity->getEntityId()
        )));
    }

    /**
     * Preview Workshopschedulingpoll entity.
     *
     */
    public function previewPollAction(Request $request, $id, $page)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $array_questions = '';
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('pollscheduling'));
        }

        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            if ($entity->getEntitytype() == "workshopscheduling") {
                
                $array_questions = '';
                
                if ($request->request->get('send', false)) {
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_preview_message', array(
                        'id' => $id
                    )));
                }
                
                // Paginator
        
                $array_paginator = Pollapplicationquestion::getPaginator($this->getDoctrine(), $entity, $page);
                
                if ($array_paginator['number_pagers'] > 0 && $page > $array_paginator['number_pagers']) {
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_preview', array(
                        'id' => $id,
                        'page' => $array_paginator['number_pagers']
                    )));
                }
        
                // Recover Questions in page current
                
                $array['sections'] = $entity->getSerialized();
                
                if ($entity->getDivide() == 'sections_show') {
                    $array_questions = Pollschedulingsection::getArraySectionShow($this->getDoctrine(), $array, $entity->getId(), $page);
                }
                else if ($entity->getDivide() == 'number_questions') {
                    $array_questions = Pollschedulingsection::getArrayNumberQuestions($this->getDoctrine(), $array['sections'], $entity->getId(), $page, $entity->getNumberQuestion());
                }
                else if ($entity->getDivide() == 'none') {
                    $array_questions = $array;
                }
                
                if ($request->request->get('first', false)) {
                    $page = 1;
                }
                if ($request->request->get('prev', false)) {
                    $page--;
                }
                if ($request->request->get('page')) {
                    $page = $request->request->get('page');
                }
                if ($request->request->get('next', false)) {
                    $page++;
                }
                if ($request->request->get('last', false)) {
                    $page = $array_paginator['number_pagers'];
                }
                
                if ($request->request->get('page') || $request->request->get('first', false) || $request->request->get('prev', false) || $request->request->get('next', false) || $request->request->get('last', false)) {
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_preview', array(
                        'id' => $id,
                        'page' => $page
                    )));
                }
                
                // Array Parameters
                
                $array_parameters = array(
                    'entity' => $entity,
                    'workshopscheduling' => $workshopscheduling,
                    'page' => $page,
                    'paginator' => $array_paginator['paginator'],
                    'lastpage' => $array_paginator['number_pagers'],
                    'alignment' => 'vertical',
                    'questions' => $array_questions,
                    'preview_multiple' => FALSE
                );
                
                if ($entity->getAlignment() == 'horizontal') {
                    $array_parameters['alignment'] = 'horizontal';
                }
                
                if ($entity->getMultipleEvaluation()) {
                    $array_parameters['preview_multiple'] = array(
                        0 => array('id' => 1, 'name' => 'Evaluado N°1', 'answer' => ''),
                        1 => array('id' => 2, 'name' => 'Evaluado N°2', 'answer' => ''),
                        2 => array('id' => 3, 'name' => 'Evaluado N°3', 'answer' => '')
                    );
                }
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Preview:resolved.html.twig', $array_parameters);
                
            }
            else {
                $session->getFlashBag()->add('error', 'Esta encuesta no pertenece a un taller, ingrese a traves de la sección programación de encuestas.');
    
                return $this->redirect($this->generateUrl('workshopscheduling'));
            }
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Preview Workshopschedulingpoll Terms.
     *
     */
    public function previewTermsPollAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            if ($entity->getEntitytype() == "workshopscheduling") {
                
                if (!$entity->getUseTerms()) {
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_preview', array(
                        'id' => $id
                    )));
                }
                
                // Array Parameters
        
                $array_parameters = array(
                    'entity' => $entity,
                    'workshopscheduling' => $workshopscheduling,
                    'terms' => $entity->getTerms()
                );
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Preview:terms.html.twig', $array_parameters);
                
            }
            else {
                $session->getFlashBag()->add('error', 'Esta encuesta no pertenece a un taller, ingrese a traves de la sección programación de encuestas.');
    
                return $this->redirect($this->generateUrl('workshopscheduling'));
            }
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Preview Workshopschedulingpoll Message.
     *
     */
    public function previewMessagePollAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover Pollscheduling
        $entity = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            if ($entity->getEntitytype() == "workshopscheduling") {
                
                // Array Parameters
        
                $array_parameters = array(
                    'entity' => $entity,
                    'workshopscheduling' => $workshopscheduling,
                    'message' => $entity->getMessageGratitude()
                );
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Preview:message.html.twig', $array_parameters);
                
            }
            else {
                $session->getFlashBag()->add('error', 'Esta encuesta no pertenece a un taller, ingrese a traves de la sección programación de encuestas.');
    
                return $this->redirect($this->generateUrl('workshopscheduling'));
            }
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Export Pollscheduling Links.
     *
     */
    public function exportLinksPollAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
            
            $router = $this->get('router');
            $doctrine = $this->getDoctrine();
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($id);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
                return $this->redirect($this->generateUrl('pollscheduling'));
            }
        
            // Recover Workshopscheduling
            $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($pollscheduling->getEntityId());
            if (!$workshopscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopscheduling'));
            }
            
            if (!$workshopscheduling->getDeleted()) {
                
                // Recover Company
                $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
                
                $phpExcel = new PHPExcel();
                $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
                $phpExcel->getProperties()->setLastModifiedBy("");
                $phpExcel->getProperties()->setTitle("Rutas de Evaluaciones por Persona");
                $phpExcel->getProperties()->setSubject("");
                $phpExcel->getProperties()->setDescription("Muesta una lista de todas las personas con sus rutas de acceso para la lista de evaluacciones pendientes a resolver.");
        
                $phpExcel->setActiveSheetIndex(0);
                $phpExcel->getActiveSheet()->setTitle('Rutas de Evaluaciones');
        
                $worksheet = $phpExcel->getSheet(0);
                $worksheet->getCellByColumnAndRow(1, 2)->setValue('Rutas de Evaluaciones');
                $worksheet->getCellByColumnAndRow(1, 3)->setValue('Taller: ' . $workshopscheduling->getName());
                $worksheet->getCellByColumnAndRow(1, 4)->setValue('Encuesta: ' . $pollscheduling->getTitle() . ' | ' 
                                                                               . $pollscheduling->getVersion() . ' | ' 
                                                                               . $workshopscheduling->getCompany()->getName() . ' | ' 
                                                                               . $pollscheduling->getPollschedulingPeriod());
                
                $pathEvaluations = Pollscheduling::getPollListLinks($doctrine, $router, $pollscheduling);
                
                Pollscheduling::generateReportPathEvaluations($pathEvaluations, $worksheet, $phpExcel, $company, 7);
                
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="taller-encuesta-personas-evaluaciones.xls"');
                header('Cache-Control: max-age=0');
                
                $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
                $writer->save('php://output');
                
            }
        }
        exit;
    }
    
    /**
     * Lists all Pollapplication entities.
     *
     */
    public function indexPollEvaluatorAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            elseif ($pollscheduling->getType() == 'not_evaluateds') {
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds_new', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            if (!$pollscheduling->getDeleted()) {
                
                // Recovering data
                
                $rol_options = Pollapplication::getListRolOptions($this->getDoctrine());
                
                // Find Entities
                
                $defaultData = array(
                    'evaluated_code' => '', 
                    'evaluated_names' => '', 
                    'evaluated_surname' => '', 
                    'evaluated_lastname' => '', 
                    'evaluator_code' => '', 
                    'evaluator_names' => '', 
                    'evaluator_surname' => '', 
                    'evaluator_lastname' => '', 
                    'evaluator_rol' => ''
                );
                $formData = array();
                $form = $this->createFormBuilder($defaultData)
                    ->add('evaluated_code', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluated_names', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluated_surname', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluated_lastname', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_code', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_names', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_surname', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_lastname', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_rol', 'choice', array(
                        'choices' => $rol_options, 
                        'empty_value' => 'Choose an option',
                        'required' => FALSE
                    ))
                    ->getForm();
        
                $data = array(
                    'evaluated_code' => '', 
                    'evaluated_names' => '', 
                    'evaluated_surname' => '', 
                    'evaluated_lastname' => '', 
                    'evaluator_code' => '', 
                    'evaluator_names' => '', 
                    'evaluator_surname' => '', 
                    'evaluator_lastname' => '', 
                    'evaluator_rol' => ''
                );
                if (isset($_GET['form'])) {
                    $formData = $_GET['form'];
                }
                if ($request->getMethod() == 'GET') {
                    $form->bindRequest($request);
                    $data = $form->getData();
                }
                
                // Delete registers
                $deletes_form = $this->createFormBuilder()
                    ->add('all_select', 'choice', array(
                        'required' => false, 
                        'multiple' => true, 
                        'expanded' => true
                    ))
                    ->add('registers', 'choice', array(
                        'required' => false, 
                        'multiple' => true, 
                        'expanded' => true
                    ))
                    ->getForm();
                    
                // Query
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                $queryBuilder = $repository->createQueryBuilder('pa')
                    ->select('pa.id, wi_one.code evaluated_code, u_one.names evaluated_names, u_one.surname evaluated_surname, 
                              u_one.lastname evaluated_lastname, wi_two.code evaluator_code, u_two.names evaluator_names, 
                              u_two.surname evaluator_surname, u_two.lastname evaluator_lastname, pa.evaluator_rol, 
                              pa.changed, u.names, u.surname, u.lastname')
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
                    ->innerJoin('wi_one.user', 'u_one')
                    ->innerJoin('wi_two.user', 'u_two')
                    ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pa.modified_by = u.id')
                    ->where('pa.pollscheduling = :pollscheduling 
                            AND pa.deleted = :deleted')
                    ->setParameter('pollscheduling', $pollschedulingid)
                    ->setParameter('deleted', FALSE)
                    ->orderBy('pa.id', 'ASC');
            
                // Add arguments
                
                if ($data['evaluated_code'] != '') {
                    $queryBuilder
                        ->andWhere('wi_one.code LIKE :evaluated_code')
                        ->setParameter('evaluated_code', '%' . $data['evaluated_code'] . '%');
                }
                if ($data['evaluated_names'] != '') {
                    $queryBuilder
                        ->andWhere('u_one.names LIKE :evaluated_names')
                        ->setParameter('evaluated_names', '%' . $data['evaluated_names'] . '%');
                }
                if ($data['evaluated_surname'] != '') {
                    $queryBuilder
                        ->andWhere('u_one.surname LIKE :evaluated_surname')
                        ->setParameter('evaluated_surname', '%' . $data['evaluated_surname'] . '%');
                }
                if ($data['evaluated_lastname'] != '') {
                    $queryBuilder
                        ->andWhere('u_one.lastname LIKE :evaluated_lastname')
                        ->setParameter('evaluated_lastname', '%' . $data['evaluated_lastname'] . '%');
                }
                if ($data['evaluator_code'] != '') {
                    $queryBuilder
                        ->andWhere('wi_two.code LIKE :evaluator_code')
                        ->setParameter('evaluator_code', '%' . $data['evaluator_code'] . '%');
                }
                if ($data['evaluator_names'] != '') {
                    $queryBuilder
                        ->andWhere('u_two.names LIKE :evaluator_names')
                        ->setParameter('evaluator_names', '%' . $data['evaluator_names'] . '%');
                }
                if ($data['evaluator_surname'] != '') {
                    $queryBuilder
                        ->andWhere('u_two.surname LIKE :evaluator_surname')
                        ->setParameter('evaluator_surname', '%' . $data['evaluator_surname'] . '%');
                }
                if ($data['evaluator_lastname'] != '') {
                    $queryBuilder
                        ->andWhere('u_two.lastname LIKE :evaluator_lastname')
                        ->setParameter('evaluator_lastname', '%' . $data['evaluator_lastname'] . '%');
                }
                if ($data['evaluator_rol'] != '') {
                    $queryBuilder
                        ->andWhere('pa.evaluator_rol = :evaluator_rol')
                        ->setParameter('evaluator_rol', $data['evaluator_rol']);
                }
                
                $query = $queryBuilder->getQuery();
                
                // Paginator
                
                $paginator = $this->get('ideup.simple_paginator');
                $paginator->setItemsPerPage(20, 'workshoppollapplication');
                $paginator->setMaxPagerItems(5, 'workshoppollapplication');
                $entities = $paginator->paginate($query, 'workshoppollapplication')->getResult();
                
                $startPageItem = $paginator->getStartPageItem('workshoppollapplication');
                $endPageItem = $paginator->getEndPageItem('workshoppollapplication');
                $totalItems = $paginator->getTotalItems('workshoppollapplication');
                
                if ($totalItems == 0) {
                    $info_paginator = 'No hay registros que mostrar';
                }
                else {
                    $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
                }
                
                // Return Template
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:index.html.twig', array(
                    'entities' => $entities,
                    'workshopscheduling' => $workshopscheduling,
                    'pollscheduling' => $pollscheduling,
                    'form' => $form->createView(),
                    'deletes_form' => $deletes_form->createView(),
                    'paginator' => $paginator,
                    'info_paginator' => $info_paginator,
                    'form_data' => $formData
                ));
                
            }
            else {
                $session->getFlashBag()->add('error', 'La encuesta de la Programación de Taller ha sido eliminada.');
                
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Pollapplication entity.
     *
     */
    public function newPollEvaluatorAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            // Recover Pollscheduling
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
            $result = $repository->createQueryBuilder('ps')
                ->select('ps.id, ps.title, ps.type, ps.entity_type, ps.entity_id, ps.version, 
                          c.company_type, c.id company_id, c.name company_name, 
                          ps.initdate, ps.enddate, ps.deleted')
                ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
                ->where('ps.id = :pollscheduling 
                        AND ps.entity_type = :type
                        AND ps.status = 1')
                ->setParameter(':pollscheduling', $pollschedulingid)
                ->setParameter(':type', 'workshopscheduling')
                ->getQuery()
                ->getResult();
            
            $pollscheduling = current($result);
            
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            if ($pollscheduling['type'] == 'not_evaluateds') {
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds_new', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            if (!$pollscheduling['deleted']) {
                
                if (!$pollscheduling['initdate']) {
                    $session->getFlashBag()->add('error', 'Debe ingresar los datos faltantes para la Encuesta de la Programación de Taller.');
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_edit', array(
                        'id' => $pollschedulingid
                    )));
                }
                
                // Recovering data
                
                $course_options = Companycourse::getListCompanycourseOptions($this->getDoctrine(), $pollscheduling['company_id']);
                $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $pollscheduling['company_id']);
                $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $pollscheduling['company_id']);

                if ($pollscheduling['type'] == 'self_evaluation') {
                    $dataOp = 'self' ;
                    $statusField = TRUE ;
                } else {
                    $dataOp = '';
                    $statusField = FALSE ;
                }

                // Find Workinginformations
                
                $defaultData = array(
                    'id' => '',
                    'evaluated' => '', 
                    'evaluator_name' => '', 
                    'evaluator_rol' => '', 
                    'iden' => '',
                    'type_value' => '',
                    'attrib' => '', 
                    'course' => '', 
                    'section' => '', 
                    'code' => '', 
                    'surname' => '', 
                    'lastname' => '', 
                    'data_type' => '', 
                    'grade' => '', 
                    'study_year' => '', 
                    'names' => '', 
                    'initdate' => NULL, 
                    'organizationalunit' => '', 
                    'charge' => ''
                );
                $form = $this->createFormBuilder($defaultData)
                    ->add('id', 'hidden', array(
                        'required' => TRUE
                    ))
                    ->add('iden', 'hidden', array(
                        'required' => TRUE
                    ))
                    ->add('type_value', 'hidden', array(
                        'required' => TRUE,
                        'data' => $dataOp 
                    ))
                    ->add('evaluated', 'text', array(
                        'required' => TRUE
                    ))
                    ->add('type', 'choice', array(
                        'choices' => array(
                            'one' => 'Uno a uno',
                            'collaborators' => 'Colaboradores',
                            'boss' => 'Jefe',
                            '360' => '360',
                            'self' => 'Autoevaluación'
                        ), 
                        'empty_value' => 'Choose an option',
                        'required' => TRUE,
                        'disabled' => $statusField,
                        'data' => $dataOp 
                    ))
                    ->add('evaluator_name', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_rol', 'choice', array(
                        'choices' => array(
                            'boss' => 'Jefe',
                            'pair' => 'Par',
                            'collaborator' => 'Colaborador',
                            'self' => 'Autoevaluado'
                        ), 
                        'empty_value' => 'Choose an option',
                        'required' => TRUE
                    ))
                    ->add('attrib', 'choice', array(
                        'choices' => array(
                            '0' => 'No',
                            '1' => 'Si',
                        ), 
                        'empty_value' => 'Choose an option',
                        'required' => FALSE
                    ))
                    ->add('course', 'choice', array(
                        'choices' => $course_options, 
                        'empty_value' => 'Choose an option',
                        'required' => FALSE
                    ))
                    ->add('section', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('code', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('surname', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('lastname', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('names', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('data_type', 'choice', array(
                        'choices' => array(
                            'working' => 'Laboral',
                            'university' => 'Universidad',
                            'school' => 'Colegio',
                        ), 
                        'empty_value' => 'Choose an option',
                        'required' => FALSE
                    ))
                    ->add('initdate', 'date', array(
                        'input'  => 'datetime',
                        'widget' => 'single_text',
                        'format' => 'dd-MM-yyyy',
                        'required' => FALSE
                    ))
                    ->add('organizationalunit', 'choice', array(
                        'choices' => $organizationalunit_options, 
                        'empty_value' => 'Choose an option',
                        'required' => FALSE
                    ))
                    ->add('charge', 'choice', array(
                        'choices' => $charge_options, 
                        'empty_value' => 'Choose an option',
                        'required' => FALSE
                    ))
                    ->add('grade', 'choice', array(
                        'choices' => array(
                            '1p' => '1ro de primaria', 
                            '2p' => '2do de primaria', 
                            '3p' => '3ro de primaria', 
                            '4p' => '4to de primaria', 
                            '5p' => '5to de primaria', 
                            '6p' => '6to de primaria', 
                            '1s' => '1ro de secundaria', 
                            '2s' => '2do de secundaria', 
                            '3s' => '3ro de secundaria', 
                            '4s' => '4to de secundaria', 
                            '5s' => '5to de secundaria'
                        ), 
                        'empty_value' => 'Choose an option',
                        'required' => FALSE
                    ))
                    ->add('study_year', 'text', array(
                        'required' =>false
                      ))
                    ->getForm();
        
                $data = array(
                    'id' => '',
                    'iden' => '',
                    'type_value' => '', 
                    'evaluated' => '', 
                    'type' => '', 
                    'evaluator_name' => '', 
                    'evaluator_rol' => '', 
                    'attrib' => '', 
                    'course' => '', 
                    'section' => '', 
                    'code' => '', 
                    'surname' => '', 
                    'lastname' => '', 
                    'names' => '', 
                    'data_type' => '', 
                    'grade' => '', 
                    'study_year' => '', 
                    'initdate' => NULL, 
                    'organizationalunit' => '', 
                    'charge' => ''
                );
                if($request->getMethod() == 'POST') {
                    $form->bindRequest($request);
                    $data = $form->getData();
                }
    
                if ($data['type_value'] != '') {
                
                    // Recover pollapplications ids
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                    $wsp_result = $repository->createQueryBuilder('pa')
                        ->select('wi.id')
                        ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                        ->where('pa.pollscheduling = :pollscheduling 
                                AND pa.evaluated_id = :evaluated 
                                AND wi.company = :company 
                                AND pa.deleted = :deleted')
                        ->setParameter('pollscheduling', $pollscheduling['id'])
                        ->setParameter('evaluated', $data['id'])
                        ->setParameter('company', $pollscheduling['company_id'])
                        ->setParameter('deleted', FALSE)
                        ->orderBy('wi.id', 'ASC')
                        ->groupBy('wi.id')
                        ->getQuery()
                        ->getResult();
                    
                    $wsp_ids = '';
                    
                    // We set ids chain pollapplications
                    
                    if (!empty($wsp_result)) {
                        
                        $wsp_count = count($wsp_result);
                        $i = 1;
                        $wsp_ids = '';
                        
                        foreach ($wsp_result as $wsp) {
                            $wsp_ids .= $wsp['id'];
                            if ($i < $wsp_count) {
                                $wsp_ids .= ',';
                            }
                            $i++;
                        }
                    }
                  
                    // Query
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                    $queryBuilder = $repository->createQueryBuilder('wa');
                    
                    if ($data['type_value'] == 'collaborators') {
                        $queryBuilder
                            ->select('wi.id, wi.code, u.names, u.surname, u.lastname, 
                                      co.name companyorganizationalunit, cc.name companycharge, 
                                      wi.datein, wi.status')
                            ->innerjoin('wa.workinginformation', 'wi')
                            ->leftJoin('wi.companyorganizationalunit', 'co')
                            ->leftJoin('wi.companycharge', 'cc')
                            ->innerJoin('wi.user', 'u')
                            ->where('wi.company = :company 
                                    AND wa.workshopscheduling = :workshopscheduling 
                                    AND wi.status = 1
                                    AND wi.boss = :boss
                                    AND wa.deleted = :deleted')
                            ->setParameter('boss', $data['id']);
                    }
                    elseif ($data['type_value'] == 'boss') {
                        $queryBuilder
                            ->select('wib.id, wib.code, u.names, u.surname, u.lastname, 
                                      co.name companyorganizationalunit, cc.name companycharge, 
                                      wib.datein, wib.status')
                            ->innerjoin('wa.workinginformation', 'wi')
                            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wib', 'WITH', 'wi.boss = wib.id')
                            ->leftJoin('wib.companyorganizationalunit', 'co')
                            ->leftJoin('wib.companycharge', 'cc')
                            ->innerJoin('wib.user', 'u')
                            ->where('wib.company = :company 
                                    AND wa.workshopscheduling = :workshopscheduling 
                                    AND wib.status = 1
                                    AND wi.id = :workinginformation
                                    AND wa.deleted = :deleted')
                            ->setParameter('workinginformation', $data['id']);
                    }
                    elseif ($data['type_value'] == '360') {
                        $queryBuilder
                            ->select('wi.id, wi.code, u.names, u.surname, u.lastname, 
                                      co.name companyorganizationalunit, cc.name companycharge, 
                                      wi.datein, wi.status, wi.grade, wi.study_year, wi.type')
                            ->innerjoin('wa.workinginformation', 'wi')
                            ->leftJoin('wi.companyorganizationalunit', 'co')
                            ->leftJoin('wi.companycharge', 'cc')
                            ->innerJoin('wi.user', 'u')
                            ->where('wi.company = :company 
                                    AND wa.workshopscheduling = :workshopscheduling 
                                    AND wi.status = 1
                                    AND wa.deleted = :deleted');
                    }
                    elseif ($data['type_value'] == 'self') {
                        $queryBuilder
                            ->select('wi.id, wi.code, u.names, u.surname, u.lastname, 
                                      co.name companyorganizationalunit, cc.name companycharge, 
                                      wi.datein, wi.status')
                            ->innerjoin('wa.workinginformation', 'wi')
                            ->leftJoin('wi.companyorganizationalunit', 'co')
                            ->leftJoin('wi.companycharge', 'cc')
                            ->innerJoin('wi.user', 'u')
                            ->where('wi.company = :company 
                                    AND wa.workshopscheduling = :workshopscheduling 
                                    AND wi.status = 1 
                                    AND wi.id = :workinginformation
                                    AND wa.deleted = :deleted')
                            ->setParameter('workinginformation', $data['id']);
                    }elseif ($data['type_value'] == 'one') {
                        $queryBuilder
                            ->select('wi.id, wi.code, wi.grade, wi.study_year, u.names, 
                                      u.surname, u.lastname, co.name companyorganizationalunit, 
                                      cc.name companycharge, wi.datein, wi.status')
                            ->innerjoin('psp.workinginformation', 'wi')
                            ->leftJoin('wi.companyorganizationalunit', 'co')
                            ->leftJoin('wi.companycharge', 'cc')
                            ->innerJoin('wi.user', 'u')
                            ->where('wa.workshopscheduling = :workshopscheduling 
                                    AND wi.company = :company
                                    AND wi.status = 1
                                    AND wi.id = :workinginformation')
                            ->setParameter('workinginformation', $data['iden']);
                    }
                    
                    $queryBuilder
                        ->setParameter('company', $pollscheduling['company_id'])
                        ->setParameter('workshopscheduling', $workshopschedulingid)
                        ->setParameter('deleted', FALSE)
                        ->orderBy('wi.id', 'ASC');
                    
                    // Check if there are people assigned
                    
                    if ($wsp_ids != '' && $data['type_value'] != 'self') {
                        $queryBuilder
                            ->andWhere('wi.id NOT IN(' . $wsp_ids . ')');
                    }
                    
                    // Add the arguments that are being asked
                    
                    if ($data['code'] != '') {
                        $queryBuilder
                            ->andWhere('wi.code LIKE :code')
                            ->setParameter('code', '%' . $data['code'] . '%');
                    }
                    if (!empty($data['surname'])) {
                        $queryBuilder
                            ->andWhere('u.surname LIKE :surname')
                            ->setParameter('surname', $data['surname']);
                    }
                    if (!empty($data['lastname'])) {
                        $queryBuilder
                            ->andWhere('u.lastname LIKE :lastname')
                            ->setParameter('lastname', $data['lastname']);
                    }
                    if (!empty($data['names'])) {
                        $queryBuilder
                            ->andWhere('u.names LIKE :names')
                            ->setParameter('names', $data['names']);
                    }
                    if ($data['data_type'] == 'school') {
                        
                        if (!empty($data['data_type'])) {
                            $queryBuilder
                                ->andWhere('wi.type LIKE :type')
                                ->setParameter('type', '%schoolinformation%');
                        }

                        if (!empty($data['grade'])) {
                            $queryBuilder
                                ->andWhere('wi.grade LIKE :grade')
                                ->setParameter('grade', '%' . $data['grade'] . '%');
                        }
                        if (!empty($data['study_year'])) {
                            $queryBuilder
                                ->andWhere('wi.study_year LIKE :study_year')
                                ->setParameter('study_year', '%' . $data['study_year'] . '%');
                        }
                    }
                    if ($data['data_type'] == 'working') {
                        if (!empty($data['data_type'])) {
                            $queryBuilder
                                ->andWhere('wi.type LIKE :type')
                                ->setParameter('type', '%workinginformation%');
                        }

                        if (!empty($data['initdate'])) {
                            $queryBuilder
                                ->andWhere('wi.datein = :initdate')
                                ->setParameter('initdate', $data['initdate']);
                        }
                        elseif ($data['organizationalunit'] != '') {
                            $queryBuilder
                                ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                                ->setParameter('organizationalunit', $data['organizationalunit']);
                        }
                        elseif ($data['charge'] != '') {
                            $queryBuilder
                                ->andWhere('wi.companycharge = :charge')
                                ->setParameter('charge', $data['charge']);
                        }
                    }
                    
                    $query = $queryBuilder->getQuery();
                    
                    if ($data['type_value'] == 'one') {
                      
                        // User
                        $userBy = $this->get('security.context')->getToken()->getUser();
                        
                        $data = $request->request->get('form');
                        
                        $workinginformations = '';
                        $attArray = array();
                        
                        $pollschedulingOb = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollscheduling['id']);
                        
                        $evaluator[] = $data['iden'];
                        $evaluated[] = $data['id'];
                        $attArray[0]= $data['course'];
                        $attArray[1]= $data['section'];
                        
                        // Add Pollapplications
                        $pollapplication = Pollapplication::addPollapplications(
                            $this->getDoctrine(), 
                            $evaluator, 
                            $evaluated, 
                            'workshopapplication', 
                            $pollschedulingOb, 
                            $userBy, 
                            $workshopschedulingid, 
                            $attArray,
                            $data['evaluator_rol']
                        );
                        
                        if ($pollapplication['message_error']) {
                            $session->getFlashBag()->add('error', $pollapplication['message_error']);
                        }

                        $session->getFlashBag()->add('status', 'Se asignaron ' . $pollapplication['number_register'] . ' de Evaluaciones a la encuesta satisfactoriamente.');

                        if($request->request->get('saveandclose', true)){
                            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                                'workshopschedulingid' => $workshopschedulingid, 
                                'pollschedulingid' => $pollschedulingid
                            )));
                        }
                        else {
                            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_new', array(
                                'workshopschedulingid' => $workshopschedulingid, 
                                'pollschedulingid' => $pollschedulingid
                            )));
                        }
    
                    }
        
                    // Paginator
                    
                    $paginator = $this->get('ideup.simple_paginator');
                    $paginator->setItemsPerPage(20, 'workinginformationevaluator');
                    $paginator->setMaxPagerItems(5, 'workinginformationevaluator');
                    $entities = $paginator->paginate($query, 'workinginformationevaluator')->getResult();
                    
                    $startPageItem = $paginator->getStartPageItem('workinginformationevaluator');
                    $endPageItem = $paginator->getEndPageItem('workinginformationevaluator');
                    $totalItems = $paginator->getTotalItems('workinginformationevaluator');
            
                    if ($totalItems == 0) {
                        $info_paginator = 'No hay registros que mostrar';
                    }
                    else {
                        $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
                    }
                
              
                }
                else {
                    $entities = '';
                    $paginator = '';
                    $info_paginator = '';
                }
        
                // Asigned registers
                
                $defaultDataRegisters = array(
                    'evaluated' => $data['id'],
                    'evaluators' => array()
                );
                if ($data['type_value'] == 'self') {
                    $defaultDataRegisters['evaluators_rol'] = 'self';
                }
                $asigneds_form = $this->createFormBuilder($defaultDataRegisters)
                    ->add('evaluated', 'hidden', array(
                        'required'=>true
                    ))
                    ->add('evaluators', 'choice', array(
                        'required' => false, 
                        'multiple' => true, 
                        'expanded' => true
                    ))
                    ->add('evaluators_rol', 'choice', array(
                        'choices' => array(
                            'boss' => 'Jefe',
                            'pair' => 'Par',
                            'collaborator' => 'Colaborador',
                            'self' => 'Autoevaluado'
                        ),
                        'empty_value' => 'Choose an option',
                        'required' => true
                    ))
                    ->getForm();
                    
                // Recover Persons
                $workinginformations = Workshopapplication::getWorkshopapplicationsList($this->getDoctrine(), $pollscheduling['company_id'], $pollscheduling['entity_id']);
                
                for ($i = 0; $i < count($workinginformations) ; $i++) {
                    $workinginformations[$i]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                                        $workinginformations[$i]['lastname'] . ' ' . 
                                                        $workinginformations[$i]['surname'] . ' (' . 
                                                        Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
                }
    
                $workinginformations = 'var workinginformations = ' . json_encode($workinginformations);
    
                // Return new.html.twig            
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:new.html.twig', array(
                    'entities' => $entities,
                    'workshopscheduling' => $workshopscheduling,
                    'pollscheduling' => $pollscheduling,
                    'form' => $form->createView(),
                    'asigneds_form' => $asigneds_form->createView(),
                    'paginator' => $paginator,
                    'info_paginator' => $info_paginator,
                    'jsonworkinginformations' => $workinginformations,
                ));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La encuesta ha sido eliminada de la Programación de Taller.');

                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Creates a new Pollapplication entity.
     *
     */
    public function asignedsPollEvaluatorAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $data = $request->request->get('form');
        
        if (!empty($data['evaluators'])) {
            
            $em = $this->getDoctrine()->getManager();
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
            
            $attArray = array();
            
            $numberRegister = 0;
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            // Recovering data
            
            if (isset($data['iden'])) {
                $evaluator[] = $data['iden'];
            }
            $evaluated = NULL;
            
            // Add Pollapplications
            
            if (in_array($company->getCompanyType(), array('school', 'university'))) {
                if (isset($data['course'])) {
                    $attArray[0]= $data['course'];
                }
                if (isset($data['section'])) {
                    $attArray[1]= $data['section'];
                }
                $pollapplication = Pollapplication::addPollapplications(
                    $this->getDoctrine(), 
                    $data['evaluators'], 
                    $data['evaluated'], 
                    'workshopapplication', 
                    $pollscheduling, 
                    $userBy, 
                    $workshopschedulingid,
                    $attArray,
                    $data['evaluators_rol']
                );
            }
            else {
                $pollapplication = Pollapplication::addPollapplications(
                    $this->getDoctrine(), 
                    $data['evaluators'], 
                    $data['evaluated'], 
                    'workshopapplication', 
                    $pollscheduling, 
                    $userBy, 
                    $workshopschedulingid, 
                    NULL,
                    $data['evaluators_rol']
                );
            }
            
            if ($pollapplication['message_error']) {
                $session->getFlashBag()->add('error', $pollapplication['message_error']);
            }
            
            $session->getFlashBag()->add('status', 'Se asignaron ' . $pollapplication['number_register'] . ' Evaluacion(es) a la encuesta satisfactoriamente.');
        }
        else {
            $session->getFlashBag()->add('error', 'No escogio ningún evaluador para asignar a la encuesta.');
        }

        if($request->request->get('saveandclose', true)){
            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                'workshopschedulingid' => $workshopschedulingid, 
                'pollschedulingid' => $pollschedulingid
            )));
        }
        else {
            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_new', array(
                'workshopschedulingid' => $workshopschedulingid, 
                'pollschedulingid' => $pollschedulingid
            )));
        }
    }

    /**
     * Displays a form to edit an existing Workshopschedulingpoll evaluator entity.
     *
     */
    public function editPollEvaluatorAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            // Pollapplication Info
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $query = $repository->createQueryBuilder('pa')
                ->select('pa.id, wi_one.id evaluated_id, wi_one.type evaluated_type, wi_one.code evaluated_code, 
                          u_one.names evaluated_names, u_one.surname evaluated_surname, u_one.lastname evaluated_lastname, 
                          wi_two.id evaluator_id, wi_two.type evaluator_type, wi_two.code evaluator_code, 
                          u_two.names evaluator_names, u_two.surname evaluator_surname, u_two.lastname evaluator_lastname, 
                          pa.evaluator_rol, pa.deleted, pa.course, pa.section, ps.id pollscheduling_id, 
                          ps.title pollscheduling_title, ps.version pollscheduling_version, ps.entity_type, ps.entity_id, 
                          ps.company_id, c.company_type, ps.type, ps.initdate, ps.enddate')
                ->innerJoin('pa.pollscheduling', 'ps')
                ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
                ->innerJoin('wi_one.user', 'u_one')
                ->innerJoin('wi_two.user', 'u_two')
                ->where('pa.id = :pollapplication 
                        AND pa.deleted = :deleted')
                ->setParameter('pollapplication', $id)
                ->setParameter('deleted', FALSE)
                ->getQuery()
                ->getResult();
                
            $entity = current($query);
            
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            elseif ($entity['type'] == 'not_evaluateds') {
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $entity['pollscheduling_id']
                )));
            }

            $courses = array();

            //Recovery Companycourse
            
            $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Companycourse');
            $result = $repository->createQueryBuilder('cc')
                ->select('cc.id, cc.name')
                ->innerJoin('cc.company', 'c')
                ->where('cc.company = :company 
                        AND cc.status = 1')
                ->setParameter(':company', $entity['company_id'])
                ->orderBy('cc.id', 'ASC')
                ->getQuery()
                ->getResult();
                          
            foreach($result as $r) {
                $courses[$r['id']] = $r['name'];
            }
                  
            // Form Edit Evaluated
            
            $defaultData = array(
                'id' => $entity['evaluator_id'],
                'evaluator' => $entity['evaluator_names'] . ' ' . $entity['evaluator_surname'] . ' ' . $entity['evaluator_lastname'] . ' (' . Workinginformation::getTypeName($entity['evaluator_type']) . ')'
            );
            $edit_form = $this->createFormBuilder($defaultData)
                ->add('id', 'hidden', array(
                    'required'=>true
                ))
                ->add('evaluator', 'text', array(
                    'required'=>true
                ))
                ->add('evaluator_rol', 'choice', array(
                    'choices' => array(
                        'boss' => 'Jefe',
                        'pair' => 'Par',
                        'collaborator' => 'Colaborador',
                        'self' => 'Autoevaluado'
                    ),
                    'empty_value' => 'Choose an option',
                    'required' => false,
                    'data' => $entity['evaluator_rol'] 
                ))
                ->add('course', 'choice', array(
                    'choices' => $courses, 
                    'empty_value' => 'Choose an option',
                    'required'=>false,
                    'data' => $entity['course'] 
                ))
                ->add('section', 'text', array(
                    'required'=>false,
                    'data' => $entity['section'] 
                ))
                ->getForm();
            
            //Posible evaluated, for the company
            $repository = $this->getDoctrine()->getManager()->getRepository('FishmanWorkshopBundle:Workshopapplication');
            $workinginformations = $repository->createQueryBuilder('wa')
                ->select('wi.id, wi.type, wi.code, u.names, u.surname, u.lastname')
                ->where('wi.company = :company
                        AND wa.workshopscheduling = :workshopscheduling
                        AND wa.deleted = :deleted')
                ->innerJoin('wa.workinginformation', 'wi')
                ->innerJoin('wi.user', 'u')
                ->setParameter('company', $entity['company_id'])
                ->setParameter('workshopscheduling', $workshopschedulingid)
                ->setParameter('deleted', FALSE)
                ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC')
                ->getQuery()
                ->getResult();
            
            $count_wis = count($workinginformations);
            $j = 0;
            $wis = array();
            
            //Create
            for ($i = 0; $i < $count_wis; $i++) {
                $query = FALSE; 
                if ($entity['evaluator_id'] != $workinginformations[$i]['id']) {
                    $query = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
                                'evaluated_id' => $entity['evaluated_id'], 
                                'evaluator_id' => $workinginformations[$i]['id'], 
                                'deleted' => FALSE));

                    if (empty($query)) {
                        $wis[$j]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                            $workinginformations[$i]['lastname'] . ' ' . 
                                            $workinginformations[$i]['surname'] . ' (' . 
                                            Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
                        $j++;
                    }
                    else {
                        unset($workinginformations[$i]);
                    }
                    
                }
                else {
                    $wis[$j]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                        $workinginformations[$i]['lastname'] . ' ' . 
                                        $workinginformations[$i]['surname'] . ' (' . 
                                        Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
                    $j++;
                }
            }
            
            $workinginformations = 'var workinginformations = ' . json_encode($wis);
            
            //Return template edit pollscheduling evaluator
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:edit.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling,
                'edit_form' => $edit_form->createView(),
                'jsonworkinginformations' => $workinginformations
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Edits an existing Workshopschedulingpoll evaluator entity.
     *
     */
    public function updatePollEvaluatorAction(Request $request, $workshopschedulingid, $id)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Pollapplication
        
        $entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la encuesta de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }
        elseif ($entity->getPollscheduling()->getType() == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
                'workshopschedulingid' => $workshopschedulingid, 
                'pollschedulingid' => $entity->getPollscheduling()->getId()
            )));
        }

        $data = $request->request->get('form');
        
        if (!empty($data['id'])) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            //Remove notification execution
            Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatorId(), 'pollapplication', $entity->getId());
            
            $entity->setEvaluatorId($data['id']);
            if ($entity->getEvaluatedId() == $data['id'] && $data['evaluator_rol'] != 'self') {
                $session->getFlashBag()->add('error', 'Esta es una autoevaluación, el rol del eveluador ha sido cambiado a "Autoevaluado".');
                $entity->setEvaluatorRol('self');
            }
            elseif ($entity->getEvaluatedId() != $data['id'] && $data['evaluator_rol'] == 'self') {
                $session->getFlashBag()->add('error', 'Esta no es una autoevaluación, el rol del eveluador no ha sido asignado.');
                $entity->setEvaluatorRol('');
            }
            else {
                $entity->setEvaluatorRol($data['evaluator_rol']);
            }
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            if (isset($data['attrib'])) {
                if ($data['attrib'] != 0 ) {
                    $entity->setCourse($data['course']);
                    $entity->setSection($data['section']);
                } else {
                    $entity->setCourse(NULL);
                    $entity->setSection(NULL);
                }
            }
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
            'workshopschedulingid' => $workshopschedulingid,
            'pollschedulingid' => $entity->getPollscheduling()->getId()
        )));
    }

    /**
     * Drop an Pollapplication entity.
     *
     */
    public function dropPollEvaluatorAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $query = $repository->createQueryBuilder('pa')
                ->select('pa.id, wi_one.id evaluated_id, u_one.names evaluated_names, u_one.surname evaluated_surname, u_one.lastname evaluated_lastname, 
                          wi_two.id evaluator_id, u_two.names evaluator_names, u_two.surname evaluator_surname, u_two.lastname evaluator_lastname, 
                          ps.id pollscheduling_id, ps.title pollscheduling_title, ps.type, ps.version pollscheduling_version, 
                          ps.initdate, ps.enddate')
                ->innerJoin('pa.pollscheduling', 'ps')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
                ->innerJoin('wi_one.user', 'u_one')
                ->innerJoin('wi_two.user', 'u_two')
                ->where('pa.id = :pollapplication 
                        AND pa.deleted = :deleted')
                ->setParameter('pollapplication', $id)
                ->setParameter('deleted', FALSE)
                ->getQuery()
                ->getResult();
                
            $entity = current($query);
            
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
          
            $deleteForm = $this->createDeleteForm($id);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:drop.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling, 
                'delete_form' => $deleteForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Drop multiple an Pollapplication entities.
     *
     */
    public function dropmultiplePollEvaluatorAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            // Recover Pollscheduling
            
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            elseif ($pollscheduling->getType() == 'not_evaluateds') {
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds_dropmultiple', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
              
            // Recover register of form
            
            $data = $request->request->get('form_deletes');
            
            if (isset($data['registers'])) {
            
                $registers = '';
                $registers_count = count($data['registers']);
                $i = 1;
                
                if (!empty($data['registers'])) {
                    
                    foreach($data['registers'] as $r) {
                        $registers .= $r;
                        if ($i < $registers_count) {
                            $registers .= ',';
                        }
                        $i++;
                    }
        
                    // Recover pollapplication of pollscheduling
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                    $entities = $repository->createQueryBuilder('pa')
                        ->select('pa.id, wi_one.id evaluated_id, u_one.names evaluated_names, u_one.surname evaluated_surname, u_one.lastname evaluated_lastname, 
                                  wi_two.id evaluator_id, u_two.names evaluator_names, u_two.surname evaluator_surname, u_two.lastname evaluator_lastname')
                        ->innerJoin('pa.pollscheduling', 'ps')
                        ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluated_id = wi_one.id')
                        ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluator_id = wi_two.id')
                        ->innerJoin('wi_one.user', 'u_one')
                        ->innerJoin('wi_two.user', 'u_two')
                        ->where('pa.id IN(' . $registers . ')')
                        ->getQuery()
                        ->getResult();
                        
                    if (!$entities) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la encuesta de la Programación de Taller.');
                        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                            'workshopschedulingid' => $workshopschedulingid, 
                            'pollschedulingid' => $pollschedulingid
                        )));
                    }
                
                    $deleteMultipleForm = $this->createDeleteMultipleForm($registers);
                
                    return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:dropmultiple.html.twig', array(
                        'entities' => $entities,
                        'workshopscheduling' => $workshopscheduling,
                        'pollscheduling' => $pollscheduling,
                        'delete_form' => $deleteMultipleForm->createView()
                    ));
                }
            }
            
            $session->getFlashBag()->add('error', 'No se ha seleccionado ningún registro para eliminar.');
            
            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                'workshopschedulingid' => $workshopschedulingid,
                'pollschedulingid' => $pollschedulingid
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Drop all an Pollapplication entities.
     *
     */
    public function dropallPollEvaluatorAction($workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            // Recover Pollscheduling
            
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            elseif ($pollscheduling->getType() == 'not_evaluateds') {
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds_dropall', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $deleteAllForm = $this->createDeleteAllForm();
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:dropall.html.twig', array(
                'workshopscheduling' => $workshopscheduling,
                'pollscheduling' => $pollscheduling,
                'delete_all_form' => $deleteAllForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
        
    }

    private function createDeleteMultipleForm($registers)
    {
        return $this->createFormBuilder(array('registers' => $registers))
            ->add('registers', 'hidden')
            ->getForm()
        ;
    }

    private function createDeleteAllForm()
    {
        return $this->createFormBuilder(array('delete_all' => TRUE))
            ->add('delete_all', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Delete a Pollapplication entity.
     *
     */
    public function deletePollEvaluatorAction(Request $request, $workshopschedulingid, $pollschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
          
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            
            $entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
            
            $entity->setDeleted(TRUE);

            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
            'workshopschedulingid' => $workshopschedulingid, 
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Deletes multiple a Pollapplication entities.
     *
     */
    public function deletemultiplePollEvaluatorAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $form = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();   
        
        if ($form['registers'] != '') {
            
            // Recover Pollapplications of form

            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $entities = $repository->createQueryBuilder('pa')
                ->where('pa.id IN(' . $form['registers'] . ')')
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No se puede encontrar las evaluacións asignadas a la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
          
            foreach($entities as $entity) {
            
                Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
              
                $entity->setDeleted(TRUE);
    
                $em->persist($entity);
                $em->flush();
            }
            
            $session->getFlashBag()->add('status', 'Los registros fueron eliminados satisfactoriamente.');

        }

        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
            'workshopschedulingid' => $workshopschedulingid, 
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Deletes all Pollapplication entities.
     *
     */
    public function deleteallPollEvaluatorAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $form = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();   
        
        if ($form['delete_all']) {
            
            // Recover Pollapplications of form
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $entities = $repository->createQueryBuilder('pa')
                ->where('pa.pollscheduling = :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No existen evaluaciones en esta Encuesta de Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            foreach($entities as $entity) {
                
                Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
                
                $entity->setDeleted(TRUE);
                
                $em->persist($entity);
                $em->flush();
            }
            
            $session->getFlashBag()->add('status', 'Los registros fueron eliminados satisfactoriamente.');
            
        }

        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
            'workshopschedulingid' => $workshopschedulingid, 
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Import Workshopschedulingpoll evaluator entities.
     *
     */
    public function importPollEvaluatorAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Mensaje de que no hay registros
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'workshopscheduling') {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            elseif ($pollscheduling->getType() == 'not_evaluateds') {
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds_import', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $companyId = $workshopscheduling->getCompany()->getId();
            $companyCode = $workshopscheduling->getCompany()->getCode();
            $companyName = $workshopscheduling->getCompany()->getName();
            $companyType = $workshopscheduling->getCompany()->getCompanyType();
            
            if (!$pollscheduling->getDeleted()) {
                
                if (!$pollscheduling->getInitdate()) {
                    $session->getFlashBag()->add('error', 'Debe ingresar los datos faltantes para la Encuesta de la Programación de Taller.');
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_edit', array(
                        'id' => $pollschedulingid
                    )));
                }
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplicationimport');
                $query = $repository->createQueryBuilder('pai')
                    ->select('COUNT(pai.id) cpais')
                    ->where('pai.pollscheduling_id = :pollscheduling')
                    ->setParameter('pollscheduling', $pollschedulingid)
                    ->groupBy('pai.pollscheduling_id')
                    ->getQuery()
                    ->getResult();
                
                $paimports = current($query);
                if ($paimports['cpais'] > 0) {
                    $session->getFlashBag()->add('status', 'Existen Evaluaciones pendientes a importar.');
                    return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:import.html.twig', array(
                        'workshopscheduling' => $workshopscheduling,
                        'pollscheduling' => $pollscheduling,
                        'pollapplicationimport' => TRUE,
                        'companytype' => $companyType
                    ));
                }
                
                $defaultData = array();
                $form = $this->createForm(new WorkshoppollevaluatorimportType(), $defaultData);
    
                $form2 = false;
                $data = '';
                $people = false;
                $evaluations = false;
                $message = '';
                $messageError = '';
                $existingwiemail = '';
                $numberRegister = 0;
                
                if ($request->isMethod('POST')) {
                    
                    //The user is confirmed to persist data as 
                    $importFormData = $request->request->get('form', false);
                    
                    if($importFormData){
                        $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                             ->find($importFormData['temporalimportdata_id']);
                        if(!$temporalentity){
                            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:import.html.twig', array(
                                'form' => $form->createView(),
                                'form2' => $form2->createView(),
                                'workshopscheduling' => $workshopscheduling,
                                'pollscheduling' => $pollscheduling
                            ));
                        }
                        
                        $evaluations = $temporalentity->getData();
                        $userBy = $this->get('security.context')->getToken()->getUser();
                        
                        // suspend auto-commit
                        $em->getConnection()->beginTransaction();
                        
                        // Try and make the transaction
                        try {
                            
                            if (in_array($companyType, array('working', 'school', 'university'))) {
                                foreach ($evaluations as $evaluation) {
                                    if ($evaluation['evaluator']['valid'] && $evaluation['evaluated']['valid']) {
                                        
                                        $evaluator = Pollpersonimport::addRegister($this->getDoctrine(), $evaluation['evaluator']);
                                        $evaluated = Pollpersonimport::addRegister($this->getDoctrine(), $evaluation['evaluated']);
                                        
                                        $pai = new Pollapplicationimport();
                                        $pai->setPollschedulingId($pollschedulingid);
                                        $pai->setEvaluatedId($evaluated->getId());
                                        $pai->setEvaluatorId($evaluator->getId());
                                        $pai->setEvaluatedCode($evaluation['evaluated']['code']);
                                        $pai->setEvaluatorCode($evaluation['evaluator']['code']);
                                        $pai->setEvaluatorRol($evaluation['evaluator']['evaluator_rol']);
                                        $pai->setCourse($evaluation['evaluated']['course_id']);
                                        $pai->setSection($evaluation['evaluated']['section']);
                                        $pai->setStatus(FALSE);
                                        
                                        $em->persist($pai);
                                        $em->flush();
                                        
                                        $numberRegister++;
                                    }
                                }
                            }
                            
                            //Delete temporal data
                            $em->remove($temporalentity);
                            $em->flush();
                            
                            // Delete temporal files
                            Workshopscheduling::deleteTemporalFiles('../tmp/');
                            
                            // Try and commit the transactioncurrentversioncurrentversion
                            $em->getConnection()->commit();
                        } catch (Exception $e) {
                            // Rollback the failed transaction attempt
                            $em->getConnection()->rollback();
                            throw $e;
                        }
                        
                        if ($numberRegister > 0) {
                            $session->getFlashBag()->add('status', 'Existen '.$numberRegister.' Evaluaciones pendientes a importar.');
                            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:import.html.twig', array(
                                'workshopscheduling' => $workshopscheduling,
                                'pollscheduling' => $pollscheduling,
                                'pollapplicationimport' => TRUE,
                                'companytype' => $companyType
                            ));
                        }
                        else {
                            $session->getFlashBag()->add('error', 'No hay Evaluaciones para importar.');
                        }
                        
                        $wspe = false;
                        
                    } //End have temporarlimportdata input
                    else {
                        
                        $form->bind($request);
                        $data = $form->getData();
        
                        // We need to move the file in order to use it
                        $randomName = rand(1, 10000000000);
                        $randomName = $randomName . '.xlsx';
                        $directory = __DIR__.'/../../../../tmp';
                        $data['file']->move($directory, $randomName);
        
                        if (null !== $data['file']) {
                            $fileName = $data['file']->getClientOriginalName();
                            $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                        }
        
                        if ($data['file']->document == 'xlsx') { 
                            
                            $inputFileName = $directory . '/' . $randomName;
                            $phpExcel = PHPExcel_IOFactory::load($inputFileName);
          
                            $worksheet = $phpExcel->getSheet(0);
                                
                            $fileEvaluationExcel = trim($worksheet->getCellByColumnAndRow(1, 1)->getValue());
                            $fileTypeExcel = trim($worksheet->getCellByColumnAndRow(1, 2)->getValue());
                                
                            if (in_array($fileEvaluationExcel, array('evaluateds', 'selves'))) {
                                
                                if (in_array($fileTypeExcel, array('working', 'school', 'university'))) {
                                    
                                    // Data defaults
                                    
                                    $typeIdentity = '';
                                    $birthday = '';
                                    
                                    $i = 0;
                                    $row = 5;
                                    $group = -1;
                                    $subgroup = '';
                                    $people = array();
                                    
                                    do {
                                        
                                        $field = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                                        
                                        if (in_array($field, array('group', 'GROUP'))) {
                                            $group++;
                                        }
                                        elseif (in_array($field, array('evaluateds', 'EVALUATEDS', 'evaluateds/evaluators', 'EVALUATEDS/EVALUATORS'))) {
                                            $subgroup = 'evaluateds';
                                            $i = 0;
                                        }
                                        elseif (in_array($field, array('evaluators', 'EVALUATORS'))) {
                                            $subgroup = 'evaluators';
                                            $i = 0;
                                        }
                                        else {
                                            
                                            // Data default
                                            
                                            $code = '';
                                            $names = '';
                                            $birthday = '';
                                            $email = '';
                                            $emailInformation = '';
                                            
                                            $headquarterId = '';
                                            $headquarterName = '';
                                            $status = 0;
                                            
                                            $ouId = '';
                                            $ouName = '';
                                            $ouUnitType = '';
                                            $chargeId = '';
                                            $chargeName = '';
                                            
                                            $facultyCode = '';
                                            $gradeCode = '';
                                            $studyYear = '';
                                            $academicYear = '';
                                            
                                            $courseId = '';
                                            $courseCode = '';
                                            $courseName = '';
                                            $section = '';
                                            
                                            // Data validation
                                            
                                            $message = '';
                                            $valid = 1;
                                            
                                            // Get Data
                                            
                                            $type = strtolower(trim($worksheet->getCellByColumnAndRow(0, $row)->getValue()));
                                            $rol = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                                            
                                            // Data User
                                            
                                            $surname = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                                            $lastname = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                                            $names = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                                            $typeIdentityRow = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
                                            if (in_array($typeIdentityRow, array('d', 'e', 'p'))) {
                                                $typeIdentity = User::getDocumentTypeByEquivalent($typeIdentityRow);
                                            }
                                            $numberIdentity = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
                                            $birthdayRow = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                                            if ($birthdayRow) {
                                                $birthday = \DateTime::createFromFormat('d/m/Y', $birthdayRow);
                                            }
                                            $sex = trim($worksheet->getCellByColumnAndRow(8, $row)->getValue());
                                            $email = trim($worksheet->getCellByColumnAndRow(9, $row)->getValue());
                                            
                                            // Generate Code
                                            $code = $numberIdentity;
                                            
                                            // Data Working
                                            
                                            $headquarterCode = trim($worksheet->getCellByColumnAndRow(10, $row)->getValue());
                                            $emailInformation = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                            
                                            if ($type == 'working') {
                                                
                                                $ouCode = trim($worksheet->getCellByColumnAndRow(11, $row)->getValue());
                                                $chargeCode = trim($worksheet->getCellByColumnAndRow(12, $row)->getValue());
                                                if ($fileTypeExcel == 'school') {
                                                    $evaluatorRol = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                                }
                                                elseif ($fileTypeExcel == 'university') {
                                                    $evaluatorRol = trim($worksheet->getCellByColumnAndRow(19, $row)->getValue());
                                                }
                                                elseif ($fileTypeExcel == 'working') {
                                                    $evaluatorRol = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                                }
                                                
                                            }
                                            elseif ($type == 'school') {
                                                
                                                $gradeCode = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                                $studyYear = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                                $section = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                                $courseCode = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                                $evaluatorRol = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                                
                                            }
                                            elseif ($type == 'university') {
                                                
                                                // Data University
                                                
                                                $emailInformation = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                                $facultyCode = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                                $careerCode = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                                $academicYear = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                                $section = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                                $courseCode = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                                $evaluatorRol = trim($worksheet->getCellByColumnAndRow(19, $row)->getValue());
                                                
                                                $facultyId = '';
                                                $facultyName = '';
                                                $careerId = '';
                                                $careerName = '';
                                                
                                            }
                                            
                                            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
                                            $userEmail = $repository->createQueryBuilder('u')
                                                ->where('u.email = :email 
                                                        AND u.numberidentity <> :numberidentity')
                                                ->setParameter('email', $email)
                                                ->setParameter('numberidentity', $numberIdentity)
                                                ->getQuery()
                                                ->getResult();
                                            if ($userEmail) {
                                                $message .= '<p>El correo lo está usando otro usuario.</p>';
                                                $valid = 0;
                                            }
                                            
                                            // Verify code in pollapllication
                                            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                                            $peaoplepas = $repository->createQueryBuilder('pa')
                                                ->select('COUNT(pa.id) evaluations')
                                                ->where('wiev.code = :evaluated
                                                        OR wier.code = :evaluator')
                                                ->andWhere('pa.pollscheduling <> :pollscheduling')
                                                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wiev', 'WITH', 'pa.evaluated_id = wiev.id')
                                                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wier', 'WITH', 'pa.evaluator_id = wier.id')
                                                ->setParameter('evaluated', $code)
                                                ->setParameter('evaluator', $code)
                                                ->setParameter('pollscheduling', $pollschedulingid)
                                                ->groupBy('pa.id')
                                                ->getQuery()
                                                ->getResult();
                                            
                                            if (!empty($peaoplepas)) {
                                                $message .= '<p>Participa en otra encuesta, se actualizará la información laboral, verificar los datos.</p>';
                                            }
                                            
                                            // Finish do while
                                            
                                            if ($code == '') {
                                                break;
                                            }
                                            
                                            // Validate Register
                                            
                                            if ($code == '' && $subgroup == 'evaluateds') {
                                                $message .= '<p>Falta el código del evaluado</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($code == '' && $subgroup == 'evaluators') {
                                                $message .= '<p>Falta el código del evaluador</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($names == '') {
                                                $message .= '<p>Falta el nombre de la persona.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if (!$typeIdentity) {
                                                $message .= '<p>El tipo de documento de identidad no es correcto, 
                                                                debe colocar d, e, p. d = dni, e = carné de extranjería, p = pasaporte.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($headquarterCode == '') {
                                                $message .= '<p>Falta el código de la sede.</p>';
                                                $validInformation = 0;
                                            }
                                            else {
                                                $h = $em->getRepository('FishmanEntityBundle:Headquarter')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $headquarterCode , 'status' => 1));
                                                if ($h) {
                                                    $headquarterId = $h->getId();
                                                    $headquarterName = $h->getName();
                                                }
                                                else {
                                                    $message .= '<p>El código de sede no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                            // Validate Information
                                            
                                            if ($type == 'working') {
                                                
                                                if ($emailInformation == '') {
                                                    $message .= '<p>Falta el correo laboral.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($ouCode == '') {
                                                    $message .= '<p>Falta el código de la unidad organizativa.</p>';
                                                    $valid = 0;
                                                }
                                                else {
                                                    $ou = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->findOneBy(
                                                        array( 'company' => $companyId , 'code' => $ouCode , 'status' => 1));
                                                    if ($ou) {
                                                        $ouId = $ou->getId();
                                                        $ouName = $ou->getName();
                                                        $ouUnitType = $ou->getUnitType();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de unidad organizativa no existe o está inactivo.</p>';
                                                        $valid = 0;
                                                    }
                                                }
                                                
                                                if ($chargeCode == '') {
                                                    $message .= '<p>Falta el código del cargo.</p>';
                                                    $valid = 0;
                                                }
                                                else {
                                                    $charge = $em->getRepository('FishmanEntityBundle:Companycharge')->findOneBy(
                                                        array( 'company' => $companyId , 'code' => $chargeCode , 'status' => 1));
                                                    if ($charge) {
                                                        $chargeId = $charge->getId();
                                                        $chargeName = $charge->getName();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de cargo no existe o está inactivo.</p>';
                                                        $valid = 0;
                                                    }
                                                }
                                                
                                            }
                                            elseif ($type == 'school') {
                                                
                                                if ($gradeCode == '') {
                                                    $message .= '<p>Falta el código de grado.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($studyYear == '') {
                                                    $message .= '<p>Falta el año de estudio.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($courseCode == '') {
                                                    $message .= '<p>Falta el código del curso</p>';
                                                }
                                                else {
                                                    $co = $em->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                                        array('code' => $courseCode, 'company' => $pollscheduling->getCompanyId(), 'status' => 1));
                                                    if ($co) {
                                                        $courseName = $co->getName();
                                                        $courseId = $co->getId();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de curso no existe o esta inactivo.</p>';
                                                    }
                                                }
                                            
                                            }
                                            elseif ($type == 'university') {
                                                
                                                if ($emailInformation == '') {
                                                    $message .= '<p>Falta el correo universitario.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($facultyCode == '') {
                                                    $message .= '<p>Falta el código de la facultad.</p>';
                                                    $valid = 0;
                                                }
                                                else {
                                                    $faculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->findOneBy(
                                                        array( 'company' => $companyId , 'code' => $facultyCode , 'status' => 1));
                                                    if ($faculty) {
                                                        $facultyId = $faculty->getId();
                                                        $facultyName = $faculty->getName();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de facultad no existe o está inactivo.</p>';
                                                        $valid = 0;
                                                    }
                                                }
                                                
                                                if ($careerCode == '') {
                                                    $message .= '<p>Falta el código de la carrera.</p>';
                                                    $valid = 0;
                                                }
                                                else {
                                                    $career = $em->getRepository('FishmanEntityBundle:Companycareer')->findOneBy(
                                                        array( 'company' => $companyId , 'code' => $careerCode , 'status' => 1));
                                                    if ($career) {
                                                        $careerId = $career->getId();
                                                        $careerName = $career->getName();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de carrera no existe o está inactivo.</p>';
                                                        $valid = 0;
                                                    }
                                                }
                                                
                                                if ($academicYear == '') {
                                                    $message .= '<p>Falta el ciclo académico.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($courseCode == '') {
                                                    $message .= '<p>Falta el código del curso</p>';
                                                }
                                                else {
                                                    $co = $em->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                                        array('code' => $courseCode, 'company' => $pollscheduling->getCompanyId(), 'status' => 1));
                                                    if ($co) {
                                                        $courseName = $co->getName();
                                                        $courseId = $co->getId();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de curso no existe o esta inactivo.</p>';
                                                    }
                                                }
                                                
                                            }
                                            
                                            if ($subgroup == 'evaluators') {
                                                if (!in_array($evaluatorRol, array('boss', 'pair', 'collaborator', 'self'))) {
                                                    $message .= '<p>El rol de evaluador no es válido, no se asignará el rol.</p>';
                                                }
                                            }
                                            
                                            if($valid && $message == ''){
                                                $message = 'OK';
                                            }
                                            
                                            // Array evaluations
                                            
                                            $people['groups'][$group][$subgroup][$i] = array(
                                                'rol' => $rol,
                                                'code' => $code,
                                                'surname' => $surname,
                                                'lastname' => $lastname,
                                                'names' => $names,
                                                'sex' => $sex,
                                                'identity' => $typeIdentity,
                                                'numberidentity' => $numberIdentity,
                                                'birthday' => $birthday,
                                                'email' => $email,
                                                'company_id' => $companyId,
                                                'company_code' => $companyCode,
                                                'company_name' => $companyName,
                                                'company_type' => $companyType,
                                                'headquarter_id' => $headquarterId,
                                                'headquarter_name' => $headquarterName,
                                                'email_information' => $emailInformation,
                                                'message' => $message,
                                                'valid' => $valid
                                            );
                                            
                                            if ($type == 'working') {
                                                
                                                $people['groups'][$group][$subgroup][$i]['wi_type'] = 'workinginformation';
                                                $people['groups'][$group][$subgroup][$i]['ou_id'] = $ouId;
                                                $people['groups'][$group][$subgroup][$i]['ou_name'] = $ouName;
                                                $people['groups'][$group][$subgroup][$i]['ou_unit_type'] = $ouUnitType;
                                                $people['groups'][$group][$subgroup][$i]['charge_id'] = $chargeId;
                                                $people['groups'][$group][$subgroup][$i]['charge_name'] = $chargeName;
                                                $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                                $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                                $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                                $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                                
                                            }
                                            elseif ($type == 'school') {
                                                
                                                $people['groups'][$group][$subgroup][$i]['wi_type'] = 'schoolinformation';
                                                $people['groups'][$group][$subgroup][$i]['grade_code'] = $gradeCode;
                                                $people['groups'][$group][$subgroup][$i]['study_year'] = $studyYear;
                                                $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                                $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                                $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                                $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                                
                                            }
                                            elseif ($type == 'university') {
                                                
                                                $people['groups'][$group][$subgroup][$i]['wi_type'] = 'universityinformation';
                                                $people['groups'][$group][$subgroup][$i]['faculty_id'] = $facultyId;
                                                $people['groups'][$group][$subgroup][$i]['faculty_name'] = $facultyName;
                                                $people['groups'][$group][$subgroup][$i]['career_id'] = $careerId;
                                                $people['groups'][$group][$subgroup][$i]['career_name'] = $careerName;
                                                $people['groups'][$group][$subgroup][$i]['academic_year'] = $academicYear;
                                                $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                                $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                                $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                                $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                                
                                            }
                                            
                                            $people['groups'][$group][$subgroup][$i]['evaluator_rol'] = $evaluatorRol;
                                            
                                            $i++;
                                        }
                                        
                                        $row++;
                                        
                                    } while (true);
                                    
                                    $i = 0;
                                    $evaluations = array();
                                    
                                    if ($fileEvaluationExcel == 'evaluateds') {
                                        foreach ($people['groups'] as $group) {
                                            foreach ($group['evaluators'] as $evaluator) {
                                                foreach ($group['evaluateds'] as $evaluated) {
                                                    $evaluations[$i]['evaluator'] = $evaluator;
                                                    $evaluations[$i]['evaluated'] = $evaluated;
                                                    $i++;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        foreach ($people['groups'] as $group) {
                                            foreach ($group['evaluateds'] as $evaluated) {
                                                $evaluations[$i]['evaluator'] = $evaluated;
                                                $evaluations[$i]['evaluated'] = $evaluated;
                                                $i++;
                                            }
                                        }
                                    }
                                    
                                }
                                else {
                                    $session->getFlashBag()->add('error', 'El tipo de empresa de la plantilla es erroneo debe ser "school", "university".');
                                }
                                
                            }
                            else {
                                $session->getFlashBag()->add('error', 'El tipo de evaluación de la plantilla es erroneo debe ser "evaluateds".');
                            }
                            
                        }
                        else {
                            $session->getFlashBag()->add('error', 'El archivo a importar debe ser de formato excel');
                        }
                        
                        if (count($evaluations) > 0 and is_array($evaluations)) {
                            $temporal = new Temporalimportdata();
                            $temporal->setData($evaluations);
                            $temporal->setEntity('evaluations');
                            $temporal->setDatetime(new \DateTime());
                            $em->persist($temporal);
                            $em->flush();
        
                            // Secondary form
                            $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                            $form2 = $this->createFormBuilder($defaultData2)
                                ->add('temporalimportdata_id',
                                      'integer', array (
                                               'required' => true 
                                     ))
                                ->getForm();
                            $form2 = $form2->createView();
                        }
                        else {
                            $session->getFlashBag()->add('error', 'No hay evaluaciones para importar.');
                        }
                        
                        //Delete temporal file
                        if ($data['file']->document == 'xlsx') {
                            unlink($inputFileName);
                        }
                    }
                } //End is method post
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Evaluator:import.html.twig', array(
                    'form' => $form->createView(),
                    'form2' => $form2,
                    'workshopscheduling' => $workshopscheduling,
                    'pollscheduling' => $pollscheduling,
                    'evaluations' => $evaluations,
                    'pollapplicationimport' => FALSE,
                    'companytype' => $companyType
                ));
                
            }
            else {
                $session->getFlashBag()->add('error', 'No se puede encontrar la Encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $type_evaluation = $_GET['type_evaluation'];
        $type = $_GET['type'];
        $filename = '';
        
        switch ($type_evaluation) {
            case 'regular':
                switch ($type) {
                    case 'working':
                        $filename = 'importar-evaluaciones-taller-laboral.xlsx';
                        break;
                    case 'school':
                        $filename = 'importar-evaluaciones-taller-colegio.xlsx';
                        break;
                    case 'university':
                        $filename = 'importar-evaluaciones-taller-universidad.xlsx';
                        break;
                }
                break;
            case 'self':
                switch ($type) {
                    case 'working':
                        $filename = 'importar-evaluaciones-taller-laboral-autoevaluacion.xlsx';
                        break;
                    case 'school':
                        $filename = 'importar-evaluaciones-taller-colegio-autoevaluacion.xlsx';
                        break;
                    case 'university':
                        $filename = 'importar-evaluaciones-taller-universidad-autoevaluacion.xlsx';
                        break;
                }
                break;
        }

        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"'
        );
        
        if ($type_evaluation == 'regular') {
            return new Response(file_get_contents( __DIR__.'/../../../../templates/WorkshopBundle/Workshopscheduling/Poll/' . $filename), 200, $headers);
        }
        else {
            return new Response(file_get_contents( __DIR__.'/../../../../templates/WorkshopBundle/Workshopscheduling/Poll/SelfEvaluation/' . $filename), 200, $headers);
        }
    }
    
    /**
     * Lists all Pollapplication entities.
     *
     */
    public function indexPollEvaluatorNotEvaluatedsAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous() || $pollscheduling->getEntityType() != 'pollscheduling') {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('pollscheduling'));
            }
            
            if (!$pollscheduling->getDeleted()) {
                
                // Find Entities
                
                $defaultData = array(
                    'evaluator_code' => '', 
                    'evaluator_names' => '', 
                    'evaluator_surname' => '', 
                    'evaluator_lastname' => ''
                );
                $formData = array();
                $form = $this->createFormBuilder($defaultData)
                    ->add('evaluator_code', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_names', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_surname', 'text', array(
                        'required' => FALSE
                    ))
                    ->add('evaluator_lastname', 'text', array(
                        'required' => FALSE
                    ))
                    ->getForm();
        
                $data = array(
                    'evaluator_code' => '', 
                    'evaluator_names' => '', 
                    'evaluator_surname' => '', 
                    'evaluator_lastname' => ''
                );
                if (isset($_GET['form'])) {
                    $formData = $_GET['form'];
                }
                if ($request->getMethod() == 'GET') {
                    $form->bindRequest($request);
                    $data = $form->getData();
                }
                
                // Delete registers
                
                $deletes_form = $this->createFormBuilder()
                    ->add('registers', 'choice', array(
                        'required' => false, 
                        'multiple' => true, 
                        'expanded' => true
                    ))
                    ->getForm();
                    
                // Query
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                $queryBuilder = $repository->createQueryBuilder('pa')
                    ->select('pa.id, wi.code evaluator_code, ue.names evaluator_names, ue.surname evaluator_surname, 
                              ue.lastname evaluator_lastname, pa.changed, u.names, u.surname, u.lastname')
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                    ->innerJoin('wi.user', 'ue')
                    ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pa.modified_by = u.id')
                    ->where('pa.pollscheduling = :pollscheduling 
                            AND pa.deleted = :deleted')
                    ->setParameter('pollscheduling', $pollschedulingid)
                    ->setParameter('deleted', FALSE)
                    ->orderBy('pa.id', 'ASC');
            
                // Add arguments
                
                if ($data['evaluator_code'] != '') {
                    $queryBuilder
                        ->andWhere('wi.code LIKE :evaluator_code')
                        ->setParameter('evaluator_code', '%' . $data['evaluator_code'] . '%');
                }
                if ($data['evaluator_names'] != '') {
                    $queryBuilder
                        ->andWhere('ue.names LIKE :evaluator_names')
                        ->setParameter('evaluator_names', '%' . $data['evaluator_names'] . '%');
                }
                if ($data['evaluator_surname'] != '') {
                    $queryBuilder
                        ->andWhere('ue.surname LIKE :evaluator_surname')
                        ->setParameter('evaluator_surname', '%' . $data['evaluator_surname'] . '%');
                }
                if ($data['evaluator_lastname'] != '') {
                    $queryBuilder
                        ->andWhere('ue.lastname LIKE :evaluator_lastname')
                        ->setParameter('evaluator_lastname', '%' . $data['evaluator_lastname'] . '%');
                }
                
                $query = $queryBuilder->getQuery();
                
                // Paginator
                
                $paginator = $this->get('ideup.simple_paginator');
                $paginator->setItemsPerPage(20, 'workshoppollapplication');
                $paginator->setMaxPagerItems(5, 'workshoppollapplication');
                $entities = $paginator->paginate($query, 'workshoppollapplication')->getResult();
                
                $startPageItem = $paginator->getStartPageItem('workshoppollapplication');
                $endPageItem = $paginator->getEndPageItem('workshoppollapplication');
                $totalItems = $paginator->getTotalItems('workshoppollapplication');
                
                if ($totalItems == 0) {
                    $info_paginator = 'No hay registros que mostrar';
                }
                else {
                    $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
                }
                
                // Pollscheduling Info
                
                $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
                
                // Return Template
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/EvaluatorNotEvaluateds:index.html.twig', array(
                    'entities' => $entities,
                    'workshopscheduling' => $workshopscheduling,
                    'pollscheduling' => $pollscheduling,
                    'form' => $form->createView(),
                    'deletes_form' => $deletes_form->createView(),
                    'paginator' => $paginator,
                    'info_paginator' => $info_paginator,
                    'form_data' => $formData
                ));
               
            }
            else {
                $session->getFlashBag()->add('error', 'La encuesta de la Programación de Taller ha sido eliminada.');
                
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Pollapplication entity.
     *
     */
    public function newPollEvaluatorNotEvaluatedsAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            // Recover Pollscheduling
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
            $result = $repository->createQueryBuilder('ps')
                ->select('ps.id, ps.title, ps.type, ps.entity_type, ps.entity_id, ps.version, 
                          c.company_type, c.id company_id, c.name company_name, 
                          ps.initdate, ps.enddate, ps.deleted')
                ->innerJoin('FishmanEntityBundle:Company', 'c', 'WITH', 'ps.company_id = c.id')
                ->where('ps.id = :pollscheduling 
                        AND ps.entity_type = :type
                        AND ps.status = 1')
                ->setParameter(':pollscheduling', $pollschedulingid)
                ->setParameter(':type', 'workshopscheduling')
                ->getQuery()
                ->getResult();
            
            $pollscheduling = current($result);
            
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
        
            if (!$pollscheduling['deleted']) {
                
                if (!$pollscheduling['initdate']) {
                    $session->getFlashBag()->add('error', 'Debe ingresar los datos faltantes para la Encuesta de la Programación de Taller.');
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_edit', array(
                        'id' => $pollschedulingid
                    )));
                }
                
                // Recovering data
                $course_options = Companycourse::getListCompanycourseOptions($this->getDoctrine(), $pollscheduling['company_id']);
                
                // Find Workinginformations
                
                $defaultData = array( 
                    'iden' => '',
                    'evaluator_name' => '',
                    'attrib' => '', 
                    'course' => '', 
                    'section' => ''
                );
                $form = $this->createFormBuilder($defaultData)
                    ->add('iden', 'hidden', array(
                        'required'=>true
                    ))
                    ->add('evaluator_name', 'text', array(
                        'required'=>false
                    ))
                    ->add('attrib', 'choice', array(
                        'choices' => array(
                            '0' => 'No',
                            '1' => 'Si',
                        ), 
                        'empty_value' => 'Choose an option',
                        'required'=>false
                    ))
                    ->add('course', 'choice', array(
                        'choices' => $course_options, 
                        'empty_value' => 'Choose an option',
                        'required'=>false
                    ))
                    ->add('section', 'text', array(
                        'required'=>false
                    ))
                    ->getForm();
        
                $data = array(
                    'iden' => '',
                    'evaluator_name' => '', 
                    'attrib' => '', 
                    'course' => '', 
                    'section' => ''
                );
                if($request->getMethod() == 'POST') {
                    $form->bindRequest($request);
                    $data = $form->getData();
                }
                
                $repository = $this->getDoctrine()->getManager()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $workinginformations = $repository->createQueryBuilder('wa')
                    ->select('wi.id, wi.type, wi.code, u.names, u.surname, u.lastname')
                    ->where('wi.company = :company
                            AND wa.workshopscheduling = :workshopscheduling
                            AND wa.deleted = :deleted')
                    ->innerJoin('wa.workinginformation', 'wi')
                    ->innerJoin('wi.user', 'u')
                    ->setParameter('company', $pollscheduling['company_id'])
                    ->setParameter('workshopscheduling', $pollscheduling['entity_id'])
                    ->setParameter('deleted', FALSE)
                    ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC')
                    ->getQuery()
                    ->getResult();
                
                for ($i = 0; $i < count($workinginformations); $i++) {
                    $workinginformations[$i]['value'] = $workinginformations[$i]['names'] . ' ' . 
                                                        $workinginformations[$i]['surname'] . ' ' . 
                                                        $workinginformations[$i]['surname'] . ' (' . 
                                                        Workinginformation::getTypeName($workinginformations[$i]['type']) . ')';
                }
    
                $workinginformations = 'var workinginformations = ' . json_encode($workinginformations);
                
                // Return new.html.twig            
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/EvaluatorNotEvaluateds:new.html.twig', array(
                    'workshopscheduling' => $workshopscheduling,
                    'pollscheduling' => $pollscheduling,
                    'form' => $form->createView(),
                    'jsonworkinginformations' => $workinginformations,
                ));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Encuesta de la Programación de Taller ha sido eliminada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Creates a new Pollapplication entity.
     *
     */
    public function asignedsPollEvaluatorNotEvaluatedsAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || $pollscheduling->getPollschedulingAnonymous()) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        $data = $request->request->get('form');
        
        if ($data['iden'] != '') {
            
            $entityAT = 'workshopapplication';
            $entityId = $workshopschedulingid;
            
            $workinginformations = '';
            $attArray = array();
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            // Recovering data
            
            $evaluator[] = $data['iden'];
            $evaluated = NULL;
            $attArray[0]= $data['course'];
            $attArray[1]= $data['section'];
            
            // Add Pollapplications
            
            if (in_array($company->getCompanyType(), array('school', 'university'))) {
                $numberRegister = Pollapplication::addPollapplications(
                    $this->getDoctrine(), 
                    $evaluator, 
                    $evaluated, 
                    $entityAT, 
                    $pollscheduling, 
                    $userBy, 
                    $entityId,
                    $attArray
                );
            }
            else {
                $numberRegister = Pollapplication::addPollapplications(
                    $this->getDoctrine(), 
                    $evaluator, 
                    $evaluated, 
                    $entityAT, 
                    $pollscheduling, 
                    $userBy, 
                    $entityId
                );
            }
         
            $session->getFlashBag()->add('status', 'El evaluador fue asignado a la encuesta satisfactoriamente.');
        }
        else {
            $session->getFlashBag()->add('error', 'No escogio ningún evaluador para asignar a la encuesta.');
        }
        
        if($request->request->get('saveandclose', TRUE)){
            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
                'workshopschedulingid' => $workshopschedulingid, 
                'pollschedulingid' => $pollschedulingid
            )));
        }
        else {
            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds_new', array(
                'workshopschedulingid' => $workshopschedulingid, 
                'pollschedulingid' => $pollschedulingid
            )));
        }
    }

    /**
     * Drop an Pollapplication not evaluateds entity.
     *
     */
    public function dropPollEvaluatorNotEvaluatedsAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $query = $repository->createQueryBuilder('pa')
                ->select('pa.id, wi.id evaluator_id, ue.names evaluator_names, ue.surname evaluator_surname, 
                          ue.lastname evaluator_lastname, pa.deleted, ps.id pollscheduling_id, ps.title pollscheduling_title, 
                          ps.pollscheduling_anonymous anonymous, ps.version pollscheduling_version, 
                          ps.deleted pollscheduling_deleted, ps.initdate, ps.enddate')
                ->innerJoin('pa.pollscheduling', 'ps')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                ->innerJoin('wi.user', 'ue')
                ->where('pa.id = :pollapplication 
                        AND pa.evaluated_id IS NULL')
                ->setParameter('pollapplication', $id)
                ->getQuery()
                ->getResult();
                
            $entity = current($query);
            
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
        
            if (!$entity['pollscheduling_deleted']) {
              
                if ($entity['deleted']) {
                    $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la encuesta de la Programación de Taller.');
                    return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
                        'workshopschedulingid' => $workshopschedulingid, 
                        'pollschedulingid' => $entity['pollscheduling_id']
                    )));
                }
                
                $deleteForm = $this->createDeleteForm($id);
                
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/EvaluatorNotEvaluateds:drop.html.twig', array(
                    'entity' => $entity,
                    'workshopscheduling' => $workshopscheduling, 
                    'delete_form' => $deleteForm->createView()
                ));
                
            }
            else {
                $session->getFlashBag()->add('error', 'La encuesta de la Programación de Taller ha sido eliminada.');
                
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Drop multiple an Pollapplication not evaluateds entities.
     *
     */
    public function dropmultiplePollEvaluatorNotEvaluatedsAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            // Recover Pollscheduling
            
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
              
            // Recover register of form
            
            $data = $request->request->get('form_deletes');
            
            if (isset($data['registers'])) {
            
                $registers = '';
                $registers_count = count($data['registers']);
                $i = 1;
                
                if (!empty($data['registers'])) {
                    
                    foreach($data['registers'] as $r) {
                        $registers .= $r;
                        if ($i < $registers_count) {
                            $registers .= ',';
                        }
                        $i++;
                    }
        
                    // Recover pollapplication of pollscheduling
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                    $entities = $repository->createQueryBuilder('pa')
                        ->select('pa.id, wi.id evaluator_id, ue.names evaluator_names, ue.surname evaluator_surname, 
                                  ue.lastname evaluator_lastname')
                        ->innerJoin('pa.pollscheduling', 'ps')
                        ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id')
                        ->innerJoin('wi.user', 'ue')
                        ->where('pa.id IN(' . $registers . ')
                              AND pa.evaluated_id IS NULL')
                        ->getQuery()
                        ->getResult();
                        
                    if (!$entities) {
                        $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la encuesta de la Programación de Taller.');
                        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                            'workshopschedulingid' => $workshopschedulingid, 
                            'pollschedulingid' => $pollschedulingid
                        )));
                    }
                
                    $deleteMultipleForm = $this->createDeleteMultipleForm($registers);
                
                    return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/EvaluatorNotEvaluateds:dropmultiple.html.twig', array(
                        'entities' => $entities,
                        'workshopscheduling' => $workshopscheduling,
                        'pollscheduling' => $pollscheduling,
                        'delete_form' => $deleteMultipleForm->createView()
                    ));
                }
            }
            
            $session->getFlashBag()->add('error', 'No se ha seleccionado ningún registro para eliminar.');
            
            return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
                'workshopschedulingid' => $workshopschedulingid,
                'pollschedulingid' => $pollschedulingid
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Drop all an Pollapplication not evaluateds entities.
     *
     */
    public function dropallPollEvaluatorNotEvaluatedsAction($workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            // Recover Pollscheduling
            
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $deleteAllForm = $this->createDeleteAllForm();
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/EvaluatorNotEvaluateds:dropall.html.twig', array(
                'workshopscheduling' => $workshopscheduling,
                'pollscheduling' => $pollscheduling,
                'delete_all_form' => $deleteAllForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
        
    }

    /**
     * Delete a Pollapplication entity.
     *
     */
    public function deletePollEvaluatorNotEvaluatedsAction(Request $request, $workshopschedulingid, $pollschedulingid, $id)
    {    
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la evaluación asignada a la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
            
            $entity->setDeleted(TRUE);

            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
            'workshopschedulingid' => $workshopschedulingid, 
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Deletes multiple a Pollapplication entities.
     *
     */
    public function deletemultiplePollEvaluatorNotEvaluatedsAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        $form = $request->request->get('form');
        
        if ($form['registers'] != '') {
            
            // Recover Pollapplications of form

            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $entities = $repository->createQueryBuilder('pa')
                ->where('pa.id IN(' . $form['registers'] . ')')
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No se puede encontrar las evaluaciones asignadas a la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
          
            foreach($entities as $entity) {
            
                Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
              
                $entity->setDeleted(TRUE);
    
                $em->persist($entity);
                $em->flush();
            }
            
            $session->getFlashBag()->add('status', 'Los registros fueron eliminados satisfactoriamente.');

        }

        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
            'workshopschedulingid' => $workshopschedulingid, 
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Deletes all Pollapplication not evaluateds entities.
     *
     */
    public function deleteallPollEvaluatorNotEvaluatedsAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $form = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();   
        
        if ($form['delete_all']) {
            
            // Recover Pollapplications of form
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $entities = $repository->createQueryBuilder('pa')
                ->where('pa.pollscheduling = :pollscheduling')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No existen evaluaciones en esta Encuesta de Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            foreach($entities as $entity) {
                
                Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'pollscheduling', $entity->getPollscheduling()->getId(), $entity->getEvaluatedId(), 'pollapplication', $entity->getId());
                
                $entity->setDeleted(TRUE);
                
                $em->persist($entity);
                $em->flush();
            }
            $session->getFlashBag()->add('status', 'Los registros fueron eliminados satisfactoriamente.');
            
        }

        return $this->redirect($this->generateUrl('workshopschedulingpollevaluator_notevaluateds', array(
            'workshopschedulingid' => $workshopschedulingid, 
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Import Workshopschedulingpoll evaluator entities.
     *
     */
    public function importPollEvaluatorNotEvaluatedsAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Mensaje de que no hay registros
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Recover Pollscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $companyId = $workshopscheduling->getCompany()->getId();
            $companyCode = $workshopscheduling->getCompany()->getCode();
            $companyName = $workshopscheduling->getCompany()->getName();
            $companyType = $workshopscheduling->getCompany()->getCompanyType();
            
            if (!$pollscheduling->getDeleted()) {
                
                if (!$pollscheduling->getInitdate()) {
                    $session->getFlashBag()->add('error', 'Debe ingresar los datos faltantes para la Encuesta de la Programación de Taller.');
                    return $this->redirect($this->generateUrl('workshopschedulingpoll_edit', array(
                        'id' => $pollschedulingid
                    )));
                }
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplicationimport');
                $query = $repository->createQueryBuilder('pai')
                    ->select('COUNT(pai.id) cpais')
                    ->where('pai.pollscheduling_id = :pollscheduling')
                    ->setParameter('pollscheduling', $pollschedulingid)
                    ->groupBy('pai.pollscheduling_id')
                    ->getQuery()
                    ->getResult();
                
                $paimports = current($query);
                if ($paimports['cpais'] > 0) {
                    $session->getFlashBag()->add('status', 'Existen Evaluaciones pendientes a importar.');
                    return $this->render('FishmanPollBundle:Pollscheduling/Evaluator:import.html.twig', array(
                        'pollscheduling' => $pollscheduling,
                        'pollapplicationimport' => TRUE,
                        'companytype' => $companyType
                    ));
                }
                
                $defaultData = array();
                $form = $this->createForm(new WorkshoppollevaluatorimportType(), $defaultData);
    
                $form2 = false;
                $data = '';
                $people = false;
                $evaluations = false;
                $message = '';
                $messageError = '';
                $existingwiemail = '';
                $numberRegister = 0;
    
                if ($request->isMethod('POST')) {
                    
                    //The user is confirmed to persist data as 
                    $importFormData = $request->request->get('form', false);
        
                    if ($importFormData) {
                        
                        $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                             ->find($importFormData['temporalimportdata_id']);
                        if(!$temporalentity){
                          return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/EvaluatorNotEvaluateds:import.html.twig', array(
                              'form' => $form->createView(),
                              'form2' => $form2->createView(),
                              'workshopscheduling' => $workshopscheduling,
                              'pollscheduling' => $pollscheduling
                          ));
                        }
                        
                        $evaluations = $temporalentity->getData();
                        $userBy = $this->get('security.context')->getToken()->getUser();
                        
                        // suspend auto-commit
                        $em->getConnection()->beginTransaction();
                        
                        // Try and make the transaction
                        try {
                            
                            if (in_array($companyType, array('working', 'school', 'university'))) {
                                foreach ($evaluations as $evaluation) {
                                    if ($evaluation['evaluator']['valid']) {
                                        
                                        $evaluator = Pollpersonimport::addRegister($this->getDoctrine(), $evaluation['evaluator']);
                                        
                                        $pai = new Pollapplicationimport();
                                        $pai->setPollschedulingId($pollschedulingid);
                                        $pai->setEvaluatedId(NULL);
                                        $pai->setEvaluatorId($evaluator->getId());
                                        $pai->setEvaluatedCode(NULL);
                                        $pai->setEvaluatorCode($evaluation['evaluator']['code']);
                                        $pai->setCourse($evaluation['evaluator']['course_id']);
                                        $pai->setSection($evaluation['evaluator']['section']);
                                        $pai->setStatus(FALSE);
                                        
                                        $em->persist($pai);
                                        $em->flush();
                                        
                                        $numberRegister++;
                                    }
                                }
                            }
                            
                            //Delete temporal data
                            $em->remove($temporalentity);
                            $em->flush();
                            
                            // Delete temporal files
                            Workshopscheduling::deleteTemporalFiles('../tmp/');
                            
                            // Try and commit the transactioncurrentversioncurrentversion
                            $em->getConnection()->commit();
                        } catch (Exception $e) {
                            // Rollback the failed transaction attempt
                            $em->getConnection()->rollback();
                            throw $e;
                        }
                        
                        if ($numberRegister > 0) {
                            $session->getFlashBag()->add('status', 'Existen '.$numberRegister.' evaluaciones pendientes a importar.');
                            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/EvaluatorNotEvaluateds:import.html.twig', array(
                                'workshopscheduling' => $workshopscheduling,
                                'pollscheduling' => $pollscheduling,
                                'pollapplicationimport' => TRUE,
                                'companytype' => $companyType
                            ));
                        }
                        else {
                            $session->getFlashBag()->add('error', 'No hay evaluaciones para importar.');
                        }
                        
                        $people = false;
        
                    } //End have temporarlimportdata input
                    else {
                        
                        $form->bind($request);
                        $data = $form->getData();
        
                        // We need to move the file in order to use it
                        $randomName = rand(1, 10000000000);
                        $randomName = $randomName . '.xlsx';
                        $directory = __DIR__.'/../../../../tmp';
                        $data['file']->move($directory, $randomName);
        
                        if (null !== $data['file']) {
                            $fileName = $data['file']->getClientOriginalName();
                            $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                        }
        
                        if ($data['file']->document == 'xlsx') { 
                            
                            $inputFileName = $directory . '/' . $randomName;
                            $phpExcel = PHPExcel_IOFactory::load($inputFileName);
          
                            $worksheet = $phpExcel->getSheet(0);
                            
                            $fileEvaluationExcel = trim($worksheet->getCellByColumnAndRow(1, 1)->getValue());
                            $fileTypeExcel = trim($worksheet->getCellByColumnAndRow(1, 2)->getValue());
                                
                            if ($fileEvaluationExcel == 'evaluators') {
                                
                                if (in_array($fileTypeExcel, array('working', 'school', 'university'))) {
                                    
                                    // Data defaults
                                    
                                    $typeIdentity = '';
                                    $birthday = '';
                                    
                                    $i = 0;
                                    $row = 5;
                                    $group = -1;
                                    $subgroup = '';
                                    $people = array();
                                    
                                    do {
                                        
                                        $field = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                                        
                                        if (in_array($field, array('group', 'GROUP'))) {
                                            $group++;
                                        }
                                        elseif (in_array($field, array('evaluators', 'EVALUATORS'))) {
                                            $subgroup = 'evaluators';
                                            $i = 0;
                                        }
                                        else {
                                            
                                            // Data default
                                            
                                            $code = '';
                                            $names = '';
                                            $birthday = '';
                                            $email = '';
                                            $emailInformation = '';
                                            
                                            $headquarterId = '';
                                            $headquarterName = '';
                                            $status = 0;
                                            
                                            $ouId = '';
                                            $ouName = '';
                                            $ouUnitType = '';
                                            $chargeId = '';
                                            $chargeName = '';
                                            
                                            $facultyCode = '';
                                            $gradeCode = '';
                                            $studyYear = '';
                                            $academicYear = '';
                                            
                                            $courseId = '';
                                            $courseCode = '';
                                            $courseName = '';
                                            $section = '';
                                            
                                            // Data validation
                                            
                                            $message = '';
                                            $valid = 1;
                                            
                                            // Get Data
                                            
                                            $type = strtolower(trim($worksheet->getCellByColumnAndRow(0, $row)->getValue()));
                                            $rol = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                                            
                                            // Data User
                                            
                                            $surname = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                                            $lastname = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                                            $names = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                                            $typeIdentityRow = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
                                            if (in_array($typeIdentityRow, array('d', 'e', 'p'))) {
                                                $typeIdentity = User::getDocumentTypeByEquivalent($typeIdentityRow);
                                            }
                                            $numberIdentity = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
                                            $birthdayRow = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                                            if ($birthdayRow) {
                                                $birthday = \DateTime::createFromFormat('d/m/Y', $birthdayRow);
                                            }
                                            $sex = trim($worksheet->getCellByColumnAndRow(8, $row)->getValue());
                                            $email = trim($worksheet->getCellByColumnAndRow(9, $row)->getValue());
                                            
                                            // Generate Code
                                            $code = $numberIdentity;
                                            
                                            // Data Working
                                            
                                            $headquarterCode = trim($worksheet->getCellByColumnAndRow(10, $row)->getValue());
                                            $emailInformation = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                            
                                            if ($type == 'working') {
                                                
                                                $ouCode = trim($worksheet->getCellByColumnAndRow(11, $row)->getValue());
                                                $chargeCode = trim($worksheet->getCellByColumnAndRow(12, $row)->getValue());
                                                if ($fileTypeExcel == 'school') {
                                                    $newWI = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                                }
                                                elseif ($fileTypeExcel == 'university') {
                                                    $newWI = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                                }
                                                
                                            }
                                            elseif ($type == 'school') {
                                                
                                                $gradeCode = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                                $studyYear = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                                $section = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                                $courseCode = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                                $newWI = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                                
                                            }
                                            elseif ($type == 'university') {
                                                
                                                // Data University
                                                
                                                $emailInformation = trim($worksheet->getCellByColumnAndRow(13, $row)->getValue());
                                                $facultyCode = trim($worksheet->getCellByColumnAndRow(14, $row)->getValue());
                                                $careerCode = trim($worksheet->getCellByColumnAndRow(15, $row)->getValue());
                                                $academicYear = trim($worksheet->getCellByColumnAndRow(16, $row)->getValue());
                                                $section = trim($worksheet->getCellByColumnAndRow(17, $row)->getValue());
                                                $courseCode = trim($worksheet->getCellByColumnAndRow(18, $row)->getValue());
                                                $newWI = trim($worksheet->getCellByColumnAndRow(19, $row)->getValue());
                                                
                                                $facultyId = '';
                                                $facultyName = '';
                                                $careerId = '';
                                                $careerName = '';
                                                
                                            }
                                            
                                            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:User');
                                            $userEmail = $repository->createQueryBuilder('u')
                                                ->where('u.email = :email 
                                                        AND u.numberidentity <> :numberidentity')
                                                ->setParameter('email', $email)
                                                ->setParameter('numberidentity', $numberIdentity)
                                                ->getQuery()
                                                ->getResult();
                                            if ($userEmail) {
                                                $message .= '<p>El correo lo está usando otro usuario.</p>';
                                                $valid = 0;
                                            }
                                            
                                            // Verify code in pollapllication
                                            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                                            $peaoplepas = $repository->createQueryBuilder('pa')
                                                ->select('COUNT(pa.id) evaluations')
                                                ->where('wiev.code = :evaluated
                                                        OR wier.code = :evaluator')
                                                ->andWhere('pa.pollscheduling <> :pollscheduling')
                                                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wiev', 'WITH', 'pa.evaluated_id = wiev.id')
                                                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wier', 'WITH', 'pa.evaluator_id = wier.id')
                                                ->setParameter('evaluated', $code)
                                                ->setParameter('evaluator', $code)
                                                ->setParameter('pollscheduling', $pollschedulingid)
                                                ->groupBy('pa.id')
                                                ->getQuery()
                                                ->getResult();
                                            
                                            if (!empty($peaoplepas)) {
                                                $message .= '<p>Participa en otra encuesta, se actualizará la información laboral, verificar los datos.</p>';
                                            }
                                            
                                            // Finish do while
                                            
                                            if ($code == '') {
                                                break;
                                            }
                                            
                                            // Validate Register
                                            
                                            if ($code == '' && $subgroup == 'evaluateds') {
                                                $message .= '<p>Falta el código del evaluado</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($code == '' && $subgroup == 'evaluators') {
                                                $message .= '<p>Falta el código del evaluador</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($names == '') {
                                                $message .= '<p>Falta el nombre de la persona.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if (!$typeIdentity) {
                                                $message .= '<p>El tipo de documento de identidad no es correcto, 
                                                                debe colocar d, e, p. d = dni, e = carné de extranjería, p = pasaporte.</p>';
                                                $valid = 0;
                                            }
                                            
                                            if ($headquarterCode == '') {
                                                $message .= '<p>Falta el código de la sede.</p>';
                                                $validInformation = 0;
                                            }
                                            else {
                                                $h = $em->getRepository('FishmanEntityBundle:Headquarter')->findOneBy(
                                                    array( 'company' => $companyId , 'code' => $headquarterCode , 'status' => 1));
                                                if ($h) {
                                                    $headquarterId = $h->getId();
                                                    $headquarterName = $h->getName();
                                                }
                                                else {
                                                    $message .= '<p>El código de sede no existe o está inactivo.</p>';
                                                    $valid = 0;
                                                }
                                            }
                                            
                                            // Validate Information
                                            
                                            if ($type == 'working') {
                                                
                                                if ($emailInformation == '') {
                                                    $message .= '<p>Falta el correo laboral.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($ouCode == '') {
                                                    $message .= '<p>Falta el código de la unidad organizativa.</p>';
                                                    $valid = 0;
                                                }
                                                else {
                                                    $ou = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->findOneBy(
                                                        array( 'company' => $companyId , 'code' => $ouCode , 'status' => 1));
                                                    if ($ou) {
                                                        $ouId = $ou->getId();
                                                        $ouName = $ou->getName();
                                                        $ouUnitType = $ou->getUnitType();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de unidad organizativa no existe o está inactivo.</p>';
                                                        $valid = 0;
                                                    }
                                                }
                                                
                                                if ($chargeCode == '') {
                                                    $message .= '<p>Falta el código del cargo.</p>';
                                                    $valid = 0;
                                                }
                                                else {
                                                    $charge = $em->getRepository('FishmanEntityBundle:Companycharge')->findOneBy(
                                                        array( 'company' => $companyId , 'code' => $chargeCode , 'status' => 1));
                                                    if ($charge) {
                                                        $chargeId = $charge->getId();
                                                        $chargeName = $charge->getName();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de cargo no existe o está inactivo.</p>';
                                                        $valid = 0;
                                                    }
                                                }
                                                
                                            }
                                            elseif ($type == 'school') {
                                                
                                                if ($gradeCode == '') {
                                                    $message .= '<p>Falta el código de grado.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($studyYear == '') {
                                                    $message .= '<p>Falta el año de estudio.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($courseCode == '') {
                                                    $message .= '<p>Falta el código del curso</p>';
                                                }
                                                else {
                                                    $co = $em->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                                        array('code' => $courseCode, 'company' => $pollscheduling->getCompanyId(), 'status' => 1));
                                                    if ($co) {
                                                        $courseName = $co->getName();
                                                        $courseId = $co->getId();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de curso no existe o esta inactivo.</p>';
                                                    }
                                                }
                                            
                                            }
                                            elseif ($type == 'university') {
                                                
                                                if ($emailInformation == '') {
                                                    $message .= '<p>Falta el correo universitario.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($facultyCode == '') {
                                                    $message .= '<p>Falta el código de la facultad.</p>';
                                                    $valid = 0;
                                                }
                                                else {
                                                    $faculty = $em->getRepository('FishmanEntityBundle:Companyfaculty')->findOneBy(
                                                        array( 'company' => $companyId , 'code' => $facultyCode , 'status' => 1));
                                                    if ($faculty) {
                                                        $facultyId = $faculty->getId();
                                                        $facultyName = $faculty->getName();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de facultad no existe o está inactivo.</p>';
                                                        $valid = 0;
                                                    }
                                                }
                                                
                                                if ($careerCode == '') {
                                                    $message .= '<p>Falta el código de la carrera.</p>';
                                                    $valid = 0;
                                                }
                                                else {
                                                    $career = $em->getRepository('FishmanEntityBundle:Companycareer')->findOneBy(
                                                        array( 'company' => $companyId , 'code' => $careerCode , 'status' => 1));
                                                    if ($career) {
                                                        $careerId = $career->getId();
                                                        $careerName = $career->getName();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de carrera no existe o está inactivo.</p>';
                                                        $valid = 0;
                                                    }
                                                }
                                                
                                                if ($academicYear == '') {
                                                    $message .= '<p>Falta el ciclo académico.</p>';
                                                    $valid = 0;
                                                }
                                                
                                                if ($courseCode == '') {
                                                    $message .= '<p>Falta el código del curso</p>';
                                                }
                                                else {
                                                    $co = $em->getRepository('FishmanEntityBundle:Companycourse')->findOneBy(
                                                        array('code' => $courseCode, 'company' => $pollscheduling->getCompanyId(), 'status' => 1));
                                                    if ($co) {
                                                        $courseName = $co->getName();
                                                        $courseId = $co->getId();
                                                    }
                                                    else {
                                                        $message .= '<p>El código de curso no existe o esta inactivo.</p>';
                                                    }
                                                }
                                                
                                            }
                                            
                                            if($valid && $message == ''){
                                                $message = 'OK';
                                            }
                                            
                                            // Array evaluations
                                            
                                            $people['groups'][$group][$subgroup][$i] = array(
                                                'rol' => $rol,
                                                'code' => $code,
                                                'surname' => $surname,
                                                'lastname' => $lastname,
                                                'names' => $names,
                                                'sex' => $sex,
                                                'identity' => $typeIdentity,
                                                'numberidentity' => $numberIdentity,
                                                'birthday' => $birthday,
                                                'email' => $email,
                                                'company_id' => $companyId,
                                                'company_code' => $companyCode,
                                                'company_name' => $companyName,
                                                'company_type' => $companyType,
                                                'headquarter_id' => $headquarterId,
                                                'headquarter_name' => $headquarterName,
                                                'email_information' => $emailInformation,
                                                'message' => $message,
                                                'valid' => $valid
                                            );
                                            
                                            if ($type == 'working') {
                                                
                                                $people['groups'][$group][$subgroup][$i]['wi_type'] = 'workinginformation';
                                                $people['groups'][$group][$subgroup][$i]['ou_id'] = $ouId;
                                                $people['groups'][$group][$subgroup][$i]['ou_name'] = $ouName;
                                                $people['groups'][$group][$subgroup][$i]['ou_unit_type'] = $ouUnitType;
                                                $people['groups'][$group][$subgroup][$i]['charge_id'] = $chargeId;
                                                $people['groups'][$group][$subgroup][$i]['charge_name'] = $chargeName;
                                                $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                                $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                                $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                                $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                                
                                            }
                                            elseif ($type == 'school') {
                                                
                                                $people['groups'][$group][$subgroup][$i]['wi_type'] = 'schoolinformation';
                                                $people['groups'][$group][$subgroup][$i]['grade_code'] = $gradeCode;
                                                $people['groups'][$group][$subgroup][$i]['study_year'] = $studyYear;
                                                $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                                $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                                $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                                $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                                
                                            }
                                            elseif ($type == 'university') {
                                                
                                                $people['groups'][$group][$subgroup][$i]['wi_type'] = 'universityinformation';
                                                $people['groups'][$group][$subgroup][$i]['faculty_id'] = $facultyId;
                                                $people['groups'][$group][$subgroup][$i]['faculty_name'] = $facultyName;
                                                $people['groups'][$group][$subgroup][$i]['career_id'] = $careerId;
                                                $people['groups'][$group][$subgroup][$i]['career_name'] = $careerName;
                                                $people['groups'][$group][$subgroup][$i]['academic_year'] = $academicYear;
                                                $people['groups'][$group][$subgroup][$i]['course_id'] = $courseId;
                                                $people['groups'][$group][$subgroup][$i]['course_name'] = $courseCode;
                                                $people['groups'][$group][$subgroup][$i]['course_code'] = $courseName;
                                                $people['groups'][$group][$subgroup][$i]['section'] = $section;
                                                
                                            }
                                            
                                            $i++;
                                        }
                                        
                                        $row++;
                                        
                                    } while (true);
                                    
                                    $i = 0;
                                    $evaluations = array();
                                    foreach ($people['groups'] as $group) {
                                        foreach ($group['evaluators'] as $evaluator) {
                                            $evaluations[$i]['evaluator'] = $evaluator;
                                            $evaluations[$i]['evaluated'] = $evaluator;
                                            $i++;
                                        }
                                    }
                                    
                                }
                                else {
                                    $session->getFlashBag()->add('error', 'El tipo de empresa de la plantilla es erroneo debe ser "school", "university".');
                                }
                                
                            }
                            else {
                                $session->getFlashBag()->add('error', 'El tipo de evaluación de la plantilla es erroneo debe ser "evaluators".');
                            }
                            
                        }
                        else {
                            $session->getFlashBag()->add('error', 'El archivo a importar debe ser de formato excel');
                        }
                        
                        if(count($evaluations) > 0 and is_array($evaluations)){
                            $temporal = new Temporalimportdata();
                            $temporal->setData($evaluations);
                            $temporal->setEntity('evaluations');
                            $temporal->setDatetime(new \DateTime());
                            $em->persist($temporal);
                            $em->flush();
        
                            // Secondary form
                            $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                            $form2 = $this->createFormBuilder($defaultData2)
                                ->add('temporalimportdata_id',
                                      'integer', array (
                                               'required' => true 
                                     ))
                                ->getForm();
                            $form2 = $form2->createView();
                        }
                        else {
                            $session->getFlashBag()->add('error', 'No hay evaluaciones para importar.');
                        }
                        
                        //Delete temporal file
                        if ($data['file']->document == 'xlsx') {
                            unlink($inputFileName);
                        }
                    }
                } //End is method post
                  
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/EvaluatorNotEvaluateds:import.html.twig', array(
                    'form' => $form->createView(),
                    'form2' => $form2,
                    'workshopscheduling' => $workshopscheduling,
                    'pollscheduling' => $pollscheduling,
                    'evaluations' => $evaluations,
                    'pollapplicationimport' => FALSE,
                    'companytype' => $companyType
                ));
                
            }
            else {
              $session->getFlashBag()->add('error', 'La Encuesta de la Programación de Taller ha sido eliminada.');
    
              return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                  'workshopschedulingid' => $workshopschedulingid
              )));
            }
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateNotEvaluatedsAction()
    {
        $type = $_GET['type'];
        $filename = '';
        
        switch ($type) {
            case 'working':
                $filename = 'importar-evaluaciones-taller-laboral-sin-evaluados.xlsx';
                break;
            case 'school':
                $filename = 'importar-evaluaciones-taller-colegio-sin-evaluados.xlsx';
                break;
            case 'university':
                $filename = 'importar-evaluaciones-taller-universidad-sin-evaluados.xlsx';
                break;
        }

        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"'
        );
        return new Response(file_get_contents( __DIR__.'/../../../../templates/WorkshopBundle/Workshopscheduling/Poll/NotEvaluateds/' . $filename), 200, $headers);
    }
    
    /**
     * Lists all Workshopschedulingpollsection entities.
     *
     */
    public function indexPollsectionAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Poll Info
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll'));
            }
            
            if ($pollscheduling->getLevel() === 0) {
                $session->getFlashBag()->add('error', 'La encuesta ha sido programada sin secciones.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll_show', array(
                    'id' => $pollschedulingid
                )));
            }
            
            // Recovering data
            
            $parent_options = Pollschedulingsection::getPollschedulingSectionsOptions($this->getDoctrine(), $pollschedulingid);
            for ($i = 1; $i <= 50; $i++) {
                $sequence_options[$i] = $i;
            }
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '',  
                'parent' => '', 
                'sequence' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('parent', 'choice', array(
                    'choices' => $parent_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('sequence', 'choice', array(
                    'choices' => $sequence_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '',  
                'parent' => '', 
                'sequence' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingsection');
            $queryBuilder = $repository->createQueryBuilder('pss')
                ->select('pss.id, pss.name, pssp.name parent, pss.sequence, pss.changed, pss.status, 
                          u.names, u.surname, u.lastname')
                ->leftJoin('FishmanPollBundle:Pollschedulingsection', 'pssp', 'WITH', 'pss.parent_id = pssp.id')
                ->leftJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pss.modified_by = u.id')
                ->where('pss.pollscheduling = :pollscheduling')
                ->andWhere('pss.id LIKE :id 
                        OR pss.name LIKE :name')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->setParameter('id', '%' . $data['word'] . '%')
                ->setParameter('name', '%' . $data['word'] . '%')
                ->orderBy('pss.id', 'ASC');
            
            // Add arguments
            
            if ($data['parent'] != '') {
                $queryBuilder
                    ->andWhere('pss.parent_id = :parent')
                    ->setParameter('parent', $data['parent']);
            }
            if ($data['sequence'] != '') {
                $queryBuilder
                    ->andWhere('pss.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }
            if ($data['status'] != '' || $data['status'] === 0) {
                $queryBuilder
                    ->andWhere('pss.status = :status')
                    ->setParameter('status', $data['status']);
            }
            
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workshopschedulingpollsection');
            $paginator->setMaxPagerItems(5, 'workshopschedulingpollsection');
            $entities = $paginator->paginate($query, 'workshopschedulingpollsection')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workshopschedulingpollsection');
            $endPageItem = $paginator->getEndPageItem('workshopschedulingpollsection');
            $totalItems = $paginator->getTotalItems('workshopschedulingpollsection');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Section:index.html.twig', array(
                'entities' => $entities,
                'workshopscheduling' => $workshopscheduling,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Finds and displays a Workshopschedulingpollsection entity.
     *
     */
    public function showPollsectionAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Query
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingsection');
            $result = $repository->createQueryBuilder('pss')
                ->select('pss.id, pss.name, ps.id pollscheduling_id, ps.title pollscheduling_title, 
                          ps.version pollscheduling_version, ps.entity_type pollscheduling_entitytype, 
                          ps.pollscheduling_anonymous, pss.description, 
                          pssp.name parent, pss.sequence, pss.changed, pss.status, pss.created, 
                          pss.changed, pss.created_by, pss.modified_by')
                ->innerJoin('pss.pollscheduling', 'ps')
                ->leftJoin('FishmanPollBundle:Pollschedulingsection', 'pssp', 'WITH', 'pss.parent_id = pssp.id')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'pss.modified_by = u.id')
                ->where('pss.id = :pollschedulingsection')
                ->setParameter('pollschedulingsection', $id)
                ->getQuery()
                ->getResult();
            
            $entity = current($result);
    
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
    
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Section:show.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling, 
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Workshopschedulingpollsection entity.
     *
     */
    public function newPollsectionAction($workshopschedulingid, $pollschedulingid)
    {
        $entity = new Pollschedulingsection();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            if ($pollscheduling->getLevel() === 0) {
                $session->getFlashBag()->add('error', 'La encuesta ha sido programada sin secciones.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll_show', array(
                    'id' => $pollschedulingid
                )));
            }
            
            $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $pollscheduling->getId());
            if (count($paq) > 0) {
                $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, no se permite crear nuevas secciones');
                return $this->redirect($this->generateUrl('workshopschedulingpollsection', array(
                    'workshopschedulingid' => $workshopschedulingid,
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $entity->setPollscheduling($pollscheduling);
            $entity->setStatus(1);
            $form = $this->createForm(new PollschedulingsectionType($entity, $this->getDoctrine()), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Section:new.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling, 
                'pollscheduling' => $pollscheduling,
                'form'   => $form->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Creates a new Workshopschedulingpollsection entity.
     *
     */
    public function createPollsectionAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $entity  = new Pollschedulingsection();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Pollscheduling Info
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }

        $entity->setPollscheduling($pollscheduling);
        $form = $this->createForm(new PollschedulingsectionType($entity, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();

            if ($entity->getParentId() != '-1') {
                $ps_entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($entity->getParentId());
                $entity->setL1($ps_entity->getL1()); 
                $entity->setL2($ps_entity->getL2()); 
                $entity->setL3($ps_entity->getL3()); 
                $entity->setL4($ps_entity->getL4()); 
                $entity->setL5($ps_entity->getL5()); 
                $entity->setL6($ps_entity->getL6()); 
                $entity->setL7($ps_entity->getL7()); 
                $entity->setL8($ps_entity->getL8()); 
                $entity->setL9($ps_entity->getL9());
            }
            $entity->setType(1);
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setPollscheduling($pollscheduling);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            if ($entity->getParentId() == '-1') {
                $entity->setL1($entity->getId());
            }
            else {
                $level = FALSE;
              
                if ($entity->getL2() == '') {
                    $entity->setL2($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL3() == '' && !$level) {
                    $entity->setL3($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL4() == '' && !$level) {
                    $entity->setL4($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL5() == '' && !$level) {
                    $entity->setL5($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL6() == '' && !$level) {
                    $entity->setL6($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL7() == '' && !$level) {
                    $entity->setL7($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL8() == '' && !$level) {
                    $entity->setL8($entity->getId());
                    $level = TRUE;
                }
                if ($entity->getL9() == '' && !$level) {
                    $entity->setL9($entity->getId());
                    $level = TRUE;
                }
            }
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopschedulingpollsection_show', array(
                'workshopschedulingid' => $workshopschedulingid,
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Section:new.html.twig', array(
            'entity' => $entity,
            'workshopscheduling' => $workshopscheduling,
            'pollscheduling' => $pollscheduling,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Workshopschedulingpollsection entity.
     *
     */
    public function editPollsectionAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $disabled = FALSE;
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Pollschedulingsection Info
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $entity->getPollscheduling()->getId());
            if (count($paq) > 0) {
                $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, sólo puede editar el nombre y/o descripción de la sección');
                $disabled = TRUE;
            }
    
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
            
            $editForm = $this->createForm(new PollschedulingsectionType($entity, $this->getDoctrine(), $disabled), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Section:edit.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'edit_form' => $editForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Edits an existing Workshopschedulingpollsection entity.
     *
     */
    public function updatePollsectionAction(Request $request, $workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Pollschedulingsection Info
        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
            return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }

        $parent_id = $entity->getParentId();
        
        $editForm = $this->createForm(new PollschedulingsectionType($entity, $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            if (!in_array($entity->getParentId(), array('-1', $parent_id))) {
                $ps_entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($entity->getParentId());
                $entity->setL1($ps_entity->getL1()); 
                $entity->setL2($ps_entity->getL2()); 
                $entity->setL3($ps_entity->getL3()); 
                $entity->setL4($ps_entity->getL4()); 
                $entity->setL5($ps_entity->getL5()); 
                $entity->setL6($ps_entity->getL6()); 
                $entity->setL7($ps_entity->getL7()); 
                $entity->setL8($ps_entity->getL8()); 
                $entity->setL9($ps_entity->getL9());
            }
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();

            if ($entity->getParentId() != $parent_id) {
                // Update Pollschedulingsection to ParentId
                Pollschedulingsection::updateRegisterParentId($this->getDoctrine(), $entity);
            }
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopschedulingpollsection_show', array(
                'workshopschedulingid' => $workshopschedulingid,
                'id' => $id
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollschedulingsection:edit.html.twig', array(
            'entity' => $entity,
            'workshopscheduling' => $workshopscheduling,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));

    }

    /**
     * Deletes a Workshopschedulingpollsection entity.
     *
     */
    public function deletePollsectionAction(Request $request, $workshopschedulingid, $pollschedulingid, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $em = $this->getDoctrine()->getManager();
          
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpollsection', array(
                    'workshopschedulingid' => $workshopschedulingid,
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('workshopschedulingpollsection', array(
            'workshopschedulingid' => $workshopschedulingid,
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Drop an Workshopschedulingpollsection entity.
     *
     */
    public function dropPollsectionAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la sección de la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $entity->getPollscheduling()->getId());
            if (count($paq) > 0) {
                $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, no se permite eliminar secciones');
                return $this->redirect($this->generateUrl('workshopschedulingpollsection', array(
                    'workshopschedulingid' => $workshopschedulingid,
                    'pollschedulingid' => $entity->getPollscheduling()->getId()
                )));
            }
    
            $deleteForm = $this->createDeleteForm($id);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Section:drop.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling,
                'delete_form' => $deleteForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }
    /**
     * Lists all Workshopschedulingpollquestion entities.
     *
     */
    public function indexPollquestionAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Pollscheduling Info
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            // Recovering data
            
            $section_options = Pollschedulingsection::getPollschedulingSectionsOptions($this->getDoctrine(), $pollschedulingid);
            $type_options = Pollquestion::getListTypeOptions();
            for ($i = 1; $i <= 50; $i++) {
                $sequence_options[$i] = $i;
            }
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '',  
                'section' => '', 
                'type' => '', 
                'sequence' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('section', 'choice', array(
                    'choices' => $section_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('type', 'choice', array(
                    'choices' => $type_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('sequence', 'choice', array(
                    'choices' => $sequence_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'section' => '', 
                'type' => '', 
                'sequence' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingquestion');
            $queryBuilder = $repository->createQueryBuilder('psq')
                ->select('psq.id, psq.question, pss.name pollsection, psq.type, psq.changed, psq.sequence, psq.status, u.names, u.surname, u.lastname')
                ->leftJoin('psq.pollschedulingsection', 'pss')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'psq.modified_by = u.id')
                ->where('psq.pollscheduling = :pollscheduling')
                ->andWhere('psq.id LIKE :id 
                        OR psq.question LIKE :question')
                ->setParameter('pollscheduling', $pollschedulingid)
                ->setParameter('id', '%' . $data['word'] . '%')
                ->setParameter('question', '%' . $data['word'] . '%')
                ->orderBy('psq.id', 'ASC');
            
            // Add arguments
            
            if ($data['section'] != '') {
                $queryBuilder
                    ->andWhere('psq.pollschedulingsection = :section')
                    ->setParameter('section', $data['section']);
            }
            if ($data['type'] != '') {
                $queryBuilder
                    ->andWhere('psq.type = :type')
                    ->setParameter('type', $data['type']);
            }
            if ($data['sequence'] != '') {
                $queryBuilder
                    ->andWhere('psq.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }
            if ($data['status'] != '' || $data['status'] === 0) {
                $queryBuilder
                    ->andWhere('psq.status = :status')
                    ->setParameter('status', $data['status']);
            }
            
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workshopschedulingpollquestion');
            $paginator->setMaxPagerItems(5, 'workshopschedulingpollquestion');
            $entities = $paginator->paginate($query, 'workshopschedulingpollquestion')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workshopschedulingpollquestion');
            $endPageItem = $paginator->getEndPageItem('workshopschedulingpollquestion');
            $totalItems = $paginator->getTotalItems('workshopschedulingpollquestion');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Question:index.html.twig', array(
                'entities' => $entities,
                'workshopscheduling' => $workshopscheduling,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Finds and displays a Workshopschedulingpollquestion entity.
     *
     */
    public function showPollquestionAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
    
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Question:show.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Workshopschedulingpollquestion entity.
     *
     */
    public function newPollquestionAction($workshopschedulingid, $pollschedulingid)
    {   
        $entity = new Pollschedulingquestion();
        $entity->setStatus(1);
        $em = $this->getDoctrine()->getManager();  
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $pollscheduling->getId());
            if (count($paq) > 0) {
                $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, no se permite crear nuevas preguntas');
                return $this->redirect($this->generateUrl('workshopschedulingpollquestion', array(
                    'workshopschedulingid' => $workshopschedulingid,
                    'pollschedulingid' => $pollscheduling->getId()
                )));
            }
            
            $entity->setPollscheduling($pollscheduling);
            $entity->setStatus(1);
            $form   = $this->createForm(new PollschedulingquestionType($pollschedulingid, '', $this->getDoctrine()), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Question:new.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling,
                'pollscheduling' => $pollscheduling,
                'form' => $form->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Creates a new Workshopschedulingpollquestion entity.
     *
     */
    public function createPollquestionAction(Request $request, $workshopschedulingid, $pollschedulingid)
    {
        $entity  = new Pollschedulingquestion();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Pollscheduling Info
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta programada.');
            return $this->redirect($this->generateUrl('worksñhopschedulingpoll', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }
        
        // Recover options
        
        $options = array();
        $requestPollschedulingQuestion = $request->request->get('fishman_pollbundle_pollschedulingquestiontype');
        
        if (in_array($requestPollschedulingQuestion['type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestPollschedulingQuestion['options'])) {
                $optionsArray = explode('%%', $requestPollschedulingQuestion['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }
        
        if ($pollscheduling->getLevel() === 0) {
            $form = $this->createForm(new PollschedulingquestionType(
                           $pollschedulingid, null, $this->getDoctrine()), $entity);
        }
        else {
            $form = $this->createForm(new PollschedulingquestionType(
                           $pollschedulingid, $requestPollschedulingQuestion['pollschedulingsection_id'], $this->getDoctrine()), $entity);
        }
        
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $section = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find(
                         $requestPollschedulingQuestion['pollschedulingsection_id']);
            $entity->setPollschedulingsection($section);

            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setPollscheduling($pollscheduling);
            if (in_array($entity->getType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                $entity->setAlignment('');
            }
            if (in_array($entity->getType(), array('multiple_text', 'simple_text'))) {
                $entity->setOptions(array());
            }
            else {
                $entity->setOptions($options);
            }
            $entity->setOriginalSequence($entity->getSequence());
            $entity->setLevel($pollscheduling->getLevel());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopschedulingpollquestion_show', array(
                'workshopschedulingid' => $workshopschedulingid,
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanPollBundle:Pollschedulingquestion:new.html.twig', array(
            'entity' => $entity,
            'workshopscheduling' => $workshopscheduling,
            'pollscheduling' => $pollscheduling,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Workshopschedulingpollquestion entity.
     *
     */
    public function editPollquestionAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $disabled = FALSE;
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopscheduling', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $entity->getPollscheduling()->getId());
            if (count($paq) > 0) {
                $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, sólo puede editar el nombre de la pregunta');
                $disabled = TRUE;
            }
    
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
            
            if ($entity->getPollscheduling()->getLevel() === 0) {
                $editForm = $this->createForm(new PollschedulingquestionType($entity->getPollscheduling()->getId(),
                                              null, $this->getDoctrine(), $disabled), $entity);
            }
            else {
                $editForm = $this->createForm(new PollschedulingquestionType($entity->getPollscheduling()->getId(),
                                              $entity->getPollschedulingsection()->getId(), $this->getDoctrine(), $disabled), $entity);
            }
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Question:edit.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'edit_form' => $editForm->createView(), 
                'disabled' => $disabled
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Edits an existing Workshopschedulingpollquestion entity.
     *
     */
    public function updatePollquestionAction(Request $request, $workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
            return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }
        
        // Recover options
        
        $options = array();
        $requestPollschedulingQuestion = $request->request->get('fishman_pollbundle_pollschedulingquestiontype');
        
        if (in_array($requestPollschedulingQuestion['type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestPollschedulingQuestion['options'])) {
                $optionsArray = explode('%%', $requestPollschedulingQuestion['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }
        
        if ($entity->getPollscheduling()->getLevel() === 0) {
            $editForm = $this->createForm(new PollschedulingquestionType($entity->getPollscheduling()->getId(),
                                          null, $this->getDoctrine()), $entity);
        }
        else {
            $editForm = $this->createForm(new PollschedulingquestionType($entity->getPollscheduling()->getId(),
                                          $entity->getPollschedulingsection()->getId(), $this->getDoctrine()), $entity);
        }
        
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $section = $em->getRepository('FishmanPollBundle:Pollschedulingsection')->find(
                         $requestPollschedulingQuestion['pollschedulingsection_id']);
            $entity->setPollschedulingsection($section);
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            if (in_array($entity->getType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                $entity->setAlignment('');
            }
            if (in_array($entity->getType(), array('multiple_text', 'simple_text'))) {
                $entity->setOptions(array());
            }
            else {
                $entity->setOptions($options);
            }
            $entity->setChanged(new \DateTime());
            $entity->setModifiedBy($modifiedBy->getId());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.'); 

            return $this->redirect($this->generateUrl('workshopschedulingpollquestion_show', array(
                'workshopschedulingid' => $workshopschedulingid,
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanPollBundle:Pollschedulingquestion:edit.html.twig', array(
            'entity' => $entity,
            'workshopscheduling' => $workshopscheduling,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Workshopschedulingpollquestion entity.
     *
     */
    public function deletePollquestionAction(Request $request, $workshopschedulingid, $pollschedulingid, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpollquestion', array(
                    'workshopschedulingid' => $workshopschedulingid,
                    'pollschedulingid' => $pollschedulingid
                )));
            }
            
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('workshopschedulingpollquestion', array(
            'workshopschedulingid' => $workshopschedulingid,
            'pollschedulingid' => $pollschedulingid
        )));
    }

    /**
     * Drop an Workshopschedulingpollquestion entity.
     *
     */
    public function dropPollquestionAction($workshopschedulingid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            $entity = $em->getRepository('FishmanPollBundle:Pollschedulingquestion')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la pregunta de la encuesta programada.');
                return $this->redirect($this->generateUrl('workshopschedulingpoll', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $paq = Pollapplicationquestion::getCountResolvedPollapplicationquestion($this->getDoctrine(), $entity->getPollscheduling()->getId());
            if (count($paq) > 0) {
                $session->getFlashBag()->add('error', 'La Encuesta está en funcionamiento, no se permite eliminar preguntas');
                return $this->redirect($this->generateUrl('workshopschedulingpollquestion', array(
                    'workshopschedulingid' => $workshopschedulingid,
                    'pollschedulingid' => $entity->getPollscheduling()->getId()
                )));
            }
    
            $deleteForm = $this->createDeleteForm($id);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Poll/Question:drop.html.twig', array(
                'workshopscheduling' => $workshopscheduling,
                'entity' => $entity,
                'delete_form' => $deleteForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Lists all Workshopschedulingnotification entities.
     *
     */
    public function indexNotificationAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Recovering data
            
            $type_options = Notification::getListTypeOptions();
            $activity_options = Workshopschedulingactivity::getListWorkshopschedulingactivityOptions($this->getDoctrine(), $workshopschedulingid);
            $predecessor_options = Notificationscheduling::getListNotificationschedulingOptions($this->getDoctrine(), 'workshopscheduling', $workshopschedulingid);
            for ($i = 1; $i<= 20; $i++) {
                $sequence_options[$i] = $i;
            }
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'type' => '', 
                'activity' => '', 
                'predecessor' => '', 
                'sequence' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('type', 'choice', array(
                    'choices' => $type_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('activity', 'choice', array(
                    'choices' => $activity_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('predecessor', 'choice', array(
                    'choices' => $predecessor_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('sequence', 'choice', array(
                    'choices' => $sequence_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'type' => '', 
                'activity' => '', 
                'predecessor' => '', 
                'sequence' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Query
                
            $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationscheduling');
            $queryBuilder = $repository->createQueryBuilder('ns')
                ->select('ns.id, ns.name, ns.notification_type type, ns.notification_type_name type_name, 
                          nsp.name predecessor, ns.sequence, ns.changed, ns.status, ns.own, u.names, u.surname, u.lastname')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'ns.modified_by = u.id')
                ->leftJoin('FishmanNotificationBundle:Notificationscheduling', 'nsp', 'WITH', 'ns.predecessor = nsp.id')
                ->where('ns.entity_id = :workshopscheduling 
                        AND ns.entity_type = :entity_type')
                ->andWhere('ns.id LIKE :id 
                        OR ns.name LIKE :name')
                ->setParameter('workshopscheduling', $workshopschedulingid)
                ->setParameter('entity_type', 'workshopscheduling')
                ->setParameter('id', '%' . $data['word'] . '%')
                ->setParameter('name', '%' . $data['word'] . '%')
                ->orderBy('ns.id', 'ASC');
        
            // Add arguments
            
            if ($data['type'] != '') {
                $queryBuilder
                    ->andWhere('ns.notification_type = :type')
                    ->setParameter('type', $data['type']);
            }
            if ($data['activity'] != '') {
                $queryBuilder
                    ->andWhere('ns.notification_type_id = :activity')
                    ->setParameter('activity', $data['activity']);
            }
            if ($data['predecessor'] != '') {
                $queryBuilder
                    ->andWhere('ns.predecessor = :predecessor')
                    ->setParameter('predecessor', $data['predecessor']);
            }
            if ($data['sequence'] != '') {
                $queryBuilder
                    ->andWhere('ns.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }
            if ($data['status'] != '' || $data['status'] === 0) {
                $queryBuilder
                    ->andWhere('ns.status = :status')
                    ->setParameter('status', $data['status']);
            }
            
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workshopschedulingnotification');
            $paginator->setMaxPagerItems(5, 'workshopschedulingnotification');
            $entities = $paginator->paginate($query, 'workshopschedulingnotification')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workshopschedulingnotification');
            $endPageItem = $paginator->getEndPageItem('workshopschedulingnotification');
            $totalItems = $paginator->getTotalItems('workshopschedulingnotification');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Notification:index.html.twig', array(
                'entities' => $entities,
                'workshopscheduling' => $workshopscheduling,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Finds and displays a Workshopschedulingnotification entity.
     *
     */
    public function showNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
            
        // Recover Notificationscheduling
        $entity = Notificationscheduling::getNotificationscheduling($this->getDoctrine(), $id);
        
        // Recover Pollscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity['entity_id']);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {

            $entityTypeName = '';
    
            if ($entity['type'] == 'poll') {
                $entityType = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entity['notification_type_id']);
                $entityTypeName = $entityType->getTitle();
            } elseif ($entity['type'] == 'activity') {
                $entityType = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($entity['notification_type_id']);
                $entityTypeName = $entityType->getName();
            }
            
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Notification:show.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling, 
                'entityTypeName' => $entityTypeName,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Workshopschedulingnotification entity.
     *
     */
    public function newNotificationAction($workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        if (!$workshopscheduling->getDeleted()) {
          
            $entity = new Notificationscheduling();
            
            $entity->setFinish('never');
            $entity->setSelectedTrigger('to_send');
            $entity->setStatus(1);
            $form = $this->createForm(new NotificationschedulingType($entity, 'workshopscheduling', $workshopschedulingid, true, $this->getDoctrine()), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Notification:new.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling,
                'form'   => $form->createView(),
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La Programación de Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Return the list of a notification type 
     */
    public function choiceByTypeNotificationAction($workshopschedulingid)
    {
        //variables defined
        $first = true;
        $typeId = '';
        
        //Entity manager
        $em = $this->getDoctrine()->getManager();

        //Recovery workshop
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        $typeNotification = $_GET['type'];

        //Filter for type Notification
        if ($typeNotification == 'activity' || $typeNotification == 'message') {
            if ($typeNotification == 'activity') {
                $repository = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
                $combo = Workshopschedulingactivity::getActivityListOfWorkshop($repository, $workshopscheduling, $first, $typeId);
            } else {
                $combo = '';
            }
            $multiSelect = '<option value="boss">Jefe</option>';
            $multiSelect .= '<option value="collaborator">Colaborador</option>';
            $multiSelect .= '<option value="integrant">Participante</option>';
            $multiSelect .= '<option value="company">Empresa</option>';
            $multiSelect .= '<option value="responsible">Responsable</option>';
        } elseif ($typeNotification == 'poll') {
            $repository = $em->getRepository('FishmanPollBundle:Pollscheduling');
            $combo = Pollscheduling::getPollListOfWorkshop($repository, $workshopscheduling, $first, $typeId);
            $multiSelect = '<option value="evaluated">Evaluado</option>';
            $multiSelect .= '<option value="evaluator">Evaluador</option>';
        }

        $response = new Response(json_encode(
            array('combo' => $combo, 'multi_select' => $multiSelect)));  
        return $response;
    }
    

    /**
     * Creates a new Workshopschedulingnotification entity.
     *
     */
    public function createNotificationAction(Request $request, $workshopschedulingid)
    {
        $entity  = new Notificationscheduling();
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        $asigneds = $request->request->get('asigned_options');
        
        $form = $this->createForm(new NotificationschedulingType($entity, 'workshopscheduling', $workshopschedulingid, true, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            if ($entity->getNotificationType() == 'activity') {
                $activity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($entity->getNotificationTypeId());
                $entity->setNotificationTypeName($activity->getName());
                $entity->setNotificationTypeId($activity->getId());
            }
            elseif ($entity->getNotificationType() == 'poll') {
                $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entity->getNotificationTypeId());
                $entity->setNotificationTypeName($pollscheduling->getTitle());
            }
            elseif ($entity->getNotificationType() == 'message') {
                $entity->setNotificationTypeId('');
                $entity->setNotificationTypeName('');
                $entity->setNotificationTypeStatus(FALSE);
            }
            if ($entity->getExecutionType() == 'manual' || $entity->getExecutionType() == 'trigger') {
                $entity->setPredecessor(-1);
            }
            if ($entity->getExecutionType() == 'trigger') {
                $entity->setNotificationTypeStatus(TRUE);
            }
            else {
                $entity->setSelectedTrigger('');
            }
            if ($entity->getExecutionType() != 'automatic') {
                $entity->setSince('');
                $entity->setReplay(FALSE);
            }
            if ($entity->getReplay() == 0) {
                $entity->setRepeatPeriod('');
                $entity->setRepeatRange('');
                $entity->setFinish('');
            }
            if ($entity->getFinish() != 'after') {
                $entity->setRepetitionNumber('');
            }
            if ($entity->getPredecessor() == '') {
                $entity->setPredecessor(-1);
            }
            $entity->setAsigned($asigneds['data2']);
            $entity->setEntityType('workshopscheduling');
            $entity->setEntityId($workshopscheduling->getId());
            $entity->setEntityName($workshopscheduling->getName());
            $entity->setEntityStatus($workshopscheduling->getStatus());
            $entity->setOwn(TRUE);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $em->persist($entity);
            $em->flush();
            
            // Create Notificationexecution
            switch ($entity->getNotificationType()) {
                
                case 'activity':
                    
                    // Recover Workshopapplicationactivities
                    $workshopapplicationactivitys = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->findBy(array(
                        'workshopschedulingactivity_id' => $entity->getNotificationTypeId(),
                        'deleted' => 0
                    ));
                    
                    if (!empty($workshopapplicationactivitys)) {
                        foreach ($workshopapplicationactivitys as $waa_entity) {
                            Workshopapplicationactivity::generateNotificationexecutions($this->getDoctrine(), $waa_entity, $entity->getId());
                        }
                    }

                    break;
                
                case 'poll':
                    
                    // Recover Pollapplications
                    $pollapplications = $em->getRepository('FishmanPollBundle:Pollapplication')->findBy(array(
                        'pollscheduling' => $entity->getNotificationTypeId(),
                        'deleted' => 0
                    ));
                    
                    if (!empty($pollapplications)) {
                        foreach ($pollapplications as $pa_entity) {
                            Pollapplication::generateNotificationexecutions($this->getDoctrine(), $pa_entity, $entity->getId());
                        }
                    }
                    
                    break;
                    
                case 'message':
                    
                    // Recover Workshopapplications
                    $workshopapplications = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findBy(array(
                        'workshopscheduling' => $entity->getEntityId(), 
                        'deleted' => 0
                    ));
                    
                    if (!empty($workshopapplications)) {
                        foreach ($workshopapplications as $wa_entity) {
                            Workshopapplication::generateNotificationexecutions($this->getDoctrine(), $wa_entity, $entity->getId());
                        }
                    }
                    
                    break;
            }
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopschedulingnotification_show', array(
                'id' => $entity->getId()
            )));
        }
        
        return $this->render('FishmanWorkshopBundle:Workshopscheduling/Notification:new.html.twig', array(
            'entity' => $entity,
            'workshopscheduling' => $workshopscheduling,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Workshopschedulingnotification entity.
     *
     */
    public function editNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Notificationscheduling
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        $typeNotification = $entity->getNotificationType();
        $typeId = $entity->getNotificationTypeId();

        if ($typeNotification == 'activity') {
          $repository = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
          $combo = Workshopschedulingactivity::getActivityListOfWorkshop($repository, $workshopscheduling, TRUE, $typeId);
        } elseif ($typeNotification == 'poll') {
          $repository = $em->getRepository('FishmanPollBundle:Pollscheduling');
          $combo = Pollscheduling::getPollListOfWorkshop($repository, $workshopscheduling, TRUE, $typeId);
        }
        else {
          $combo = '';
        }
        
        if (!$workshopscheduling->getDeleted()) {
         
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
            
            $entity->setFinish('never');
            $entity->setSelectedTrigger('to_send');
            $editForm = $this->createForm(new NotificationschedulingType($entity, 'workshopscheduling', $entity->getEntityId(), $entity->getOwn(), $this->getDoctrine()), $entity);
            
            return $this->render('FishmanWorkshopBundle:Workshopscheduling/Notification:edit.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling, 
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'combo' => $combo,
                'edit_form'   => $editForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Edits an existing Workshopschedulingnotification entity.
     *
     */
    public function updateNotificationAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        $asigneds = $request->request->get('asigned_options');
        
        $dataEntity = array(
            'notification_type' => $entity->getNotificationType(),
            'notification_type_status' => $entity->getNotificationTypeStatus(),
            'one_per_evaluator' => $entity->getOnePerEvaluator(),
            'execution_type' => $entity->getExecutionType(),
            'predecessor' => $entity->getPredecessor(),
            'since' => $entity->getSince(),
            'selected_trigger' => $entity->getSelectedTrigger(),
            'replay' => $entity->getReplay(),
            'repeat_period' => $entity->getRepeatPeriod(),
            'repeat_range' => $entity->getRepeatRange(),
            'finish' => $entity->getFinish(),
            'repetition_number' => $entity->getRepetitionNumber()
        );
        
        $activityRequest = $request->request->get('fishman_notificationbundle_notificationschedulingtype');
        $editForm = $this->createForm(new NotificationschedulingType($entity, 'workshopscheduling', $entity->getEntityId(), $entity->getOwn(), $this->getDoctrine()), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setNotificationType($dataEntity['notification_type']);
            $entity->setNotificationTypeStatus($dataEntity['notification_type_status']);
            $entity->setOnePerEvaluator($dataEntity['one_per_evaluator']);
            $entity->setExecutionType($dataEntity['execution_type']);
            $entity->setPredecessor($dataEntity['predecessor']);
            $entity->setSince($dataEntity['since']);
            $entity->setSelectedTrigger($dataEntity['selected_trigger']);
            $entity->setReplay($dataEntity['replay']);
            $entity->setRepeatPeriod($dataEntity['repeat_period']);
            $entity->setRepeatRange($dataEntity['repeat_range']);
            $entity->setFinish($dataEntity['finish']);
            $entity->setRepetitionNumber($dataEntity['repetition_number']);
            
            $entity->setAsigned($asigneds['data2']);
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopschedulingnotification_show', array(
                'id' => $id
            )));
        }
        
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanWorkshopBundle:Workshopscheduling/Notification:edit.html.twig', array(
            'entity' => $entity,
            'workshopscheduling' => $workshopscheduling,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Workshopschedulingnotification entity.
     *
     */
    public function dropNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entity->getEntityId());
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la Programación de Taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
        
            if ($entity->getOwn()) {
    
                $deleteForm = $this->createDeleteForm($id);
        
                return $this->render('FishmanWorkshopBundle:Workshopscheduling/Notification:drop.html.twig', array(
                    'entity' => $entity,
                    'workshopscheduling' => $workshopscheduling, 
                    'delete_form' => $deleteForm->createView()
                ));
            }
        
            // set flash messages                                                                            
            $session = $this->getRequest()->getSession();                                                    
            $session->getFlashBag()->add('error', 'El registro no puede ser eliminado.');
    
            return $this->redirect($this->generateUrl('workshopschedulingnotification', array(
              'workshopschedulingid' => $workshopscheduling->getId()
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'La programación del Taller ha sido eliminada.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Deletes a Workshopschedulingnotification entity.
     *
     */
    public function deleteNotificationAction(Request $request, $id, $workshopschedulingid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $em = $this->getDoctrine()->getManager();
          
            $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la Programación de Taller.');
                return $this->redirect($this->generateUrl('workshopschedulingnotification', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $nds = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationsend')
               ->createQueryBuilder('nd')
               ->innerJoin('nd.notificationexecution', 'ne')
               ->where('ne.notificationscheduling = :notificationscheduling')
               ->setParameter('notificationscheduling', $id)
               ->getQuery()
               ->getResult();
            
            if (!empty($nds)) {
                $session->getFlashBag()->add('error', 'El registro no puede ser eliminado, algunas notificaciones han sido enviadas.');
                return $this->redirect($this->generateUrl('workshopschedulingnotification', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $nmss = $em->getRepository('FishmanNotificationBundle:Notificationmanualsend')->findBy(array(
                'notificationscheduling' => $id
            ));
            
            if (!empty($nmss)) {
                foreach ($nmss as $nms) {
                    $nmds = $em->getRepository('FishmanNotificationBundle:Notificationmanualdetails')->findBy(array(
                        'notificationmanualsend' => $nms->getId()
                    ));
                    
                    if (!empty($nmds)) {
                        foreach ($nmds as $nmd) {
                            $em->remove($nmd);
                            $em->flush();
                        }
                    }
                    
                    $em->remove($nms);
                    $em->flush();
                }
            }

            // Update children predecessor
            $em->createQueryBuilder()
                ->update('FishmanNotificationBundle:Notificationscheduling ns')
                ->set('ns.predecessor', ':predecessor')
                ->where('ns.predecessor = :notificationscheduling')
                ->setParameter('predecessor', '-1')
                ->setParameter('notificationscheduling', $id)
                ->getQuery()
                ->execute();
            
            Notificationexecution::removeNotificationexecutions($this->getDoctrine(), $entity->getId(), $entity->getEntityType(), $entity->getEntityId());
            
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('workshopschedulingnotification', array(
            'workshopschedulingid' => $workshopschedulingid
        )));
    }
}
