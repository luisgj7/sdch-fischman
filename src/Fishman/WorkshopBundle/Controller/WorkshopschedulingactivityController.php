<?php

namespace Fishman\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\WorkshopBundle\Entity\Workshopschedulingactivity;
use Fishman\WorkshopBundle\Entity\Workshopapplicationactivity;
use Fishman\WorkshopBundle\Entity\Workshopscheduling;
use Fishman\WorkshopBundle\Entity\Activity;
use Fishman\EntityBundle\Entity\Category;
use Fishman\WorkshopBundle\Form\WorkshopschedulingactivityType;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Workshopschedulingactivity controller.
 *
 */
class WorkshopschedulingactivityController extends Controller
{
    /**
     * Lists all Workshopschedulingactivity entities.
     *
     */
    public function indexAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            // Recovering data
            
            $activity_options = Activity::getListActivityOptions($this->getDoctrine());
            $category_options = Category::getListCategoryOptions($this->getDoctrine());
            $predecessor_options = Workshopschedulingactivity::getListWorkshopschedulingactivityOptions($this->getDoctrine(), $workshopschedulingid);
            $period_options = array('day' => 'Días', 'week' => 'Semanas', 'month' => 'Meses');
            for ($i = 1; $i<= 20; $i++) {
                $sequence_options[$i] = $i;
            }
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'activity' => '', 
                'category' => '', 
                'predecessor' => '', 
                'duration' => '', 
                'period' => '', 
                'initdate' => NULL, 
                'enddate' => NULL, 
                'sequence' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('activity', 'choice', array(
                    'choices' => $activity_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('category', 'choice', array(
                    'choices' => $category_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('predecessor', 'choice', array(
                    'choices' => $predecessor_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('duration', 'text', array(
                    'required' => FALSE
                ))
                ->add('period', 'choice', array(
                    'choices' => $period_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('enddate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('sequence', 'choice', array(
                    'choices' => $sequence_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'activity' => '', 
                'category' => '', 
                'predecessor' => '', 
                'duration' => '', 
                'period' => '', 
                'initdate' => NULL, 
                'enddate' => NULL, 
                'sequence' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
            $queryBuilder = $repository->createQueryBuilder('wsa')
                ->select('wsa.id, wsa.name, a.name activity, c.name category, pwsa.name predecessor, wsa.duration, wsa.period, 
                          wsa.datein, wsa.dateout, wsa.sequence, wsa.status, wsa.changed, u.names, u.surname, u.lastname, wsa.own')
                ->innerJoin('wsa.activity', 'a')
                ->innerJoin('wsa.category', 'c')
                ->leftJoin('FishmanWorkshopBundle:Workshopschedulingactivity', 'pwsa', 'WITH', 'wsa.predecessor_activity = pwsa.id')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wsa.modified_by = u.id')
                ->where('wsa.workshopscheduling = :workshopscheduling 
                        AND wsa.deleted = :deleted')
                ->andWhere('wsa.id LIKE :id 
                        OR wsa.name LIKE :name')
                ->setParameter('workshopscheduling', $workshopschedulingid)
                ->setParameter('deleted', FALSE)
                ->setParameter('id', '%' . $data['word'] . '%')
                ->setParameter('name', '%' . $data['word'] . '%')
                ->orderBy('wsa.id', 'ASC');
            
            // Add arguments
            
            if ($data['activity'] != '') {
                $queryBuilder
                    ->andWhere('wsa.activity = :activity')
                    ->setParameter('activity', $data['activity']);
            }
            if ($data['category'] != '') {
                $queryBuilder
                    ->andWhere('wsa.category = :category')
                    ->setParameter('category', $data['category']);
            }
            if ($data['predecessor'] != '') {
                $queryBuilder
                    ->andWhere('wsa.predecessor_activity = :predecessor')
                    ->setParameter('predecessor', $data['predecessor']);
            }
            if ($data['duration'] != '') {
                $queryBuilder
                    ->andWhere('wsa.duration LIKE :duration')
                    ->setParameter('duration', '%' . $data['duration'] . '%');
            }
            if ($data['period'] != '') {
                $queryBuilder
                    ->andWhere('wsa.period = :period')
                    ->setParameter('period', $data['period']);
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('wsa.datein = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('wsa.dateout = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
            if ($data['sequence'] != '') {
                $queryBuilder
                    ->andWhere('wsa.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }
            if ($data['status'] != '' || $data['status'] === 0) {
                $queryBuilder
                    ->andWhere('wsa.status = :status')
                    ->setParameter('status', $data['status']);
            }
            
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workshopschedulingactivity');
            $paginator->setMaxPagerItems(5, 'workshopschedulingactivity');
            $entities = $paginator->paginate($query, 'workshopschedulingactivity')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workshopschedulingactivity');
            $endPageItem = $paginator->getEndPageItem('workshopschedulingactivity');
            $totalItems = $paginator->getTotalItems('workshopschedulingactivity');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
            
            return $this->render('FishmanWorkshopBundle:Workshopschedulingactivity:index.html.twig', array(
                'entities' => $entities,
                'workshopscheduling' => $workshopscheduling,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Finds and displays a Workshopschedulingactivity entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $result = $repository->createQueryBuilder('wsa')
            ->select('wsa.id, c.name category, a.id activity_id, a.name activity, wsa.name name, wsa.answer_type, 
                      wsa.alignment, wsa.options, ps.title pollscheduling_title, wsap.name predecessor, wsa.duration, 
                      wsa.period, wsa.datein, wsa.dateout, wsa.sequence, wsa.status, wsa.own, wsa.created, wsa.changed, 
                      wsa.created_by, wsa.modified_by, ws.id workshopscheduling_id, ws.name workshopscheduling_name, 
                      ws.deleted workshopscheduling_deleted')
            ->innerJoin('wsa.workshopscheduling', 'ws')
            ->innerJoin('wsa.category', 'c')
            ->innerJoin('wsa.activity', 'a')
            ->leftJoin('FishmanPollBundle:Pollscheduling', 'ps', 'WITH', 'wsa.pollscheduling_id = ps.id')
            ->leftJoin('FishmanWorkshopBundle:Workshopschedulingactivity', 'wsap', 'WITH', 'wsa.predecessor_activity = wsap.id')
            ->where('wsa.id = :workshopschedulingactivity 
                    AND wsa.deleted = :deleted')
            ->setParameter('workshopschedulingactivity', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$entity['workshopscheduling_deleted']) {

            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
            
            return $this->render('FishmanWorkshopBundle:Workshopschedulingactivity:show.html.twig', array(
                'entity' => $entity,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Workshopschedulingactivity entity.
     *
     */
    public function newAction($workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {

            // Select Pullscheduling
            $select_pollscheduling = Workshopscheduling::getListPollschedulingToWorkshopschedulingOptions(
                $this->getDoctrine(), 
                $workshopschedulingid
            );

            $entity = new Workshopschedulingactivity();
        
            $entity->setWorkshopscheduling($workshopscheduling);
            $entity->setStatus(1);
            
            $form   = $this->createForm(new WorkshopschedulingactivityType($workshopschedulingid, $entity, true, $this->getDoctrine()), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopschedulingactivity:new.html.twig', array(
                'entity' => $entity,
                'workshopscheduling' => $workshopscheduling,
                'select_pollscheduling' => $select_pollscheduling,
                'form' => $form->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Creates a new Workshopschedulingactivity entity.
     *
     */
    public function createAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        $entity  = new Workshopschedulingactivity();
        
        // Recover options
        
        $options = array();
        $requestWorkshopSchedulingActivity = $request->request->get('fishman_workshopbundle_workshopschedulingactivitytype');
        
        if (in_array($requestWorkshopSchedulingActivity['answer_type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestWorkshopSchedulingActivity['options'])) {
                $optionsArray = explode('%%', $requestWorkshopSchedulingActivity['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }

        $form = $this->createForm(new WorkshopschedulingactivityType($workshopschedulingid, $entity, true, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            // Recover Initdate and Enddate Workshopscheduling
            $initdate = strtotime($workshopscheduling->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($workshopscheduling->getEnddate()->format('Y/m/d'));
            
            // Recover Datein and Dateout
            $datein = strtotime($entity->getDatein()->format('Y/m/d'));
            $dateout = strtotime($entity->getDateout()->format('Y/m/d'));
            
            if ($datein < $dateout) {
                
                $message = '';
                if ($datein < $initdate) {
                    $message .= 'La Fecha de Inicio de la actividad no puede ser menor a la Fecha de Inicio de la Programación de Taller.' . "</br>";
                }
                if ($dateout > $enddate) {
                    $message .= 'La Fecha de Fin de la actividad no puede ser mayor a la Fecha de Fin de la Programación de Taller.' . "</br>";
                }
                
                if ($message) {
                    $session->getFlashBag()->add('error', $message);
                    return $this->redirect($this->generateUrl('workshopschedulingactivity_new', array(
                        'workshopschedulingid' => $workshopschedulingid
                    )));
                }
                
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                $entity->setWorkshopscheduling($workshopscheduling);
                if ($entity->getActivity()->getId() != 4) {
                    $entity->setAnswerType('');
                }
                if (in_array($entity->getActivity()->getId(), array(1,2,3))) {
                    $entity->setPollschedulingId($_POST['workshopschedulingactivity_pollscheudling_id']);
                }
                if (in_array($entity->getAnswerType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                    $entity->setAlignment('');
                }
                if (in_array($entity->getAnswerType(), array('multiple_text', 'simple_text'))) {
                    $entity->setOptions(array());
                }
                else {
                    $entity->setOptions($options);
                }
                if ($entity->getPredecessorActivity() == '') {
                    $entity->setPredecessorActivity(-1);
                }
                $entity->setOwn(true);
                $entity->setDeleted(false);
                $entity->setCreated(new \DateTime());
                $entity->setChanged(new \DateTime());
                $entity->setCreatedBy($userBy->getId());
                $entity->setModifiedBy($userBy->getId());
                
                $em->persist($entity);
                $em->flush();
                
                Workshopapplicationactivity::createRegisters($this->getDoctrine(), $entity);
                
                $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');
                
                return $this->redirect($this->generateUrl('workshopschedulingactivity_show', array(
                    'id' => $entity->getId()
                )));
                
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }

        // Select Pullscheduling
        $select_pollscheduling = Workshopscheduling::getListPollschedulingToWorkshopschedulingOptions(
            $this->getDoctrine(), 
            $workshopschedulingid
        );

        return $this->render('FishmanWorkshopBundle:Workshopschedulingactivity:new.html.twig', array(
            'entity' => $entity,
            'workshopscheduling' => $workshopscheduling,
            'select_pollscheduling' => $select_pollscheduling, 
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Workshopschedulingactivity entity.
     *
     */
    public function editAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $result = $repository->createQueryBuilder('wsa')
            ->where('wsa.id = :workshopschedulingactivity 
                    AND wsa.deleted = :deleted')
            ->setParameter('workshopschedulingactivity', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$entity->getWorkshopscheduling()->getDeleted()) {

            // Select Pullscheduling
            $select_pollscheduling = Workshopscheduling::getListPollschedulingToWorkshopschedulingOptions(
                $this->getDoctrine(), 
                $entity->getWorkshopscheduling()->getId(), 
                $entity->getPollschedulingId()
            );

            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
            
            $editForm = $this->createForm(new WorkshopschedulingactivityType($entity->getWorkshopscheduling()->getId(), $entity, $entity->getOwn(), $this->getDoctrine()), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopschedulingactivity:edit.html.twig', array(
                'entity'      => $entity,
                'select_pollscheduling' => $select_pollscheduling, 
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'edit_form'   => $editForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Edits an existing Workshopschedulingactivity entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        $idOld = $entity->getId();
        $statusOld = $entity->getStatus();

        // Recover options
        
        $options = array();
        $requestWorkshopSchedulingActivity = $request->request->get('fishman_workshopbundle_workshopschedulingactivitytype');
        
        if (in_array($requestWorkshopSchedulingActivity['answer_type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestWorkshopSchedulingActivity['options'])) {
                $optionsArray = explode('%%', $requestWorkshopSchedulingActivity['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }
        
        $editForm = $this->createForm(new WorkshopschedulingactivityType($entity->getWorkshopscheduling()->getId(), $entity, $entity->getOwn(), $this->getDoctrine()), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            
            // Recover Initdate and Enddate Workshopscheduling
            $initdate = strtotime($entity->getWorkshopscheduling()->getInitdate()->format('Y/m/d'));
            $enddate = strtotime($entity->getWorkshopscheduling()->getEnddate()->format('Y/m/d'));
            
            // Recover Datein and Dateout
            $datein = strtotime($entity->getDatein()->format('Y/m/d'));
            $dateout = strtotime($entity->getDateout()->format('Y/m/d'));
            
            if ($datein < $dateout) {
                
                $message = '';
                if ($datein < $initdate) {
                    $message .= 'La Fecha de Inicio de la actividad no puede ser menor a la Fecha de Inicio de la Programación de Taller.' . "</br>";
                }
                if ($dateout > $enddate) {
                    $message .= 'La Fecha de Fin de la actividad no puede ser mayor a la Fecha de Fin de la Programación de Taller.' . "</br>";
                }
                
                if ($message) {
                    $session->getFlashBag()->add('error', $message);
                    return $this->redirect($this->generateUrl('workshopschedulingactivity_edit', array(
                        'id' => $entity->getId()
                    )));
                }
                
                // User
                $modifiedBy = $this->get('security.context')->getToken()->getUser();
                
                if ($entity->getStatus() == FALSE) {
                    $ns = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->findOneBy(
                        array('entity_type' => 'workshopscheduling', 'status' => 1, 
                              'notification_type_id' => $entity->getId() , 'notification_type' => 'activity', 
                              'entity_id' => $entity->getWorkshopscheduling()->getId()));

                    Notificationexecution::removeNotificationexecutions($this->getDoctrine(), $ns->getId(), 'workshopscheduling', $entity->getWorkshopscheduling()->getId(), '' , 'workshopapplicationactivity', $entity->getId());

                } elseif ($entity->getStatus() == TRUE) {
                    
                    if ($statusOld != $entity->getStatus()) {
                        
                        $workshopapplication = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findByWorkshopscheduling($entity->getWorkshopscheduling());
                        
                        foreach ($workshopapplication as $wapplication) {
                            
                            if ($wapplication->getDeleted() == FALSE) {
                                
                                $ns = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->findOneBy(
                                    array('entity_type' => 'workshopscheduling', 'status' => 1, 
                                          'notification_type_id' => $entity->getId() , 'notification_type' => 'activity', 
                                          'entity_id' => $entity->getWorkshopscheduling()->getId()));
                                
                                $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
                                $queryWaa = $repository->createQueryBuilder('waa')
                                    ->select('waa')
                                    ->innerJoin('waa.workshopapplication', 'wa')                              
                                    ->where('wa.workshopscheduling = :waid')
                                    ->setParameter('waid', $entity->getWorkshopscheduling())
                                    ->getQuery()
                                    ->getResult();
                                
                                  //Se genera un Notificationexecution por cada waa existente
                                foreach($queryWaa as $waa){
                                    $wiid = $wapplication->getWorkinginformation()->getId();
                                    
                                    $nx = Notificationexecution::generateNotificationexecution($ns, $wiid,
                                          'workshopapplicationactivity', $waa->getId(), $ns->getInitdate(),
                                          $ns->getEnddate(), NULL, 1, NULL, $this->getDoctrine());
                                    $em->persist($nx);
                                }
                                
                                $em->flush($nx);
                            }
                        }
                    }
                }
                
                if ($entity->getActivity()->getId() != 4) {
                    $entity->setAnswerType('');
                }
                if (in_array($entity->getActivity()->getId(), array(1,2,3))) {
                    $entity->setPollschedulingId($_POST['workshopschedulingactivity_pollscheudling_id']);
                }
                if (in_array($entity->getAnswerType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                    $entity->setAlignment('');
                }
                if (in_array($entity->getAnswerType(), array('multiple_text', 'simple_text'))) {
                    $entity->setOptions(array());
                }
                else {
                    $entity->setOptions($options);
                }
                if ($entity->getPredecessorActivity() == '') {
                    $entity->setPredecessorActivity(-1);
                }
                $entity->setChanged(new \DateTime());
                $entity->setModifiedBy($modifiedBy->getId());
                
                $em->persist($entity);
                $em->flush();
                
                Workshopapplicationactivity::updateRegisters($this->getDoctrine(), $entity);
                                           
                $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');
    
                return $this->redirect($this->generateUrl('workshopschedulingactivity_show', array(
                    'id' => $id
                )));
            
            }
            else {
                $session->getFlashBag()->add('error', 'La Fecha de Inicio debe ser menor a la Fecha de Fin.');
            }
        }
        
        // Select Pullscheduling
        $select_pollscheduling = Workshopscheduling::getListPollschedulingToWorkshopschedulingOptions(
            $this->getDoctrine(), 
            $entity->getWorkshopscheduling()->getId(), 
            $entity->getPollschedulingId()
        );

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanWorkshopBundle:Workshopschedulingactivity:edit.html.twig', array(
            'entity'      => $entity,
            'select_pollscheduling' => $select_pollscheduling,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Workshopscheduling activity entity.
     *
     */
    public function dropAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $result = $repository->createQueryBuilder('wsa')
            ->where('wsa.id = :workshopschedulingactivity 
                    AND wsa.deleted = :deleted')
            ->setParameter('workshopschedulingactivity', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        if (!$entity->getWorkshopscheduling()->getDeleted()) {
        
            if ($entity->getOwn()) {
              
                $deleteForm = $this->createDeleteForm($id);
        
                return $this->render('FishmanWorkshopBundle:Workshopschedulingactivity:drop.html.twig', array(
                    'entity'      => $entity,
                    'delete_form' => $deleteForm->createView()
                ));
            }
            
            $session->getFlashBag()->add('error', 'El registro no puede ser eliminado.');
    
            return $this->redirect($this->generateUrl('workshopschedulingactivity', array(
              'workshopschedulingid' => $entity->getWorkshopscheduling()->getId(),
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Deletes a Workshopschedulingactivity entity.
     *
     */
    public function deleteAction(Request $request, $id, $workshopschedulingid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $em = $this->getDoctrine()->getManager();
          
            $entity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la actividad de la programación de taller.');
                return $this->redirect($this->generateUrl('workshopschedulingactivity', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            $entity->setDeleted(TRUE);

            $em->persist($entity);
            $em->flush();
            
            Workshopapplicationactivity::deleteRegisters($this->getDoctrine(), $entity);
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');

        }

        return $this->redirect($this->generateUrl('workshopschedulingactivity', array(
          'workshopschedulingid' => $workshopschedulingid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
