<?php

namespace Fishman\WorkshopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('FishmanWorkshopBundle:Default:index.html.twig', array('name' => $name));
    }
}
