<?php

namespace Fishman\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\WorkshopBundle\Entity\Workshopactivity;
use Fishman\WorkshopBundle\Form\WorkshopactivityType;
use Fishman\WorkshopBundle\Entity\Workshop;
use Fishman\WorkshopBundle\Entity\Activity;
use Fishman\EntityBundle\Entity\Category;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Workshopactivity controller.
 *
 */
class WorkshopactivityController extends Controller
{
    /**
     * Lists all Workshopactivity entities.
     *
     */
    public function indexAction(Request $request, $workshopid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshop Info
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Recovering data
        
        $activity_options = Activity::getListActivityOptions($this->getDoctrine());
        $category_options = Category::getListCategoryOptions($this->getDoctrine());
        $predecessor_options = Workshopactivity::getListWorkshopactivityOptions($this->getDoctrine(), $workshopid);
        $period_options = array('day' => 'Días', 'week' => 'Semanas', 'month' => 'Meses');
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'activity' => '', 
            'category' => '', 
            'predecessor' => '', 
            'duration' => '', 
            'period' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('activity', 'choice', array(
                'choices' => $activity_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('category', 'choice', array(
                'choices' => $category_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('predecessor', 'choice', array(
                'choices' => $predecessor_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('duration', 'text', array(
                'required' => FALSE
            ))
            ->add('period', 'choice', array(
                'choices' => $period_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'activity' => '', 
            'category' => '', 
            'predecessor' => '', 
            'duration' => '', 
            'period' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopactivity');
        $queryBuilder = $repository->createQueryBuilder('wa')
            ->select('wa.id, wa.name, a.name activity, c.name category, wap.name predecessor, wa.duration, wa.period, 
                      wa.sequence, wa.status, wa.changed, u.names, u.surname, u.lastname')
            ->innerJoin('wa.activity', 'a')
            ->innerJoin('wa.category', 'c')
            ->leftJoin('FishmanWorkshopBundle:Workshopactivity', 'wap', 'WITH', 'wa.predecessor_activity = wap.id')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wa.modified_by = u.id')
            ->where('wa.workshop = :workshop') 
            ->andWhere('wa.id LIKE :id 
                    OR wa.name LIKE :name')
            ->setParameter('workshop', $workshopid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('wa.id', 'ASC');
        
        // Add arguments
        
        if ($data['activity'] != '') {
            $queryBuilder
                ->andWhere('wa.activity = :activity')
                ->setParameter('activity', $data['activity']);
        }
        if ($data['category'] != '') {
            $queryBuilder
                ->andWhere('wa.category = :category')
                ->setParameter('category', $data['category']);
        }
        if ($data['predecessor'] != '') {
            $queryBuilder
                ->andWhere('wa.predecessor_activity = :predecessor')
                ->setParameter('predecessor', $data['predecessor']);
        }
        if ($data['duration'] != '') {
            $queryBuilder
                ->andWhere('wa.duration LIKE :duration')
                ->setParameter('duration', '%' . $data['duration'] . '%');
        }
        if ($data['period'] != '') {
            $queryBuilder
                ->andWhere('wa.period = :period')
                ->setParameter('period', $data['period']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('wa.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('wa.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopactivity');
        $paginator->setMaxPagerItems(5, 'workshopactivity');
        $entities = $paginator->paginate($query, 'workshopactivity')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopactivity');
        $endPageItem = $paginator->getEndPageItem('workshopactivity');
        $totalItems = $paginator->getTotalItems('workshopactivity');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanWorkshopBundle:Workshopactivity:index.html.twig', array(
            'entities' => $entities,
            'workshop' => $workshop,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Workshopactivity entity.
     *
     */
    public function showAction($id)
    {
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopactivity');
        $result = $repository->createQueryBuilder('wa')
            ->select('wa.id, c.name category, a.id activity_id, a.name activity, wa.name name, wa.answer_type, 
                      wa.alignment, wa.options, wap.name predecessor, wa.duration, wa.period, wa.sequence, wa.status, 
                      wa.created, wa.changed, wa.created_by, wa.modified_by, w.id workshop_id, w.name workshop_name')
            ->innerJoin('wa.workshop', 'w')
            ->innerJoin('wa.category', 'c')
            ->innerJoin('wa.activity', 'a')
            ->leftJoin('FishmanWorkshopBundle:Workshopactivity', 'wap', 'WITH', 'wa.predecessor_activity = wap.id')
            ->where('wa.id = :workshopactivity')
            ->setParameter('workshopactivity', $id)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanWorkshopBundle:Workshopactivity:show.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Workshopactivity entity.
     *
     */
    public function newAction($workshopid)
    {
        $entity = new Workshopactivity();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover workshop
        $em = $this->getDoctrine()->getManager();
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $entity->setWorkshop($workshop);
        $entity->setStatus(1);
        $form   = $this->createForm(new WorkshopactivityType($workshopid, $entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanWorkshopBundle:Workshopactivity:new.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new Workshopactivity entity.
     *
     */
    public function createAction(Request $request, $workshopid)
    {
        $entity  = new Workshopactivity();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $em = $this->getDoctrine()->getManager();
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Recover options
        
        $options = array();
        $requestWorkshopActivity = $request->request->get('fishman_workshopbundle_workshopactivitytype');
        
        if (in_array($requestWorkshopActivity['answer_type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestWorkshopActivity['options'])) {
                $optionsArray = explode('%%', $requestWorkshopActivity['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }

        $form = $this->createForm(new WorkshopactivityType($workshopid, $entity, $this->getDoctrine()), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();

            $entity->setWorkshop($workshop);
            if ($entity->getActivity()->getId() != 4) {
              $entity->setAnswerType('');
            }
            if (in_array($entity->getAnswerType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                $entity->setAlignment('');
            }
            if (in_array($entity->getAnswerType(), array('multiple_text', 'simple_text'))) {
                $entity->setOptions(array());
            }
            else {
                $entity->setOptions($options);
            }
            if ($entity->getPredecessorActivity() == '') {
                $entity->setPredecessorActivity(-1);
            }
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $em->persist($entity);
            $em->flush();

            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopactivity_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanWorkshopBundle:Workshopactivity:new.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Workshopactivity entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopactivity')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new WorkshopactivityType($entity->getWorkshop()->getId(), $entity, $this->getDoctrine()), $entity);

        return $this->render('FishmanWorkshopBundle:Workshopactivity:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Workshopactivity entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopactivity')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Recover options
        
        $options = array();
        $requestWorkshopActivity = $request->request->get('fishman_workshopbundle_workshopactivitytype');
        
        if (in_array($requestWorkshopActivity['answer_type'], array('unique_option', 'multiple_option', 'selection_option'))) {
            if (!empty($requestWorkshopActivity['options'])) {
                $optionsArray = explode('%%', $requestWorkshopActivity['options']);
                $i = 0;
                foreach ($optionsArray as $op_text) {
                    $op = explode('::', $op_text);
                    if (!empty($op)) {
                        $option = explode(' | ', $op[1]);
                        $options[$op[0]] = array('score' => $option[0], 'text' => $option[1]);
                    }
                    $i++;
                }
            }
        }
        
        $editForm = $this->createForm(new WorkshopactivityType($entity->getWorkshop()->getId(), $entity, $this->getDoctrine()), $entity);
        $editForm->bind($request);
        
        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();

            if ($entity->getActivity()->getId() != 4) {
                $entity->setAnswerType('');
            }
            if (in_array($entity->getAnswerType(), array('selection_option', 'multiple_text', 'simple_text'))) {
                $entity->setAlignment('');
            }
            if (in_array($entity->getAnswerType(), array('multiple_text', 'simple_text'))) {
                $entity->setOptions(array());
            }
            else {
                $entity->setOptions($options);
            }
            if ($entity->getPredecessorActivity() == '') {
                $entity->setPredecessorActivity(-1);
            }
            $entity->setChanged(new \DateTime());
            $entity->setModifiedBy($modifiedBy->getId());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopactivity_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanWorkshopBundle:Workshopactivity:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Workshopactivity entity.
     *
     */
    public function deleteAction(Request $request, $id, $workshopid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FishmanWorkshopBundle:Workshopactivity')->find($id);

            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la actividad del taller.');
                return $this->redirect($this->generateUrl('workshopactivity', array(
                    'workshopid' => $workshopid    
                )));
            }

            $em->remove($entity);
            $em->flush();

            // set flash messages                                                                            
            $session = $this->getRequest()->getSession();                                                    
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');

        }

        return $this->redirect($this->generateUrl('workshopactivity', array(
          'workshopid' => $workshopid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Workshopactivity entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopactivity')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanWorkshopBundle:Workshopactivity:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }
}
