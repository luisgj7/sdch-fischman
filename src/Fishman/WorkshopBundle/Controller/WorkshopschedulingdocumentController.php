<?php

namespace Fishman\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\WorkshopBundle\Entity\Workshopschedulingdocument;
use Fishman\WorkshopBundle\Entity\Workshopdocument;
use Fishman\EntityBundle\Entity\Category;
use Fishman\WorkshopBundle\Form\WorkshopschedulingdocumentType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Workshopschedulingdocument controller.
 *
 */
class WorkshopschedulingdocumentController extends Controller
{
    /**
     * Lists all Workshopschedulingdocument entities.
     *
     */
    public function indexAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
          
            // Recovering data
            
            $type_options = Workshopdocument::getListTypeOptions();
            $category_options = Category::getListCategoryOptions($this->getDoctrine());
            for ($i = 1; $i<= 20; $i++) {
                $sequence_options[$i] = $i;
            }
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'type' => '', 
                'category' => '', 
                'sequence' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('type', 'choice', array(
                    'choices' => $type_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('category', 'choice', array(
                    'choices' => $category_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('sequence', 'choice', array(
                    'choices' => $sequence_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'type' => '', 
                'category' => '', 
                'sequence' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument');
            $queryBuilder = $repository->createQueryBuilder('wsd')
                ->select('wsd.id, wsd.name, wsd.type, c.name category, wsd.sequence, wsd.status, wsd.changed, u.names, u.surname, u.lastname, wsd.own')
                ->innerJoin('wsd.category', 'c')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wsd.modified_by = u.id')
                ->where('wsd.workshopscheduling = :workshopscheduling') 
                ->andWhere('wsd.id LIKE :id 
                        OR wsd.name LIKE :name')
                ->setParameter('workshopscheduling', $workshopschedulingid)
                ->setParameter('id', '%' . $data['word'] . '%')
                ->setParameter('name', '%' . $data['word'] . '%')
                ->orderBy('wsd.id', 'ASC');
        
            // Add arguments
            
            if ($data['type'] != '') {
                $queryBuilder
                    ->andWhere('wsd.type = :type')
                    ->setParameter('type', $data['type']);
            }
            if ($data['category'] != '') {
                $queryBuilder
                    ->andWhere('wsd.category = :category')
                    ->setParameter('category', $data['category']);
            }
            if ($data['sequence'] != '') {
                $queryBuilder
                    ->andWhere('wsd.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }
            if ($data['status'] != '' || $data['status'] === 0) {
                $queryBuilder
                    ->andWhere('wsd.status = :status')
                    ->setParameter('status', $data['status']);
            }
            
            $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workshopschedulingdocument');
            $paginator->setMaxPagerItems(5, 'workshopschedulingdocument');
            $entities = $paginator->paginate($query, 'workshopschedulingdocument')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workshopschedulingdocument');
            $endPageItem = $paginator->getEndPageItem('workshopschedulingdocument');
            $totalItems = $paginator->getTotalItems('workshopschedulingdocument');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
            
            return $this->render('FishmanWorkshopBundle:Workshopschedulingdocument:index.html.twig', array(
                'entities' => $entities,
                'workshopscheduling' => $workshopscheduling,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Finds and displays a Workshopschedulingdocument entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$entity->getWorkshopscheduling()->getDeleted()) {

            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
    
            return $this->render('FishmanWorkshopBundle:Workshopschedulingdocument:show.html.twig', array(
                'entity'      => $entity,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Workshopschedulingdocument entity.
     *
     */
    public function newAction($workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
          
            $entity = new Workshopschedulingdocument();
            
            $entity->setType('d');
            $entity->setWorkshopscheduling($workshopscheduling);
            $entity->setStatus(1);
            $form   = $this->createForm(new WorkshopschedulingdocumentType(true), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopschedulingdocument:new.html.twig', array(
                'entity' => $entity,
                'workshopschedulingid' => $workshopschedulingid,
                'form'   => $form->createView(),
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

   /** 
     * Creates a new Workshopschedulingdocument entity.
     *
     */
    public function createAction(Request $request, $workshopschedulingid)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        $entity  = new Workshopschedulingdocument();
        
        $form = $this->createForm(new WorkshopschedulingdocumentType(true), $entity);
        $form->bind($request);
   
        if ($form->isValid()) {

            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setWorkshopscheduling($workshopscheduling);
            if ($entity->getType() == 'video') {
                $entity->setDocument('');
                $entity->setWebsite('');
            }
            elseif ($entity->getType() == 'document') {
                $entity->setVideo('');
                $entity->setWebsite('');
            }
            elseif ($entity->getType() == 'website') {
                $entity->setVideo('');
                $entity->setDocument('');
            }
            $entity->setOwn(true);
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopschedulingdocument_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanWorkshopBundle:Workshopschedulingdocument:new.html.twig', array(
            'entity' => $entity,
            'workshopschedulingid' => $workshopschedulingid,
            'form'   => $form->createView(),
        )); 
    }  

    /**
     * Displays a form to edit an existing Workshopschedulingdocument entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$entity->getWorkshopscheduling()->getDeleted()) {

            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
    
            $editForm = $this->createForm(new WorkshopschedulingdocumentType($entity->getOwn()), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopschedulingdocument:edit.html.twig', array(
                'entity'      => $entity,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'edit_form'   => $editForm->createView()
            ));
        
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Edits an existing Workshopschedulingdocument entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        $editForm = $this->createForm(new WorkshopschedulingdocumentType($entity->getOwn()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            if ($entity->getType() == 'video') {
                $entity->setDocument('');
                $entity->setWebsite('');
                $entity->setEmbed('');
            }
            elseif ($entity->getType() == 'document') {
                $entity->setVideo('');
                $entity->setWebsite('');
                $entity->setEmbed('');
            }
            elseif ($entity->getType() == 'website') {
                $entity->setVideo('');
                $entity->setDocument('');
                $entity->setEmbed('');
            }
            elseif ($entity->getType() == 'embed') {
                $entity->setVideo('');
                $entity->setDocument('');
                $entity->setWebsite('');
            }
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.'); 

            return $this->redirect($this->generateUrl('workshopschedulingdocument_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanWorkshopBundle:Workshopschedulingdocument:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Workshopscheduling document entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        if (!$entity->getWorkshopscheduling()->getDeleted()) {
        
            if ($entity->getOwn()) {
              
                $deleteForm = $this->createDeleteForm($id);
        
                return $this->render('FishmanWorkshopBundle:Workshopschedulingdocument:drop.html.twig', array(
                    'entity'      => $entity,
                    'delete_form' => $deleteForm->createView()
                ));
            }
            
            $session->getFlashBag()->add('error', 'El registro no puede ser eliminado.');
    
            return $this->redirect($this->generateUrl('workshopschedulingdocument', array(
              'workshopschedulingid' => $entity->getWorkshopscheduling()->getId(),
            )));
        
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Deletes a Workshopschedulingdocument entity.
     *
     */
    public function deleteAction(Request $request, $id, $workshopschedulingid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el documento de la programación de taller.');
                return $this->redirect($this->generateUrl('workshopschedulingdocument', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }

            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');

        }

        return $this->redirect($this->generateUrl('workshopschedulingdocument', array(
          'workshopschedulingid' => $workshopschedulingid,
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Download document.
     * TODO: Access control. The user can access actual Workshpapplication documents
     */
    public function downloadAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible workinginformation
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento de la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        if ($rol == 'ROLE_USER') {
            $wa = $session->get('workshopapplication');
            if ($entity->getWorkshopscheduling()->getId() != $wa['ws_id']) {
                $session->getFlashBag()->add('error', 'Usted no tiene permisos para descargar este archivo o no se encuentra en el taller apropiado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }

        $file = new File($entity->getAbsolutePath());
        $headers = array(
            'Content-Type' => $file->getMimeType(),
            'Content-Disposition' => 'attachment; filename="'.$entity->getName().'.'.$file->getExtension().'"'
        );
        return new Response(file_get_contents($entity->getAbsolutePath()), 200, $headers);
    }
}
