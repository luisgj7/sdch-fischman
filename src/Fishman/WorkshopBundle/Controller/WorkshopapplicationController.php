<?php

namespace Fishman\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\WorkshopBundle\Entity\Workshopscheduling;
use Fishman\WorkshopBundle\Entity\Workshopapplication;
use Fishman\WorkshopBundle\Entity\Workshopapplicationactivity;
use Fishman\PollBundle\Entity\Pollapplication;
use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\EntityBundle\Entity\Companycharge;
use Fishman\ImportExportBundle\Entity\Temporalimportdata;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Fishman\WorkshopBundle\Form\WorkshopapplicationType;
use Fishman\WorkshopBundle\Form\WorkshopschedulingpeopleimportType;

use Symfony\Component\HttpFoundation\Response;

use PHPExcel_IOFactory;

/**
 * Workshopapplication controller.
 *
 */
class WorkshopapplicationController extends Controller
{
    /* TODO: Las respectivas actividades se crearan en el momento en que el usuario ejecute su aplicación */
    
    /**
     * Lists all Workshopapplication entities.
     *
     */
    public function indexAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshopscheduling Info

        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
          
            // Recovering data
            
            $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $workshopscheduling->getCompany()->getId());
            $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $workshopscheduling->getCompany()->getId());
            $profile_options = Workshopapplication::getListProfileOptions();
            $status_options = array(0 => 'Desactivo', 1 => 'Activo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'organizationalunit' => '', 
                'charge' => '', 
                'profile' => '', 
                'status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('organizationalunit', 'choice', array(
                    'choices' => $organizationalunit_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('charge', 'choice', array(
                    'choices' => $charge_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('profile', 'choice', array(
                    'choices' => $profile_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'organizationalunit' => '', 
                'charge' => '', 
                'profile' => '', 
                'status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Delete registers
            
            $deletes_form = $this->createFormBuilder()
                ->add('registers', 'choice', array(
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => true
                ))
                ->getForm();
                
            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
            $queryBuilder = $repository->createQueryBuilder('wa')
                ->select('wa.id, wi.code, wi.type, wau.names wau_names, wau.surname wau_surname, wau.lastname wau_lastname, cou.name organizationalunit, 
                          cch.name charge, wa.profile, wa.status, wa.changed, u.names, u.surname, u.lastname')
                ->innerJoin('wa.workinginformation', 'wi')
                ->leftJoin('wi.companyorganizationalunit', 'cou')
                ->leftJoin('wi.companycharge', 'cch')
                ->innerJoin('wi.user', 'wau')
                ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wa.modified_by = u.id')
                ->where('wa.workshopscheduling = :workshopscheduling 
                        AND wa.deleted = :deleted')
                ->andWhere('wi.code LIKE :code 
                        OR wau.names LIKE :names 
                        OR wau.surname LIKE :surname 
                        OR wau.lastname LIKE :lastname ')
                ->setParameter('workshopscheduling', $workshopschedulingid)
                ->setParameter('deleted', FALSE)
                ->setParameter('code', '%' . $data['word'] . '%')
                ->setParameter('names', '%' . $data['word'] . '%')
                ->setParameter('surname', '%' . $data['word'] . '%')
                ->setParameter('lastname', '%' . $data['word'] . '%')
                ->orderBy('wau.surname', 'ASC', 'wau.lastname', 'ASC', 'wau.names', 'ASC');
        
        // Add arguments
        
        if ($data['organizationalunit'] != '') {
            $queryBuilder
                ->andWhere('cou.id = :organizationalunit')
                ->setParameter('organizationalunit', $data['organizationalunit']);
        }
        if ($data['charge'] != '') {
            $queryBuilder
                ->andWhere('cch.id = :charge')
                ->setParameter('charge', $data['charge']);
        }
        if ($data['profile'] != '') {
            $queryBuilder
                ->andWhere('wa.profile = :profile')
                ->setParameter('profile', $data['profile']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('wa.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workshopapplication');
            $paginator->setMaxPagerItems(5, 'workshopapplication');
            $entities = $paginator->paginate($query, 'workshopapplication')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workshopapplication');
            $endPageItem = $paginator->getEndPageItem('workshopapplication');
            $totalItems = $paginator->getTotalItems('workshopapplication');
            
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
            
            return $this->render('FishmanWorkshopBundle:Workshopapplication:index.html.twig', array(
                'entities' => $entities,
                'workshopscheduling' => $workshopscheduling,
                'form' => $form->createView(),
                'deletes_form' => $deletes_form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Finds and displays a Workshopapplication entity.
     *
     */
    public function showAction($id)
    {   
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Query
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $result = $repository->createQueryBuilder('wa')
            ->select('wa.id, wi.code, wi.type, u.names, u.surname, u.lastname, u.sex, u.email, wa.profile, 
                      wa.status, wa.created, wa.changed, wa.created_by, wa.modified_by, 
                      ws.id workshopscheduling_id, ws.name workshopscheduling_name, ws.deleted workshopscheduling_deleted')
            ->innerJoin('wa.workshopscheduling', 'ws')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('wi.user', 'u')
            ->where('wa.id = :workshopschedulingpeople 
                    AND wa.deleted = :deleted')
            ->setParameter('workshopschedulingpeople', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();

        $entity = current($result);
        
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la persona asignada a la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        if (!$entity['workshopscheduling_deleted']) {
          
            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
    
            return $this->render('FishmanWorkshopBundle:Workshopapplication:show.html.twig', array(
                'entity' => $entity,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Displays a form to create a new Workshopapplication entity.
     *
     */
    public function newAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
            
            $repository = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
            $wsaIncomplete = $repository->createQueryBuilder('wsa')
                ->where('wsa.datein IS NULL 
                        OR wsa.datein IS NULL')
                ->andWhere('wsa.workshopscheduling = :workshopschedulingid')
                ->setParameter('workshopschedulingid', $workshopschedulingid)
                ->orderBy('wsa.id', 'ASC')
                ->getQuery()
                ->getResult();
            
            if ($wsaIncomplete || ($wsaIncomplete->getActivity()->getId() != 4 && !$wsaIncomplete->getPollschedulingId())) {
                $session->getFlashBag()->add('error', 'Existen actividades con datos faltantes. Favor de completar estos datos.');
                return $this->redirect($this->generateUrl('workshopschedulingactivity', array(
                    'workshopschedulingid' => $workshopschedulingid
                )));
            }
            
            // Recover Data
            $organizationalunit_options = Companyorganizationalunit::getCOUOptions($this->getDoctrine(), $workshopscheduling->getCompany()->getId());
            $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $workshopscheduling->getCompany()->getId());
            
            // Find Workinginformations
            
            $defaultData = array(
                'code' => '', 
                'surname' => '', 
                'lastname' => '', 
                'names' => '', 
                'initdate' => NULL, 
                'organizationalunit' => '', 
                'charge' => ''
            );
            $form = $this->createFormBuilder($defaultData)
                ->add('code', 'text', array(
                    'required'=>false
                ))
                ->add('surname', 'text', array(
                    'required'=>false
                ))
                ->add('lastname', 'text', array(
                    'required'=>false
                ))
                ->add('names', 'text', array(
                    'required'=>false
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => false
                ))
                ->add('organizationalunit', 'choice', array(
                    'choices' => $organizationalunit_options, 
                    'empty_value' => 'Choose an option',
                    'required' => false
                ))
                ->add('charge', 'choice', array(
                    'choices' => $charge_options, 
                    'empty_value' => 'Choose an option',
                    'required' => false
                ))
                ->getForm();
    
            $data = array(
                'code' => '', 
                'surname' => '', 
                'lastname' => '', 
                'names' => '', 
                'initdate' => NULL, 
                'organizationalunit' => '', 
                'charge' => ''
            );
            if($request->getMethod() == 'POST') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Recover Workshopapplications ids
            
            $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
            $wa_result = $repository->createQueryBuilder('wa')
                ->select('wi.id')
                ->innerJoin('wa.workinginformation', 'wi')
                ->where('wa.workshopscheduling = :workshopscheduling
                        AND wi.company = :company 
                        AND wa.deleted = :deleted')
                ->setParameter('workshopscheduling', $workshopscheduling->getId())
                ->setParameter('company', $workshopscheduling->getCompany()->getId())
                ->setParameter('deleted', FALSE)
                ->orderBy('wi.id', 'ASC')
                ->getQuery()
                ->getResult();
            
            $wi_ids = '';
            
            // We set ids chain Workshopapplications
            
            if (!empty($wa_result)) {
                
                $wa_count = count($wa_result);
                $i = 1;
                $wi_ids = '';
                
                foreach ($wa_result as $wa) {
                    $wi_ids .= $wa['id'];
                    if ($i < $wa_count) {
                        $wi_ids .= ',';
                    }
                    $i++;
                }
            }

            // Query
            
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $queryBuilder = $repository->createQueryBuilder('wi')
                ->select('wi.id, wi.code, wi.type, u.names, u.surname, u.lastname, co.name companyorganizationalunit, 
                          cc.name companycharge, wi.datein, wi.status')
                ->leftJoin('wi.companyorganizationalunit', 'co')
                ->leftJoin('wi.companycharge', 'cc')
                ->innerJoin('wi.user', 'u')
                ->where('wi.company = :company  
                        AND wi.status = 1')
                ->setParameter('company', $workshopscheduling->getCompany()->getId())
                ->orderBy('u.surname', 'ASC', 'u.lastname', 'ASC', 'u.names', 'ASC')
                ->groupBy('wi.id');
            
            // Check if there are people assigned
            
            if ($wi_ids != '') {
                $queryBuilder
                    ->andWhere('wi.id NOT IN(' . $wi_ids . ')');
            }
            
            // Add the arguments that are being asked
            
            if ($data['code'] != '') {
                $queryBuilder
                    ->andWhere('wi.code LIKE :code')
                    ->setParameter('code', '%' . $data['code'] . '%');
            }
            if (!empty($data['surname'])) {
                $queryBuilder
                    ->andWhere('u.surname LIKE :surname')
                    ->setParameter('surname', '%' . $data['surname'] . '%');
            }
            if (!empty($data['lastname'])) {
                $queryBuilder
                    ->andWhere('u.lastname LIKE :lastname')
                    ->setParameter('lastname', '%' . $data['lastname'] . '%');
            }
            if (!empty($data['names'])) {
                $queryBuilder
                    ->andWhere('u.names LIKE :names')
                    ->setParameter('names', '%' . $data['names'] . '%');
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('wi.datein = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            elseif ($data['organizationalunit'] != '') {
                $queryBuilder
                    ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                    ->setParameter('organizationalunit', $data['organizationalunit']);
            }
            elseif ($data['charge'] != '') {
                $queryBuilder
                    ->andWhere('wi.companycharge = :charge')
                    ->setParameter('charge', $data['charge']);
            }
            
            $query = $queryBuilder->getQuery();
    
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workinginformation');
            $paginator->setMaxPagerItems(5, 'workinginformation');
            $entities = $paginator->paginate($query, 'workinginformation')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workinginformation');
            $endPageItem = $paginator->getEndPageItem('workinginformation');
            $totalItems = $paginator->getTotalItems('workinginformation');
        
            if ($totalItems == 0) {
                $info_paginator = 'No hay registros que mostrar';
            }
            else {
                $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            }
            
            // Asigned registers
            
            $asigneds_form = $this->createFormBuilder()
                ->add('registers', 'choice', array(
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => true
                ))
                ->add('profile', 'choice', array(
                    'choices' => array(
                        'boss' => 'Jefe',
                        'collaborator' => 'Colaborador',
                        'integrant' => 'Participante',
                        'company' => 'Empresa'/*,
                        'responsible' => 'Responsable'*/
                    ),
                    'empty_value' => 'Choose an option',
                    'required' => false
                ))
                ->getForm();
            
            // Return new.html.twig
            
            return $this->render('FishmanWorkshopBundle:Workshopapplication:new.html.twig', array(
                'entities' => $entities,
                'workshopscheduling' => $workshopscheduling,
                'form' => $form->createView(),
                'asigneds_form' => $asigneds_form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator
            ));
        
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Asigneds Workshopapplications.
     *
     */
    public function asignedsAction(Request $request, $workshopschedulingid)
    {
        $data = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if (!empty($data['registers'])) {
              
            $em = $this->getDoctrine()->getManager();
            
            if (!empty($data['profile'])) {
                
                $numberRegister = 0;
                
                // User
                $userBy = $this->get('security.context')->getToken()->getUser();
                
                // We travel registers
                foreach ($data['registers'] as $r) {
                    
                    $workinginformation = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($r);
                    
                    Workshopapplication::addRegister($this->getDoctrine(), $data['profile'], $workinginformation, $workshopschedulingid, $userBy);
                    
                    $numberRegister++;
                }
                $session->getFlashBag()->add('status', 'Se asignaron ' . $numberRegister . ' Persona(s) al taller satisfactoriamente');
            }
        }
        else {
            $session->getFlashBag()->add('error', 'No escogio ninguna persona para asignar al taller.');
        }

        if($request->request->get('saveandclose', true)){
            return $this->redirect($this->generateUrl('workshopapplication', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }
        else {
            return $this->redirect($this->generateUrl('workshopapplication_new', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }
    }

    /**
     * Displays a form to edit an existing Workshopapplication entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopapplication
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $result = $repository->createQueryBuilder('wa')
            ->where('wa.id = :workshopapplication  
                    AND wa.deleted = :deleted')
            ->setParameter('workshopapplication', $id)
            ->setParameter('deleted', FALSE)
            ->getQuery()
            ->getResult();
            
        $entity = current($result);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la persona asignada a la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$entity->getWorkshopscheduling()->getDeleted()) {

            // User
            $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
            $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
            $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
            $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
            
            $editForm = $this->createForm(new WorkshopapplicationType(), $entity);
    
            return $this->render('FishmanWorkshopBundle:Workshopapplication:edit.html.twig', array(
                'entity' => $entity,
                'createdByName' => $createdByName,
                'modifiedByName' => $modifiedByName,
                'edit_form' => $editForm->createView()
            ));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Edits an existing Workshopapplication entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la persona asignada a la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }

        $editForm = $this->createForm(new WorkshopapplicationType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());

            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopapplication_show', array(
                'id' => $id
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanWorkshopBundle:Workshopapplication:edit.html.twig', array(
            'entity' => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Drop multiple an Workshopapplications entities.
     *
     */
    public function dropmultipleAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        if (!$workshopscheduling->getDeleted()) {
          
            // Recover register of form
            
            $data = $request->request->get('form_deletes');
            
            if (isset($data['registers'])) {
            
                $registers = '';
                $registers_count = count($data['registers']);
                $i = 1;
                
                if (!empty($data['registers'])) {
                    
                    foreach($data['registers'] as $r) {
                        $registers .= $r;
                        if ($i < $registers_count) {
                            $registers .= ',';
                        }
                        $i++;
                    }
        
                    // Recover Workshopapplication of Workshopscheduling
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                    $entities = $repository->createQueryBuilder('wa')
                        ->where('wa.id IN(' . $registers . ')')
                        ->getQuery()
                        ->getResult();
                    if (!$entities) {
                        throw $this->createNotFoundException('Unable to find Workshopapplications entities.');
                    }
                
                    $deleteMultipleForm = $this->createDeleteMultipleForm($registers);
                
                    return $this->render('FishmanWorkshopBundle:Workshopapplication:dropmultiple.html.twig', array(
                        'entities' => $entities,
                        'workshopscheduling' => $workshopscheduling,
                        'delete_form' => $deleteMultipleForm->createView()
                    ));
                }
            }
            
            $session->getFlashBag()->add('error', 'No se ha seleccionado ningún registro para eliminar.');
            
            return $this->redirect($this->generateUrl('workshopapplication', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
            
        }
        else {
            $session->getFlashBag()->add('error', 'El Taller ha sido eliminado.');

            return $this->redirect($this->generateUrl('workshopscheduling', array()));
        }
    }

    /**
     * Deletes multiple a Workshopapplications entities.
     *
     */
    public function deletemultipleAction(Request $request, $workshopschedulingid)
    {    
        $form = $request->request->get('form');
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        if ($form['registers'] != '') {
         
            $em = $this->getDoctrine()->getManager();
            
            // Recover Workshopapplications of form
            $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
            $entities = $repository->createQueryBuilder('wa')
                ->where('wa.id IN(' . $form['registers'] . ')')
                ->getQuery()
                ->getResult();
            if (!$entities) {
                $session->getFlashBag()->add('error', 'No se puede encontrar las personas asignadas a la programación de taller.');
                return $this->redirect($this->generateUrl('workshopscheduling'));
            }
            
            $message = '';
            $messageError = '';
            $messagePeople = '';
            $deleted = 0;
            $noDeleted = 0;
            
            // Recover Pollschedulings of workshopscheduling
                
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling');
            $pscs = $repository->createQueryBuilder('psc')
                ->where('psc.entity_id = :workshopscheduling 
                        AND psc.entity_type = :entity_type')
                ->setParameter('workshopscheduling', $workshopschedulingid)
                ->setParameter('entity_type', 'workshopscheduling')
                ->getQuery()
                ->getResult();
                
            if ($pscs) {
                $i = 0;
                foreach ($pscs as $psc) {
                    if ($i == 0) {
                        $pscIds = $psc->getId();
                    }
                    else {
                        $pscIds .= ', ' . $psc->getId();
                    }
                    $i++;
                }
            }
            
            foreach($entities as $entity) {
              
                if ($pscs) {
                    $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                    $pa = $repository->createQueryBuilder('pa')
                        ->where('pa.evaluated_id = :evaluated 
                                OR pa.evaluator_id = :evaluator')
                        ->andWhere('pa.pollscheduling IN(' . $pscIds . ')')
                        ->andWhere('pa.deleted = 0')
                        ->setParameter('evaluated', $entity->getWorkinginformation()->getId())
                        ->setParameter('evaluator', $entity->getWorkinginformation()->getId())
                        ->getQuery()
                        ->getResult();
                }
                else {
                    $pa = FALSE;
                }
                
                if ($pa) {
                    $upa = $entity->getWorkinginformation()->getUser();
                    if ($noDeleted > 0) {
                        $messagePeople .= '</br>';
                    }
                    $messagePeople .= $upa->getNames() . ' ' . $upa->getSurname() . ' ' . $upa->getLastname();
                    $noDeleted++;
                }
                else {
                    
                    if ($entity->getProfile() == 'integrant') {
                        Workshopapplicationactivity::integrantRegisters($this->getDoctrine(), $entity, TRUE);
                    }
    
                    Notificationexecution::removeNotificationexecutions($this->getDoctrine(), '', 'workshopscheduling', $entity->getWorkshopscheduling()->getId(), $entity->getWorkinginformation()->getId());
    
                    $entity->setDeleted(TRUE);
        
                    $em->persist($entity);
                    $em->flush();
                    
                    $deleted++;
                }
                
            }

            if ($messagePeople != '') {
                if ($deleted > 0) {
                    $message = 'Se eliminaron ' . $deleted . ' Persona(s).';
                    $messageError = 'No se eliminaron ' . $noDeleted . ' Persona(s) porque participan como ' . 
                                    'Evaluado y/o Evaluador en alguna Encuesta del taller:</br>' . $messagePeople;
                }
                else {
                    $messageError = 'No se eliminaron las personas seleccionadas porque participan como ' .
                                    'Evaluado y/o Evaluador en alguna Encuesta del taller.';
                }
            }
            else {
                $message = 'Los registros fueron eliminados satisfactoriamente.';
            }
            
            if ($message) {
                $session->getFlashBag()->add('status', $message);
            }
            if ($messageError) {
                $session->getFlashBag()->add('error', $messageError);
            }

        }

        return $this->redirect($this->generateUrl('workshopapplication', array(
            'workshopschedulingid' => $workshopschedulingid
        )));
    }

    private function createDeleteMultipleForm($registers)
    {
        return $this->createFormBuilder(array('registers' => $registers))
            ->add('registers', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Import Workshopapplications entities.
     *
     */
    public function importAction(Request $request, $workshopschedulingid)
    {
        set_time_limit(120);
        ini_set('memory_limit','512M');
        
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la programación de taller.');
            return $this->redirect($this->generateUrl('workshopscheduling'));
        }
        
        $repository = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $wsaIncomplete = $repository->createQueryBuilder('wsa')
            ->where('wsa.datein IS NULL 
                    OR wsa.datein IS NULL')
            ->andWhere('wsa.workshopscheduling = :workshopschedulingid')
            ->setParameter('workshopschedulingid', $workshopschedulingid)
            ->orderBy('wsa.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        if ($wsaIncomplete) {
            $session->getFlashBag()->add('error', 'Existen actividades con datos faltantes (Fecha de Inicio / Fecha de Fin). Favor de completar estos datos.');
            return $this->redirect($this->generateUrl('workshopschedulingactivity', array(
                'workshopschedulingid' => $workshopschedulingid
            )));
        }

        $defaultData = array();
        $form = $this->createForm(new WorkshopschedulingpeopleimportType(), $defaultData);

        $form2 = false;
        $data = '';
        $wiData = '';
        $people = false;
        $valido = 1;
        $numberRegister = 0;

        if ($request->isMethod('POST')) {
            $importFormData = $request->request->get('form', false);

            if($importFormData){
                $temporalentity = $em->getRepository('FishmanImportExportBundle:Temporalimportdata')
                                     ->find($importFormData['temporalimportdata_id']);
                if(!$temporalentity){
                    return $this->render('FishmanWorkshopBundle:Workshopapplication:import.html.twig', array(
                         'workshopscheduling' => $workshopscheduling,
                         'form' => $form->createView(),
                         'form2' => $form2,
                         'people' => $people,
                    ));
                }

                $people = $temporalentity->getData();
                
                // suspend auto-commit
                $em->getConnection()->beginTransaction();
                
                // Try and make the transaction
                try {
                    
                    $userBy = $this->get('security.context')->getToken()->getUser();
                    
                    foreach ($people as $peoplearray){
                        
                        if(!$peoplearray['valid']) continue;
                            
                        $workinginformation = $em->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(
                            array( 'code' => $peoplearray['code'] , 'company' => $workshopscheduling->getCompany()->getId()
                        ));
                        
                        Workshopapplication::addRegister($this->getDoctrine(), $peoplearray['role'], $workinginformation, $workshopscheduling->getId(), $userBy);
                        
                        $numberRegister = $numberRegister + 1;
                        
                    } // end foreach
                    
                    //Delete temporal data
                    $em->remove($temporalentity);
                    $em->flush();
                    
                    // Delete temporal files
                    Workshopscheduling::deleteTemporalFiles('../tmp/');
                    
                    // Try and commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
                if ($numberRegister > 0) {
                    $session->getFlashBag()->add('status', 'Se grabó la importación de ' . $numberRegister . ' Persona(s)');
                }
                else {
                    $session->getFlashBag()->add('error', 'No se logró importar ninguna Persona.');
                }

                if($request->request->get('saveandclose', TRUE)){
                    return $this->redirect($this->generateUrl('workshopapplication', array('workshopschedulingid' => $workshopschedulingid)));
                }

                $people = false;

            } //End have temporarlimportdata input
            else{
                $form->bind($request);
                $data = $form->getData();

                // We need to move the file in order to use it
                $randomName = rand(1, 10000000000);
                $randomName = $randomName . '.xlsx';
                $directory = __DIR__.'/../../../../tmp';
                $data['file']->move($directory, $randomName);

                if (null !== $data['file']) {
                    $fileName = $data['file']->getClientOriginalName();
                    $data['file']->document = substr($fileName, strrpos($fileName, '.') + 1);
                }

                if ($data['file']->document == 'xlsx') { 
                    $inputFileName = $directory . '/' . $randomName;
                    $phpExcel = PHPExcel_IOFactory::load($inputFileName);

                    $worksheet = $phpExcel->getSheet(0);
                    $row = 2;
                    $people = array();
                    do {
                        $code = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                        $type = Workinginformation::getTypeCompanyCode(trim($worksheet->getCellByColumnAndRow(1, $row)->getValue()));
                        $role = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                        $message = '';
                        $people_name = '';
                        $people_surname = '';
                        $people_lastname = '';
                        $organizational_unit = '';
                        $valido = 1;
                        $init_date = '';
                        $charge = '';
                        $wi_id = '';
                        $company = '';

                        if ($code == '' && $type == '' && $role == '') break;
                        
                        if($type == ''){
                            $message .= '<p>Falta el tipo de la información</p>';
                            $valido = 0;
                        }
                        elseif (!in_array($type, array('workinginformation', 'schoolinformation', 'universityinformation'))) {
                            $message .= '<p>Tipo de información inválido, debe ser: <strong>working</strong>, <strong>school</strong>, <strong>university</strong></p>';
                            $valido = 0;
                        }
                        else {
                            if ($code == ''){
                                $message .= '<p>Falta el código del usuario</p>';
                                $valido = 0;
                            }
                            else {
                                $wiCode = $em->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(
                                    array( 'code' => $code, 'status' => '1')
                                );
                                
                                if (!empty($wiCode)) {
                                    
                                    $wiData = $em->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(
                                        array( 'code' => $code, 'type' => $type, 'status' => '1', 'company' => $workshopscheduling->getCompany()->getId())
                                    );
                                    
                                    if (!empty($wiData)) {
                                        $code_wi = $wiData->getCode();
                                        $company = $wiData->getCompany()->getId();
                                        $user_data = $wiData->getUser();
                                        $userStatus = $em->getRepository('FishmanAuthBundle:User')->findOneBy(
                                        array( 'enabled' => '1' , 'id' => $user_data->getId() ));
                                        
                                        $wi_id = $wiData->getId();
                                        
                                        if ($code_wi == $code  and $company == $workshopscheduling->getCompany()->getId() and !empty($userStatus)){
                                            $people_name = $wiData->getUser()->getNames();
                                            $people_surname = $wiData->getUser()->getSurname();
                                            $people_lastname = $wiData->getUser()->getLastname();
                                            
                                            if($people_surname == ''){
                                                $message .= '<p>Falta el apellido de la persona</p>';
                                                $valido = 0;
                                            }
                                            
                                            if($people_name == ''){
                                                $message .= '<p>Falta el nombre de la persona</p>';
                                                $valido = 0;
                                            }
                                            
                                            if (is_object($wiData->getCompanyorganizationalunit())) {
                                                $organizational_unit = $wiData->getCompanyorganizationalunit()->getName();
                                            }
                                            else {
                                                $organizational_unit = '';
                                            }
                                            
                                            if (is_object($wiData->getCompanycharge())) {
                                                $charge = $wiData->getCompanycharge()->getName();
                                            }
                                            else {
                                                $charge = '';
                                            }
                                            
                                        } elseif (empty($userStatus)) {
                                            $message .= '<p>La persona esta inactiva, no procedera la importación de este registro</p>';
                                            $valido = 0;
                                        }
                                        else {
                                            $message .= '<p>Persona inexistente o su tipo de información no es válido.</p>';
                                            $valido = 0;
                                        }
                                    } else {
                                        $message .= '<p>El código y tipo de información, no pertenece a la empresa de esta programación.</p>';
                                        $valido = 0;
                                    }
                                } else {
                                    $message .= '<p>No existe persona con el código ingresado</p>';
                                    $valido = 0;
                                }
                            }
                        }
                        
                        if($role == ''){
                            $message .= '<p>Falta el perfil de la persona</p>';
                            $valido = 0;
                        }
                        elseif (($role != 'boss') && ($role !='collaborator') && ($role != 'integrant') && ($role !='company')) {
                            $message .= '<p>El perfil ingresado es incorrectoo</p>';
                            $valido = 0;
                        }

                        if($valido){
                            $message = 'OK';
                        }

                        $people[] = array(
                            'code' => $code,
                            'type' => $type,
                            'role' => $role,
                            'surname' => $people_surname,
                            'lastname' => $people_lastname,
                            'name' => $people_name,
                            'organizationalunit' => $organizational_unit,
                            'charge' => $charge,
                            'company_id' => $company,
                            'wiid' => $wi_id,
                            'valid' => $valido,
                            'status' => 1,
                            'message' => $message
                        );
                        
                        $row++;
                        
                    } while (true);
                }else {
                    //Mensaje de que no hay registros
                    $session = $this->getRequest()->getSession();
                    $session->getFlashBag()->add('status', 'el archivo a importar debe ser de formato excel');
                }

                if(count($people) > 0 and is_array($people)){

                    $temporal = new Temporalimportdata();
                    $temporal->setData($people);
                    $temporal->setEntity('workshopapplication');
                    $temporal->setDatetime(new \DateTime());
                    $em->persist($temporal);
                    $em->flush();

                    // Secondary form
                    $defaultData2 = array('temporalimportdata_id' => $temporal->getId());
                    $form2 = $this->createFormBuilder($defaultData2)
                        ->add('temporalimportdata_id',
                              'integer', array (
                                       'required' => true 
                             ))
                        ->getForm();
                    $form2 = $form2->createView();
                }
                else{
                    $session->getFlashBag()->add('status', 'No hay personas para importar.');
                }
                //Borrar archivo temporal
                if ($data['file']->document == 'xlsx') {
                    unlink($inputFileName);
                }
            }
          } //End is method post

        return $this->render('FishmanWorkshopBundle:Workshopapplication:import.html.twig', array(
            'workshopscheduling' => $workshopscheduling,
            'form' => $form->createView(),
            'form2' => $form2,
            'people' => $people,
        ));
    }

    /**
     * Download document.
     * The user can access actual User documents
     */
    public function downloadtemplateAction()
    {
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="importar-programacion-taller-personas.xlsx'.'"'
        );
        return new Response(file_get_contents( __DIR__.'/../../../../templates/WorkshopBundle/Workshopapplication/importar-programacion-taller-personas.xlsx'), 200, $headers);
    }
}
