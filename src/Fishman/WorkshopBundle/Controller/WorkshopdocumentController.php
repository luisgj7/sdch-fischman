<?php

namespace Fishman\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\WorkshopBundle\Entity\Workshopdocument;
use Fishman\EntityBundle\Entity\Category;
use Fishman\WorkshopBundle\Form\WorkshopdocumentType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;

use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Workshopdocument controller.
 *
 */
class WorkshopdocumentController extends Controller
{
    /**
     * Lists all Workshopdocument entities.
     *
     */
    public function indexAction(Request $request, $workshopid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshop Info
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Recovering data
        
        $type_options = Workshopdocument::getListTypeOptions();
        $category_options = Category::getListCategoryOptions($this->getDoctrine());
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'type' => '', 
            'category' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('category', 'choice', array(
                'choices' => $category_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'type' => '', 
            'category' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopdocument');
        $queryBuilder = $repository->createQueryBuilder('wd')
            ->select('wd.id, wd.name, wd.type, c.name category, wd.sequence, wd.status, wd.changed, u.names, u.surname, u.lastname')
            ->innerJoin('wd.category', 'c')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'wd.modified_by = u.id')
            ->where('wd.workshop = :workshop')
            ->andWhere('wd.id LIKE :id 
                    OR wd.name LIKE :name')
            ->setParameter('workshop', $workshopid)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('wd.id', 'ASC');
        
        // Add arguments
        
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('wd.type = :type')
                ->setParameter('type', $data['type']);
        }
        if ($data['category'] != '') {
            $queryBuilder
                ->andWhere('wd.category = :category')
                ->setParameter('category', $data['category']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('wd.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('wd.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopdocument');
        $paginator->setMaxPagerItems(5, 'workshopdocument');
        $entities = $paginator->paginate($query, 'workshopdocument')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopdocument');
        $endPageItem = $paginator->getEndPageItem('workshopdocument');
        $totalItems = $paginator->getTotalItems('workshopdocument');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanWorkshopBundle:Workshopdocument:index.html.twig', array(
            'entities' => $entities,
            'workshop' => $workshop,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Workshopdocument entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanWorkshopBundle:Workshopdocument:show.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Workshopdocument entity.
     *
     */
    public function newAction($workshopid)
    {
        $entity = new Workshopdocument();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $entity->setType('d');
        $entity->setWorkshop($workshop);
        $entity->setStatus(1);
        $form   = $this->createForm(new WorkshopdocumentType(), $entity);

        return $this->render('FishmanWorkshopBundle:Workshopdocument:new.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new Workshopdocument entity.
     *
     */
    public function createAction(Request $request, $workshopid)
    {
        $entity  = new Workshopdocument();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $form = $this->createForm(new WorkshopdocumentType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setWorkshop($workshop);
            if ($entity->getType() == 'video') {
                $entity->setDocument('');
                $entity->setWebsite('');
            }
            elseif ($entity->getType() == 'document') {
                $entity->setVideo('');
                $entity->setWebsite('');
            }
            elseif ($entity->getType() == 'website') {
                $entity->setVideo('');
                $entity->setDocument('');
            }
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();

            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopdocument_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanWorkshopBundle:Workshopdocument:new.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Workshopdocument entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new WorkshopdocumentType(), $entity);

        return $this->render('FishmanWorkshopBundle:Workshopdocument:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Workshopdocument entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $editForm = $this->createForm(new WorkshopdocumentType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            if ($entity->getType() == 'video') {
                $entity->setDocument('');
                $entity->setWebsite('');
                $entity->setEmbed('');
            }
            elseif ($entity->getType() == 'document') {
                $entity->setVideo('');
                $entity->setWebsite('');
                $entity->setEmbed('');
            }
            elseif ($entity->getType() == 'website') {
                $entity->setVideo('');
                $entity->setDocument('');
                $entity->setEmbed('');
            }
            elseif ($entity->getType() == 'embed') {
                $entity->setVideo('');
                $entity->setDocument('');
                $entity->setWebsite('');
            }
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.'); 

            return $this->redirect($this->generateUrl('workshopdocument_show', array(
                'id' => $id
            )));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanWorkshopBundle:Workshopdocument:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Workshopdocument entity.
     *
     */
    public function deleteAction(Request $request, $id, $workshopid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanWorkshopBundle:Workshopdocument')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el documento del taller.');
                return $this->redirect($this->generateUrl('workshopdocument', array(
                    'workshopid' => $workshopid
                )));
            }

            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');

        }

        return $this->redirect($this->generateUrl('workshopdocument', array(
          'workshopid' => $workshopid
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Workshopdocument entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanWorkshopBundle:Workshopdocument:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Download document.
     * TODO: Access control. The user can access actual Workshpapplication documents
     */
    public function downloadAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshopdocument')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $file = new File($entity->getAbsolutePath());
        $headers = array(
            'Content-Type' => $file->getMimeType(),
            'Content-Disposition' => 'attachment; filename="'.$entity->getName().'.'.$file->getExtension().'"'
        );
        return new Response(file_get_contents($entity->getAbsolutePath()), 200, $headers);
    }
}
