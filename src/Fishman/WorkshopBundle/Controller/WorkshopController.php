<?php

namespace Fishman\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\WorkshopBundle\Entity\Workshop;
use Fishman\EntityBundle\Entity\Category;
use Fishman\WorkshopBundle\Entity\Workshopactivity;
use Fishman\NotificationBundle\Entity\Notification;
use Fishman\PollBundle\Entity\Poll;
use Fishman\PollBundle\Entity\Entitypoll;
use Fishman\WorkshopBundle\Form\WorkshopType;
use Fishman\PollBundle\Form\EntitypollType;
use Fishman\NotificationBundle\Form\NotificationType;
use Ideup\SimplePaginatorBundle\Paginator;

/**
 * Workshop controller.
 *
 */
class WorkshopController extends Controller
{
    /**
     * Lists all Workshop entities.
     *
     */
    public function indexAction(Request $request)
    {
        // Recovering data
        
        $category_options = Category::getListCategoryOptions($this->getDoctrine());
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',  
            'category' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('category', 'choice', array(
                'choices' => $category_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '',  
            'category' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
            
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshop');
        $queryBuilder = $repository->createQueryBuilder('w')
            ->select('w.id, w.name, c.name category, w.changed, w.status, u.names, u.surname, u.lastname')
            ->innerJoin('w.category', 'c')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'w.modified_by = u.id')
            ->where('w.id LIKE :id 
                    OR w.name LIKE :name')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('w.id', 'ASC');
        
        // Add arguments
        if ($data['category'] != '') {
            $queryBuilder
                ->andWhere('c.id = :category')
                ->setParameter('category', $data['category']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('w.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshop');
        $paginator->setMaxPagerItems(5, 'workshop');
        $entities = $paginator->paginate($query, 'workshop')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshop');
        $endPageItem = $paginator->getEndPageItem('workshop');
        $totalItems = $paginator->getTotalItems('workshop');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanWorkshopBundle:Workshop:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Workshop entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanWorkshopBundle:Workshop:show.html.twig', array(
            'entity'  => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Workshop entity.
     *
     */
    public function newAction()
    {
        $entity = new Workshop();
        $entity->setStatus(1);
        $form = $this->createForm(new WorkshopType(), $entity);

        return $this->render('FishmanWorkshopBundle:Workshop:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Workshop entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Workshop();
        $form = $this->createForm(new WorkshopType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
          
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();

            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshop_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanWorkshopBundle:Workshop:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Workshop entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new WorkshopType(), $entity);

        return $this->render('FishmanWorkshopBundle:Workshop:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Workshop entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $editForm = $this->createForm(new WorkshopType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
          
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            $entity->setModifiedBy($modifiedBy->getId());
            
            $entity->setChanged(new \DateTime());
            $em->persist($entity);
            $em->flush();

            // set flash messages
            $session = $this->getRequest()->getSession();                                                
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshop_show', array(
                'id' => $id
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        return $this->render('FishmanWorkshopBundle:Workshop:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Workshop entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($id);

            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
                return $this->redirect($this->generateUrl('workshop'));
            }

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling');
            $ws = $repository->createQueryBuilder('ws')
                ->select('count(ws.workshop)')
                ->where('ws.workshop = :workshop')
                ->setParameter('workshop', $entity->getId())
                ->groupBy('ws.workshop')
                ->getQuery()
                ->getResult();

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopdocument');
            $wd = $repository->createQueryBuilder('wd')
                ->select('count(wd.workshop)')
                ->where('wd.workshop = :workshop')
                ->setParameter('workshop', $entity->getId())
                ->groupBy('wd.workshop')
                ->getQuery()
                ->getResult();

            // Check if you have legacy
            $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopactivity');
            $wa = $repository->createQueryBuilder('wa')
                ->select('count(wa.workshop)')
                ->where('wa.workshop = :workshop')
                ->setParameter('workshop', $entity->getId())
                ->groupBy('wa.workshop')
                ->getQuery()
                ->getResult();
            
            // set flash messages
            $session = $this->getRequest()->getSession();
            
            if (current($ws) == 0 || current($wd) == 0 || current($wa) == 0) {
                $em->remove($entity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
            }
            else {
                $session->getFlashBag()->add('error', 'No se puede borrar el Taller, existen datos asociados.');
            }

        }

        return $this->redirect($this->generateUrl('workshop'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Drop an Workshop entity.
     *
     */
    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanWorkshopBundle:Workshop:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Return workshop select list with some category id
     * TODO: Change this as ajaxChoiceByCategory
     */
    public function choiceByCategoryAction()
    {
        $category_id = $_GET['category_id'];
        $repository = $this->getDoctrine()->getManager()->getRepository('FishmanWorkshopBundle:Workshop');

        $query = $repository->createQueryBuilder('w')
             ->where('w.category = :category_id 
                    AND w.status = 1')
             ->setParameter('category_id', $category_id)
             ->orderBy('w.id', 'ASC')
             ->getQuery();

        $workshops = $query->getResult();

        $first = true;
        $name = '';
        $description = '';
        //$period = '';

        $combo = '<select class="workshops" name="workshop_workshops">';
        $combo .= '<option value="">Seleccione el Taller</option>';
        foreach($workshops as $workshop){
            if($first){
                $description = $workshop->getDescription();
                $name = $workshop->getName();
                $first = false;
            }
            $combo .= '<option value="' . $workshop->getId() . '">' . $workshop->getName() . '</option>';
        }
        $combo .= '</select>';

        //$this->render('FishmanPollBundle:Entitypoll:choicebycategory.html.twig', array('polls' => $polls));

        $response = new Response(json_encode(
            array('combo' => $combo, 'description' => $description , 'name' => $name)));  
        return $response;
    }

    //TODO: Change name by more descriptive
    function jsonAction()
    {
        $workshop_id = $_GET['workshop_id'];   
        $workshop = $this->getDoctrine()->getManager()->getRepository('FishmanWorkshopBundle:Workshop')->find($workshop_id);

        $name = '';
        $description = '';
          
        if($workshop){
            $description = $workshop->getDescription();
            $name = $workshop->getName();
        }
          
        $response = new Response(json_encode(array('description' => $description, 'name' => $name))); 
        return $response;
    }
    
    /**
     * Lists all Entitypoll entities.
     *
     */
    public function indexPollAction(Request $request, $workshopid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshop Info
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Recovering data
        
        $category_options = Category::getListCategoryOptions($this->getDoctrine());
        $period_options = array('day' => 'Días', 'week' => 'Semanas', 'month' => 'Meses');
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',  
            'category' => '', 
            'duration' => '', 
            'period' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('category', 'choice', array(
                'choices' => $category_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('duration', 'text', array(
                'required' => FALSE
            ))
            ->add('period', 'choice', array(
                'choices' => $period_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'category' => '', 
            'duration' => '', 
            'period' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
            
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Entitypoll');
        $queryBuilder = $repository->createQueryBuilder('ep')
            ->select('ep.id, p.id poll_id, p.title name, c.name category, ep.duration, ep.period, ep.sequence, ep.status, 
                      ep.changed, u.names, u.surname, u.lastname')
            ->innerJoin('ep.poll', 'p')
            ->innerJoin('p.category', 'c')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'ep.modified_by = u.id')
            ->where('ep.entity_id = :workshop 
                    AND ep.entity_type = :entity_type')
            ->andWhere('ep.id LIKE :id 
                    OR p.title LIKE :name')
            ->setParameter('workshop', $workshopid)
            ->setParameter('entity_type', 'workshop')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('ep.id', 'ASC');
        
        // Add arguments
        
        if ($data['category'] != '') {
            $queryBuilder
                ->andWhere('p.category = :category')
                ->setParameter('category', $data['category']);
        }
        if ($data['duration'] != '') {
            $queryBuilder
                ->andWhere('ep.duration LIKE :duration')
                ->setParameter('duration', '%' . $data['duration'] . '%');
        }
        if ($data['period'] != '') {
            $queryBuilder
                ->andWhere('ep.period = :period')
                ->setParameter('period', $data['period']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('ep.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('ep.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshoppoll');
        $paginator->setMaxPagerItems(5, 'workshoppoll');
        $entities = $paginator->paginate($query, 'workshoppoll')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshoppoll');
        $endPageItem = $paginator->getEndPageItem('workshoppoll');
        $totalItems = $paginator->getTotalItems('workshoppoll');
        
        $array_poll = '';
        $i = 0;
        foreach ($entities as $entity) {
            $pid = $entity['poll_id'];
            
            if (!isset($array_poll[$pid])) {
                $array_poll[$pid] = 1;
            }
            else {
                $array_poll[$pid] = $array_poll[$pid] + 1;
            }
            $entities[$i]['name'] = $entity['name'] . ' (' . $array_poll[$pid] . ')';
            $i++;
        }
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanWorkshopBundle:Workshop/Poll:index.html.twig', array(
            'entities' => $entities,
            'workshop' => $workshop,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Entityppoll entity.
     *
     */
    public function showPollAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($entity->getEntityId());
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanWorkshopBundle:Workshop/Poll:show.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop, 
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Entitypoll entity.
     *
     */
    public function newPollAction($workshopid)
    {
        $entity = new EntityPoll();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();

        // Recover Polls
        $polls = $em->getRepository('FishmanPollBundle:Poll')->findAll();
             
        if (!empty($polls)) {
          
            $entity->setStatus(1);
            
            $form = $this->createForm(new EntitypollType(), $entity);
            
            return $this->render('FishmanWorkshopBundle:Workshop/Poll:new.html.twig', array(
                'entity' => $entity,
                'workshop' => $workshop,
                'categories' => $categories,
                'form'   => $form->createView(),
            ));
        }
        else {
            $session->getFlashBag()->add('error', 'Debe crear una encuesta para poder ser asignada al taller.');

            return $this->redirect($this->generateUrl('workshop', array()));
        }
    }

    public function createPollAction(Request $request, $workshopid)
    {
        $entity  = new EntityPoll();

        $form = $this->createForm(new EntitypollType(), $entity);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            // Poll
            $postData = $request->request->get('fishman_pollbundle_entitypolltype', null);
            $poll = $em->getRepository('FishmanPollBundle:Poll')->find($postData['poll_id']);
            if (!$poll) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
                return $this->redirect($this->generateUrl('workshop'));
            }
            
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setEntityType('workshop');
            $entity->setEntityId($workshopid);
            $entity->setPoll($poll);
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshoppoll_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanWorkshopBundle:Entitypoll:new.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Entitypoll entity.
     *
     */
    public function editPollAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($entity->getEntityId());
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Recover Categories
        $repository = $this->getDoctrine()->getRepository('FishmanEntityBundle:Category');
        $categories = $repository->createQueryBuilder('c')
             ->where('c.status = 1')
             ->orderBy('c.name', 'ASC')
             ->getQuery()
             ->getResult();
        $current_category = $entity->getPoll()->getCategory();

        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Poll');
        $polls = $repository->createQueryBuilder('p')
             ->where('p.category = :category_id AND p.current_version = :current_version')
             ->setParameter('category_id', $current_category->getId())
             ->setParameter('current_version', 1)
             ->orderBy('p.id', 'ASC')
             ->getQuery()
             ->getResult();
        $current_poll = $entity->getPoll();
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $editForm = $this->createForm(new EntitypollType(), $entity);

        return $this->render('FishmanWorkshopBundle:Workshop/Poll:edit.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop, 
            'categories' => $categories,
            'polls' => $polls,
            'current_poll' => $current_poll,
            'current_category' => $current_category,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    public function updatePollAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $editForm = $this->createForm(new EntitypollType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();
            
            // Poll
            $postData = $request->request->get('fishman_pollbundle_entitypolltype', 'default value if bar does not exist');
            $poll = $em->getRepository('FishmanPollBundle:Poll')->find($postData['poll_id']);
            if (!$poll) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
                return $this->redirect($this->generateUrl('workshop'));
            }
            
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            $entity->setPoll($poll);

            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshoppoll_show', array(
                'id' => $id
            )));
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        return $this->render('FishmanWorkshopBundle:Workshop:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Entityppoll entity.
     *
     */
    public function deletePollAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);
            $workshopid = $entity->getEntityId();
            
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
                return $this->redirect($this->generateUrl('workshop'));
            }

            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.'); 
        }

        return $this->redirect($this->generateUrl('workshoppoll', array(
            'workshopid' => $workshopid
        )));
    }

    /**
     * Drop an Entitypoll entity.
     *
     */
    public function dropPollAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($entity->getEntityId());
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanWorkshopBundle:Workshop/Poll:drop.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop, 
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Preview Poll entity.
     *
     */
    public function previewPollAction(Request $request, $id, $page)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $array_questions = '';
        
        $entity = $em->getRepository('FishmanPollBundle:Entitypoll')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($entity->getEntityId());
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $poll = $entity->getPoll();
        
        if (!$poll->getSerialized() || $poll->getModifiedOn()) {
            Poll::serializedPoll($this->getDoctrine(), $poll);
        }
        
        // Paginator
        
        $array_paginator = Poll::getPaginator($this->getDoctrine(), $poll, $page);
        
        if ($array_paginator['number_pagers'] > 0 && $page > $array_paginator['number_pagers']) {
            return $this->redirect($this->generateUrl('workshoppoll_preview', array(
                'id' => $id,
                'page' => $page
            )));
        }

        // Recover Questions in page current
        
        $array['sections'] = $poll->getSerialized();
        
        if ($poll->getDivide() == 'sections_show') {
            $array_questions = Poll::getArraySectionShow($this->getDoctrine(), $array, $poll->getId(), $page);
        }
        else if ($poll->getDivide() == 'number_questions') {
            $array_questions = Poll::getArrayNumberQuestions($this->getDoctrine(), $array['sections'], $poll->getId(), $page, $poll->getNumberQuestion());
        }
        else if ($poll->getDivide() == 'none') {
            $array_questions = $array;
        }
        
        if ($request->request->get('first', false)) {
            $page = 1;
        }
        if ($request->request->get('prev', false)) {
            $page--;
        }
        if ($request->request->get('page')) {
            $page = $request->request->get('page');
        }
        if ($request->request->get('next', false)) {
            $page++;
        }
        if ($request->request->get('last', false)) {
            $page = $array_paginator['number_pagers'];
        }
        
        if ($request->request->get('page') || $request->request->get('first', false) || $request->request->get('prev', false) || $request->request->get('next', false) || $request->request->get('last', false)) {
            $array_parameters['page'] = $page;
            return $this->redirect($this->generateUrl('workshoppoll_preview', array(
                'id' => $id,
                'page' => $page
            )));
        }
        
        // Array Parameters
        
        $array_parameters = array(
            'entity' => $entity,
            'workshop' => $workshop,
            'page' => $page,
            'paginator' => $array_paginator['paginator'],
            'lastpage' => $array_paginator['number_pagers'],
            'questions' => $array_questions, 
            'alignment' => 'vertical'
        );
        
        if ($poll->getAlignment() == 'horizontal') {
            $array_parameters['alignment'] = 'horizontal';
        }
        
        return $this->render('FishmanWorkshopBundle:Workshop/Poll/Preview:resolved.html.twig', $array_parameters);
    }
    
    /**
     * Lists all Notification entities.
     *
     */
    public function indexNotificationAction(Request $request, $workshopid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        // Workshop Info
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        // Recovering data
        
        $type_options = Notification::getListTypeOptions();
        $activity_options = Workshopactivity::getListWorkshopactivityOptions($this->getDoctrine(), $workshopid);
        $predecessor_options = Notification::getListNotificationOptions($this->getDoctrine(), 'workshop', $workshopid);
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        $status_options = array(0 => 'Desactivo', 1 => 'Activo');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'type' => '', 
            'activity' => '', 
            'predecessor' => '', 
            'sequence' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('activity', 'choice', array(
                'choices' => $activity_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('predecessor', 'choice', array(
                'choices' => $predecessor_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'type' => '', 
            'activity' => '', 
            'predecessor' => '', 
            'sequence' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Query
            
        $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notification');
        $queryBuilder = $repository->createQueryBuilder('n')
            ->select('n.id, n.name, n.notification_type type, n.notification_type_name type_name, 
                      np.name predecessor, n.sequence, n.changed, n.status, u.names, u.surname, u.lastname')
            ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'n.modified_by = u.id')
            ->leftJoin('FishmanNotificationBundle:Notification', 'np', 'WITH', 'n.predecessor = np.id')
            ->where('n.entity_id = :workshop 
                    AND n.entity_type = :entity_type')
            ->andWhere('n.id LIKE :id 
                    OR n.name LIKE :name')
            ->setParameter('workshop', $workshopid)
            ->setParameter('entity_type', 'workshop')
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('n.id', 'ASC');
        
        // Add arguments
        
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('n.notification_type = :type')
                ->setParameter('type', $data['type']);
        }
        if ($data['activity'] != '') {
            $queryBuilder
                ->andWhere('n.notification_type_id = :activity')
                ->setParameter('activity', $data['activity']);
        }
        if ($data['predecessor'] != '') {
            $queryBuilder
                ->andWhere('n.predecessor = :predecessor')
                ->setParameter('predecessor', $data['predecessor']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('n.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['status'] != '' || $data['status'] === 0) {
            $queryBuilder
                ->andWhere('n.status = :status')
                ->setParameter('status', $data['status']);
        }
        
        $query = $queryBuilder->getQuery();
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopnotification');
        $paginator->setMaxPagerItems(5, 'workshopnotification');
        $entities = $paginator->paginate($query, 'workshopnotification')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopnotification');
        $endPageItem = $paginator->getEndPageItem('workshopnotification');
        $totalItems = $paginator->getTotalItems('workshopnotification');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanWorkshopBundle:Workshop/Notification:index.html.twig', array(
            'entities' => $entities,
            'workshop' => $workshop,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Finds and displays a Notification entity.
     *
     */
    public function showNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Recover Notification
        $entity = Notification::getNotification($this->getDoctrine(), $id);
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($entity['entity_id']);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $notification = $em->getRepository('FishmanNotificationBundle:Notification')->find($entity['id']);
        $entityTypeName = '';

        if ($entity['type'] == 'poll') {
          $entityType = $em->getRepository('FishmanPollBundle:Poll')->find($notification->getNotificationTypeId());
            $entityTypeName = $entityType->getTitle();
        } elseif ($entity['type'] == 'activity') {
          $entityType = $em->getRepository('FishmanWorkshopBundle:Workshopactivity')->find($notification->getNotificationTypeId());
          $entityTypeName = $entityType->getName();
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['created_by']);
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity['modified_by']);
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname();
        
        return $this->render('FishmanWorkshopBundle:Workshop/Notification:show.html.twig', array(
            'entity' => $entity,
            'entityTypeName' => $entityTypeName,
            'workshop' => $workshop, 
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName
        ));
    }

    /**
     * Displays a form to create a new Notification entity.
     *
     */
    public function newNotificationAction($workshopid)
    {
        $entity = new Notification();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $entity->setNotificationTypeStatus(0);
        $entity->setFinish('never');
        $entity->setSelectedTrigger('to_send');
        $entity->setStatus(1);
        $form = $this->createForm(new NotificationType($entity, 'workshop', $workshopid, $this->getDoctrine()), $entity);

        return $this->render('FishmanWorkshopBundle:Workshop/Notification:new.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Return the list of a notification type 
     */
    public function choiceByTypeNotificationAction($workshopid)
    {
        //variables defined
        $first = true;
        $typeId = '';

        //Entity manager
        $em = $this->getDoctrine()->getManager();

        //Recovery workshop
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        $typeNotification = $_GET['type'];

        //Filter for type Notification
        if ($typeNotification == 'activity' || $typeNotification == 'message') {
          if ($typeNotification == 'activity') {
            $repository = $em->getRepository('FishmanWorkshopBundle:Workshopactivity');
            $combo = Workshopactivity::getActivityListOfWorkshop($repository, $workshop, $first, $typeId);
          } else {
            $combo = '';
          }
          $multiSelect = '<option value="boss">Jefe</option>';
          $multiSelect .= '<option value="collaborator">Colaborador</option>';
          $multiSelect .= '<option value="integrant">Participante</option>';
          $multiSelect .= '<option value="company">Empresa</option>';
          $multiSelect .= '<option value="responsible">Responsable</option>';
        } elseif ($typeNotification == 'poll') {
          $repository = $em->getRepository('FishmanPollBundle:Entitypoll');
          $combo = Entitypoll::getPollListOfWorkshop($repository, $workshop, $first, $typeId);
          $multiSelect = '<option value="evaluated">Evaluado</option>';
          $multiSelect .= '<option value="evaluator">Evaluador</option>';
          /*$multiSelect .= '<option value="boss">Jefe</option>';
          $multiSelect .= '<option value="collaborator">Colaborador</option>';
          $multiSelect .= '<option value="company">Empresa</option>';
          $multiSelect .= '<option value="responsible">Responsable</option>';*/
        } 

        $response = new Response(json_encode(
            array('combo' => $combo , 'multi_select' => $multiSelect)));  
        return $response;
    }
    
    /**
     * Creates a new Notification entity.
     *
     */
    public function createNotificationAction(Request $request, $workshopid)
    {
        $entity  = new Notification();
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($workshopid);
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $asigneds = $request->request->get('asigned_options');
        
        $form = $this->createForm(new NotificationType($entity, 'workshop', $workshopid, $this->getDoctrine()), $entity);
        $form->bind($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            // User
            $userBy = $this->get('security.context')->getToken()->getUser();
            
            if ($entity->getNotificationType() == 'activity') {
                $activity = $em->getRepository('FishmanWorkshopBundle:Workshopactivity')->find($entity->getNotificationTypeId());
                $entity->setNotificationTypeName($activity->getName());
                $entity->setNotificationTypeId($activity->getId());
            }
            elseif ($entity->getNotificationType() == 'poll') {
                $poll = $em->getRepository('FishmanPollBundle:Entitypoll')->findOneBy(array('id' => $entity->getNotificationTypeId()));
                $entity->setNotificationTypeName($poll->getPoll()->getTitle());
                $entity->setNotificationTypeId($poll->getPoll()->getId());
                $entity->setEntityPollId($poll->getId());
            }
            elseif ($entity->getNotificationType() == 'message') {
                $entity->setNotificationTypeId('');
                $entity->setNotificationTypeName('');
                $entity->setNotificationTypeStatus(FALSE);
            }
            if ($entity->getEjecutionType() == 'manual' || $entity->getEjecutionType() == 'trigger') {
                $entity->setPredecessor(-1);
            }
            if ($entity->getEjecutionType() == 'trigger') {
                $entity->setNotificationTypeStatus(TRUE);
            }
            else {
                $entity->setSelectedTrigger('');
            }
            if ($entity->getEjecutionType() != 'automatic') {
                $entity->setSince('');
                $entity->setReplay(false);
            }
            if ($entity->getReplay() == 0) {
                $entity->setRepeatPeriod('');
                $entity->setRepeatRange('');
                $entity->setFinish('');
            }
            if ($entity->getFinish() != 'after') {
                $entity->setRepetitionNumber('');
            }
            if ($entity->getPredecessor() == '') {
                $entity->setPredecessor(-1);
            }
            $entity->setAsigned($asigneds['data2']);
            $entity->setEntityType('workshop');
            $entity->setEntityId($workshop->getId());
            $entity->setEntityName($workshop->getName());
            $entity->setEntityStatus($workshop->getStatus());
            $entity->setCreated(new \DateTime());
            $entity->setChanged(new \DateTime());
            $entity->setCreatedBy($userBy->getId());
            $entity->setModifiedBy($userBy->getId());
            
            $em->persist($entity);
            $em->flush();

            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro ha sido creado satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopnotification_show', array(
                'id' => $entity->getId()
            )));
        }

        return $this->render('FishmanWorkshopBundle:Workshop/Notification:new.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Notification entity.
     *
     */
    public function editNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($entity->getEntityId());
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        //variables defined
        $first = true;
        $combo = '';
        
        $typeNotification = $entity->getNotificationType();
        $typeId = $entity->getNotificationTypeId();

        //Filter for type Notification
        if ($typeNotification == 'activity') {
          $repository = $em->getRepository('FishmanWorkshopBundle:Workshopactivity');
          $combo = Workshopactivity::getActivityListOfWorkshop($repository, $workshop, $first, $typeId);
        } elseif ($typeNotification == 'poll') {
          $repository = $em->getRepository('FishmanPollBundle:Entitypoll');
          $combo = Entitypoll::getPollListOfWorkshop($repository, $workshop, $first, $typeId);
        }

        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 
        
        $entity->setFinish('never');
        $entity->setSelectedTrigger('to_send');
        $editForm = $this->createForm(new NotificationType($entity, 'workshop', $entity->getEntityId(), $this->getDoctrine()), $entity);
        
        return $this->render('FishmanWorkshopBundle:Workshop/Notification:edit.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop, 
            'createdByName' => $createdByName,
            'combo' => $combo,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Edits an existing Notification entity.
     *
     */
    public function updateNotificationAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }

        $asigneds = $request->request->get('asigned_options');
        $activityRequest = $request->request->get('fishman_notificationbundle_notificationtype');
        $notificationType = $entity->getNotificationType();

        $editForm = $this->createForm(new NotificationType($entity, 'workshop', $entity->getEntityId(), $this->getDoctrine()), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            // User
            $modifiedBy = $this->get('security.context')->getToken()->getUser();

            $entity->setNotificationType($notificationType);
            if ($notificationType == 'activity') {
                $activity = $em->getRepository('FishmanWorkshopBundle:Workshopactivity')->find($activityRequest['notification_type_id']);
                $entity->setNotificationTypeName($activity->getName());
                $entity->setNotificationTypeId($activity->getId());
            }
            elseif ($notificationType == 'poll') {
                $poll = $em->getRepository('FishmanPollBundle:Entitypoll')->findOneBy(array('poll' => $entity->getNotificationTypeId()));
                $entity->setNotificationTypeName($poll->getPoll()->getTitle());
                $entity->setNotificationTypeId($poll->getPoll()->getId());
            }
            elseif ($notificationType == 'message') {
                $entity->setNotificationTypeId($entity->getNotificationTypeId());
                $entity->setNotificationTypeName('');
                $entity->setNotificationTypeStatus(FALSE);
            }
            if ($entity->getEjecutionType() == 'manual' || $entity->getEjecutionType() == 'trigger') {
                $entity->setPredecessor(-1);
            }
            if ($entity->getEjecutionType() == 'trigger') {
                $entity->setNotificationTypeStatus(TRUE);
            }
            else {
                $entity->setSelectedTrigger('');
            }
            if ($entity->getEjecutionType() != 'automatic') {
                $entity->setSince('');
                $entity->setReplay(false);
            }
            if ($entity->getReplay() == 0) {
                $entity->setRepeatPeriod('');
                $entity->setRepeatRange('');
                $entity->setFinish('');
            }
            if ($entity->getFinish() != 'after') {
                $entity->setRepetitionNumber('');
            }
            $entity->setAsigned($asigneds['data2']);
            $entity->setModifiedBy($modifiedBy->getId());
            $entity->setChanged(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'Los cambios fueron actualizados satisfactoriamente.');

            return $this->redirect($this->generateUrl('workshopnotification_show', array(
                'id' => $id
            )));
        }
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($entity->getEntityId());
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanWorkshopBundle:Workshop/Notification:edit.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Drop an Notification entity.
     *
     */
    public function dropNotificationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación del taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        $workshop = $em->getRepository('FishmanWorkshopBundle:Workshop')->find($entity->getEntityId());
        if (!$workshop) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('workshop'));
        }
        
        // Check if you have legacy
        $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $wns = $repository->createQueryBuilder('ns')
            ->select('count(ns.notification)')
            ->where('ns.notification = :notification')
            ->setParameter('notification', $entity->getId())
            ->groupBy('ns.notification')
            ->getQuery()
            ->getResult();
        
        if (current($wns) > 0) {
            $session->getFlashBag()->add('error', 'La Notificación está en uso.');
            return $this->redirect($this->generateUrl('workshopnotification', array(
                'workshopid' => $entity->getEntityId()
            )));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanWorkshopBundle:Workshop/Notification:drop.html.twig', array(
            'entity' => $entity,
            'workshop' => $workshop, 
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a Notification entity.
     *
     */
    public function deleteNotificationAction(Request $request, $id, $workshopid)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        // set flash messages
        $session = $this->getRequest()->getSession();

        if ($form->isValid()) {
          
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('FishmanNotificationBundle:Notification')->find($id);
            if (!$entity) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la notificación del taller.');
                return $this->redirect($this->generateUrl('workshopnotification', array(
                    'workshopid' => $workshopid
                )));
            }

            // Update children predecessor
            $em->createQueryBuilder()
                ->update('FishmanNotificationBundle:Notification n')
                ->set('n.predecessor', ':predecessor')
                ->where('n.predecessor = :notification')
                ->setParameter('predecessor', '-1')
                ->setParameter('notification', $id)
                ->getQuery()
                ->execute();
            
            $em->remove($entity);
            $em->flush();
            
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('workshopnotification', array(
            'workshopid' => $workshopid
        )));
    }
}
