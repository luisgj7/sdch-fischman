<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection; 
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\WorkshopBundle\Entity\Workshopscheduling
 */
class Workshopscheduling
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var smallint $time_number
     */
    private $time_number;

    /**
     * @var \DateTime $initdate
     */
    private $initdate;

    /**
     * @var \DateTime $enddate
     */
    private $enddate;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @var text $custom_header_title
     */
    private $custom_header_title;

    /**
     * @var string $custom_header_title_text
     */
    private $custom_header_title_text;

    /**
     * @var string $custom_header_title_description
     */
    private $custom_header_title_description;

    /**
     * @var text $custom_header_logo
     */
    private $custom_header_logo;

    /**
     * @var boolean $deleted
     */
    private $deleted;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshop", inversedBy="workshopschedulings")
     * @ORM\JoinColumn(name="workshop_id", referencedColumnName="id")
     */
    protected $workshop;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Company", inversedBy="workshopschedulings")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopapplication", mappedBy="workshopscheduling")
     */
    protected $workshopapplications; 

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopschedulingdocument", mappedBy="workshopscheduling")
     */
    protected $workshopschedulingdocuments;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopschedulingactivity", mappedBy="workshopscheduling")
     */
    protected $workshopschedulingactivitys;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Workshopscheduling
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return Workshopscheduling
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set time_number
     *
     * @param smallint $timenumber
     * @return Workshopscheduling
     */
    public function setTimeNumber($timeNumber)
    {
        $this->time_number = $timeNumber;
    
        return $this;
    }

    /**
     * Get time_number
     *
     * @return smallint 
     */
    public function getTimeNumber()
    {
        return $this->time_number;
    }

    /**
     * Set initdate
     *
     * @param \DateTime $initdate
     * @return Workshopscheduling
     */
    public function setInitdate($initdate)
    {
        $this->initdate = $initdate;
    
        return $this;
    }

    /**
     * Get initdate
     *
     * @return \DateTime 
     */
    public function getInitdate()
    {
        return $this->initdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return Workshopscheduling
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Workshopscheduling
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workshopscheduling
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workshopscheduling
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Workshopscheduling
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Workshopscheduling
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set custom_header_title 
     *
     * @param string $custom_header_title
     * @return Workshopscheduling 
     */
    public function setCustomHeaderTitle($customHeaderTitle)
    {
        $this->custom_header_title = $customHeaderTitle;
    
        return $this;
    }

    /**
     * Get custom_header_title
     *
     * @return string 
     */
    public function getCustomHeaderTitle()
    {
        return $this->custom_header_title;
    }

    /**
     * Set custom_header_title_text
     *
     * @param string $customHeaderTitleText
     * @return Workshopscheduling
     */
    public function setCustomHeaderTitleText($customHeaderTitleText)
    {
        $this->custom_header_title_text = $customHeaderTitleText;
    
        return $this;
    }

    /**
     * Get custom_header_title_text
     *
     * @return string 
     */
    public function getCustomHeaderTitleText()
    {
        return $this->custom_header_title_text;
    }

    /**
     * Set custom_header_title_description
     *
     * @param string $customHeaderTitleDescription
     * @return Workshopscheduling
     */
    public function setCustomHeaderTitleDescription($customHeaderTitleDescription)
    {
        $this->custom_header_title_description = $customHeaderTitleDescription;
    
        return $this;
    }

    /**
     * Get custom_header_title_description
     *
     * @return string 
     */
    public function getCustomHeaderTitleDescription()
    {
        return $this->custom_header_title_description;
    }

    /**
     * Set custom_header_logo 
     *
     * @param string $custom_header_logo
     * @return Workshopscheduling
     */
    public function setCustomHeaderLogo($customHeaderLogo)
    {
        $this->custom_header_logo = $customHeaderLogo;
    
        return $this;
    }

    /**
     * Get custom_header_logo 
     *
     * @return string 
     */
    public function getCustomHeaderLogo()
    {
        return $this->custom_header_logo;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Workshopscheduling
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set workshop
     *
     * @param Fishman\WorkshopBundle\Entity\Workshop $workshop
     * @return Workshopscheduling
     */
    public function setWorkshop(\Fishman\WorkshopBundle\Entity\Workshop $workshop = null)
    {
        $this->workshop = $workshop;
    
        return $this;
    }

    /**
     * Get workshop
     *
     * @return Fishman\WorkshopBundle\Entity\Workshop 
     */
    public function getWorkshop()
    {
        return $this->workshop;
    }

    /**
     * Set company
     *
     * @param Fishman\EntityBundle\Entity\Company $company
     * @return Workshopscheduling
     */
    public function setCompany(\Fishman\EntityBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return Fishman\EntityBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    public function __toString()
    {
         return $this->description;
    } 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workshopapplications = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add workshopapplications
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplications
     * @return Workshopscheduling
     */
    public function addWorkshopapplication(\Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplications)
    {
        $this->workshopapplications[] = $workshopapplications;
    
        return $this;
    }

    /**
     * Remove workshopapplications
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplications
     */
    public function removeWorkshopapplication(\Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplications)
    {
        $this->workshopapplications->removeElement($workshopapplications);
    }

    /**
     * Get workshopapplications
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopapplications()
    {
        return $this->workshopapplications;
    }

    /**
     * Add workshopschedulingdocuments
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingdocument $workshopschedulingdocuments
     * @return Workshopscheduling
     */
    public function addWorkshopschedulingdocument(\Fishman\WorkshopBundle\Entity\Workshopschedulingdocument $workshopschedulingdocuments)
    {
        $this->workshopschedulingdocuments[] = $workshopschedulingdocuments;
    
        return $this;
    }

    /**
     * Remove workshopschedulingdocuments
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingdocument $workshopschedulingdocuments
     */
    public function removeWorkshopschedulingdocument(\Fishman\WorkshopBundle\Entity\Workshopschedulingdocument $workshopschedulingdocuments)
    {
        $this->workshopschedulingdocuments->removeElement($workshopschedulingdocuments);
    }

    /**
     * Get workshopschedulingdocuments
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopschedulingdocuments()
    {
        return $this->workshopschedulingdocuments;
    }

    /**
     * Add workshopschedulingactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys
     * @return Workshopscheduling
     */
    public function addWorkshopschedulingactivity(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys)
    {
        $this->workshopschedulingactivitys[] = $workshopschedulingactivitys;
    
        return $this;
    }

    /**
     * Remove workshopschedulingactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys
     */
    public function removeWorkshopschedulingactivity(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys)
    {
        $this->workshopschedulingactivitys->removeElement($workshopschedulingactivitys);
    }

    /**
     * Get workshopschedulingactivitys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopschedulingactivitys()
    {
        return $this->workshopschedulingactivitys;
    }

    /**
     * Get current workshopapplication 
     */
    public static function getCurrentWorshopscheduling(DoctrineRegistry $doctrineRegistry, $wsId)
    { 
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopscheduling');
        $query = $repository->createQueryBuilder('ws')
            ->select('ws.id, ws.name, ws.custom_header_logo, ws.custom_header_title, 
                      ws.custom_header_title_text, ws.custom_header_title_description, 
                      c.id company_id, c.name company_name, c.image company_image')
            ->innerJoin('ws.company', 'c')
            ->where('ws.id = :workshopscheduling
                    AND ws.status = 1 
                    AND ws.deleted = :deleted')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('deleted', FALSE)
            ->orderBy('ws.initdate', 'DESC')
            ->getQuery();
        $output = $query->getResult();
        
        return $output;
    }

    /**
     * Get Last Five Workshopschedulings
     */
    public static function getLastFiveWorkshopschedulings(DoctrineRegistry $doctrineRegistry)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopscheduling');
        $query = $repository->createQueryBuilder('ws')
            ->select('ws.id, ws.name, ws.time_number, ws.initdate, ws.enddate')
            ->where('ws.status = 1 
                    AND ws.deleted = :deleted')
            ->setParameter('deleted', FALSE)
            ->orderBy('ws.initdate', 'DESC')
            ->setMaxResults(5)
            ->getQuery();
        $output = $query->getResult();
        
        return $output;
    }

    /**
     * Get List Workshopsschedulings 
     */
    public static function getListWorkshopschedulings(DoctrineRegistry $doctrineRegistry, $data)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopscheduling');
        $queryBuilder = $repository->createQueryBuilder('ws')
            ->select('ws.id, ws.name, ws.time_number, c.name company, ws.initdate, ws.enddate')
            ->innerJoin('ws.company', 'c')
            ->where('ws.status = 1 
                    AND ws.deleted = :deleted')
            ->andWhere('ws.id LIKE :id 
                    OR ws.name LIKE :name')
            ->setParameter('deleted', FALSE)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('ws.initdate', 'DESC');
        
        // Add arguments
        
        if ($data['resolved'] != '' || $data['resolved'] === 0) {
            $queryBuilder
                ->andWhere('ws.time_number LIKE :resolved')
                ->setParameter('resolved', '%' . $data['resolved'] . '%');
        }
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('c.name LIKE :company')
                ->setParameter('company', '%' . $data['company'] . '%');
        }
        if (!empty($data['initdate'])) {
            $queryBuilder
                ->andWhere('ws.initdate = :initdate')
                ->setParameter('initdate', $data['initdate']);
        }
        if (!empty($data['enddate'])) {
            $queryBuilder
                ->andWhere('ws.enddate = :enddate')
                ->setParameter('enddate', $data['enddate']);
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }
    
    /**
     * Delete temporal files
     * 
     */
    public static function deleteTemporalFiles($directory)
    {
        $dir = opendir($directory);
        while ($file = readdir($dir)) {
            if (is_file($file)) {
                unlink($directory . $file);
            }
        }
    }

    /**
     * Get List Workshopsschedulings 
     */
    public static function getListPollschedulingToWorkshopschedulingOptions(DoctrineRegistry $doctrineRegistry, $wsId, $psId = NULL)
    {
        $repository = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
        $queryBuilder = $repository->createQueryBuilder('ps')
            ->select('ps.id, ps.title, ps.pollscheduling_period period')
            ->where('ps.status = 1 
                    AND ps.deleted = 0 
                    AND ps.entity_id = :entity_id
                    AND ps.entity_type = :entity_type')
            ->setParameter('entity_id', $wsId)
            ->setParameter('entity_type', 'workshopscheduling')
            ->orderBy('ps.title', 'ASC');
        
        $pollschedulings = $queryBuilder->getQuery()->getResult();
        
        $output = '<select name="workshopschedulingactivity_pollscheudling_id" required="required">';
        
        $output .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
        if (!empty($pollschedulings)) {
            foreach($pollschedulings as $poll){
                if ($psId != '' && $psId == $poll['id']) {
                    $output .= '<option selected value="' . $poll['id'] . '">' . $poll['title'] . '</option>';
                    $psId = '';
                }
                else {
                    $output .= '<option value="' . $poll['id'] . '">' . $poll['title'] . '</option>';
                }
            }
        }
        $output .= '</select>';
        
        return $output;
    }
    
}