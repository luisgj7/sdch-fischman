<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fishman\WorkshopBundle\Entity\Myerbriggs
 */
class Myerbriggs
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $ei
     */
    private $ei;

    /**
     * @var string $sn
     */
    private $sn;

    /**
     * @var string $tf
     */
    private $tf;

    /**
     * @var string $jp
     */
    private $jp;

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workbook", inversedBy="myerbriggs")
     * @ORM\JoinColumn(name="workbook_id", referencedColumnName="id")
     */
    protected $workbook;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ei
     *
     * @param string $ei
     * @return Myerbriggs
     */
    public function setEi($ei)
    {
        $this->ei = $ei;
    
        return $this;
    }

    /**
     * Get ei
     *
     * @return string 
     */
    public function getEi()
    {
        return $this->ei;
    }

    /**
     * Set sn
     *
     * @param string $sn
     * @return Myerbriggs
     */
    public function setSn($sn)
    {
        $this->sn = $sn;
    
        return $this;
    }

    /**
     * Get sn
     *
     * @return string 
     */
    public function getSn()
    {
        return $this->sn;
    }

    /**
     * Set tf
     *
     * @param string $tf
     * @return Myerbriggs
     */
    public function setTf($tf)
    {
        $this->tf = $tf;
    
        return $this;
    }

    /**
     * Get tf
     *
     * @return string 
     */
    public function getTf()
    {
        return $this->tf;
    }

    /**
     * Set jp
     *
     * @param string $jp
     * @return Myerbriggs
     */
    public function setJp($jp)
    {
        $this->jp = $jp;
    
        return $this;
    }

    /**
     * Get jp
     *
     * @return string 
     */
    public function getJp()
    {
        return $this->jp;
    }

    /**
     * Set workbook
     *
     * @param Fishman\WorkshopBundle\Entity\Workbook $workbook
     * @return Myerbriggs
     */
    public function setWorkbook(\Fishman\WorkshopBundle\Entity\Workbook $workbook = null)
    {
        $this->workbook = $workbook;
    
        return $this;
    }

    /**
     * Get workbook
     *
     * @return Fishman\WorkshopBundle\Entity\Workbook 
     */
    public function getWorkbook()
    {
        return $this->workbook;
    }
}