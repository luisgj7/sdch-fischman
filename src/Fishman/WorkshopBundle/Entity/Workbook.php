<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Fishman\WorkshopBundle\Entity\Myerbriggs;

/**
 * Fishman\WorkshopBundle\Entity\Workbook
 */
class Workbook
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $evaluated
     */
    private $evaluated;

    /**
     * @var boolean $myerbriggs_status
     */
    private $myerbriggs_status;

    /**
     * @var boolean $talents_status
     */
    private $talents_status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshopapplicationactivity", inversedBy="workbooks")
     * @ORM\JoinColumn(name="workshopapplicationactivity_id", referencedColumnName="id")
     */
    protected $workshopapplicationactivity;

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Myerbriggs", mappedBy="workbook")
     */
    protected $myerbriggs;  

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workbooktalent", mappedBy="workbook")
     */
    protected $workbooktalents; 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set evaluated
     *
     * @param integer $evaluated
     * @return Workbook
     */
    public function setEvaluated($evaluated)
    {
        $this->evaluated = $evaluated;
    
        return $this;
    }

    /**
     * Get evaluated
     *
     * @return integer 
     */
    public function getEvaluated()
    {
        return $this->evaluated;
    }

    /**
     * Set myerbriggs_status
     *
     * @param boolean $myerbriggsStatus
     * @return Workbook
     */
    public function setMyerbriggsStatus($myerbriggsStatus)
    {
        $this->myerbriggs_status = $myerbriggsStatus;
    
        return $this;
    }

    /**
     * Get myerbriggs_status
     *
     * @return boolean 
     */
    public function getMyerbriggsStatus()
    {
        return $this->myerbriggs_status;
    }

    /**
     * Set talents_status
     *
     * @param boolean $talentsStatus
     * @return Workbook
     */
    public function setTalentsStatus($talentsStatus)
    {
        $this->talents_status = $talentsStatus;
    
        return $this;
    }

    /**
     * Get talents_status
     *
     * @return boolean 
     */
    public function getTalentsStatus()
    {
        return $this->talents_status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workbook
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workbook
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set workshopapplicationactivity
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivity
     * @return Workbook
     */
    public function setWorkshopapplicationactivity(\Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivity = null)
    {
        $this->workshopapplicationactivity = $workshopapplicationactivity;
    
        return $this;
    }

    /**
     * Get workshopapplicationactivity
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopapplicationactivity 
     */
    public function getWorkshopapplicationactivity()
    {
        return $this->workshopapplicationactivity;
    }

    /**
     * Set myerbriggs
     *
     * @param Fishman\WorkshopBundle\Entity\Myerbriggs $myerbriggs
     * @return Workbook
     */
    public function setMyerbriggs(\Fishman\WorkshopBundle\Entity\Myerbriggs $myerbriggs = null)
    {
        $this->myerbriggs = $myerbriggs;
    
        return $this;
    }

    /**
     * Get myerbriggs
     *
     * @return Fishman\WorkshopBundle\Entity\Myerbriggs
     */
    public function getMyerbriggs()
    {
        return $this->myerbriggs;
    }

    public function __toString()
    {
         return $this->evaluated;
    }    

    /**
     * Add workbooktalents
     *
     * @param Fishman\WorkshopBundle\Entity\Workbooktalent $workbooktalents
     * @return Workbook
     */
    public function addWorkbooktalent(\Fishman\WorkshopBundle\Entity\Workbooktalent $workbooktalents)
    {
        $this->workbooktalents[] = $workbooktalents;
    
        return $this;
    }

    /**
     * Remove workbooktalents
     *
     * @param Fishman\WorkshopBundle\Entity\Workbooktalent $workbooktalents
     */
    public function removeWorkbooktalent(\Fishman\WorkshopBundle\Entity\Workbooktalent $workbooktalents)
    {
        $this->workbooktalents->removeElement($workbooktalents);
    }

    /**
     * Get workbooktalents
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkbooktalents()
    {
        return $this->workbooktalents;
    }

    public function getMoreTalents()
    {
        $moreTalents = array();
        $talents = $this->getWorkbooktalents();
        foreach($talents as $t){
            if($t->getDirection() == '+'){
                $moreTalents[] = $t;
            }
        }
        usort($moreTalents, 'Fishman\WorkshopBundle\Entity\Workbooktalent::compareWBTalents');

        return $moreTalents;
    }
    
    public function getLessTalents()
    {
        $lessTalents = array();
        $talents = $this->getWorkbooktalents();
        foreach($talents as $t){
            if($t->getDirection() == '-'){
                $lessTalents[] = $t;
            }
        }
        usort($lessTalents, 'Fishman\WorkshopBundle\Entity\Workbooktalent::compareWBTalents');

        return $lessTalents;        
    }

    /**
     * createWorkbooks
     */
    public static function createWorkbooks(DoctrineRegistry $doctrineRegistry, $wiBoss, $wiCompany, $entity)
    {
        $em = $doctrineRegistry->getManager();
        
        // Recovers Workinginformation collaborators

        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $results = $repository->createQueryBuilder('wa')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('wi.user', 'u')
            ->where('wa.workshopscheduling = :workshopscheduling 
                    AND wa.deleted = 0
                    AND wi.company = :company')
            ->andWhere('wi.boss = :boss 
                    OR wi.id = :workinginformation')
            ->andWhere('wi.status = 1')
            ->setParameter(':workshopscheduling', $entity->getWorkshopapplication()->getWorkshopscheduling()->getId())
            ->setParameter(':company', $wiCompany)
            ->setParameter(':boss', $wiBoss)
            ->setParameter(':workinginformation', $wiBoss)
            ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC')
            ->getQuery()
            ->getResult();
        
        if (!empty($results)) {
            
            foreach ($results as $r) {
                
                // Add Workbooks
                
                $wb_entity  = new Workbook();
                
                $wb_entity->setWorkshopapplicationactivity($entity);
                $wb_entity->setEvaluated($r->getWorkinginformation()->getId());
                $wb_entity->setMyerbriggsStatus(FALSE);
                $wb_entity->setTalentsStatus(FALSE);
                $wb_entity->setCreated(new \DateTime());
                $wb_entity->setChanged(new \DateTime());
                
                $em->persist($wb_entity);
                $em->flush();
                
                // Add Myerbriggs
                
                $mb_entity  = new Myerbriggs();
                
                $mb_entity->setWorkbook($wb_entity);
        
                $em->persist($mb_entity);
                $em->flush();
            }
            
        }

        $entity->setAction('none');
        $entity->setInitdate(new \DateTime());

        $em->persist($entity);
        $em->flush();
    }

    /**
     * Get List Workbooks
     */
    public static function getListWorkbooks(DoctrineRegistry $doctrineRegistry, $waaId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workbook');
        $queryBuilder = $repository->createQueryBuilder('wb')
            ->select('wb.id, wi.id wi_id, wi.code, u.surname, u.lastname, u.names, cou.name unit, cch.name charge, 
                      wb.myerbriggs_status, wb.talents_status')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wb.evaluated = wi.id')
            ->innerJoin('wi.user', 'u')
            ->innerJoin('wi.companycharge', 'cch')
            ->innerJoin('wi.companyorganizationalunit', 'cou')
            ->where('wb.workshopapplicationactivity = :workshopapplicationactivity')
            ->setParameter('workshopapplicationactivity', $waaId)
            ->orderBy('wi.code', 'ASC');
        
        // Add arguments
        
        if (!empty($data)) {
            if ($data['word']) {
                $queryBuilder
                    ->andWhere('wi.code LIKE :code 
                            OR u.surname LIKE :surname 
                            OR u.lastname LIKE :lastname 
                            OR u.names LIKE :names')
                    ->setParameter('code', '%' . $data['word'] . '%')
                    ->setParameter('surname', '%' . $data['word'] . '%')
                    ->setParameter('lastname', '%' . $data['word'] . '%')
                    ->setParameter('names', '%' . $data['word'] . '%');
            }
            
            if ($data['organizationalunit'] != '') {
                $queryBuilder
                    ->andWhere('cou.id = :organizationalunit')
                    ->setParameter('organizationalunit', $data['organizationalunit']);
            }
            if ($data['charge'] != '') {
                $queryBuilder
                    ->andWhere('cch.id = :charge')
                    ->setParameter('charge', $data['charge']);
            }
            if ($data['myerbriggs_status'] != '' || $data['myerbriggs_status'] === 0) {
                $queryBuilder
                    ->andWhere('wb.myerbriggs_status = :myerbriggs_status')
                    ->setParameter('myerbriggs_status', $data['myerbriggs_status']);
            }
            if ($data['talents_status'] != '' || $data['talents_status'] === 0) {
                $queryBuilder
                    ->andWhere('wb.talents_status = :talents_status')
                    ->setParameter('talents_status', $data['talents_status']);
            }
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workbooktalents = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}