<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Fishman\WorkshopBundle\Entity\Workshopschedulingactivity;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Fishman\WorkshopBundle\Entity\Plangoal;
use Fishman\WorkshopBundle\Entity\Workbook;

/**
 * IMPORTANTE
 * ========================================================
 * El campo status, indica si la actividad está activa o no.
 * La condición para saber si está activa es que la fecha de inicio sea igual o mayor que la fecha actual
 * y además que no tenga actividad predecesora sin terminar.
 * ========================================================
 * El proceso de determinar el status será:
 * - Un proceso tipo cron que verifique las fechas y si no tiene predecesora, cada comienzo de día.
 * - Al enviar una actividad, se verificará las actividades que tenían dependencia con la misma y si 
 *   su fecha es igual o mayor a la actual, las activará.
 */

/**
 * Fishman\WorkshopBundle\Entity\Workshopapplicationactivity
 */
class Workshopapplicationactivity
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var \DateTime $initdate
     */
    private $initdate;

    /**
     * @var \DateTime $enddate
     */
    private $enddate;

    /**
     * @var integer $duration
     */
    private $duration;

    /**
     * @var string $activity_type
     */
    private $activity_type;

    /**
     * @var string $period
     */
    private $period;

    /**
     * @var integer $predecessor_activity
     */
    private $predecessor_activity;

    /**
     * @var integer $workshopschedulingactivity_id
     */
    private $workshopschedulingactivity_id;
    
    /**
     * @var integer $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var boolean $finished
     */
    private $finished;

    /**
     * @var string $action
     */
    private $action;

    /**
     * @var boolean $deleted
     */
    private $deleted;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshopapplication", inversedBy="workshopapplicationactivitys")
     * @ORM\JoinColumn(name="workshopapplication_id", referencedColumnName="id")
     */
    protected $workshopapplication;

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workbook", mappedBy="workshopapplicationactivity")
     */
    protected $workbooks;

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Plan", mappedBy="workshopapplicationactivity")
     */
    protected $plans;  
  

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Questionapplication", mappedBy="workshopapplicationactivity")
     */
    protected $questionapplications;  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Workshopapplicationactivity
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set initdate
     *
     * @param \DateTime $initdate
     * @return Workshopapplicationactivity
     */
    public function setInitdate($initdate)
    {
        $this->initdate = $initdate;
    
        return $this;
    }

    /**
     * Get initdate
     *
     * @return \DateTime 
     */
    public function getInitdate()
    {
        return $this->initdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return Workshopapplicationactivity
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Workshopapplicationactivity
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set activity_type
     *
     * @param string $activity_type
     * @return Workshopapplicationactivity
     */
    public function setActivityType($activityType)
    {
        $this->activity_type = $activityType;
    
        return $this;
    }


    /**
     * Get activity_type
     *
     * @return string 
     */
    public function getActivityType()
    {
        return $this->activity_type;
    }

    /**
     * Set period
     *
     * @param string $period
     * @return Workshopapplicationactivity
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return string 
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set predecessor_activity
     *
     * @param integer $predecessorActivity
     * @return Workshopapplicationacitivity
     */
    public function setPredecessorActivity($predecessorActivity)
    {
        $this->predecessor_activity = $predecessorActivity;
    
        return $this;
    }

    /**
     * Get predecessor_activity
     *
     * @return integer 
     */
    public function getPredecessorActivity()
    {
        return $this->predecessor_activity;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Workshopapplicationactivity
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Workshopapplicationactivity
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set finished
     *
     * @param boolean $finished
     * @return Workshopapplicationactivity
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;
    
        return $this;
    }

    /**
     * Get finished
     *
     * @return boolean 
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Workshopapplicationactivity
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Workshopapplicationactivity
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set workshopapplication
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplication
     * @return Workshopapplicationactivity
     */
    public function setWorkshopapplication(\Fishman\WorkshopBundle\Entity\Workshopapplication $workshopapplication = null)
    {
        $this->workshopapplication = $workshopapplication;
    
        return $this;
    }

    /**
     * Get workshopapplication
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopapplication 
     */
    public function getWorkshopapplication()
    {
        return $this->workshopapplication;
    }

    /**
     * Set workbooks
     *
     * @param Fishman\WorkshopBundle\Entity\Workbook $workbooks
     * @return Workshopapplicationactivity
     */
    public function setWorkbooks(\Fishman\WorkshopBundle\Entity\Workbook $workbooks = null)
    {
        $this->workbooks = $workbooks;
    
        return $this;
    }

    /**
     * Get workbooks
     *
     * @return Fishman\WorkshopBundle\Entity\Workbook 
     */
    public function getWorkbooks()
    {
        return $this->workbooks;
    }

    /**
     * Set plans
     *
     * @param Fishman\WorkshopBundle\Entity\Plan $plans
     * @return Workshopapplicationactivity
     */
    public function setPlans(\Fishman\WorkshopBundle\Entity\Plan $plans = null)
    {
        $this->plans = $plans;
    
        return $this;
    }

    /**
     * Get plans
     *
     * @return Fishman\WorkshopBundle\Entity\Plan 
     */
    public function getPlans()
    {
        return $this->plans;
    }

    /**
     * Set questionapplications
     *
     * @param Fishman\WorkshopBundle\Entity\Questionapplication $questionapplications
     * @return Workshopapplicationactivity
     */
    public function setQuestionapplications(\Fishman\WorkshopBundle\Entity\Questionapplication $questionapplications = null)
    {
        $this->questionapplications = $questionapplications;
    
        return $this;
    }

    /**
     * Get questionapplications
     *
     * @return Fishman\WorkshopBundle\Entity\Questionapplication 
     */
    public function getQuestionapplications()
    {
        return $this->questionapplications;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workbooks = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    /**
     * Add workbooks
     *
     * @param Fishman\WorkshopBundle\Entity\Workbook $workbooks
     * @return Workshopapplicationactivity
     */
    public function addWorkbook(\Fishman\WorkshopBundle\Entity\Workbook $workbooks)
    {
        $this->workbooks[] = $workbooks;
    
        return $this;
    }

    /**
     * Remove workbooks
     *
     * @param Fishman\WorkshopBundle\Entity\Workbook $workbooks
     */
    public function removeWorkbook(\Fishman\WorkshopBundle\Entity\Workbook $workbooks)
    {
        $this->workbooks->removeElement($workbooks);
    }

    /**
     * Get Last Five Workshopapplications
     */
    public static function getLastFiveWorkshopapplicationactivities(DoctrineRegistry $doctrineRegistry, $wiId, $waId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
        $query = $repository->createQueryBuilder('waa')
            ->select('waa.id, waa.name, waa.sequence, waa.activity_type type, waa.predecessor_activity predecessor, 
                      waap.finished waap_finished, waa.initdate, waa.enddate, waa.finished')
            ->innerJoin('waa.workshopapplication', 'wa')
            ->innerJoin('wa.workinginformation', 'wi')
            ->leftJoin('FishmanWorkshopBundle:Workshopapplicationactivity', 'waap', 'WITH', 'waa.predecessor_activity = waap.id')
            ->where('wa.id = :workshopapplication 
                    AND wi.id = :workinginformation 
                    AND waa.status = 1 
                    AND waa.deleted = :deleted')
            ->setParameter('workshopapplication', $waId)
            ->setParameter('workinginformation', $wiId)
            ->setParameter('deleted', FALSE)
            ->orderBy('waa.finished', 'ASC', 'waa.initdate', 'ASC', 'waa.enddate', 'ASC', 'waa.status', 'ASC')
            ->setMaxResults(5)
            ->getQuery();
            
        $output = $query->getResult();
        
        return $output;
    }

    /**
     * Get List Workshopapplicationactivities
     */
    public static function getListWorkshopapplicationactivities(DoctrineRegistry $doctrineRegistry, $wiId, $waId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
        $queryBuilder = $repository->createQueryBuilder('waa')
            ->select('waa.id, waa.name, waa.sequence, waa.duration, waa.period, waa.activity_type type, 
                      waa.predecessor_activity predecessor, waap.finished waap_finished, waa.initdate, waa.enddate, waa.finished')
            ->innerJoin('waa.workshopapplication', 'wa')
            ->innerJoin('wa.workinginformation', 'wi')
            ->leftJoin('FishmanWorkshopBundle:Workshopapplicationactivity', 'waap', 'WITH', 'waa.predecessor_activity = waap.id')
            ->where('wa.id = :workshopapplication 
                    AND wi.id = :workinginformation 
                    AND waa.status = 1 
                    AND waa.deleted = :deleted')
            ->andWhere('waa.name LIKE :name')
            ->setParameter('workshopapplication', $waId)
            ->setParameter('workinginformation', $wiId)
            ->setParameter('deleted', FALSE)
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('waa.finished', 'ASC', 'waa.initdate', 'ASC', 'waa.enddate', 'ASC');
        
        // Add arguments
        
        if ($data['duration'] != '') {
            $queryBuilder
                ->andWhere('waa.duration LIKE :duration')
                ->setParameter('duration', '%' . $data['duration'] . '%');
        }
        if ($data['period'] != '') {
            $queryBuilder
                ->andWhere('waa.period = :period')
                ->setParameter('period', $data['period']);
        }
        if (!empty($data['initdate'])) {
            $queryBuilder
                ->andWhere('waa.initdate = :initdate')
                ->setParameter('initdate', $data['initdate']);
        }
        if (!empty($data['enddate'])) {
            $queryBuilder
                ->andWhere('waa.enddate = :enddate')
                ->setParameter('enddate', $data['enddate']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('waa.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        if ($data['finished'] != '' || $data['finished'] === 0) {
            $queryBuilder
                ->andWhere('waa.finished = :finished')
                ->setParameter('finished', $data['finished']);
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    public static function getByWorkshopschedulingactivity(DoctrineRegistry $doctrineRegistry, $wsaId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
        $queryBuilder = $repository->createQueryBuilder('waa')
            ->select('wi.code, wi.email, u.names, u.lastname, u.surname, waa.finished,
                      cou.name as couname, cch.name as ccname')
            ->innerJoin('waa.workshopapplication', 'wa')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('wi.user', 'u')
            ->innerJoin('wi.companyorganizationalunit', 'cou')
            ->innerJoin('wi.companycharge', 'cch')
            ->where('waa.workshopschedulingactivity_id = :workshopschedulingactivity_id
                    AND waa.status = 1 
                    AND waa.deleted = 0')
            ->setParameter('workshopschedulingactivity_id', $wsaId)
            ->orderBy('waa.finished', 'ASC', 'waa.initdate', 'ASC', 'waa.enddate', 'ASC');
        
        if ($data != '') {
            if ($data['word'] != '') {
                $queryBuilder
                    ->andWhere('wi.code LIKE :code')
                    ->setParameter('code', '%' . $data['word'] . '%');
            }
            if ($data['user_names'] != '') {
                $queryBuilder
                    ->andWhere('u.names LIKE :names')
                    ->setParameter('names', '%' . $data['user_names'] . '%');
            }
            if ($data['user_surname'] != '') {
                $queryBuilder
                    ->andWhere('u.surname LIKE :surname')
                    ->setParameter('surname', '%' . $data['user_surname'] . '%');
            }
            if ($data['user_lastname'] != '') {
                $queryBuilder
                    ->andWhere('u.lastname LIKE :lastname')
                    ->setParameter('lastname', '%' . $data['user_lastname'] . '%');
            }
            if ($data['word'] != '') {
                $queryBuilder
                    ->andWhere('wi.code LIKE :code')
                    ->setParameter('code', '%' . $data['word'] . '%');
            }
            if ($data['word'] != '') {
                $queryBuilder
                    ->andWhere('wi.code LIKE :code')
                    ->setParameter('code', '%' . $data['word'] . '%');
            }
            if ($data['organizationalunit'] != '') {
                $queryBuilder
                    ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                    ->setParameter('organizationalunit', $data['organizationalunit']);
            }
            if ($data['charge'] != '') {
                $queryBuilder
                    ->andWhere('wi.companycharge = :charge')
                    ->setParameter('charge', $data['charge']);
            }
            if ($data['finished'] != '' || $data['finished'] === 0) {
                $queryBuilder
                    ->andWhere('waa.finished = :finished')
                    ->setParameter('finished', $data['finished']);
            }
        }
 
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * cloneChildren
     */
    public static function cloneChildren(DoctrineRegistry $doctrineRegistry, $parentWSAId = -1, $parentWAAId = -1, $entity)
    {
        $em = $doctrineRegistry->getManager();
        
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $results = $repository->createQueryBuilder('wsa')
            ->where('wsa.workshopscheduling = :workshopscheduling 
                    AND wsa.predecessor_activity = :predecessor 
                    AND wsa.status = 1')
            ->setParameter(':workshopscheduling', $entity->getWorkshopscheduling()->getId())
            ->setParameter('predecessor', $parentWSAId)
            ->orderBy('wsa.id', 'ASC')
            ->getQuery()
            ->getResult();

        foreach ($results as $r) {

            $waa = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->findOneBy(array(
                'workshopapplication' => $entity,
                'workshopschedulingactivity_id' => $r->getId()
            ));

            if (!empty($waa)) {
                $query = $em->createQueryBuilder()
                    ->update('FishmanWorkshopBundle:Workshopapplicationactivity waa')
                    ->where('waa.workshopapplication = :workshopapplication
                            AND waa.workshopschedulingactivity_id = :wsactivity')
                    ->setParameter('workshopapplication', $entity)
                    ->setParameter('wsactivity', $r->getId())
                    ->set('waa.deleted', ':deleted')
                    ->setParameter('deleted', FALSE)
                    ->getQuery()
                    ->execute();
            } else {
                $waa_entity  = new Workshopapplicationactivity();
                
                $waa_entity->setName($r->getName());
                $waa_entity->setDuration($r->getDuration());
                $waa_entity->setPeriod($r->getPeriod());
                $waa_entity->setPredecessorActivity($parentWAAId);
                $waa_entity->setWorkshopschedulingactivityId($r->getId());
                $waa_entity->setSequence($r->getSequence());
                $waa_entity->setWorkshopapplication($entity);
                $waa_entity->setActivityType($r->getActivity()->getId());
                $waa_entity->setStatus(true);
                $waa_entity->setFinished(false);
                $waa_entity->setAction('create');
                $waa_entity->setDeleted($r->getDeleted());
                
                $em->persist($waa_entity);
                $em->flush();
                
                self::cloneChildren($doctrineRegistry, $r->getId(), $waa_entity->getId(), $entity);
            }
        }
    }

    /**
     * createRegisters
     */
    public static function createRegisters(DoctrineRegistry $doctrineRegistry, $entity)
    {
        $em = $doctrineRegistry->getManager();
        
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $results = $repository->createQueryBuilder('wa')
            ->where('wa.workshopscheduling = :workshopscheduling 
                    AND wa.profile = :profile')
            ->setParameter(':workshopscheduling', $entity->getWorkshopscheduling()->getId())
            ->setParameter(':profile', 'integrant')
            ->orderBy('wa.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach ($results as $r) {
              
            // Recover workshopapplicationactivity predecessor

            if ($entity->getPredecessorActivity() != -1) {
                
                $waa_predecessor = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
                $waa_predecessor_result = $waa_predecessor->createQueryBuilder('waa')
                    ->where('waa.workshopschedulingactivity_id = :workshopschedulingactivity')
                    ->setParameter(':workshopschedulingactivity', $entity->getPredecessorActivity())
                    ->getQuery()
                    ->getResult();
                $waa_predecessor_entity = current($waa_predecessor_result);
                $predecessor_id = $waa_predecessor_entity->getId();
                
            }
            else {
                $predecessor_id = -1;
            }
            
            // Add workshopapplicationactivity
            
            $waa_entity  = new Workshopapplicationactivity();
            
            $waa_entity->setName($entity->getName());
            $waa_entity->setDuration($entity->getDuration());
            $waa_entity->setPeriod($entity->getPeriod());
            $waa_entity->setPredecessorActivity($predecessor_id);
            $waa_entity->setWorkshopschedulingactivityId($entity->getId());
            $waa_entity->setSequence($entity->getSequence());
            $waa_entity->setWorkshopapplication($r);
            $waa_entity->setActivityType($entity->getActivity()->getId());
            $waa_entity->setStatus(true);
            $waa_entity->setFinished(false);
            $waa_entity->setAction('create');
            $waa_entity->setDeleted($entity->getDeleted());
    
            $em->persist($waa_entity);
            $em->flush();
        }
    }

    /**
     * updateRegisters
     */
    public static function updateRegisters(DoctrineRegistry $doctrineRegistry, $entity)
    {
        $em = $doctrineRegistry->getManager();
        
        $waa_predecessor_entity = FALSE;
        
        // Recover workshopapplicationactivity predecessor    
        if ($entity->getPredecessorActivity() != -1) {
            
            $waa_predecessor = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
            $waa_predecessor_result = $waa_predecessor->createQueryBuilder('waa')
                ->where('waa.workshopschedulingactivity_id = :workshopschedulingactivity')
                ->setParameter(':workshopschedulingactivity', $entity->getPredecessorActivity())
                ->getQuery()
                ->getResult();
            $waa_predecessor_entity = current($waa_predecessor_result);
            
        }
        
        if ($waa_predecessor_entity) {
            $predecessor_id = $waa_predecessor_entity->getId();
        }
        else {
            $predecessor_id = -1;
        }
        
        // Update workshopapplicationactivities
        
        $query = $em->createQueryBuilder()
            ->update('FishmanWorkshopBundle:Workshopapplicationactivity waa')
            ->set('waa.name', ':name')
            ->set('waa.duration', ':duration')
            ->set('waa.period', ':period')
            ->set('waa.predecessor_activity', ':predecessor')
            ->set('waa.sequence', ':sequence')
            ->set('waa.activity_type', ':activity_type')
            ->set('waa.status', ':status')
            ->set('waa.deleted', ':deleted')
            ->where('waa.workshopschedulingactivity_id = :workshopschedulingactivity')
            ->setParameter('name', $entity->getName())
            ->setParameter('duration', $entity->getDuration())
            ->setParameter('period', $entity->getPeriod())
            ->setParameter('predecessor', $predecessor_id)
            ->setParameter('sequence', $entity->getSequence())
            ->setParameter('activity_type', $entity->getActivity()->getId())
            ->setParameter('status', $entity->getStatus())
            ->setParameter('deleted', $entity->getDeleted())
            ->setParameter('workshopschedulingactivity', $entity->getId())
            ->getQuery()
            ->execute();
    }

    /**
     * deleteRegisters
     */
    public static function deleteRegisters(DoctrineRegistry $doctrineRegistry, $entity)
    {
        $em = $doctrineRegistry->getManager();
        
        // Delete workshopapplicationactivities
        
        $query = $em->createQueryBuilder()
            ->update('FishmanWorkshopBundle:Workshopapplicationactivity waa')
            ->set('waa.deleted', ':deleted')
            ->where('waa.workshopschedulingactivity_id = :workshopschedulingactivity')
            ->setParameter('deleted', TRUE)
            ->setParameter('workshopschedulingactivity', $entity->getId())
            ->getQuery()
            ->execute();
    }

    /**
     * integrantRegisters
     */
    public static function integrantRegisters(DoctrineRegistry $doctrineRegistry, $entity, $deleted)
    {
        /* TODO:
         * Al cambiar a la persona asignada de perfil que no sea PARTICIPANTE a COLABORADOR
         * deberán clonarse o actualizarce las actividades respectivas verificando los casos 
         * de mayor complejidad.
         */
         
        $em = $doctrineRegistry->getManager();
        
        if ($deleted) {
            
            // Selected workshopapplicationactivities
            
            $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
            $waa_results = $repository->createQueryBuilder('waa')
                ->where('waa.workshopapplication = :workshopapplication')
                ->setParameter(':workshopapplication', $entity->getId())
                ->orderBy('waa.id', 'ASC')
                ->getQuery()
                ->getResult();
                
            foreach ($waa_results as $waa_entity) {
                
                Notificationexecution::removeNotificationexecutions($doctrineRegistry, '', 'workshopscheduling', $entity->getWorkshopscheduling()->getId(), $entity->getWorkinginformation()->getId(), 'workshopapplicationactivity', $waa_entity->getId());
                
                // Update Workshopsppalicationactivity
                
                $waa_entity->setDeleted(FALSE);
                
                $em->persist($waa_entity);
                $em->flush();
            }
            
        }
        else {
            
            $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
            $waa_results = $repository->createQueryBuilder('waa')
                ->where('waa.workshopapplication = :workshopapplication')
                ->setParameter(':workshopapplication', $entity->getId())
                ->orderBy('waa.id', 'ASC')
                ->getQuery()
                ->getResult();
            
            // We set ids chain Workshopapplications declare and deleted = FALSE
            
            $wsa_ids = '';
            
            if (!empty($waa_results)) {
                
                $waa_count = count($waa_results);
                $i = 1;
            
                foreach ($waa_results as $waa_entity) {
                  
                    // Create string to ids
                  
                    $wsa_ids .= $waa_entity->getWorkshopschedulingactivityId();
                    if ($i < $waa_count) {
                        $wsa_ids .= ',';
                    }
                    $i++;
                    
                    // Update Workshopsppalicationactivity
                    
                    $waa_entity->setDeleted(FALSE);
                    
                    $em->persist($waa_entity);
                    $em->flush();
                }
            }
            
            $em = $doctrineRegistry->getManager();
            
            $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
            $queryBuilder = $repository->createQueryBuilder('wsa')
                ->where('wsa.workshopscheduling = :workshopscheduling 
                        AND wsa.status = 1')
                ->setParameter(':workshopscheduling', $entity->getWorkshopscheduling()->getId())
                ->orderBy('wsa.id', 'ASC');
            
            // Check if there are people assigned
            
            if ($wsa_ids != '') {
                $queryBuilder
                    ->andWhere('wsa.id NOT IN(' . $wsa_ids . ')');
            }
            
            $results = $queryBuilder->getQuery()->getResult();
            
            if (!empty($results)) {
                
                foreach ($results as $r) {
                    
                    if ($r->getPredecessorActivity() != -1) {
                        
                        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
                        $waa_predecessor_result = $repository->createQueryBuilder('waa')
                            ->where('waa.workshopapplication = :workshopapplication
                                    AND waa.workshopschedulingactivity_id = :workshopschedulingactivity')
                            ->setParameter(':workshopapplication', $entity->getId())
                            ->setParameter(':workshopschedulingactivity', $r->getPredecessorActivity())
                            ->orderBy('waa.id', 'ASC')
                            ->getQuery()
                            ->getResult();
                        
                        $waa_predecessor_entity = current($waa_predecessor_result);
                        
                        $predecessor_id = $waa_predecessor_entity->getId();
                    }
                    else {
                        $predecessor_id = $r->getPredecessorActivity();
                    }
                    
                    // Add workshopapplicationactivity
                    
                    $waa_entity  = new Workshopapplicationactivity();
                    
                    $waa_entity->setName($r->getName());
                    $waa_entity->setDuration($r->getDuration());
                    $waa_entity->setPeriod($r->getPeriod());
                    $waa_entity->setPredecessorActivity($predecessor_id);
                    $waa_entity->setWorkshopschedulingactivityId($r->getId());
                    $waa_entity->setSequence($r->getSequence());
                    $waa_entity->setWorkshopapplication($entity);
                    $waa_entity->setActivityType($r->getActivity()->getId());
                    $waa_entity->setStatus(TRUE);
                    $waa_entity->setFinished(FALSE);
                    $waa_entity->setAction('create');
                    $waa_entity->setDeleted($r->getDeleted());
                    
                    $em->persist($waa_entity);
                    $em->flush();
                    /*
                    //Se genera un Notificationexecution por cada waa existente
                    $nx = Notificationexecution::generateNotificationexecution($ns, $r,
                                        'workshopapplicationactivity', $waa_entity->getId(), $ns->getInitdate(),
                                        $ns->getEnddate(), NULL, 1, $entity->getProfile(),
                                        $doctrineRegistry);
                    */
                    $em->persist($nx);
                    $em->flush();
                }
                
            }
            
        }
        
    }

    /**
     * Recovery Type Activity Preprocess 
     */
    public static function activitiesTypePreprocess(DoctrineRegistry $doctrineRegistry, $workshopapplicationactivity)
    {
        $em = $doctrineRegistry->getManager();
      
        if ($workshopapplicationactivity) {
            switch ($workshopapplicationactivity->getActivityType()) {
                case 1: 
                    $wba = $workshopapplicationactivity->getWorkbooks();
                    $wba->workbooks = Workbook::getListWorkbooks($doctrineRegistry, $workshopapplicationactivity->getId());
                    $wba->workbooks = $wba->workbooks->getResult();
                    break;
    
                case 2:
                case 3:
                    if ($workshopapplicationactivity->getPlans()) {
                    $pa = $workshopapplicationactivity->getPlans();
                    $pa->plan = Plangoal::getPlangoalsList($doctrineRegistry, $pa);
                    }
                    break;
                
                case 4:
                    $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Questionapplication');
                    $results = $repository->createQueryBuilder('qa')
                        ->where('qa.workshopapplicationactivity = :workshopapplicationactivity')
                        ->setParameter(':workshopapplicationactivity', $workshopapplicationactivity)
                        ->getQuery()
                        ->getResult();
                                       
                    if ($results) {
                      $workshopapplicationactivity->getQuestionapplications()->qa = $results;
                    }
                    break;
                
                default:
                    // code...
                    break;
            } 

        }
    }
    
    /**
     * Generate Notification executions 
     */
    public static function generateNotificationexecutions(DoctrineRegistry $doctrineRegistry, $entityWAA, $nsId = '')
    {
        $em = $doctrineRegistry->getManager();
        
        $nextNotification = '';
        $nsRepository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $queryBuilder = $nsRepository->createQueryBuilder('ns')
             ->where("ns.entity_type = :entity_type")
             ->setParameter('entity_type', 'workshopscheduling')
             ->andWhere("ns.notification_type_id = :notification_type_id")
             ->setParameter('notification_type_id', $entityWAA->getWorkshopschedulingactivityId());
        
        if ($nsId != '') {
            $queryBuilder
                ->andWhere('ns.id = :notificationscheduling')
                ->setParameter('notificationscheduling', $nsId);
        }
        
        $nss = $queryBuilder->getQuery()->getResult();
        
        foreach ($nss as $ns) {
            
            $predecessor = $ns->getPredecessor();
            $nextNotification = NULL;
            
            if (!empty($predecessor) && $predecessor == '-1') {
                
                $initdate = FALSE;
                
                // get init date pollscheduling
                if (!$ns->getNotificationTypeStatus()) {
                    $entityWSA = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($entityWAA->getWorkshopschedulingactivityId());
                    $initdate = strtotime($entityWSA->getDatein()->format('Y/m/d'));
                }
                elseif ($entityWAA->getFinished()) {
                    $initdate = strtotime($entityWAA->getEnddate()->format('Y/m/d'));
                }
                
                if ($initdate) {
                    // case since
                    if ($ns->getSince() > 0 ) {
                        $initdate = $initdate + ($ns->getSince()*24*60*60);
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                    else {
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                }
                
            }
            
            $profiles = $ns->getAsigned();
           
            if (!empty($profiles)) {
                
                foreach ($profiles as $asigned) {
                    
                    Notificationexecution::generateNotificationexecutions(
                        $doctrineRegistry, 
                        $ns, 
                        $nextNotification, 
                        $asigned, 
                        'workshopapplicationactivity', 
                        $entityWAA->getId(),  
                        $entityWAA->getWorkshopapplication()->getWorkinginformation()->getId(),
                        'workshopscheduling',  
                        $entityWAA->getWorkshopapplication()->getWorkshopscheduling()->getId(), 
                        $entityWAA->getWorkshopapplication()->getWorkshopscheduling()->getCompany()->getId()
                    );
                    
                }
            }
        }

    }

    /**
     * Set workshopschedulingactivity_id
     *
     * @param integer $workshopschedulingactivityId
     * @return Workshopapplicationactivity
     */
    public function setWorkshopschedulingactivityId($workshopschedulingactivityId)
    {
        $this->workshopschedulingactivity_id = $workshopschedulingactivityId;
    
        return $this;
    }

    /**
     * Get workshopschedulingactivity_id
     *
     * @return integer 
     */
    public function getWorkshopschedulingactivityId()
    {
        return $this->workshopschedulingactivity_id;
    }
}