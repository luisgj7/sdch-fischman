<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\WorkshopBundle\Entity\Workshopschedulingactivity
 */
class Workshopschedulingactivity
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $answer_type
     */
    private $answer_type;
    
    /**
     * @var string $alignment
     */
    private $alignment;
    
    /**
     * @var string $options
     */
    private $options;

    /**
     * @var integer $pollscheduling_id
     */
    private $pollscheduling_id;

    /**
     * @var integer $predecessor_activity
     */
    private $predecessor_activity;

    /**
     * @var integer $duration
     */
    private $duration;

    /**
     * @var string $period
     */
    private $period;

    /**
     * @var integer $sequence
     */
    private $sequence;

    /**
     * @var \DateTime $datein
     */
    private $datein;

    /**
     * @var \DateTime $dateout
     */
    private $dateout;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @var boolean $own
     */
    private $own;

    /**
     * @var boolean $deleted
     */
    private $deleted;

    /* @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshopactivity", inversedBy="workshopschedulingactivitys")
       * @ORM\JoinColumn(name="workshopactivity_id", referencedColumnNameumnName="id")
        */
        protected $workshopactivity;



    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Category", inversedBy="workshopschedulingactivitys")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Activity", inversedBy="workshopschedulingactivitys")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    protected $activity;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshopscheduling", inversedBy="workshopschedulingactivitys")
     * @ORM\JoinColumn(name="workshopscheduling_id", referencedColumnName="id")
     */
    protected $workshopscheduling;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Workshopschedulingactivity
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set answer_type
     *
     * @param string $answerType
     * @return Workshopschedulingactivity
     */
    public function setAnswerType($answerType)
    {
        $this->answer_type = $answerType;
    
        return $this;
    }

    /**
     * Get answer_type
     *
     * @return string 
     */
    public function getAnswerType()
    {
        return $this->answer_type;
    }

    /**
     * Set alignment
     *
     * @param string $alignment
     * @return Pollquestion
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
    
        return $this;
    }

    /**
     * Get alignment
     *
     * @return string 
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return Pollquestion
     */
    public function setOptions($options)
    {
        $this->options = $options;
    
        return $this;
    }

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set pollscheduling_id
     *
     * @param integer $pollschedulingId
     * @return Workshopschedulingactivity
     */
    public function setPollschedulingId($pollschedulingId)
    {
        $this->pollscheduling_id = $pollschedulingId;
    
        return $this;
    }

    /**
     * Get pollscheduling_id
     *
     * @return integer 
     */
    public function getPollschedulingId()
    {
        return $this->pollscheduling_id;
    }

    /**
     * Set predecessor_activity
     *
     * @param integer $predecessorActivity
     * @return Workshopschedulingactivity
     */
    public function setPredecessorActivity($predecessorActivity)
    {
        $this->predecessor_activity = $predecessorActivity;
    
        return $this;
    }

    /**
     * Get predecessor_activity
     *
     * @return integer 
     */
    public function getPredecessorActivity()
    {
        return $this->predecessor_activity;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Workshopschedulingactivity
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set period
     *
     * @param string $period
     * @return Workshopschedulingactivity
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return string 
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Workshopschedulingactivity
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set datein
     *
     * @param \DateTime $datein
     * @return Workshopschedulingactivity
     */
    public function setDatein($datein)
    {
        $this->datein = $datein;
    
        return $this;
    }

    /**
     * Get datein
     *
     * @return \DateTime 
     */
    public function getDatein()
    {
        return $this->datein;
    }

    /**
     * Set dateout
     *
     * @param \DateTime $dateout
     * @return Workshopschedulingactivity
     */
    public function setDateout($dateout)
    {
        $this->dateout = $dateout;
    
        return $this;
    }

    /**
     * Get dateout
     *
     * @return \DateTime 
     */
    public function getDateout()
    {
        return $this->dateout;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Workshopschedulingactivity
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workshopschedulingactivity
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workshopschedulingactivity
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Workshopschedulingactivity
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Workshopschedulingactivity
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set own
     *
     * @param boolean $own
     * @return Workshopschedulingactivity
     */
    public function setOwn($own)
    {
        $this->own = $own;
    
        return $this;
    }

    /**
     * Get own
     *
     * @return boolean 
     */
    public function getOwn()
    {
        return $this->own;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Workshopschedulingactivity
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set category
     *
     * @param Fishman\EntityBundle\Entity\Category $category
     * @return Workshopschedulingactivity
     */
    public function setCategory(\Fishman\EntityBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return Fishman\EntityBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set activity
     *
     * @param Fishman\WorkshopBundle\Entity\Activity $activity
     * @return Workshopschedulingactivity
     */
    public function setActivity(\Fishman\WorkshopBundle\Entity\Activity $activity = null)
    {
        $this->activity = $activity;
    
        return $this;
    }

    /**
     * Get activity
     *
     * @return Fishman\WorkshopBundle\Entity\Activity 
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set workshopscheduling
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopscheduling
     * @return Workshopschedulingactivity
     */
    public function setWorkshopscheduling(\Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopscheduling = null)
    {
        $this->workshopscheduling = $workshopscheduling;
    
        return $this;
    }

    /**
     * Get workshopscheduling
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopscheduling 
     */
    public function getWorkshopscheduling()
    {
        return $this->workshopscheduling;
    }

    /**
     * SetFunction Activityscheduling 
     */
    public static function cloneChildren(DoctrineRegistry $doctrineRegistry, $entity,$parentNId = -1, $parentNSId = -1, $entityId, $userBy)
    {
        $em = $doctrineRegistry->getManager();
        
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopactivity');
        $results = $repository->createQueryBuilder('wa')
            ->where('wa.workshop = :workshopid 
                    AND wa.predecessor_activity = :predecessor')
            ->setParameter('workshopid', $entityId)
            ->setParameter('predecessor', $parentNId)
            ->orderBy('wa.id', 'ASC')
            ->getQuery()
            ->getResult();
            
        foreach ($results as $wa) {
            $activity = new Workshopschedulingactivity();
            
            $activity->setName($wa->getName()); 
            $activity->setAnswerType($wa->getAnswerType());
            $activity->setAlignment($wa->getAlignment());
            $activity->setOptions($wa->getOptions());
            $activity->setPredecessorActivity($parentNSId);
            $activity->setDuration($wa->getDuration());
            $activity->setPeriod($wa->getPeriod());
            $activity->setSequence($wa->getSequence());
            $activity->setStatus($wa->getStatus());
            $activity->setCreatedBy($userBy->getId());
            $activity->setModifiedBy($userBy->getId());
            $activity->setCreated(new \DateTime());
            $activity->setChanged(new \DateTime());
            $activity->setDeleted(FALSE);
            $activity->setOwn(FALSE);

            $activity->setWorkshopscheduling($entity);
            $activity->setCategory($wa->getCategory());
            $activity->setActivity($wa->getActivity());
            $activity->setWorkshopactivity($wa);
            $em->persist($activity);
            $em->flush();
        
            self::cloneChildren($doctrineRegistry, $entity, $wa->getId(), $activity->getId(), $wa->getWorkshop(), $userBy);
        }
    }

    public static function tracingByWorkshopscheduling(DoctrineRegistry $doctrineRegistry, $workshopschedulingid, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $queryBuilder = $repository->createQueryBuilder('wsa')
            ->select('wsa.id, wsa.sequence, wsa.name, wsa.datein, wsa.dateout,
                      SUM(waa.finished) / COUNT(waa.id) * 100 as percentaje,
                      waa.duration, waa.period')
            ->leftJoin('FishmanWorkshopBundle:Workshopapplicationactivity',
                       'waa', 'WITH', 'wsa.id = waa.workshopschedulingactivity_id')
            ->where('wsa.workshopscheduling = :workshopscheduling
                     AND wsa.status = 1 AND wsa.deleted = 0
                     AND waa.status = 1 AND waa.deleted = 0')
            ->setParameter('workshopscheduling', $workshopschedulingid)
            ->groupBy('wsa.id');
        
        if (!empty($data)) {
            if ($data['sequence'] != '' || $data['sequence'] === 0) {
                $queryBuilder
                    ->andWhere('wsa.sequence = :sequence')
                    ->setParameter('sequence', $data['sequence']);
            }   
            if ($data['word'] != '') {
                $queryBuilder
                    ->andWhere('wsa.name LIKE :name')
                    ->setParameter('name', '%' . $data['word'] . '%');
            }
            if ($data['duration'] != '') {
                $queryBuilder
                    ->andWhere('waa.duration LIKE :duration')
                    ->setParameter('duration', '%' . $data['duration'] . '%');
            }
            if ($data['period'] != '') {
                $queryBuilder
                    ->andWhere('waa.period = :period')
                    ->setParameter('period', $data['period']);
            }
            if (!empty($data['initdate'])) {
                $queryBuilder
                    ->andWhere('wsa.datein = :initdate')
                    ->setParameter('initdate', $data['initdate']);
            }
            if (!empty($data['enddate'])) {
                $queryBuilder
                    ->andWhere('wsa.dateout = :enddate')
                    ->setParameter('enddate', $data['enddate']);
            }
        }
                
        $query = $queryBuilder->getQuery();
            
        return $query;
    }

    public static function getActivityListOfWorkshop($repository, $workshopscheduling, $first, $typeId)
    {   
      $output = '';
      $query = $repository->createQueryBuilder('wsa')
           ->where('wsa.workshopscheduling = :workshopscheduling_id
                AND wsa.datein IS NOT NULL
                AND wsa.dateout IS NOT NULL')
           ->setParameter('workshopscheduling_id', $workshopscheduling)
           ->orderBy('wsa.id', 'ASC')
           ->getQuery();
      $activities = $query->getResult();
      
      $output = '<select name="fishman_notificationbundle_notificationschedulingtype[notification_type_id]" class="type_notification type_na" >';
      
      $output .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
      foreach($activities as $activity){
          if($first){
              $name = $activity->getName();
              $first = false;
          }
          if ($typeId != '' && $typeId == $activity->getId()) {
            $output .= '<option selected value="' . $activity->getId() . '">' . $activity->getName() . '</option>';
            $typeId = '';
          }
          else {
            $output .= '<option value="' . $activity->getId() . '">' . $activity->getName() . '</option>';
          }
      }
      $output .= '</select>';

      return $output;
    }


    /**
     * Set workshopactivity
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivity
     * @return Workshopschedulingactivity
     */
    public function setWorkshopactivity(\Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivity = null)
    {
        $this->workshopactivity = $workshopactivity;
    
        return $this;
    }

    /**
     * Get workshopactivity
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopactivity 
     */
    public function getWorkshopactivity()
    {
        return $this->workshopactivity;
    }
    
    /**
     * Get list workshopschedulingactivity options
     * 
     */
    public static function getListWorkshopschedulingactivityOptions(DoctrineRegistry $doctrine, $wsId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $queryBuilder = $repository->createQueryBuilder('wsa')
            ->select('wsa.id, wsa.name')
            ->where('wsa.status = 1 
                    AND wsa.workshopscheduling = :workshopscheduling')
            ->setParameter('workshopscheduling', $wsId)
            ->orderBy('wsa.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
                      
        foreach($result as $r) {
            $output[$r['id']] = $r['name'];
        }
        
        return $output;
    }
}