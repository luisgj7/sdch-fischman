<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection; 
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Fishman\WorkshopBundle\Entity\Workshopscheduling;
use Fishman\WorkshopBundle\Entity\Workshopapplicationactivity;
use Fishman\PollBundle\Entity\Pollapplication;
use Fishman\NotificationBundle\Entity\Notificationexecution;

/**
 * Fishman\WorkshopBundle\Entity\Workshopapplication
 */
class Workshopapplication
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var \DateTime $initdate
     */
    private $initdate;

    /**
     * @var \DateTime $enddate
     */
    private $enddate;

    /**
     * @var string $profile
     */
    private $profile;

    /**
     * @var string $personaltoken
     */
    private $personaltoken;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @var boolean $deleted
     */
    private $deleted;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshopscheduling", inversedBy="workshopapplications")
     * @ORM\JoinColumn(name="workshopscheduling_id", referencedColumnName="id")
     */
    protected $workshopscheduling;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\AuthBundle\Entity\Workinginformation", inversedBy="workshopapplications")
     * @ORM\JoinColumn(name="workinginformation_id", referencedColumnName="id")
     */
    protected $workinginformation;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopapplicationactivity", mappedBy="workshopapplication")
     */
    protected $workshopapplicationactivitys;    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set initdate
     *
     * @param \DateTime $initdate
     * @return Workshopapplication
     */
    public function setInitdate($initdate)
    {
        $this->initdate = $initdate;
    
        return $this;
    }

    /**
     * Get initdate
     *
     * @return \DateTime 
     */
    public function getInitdate()
    {
        return $this->initdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return Workshopapplication
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set profile
     *
     * @param string $profile
     * @return Workshopapplication
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    
        return $this;
    }

    /**
     * Get profile
     *
     * @return string 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set personaltoken
     *
     * @param string $personaltoken
     * @return Workshopapplication
     */
    public function setPersonaltoken($personaltoken)
    {
        $this->personaltoken = $personaltoken;
    
        return $this;
    }

    /**
     * Get personaltoken
     *
     * @return string 
     */
    public function getPersonaltoken()
    {
        return $this->personaltoken;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Workshopapplication
     */
    public function setStatus($status)
    {
        $this->status = $status;    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set workshopscheduling
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopscheduling
     * @return Workshopapplication
     */
    public function setWorkshopscheduling(\Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopscheduling = null)
    {
        $this->workshopscheduling = $workshopscheduling;
    
        return $this;
    }

    /**
     * Get workshopscheduling
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopscheduling 
     */
    public function getWorkshopscheduling()
    {
        return $this->workshopscheduling;
    }

    /**
     * Set workinginformation
     *
     * @param Fishman\AuthBundle\Entity\Workinginformation $workinginformation
     * @return Workshopapplication
     */
    public function setWorkinginformation(\Fishman\AuthBundle\Entity\Workinginformation $workinginformation = null)
    {
        $this->workinginformation = $workinginformation;
    
        return $this;
    }

    /**
     * Get workinginformation
     *
     * @return Fishman\AuthBundle\Entity\Workinginformation 
     */
    public function getWorkinginformation()
    {
        return $this->workinginformation;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workshopapplicationactivitys = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add workshopapplicationactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivitys
     * @return Workshopapplication
     */
    public function addWorkshopapplicationactivity(\Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivitys)
    {
        $this->workshopapplicationactivitys[] = $workshopapplicationactivitys;
    
        return $this;
    }

    /**
     * Remove workshopapplicationactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivitys
     */
    public function removeWorkshopapplicationactivity(\Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivitys)
    {
        $this->workshopapplicationactivitys->removeElement($workshopapplicationactivitys);
    }

    /**
     * Get workshopapplicationactivitys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopapplicationactivitys()
    {
        return $this->workshopapplicationactivitys;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workshopapplication
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workshopapplication
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Workshopapplication
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Workshopapplication
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Workshopapplication
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Get current workshopapplication 
     */
    public static function getCurrentWorshopapplication(DoctrineRegistry $doctrineRegistry, $waId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $query = $repository->createQueryBuilder('wa')
            ->select('wa.id, ws.id ws_id, ws.name, c.id company_id, c.name company_name, wa.profile')
            ->innerJoin('wa.workshopscheduling', 'ws')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('wi.company', 'c')
            ->where('wa.id = :workshopapplication 
                    AND wa.status = 1 
                    AND wi.status = 1 
                    AND ws.status = 1 
                    AND ws.deleted = :ws_deleted
                    AND wa.deleted = :wa_deleted')
            ->setParameter('workshopapplication', $waId)
            ->setParameter('ws_deleted', FALSE)
            ->setParameter('wa_deleted', FALSE)
            ->orderBy('wa.initdate', 'DESC')
            ->getQuery();
        $output = $query->getResult();
        
        return $output;
    }

    /**
     * Get current workshopapplication 
     */
    public static function getCurrentWorkshopapplicationToPollscheduling(DoctrineRegistry $doctrineRegistry, $pscId, $wiId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $query = $repository->createQueryBuilder('wa')
            ->innerJoin('wa.workshopscheduling', 'ws')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('FishmanPollBundle:Pollscheduling', 'psc', 'WITH', 'ws.id = psc.entity_id')
            ->where('wi.id = :workinginformation 
                    AND psc.id = :pollscheduling 
                    AND wa.status = 1 
                    AND wi.status = 1 
                    AND ws.status = 1 
                    AND ws.deleted = 0
                    AND wa.deleted = 0')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('pollscheduling', $pscId)
            ->groupBy('wi.id')
            ->getQuery()
            ->getResult();
        $output = current($query);
        
        return $output;
    }

    /**
     * Get current workshopapplication to Workshopscheduling
     */
    public static function getCurrentWorshopapplicationToWorkshopscheduling(DoctrineRegistry $doctrineRegistry, $wiId, $wsId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $query = $repository->createQueryBuilder('wa')
            ->innerJoin('wa.workshopscheduling', 'ws')
            ->innerJoin('wa.workinginformation', 'wi')
            ->where('ws.id = :workshopscheduling 
                    AND wi.id = :workinginformation 
                    AND wa.status = 1 
                    AND wa.deleted = :deleted')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('workinginformation', $wiId)
            ->setParameter('deleted', FALSE)
            ->orderBy('wa.initdate', 'DESC')
            ->setMaxResults(1)
            ->getQuery();
        $output = $query->getResult();
        
        return current($output);
    }

    /**
     * Get active workshopsapplication 
     */
    public static function getListActiveWorshopapplications(DoctrineRegistry $doctrineRegistry, $wiId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $query = $repository->createQueryBuilder('wa')
            ->select('wa.id, ws.id ws_id, ws.name, wa.initdate, wa.enddate, wa.profile, c.id company_id, c.name company_name')
            ->innerJoin('wa.workshopscheduling', 'ws')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('wi.company', 'c')
            ->where('wi.id = :workinginformation 
                    AND wa.status = 1 
                    AND wi.status = 1 
                    AND ws.status = 1 
                    AND ws.deleted = :ws_deleted
                    AND wa.deleted = :wa_deleted')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('ws_deleted', FALSE)
            ->setParameter('wa_deleted', FALSE)
            ->orderBy('wa.initdate', 'DESC')
            ->groupBy('ws.id')
            ->getQuery();
        $output = $query->getResult();
        
        return $output;
    }

    /**
     * Get Last Five Workshopschedulings
     */
    public static function getLastFiveWorkshopapplications(DoctrineRegistry $doctrineRegistry, $wiId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $query = $repository->createQueryBuilder('wa')
            ->select('wa.id, ws.name, wa.initdate, wa.enddate')
            ->innerJoin('wa.workshopscheduling', 'ws')
            ->where('wa.workinginformation = :workinginformation
                    AND wa.status = 1 
                    AND ws.status = 1 
                    AND ws.deleted = :ws_deleted
                    AND wa.deleted = :wa_deleted')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('ws_deleted', FALSE)
            ->setParameter('wa_deleted', FALSE)
            ->orderBy('wa.initdate', 'DESC')
            ->groupBy('ws.id')
            ->setMaxResults(5)
            ->getQuery();
        $output = $query->getResult();
        
        return $output;
    }

    /**
     * Get List Workshopsschedulings 
     */
    public static function getListWorkshopapplications(DoctrineRegistry $doctrineRegistry, $wiId, $data)
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $queryBuilder = $repository->createQueryBuilder('wa')
            ->select('wa.id, ws.name, ws.time_number, c.name company, wa.initdate, wa.enddate')
            ->innerJoin('wa.workshopscheduling', 'ws')
            ->innerJoin('ws.company', 'c')
            ->where('wa.workinginformation = :workinginformation
                    AND wa.status = 1 
                    AND ws.status = 1 
                    AND ws.deleted = :ws_deleted
                    AND wa.deleted = :wa_deleted')
            ->andWhere('wa.id LIKE :id 
                    OR ws.name LIKE :name')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('ws_deleted', FALSE)
            ->setParameter('wa_deleted', FALSE)
            ->setParameter('id', '%' . $data['word'] . '%')
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('wa.initdate', 'DESC')
            ->groupBy('ws.id');
        
        // Add arguments
        
        if (!empty($data['initdate'])) {
            $queryBuilder
                ->andWhere('wa.initdate = :initdate')
                ->setParameter('initdate', $data['initdate']);
        }
        if (!empty($data['enddate'])) {
            $queryBuilder
                ->andWhere('wa.enddate = :enddate')
                ->setParameter('enddate', $data['enddate']);
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get List Workshopsapplications
     */
    public static function getListWorkshopapplicationsToWorkshopscheduling(DoctrineRegistry $doctrineRegistry, $wsId, $cId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $queryBuilder = $repository->createQueryBuilder('wa')
            ->select('wa.id, ws.id ws_id, wi.code, u.surname, u.lastname, u.names, co.name organizationalunit, cc.name charge')
            ->innerJoin('wa.workshopscheduling', 'ws')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('wi.user', 'u')
            ->innerJoin('wi.companyorganizationalunit', 'co')
            ->innerJoin('wi.companycharge', 'cc')
            ->where('wa.workshopscheduling = :workshopscheduling 
                    AND ws.company = :company
                    AND wa.status = 1 
                    AND ws.status = 1 
                    AND ws.deleted = :ws_deleted
                    AND wa.deleted = :wa_deleted')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('company', $cId)
            ->setParameter('ws_deleted', FALSE)
            ->setParameter('wa_deleted', FALSE)
            ->orderBy('wa.initdate', 'DESC');
        
        if ($data['word'] != '') {
            $queryBuilder
                ->andWhere('wi.code LIKE :code')
                ->setParameter('code', '%' . $data['word'] . '%');
        }
        if ($data['user_names'] != '') {
            $queryBuilder
                ->andWhere('u.names LIKE :names')
                ->setParameter('names', '%' . $data['user_names'] . '%');
        }
        if ($data['user_surname'] != '') {
            $queryBuilder
                ->andWhere('u.surname LIKE :surname')
                ->setParameter('surname', '%' . $data['user_surname'] . '%');
        }
        if ($data['user_lastname'] != '') {
            $queryBuilder
                ->andWhere('u.lastname LIKE :lastname')
                ->setParameter('lastname', '%' . $data['user_lastname'] . '%');
        }
        if ($data['word'] != '') {
            $queryBuilder
                ->andWhere('wi.code LIKE :code')
                ->setParameter('code', '%' . $data['word'] . '%');
        }
        if ($data['word'] != '') {
            $queryBuilder
                ->andWhere('wi.code LIKE :code')
                ->setParameter('code', '%' . $data['word'] . '%');
        }
        if ($data['organizationalunit'] != '') {
            $queryBuilder
                ->andWhere('wi.companyorganizationalunit = :organizationalunit')
                ->setParameter('organizationalunit', $data['organizationalunit']);
        }
        if ($data['charge'] != '') {
            $queryBuilder
                ->andWhere('wi.companycharge = :charge')
                ->setParameter('charge', $data['charge']);
        }
 
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * deleteRegisters
     */
    public static function deleteRegisters(DoctrineRegistry $doctrineRegistry, $entity)
    {   
        $em = $doctrineRegistry->getManager();
        
        // Delete workshopapplications
        
        $query = $em->createQueryBuilder()
            ->update('FishmanWorkshopBundle:Workshopapplication wa')
            ->set('wa.deleted', ':deleted')
            ->where('wa.workshopscheduling = :workshopscheduling')
            ->setParameter('deleted', TRUE)
            ->setParameter('workshopscheduling', $entity->getId())
            ->getQuery()
            ->execute();
    }

    /**
     * Get list profile options
     * 
     */
    public static function getListProfileOptions() 
    {
        $output = array(
            'collaborator' => 'Colaborador',
            'integrant' => 'Participante',
            'boss' => 'Jefe',
            /*'responsible' => 'Responsable',*/ 
            'company' => 'Empresa'
        );
        
        return $output;
    }
    
    /**
     * Get Workshopapplications List
     */
    public static function getWorkshopapplicationsList(DoctrineRegistry $doctrineRegistry, $companyid, $workshopschedulingid, $evaluateds = NULL)
    {
        $repository = $doctrineRegistry->getManager()->getRepository('FishmanWorkshopBundle:Workshopapplication');
        $workinginformations = $repository->createQueryBuilder('wa')
            ->select('wi.id, wi.type, wi.code, u.names, u.surname, u.lastname')
            ->where('wi.company = :company
                    AND wa.workshopscheduling = :workshopscheduling
                    AND wa.deleted = :deleted')
            ->innerJoin('wa.workinginformation', 'wi')
            ->innerJoin('wi.user', 'u')
            ->setParameter('company', $companyid)
            ->setParameter('workshopscheduling', $workshopschedulingid)
            ->setParameter('deleted', FALSE)
            ->orderBy('u.names', 'ASC', 'u.surname', 'ASC', 'u.lastname', 'ASC');

        if ($evaluateds != NULL ) {
            $workinginformations
                ->andWhere('wa.workinginformation NOT IN (:ids) ')
                ->setParameter('ids', $evaluateds);
        }

        $currentQuery = $workinginformations
            ->getQuery()
            ->getResult();

        $output = $currentQuery;
        return $output;
    }
    
    /**
     * Add Workshopapplication
     * 
     */
    public static function addRegister(DoctrineRegistry $doctrine, $profile, $workinginformation, $workshopschedulingid, $userBy)
    {
        $em = $doctrine->getManager();
        
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->findOneBy(array(
            'id' => $workshopschedulingid
        ));
        
        $wa_entity = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
            'workshopscheduling' => $workshopscheduling->getId(),
            'workinginformation' => $workinginformation->getId()
        ));
        
        if (!$wa_entity) {
            $wa_entity = new Workshopapplication();
            $token = Pollapplication::generateCode();
            $wa_entity->setPersonaltoken($token);
            $wa_entity->setWorkshopscheduling($workshopscheduling);
            $wa_entity->setWorkinginformation($workinginformation);
            $wa_entity->setCreated(new \DateTime());
            $wa_entity->setCreatedBy($userBy->getId());
        }
        
        $wa_entity->setProfile($profile);
        $wa_entity->setStatus(TRUE);
        $wa_entity->setDeleted(FALSE);
        $wa_entity->setChanged(new \DateTime());
        $wa_entity->setModifiedBy($userBy->getId());
        
        $em->persist($wa_entity);
        $em->flush();
        
        // Add Workshopschedulingactivity if profile is integrant
        if ($profile == 'integrant') {
            Workshopapplicationactivity::cloneChildren($doctrine, -1, -1, $wa_entity);
        }
        
        // Recuperamos roles del usuario
        $urol = $wa_entity->getWorkinginformation()->getUser()->getRoles();
        
        // Verify notifications for profile
        $repository = $doctrine->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $queryNS = $repository->createQueryBuilder('ns')
            ->where('ns.entity_type = :entity_type 
                  AND ns.entity_id = :entity_id')
            ->setParameter('entity_type', 'workshopscheduling')
            ->setParameter('entity_id', $workshopscheduling->getId())
            ->getQuery()
            ->getResult();
        
        if (!empty($queryNS)) {
            
            foreach ($queryNS as $ns) {
                
                foreach ($ns->getAsigned() as $asigned) {
                    
                    $nsrol = '';
                    
                    if ($asigned == 'responsible') $nsrol = 'ROLE_TEACHER';
                    
                    if ($profile == $asigned || in_array($nsrol, $urol)) {
                        
                        $repository = $em->getRepository('FishmanNotificationBundle:Notificationexecution');
                        $nxTemp = $repository->createQueryBuilder('nx')
                            ->select('nx.entity_id, nx.profile')
                            ->where('nx.notificationscheduling = :nsid 
                                    AND nx.workinginformation_id = :wiid')
                            ->setParameter('nsid', $ns->getId())
                            ->setParameter('wiid', $wa_entity->getWorkinginformation()->getId())
                            ->getQuery()
                            ->getResult();
                        
                        $nx_entity = current($nxTemp);
                        
                        if (!$nx_entity) {
                            
                            switch ($ns->getNotificationType()) {
                                case 'message':
                                    
                                    $nx = Notificationexecution::generateNotificationexecution($ns, $workinginformation->getId(), 
                                              'workshopapplication', $wa_entity->getId(), $ns->getInitdate(), 
                                              $ns->getEnddate(), NULL, 1, $asigned, $doctrine);
                                    $em->persist($nx);
                                    
                                    break;
                                    
                                case 'activity':
                                    
                                    if ($asigned == 'integrant') {
                                        
                                        $repository = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
                                        $waaTemp = $repository->createQueryBuilder('waa')
                                            ->select('waa.id')
                                            ->where('waa.workshopschedulingactivity_id = :wsaid 
                                                    AND waa.workshopapplication = :waid')
                                            ->setParameter('wsaid', $ns->getNotificationTypeId())
                                            ->setParameter('waid', $wa_entity->getId())
                                            ->getQuery()
                                            ->getResult();
                                        
                                        $waa_entity = current($waaTemp);
                                        
                                        $nx = Notificationexecution::generateNotificationexecution($ns, $workinginformation->getId(),
                                                  'workshopapplicationactivity', $waa_entity['id'], $ns->getInitdate(),
                                                  $ns->getEnddate(), NULL, 1, $asigned, $doctrine);
                                        $em->persist($nx);
                                    }
                                    else {
                                        
                                        $repository = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity');
                                        $queryWAA = $repository->createQueryBuilder('waa')
                                            ->select('waa.id')
                                            ->where('waa.workshopschedulingactivity_id = :wsaid')
                                            ->setParameter('wsaid', $ns->getNotificationTypeId())
                                            ->getQuery()
                                            ->getResult();
                                        
                                        foreach ($queryWAA as $waa_entity) {
                                            $nx = Notificationexecution::generateNotificationexecution($ns, $workinginformation->getId(),
                                                      'workshopapplicationactivity', $waa_entity['id'], $ns->getInitdate(),
                                                      $ns->getEnddate(), NULL, 1, $asigned, $doctrine);
                                            $em->persist($nx);
                                        }
                                    }
                                    
                                    break;
                                    
                                case 'poll':
                                    
                                    // Recover Pollapplications
                                    $pollapplications = $em->getRepository('FishmanPollBundle:Pollapplication')->findByPollscheduling($ns->getNotificationTypeId());
                                    
                                    if (!empty($pollapplications)) {
                                        foreach ($pollapplications as $pa_entity) {
                                            $nx = Notificationexecution::generateNotificationexecution($ns, $workinginformation->getId(),
                                                      'pollapplication', $pa_entity->getId(), $ns->getInitdate(),
                                                      $ns->getEnddate(), NULL, 1, $asigned, $doctrine);
                                            $em->persist($nx);
                                        }
                                    }
                                    
                                    break;
                            }
                        }
                    }
                }
            }
            
        }
        
        $em->flush();
        
    }
    
    /**
     * Generate Notification executions 
     */
    public static function generateNotificationexecutions(DoctrineRegistry $doctrineRegistry, $entityWA, $nsId = '')
    {
        $em = $doctrineRegistry->getManager();
        
        $nextNotification = '';
        $nsRepository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $queryBuilder = $nsRepository->createQueryBuilder('ns')
             ->where("ns.entity_type = :entity_type")
             ->setParameter('entity_type', 'workshopscheduling');
        
        if ($nsId != '') {
            $queryBuilder
                ->andWhere('ns.id = :notificationscheduling')
                ->setParameter('notificationscheduling', $nsId);
        }
        
        $nss = $queryBuilder->getQuery()->getResult();
        
        foreach ($nss as $ns) {
            
            $predecessor = $ns->getPredecessor();
            $nextNotification = NULL;
            
            if (!empty($predecessor) && $predecessor == '-1') {
                
                $initdate = FALSE;
                
                // get init date pollscheduling
                if (!$ns->getNotificationTypeStatus()) {
                    $entityWS = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($entityWA->getWorkshopscheduling()->getId());
                    $initdate = strtotime($entityWS->getInitdate()->format('Y/m/d'));
                }
                elseif ($entityWS->getFinished()) {
                    $initdate = strtotime($entityWS->getEnddate()->format('Y/m/d'));
                }
                
                if ($initdate) {
                    // case since
                    if ($ns->getSince() > 0 ) {
                        $initdate = $initdate + ($ns->getSince()*24*60*60);
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                    else {
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                }
                
            }
            
            $profiles = $ns->getAsigned();
           
            if (!empty($profiles)) {
                
                foreach ($profiles as $asigned) {
                    
                    Notificationexecution::generateNotificationexecutions(
                        $doctrineRegistry, 
                        $ns, 
                        $nextNotification, 
                        $asigned, 
                        'workshopapplication', 
                        $entityWA->getId(), 
                        $entityWA->getWorkinginformation()->getId(),
                        'workshopscheduling', 
                        $entityWA->getWorkshopscheduling()->getId(), 
                        $entityWA->getWorkshopscheduling()->getCompany()->getId()
                    );
                    
                }
            }
        }

    }
    
}