<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\WorkshopBundle\Entity\Plan
 */
class Plan
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $type
     */
    private $type;


    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshopapplicationactivity", inversedBy="plans")
     * @ORM\JoinColumn(name="workshopapplicationactivity_id", referencedColumnName="id")
     */
    protected $workshopapplicationactivity;

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Plangoal", mappedBy="plan")
     */
    protected $plangoals;

    /**
     * @var string $comments
     */
    private $comments;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Plan
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set workshopapplicationactivity
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivity
     * @return Plan
     */
    public function setWorkshopapplicationactivity(\Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivity = null)
    {
        $this->workshopapplicationactivity = $workshopapplicationactivity;
    
        return $this;
    }

    /**
     * Get workshopapplicationactivity
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopapplicationactivity 
     */
    public function getWorkshopapplicationactivity()
    {
        return $this->workshopapplicationactivity;
    }

    /**
     * Set plangoals
     *
     * @param Fishman\WorkshopBundle\Entity\Plangoal $plangoals
     * @return Plan
     */
    public function setPlangoals(\Fishman\WorkshopBundle\Entity\Plangoal $plangoals = null)
    {
        $this->plangoals = $plangoals;
    
        return $this;
    }

    /**
     * Get plangoals
     *
     * @return Fishman\WorkshopBundle\Entity\Plangoal 
     */
    public function getPlangoals()
    {
        return $this->plangoals;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Plangoal
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    
        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Crear un plan (grupal o individual) para una actividad de este tipo
     */
    public static function createPlan(DoctrineRegistry $doctrineRegistry, $wsaactivity){
        //Verificar que la actividad es de tipo (activity_type) plan grupal(2) o plan individual(3)
        $type = $wsaactivity->getActivityType();
        if(!($type == 2 || $type == 3)){
            return false;
        }

        $plan = new Plan();
        $plan->setWorkshopapplicationactivity($wsaactivity);
        if($type ==2){
            $plan->setType('group');
        }
        else{
            $plan->setType('personal');
        }

        $em = $doctrineRegistry->getManager();
        $em->persist($plan);
        $em->flush();

        return $plan;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plangoals = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add plangoals
     *
     * @param Fishman\WorkshopBundle\Entity\Plangoal $plangoals
     * @return Plan
     */
    public function addPlangoal(\Fishman\WorkshopBundle\Entity\Plangoal $plangoals)
    {
        $this->plangoals[] = $plangoals;
    
        return $this;
    }

    /**
     * Remove plangoals
     *
     * @param Fishman\WorkshopBundle\Entity\Plangoal $plangoals
     */
    public function removePlangoal(\Fishman\WorkshopBundle\Entity\Plangoal $plangoals)
    {
        $this->plangoals->removeElement($plangoals);
    }
}