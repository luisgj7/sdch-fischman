<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\WorkshopBundle\Entity\Activity
 */
class Activity
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopactivity", mappedBy="activity")
     */
    protected $workshopactivitys;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopschedulingactivity", mappedBy="activity")
     */
    protected $workshopschedulingactivitys;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Activity
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
         return $this->name;
    } 

    /**
     * Set status
     *
     * @param boolean $status
     * @return Activity
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workshopactivitys = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add workshopactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys
     * @return Activity
     */
    public function addWorkshopactivity(\Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys)
    {
        $this->workshopactivitys[] = $workshopactivitys;
    
        return $this;
    }

    /**
     * Remove workshopactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys
     */
    public function removeWorkshopactivity(\Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys)
    {
        $this->workshopactivitys->removeElement($workshopactivitys);
    }

    /**
     * Get workshopactivitys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopactivitys()
    {
        return $this->workshopactivitys;
    }

    /**
     * Add workshopschedulingactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys
     * @return Activity
     */
    public function addWorkshopschedulingactivity(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys)
    {
        $this->workshopschedulingactivitys[] = $workshopschedulingactivitys;
    
        return $this;
    }

    /**
     * Remove workshopschedulingactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys
     */
    public function removeWorkshopschedulingactivity(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys)
    {
        $this->workshopschedulingactivitys->removeElement($workshopschedulingactivitys);
    }

    /**
     * Get workshopschedulingactivitys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopschedulingactivitys()
    {
        return $this->workshopschedulingactivitys;
    }
    
    /**
     * Get list activity options
     * 
     */
    public static function getListActivityOptions(DoctrineRegistry $doctrine) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanWorkshopBundle:Activity');
        $queryBuilder = $repository->createQueryBuilder('a')
            ->select('a.id, a.name')
            ->where('a.status = 1')
            ->orderBy('a.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
                      
        foreach($result as $r) {
            $output[$r['id']] = $r['name'];
        }
        
        return $output;
    }
}