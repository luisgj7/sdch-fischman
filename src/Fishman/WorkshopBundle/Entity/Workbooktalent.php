<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fishman\WorkshopBundle\Entity\Workbooktalent
 */
class Workbooktalent
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $direction
     */
    private $direction;

    /**
     * @var integer $sequence
     */
    private $sequence;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Talent", inversedBy="workbooktalents")
     * @ORM\JoinColumn(name="talent_id", referencedColumnName="id")
     */
    protected $talent;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workbook", inversedBy="workbooktalents")
     * @ORM\JoinColumn(name="workbook_id", referencedColumnName="id")
     */
    protected $workbook;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set direction
     *
     * @param string $direction
     * @return Workbooktalent
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    
        return $this;
    }

    /**
     * Get direction
     *
     * @return string 
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Workbooktalent
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set talent
     *
     * @param Fishman\EntityBundle\Entity\Talent $talent
     * @return Workbooktalent
     */
    public function setTalent(\Fishman\EntityBundle\Entity\Talent $talent = null)
    {
        $this->talent = $talent;
    
        return $this;
    }

    /**
     * Get talent
     *
     * @return Fishman\EntityBundle\Entity\Talent 
     */
    public function getTalent()
    {
        return $this->talent;
    }

    /**
     * Set workbook
     *
     * @param Fishman\WorkshopBundle\Entity\Workbook $workbook
     * @return Workbooktalent
     */
    public function setWorkbook(\Fishman\WorkshopBundle\Entity\Workbook $workbook = null)
    {
        $this->workbook = $workbook;
    
        return $this;
    }

    /**
     * Get workbook
     *
     * @return Fishman\WorkshopBundle\Entity\Workbook 
     */
    public function getWorkbook()
    {
        return $this->workbook;
    }

    /**
     * Función de comparación para ordenar Workbook Talents.
     * Se usará para funciones tipo usort
     */
    public static function compareWBTalents($firstWBTalent, $secondWBTalent)
    {
        return $firstWBTalent->getSequence() - $secondWBTalent->getSequence();
    }
}