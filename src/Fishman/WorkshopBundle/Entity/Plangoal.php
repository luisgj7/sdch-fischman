<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\WorkshopBundle\Entity\Plangoal
 */
class Plangoal
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $section
     */
    private $section;

    /**
     * @var string $goal
     */
    private $goal;

    /**
     * @var integer $goal_to
     */
    private $goal_to;

    /**
     * @var integer $in_charge
     */
    private $in_charge;

    /**
     * @var \DateTime $deadline
     */
    private $deadline;

    /**
     * @var boolean $completed
     */
    private $completed;

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Plan", inversedBy="plangoals")
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     */
    protected $plan;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set section
     *
     * @param string $section
     * @return Plangoal
     */
    public function setSection($section)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set goal
     *
     * @param string $goal
     * @return Plangoal
     */
    public function setGoal($goal)
    {
        $this->goal = $goal;
    
        return $this;
    }

    /**
     * Get goal
     *
     * @return string 
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set goal_to
     *
     * @param integer $goalTo
     * @return Plangoal
     */
    public function setGoalTo($goalTo)
    {
        $this->goal_to = $goalTo;
    
        return $this;
    }

    /**
     * Get goal_to
     *
     * @return integer 
     */
    public function getGoalTo()
    {
        return $this->goal_to;
    }

    /**
     * Set in_charge
     *
     * @param integer $inCharge
     * @return Plangoal
     */
    public function setInCharge($inCharge)
    {
        $this->in_charge = $inCharge;
    
        return $this;
    }

    /**
     * Get in_charge
     *
     * @return integer 
     */
    public function getInCharge()
    {
        return $this->in_charge;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     * @return Plangoal
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    
        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime 
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set completed
     *
     * @param boolean $completed
     * @return Plangoal
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    
        return $this;
    }

    /**
     * Get completed
     *
     * @return boolean 
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * Set plan
     *
     * @param Fishman\WorkshopBundle\Entity\Plan $plan
     * @return Plangoal
     */
    public function setPlan(\Fishman\WorkshopBundle\Entity\Plan $plan = null)
    {
        $this->plan = $plan;
    
        return $this;
    }

    /**
     * Get plan
     *
     * @return Fishman\WorkshopBundle\Entity\Plan 
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Recupera los objetivos de un plan, con datos adicionales del usuario y su wi
     * @$doctrineRegistry
     * @$plan, es el plan padre, se podría obtener la información del mismo mediante $plan->getPlangoals(), pero
     * por una cuestion de rendimiento (por la informacion de wi y user) se usa una consulta personalizada
     */
    public static function getPlangoalsList(DoctrineRegistry $doctrineRegistry, $plan)
    {
        //Dependiendo el tipo de plan, obtiene algunos datos diferentes
        switch($plan->getType()){
              
            case 'group': //2, group
                $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Plangoal');
                $query = $repository->createQueryBuilder('pg')
                    ->select('pg.id, ps.name as section, pg.goal, pg.deadline, pg.completed, u.names, u.lastname, u.surname')
                    ->innerJoin('pg.plan', 'p')
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = pg.in_charge')
                    ->innerJoin('wi.user', 'u')
                    ->innerJoin('FishmanPollBundle:Pollsection', 'ps', 'WITH', 'pg.section = ps.id')
                    ->where('pg.plan = :plan')
                    ->setParameter('plan', $plan)
                    ->orderBy('pg.id', 'ASC')
                    ->getQuery();
                return $query->getResult();
                break;
                
            case 'personal': //3, personal
                $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Plangoal');
                $query = $repository->createQueryBuilder('pg')
                    ->select('pg.id, ps.name as section, pg.goal, pg.deadline, pg.completed, u.names, u.lastname, u.surname,
                              tou.names tonames, tou.lastname tolastname, tou.surname tosurname')
                    ->innerJoin('pg.plan', 'p')
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = pg.in_charge')
                    ->innerJoin('wi.user', 'u')
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'towi', 'WITH', 'towi.id = pg.goal_to')
                    ->innerJoin('towi.user', 'tou')
                    ->innerJoin('FishmanPollBundle:Pollsection', 'ps', 'WITH', 'pg.section = ps.id')
                    ->where('pg.plan = :plan')
                    ->setParameter('plan', $plan)
                    ->orderBy('pg.id', 'ASC')
                    ->getQuery();
                return $query->getResult();                
                break;
        }

    }


}