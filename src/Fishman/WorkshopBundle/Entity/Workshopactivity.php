<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\WorkshopBundle\Entity\Workshopactivity
 */
class Workshopactivity
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $answer_type
     */
    private $answer_type;
    
    /**
     * @var string $alignment
     */
    private $alignment;
    
    /**
     * @var string $options
     */
    private $options;

    /**
     * @var integer $predecessor_activity
     */
    private $predecessor_activity;

    /**
     * @var integer $duration
     */
    private $duration;

    /**
     * @var string $period
     */
    private $period;

    /**
     * @var integer $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntiyBundle\Entity\Category", inversedBy="workshopactivitys")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshop", inversedBy="workshopactivitys")
     * @ORM\JoinColumn(name="workshop_id", referencedColumnName="id")
     */
    protected $workshop;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Activity", inversedBy="workshopactivitys")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    protected $activity;

   /**
    * @var \Doctrine\Common\Collections\ArrayCollection
    */
    protected $workshopschedulingactivitys;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Workshopactivity
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set answer_type
     *
     * @param string $answerType
     * @return Workshopactivity
     */
    public function setAnswerType($answerType)
    {
        $this->answer_type = $answerType;
    
        return $this;
    }

    /**
     * Get answer_type
     *
     * @return string 
     */
    public function getAnswerType()
    {
        return $this->answer_type;
    }

    /**
     * Set alignment
     *
     * @param string $alignment
     * @return Pollquestion
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;
    
        return $this;
    }

    /**
     * Get alignment
     *
     * @return string 
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return Pollquestion
     */
    public function setOptions($options)
    {
        $this->options = $options;
    
        return $this;
    }

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set predecessor_activity
     *
     * @param integer $predecessorActivity
     * @return Workshopactivity
     */
    public function setPredecessorActivity($predecessorActivity)
    {
        $this->predecessor_activity = $predecessorActivity;
    
        return $this;
    }

    /**
     * Get predecessor_activity
     *
     * @return integer 
     */
    public function getPredecessorActivity()
    {
        return $this->predecessor_activity;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Workshopactivity
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set period
     *
     * @param string $period
     * @return Workshopactivity
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return string 
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Workshopactivity
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Workshopactivity
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workshopactivity
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workshopactivity
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Workshopactivity
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Workshopactivity
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }


    /**
     * Set category
     *
     * @param Fishman\EntityBundle\Entity\Category $category
     * @return Workshopactivity
     */
    public function setCategory(\Fishman\EntityBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return Fishman\EntityBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set activity
     *
     * @param Fishman\WorkshopBundle\Entity\Activity $activity
     * @return Workshopactivity
     */
    public function setActivity(\Fishman\WorkshopBundle\Entity\Activity $activity = null)
    {
        $this->activity = $activity;
    
        return $this;
    }

    /**
     * Get activity
     *
     * @return Fishman\WorkshopBundle\Entity\Activity 
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set workshop
     *
     * @param Fishman\WorkshopBundle\Entity\Workshop $workshop
     * @return Workshopactivity
     */
    public function setWorkshop(\Fishman\WorkshopBundle\Entity\Workshop $workshop = null)
    {
        $this->workshop = $workshop;
    
        return $this;
    }

    /**
     * Get workshop
     *
     * @return Fishman\WorkshopBundle\Entity\Workshop 
     */
    public function getWorkshop()
    {
        return $this->workshop;
    }

    public function __toString()
    {
           return $this->name;
    }

    public static function getActivityListOfWorkshop($repository, $workshop, $first, $typeId)
    {   
      $output = '';
      $query = $repository->createQueryBuilder('wa')
           ->where('wa.workshop = :workshop_id')
           ->setParameter('workshop_id', $workshop)
           ->orderBy('wa.id', 'ASC')
           ->getQuery();
      $activities = $query->getResult();

      if ($typeId != '') {
      $output = '<select name="fishman_notificationbundle_notificationtype[notification_type_id]" class="type_notification" >';
      }else{
      $output = '<select name="fishman_notificationbundle_notificationtype[notification_type_id]" class="type_notification" >';
      }
      $output .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
      foreach($activities as $activity){
          if($first){
              $name = $activity->getName();
              $first = false;
          }
          if ($typeId != '' && $typeId == $activity->getId()) {
            $output .= '<option selected value="' . $activity->getId() . '">' . $activity->getName() . '</option>';
            $typeId = '';
          } else {
            $output .= '<option value="' . $activity->getId() . '">' . $activity->getName() . '</option>';
          }
      }
      $output .= '</select>';

      return $output;
    }

    /**
     * @var Fishman\WorkshopBundle\Entity\Workshopschedulingactivity
     */
    private $oneToMany;


    /**
     * Set oneToMany
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $oneToMany
     * @return Workshopactivity
     */
    public function setOneToMany(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $oneToMany = null)
    {
        $this->oneToMany = $oneToMany;
    
        return $this;
    }

    /**
     * Get oneToMany
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopschedulingactivity 
     */
    public function getOneToMany()
    {
        return $this->oneToMany;
    }
    
    /**
     * Get list workshopactivity options
     * 
     */
    public static function getListWorkshopactivityOptions(DoctrineRegistry $doctrine, $workshopId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanWorkshopBundle:Workshopactivity');
        $queryBuilder = $repository->createQueryBuilder('wa')
            ->select('wa.id, wa.name')
            ->where('wa.status = 1 
                    AND wa.workshop = :workshop')
            ->setParameter('workshop', $workshopId)
            ->orderBy('wa.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
                      
        foreach($result as $r) {
            $output[$r['id']] = $r['name'];
        }
        
        return $output;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workshopschedulingactivitys = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add workshopschedulingactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys
     * @return Workshopactivity
     */
    public function addWorkshopschedulingactivity(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys)
    {
        $this->workshopschedulingactivitys[] = $workshopschedulingactivitys;
    
        return $this;
    }

    /**
     * Remove workshopschedulingactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys
     */
    public function removeWorkshopschedulingactivity(\Fishman\WorkshopBundle\Entity\Workshopschedulingactivity $workshopschedulingactivitys)
    {
        $this->workshopschedulingactivitys->removeElement($workshopschedulingactivitys);
    }

    /**
     * Get workshopschedulingactivitys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopschedulingactivitys()
    {
        return $this->workshopschedulingactivitys;
    }
}