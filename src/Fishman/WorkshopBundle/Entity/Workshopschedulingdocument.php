<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Fishman\WorkshopBundle\Entity\Workshopschedulingdocument
 * @ORM\HasLifecycleCallbacks
 */
class Workshopschedulingdocument
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $document;

    /**
     * @var text $embed
     */
    private $embed;

    /**
     * @var string $video
     */
    private $video;

    /**
     * @var string $website
     */
    private $website;

    /**
     * @var integer $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @var boolean $own
     */
    private $own;
    
    /**
     * @var Fishman\WorkshopBundle\Entity\Workshopscheduling
     */
    private $workshopscheduling;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Category", inversedBy="workshopschedulingdocuments")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Worshopscheduling", inversedBy="workshopschedulingdocuments")
     * @ORM\JoinColumn(name="workshopscheduling_id", referencedColumnName="id")
     */
    protected $worshopscheduling;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $file;
    
    // a property used temporarily while deleting
    private $filenameForRemove;

    public function getAbsolutePath()
    {
        return null === $this->document ? null : $this->getUploadRootDir().'/'.$this->id.'.'.$this->document;
    }

    public function getWebPath()
    {
        return null === $this->document ? null : $this->getUploadDir().'/'.$this->document;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../uploads/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'workshop/workshopschedulingdocument';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
                $fileName = $this->file->getClientOriginalName();
                $this->document = substr($fileName, strrpos($fileName, '.') + 1);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
       if (null === $this->file) {
            return;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->file->move($this->getUploadRootDir(), $this->id.'.'.$this->document);

        unset($this->file);
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->filenameForRemove = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->filenameForRemove) {
          if ($this->type == 'document') {
            unlink($this->filenameForRemove);
          } else {
            $this->filenameForRemove;
          }
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Workshopschedulingdocument
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Workshopschedulingdocument
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Workshopschedulingdocument
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set document
     *
     * @param string $document
     * @return Workshopschedulingdocument
     */
    public function setDocument($document)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return string 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set embed 
     *
     * @param text $embed
     * @return Workshopdocument
     */
    public function setEmbed($embed)
    {
        $this->embed = $embed;
    
        return $this;
    }

    /**
     * Get embed
     *
     * @return text 
     */
    public function getEmbed()
    {
        return $this->embed;
    }

    /**
     * Set video
     *
     * @param string $video
     * @return Workshopschedulingdocument
     */
    public function setVideo($video)
    {
        $this->video = $video;
    
        return $this;
    }

    /**
     * Get video
     *
     * @return string 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Workshopschedulingdocument
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    
        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Workshopschedulingdocument
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Workshopschedulingdocument
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workshopschedulingdocument
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workshopschedulingdocument
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Workshopschedulingdocument
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Workshopschedulingdocument
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set own
     *
     * @param boolean $own
     * @return Workshopschedulingdocument
     */
    public function setOwn($own)
    {
        $this->own = $own;
    
        return $this;
    }

    /**
     * Get own
     *
     * @return boolean 
     */
    public function getOwn()
    {
        return $this->own;
    }

    /**
     * Set category
     *
     * @param Fishman\EntityBundle\Entity\Category $category
     * @return Workshopschedulingdocument
     */
    public function setCategory(\Fishman\EntityBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return Fishman\EntityBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set workshopscheduling
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopscheduling
     * @return Workshopschedulingdocument
     */
    public function setWorkshopscheduling(\Fishman\WorkshopBundle\Entity\Workshopscheduling $workshopscheduling = null)
    {
        $this->workshopscheduling = $workshopscheduling;
    
        return $this;
    }

    /**
     * Get workshopscheduling
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopscheduling 
     */
    public function getWorkshopscheduling()
    {
        return $this->workshopscheduling;
    }

    /**
     * Get List Workshopschedulingdocuments
     */
    public static function getWorkshopListWorkshopschedulingdocuments(DoctrineRegistry $doctrineRegistry, $wsId, $data = '')
    { 
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument');
        $queryBuilder = $repository->createQueryBuilder('wsd')
            ->select('wsd.id, wsd.name, wsd.type, wsd.document, wsd.sequence')
            ->where('wsd.workshopscheduling = :workshopscheduling') 
            ->andWhere('wsd.name LIKE :name')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('name', '%' . $data['word'] . '%')
            ->orderBy('wsd.sequence', 'ASC');
        
        // Add arguments
        
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('wsd.type = :type')
                ->setParameter('type', $data['type']);
        }
        if ($data['format'] != '') {
            $queryBuilder
                ->andWhere('wsd.document LIKE :format')
                ->setParameter('format', '%' . $data['format'] . '%');
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('wsd.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get Show Workshopschedulingdocument
     */
    public static function getWorkshopShowWorkshopschedulingdocument(DoctrineRegistry $doctrineRegistry, $wsId, $id)
    { 
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopschedulingdocument');
        $result = $repository->createQueryBuilder('wsd')
            ->where('wsd.workshopscheduling = :workshopscheduling
                    AND wsd.id = :workshopschedulingdocument 
                    AND wsd.status = 1')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('workshopschedulingdocument', $id)
            ->getQuery()
            ->getResult();
            
        $entity = current($result);
        
        return $entity;
    }

    /**
     * SetFunction Documentscheduling 
     */
    public static function cloneRegister(DoctrineRegistry $doctrineRegistry ,$entity , $entityId, $userBy)
    {
        $em = $doctrineRegistry->getManager();
      
        $repository = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopdocument');
        $results = $repository->createQueryBuilder('wd')
            ->where('wd.workshop = :workshopid')
            ->setParameter('workshopid', $entityId)
            ->getQuery()
            ->getResult();
        
        foreach ($results as $wd) {
                $document  = new Workshopschedulingdocument();

                $document->setName($wd->getName()); 
                $document->setDescription($wd->getDescription()); 
                $document->setType($wd->getType()); 
                $document->setDocument($wd->getDocument()); 
                $document->setVideo($wd->getVideo()); 
                $document->setWebsite($wd->getWebsite()); 
                $document->setSequence($wd->getSequence()); 
                $document->setStatus($wd->getStatus()); 
                $document->setWorkshopscheduling($entity);
                $document->setCategory($wd->getCategory());
                $document->setCreatedBy($userBy->getId());
                $document->setModifiedBy($userBy->getId());
                $document->setCreated(new \DateTime());
                $document->setChanged(new \DateTime());
                $document->setOwn(false);

                $em->persist($document);
                $em->flush();

                $file1= __DIR__.'/../../../../uploads/workshop/workshopdocument/'.$wd->getId().'.'.$wd->getDocument();
                $file2= __DIR__.'/../../../../uploads/workshop/workshopschedulingdocument/'.$document->getId().'.'.$document->getDocument();

                if ($document->getDocument() != '') {
                  copy($file1, $file2);
                }        
        }
    }
}