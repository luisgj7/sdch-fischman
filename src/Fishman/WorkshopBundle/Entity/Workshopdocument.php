<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\WorkshopBundle\Entity\Workshopdocument
 * @ORM\HasLifecycleCallbacks
 */
class Workshopdocument
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $document;

    /**
     * @var text $embed
     */
    private $embed;

    /**
     * @var text $video
     */
    private $video;

    /**
     * @var text $website
     */
    private $website;

    /**
     * @var string $sequence
     */
    private $sequence;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\EntityBundle\Entity\Category", inversedBy="workshopdocuments")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\WorkshopBundle\Entity\Worshop", inversedBy="workshopdocuments")
     * @ORM\JoinColumn(name="workshop_id", referencedColumnName="id")
     */
    protected $worshop;
    
    /**
     * @Assert\File(maxSize="6000000")
     */
    public $file;
    
    // a property used temporarily while deleting
    private $filenameForRemove;

    public function getAbsolutePath()
    {
        return null === $this->document ? null : $this->getUploadRootDir().'/'.$this->id.'.'.$this->document;
    }

    public function getWebPath()
    {
        return null === $this->document ? null : $this->getUploadDir().'/'.$this->document;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../uploads/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'workshop/workshopdocument';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
                $fileName = $this->file->getClientOriginalName();
                $this->document = substr($fileName, strrpos($fileName, '.') + 1);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
       if (null === $this->file) {
            return;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->file->move($this->getUploadRootDir(), $this->id.'.'.$this->document);

        unset($this->file);
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->filenameForRemove = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->filenameForRemove) {
          if ($this->type == 'document') {
            unlink($this->filenameForRemove);
          } else {
            $this->filenameForRemove;
          }
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Workshopdocument
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Workshopdocument
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Workshopdocument
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set document
     *
     * @param file $document
     * @return Workshopdocument
     */
    public function setDocument($document)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return file 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set embed 
     *
     * @param text $embed
     * @return Workshopdocument
     */
    public function setEmbed($embed)
    {
        $this->embed = $embed;
    
        return $this;
    }

    /**
     * Get embed
     *
     * @return text 
     */
    public function getEmbed()
    {
        return $this->embed;
    }

    /**
     * Set video
     *
     * @param text $video
     * @return Workshopdocument
     */
    public function setVideo($video)
    {
        $this->video = $video;
    
        return $this;
    }

    /**
     * Get video
     *
     * @return text 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set website
     *
     * @param text $website
     * @return Workshopdocument
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    
        return $this;
    }

    /**
     * Get website
     *
     * @return text 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set sequence
     *
     * @param string $sequence
     * @return Workshopdocument
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return string 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Workshopdocument
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workshopdocument
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workshopdocument
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Workshopdocument
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Workshopdocument
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   



    /**
     * Set category
     *
     * @param Fishman\EntityBundle\Entity\Category $category
     * @return Workshopdocument
     */
    public function setCategory(\Fishman\EntityBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return Fishman\EntityBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
    /**
     * @var Fishman\WorkshopBundle\Entity\Workshop
     */
    private $workshop;


    /**
     * Set workshop
     *
     * @param Fishman\WorkshopBundle\Entity\Workshop $workshop
     * @return Workshopdocument
     */
    public function setWorkshop(\Fishman\WorkshopBundle\Entity\Workshop $workshop = null)
    {
        $this->workshop = $workshop;
    
        return $this;
    }

    /**
     * Get workshop
     *
     * @return Fishman\WorkshopBundle\Entity\Workshop 
     */
    public function getWorkshop()
    {
        return $this->workshop;
    }

    /**
     * Get list type options
     * 
     */
    public static function getListTypeOptions() 
    {
        $output = array(
          'video' => 'Video',
          'document' => 'Documento',
          'embed' => 'Embed',
          'website' => 'Página web'
        );
        
        return $output;
    }
}