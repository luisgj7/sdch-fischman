<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fishman\WorkshopBundle\Entity\Questionapplication
 */
class Questionapplication
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $answer_type
     */
    private $answer_type;

    /**
     * @var string $answer
     */
    private $answer;

    /**
     * @var string $answer_options
     */
    private $answer_options;

    /**
     * @var integer $score
     */
    private $score;

    /**
     * @ORM\OneToOne(targetEntity="Fishman\WorkshopBundle\Entity\Workshopapplicationactivity", inversedBy="questionapplications")
     * @ORM\JoinColumn(name="workshopapplicationactivity_id", referencedColumnName="id")
     */
    protected $workshopapplicationactivity;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer_type
     *
     * @param string $answerType
     * @return Questionapplication
     */
    public function setAnswerType($answerType)
    {
        $this->answer_type = $answerType;
    
        return $this;
    }

    /**
     * Get answer_type
     *
     * @return string 
     */
    public function getAnswerType()
    {
        return $this->answer_type;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return Questionapplication
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set answer_options
     *
     * @param string $answerOptions
     * @return Questionapplication
     */
    public function setAnswerOptions($answerOptions)
    {
        $this->answer_options = $answerOptions;
    
        return $this;
    }

    /**
     * Get answer_options
     *
     * @return string 
     */
    public function getAnswerOptions()
    {
        return $this->answer_options;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return Questionapplication
     */
    public function setScore($score)
    {
        $this->score = $score;
    
        return $this;
    }

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set workshopapplicationactivity
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivity
     * @return Questionapplication
     */
    public function setWorkshopapplicationactivity(\Fishman\WorkshopBundle\Entity\Workshopapplicationactivity $workshopapplicationactivity = null)
    {
        $this->workshopapplicationactivity = $workshopapplicationactivity;
    
        return $this;
    }

    /**
     * Get workshopapplicationactivity
     *
     * @return Fishman\WorkshopBundle\Entity\Workshopapplicationactivity 
     */
    public function getWorkshopapplicationactivity()
    {
        return $this->workshopapplicationactivity;
    }
}