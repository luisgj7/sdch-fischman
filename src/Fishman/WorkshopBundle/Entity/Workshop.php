<?php

namespace Fishman\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Fishman\WorkshopBundle\Entity\Workshop
 */
class Workshop
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;


    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopdocument", mappedBy="workshop")
     */
    protected $workshopdocuments; 

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopscheduling", mappedBy="workshop")
     */
    protected $workshopschedulings; 

    /**
     * @ORM\OneToMany(targetEntity="Fishman\WorkshopBundle\Entity\Workshopactivity", mappedBy="workshop")
     */
    protected $workshopactivitys;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Workshop
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Workshop
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Workshop
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workshop
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Workshop
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }


    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Workshop
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Workshop
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }
   

    
    /**
     * Set category
     *
     * @param Fishman\EntityBundle\Entity\Category $category
     * @return Workshop
     */
    public function setCategory(\Fishman\EntityBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return Fishman\EntityBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function __toString()
    {
         return $this->name;
    } 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workshopdocuments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add workshopdocuments
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopdocument $workshopdocuments
     * @return Workshop
     */
    public function addWorkshopdocument(\Fishman\WorkshopBundle\Entity\Workshopdocument $workshopdocuments)
    {
        $this->workshopdocuments[] = $workshopdocuments;
    
        return $this;
    }

    /**
     * Remove workshopdocuments
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopdocument $workshopdocuments
     */
    public function removeWorkshopdocument(\Fishman\WorkshopBundle\Entity\Workshopdocument $workshopdocuments)
    {
        $this->workshopdocuments->removeElement($workshopdocuments);
    }

    /**
     * Get workshopdocuments
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopdocuments()
    {
        return $this->workshopdocuments;
    }

    /**
     * Add workshopactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys
     * @return Workshop
     */
    public function addWorkshopactivity(\Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys)
    {
        $this->workshopactivitys[] = $workshopactivitys;
    
        return $this;
    }

    /**
     * Remove workshopactivitys
     *
     * @param Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys
     */
    public function removeWorkshopactivity(\Fishman\WorkshopBundle\Entity\Workshopactivity $workshopactivitys)
    {
        $this->workshopactivitys->removeElement($workshopactivitys);
    }

    /**
     * Get workshopactivitys
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopactivitys()
    {
        return $this->workshopactivitys;
    }
}