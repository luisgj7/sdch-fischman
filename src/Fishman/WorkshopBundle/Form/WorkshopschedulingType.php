<?php

namespace Fishman\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Fishman\EntityBundle\Entity\Company;

class WorkshopschedulingType extends AbstractType
{   
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('workshop', 'hidden', array(
                'mapped' => false
            ))
            ->add('description', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium' // simple, advanced, bbcode
                )
            ))
            ->add('name')
            ->add('company', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Company',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('c')
                      ->where('c.status = 1
                            AND c.id <> 2')
                      ->orderBy('c.name', 'ASC');
                }, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('custom_header_title', 'text')
            ->add('custom_header_title_text', 'text', array(
                'required' => FALSE
            ))
            ->add('custom_header_title_description', 'text', array(
                'required' => FALSE
            ))
            ->add('custom_header_logo', 'text')
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => false
            ))
            ->add('status', 'choice', array(
                'choices'   => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\WorkshopBundle\Entity\Workshopscheduling'
        ));
    }

    public function getName()
    {
        return 'fishman_workshopbundle_workshopschedulingtype';
    }
}
