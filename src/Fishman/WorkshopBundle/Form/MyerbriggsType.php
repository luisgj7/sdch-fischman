<?php

namespace Fishman\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MyerbriggsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('ei',
                'choice', array(
                         'choices'   => array(
                             'extraversion' => 'Extroversión', 
                             'introversion' => 'Introversión'
                ),
                'attr' => array('class' => 'myerBriggsItem'),                               
                'expanded' => true,
                'multiple' => false,
                'empty_value' => 'Choose an option'))
                ->add('sn',
                'choice', array(
                         'choices'   => array(
                             'sensing' => 'Sensorial', 
                             'intuition' => 'Intuitivo'
                ),
                'attr' => array('class' => 'myerBriggsItem'),                               
                'expanded' => true,
                'multiple' => false,
                'empty_value' => 'Choose an option'))
                ->add('tf',
                'choice', array(
                         'choices'   => array(
                             'thinking' => 'Racional', 
                             'feeling' => 'Emocional'
                ),
                'attr' => array('class' => 'myerBriggsItem'),                               
                'expanded' => true,
                'multiple' => false,
                'empty_value' => 'Choose an option'))
                ->add('jp',
                'choice', array(
                         'choices'   => array(
                             'judging' => 'Calificador', 
                             'perception' => 'Perceptivo'
                ),
                'attr' => array('class' => 'myerBriggsItem'),                               
                'expanded' => true,
                'multiple' => false,
                'empty_value' => 'Choose an option'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\WorkshopBundle\Entity\Myerbriggs'
        ));
    }

    public function getName()
    {
        return 'fishman_workshopbundle_myerbriggstype';
    }
}
