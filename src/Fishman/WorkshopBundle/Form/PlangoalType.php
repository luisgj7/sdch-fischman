<?php

namespace Fishman\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\PollBundle\Entity\Pollsection;
use Fishman\PollBundle\Entity\Pollschedulingsection;

class PlangoalType extends AbstractType
{
    private $wiId;
    private $doctrineRegistry;
    private $plangoal;

    public function __construct($doctrineRegistry, $plangoal, $wiId)
    {
        $this->doctrineRegistry = $doctrineRegistry;
        $this->plangoal = $plangoal;
        $this->wiId = $wiId;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $repository = $this->doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $workshopschedulingactivity = $repository->createQueryBuilder('wsa')
            ->select('wsa.pollscheduling_id')
            ->where('wsa.id = :workshopschedulingactivityid')
            ->setParameter('workshopschedulingactivityid', $this->plangoal->getPlan()->getWorkshopapplicationactivity()->getWorkshopschedulingactivityId())
            ->getQuery()
            ->getResult();
        
        //TODO: Las secciones vienen de la encuesta de motivación (se asume que es la encuesta con id = 1)
        $sections = Pollschedulingsection::getPollschedulingSectionsOptions(
            $this->doctrineRegistry, 
            $workshopschedulingactivity[0]['pollscheduling_id']
        );
        
        //Incharge options
        //La información laboral del usuario y todos sus colaboradores (personas a cargo)
        $wsId = $this->plangoal->getPlan()->getWorkshopapplicationactivity()->getWorkshopapplication()->getWorkshopscheduling()->getId();
        $wiOptions = Workinginformation::getListWorkshopapplicationToCollaborators($this->doctrineRegistry, $wsId, $this->wiId, true);
        
        $builder
            ->add('section', 'choice', array(
                'choices' => $sections, 
                'empty_value' => 'Choose an option',
                'required' => true
            ))
            ->add('goal', 'text', array(
                'required' => true
            ));

        //If plan type = 'personal', show 'goal_to'
        //Son las mismas opciones que in_charge
        if($this->plangoal->getPlan()->getType() == 'personal'){
            $builder
                ->add('goal_to', 'choice', array(
                    'choices' => $wiOptions,
                    'empty_value' => 'Choose an option',
                    'required' => true
                ));
        }

        //Los otros campos que faltan
        $builder
            ->add('in_charge', 'choice', array(
                'choices' => $wiOptions,
                'empty_value' => 'Choose an option',
                'required' => true
            ))
            ->add('deadline', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('completed', 'choice', array(
                'choices'  => array(0 => 'Pendiente', 1 => 'Completo'),
                'empty_value' => 'Choose an option',
                'required' => true
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\WorkshopBundle\Entity\Plangoal'
        ));
    }

    public function getName()
    {
        return 'fishman_workshopbundle_plangoaltype';
    }
}
