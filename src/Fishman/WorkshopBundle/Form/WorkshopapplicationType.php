<?php

namespace Fishman\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\WorkshopBundle\Entity\Workshopschedulingpeople;
use Fishman\WorkshopBundle\Entity\Workshopscheduling;
use Fishman\WorkshopBundle\Entity\Workinginformation;
use Fishman\AuthBundle\Entity\User;

class WorkshopapplicationType extends AbstractType
{   
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $builder
            ->add('status', 'choice', array(
                'choices'   => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))
            ->add('workshopscheduling')
            ->add('workinginformation')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\WorkshopBundle\Entity\Workshopapplication'
        ));
    }

    public function getName()
    {
        return 'fishman_workshopbundle_workshopapplicationtype';
    }
}