<?php

namespace Fishman\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\EntityBundle\Entity\Category;
use Fishman\WorkshopBundle\Entity\Activity;
use Fishman\WorkshopBundle\Entity\Workshopschedulingactivity;

class WorkshopschedulingactivityType extends AbstractType
{
    private $workshopSchedulingId;
    private $workshopSchedulingActivity;
    private $doctrine;

    public function __construct($workshopSchedulingId, $workshopSchedulingActivity, $own, Registry $doctrine)
    {
        $this->workshopSchedulingId = $workshopSchedulingId;
        $this->workshopSchedulingActivity = $workshopSchedulingActivity;
        $this->own = $own;
        $this->doctrine = $doctrine;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->own) {
            $disabled = false;
        }
        else {
            $disabled = true;
        }
        
        $activities_options = array();
        $repository = $this->doctrine->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $result = $repository->createQueryBuilder('wsa')
            ->select('wsa.id, wsa.name')
            ->where('wsa.workshopscheduling = :workshopscheduling 
                    AND wsa.status = 1 
                    AND wsa.deleted = :deleted')
            ->setParameter('workshopscheduling', $this->workshopSchedulingId)
            ->setParameter('deleted', FALSE)
            ->orderBy('wsa.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach($result as $r) {
            if ($this->workshopSchedulingActivity->getId() != $r['id']) {
                $activities_options[$r['id']] = $r['name'];
            }
        }
        
        $builder
            ->add('category', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Category',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('c')
                      ->where('c.status = 1')
                      ->orderBy('c.id', 'ASC');
                 },
                /*'disabled' => $disabled*/
            ))
            ->add('activity', 'entity', array(
                'class' => 'Fishman\WorkshopBundle\Entity\Activity',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('a')
                      ->where('a.status = 1')
                      ->orderBy('a.id', 'ASC');
                },
                /*'disabled' => $disabled*/
            ))
            ->add('name')
            ->add('answer_type', 'choice', array(
                'choices' => array(
                    'simple_text' => 'Cuadro de texto simple', 
                    'unique_option' => 'Opción única', 
                    'multiple_option' => 'Opción múltiple', 
                    'selection_option' => 'Opción selección', 
                    'multiple_text' => 'Cuadro de texto múltiple'
                ), 
                'empty_value' => 'Choose an option', 
                'required' => false,
                /*'disabled' => $disabled*/
            ))
            ->add('alignment', 'choice', array(
                'choices' => array(
                    'horizontal' => 'Horizontal',
                    'vertical' => 'Vertical'
                ),
                'required' => false,
                /*'disabled' => $disabled*/
            ))
            ->add('options', 'hidden', array(
                'required' => false,
                /*'disabled' => $disabled*/
            ))
            ->add('option_text', 'text', array(
                'mapped' => false,
                'required' => false,
                /*'disabled' => $disabled*/
            ))
            ->add('option_value', 'text', array(
                'mapped' => false,
                'max_length' => 2,
                'required' => false,
                /*'disabled' => $disabled*/
            ))
            ->add('predecessor_activity', 'choice', array(
                'choices' => $activities_options, 
                'empty_value' => 'Choose an option', 
                'required' => false,
                /*'disabled' => $disabled*/
            ))
            ->add('duration', 'text')
            ->add('period', 'choice', array(
                'choices'   => array(
                    'day' => 'Días', 
                    'week' => 'Semanas', 
                    'month' => 'Meses'
                ), 
                'empty_value' => 'Choose an option'
            ))
            ->add('datein', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('dateout', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('sequence', 'choice', array(
                'choices' => array(
                    1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 
                    8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 
                    15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20)
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    1 => 'Activo',
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))
            ->add('workshopscheduling')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\WorkshopBundle\Entity\Workshopschedulingactivity'
        ));
    }

    public function getName()
    {
        return 'fishman_workshopbundle_workshopschedulingactivitytype';
    }
}
