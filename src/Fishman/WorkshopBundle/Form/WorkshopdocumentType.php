<?php

namespace Fishman\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Fishman\EntityBundle\Entity\Category;

class WorkshopdocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium' // simple, advanced, bbcode
                )
            ))
            ->add('type', 'choice', array(
                'choices' => array(
                    'video' => 'Video',
                    'document' => 'Documento',
                    'embed' => 'Embed',
                    'website' => 'Página web'
                ),
                'empty_value' => 'Choose an option'
            ))
            ->add('video', 'textarea', array(
                'required' => false
            ))
            ->add('embed', 'textarea', array(
                'required' => false
            ))
            ->add('website', 'text', array(
                'required' => false
            ))
            ->add('file', 'file', array(
                'required' => false
            ))
            ->add('sequence', 'choice', array(
                'choices' => array(
                    1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 
                    8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 
                    15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20
                )
            ))
            ->add('category', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Category',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('c')
                      ->where('c.status = 1')
                      ->orderBy('c.id', 'ASC');
                 },
                 'empty_value' => 'Choose an option'
            ))
            ->add('status', 'choice', array(
                'choices'   => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\WorkshopBundle\Entity\Workshopdocument'
        ));
    }

    public function getName()
    {
        return 'fishman_workshopbundle_workshopdocumenttype';
    }
}
