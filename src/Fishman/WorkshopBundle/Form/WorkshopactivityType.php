<?php

namespace Fishman\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\EntityBundle\Entity\Category;
use Fishman\WorkshopBundle\Entity\Activity;
use Fishman\WorkshopBundle\Entity\Workshopctivity;

class WorkshopactivityType extends AbstractType
{
    private $workshopId;
    private $workshopActivity;
    private $doctrine;

    public function __construct($workshopId, $workshopActivity, Registry $doctrine)
    {
        $this->workshopId = $workshopId;
        $this->workshopActivity = $workshopActivity;
        $this->doctrine = $doctrine;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $activities_options = array();
        $repository = $this->doctrine->getRepository('FishmanWorkshopBundle:Workshopactivity');
        $result = $repository->createQueryBuilder('wa')
            ->select('wa.id, wa.name')
            ->where('wa.workshop = :workshop 
                    AND wa.status = 1')
            ->setParameter('workshop', $this->workshopId)
            ->orderBy('wa.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach($result as $r) {
            if ($this->workshopActivity->getId() != $r['id']) {
                $activities_options[$r['id']] = $r['name'];
            }
        }
        
        $builder
            ->add('category', 'entity', array(
                'class' => 'Fishman\EntityBundle\Entity\Category',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('c')
                      ->where('c.status = 1')
                      ->orderBy('c.id', 'ASC');
                 }
            ))
            ->add('activity', 'entity', array(
                'class' => 'Fishman\WorkshopBundle\Entity\Activity',
                'query_builder' => function(EntityRepository $repository) {
                   return $repository->createQueryBuilder('a')
                      ->where('a.status = 1')
                      ->orderBy('a.id', 'ASC');
                }
            ))
            ->add('name')
            ->add('answer_type', 'choice', array(
                'choices' => array(
                    'simple_text' => 'Cuadro de texto simple', 
                    'unique_option' => 'Opción única', 
                    'multiple_option' => 'Opción múltiple', 
                    'selection_option' => 'Opción selección', 
                    'multiple_text' => 'Cuadro de texto múltiple'
                ), 
                'empty_value' => 'Choose an option',
                'required' => false
            ))
            ->add('alignment', 'choice', array(
                'choices' => array(
                    'horizontal' => 'Horizontal',
                    'vertical' => 'Vertical'
                ),
                'required' => false
            ))
            ->add('options', 'hidden', array(
                'required' => false
            ))
            ->add('option_text', 'text', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('option_value', 'text', array(
                'mapped' => false,
                'max_length' => 2,
                'required' => false
            ))
            ->add('predecessor_activity', 'choice', array(
                'choices' => $activities_options, 
                'empty_value' => 'Choose an option', 
                'required' => false
            ))
            ->add('duration', 'text')
            ->add('period', 'choice', array(
                'choices'   => array(
                    'day' => 'Días', 
                    'week' => 'Semanas', 
                    'month' => 'Meses'
                ), 
                'empty_value' => 'Choose an option', 
            ))
            ->add('sequence', 'choice', array(
                'choices' => array(
                    1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 
                    8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 
                    15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20)
            ))
            ->add('status', 'choice', array(
                'choices'   => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option' 
            ))
            ->add('workshop')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\WorkshopBundle\Entity\Workshopactivity'
        ));
    }

    public function getName()
    {
        return 'fishman_workshopbundle_workshopactivitytype';
    }
}
