<?php

namespace Fishman\FrontEndBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\AuthBundle\Entity\Permission;
use Fishman\WorkshopBundle\Entity\Workshopscheduling;
use Fishman\WorkshopBundle\Entity\Workshopapplication;
use Fishman\WorkshopBundle\Entity\Workshopapplicationactivity;
use Fishman\WorkshopBundle\Entity\Workshopschedulingdocument;
use Fishman\WorkshopBundle\Entity\Workshopschedulingactivity;
use Fishman\WorkshopBundle\Entity\Workshopdocument;
use Fishman\PollBundle\Entity\Benchmarking;
use Fishman\PollBundle\Entity\Pollsection;
use Fishman\PollBundle\Entity\Pollscheduling;
use Fishman\PollBundle\Entity\Pollschedulingsection;
use Fishman\PollBundle\Entity\Pollschedulingpeople;
use Fishman\PollBundle\Entity\Pollapplication;
use Fishman\PollBundle\Entity\Pollapplicationquestion;
use Fishman\PollBundle\Entity\Pollapplicationreport;
use Fishman\NotificationBundle\Entity\Notification;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Fishman\NotificationBundle\Entity\Notificationsend;
use Fishman\EntityBundle\Entity\Company;
use Fishman\EntityBundle\Entity\Nature;
use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\EntityBundle\Entity\Companycharge;
use Fishman\EntityBundle\Entity\Companyfaculty;
use Fishman\EntityBundle\Entity\Companycareer;
use Fishman\EntityBundle\Entity\Headquarter;
use Symfony\Component\HttpFoundation\Response;

use Fishman\FrontEndBundle\Twig\FishmanExtension;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

//jtt
use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Chart;
use PHPExcel_Chart_Axis;
use PHPExcel_Chart_Layout;
use PHPExcel_Chart_Title;
use PHPExcel_Chart_PlotArea;
use PHPExcel_Chart_DataSeries;
use PHPExcel_Chart_DataSeriesValues;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Worksheet_HeaderFooter;
use PHPExcel_Worksheet_HeaderFooterDrawing;

class DashboardController extends Controller
{
    /**
     * If user have more than oner Role is mandatory select one
     * in order to use in whole application
     */
    public function chooserolAction()
    {
         return $this->render('FishmanFrontEndBundle:Dashboard:chooserol.html.twig',
                              array('roles' => $this->get('security.context')->getToken()->getUser()->getRoles()));
    }

    /**
     * If user have more than one active workinginformation (charge and company information)
     * is mandatory to choose one in order to use in whole application
     */
    public function chooseworkinginformationAction()
    {
         // Recovers actives Workinginformations
         $workinginformations = Workinginformation::getListActiveWorkinginformations($this->getDoctrine(),
                                  $this->get('security.context')->getToken()->getUser()->getId());
         return $this->render('FishmanFrontEndBundle:Dashboard:chooseworkinginformation.html.twig', array(
                        'workinginformations' => $workinginformations));
    }
    
    /**
     * Home page.
     *
     */
    public function indexAction()
    {   
        
        // Recover session varaible workinginformation
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        // TODO: Actualizar registros antiguos de pollapplications que no tengan el promedio (average)
        /*
        $pollapplicationsNotAverage = Pollapplication::getPollapplicationNotAverage($this->getDoctrine());
        foreach ($pollapplicationsNotAverage as $pa) {
            Pollapplication::getAverageByPollapplication($this->getDoctrine(), $pa['id']);
        }
        */
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            
            // Recovers last 2 Notificationssend
            $notifications = Notificationsend::getLastTwoNotificationsends($this->getDoctrine(), $wi['id']);
            
            // Recovers last 5 Workshopschedulings
            $workshops = Workshopscheduling::getLastFiveWorkshopschedulings($this->getDoctrine());
            
            // Recovers last 5 Pollaschedulings
            $polls = Pollscheduling::getLastFivePollschedulings($this->getDoctrine(), $rol);
            
            return $this->render('FishmanFrontEndBundle:Dashboard/Index:dashboard_all_admin.html.twig', array(
                'notifications' => $notifications,
                'workshops' => $workshops,
                'polls' => $polls
            ));
            
        }
        else {
          
            // Recovers last 2 Notificationssend
            $notifications = Notificationsend::getLastTwoNotificationsends($this->getDoctrine(), $wi['id']);
            
            // Recovers last 5 Workshopschedulings
            $workshopapplications = Workshopapplication::getLastFiveWorkshopapplications($this->getDoctrine(), $wi['id']);
            
            // Recovers last 5 Pollapplications
            $polls = Pollscheduling::getLastFivePollschedulingsIntegrant($this->getDoctrine(), $wi['id']);
            
            return $this->render('FishmanFrontEndBundle:Dashboard/Index:dashboard_all_profile.html.twig', array(
                'notifications' => $notifications,
                'workshopapplications' => $workshopapplications,
                'polls' => $polls
            ));
            
        }
    }
    
    /**
     * Lists all Notifications.
     *
     */
    public function notificationsAction(Request $request)
    {

        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        
        // Recovering data
        
        $type_options = Notification::getListTypeOptions();
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'type' => '', 
            'entity' => '', 
            'company' => '', 
            'sended' => NULL, 
            'sequence' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('entity', 'text', array(
                'required' => FALSE
            ))
            ->add('company', 'text', array(
                'required' => FALSE
            ))
            ->add('sended', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'type' => '', 
            'entity' => '', 
            'company' => '', 
            'sended' => NULL, 
            'sequence' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovers Notificationssend
        $query = Notificationsend::getListNotificationsends($this->getDoctrine(), $wi['id'], $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'notifications');
        $paginator->setMaxPagerItems(5, 'notifications');
        $notifications = $paginator->paginate($query, 'notifications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('notifications');
        $endPageItem = $paginator->getEndPageItem('notifications');
        $totalItems = $paginator->getTotalItems('notifications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Notification:index.html.twig', array(
            'notifications' => $notifications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }
    
    /**
     * Notifications Show temporally.
     *
     */
    public function notificationsshowAction($id)
    {   
        // Recover Notificationsend
        $entity = Notificationsend::getShowNotificationsend($this->getDoctrine(), $id);
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación.');
            return $this->redirect($this->generateUrl('fishman_front_end_notifications'));
        }
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Notification:show.html.twig', array(
            'entity' => $entity
        ));
    }

    /**
     * Lists all Workshops.
     *
     */
    public function workshopsAction(Request $request)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');

        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
        
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'resolved' => '', 
                'company' => '', 
                'initdate' => NULL, 
                'enddate' => NULL
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('resolved', 'text', array(
                    'required' => FALSE
                ))
                ->add('company', 'text', array(
                    'required' => FALSE
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('enddate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'resolved' => '', 
                'company' => '', 
                'initdate' => NULL, 
                'enddate' => NULL
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Recovers Workshopschedulings
            $query = Workshopscheduling::getListWorkshopschedulings($this->getDoctrine(), $data);
        }
        else {
        
            // Find Entities
            
            $defaultData = array(
                'word' => '',  
                'initdate' => NULL, 
                'enddate' => NULL
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('enddate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'initdate' => NULL, 
                'enddate' => NULL
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Recovers Workshopschedulings
            $query = Workshopapplication::getListWorkshopapplications($this->getDoctrine(), $wi['id'], $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshops');
        $paginator->setMaxPagerItems(5, 'workshops');
        $workshops = $paginator->paginate($query, 'workshops')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshops');
        $endPageItem = $paginator->getEndPageItem('workshops');
        $totalItems = $paginator->getTotalItems('workshops');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            return $this->render('FishmanFrontEndBundle:Dashboard/Workshop:index_admin.html.twig', array(
                'workshops' => $workshops,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
        }
        else {
            return $this->render('FishmanFrontEndBundle:Dashboard/Workshop:index_profile.html.twig', array(
                'workshopapplications' => $workshops,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
        }
    }

    /**
     * Home of Workshop.
     *
     */
    public function workshopAction()
    {
        $em = $this->getDoctrine()->getManager();

        // Recover session varaible workshopapplication
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
                          
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($wa['ws_id']);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }
        
        // Recovers Integrant last 2 Notificationssend
        $notifications = Notificationsend::getWorkshopLastTwoNotificationsends($this->getDoctrine(), $wi['id'], $wa['ws_id']);
                          
        // Recovers Integrant last 5 Workshopapplicationactivity
        $activities = Workshopapplicationactivity::getLastFiveWorkshopapplicationactivities($this->getDoctrine(), $wi['id'], $wa['id']);
        
        // Recovers Integrant last 5 Pollapplications
        $polls = Pollscheduling::getWorkshopLastFivePollschedulings($this->getDoctrine(), $wi['id'], $wa['id']);
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Index:dashboard_workshop_profile.html.twig', array(
            'workshopscheduling' => $workshopscheduling,
            'notifications' => $notifications,
            'activities' => $activities,
            'polls' => $polls
        ));
    }

    /**
     * Home of Workshop admin.
     *
     */
    public function workshopAdminAction($workshopid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible workshopapplication
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }
        
        // Recovers Integrant last 2 Notificationssend
        $notifications = Notificationsend::getWorkshopLastTwoNotificationsends($this->getDoctrine(), $wi['id'], $workshopscheduling->getId());
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Index:dashboard_workshop_admin.html.twig', array(
            'workshopscheduling' => $workshopscheduling, 
            'notifications' => $notifications
        ));
    }
    
    /**
     * Lists all Workshop integrant.
     *
     */
    public function workshopIntegrantsAction(Request $request, $workshopid)
    {
        // Recover Workshopscheduling
        $workshop = Workshopscheduling::getCurrentWorshopscheduling($this->getDoctrine(), $workshopid);
        
        // Recovering data
        
        $companyid = $workshop[0]['company_id'];
        $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $companyid);
        $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $companyid);
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'user_names' => '', 
            'user_surname' => '', 
            'user_lastname' => '', 
            'organizationalunit' => '', 
            'charge' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('user_names', 'text', array(
                'required' => FALSE
            ))
            ->add('user_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('user_lastname', 'text', array(
                'required' => FALSE
            ))
            ->add('organizationalunit', 'choice', array(
                'choices' => $organizationalunit_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('charge', 'choice', array(
                'choices' => $charge_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'user_names' => '', 
            'user_surname' => '', 
            'user_lastname' => '', 
            'organizationalunit' => '', 
            'charge' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovers Workshopapplications
        $query = Workshopapplication::getListWorkshopapplicationsToWorkshopscheduling($this->getDoctrine(), $workshopid, $workshop[0]['company_id'], $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopapplications');
        $paginator->setMaxPagerItems(5, 'workshopapplications');
        $workshopapplications = $paginator->paginate($query, 'workshopapplications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopapplications');
        $endPageItem = $paginator->getEndPageItem('workshopapplications');
        $totalItems = $paginator->getTotalItems('workshopapplications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop:integrants.html.twig', array(
            'workshop' => $workshop[0],
            'workshopapplications' => $workshopapplications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Lists all Workshop Notificationsends.
     *
     */
    public function workshopNotificationsAction(Request $request)
    {   
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');

        $em = $this->getDoctrine()->getManager();
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($wa['ws_id']);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }
        
        // Recovering data
        
        $type_options = Notification::getListTypeOptions();
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'type' => '', 
            'sequence' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'type' => '', 
            'sequence' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }

        // Recovers Notificationssend
        $query = Notificationsend::getWorkshopListNotificationsends($this->getDoctrine(), $wi['id'], $wa['ws_id'], $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopnotifications');
        $paginator->setMaxPagerItems(5, 'workshopnotifications');
        $notifications = $paginator->paginate($query, 'workshopnotifications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopnotifications');
        $endPageItem = $paginator->getEndPageItem('workshopnotifications');
        $totalItems = $paginator->getTotalItems('workshopnotifications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Notification:index.html.twig', array(
            'notifications' => $notifications,
            'form' => $form->createView(),
            'workshopscheduling' => $workshopscheduling,
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Lists all Workshop Notificationsends to Admin.
     *
     */
    public function workshopAdminNotificationsAction(Request $request, $workshopid)
    {
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        
        $em = $this->getDoctrine()->getManager();
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops', array(
                'workshopid' => $workshopid
            )));
        }
        
        // Recovering data
        
        $type_options = Notification::getListTypeOptions();
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'type' => '', 
            'sequence' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'type' => '', 
            'sequence' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }

        // Recovers Notificationssend
        $query = Notificationsend::getWorkshopListNotificationsends($this->getDoctrine(), $wi['id'], $workshopid, $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopnotifications');
        $paginator->setMaxPagerItems(5, 'workshopnotifications');
        $notifications = $paginator->paginate($query, 'workshopnotifications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopnotifications');
        $endPageItem = $paginator->getEndPageItem('workshopnotifications');
        $totalItems = $paginator->getTotalItems('workshopnotifications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Notification:index.html.twig', array(
            'workshopscheduling' => $workshopscheduling, 
            'notifications' => $notifications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Show Workshop Notificationsends.
     *
     */
    public function workshopNotificationsshowAction($id)
    { 
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wa = $session->get('workshopapplication');
        
        $em = $this->getDoctrine()->getManager();
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($wa['ws_id']);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }

        // Recover Notificationsend
        $entity = Notificationsend::getWorkshopShowNotificationsend($this->getDoctrine(), $wa['ws_id'], $id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación del taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_notifications'));
        }

        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Notification:show.html.twig', array(
            'workshopscheduling' => $workshopscheduling, 
            'entity' => $entity
        ));
    }

    /**
     * Show Workshop Notificationsends to Admin.
     *
     */
    public function workshopAdminNotificationsshowAction($workshopid, $id)
    { 
        $em = $this->getDoctrine()->getManager();
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }
        
        // Recover Notificationsend
        $entity = Notificationsend::getWorkshopShowNotificationsend($this->getDoctrine(), $workshopid, $id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación del taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin_notifications', array(
                'workshopid' => $workshopid
            )));
        }

        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Notification:show.html.twig', array(
            'workshopscheduling' => $workshopscheduling, 
            'entity' => $entity
        ));
    }

    /**
     * Lists all Workshop Activities.
     *
     */
    public function workshopActivitiesAction(Request $request)
    {           
        $em = $this->getDoctrine()->getManager();

        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($wa['ws_id']);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }

        // Recovering data
        
        $period_options = array('day' => 'Días', 'week' => 'Semanas', 'month' => 'Meses');
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        $finished_options = array(0 => 'Pendiente', 1 => 'Enviado');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',  
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'sequence' => '', 
            'finished' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('duration', 'text', array(
                'required' => FALSE
            ))
            ->add('period', 'choice', array(
                'choices' => $period_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('finished', 'choice', array(
                'choices' => $finished_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'sequence' => '', 
            'finished' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }

        // Recovers Workshopapplicationactivity
        $query = Workshopapplicationactivity::getListWorkshopapplicationactivities($this->getDoctrine(), $wi['id'], $wa['id'], $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopactivities');
        $paginator->setMaxPagerItems(5, 'workshopactivities');
        $activities = $paginator->paginate($query, 'workshopactivities')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopactivities');
        $endPageItem = $paginator->getEndPageItem('workshopactivities');
        $totalItems = $paginator->getTotalItems('workshopactivities');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity:index.html.twig', array(
            'activities' => $activities,
            'workshopscheduling' => $workshopscheduling,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Lists all Workshop Polls.
     *
     */
    public function workshopPollsAction(Request $request, $workshopid = '')
    {
        $em = $this->getDoctrine()->getManager();
                
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recovering data
        
        $period_options = array('day' => 'Días', 'week' => 'Semanas', 'month' => 'Meses');
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'word' => '',  
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'sequence' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('duration', 'text', array(
                'required' => FALSE
            ))
            ->add('period', 'choice', array(
                'choices' => $period_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL, 
            'sequence' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            // Recovers Pollapplications
            $query = Pollscheduling::getWorkshopListPollschedulingsAdmin($this->getDoctrine(), $workshopid, $data);
        }
        else {
            $wi = $session->get('workinginformation');
            $wa = $session->get('workshopapplication');
            // Recovers Pollapplications
            $query = Pollscheduling::getWorkshopListPollschedulings($this->getDoctrine(), $wi['id'], $wa['id'], $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshoppolls');
        $paginator->setMaxPagerItems(5, 'workshoppolls');
        $polls = $paginator->paginate($query, 'workshoppolls')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshoppolls');
        $endPageItem = $paginator->getEndPageItem('workshoppolls');
        $totalItems = $paginator->getTotalItems('workshoppolls');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Array Parameters
        
        $array_parameters = array(
            'polls' => $polls,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        );
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            // Recover workshopscheduling
            $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopid);
            if (!$workshopscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
            }
            $array_parameters['workshopscheduling'] = $workshopscheduling;
        } else {
            $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($wa['ws_id']);
            if (!$workshopscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
            }
            $array_parameters['workshopscheduling'] = $workshopscheduling;
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Poll:index.html.twig', $array_parameters);
    }
    
    /**
     * Lists all Workshopscheduling Documents.
     */
    public function workshopDocumentsAction(Request $request)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wa = $session->get('workshopapplication');
        
        // Recovering data
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($wa['ws_id']);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }
        
        $type_options = Workshopdocument::getListTypeOptions();
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'type' => '', 
            'format' => '', 
            'sequence' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('format', 'text', array(
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'type' => '', 
            'format' => '', 
            'sequence' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }

        // Recovers Workshopschedulingdocuments
        $query = Workshopschedulingdocument::getWorkshopListWorkshopschedulingdocuments($this->getDoctrine(), $wa['ws_id'], $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshopdocuments');
        $paginator->setMaxPagerItems(5, 'workshopdocuments');
        $documents = $paginator->paginate($query, 'workshopdocuments')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshopdocuments');
        $endPageItem = $paginator->getEndPageItem('workshopdocuments');
        $totalItems = $paginator->getTotalItems('workshopdocuments');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Document:index.html.twig', array(
            'documents' => $documents,
            'workshopscheduling' => $workshopscheduling,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Show Workshopscheduling Document.
     */
    public function workshopDocumentsShowAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wa = $session->get('workshopapplication');
        
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($wa['ws_id']);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }
        
        // Recover Workshopschedulingdocument
        $entity = Workshopschedulingdocument::getWorkshopShowWorkshopschedulingdocument($this->getDoctrine(), $wa['ws_id'], $id);
        
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el documento del taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_documents_show', array(
                'id' => $id
            )));
        }
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Document:show.html.twig', array(
            'workshopscheduling' => $workshopscheduling,
            'entity' => $entity
        ));
    }
    
    /**
     * Lists all Report - Follow up Activity.
     *
     */
    public function workshopReportActivityAction(Request $request, $workshopschedulingid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        // FIN ACCESS CONTROL
        
        // Find Entities
        
        // Recovering data
        
        $period_options = array('day' => 'Días', 'week' => 'Semanas', 'month' => 'Meses');
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'sequence' => '', 
            'word' => '', 
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('duration', 'text', array(
                'required' => FALSE
            ))
            ->add('period', 'choice', array(
                'choices' => $period_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'sequence' => '', 
            'word' => '', 
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recover Workshopschedulingactivitys
        $query = Workshopschedulingactivity::tracingByWorkshopscheduling($this->getDoctrine(), $workshopschedulingid, $data);

        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'wsas');
        $paginator->setMaxPagerItems(5, 'wsas');
        $wsas = $paginator->paginate($query, 'wsas')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('wsas');
        $endPageItem = $paginator->getEndPageItem('wsas');
        $totalItems = $paginator->getTotalItems('wsas');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Report:follow.html.twig',array(
            'wsas' => $wsas,
            'workshopscheduling' => $workshopscheduling,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }
    
    /**
     * Export all Report - Follow up Activity.
     *
     */
    public function workshopReportActivityExportAction($workshopschedulingid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage', array()));
        }
        // FIN ACCESS CONTROL

        $query = Workshopschedulingactivity::tracingByWorkshopscheduling($this->getDoctrine(), $workshopschedulingid);
        $activities = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Actividades");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta la lista de actividades del taller");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Actividades');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Taller: ' . $workshopscheduling->getName());
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Empresa: ' . $workshopscheduling->getCompany()->getName());
        
        if (!empty($activities)) {
          
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('Nombre');
            $worksheet->getCellByColumnAndRow(2, 5)->setValue('Duración');
            $worksheet->getCellByColumnAndRow(3, 5)->setValue('Inicio');
            $worksheet->getCellByColumnAndRow(4, 5)->setValue('Fin');
            $worksheet->getCellByColumnAndRow(5, 5)->setValue('Avance');
    
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
    
            $row = 6;
    
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            
            foreach($activities as $activity){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($activity['name']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($activity['duration'] . 
                                        $twigExtension->periodNameFilter($activity['period']));
    
                if(!is_null($activity['datein'])){
                    $worksheet->getCellByColumnAndRow(3, $row)->setValue(
                                 PHPExcel_Shared_Date::PHPToExcel($activity['datein']->getTimestamp()));
                    $worksheet->getStyleByColumnAndRow(3, $row)
                                 ->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');
                }
    
                if(!is_null($activity['dateout'])){
                    $worksheet->getCellByColumnAndRow(4, $row)->setValue(
                                 PHPExcel_Shared_Date::PHPToExcel($activity['dateout']->getTimestamp()));
                    $worksheet->getStyleByColumnAndRow(4, $row)
                                 ->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');                
                }
    
                $worksheet->getCellByColumnAndRow(5, $row)->setValue((int)$activity['percentaje'] . '%');
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-actividades.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
    
    /**
     * Print all Report - Follow up Activity.
     *
     */
    public function workshopReportActivityPrintAction($workshopschedulingid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage', array()));
        }
        // FIN ACCESS CONTROL

        $query = Workshopschedulingactivity::tracingByWorkshopscheduling($this->getDoctrine(), $workshopschedulingid);
        $wsas = $query->getResult();
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Report:follow_print.html.twig',array(
            'wsas' => $wsas,
            'workshopscheduling' => $workshopscheduling
        ));
    }

    /** 
     * Show of detail Report - Follow up Activity.
     *
     */
    public function workshopReportActivityShowAction(Request $request, $workshopschedulingactivityid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // recover Workshopschedulingactivity
        $workshopschedulingactivity = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($workshopschedulingactivityid);

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopschedulingactivity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La Actividad que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage', array()));
        }
        // FIN ACCESS CONTROL
        
        // Recovering data
        
        $companyid = $workshopschedulingactivity->getWorkshopscheduling()->getCompany()->getId();
        $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $companyid);
        $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $companyid);
        $finished_options = array(0 => 'Pendiente', 1 => 'Enviado');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'user_names' => '', 
            'user_surname' => '', 
            'user_lastname' => '', 
            'organizationalunit' => '', 
            'charge' => '', 
            'finished' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('user_names', 'text', array(
                'required' => FALSE
            ))
            ->add('user_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('user_lastname', 'text', array(
                'required' => FALSE
            ))
            ->add('organizationalunit', 'choice', array(
                'choices' => $organizationalunit_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('charge', 'choice', array(
                'choices' => $charge_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('finished', 'choice', array(
                'choices' => $finished_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'user_names' => '', 
            'user_surname' => '', 
            'user_lastname' => '', 
            'organizationalunit' => '', 
            'charge' => '', 
            'finished' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recover Workshopapplicationactivities
        $query = Workshopapplicationactivity::getByWorkshopschedulingactivity($this->getDoctrine(), $workshopschedulingactivityid, $data);

        //Paginator
          
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'waas');
        $paginator->setMaxPagerItems(5, 'waas');
        $waas = $paginator->paginate($query, 'waas')->getResult();

        $startPageItem = $paginator->getStartPageItem('waas');
        $endPageItem = $paginator->getEndPageItem('waas');
        $totalItems = $paginator->getTotalItems('waas');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Report:follow_show.html.twig',array(
            'waas' => $waas,
            'workshopschedulingactivity' => $workshopschedulingactivity,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /** 
     * Export of detail Report - Follow up Activity.
     *
     */
    public function workshopReportActivityShowExportAction($workshopschedulingactivityid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // recover Workshopschedulingactivity
        $workshopschedulingactivity = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($workshopschedulingactivityid);

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopschedulingactivity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La Actividad que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage', array()));
        }
        // FIN ACCESS CONTROL
        
        // Recover Workshopapplicationactivities
        $query = Workshopapplicationactivity::getByWorkshopschedulingactivity($this->getDoctrine(), $workshopschedulingactivityid);

        $activities = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Detalle de Actividades");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta las actividades por persona del taller");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Actividades por persona');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Taller: ' . $workshopschedulingactivity->getWorkshopscheduling()->getName());
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Empresa: ' . $workshopschedulingactivity->getWorkshopscheduling()->getCompany()->getName());
        $worksheet->getCellByColumnAndRow(1, 4)->setValue('Actividad: ' . $workshopschedulingactivity->getName());
        
        if (!empty($activities)) {
          
            $worksheet->getCellByColumnAndRow(1, 6)->setValue('Código');
            $worksheet->getCellByColumnAndRow(2, 6)->setValue('Participante');
            $worksheet->getCellByColumnAndRow(3, 6)->setValue('Correo electrónico');
            $worksheet->getCellByColumnAndRow(4, 6)->setValue('Unidad');
            $worksheet->getCellByColumnAndRow(5, 6)->setValue('Cargo');
            $worksheet->getCellByColumnAndRow(6, 6)->setValue('Actividad');
    
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
    
            $row = 7;
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($activities as $activity){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($activity['code']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($activity['surname'] . ' ' . $activity['lastname'] . ', ' . $activity['surname']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($activity['email']);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($activity['couname']);
                $worksheet->getCellByColumnAndRow(5, $row)->setValue($activity['ccname']);
                $worksheet->getCellByColumnAndRow(6, $row)->setValue($twigExtension->finishedNameFilter($activity['finished']));
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 6)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-detalle-actividades.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }

    /** 
     * Print of detail Report - Follow up Activity.
     *
     */
    public function workshopReportActivityShowPrintAction($workshopschedulingactivityid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // recover Workshopschedulingactivity
        $workshopschedulingactivity = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($workshopschedulingactivityid);

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopschedulingactivity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La Actividad que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage', array()));
        }
        // FIN ACCESS CONTROL

        $query = Workshopapplicationactivity::getByWorkshopschedulingactivity(
                     $this->getDoctrine(), $workshopschedulingactivityid);
        $waas = $query->getResult();

        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Report:follow_show_print.html.twig',array(
            'waas' => $waas,
            'workshopschedulingactivity' => $workshopschedulingactivity));
    }
        
    /**
     * Result of the Report Activity.
     *
     */
    public function workshopReportActivityresultAction(Request $request, $workshopschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $workshopapplicationActivity = ''; 
        $postDataIntegrant = '';
        $postDataActivities = '';
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Recover Workshopapplication
        $workshopapplication = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findByWorkshopscheduling($workshopschedulingid);
        
        // Recover Workshopschedulingactivity
        $workshopschedulingActivity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->findByWorkshopscheduling($workshopschedulingid);

        // PostData
        $postDataIntegrant = $request->request->get('options_integrant', null);
        $postDataActivities = $request->request->get('asigned_options', null);

        $waaArray = array();
        $questionArray = array();
        $ids = array();
        $waProfile = '';
        $wi_query = '';
        $wid = '';
                         
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');

        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wid = $session->get('workinginformation');
            $wad = $session->get('workshopapplication');
            if (in_array($wad['profile'], array('integrant', 'collaborator'))) {
                // Recovers Employees Integrant, Collaborator
                $ids = Workinginformation::getIdWorkinginformationEmployeesReportActivities($this->getDoctrine(), $wid['id']);
            }
            elseif ($wad['profile'] == 'boss') {
                // Recovers Employees Integrant, Collaborator
                $ids = Workinginformation::getIdWorkinginformationEmployeesReportActivities($this->getDoctrine(), $wid['id'], TRUE);
            }
            $waProfile = $wad['profile'];
        }
        
        //Reovers Working information
        if (!empty($ids)) {
            $repository = $this->getDoctrine()->getRepository('FishmanAuthBundle:Workinginformation');
            $wi_query = $repository->createQueryBuilder('wi')
                ->select('wi.id, wi.code, u.id uid, u.names, u.surname, u.lastname')
                ->where("wi.id IN (:ids) AND wa.profile = 'integrant' AND wa.workshopscheduling = :workshopscheduling")
                ->innerJoin('wi.user', 'u')
                ->innerJoin('FishmanWorkshopBundle:Workshopapplication', 'wa', 'WITH', 'wa.workinginformation = wi.id')
                ->setParameter('ids', explode(',',$ids))
                ->setParameter('workshopscheduling', $workshopschedulingid)
                ->getQuery()
                ->getResult();
        }

        if (!empty($postDataIntegrant) && !empty($postDataActivities)) {
            //Data to display activities resolved
            foreach ($postDataActivities['data2'] as $key) {
                $workshopapplicationActivity = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')
                  ->findOneBy(array('workshopapplication' => $postDataIntegrant, 'workshopschedulingactivity_id' => $key));
                  //Called to function Activities Type Preprocess
                Workshopapplicationactivity::activitiesTypePreprocess($this->getDoctrine(), $workshopapplicationActivity);
                $waaArray[] = $workshopapplicationActivity;
            }
        }

        if($request->request->get('export', false)){

            $html = $this->renderView('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Report:export.html.twig', array(
                 'workshopscheduling' => $workshopscheduling,
                 'workshopapplication' => $workshopapplication,
                 'workshopscheduling_activity' => $workshopschedulingActivity,
                 'workshopapplication_activity' => $workshopapplicationActivity,
                 'integrant' => $postDataIntegrant,
                 'activities' => $postDataActivities,
                 'waa_array' => $waaArray
            ));
          
            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="reporte_actividades.pdf"',
                    'orientation'=> 'Landscape',
                    'default-header'=> true
                )
            );
            
        }

        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Report:result.html.twig', array(
             'workshopscheduling' => $workshopscheduling,
             'workshopapplication' => $workshopapplication,
             'workshopscheduling_activity' => $workshopschedulingActivity,
             'workshopapplication_activity' => $workshopapplicationActivity,
             'integrant' => $postDataIntegrant,
             'activities' => $postDataActivities,
             'waa_array' => $waaArray,
             'wi_peoples' => $wi_query,
             'wid' => $wid,
             'rol' => $rol,
             'wa_profile' => $waProfile
        ));
    }

    /**
     * Lists all Report - Follow up Poll to Workshop.
     *
     */
    public function workshopReportPollFollowAction(Request $request, $workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        // FIN ACCESS CONTROL
 
        // Recover session varaible
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        
        // Recovering data
        
        $period_options = array('day' => 'Días', 'week' => 'Semanas', 'month' => 'Meses');
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'sequence' => '', 
            'word' => '', 
            'resolved' => '',
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('resolved', 'text', array(
                'required' => FALSE
            ))
            ->add('duration', 'text', array(
                'required' => FALSE
            ))
            ->add('period', 'choice', array(
                'choices' => $period_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'sequence' => '', 
            'word' => '', 
            'resolved' => '',
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollschedulings
            $query = Pollscheduling::getWorkshopListPollschedulingsFollow($this->getDoctrine(), $wsId, '', $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshoppolls');
        $paginator->setMaxPagerItems(5, 'workshoppolls');
        $polls = $paginator->paginate($query, 'workshoppolls')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshoppolls');
        $endPageItem = $paginator->getEndPageItem('workshoppolls');
        $totalItems = $paginator->getTotalItems('workshoppolls');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Poll/Report:follow.html.twig', array(
            'workshopscheduling' => $workshopscheduling, 
            'polls' => $polls,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Export all Report - Follow up Poll to Workshop.
     *
     */
    public function workshopReportPollFollowExportAction($workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        // FIN ACCESS CONTROL
         
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollschedulings
            $query = Pollscheduling::getWorkshopListPollschedulingsFollow($this->getDoctrine(), $wsId);
        }
        
        $polls = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Encuestas");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta la lista de encuestas del taller");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Encuestas');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Taller: ' . $workshopscheduling->getName());
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Empresa: ' . $workshopscheduling->getCompany()->getName());

        if (!empty($polls)) {
          
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('Nombre');
            $worksheet->getCellByColumnAndRow(2, 5)->setValue('Resuelto');
            $worksheet->getCellByColumnAndRow(3, 5)->setValue('Duración');
            $worksheet->getCellByColumnAndRow(4, 5)->setValue('Inicio');
            $worksheet->getCellByColumnAndRow(5, 5)->setValue('Fin');
            $worksheet->getCellByColumnAndRow(6, 5)->setValue('Avance');
    
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
    
            $row = 6;
    
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($polls as $poll){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($poll['title']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($poll['time_number']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($poll['duration'] . 
                                        $twigExtension->periodNameFilter($poll['period']));
    
                if(!is_null($poll['initdate'])){
                    $worksheet->getCellByColumnAndRow(4, $row)->setValue(
                                 PHPExcel_Shared_Date::PHPToExcel($poll['initdate']->getTimestamp()));
                    $worksheet->getStyleByColumnAndRow(4, $row)
                                 ->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');
                }
    
                if(!is_null($poll['enddate'])){
                    $worksheet->getCellByColumnAndRow(5, $row)->setValue(
                                 PHPExcel_Shared_Date::PHPToExcel($poll['enddate']->getTimestamp()));
                    $worksheet->getStyleByColumnAndRow(5, $row)
                                 ->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');                
                }
    
                $worksheet->getCellByColumnAndRow(6, $row)->setValue((int)$poll['percentaje'] . '%');
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-encuestas-taller.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }

    /**
     * Print all Report - Follow up Poll.
     *
     */
    public function workshopReportPollFollowPrintAction($workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $polls = '';
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        // FIN ACCESS CONTROL
 
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        $rol = $session->get('currentrol');

        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'), 'ROLE_TEACHER') || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollschedulings
            $query = Pollscheduling::getWorkshopListPollschedulingsFollow($this->getDoctrine(), $wsId);
        }
        
        if (!empty($query)) {
            $polls = $query->getResult();
        }
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Poll/Report:follow_print.html.twig', array(
            'polls' => $polls,
            'workshopscheduling' => $workshopscheduling
        ));
    }
    
    /**
     * Show of detail Report - Follow up Poll to Workshop.
     *
     */
    public function workshopReportPollFollowShowAction(Request $request, $pollid, $workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getWorkshopCurrentPollschedulingFollow($this->getDoctrine(), $workshopschedulingid, $pollid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_report_poll_notevaluateds_follow_show', array(
                    'pollid' => $pollid
                )));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin_report_poll_notevaluateds_follow_show', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollid' => $pollid
                )));
            }
        }
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if($pollscheduling['deleted'] == 1){
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        // FIN ACCESS CONTROL
        
        // Recover session varaible
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        
        // Find Entities
        
        $defaultData = array(
            'evaluated_code' => '', 
            'evaluated_names' => '', 
            'evaluated_surname' => '', 
            'evaluated_lastname' => '', 
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('evaluated_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_lastname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_lastname', 'text', array(
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'evaluated_code' => '', 
            'evaluated_names' => '', 
            'evaluated_surname' => '', 
            'evaluated_lastname' => '', 
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recover Pollscheduling
        $pollscheduling = Pollscheduling::getWorkshopCurrentPollschedulingFollow($this->getDoctrine(), $workshopschedulingid, $pollid);
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid, '', $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshoppollapplications');
        $paginator->setMaxPagerItems(5, 'workshoppollapplications');
        $pollapplications = $paginator->paginate($query, 'workshoppollapplications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshoppollapplications');
        $endPageItem = $paginator->getEndPageItem('workshoppollapplications');
        $totalItems = $paginator->getTotalItems('workshoppollapplications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return

        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Poll/Report:follow_show.html.twig', array(
            'workshopscheduling' => $workshopscheduling,  
            'poll' => $pollscheduling, 
            'pollapplications' => $pollapplications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }
    
    /**
     * Export of detail Report - Follow up Poll to Workshop.
     *
     */
    public function workshopReportPollFollowShowExportAction($pollid, $workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getWorkshopCurrentPollschedulingFollow($this->getDoctrine(), $workshopschedulingid, $pollid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_report_poll_notevaluateds_follow_show_export', array(
                    'pollid' => $pollid
                )));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin_report_poll_notevaluateds_follow_show_export', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollid' => $pollid
                )));
            }
        }
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if($pollscheduling['deleted'] == 1){
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando expotar ha sido borrada');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        // FIN ACCESS CONTROL
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid);
        }
        
        $pollapplications = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Detalle de Encuesta");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta las evaluaciones de la encuesta del taller");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Evaluaciones');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Taller: ' . $workshopscheduling->getName());
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Empresa: ' . $workshopscheduling->getCompany()->getName());
        $worksheet->getCellByColumnAndRow(1, 4)->setValue('Encuesta: ' . $pollscheduling['title'] . ' | ' . $pollscheduling['pollschedulingperiod']);
        
        if (!empty($pollapplications)) {
          
            $worksheet->getCellByColumnAndRow(1, 6)->setValue('Código Evaluado');
            $worksheet->getCellByColumnAndRow(2, 6)->setValue('Evaluado');
            $worksheet->getCellByColumnAndRow(3, 6)->setValue('Correo Evaluado');
            $worksheet->getCellByColumnAndRow(4, 6)->setValue('Código Evaluador');
            $worksheet->getCellByColumnAndRow(5, 6)->setValue('Evaluador');
            $worksheet->getCellByColumnAndRow(6, 6)->setValue('Correo Evaluador');
            $worksheet->getCellByColumnAndRow(7, 6)->setValue('Encuesta');
            
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
    
            $row = 7;
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($pollapplications as $pa){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($pa['evaluated_code']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($pa['evaluated_surname'] . ' ' . $pa['evaluated_lastname'] . ', ' . $pa['evaluated_names']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($pa['evaluated_email']);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($pa['evaluator_code']);
                $worksheet->getCellByColumnAndRow(5, $row)->setValue($pa['evaluator_surname'] . ' ' . $pa['evaluator_lastname'] . ', ' . $pa['evaluator_names']);
                $worksheet->getCellByColumnAndRow(6, $row)->setValue($pa['evaluator_email']);
                $worksheet->getCellByColumnAndRow(7, $row)->setValue($twigExtension->finishedNameFilter($pa['finished']));
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 6)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-detalle-encuesta-taller.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
    
    /**
     * Print of detail Report - Follow up Poll to Workshop.
     *
     */
    public function workshopReportPollFollowShowPrintAction($pollid, $workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getWorkshopCurrentPollschedulingFollow($this->getDoctrine(), $workshopschedulingid, $pollid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_report_poll_notevaluateds_follow_show_print', array(
                    'pollid' => $pollid
                )));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin_report_poll_notevaluateds_follow_show_print', array(
                    'workshopschedulingid' => $workshopschedulingid, 
                    'pollid' => $pollid
                )));
            }
        }
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if($pollscheduling['deleted'] == 1){
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        // FIN ACCESS CONTROL
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid);
        }
        
        $pollapplications = $query->getResult();

        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Poll/Report:follow_show_print.html.twig', array(
            'pollapplications' => $pollapplications, 
            'poll' => $pollscheduling, 
            'workshopscheduling' => $workshopscheduling
        ));
    }
    
    /**
     * Show of detail Report - Follow up Poll to Workshop.
     *
     */
    public function workshopReportPollNotEvaluatedsFollowShowAction(Request $request, $pollid, $workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getWorkshopCurrentPollschedulingFollow($this->getDoctrine(), $workshopschedulingid, $pollid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if($pollscheduling['deleted'] == 1){
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        
        // FIN ACCESS CONTROL
        
        // Recover session varaible
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        
        // Find Entities
        
        $defaultData = array(
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('evaluator_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_lastname', 'text', array(
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid, $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'workshoppollapplications');
        $paginator->setMaxPagerItems(5, 'workshoppollapplications');
        $pollapplications = $paginator->paginate($query, 'workshoppollapplications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('workshoppollapplications');
        $endPageItem = $paginator->getEndPageItem('workshoppollapplications');
        $totalItems = $paginator->getTotalItems('workshoppollapplications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Poll/Report/EvaluatorNotEvaluateds:follow_show.html.twig', array(
            'workshopscheduling' => $workshopscheduling,  
            'poll' => $pollscheduling, 
            'pollapplications' => $pollapplications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }
    
    /**
     * Export of detail Report - Follow up Poll to Workshop.
     *
     */
    public function workshopReportPollNotEvaluatedsFollowShowExportAction($pollid, $workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getWorkshopCurrentPollschedulingFollow($this->getDoctrine(), $workshopschedulingid, $pollid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if($pollscheduling['deleted'] == 1){
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando expotar ha sido borrada');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        
        // FIN ACCESS CONTROL
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid);
        }
        
        $pollapplications = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Detalle de Encuesta");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta las evaluaciones de la encuesta del taller");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Evaluaciones');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Taller: ' . $workshopscheduling->getName());
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Empresa: ' . $workshopscheduling->getCompany()->getName());
        $worksheet->getCellByColumnAndRow(1, 4)->setValue('Encuesta: ' . $pollscheduling['title'] . ' | ' . $pollscheduling['pollschedulingperiod']);
        
        if (!empty($pollapplications)) {
            
            $worksheet->getCellByColumnAndRow(1, 6)->setValue('Código Evaluador');
            $worksheet->getCellByColumnAndRow(2, 6)->setValue('Evaluador');
            $worksheet->getCellByColumnAndRow(3, 6)->setValue('Correo Evaluador');
            $worksheet->getCellByColumnAndRow(4, 6)->setValue('Encuesta');
            
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
    
            $row = 7;
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($pollapplications as $pa){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($pa['evaluator_code']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($pa['evaluator_surname'] . ' ' . $pa['evaluator_lastname'] . ', ' . $pa['evaluator_names']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($pa['evaluator_email']);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($twigExtension->finishedNameFilter($pa['finished']));
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 6)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-detalle-encuesta-taller-sin-evaluados.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
    
    /**
     * Print of detail Report - Follow up Poll to Workshop.
     *
     */
    public function workshopReportPollNotEvaluatedsFollowShowPrintAction($pollid, $workshopschedulingid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }

        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getWorkshopCurrentPollschedulingFollow($this->getDoctrine(), $workshopschedulingid, $pollid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            if (in_array($wa['profile'], array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
        }
        if($workshopscheduling->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'La programación de taller que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if($pollscheduling['deleted'] == 1){
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando expotar ha sido borrada');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop'));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_admin', array(
                    'workshopid' => $workshopschedulingid
                )));
            }
        }
        
        // FIN ACCESS CONTROL
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $wsId = $workshopschedulingid;
            }
            else {
                $wsId = $wa['ws_id'];
            }
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid);
        }
        
        $pollapplications = $query->getResult();

        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Poll/Report/EvaluatorNotEvaluateds:follow_show_print.html.twig', array(
            'pollapplications' => $pollapplications, 
            'poll' => $pollscheduling, 
            'workshopscheduling' => $workshopscheduling
        ));
    }
    
    /**
     * Result of the Report Poll.
     *
     */
    public function workshopReportPollResultAction(Request $request, $workshopschedulingid = '')
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }
        
        $messageError = '';
        $messagePollscheduling = '';
        
        $polls = '';
        $pollschedulings = '';
        $evaluateds = '';
        $levels = array(
            '1' => 'Nivel 1', 
            '2' => 'Nivel 2', 
            '3' => 'Nivel 3', 
            '4' => 'Nivel 4', 
            '5' => 'Nivel 5', 
            '6' => 'Nivel 6', 
            '7' => 'Nivel 7', 
            '8' => 'Nivel 8', 
            '9' => 'Nivel 9',
            'null' => 'Ninguno' 
        );
        $pollapplicationquestions = '';
        $ids = '';
        
        $postDataPoll = ''; 
        $postDataPollschedulings = '';
        $postDataEvaluated = '';
        $postDataLevel = '';
        $profileTemp = '';
        
        // PostData
        $postDataPoll = $request->request->get('options_poll', null);
        $postDataPollschedulings = $request->request->get('asigned_options', null);
        $postDataEvaluated = $request->request->get('options_evaluated', null);
        $postDataLevel = $request->request->get('options_level', null);

        if (isset($wa)) {
            if (in_array($wa['profile'], array('company'))) {
                $profileTemp = 'company';
            }
        }
        
        // Recover Workshopscheduling
        $workshopscheduling = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        $companyId = $workshopscheduling->getCompany()->getId();

        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('responsible', 'company'))) {
            // Recovers Polls
            $polls = Pollscheduling::getListPollsByProfile($this->getDoctrine(), $workshopschedulingid, $companyId);
        }
        elseif (in_array($wa['profile'], array('integrant', 'collaborator'))) {
            // Recovers Employees Integrant, Collaborator
            $ids = Workinginformation::getIdWorkinginformationEmployees($this->getDoctrine(), $wi['id']);
            if ($ids != '') {
                $ids = $wi['id'] . ', ' . $ids;
            }
            else {
                $ids = $wi['id'];
            }
            // Recovers Polls
            $polls = Pollscheduling::getListPollsByProfile($this->getDoctrine(), $workshopschedulingid, $companyId, $ids);
        }
        elseif ($wa['profile'] == 'boss') {
            // Recovers Employees Integrant, Collaborator
            $ids = Workinginformation::getIdWorkinginformationEmployees($this->getDoctrine(), $wi['id'], TRUE);
            if ($ids != '') {
                $ids = $wi['id'] . ', ' . $ids;
            }
            else {
                $ids = $wi['id'];
            }
            // Recovers Polls
            $polls = Pollscheduling::getListPollsByProfile($this->getDoctrine(), $workshopschedulingid, $companyId, $ids);
        }
        
        if (!empty($postDataPoll)) {
            
            // Recovers Pollschedulings
            $pollschedulings = Pollscheduling::getListPollschedulingsByProfile($this->getDoctrine(), $workshopschedulingid, $postDataPoll, $companyId, $ids);
 
            $pscIds = '';
            if (!empty($postDataPollschedulings['data2'])) {
                $j = 0;
                foreach ($postDataPollschedulings['data2'] as $psc) {
                    if ($j == 0) {
                        $pscIds = $psc;
                    }
                    else {
                        $pscIds = $pscIds . ', ' .$psc;
                    }
                    $j++;
                }
            }
            
            // Recovers Evaluateds
            $evaluateds = Pollapplication::getListPollschedulingEvaluatedsByProfile($this->getDoctrine(), $workshopschedulingid, $pscIds, $companyId, $ids);
        }

        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Array paramteres
        $array_parameters = array(
            'workshopscheduling' => $workshopscheduling,
            'pollapplicationquestions' => $pollapplicationquestions, 
            'polls' => $polls, 
            'rol' => $rol, 
            'profile' => $profileTemp, 
            'pollschedulings' => $pollschedulings,
            'evaluateds' => $evaluateds,
            'levels' => $levels, 
            'pollData' => $postDataPoll,
            'pollschedulingsData' => $postDataPollschedulings, 
            'evaluatedData' => $postDataEvaluated, 
            'levelData' => $postDataLevel, 
            'pollschedulingids' => '',
            'ids' => $ids
        );
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $array_parameters['workshopschedulingid'] = $wa['ws_id'];
        }
        else {
            $array_parameters['workshopschedulingid'] = $workshopschedulingid;
        }
        
        if (!empty($postDataPollschedulings) && !empty($postDataEvaluated) && !empty($postDataLevel)) {
            $i = 0;
            $arrayPoll = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $postDataPoll);
            
            if ($arrayPoll) {
                
                $arrayGroupPoll['sections'] = Pollsection::arrayGroupQuestions($this->getDoctrine(), 0, $postDataLevel, $arrayPoll);
                
                // Recover pollscheduling ids
                $p = 0;
                $psc = 0;
                $pscIds = '';
                
                foreach ($postDataPollschedulings['data2'] as $key) {
                    
                    // Recover Pollscheduling
                    $pollschedulingKey = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($key);
                    
                    // Check for participating in the survey
                    $participatingPoll = Pollapplication::checkParticipatingPoll($this->getDoctrine(), $key, $postDataEvaluated);
                    
                    // Verify number of times the assessed evaluated
                    $numberResolvedPolls = Pollapplication::getNumberResolvedPolls($this->getDoctrine(), $key, $postDataEvaluated);
                    
                    if ($participatingPoll) {
                        if (($numberResolvedPolls >= $pollschedulingKey->getAccessViewNumber() 
                            && $pollschedulingKey->getAccessViewNumber() > 0)  
                            || $pollschedulingKey->getMultipleEvaluation() 
                            || $postDataEvaluated != $wi['id']) {
                            if ($p == 0) {
                                $pscIds = $key;
                            }
                            else {
                                $pscIds .= ', ' . $key;
                            }
                            $p++;
                        }
                        else {
                            $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO DISPONIBLE)' . '</br>';
                            
                            unset($postDataPollschedulings['data2'][$psc]);
                        }
                    }
                    else {
                        $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO PARTICIPA)' . '</br>';
                        
                        unset($postDataPollschedulings['data2'][$psc]);
                    }
                    $psc++;
                }

                $array_parameters['pollschedulingids'] = $pscIds;
                
                if (!empty($postDataPollschedulings['data2'])) {
                    
                    //Data to display polls resolved
                    foreach ($postDataPollschedulings['data2'] as $key) {
                        
                        $array =  $arrayGroupPoll;
                        
                        // Recover Pollapplicationquestions
                        $pollapplicationquestions = Pollapplicationquestion::getResultPollapplicationquestionByPollscheduling($this->getDoctrine(), $postDataEvaluated, $key);
                        
                        if (!empty($pollapplicationquestions)) {
                            
                            $pscs[$i] = $key;
                            
                            $pollschedulingReport = Pollscheduling::calculatePercentageSection($array, $pollapplicationquestions);
                            
                            $pollschedulingTotals[$i] = array(
                                'percentage' => $pollschedulingReport['percentage']['consolidate'], 
                                'count' => $pollschedulingReport['percentage']['count']
                            );
                            
                            if ($postDataLevel != 'null') {
                                $pollschedulingPercentages[$i] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections'], TRUE);
                            }
                            else {
                                $pollschedulingPercentages[$i] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections']);
                            }
                            
                            // Recover Number Evaluators by Evaluated
                            $numberEvaluators[$i] = Pollapplication::getNumberEvaluatorsByEvaluated($this->getDoctrine(), $key, $postDataEvaluated);
                            
                            // Ranking Top 25%
                            $rankingTop25[$i] = Pollapplicationquestion::getResultPollapplicationquestionTopAverage($this->getDoctrine(), 25, $key, $array);
                            
                            $i++;
                        }
                        else {
                            $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO HA SIDO EVALUADO)' . '</br>';
                        }
        
                    }
                    
                }
                
                if (!empty($pollapplicationquestions)) {
                    
                    // Headers
                    $k = 0;
                    if (!empty($pscs)) {
                        foreach ($pscs as $psc) {
                            foreach ($pollschedulings as $ps) {
                                if ($psc == $ps['id']) {
                                    $pollschedulingHeaders[$k] = 'Encuesta ' . $ps['key'];
                                }
                            }
                            $k++;
                        }
                    }
        
                    // Titles
                            
                    if ($postDataLevel != 'null') {
                        $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections'], TRUE);
                    }
                    else {
                        $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections']);
                    }
                    
                    // Count report vertical
                    $countVertical = count($pollschedulingPercentages[0]);
                    
                    // Count report horizontal
                    $countHorizontal = $i;
                    
                    // Total Rows
                    for ($j = 0; $j < $countVertical; $j++) {
                        $percentageTotalRow = 0;
                        for ($n = 0; $n < $countHorizontal; $n++) {
                            $percentageTotalRow += $pollschedulingPercentages[$n][$j]['percentage'];
                        }
                        $pollschedulingPercentageRows[$j] = $percentageTotalRow / $countHorizontal;
                    }
                    
                    // Row Results
                    $row_total = 0;
                    foreach ($pollschedulingTotals as $total) {
                        $row_total += $total['percentage'];
                    }
                    $pollschedulingTotalRows = $row_total / $countHorizontal;
                    
                    // Total Ranking Top 25%
                    $pscIds = Pollscheduling::getPollschedulingIdsByPoll($this->getDoctrine(), $postDataPoll, $companyId);
                    $rankingTop25Total = Pollapplicationquestion::getResultPollapplicationquestionTopAverage($this->getDoctrine(), 25, $pscIds, $array);
                    
                    $array_parameters['header_results'] = $pollschedulingHeaders;
                    $array_parameters['title_results'] = $pollschedulingTitles;
                    $array_parameters['number_evaluators'] = $numberEvaluators;
                    $array_parameters['report_results'] = $pollschedulingPercentages;
                    $array_parameters['report_rows'] = $pollschedulingPercentageRows;
                    $array_parameters['total_results'] = $pollschedulingTotals;
                    $array_parameters['row_results'] = $pollschedulingTotalRows;
                    $array_parameters['count_horizontal'] = $countHorizontal;
                    $array_parameters['count_vertical'] = $countVertical;
                    $array_parameters['ranking_top'] = $rankingTop25;
                    $array_parameters['ranking_top_total'] = $rankingTop25Total;
                    
                }
                
                if ($messagePollscheduling != '') {
                    $messageError = $messagePollscheduling;
                }
                
            }
            else {
                $messageError = 'Actualmente la encuesta está sin preguntas.';
            }
            
        }
        
        if ($messageError != '') {
            $session->getFlashBag()->add('error', $messageError);
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Poll/Report:result.html.twig', $array_parameters);
    }
    
    /**
     * Export Result of the Report Poll.
     *
     */
    public function workshopReportPollResultExportAction($pollschedulingids, $evaluatedid, $workshopschedulingid = '')
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $wa = $session->get('workshopapplication');
            $workshopschedulingid = $wa['ws_id'];
        }
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        if (!$workshopscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar el taller.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshops'));
        }
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($wa['profile'], array('boss', 'responsible', 'company'))) {
        
            if (!empty($pollschedulingids) && !empty($evaluatedid)) {
                
                $pscId = explode(', ',  $pollschedulingids);
              
                // Recover Workshopscheduling
                $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pscId[0]);
                if (!$pollscheduling) {
                    $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta del taller.');
                    if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                        return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls'));
                    }
                    else {
                        return $this->redirect($this->generateUrl('fishman_front_end_workshop_polls_admin', array(
                            'workshopid' => $workshopschedulingid
                        )));
                    }
                }
                
                // Recovers Pollapplicationreports
                $pollapplicationreports = Pollapplicationreport::getListPollapplicationreportResults($this->getDoctrine(), $pollschedulingids, $evaluatedid);
                
                $content = '';
                $response = new Response();
                
                $content .= 'Reporte:' . ';' . 'Reporte de resultado de Enecuesta' . "\r\n";
                $content .= 'Empresa:' . ';' . $workshopscheduling->getCompany()->getName() . "\r\n";
                $content .= 'Taller: ' . ';' . $workshopscheduling->getName();
                $content .= 'Encuesta:' . ';' . $pollscheduling->getTitle() . ';' . $pollscheduling->getVersion() . ' | ' . $pollscheduling->getPollschedulingPeriod() . "\r\n";
                $content .= "\r\n";
                
                if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                    $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $workshopscheduling->getCompany());
                }
                else {
                    $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $workshopscheduling->getCompany(), FALSE);
                }
                
                $response->setContent($content);
                
                $response->headers->set('Content-Type', 'text/csv');
                $response->headers->set('Content-Disposition', 'attachment;filename="reporte-resultado-encuesta-taller.csv"');
                
                return $response;
            }
        }
        exit;
    }

    /**
     * Lists all Pollapplications.
     *
     */
    public function pollsAction(Request $request)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'resolved' => '', 
                'company' => '', 
                'initdate' => NULL, 
                'enddate' => NULL
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('resolved', 'text', array(
                    'required' => FALSE
                ))
                ->add('company', 'text', array(
                    'required' => FALSE
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('enddate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'resolved' => '', 
                'company' => '', 
                'initdate' => NULL, 
                'enddate' => NULL
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Recovers Pollapplications
            $query = Pollscheduling::getListPollschedulings($this->getDoctrine(), $data, $rol);
        }
        else {
          
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'company' => '', 
                'initdate' => NULL, 
                'enddate' => NULL
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('company', 'text', array(
                    'required' => FALSE
                ))
                ->add('initdate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->add('enddate', 'date', array(
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'company' => '', 
                'initdate' => NULL, 
                'enddate' => NULL
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            // Recovers Pollapplications
            $query = Pollscheduling::getListPollschedulingsIntegrant($this->getDoctrine(), $wi['id'], $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'polls');
        $paginator->setMaxPagerItems(5, 'polls');
        $polls = $paginator->paginate($query, 'polls')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('polls');
        $endPageItem = $paginator->getEndPageItem('polls');
        $totalItems = $paginator->getTotalItems('polls');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll:index.html.twig', array(
            'polls' => $polls,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Home of Poll.
     *
     */
    public function pollAction($pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible workshopapplication
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        $profile = '';
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
              	
                // Recover Workshopapplication
                
                $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $workshopapplication_query = $repository->createQueryBuilder('wa')
                    ->select('wa.id, wa.profile')
                    ->where('wa.workinginformation = :workinginformation
                            AND wa.workshopscheduling = :workshopscheduling
                            AND wa.deleted = :deleted')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('workshopscheduling', $pollscheduling->getEntityId())
                    ->setParameter('deleted', FALSE)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($workshopapplication_query);
                
                $profile = $entity_application['profile'];
                
            }
            else {
    
                // Recover Pollschedulingpeople
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $pollschedulingpeople_query = $repository->createQueryBuilder('psp')
                    ->select('psp.id, psp.profile')
                    ->where('psp.workinginformation = :workinginformation
                            AND psp.pollscheduling = :pollscheduling')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('pollscheduling', $pollid)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($pollschedulingpeople_query);
                
                $profile = $entity_application['profile'];
                
            }
    
            // ACCESS CONTROL
            if($pollscheduling->getDeleted()){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_homepage', array(
                    'pollid' => $pollid
                )));
            }
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_homepage', array(
                    'pollid' => $pollid
                )));
            }
            // FIN ACCESS CONTROL
            
        }
        
        // Recovers Integrant last 2 Notificationssend
        $notifications = Notificationsend::getPollLastTwoNotificationsends($this->getDoctrine(), $wi['id'], $pollscheduling->getId());

        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling, 
            'company' => $company, 
            'notifications' => $notifications, 
            'profile' => $profile
        );

        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        //Return
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            return $this->render('FishmanFrontEndBundle:Dashboard/Index:dashboard_poll_admin.html.twig', $array_parameters);
        } else {
            return $this->render('FishmanFrontEndBundle:Dashboard/Index:dashboard_poll_profile.html.twig', $array_parameters);
        } 
    }

    /**
     * Lists all Poll Integrants.
     *
     */
    public function pollIntegrantsAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        $profile = '';
        
        // Recover Pollscheduling

        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        elseif ($pollscheduling->getType() == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
          
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
              
                $wa = $session->get('workshopapplication');
                
                // Recover Workshopapplication
                
                $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $workshopapplication_query = $repository->createQueryBuilder('wa')
                    ->select('wa.id, wa.profile')
                    ->where('wa.workinginformation = :workinginformation
                            AND wa.workshopscheduling = :workshopscheduling
                            AND wa.deleted = :deleted')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('workshopscheduling', $wa['ws_id'])
                    ->setParameter('deleted', FALSE)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($workshopapplication_query);
                
                $profile = $entity_application['profile'];
                
            }
            else {
    
                // Recover Pollschedulingpeople
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $pollschedulingpeople_query = $repository->createQueryBuilder('psp')
                    ->select('psp.id, psp.profile')
                    ->where('psp.workinginformation = :workinginformation
                            AND psp.pollscheduling = :pollscheduling')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('pollscheduling', $pollid)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($pollschedulingpeople_query);
                
                $profile = $entity_application['profile'];
                
            }
    
            //ACCESS CONTROL
            
            if(empty($pollscheduling) || $pollscheduling->getDeleted()){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
            if (in_array($profile, array('integrant', 'collaborator'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
            
            // Validate Date in Poll
            $validateDate = Pollscheduling::validateDateInPollscheduling($this->getDoctrine(), $pollscheduling->getInitdate(), $pollscheduling->getEnddate());
            if (!$validateDate['valid']) {
                $session->getFlashBag()->add('error', $validateDate['message']);
                return $this->redirect($this->generateUrl('fishman_front_end_polls'));
            }
            
            //FIN - ACCESS CONTROL
            
        }
        
        // Recovering data
        
        $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $pollscheduling->getCompanyId());
        $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $pollscheduling->getCompanyId());
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'organizationalunit' => '', 
            'charge' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('organizationalunit', 'choice', array(
                'choices' => $organizationalunit_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('charge', 'choice', array(
                'choices' => $charge_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'organizationalunit' => '', 
            'charge' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('responsible', 'company'))) {
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsIntegrants($this->getDoctrine(), $wi['id'], $pollid, '', $data);
        }
        elseif (in_array($profile, array('integrant', 'collaborator'))) {
            // Recovers Employees Integrant, Collaborator
            $ids = Workinginformation::getIdWorkinginformationEmployees($this->getDoctrine(), $wi['id']);
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsIntegrants($this->getDoctrine(), $wi['id'], $pollid, $ids, $data);
        }
        elseif ($profile == 'boss') {
            // Recovers Employees Integrant, Collaborator
            $ids = Workinginformation::getIdWorkinginformationEmployees($this->getDoctrine(), $wi['id'], TRUE);
            if ($ids != '') {
                $ids = $wi['id'] . ', ' . $ids;
            }
            else {
                $ids = $wi['id'];
            }
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsIntegrants($this->getDoctrine(), $wi['id'], $pollid, $ids, $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollsintegrants');
        $paginator->setMaxPagerItems(5, 'pollsintegrants');
        $pollapplications = $paginator->paginate($query, 'pollsintegrants')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollsintegrants');
        $endPageItem = $paginator->getEndPageItem('pollsintegrants');
        $totalItems = $paginator->getTotalItems('pollsintegrants');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Array Parameters

        $array_parameters = array(
            'pollapplications' => $pollapplications,
            'poll' => $pollscheduling,
            'company' => $company, 
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'profile' => $profile,
            'form_data' => $formData
        );

        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            $rol = $session->get('currentrol');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll:integrants.html.twig', $array_parameters);
    }

    /**
     * Lists all Poll Integrants.
     *
     */
    public function pollEvaluatedsAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $wi = $session->get('workinginformation');
        
        $personaltoken = '';
        if (isset($_GET['personaltoken'])) {
            $personaltoken = $_GET['personaltoken'];
        }
        $profile = '';
        $securityContext = $this->container->get('security.context');
        $logged = $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        if ($logged) {
            
            $wiId = $wi['id'];
          
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                
                $workshopschedulingid = $pollscheduling->getEntityId();
                
                // Recover Workshopapplication
                $entity_application = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
                    'workinginformation' => $wi['id'],
                    'workshopscheduling' => $workshopschedulingid, 
                    'deleted' => 0
                ));
                if (!empty($entity_application)) {
                    $profile = $entity_application->getProfile();
                }
            }
            else {
                // Recover Pollschedulingpeople
                $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                    'workinginformation' => $wi['id'],
                    'pollscheduling' => $pollid
                ));
                if (!empty($entity_application)) {
                    $profile = $entity_application->getProfile();
                }
            }

        }
        else {
            
            $entity_application = '';
            
            if (strlen($personaltoken) == 64) {
                if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                    // Recover Workshopapplication
                    $entity_application = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
                        'personaltoken' => $personaltoken,
                        'workshopscheduling' => $pollscheduling->getEntityId(), 
                        'deleted' => 0
                    ));
                }
                else {
                    // Recover Pollschedulingpeople
                    $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                        'personaltoken' => $personaltoken,
                        'pollscheduling' => $pollscheduling->getId()
                    ));
                }
            }
            
            if (!empty($entity_application)) {  
                $wiId = $entity_application->getWorkinginformation()->getId();
                $profile = $entity_application->getProfile();
            }
            else {
                return $this->redirect($this->generateUrl('_welcome'));
            }
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // ACCESS CONTROL
        
        if($pollscheduling->getDeleted()){
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        
        if (empty($entity_application)) {
            $session->getFlashBag()->add('error', 'No existen personas a evaluar');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        
        // Validate Date in Poll
        $validateDate = Pollscheduling::validateDateInPollscheduling($this->getDoctrine(), $pollscheduling->getInitdate(), $pollscheduling->getEnddate());
        if (!$validateDate['valid']) {
            $session->getFlashBag()->add('error', $validateDate['message']);
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        // FIN - ACCESS CONTROL
        
        // If Pollscheduling type is "not_evaluateds" redifrect to poll resolve
        
        if ($pollscheduling->getType() == 'not_evaluateds') {
            
            $pollapplication = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(
                array(
                    'pollscheduling' => $pollid, 
                    'evaluator_id' => $wiId,
                    'evaluated_id' => NULL, 
                    'deleted' => FALSE
                )
            );
            
            if (!empty($pollapplication)) {
                $array_parameters = array(
                    'pollid' => $pollid, 
                    'pollapplicationid' => $pollapplication->getId()
                );
                if (!$logged) {
                    $array_parameters['token'] = $pollapplication->getToken();
                }
                
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
            }
            else {
                $session->getFlashBag()->add('error', 'No hay evaluación pendiente');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
            
        }
        
        // Recovers All Pollapplications
        $all_query = Pollapplication::getListPollapplicationsEvaluateds($this->getDoctrine(), $wiId, $pollid);
        $allPollapplications = $all_query->getResult();
        
        // Recovering data
        
        $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $pollscheduling->getCompanyId());
        $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $pollscheduling->getCompanyId());
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'organizationalunit' => '', 
            'charge' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('organizationalunit', 'choice', array(
                'choices' => $organizationalunit_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('charge', 'choice', array(
                'choices' => $charge_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'organizationalunit' => '', 
            'charge' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovers All Pollapplications
        $all_query = Pollapplication::getListPollapplicationsEvaluateds($this->getDoctrine(), $wiId, $pollid);
        $allPollapplications = $all_query->getResult();
        
        // Recovers Pollapplications
        $query = Pollapplication::getListPollapplicationsEvaluateds($this->getDoctrine(), $wiId, $pollid, '', $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollsintegrants');
        $paginator->setMaxPagerItems(5, 'pollsintegrants');
        $pollapplications = $paginator->paginate($query, 'pollsintegrants')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollsintegrants');
        $endPageItem = $paginator->getEndPageItem('pollsintegrants');
        $totalItems = $paginator->getTotalItems('pollsintegrants');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        if (count($allPollapplications) == 1 && $data['word'] == '' && $data['organizationalunit'] == '' && $data['charge'] == '') {
            $pollapplication = current($allPollapplications);
            
            $array_parameters = array(
                'pollid' => $pollid, 
                'pollapplicationid' => $pollapplication['id']
            );
            if (!$logged) {
                $array_parameters['token'] = $pollapplication['token'];
            }
            
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
        }
        
        // Array Parameters
        
        $array_parameters = array(
            'pollapplications' => $pollapplications,
            'poll' => $pollscheduling,
            'company' => $company, 
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        );
        
        // Verify if the survey is employed or dependent of a workshop
        
        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            $rol = $session->get('currentrol');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wiId, $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        if ($logged) {
            $array_parameters['profile'] = $profile;
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll:evaluateds.html.twig', $array_parameters);
        }
        else {
            $array_parameters['personaltoken'] = $personaltoken;
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll:evaluateds_token.html.twig', $array_parameters);
        }
    }

    /**
     * Resolve Poll pollEvaluateds Edit.
     *
     */
    public function pollEvaluatedsEditAction(Request $request, $pollid, $pollapplicationid, $page)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $wi = $session->get('workinginformation');
        
        $token = '';
        if (isset($_GET['token'])) {
            $token = $_GET['token'];
        }
        $token_on = TRUE;
        $securityContext = $this->container->get('security.context');
        $logged = $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $profile = '';
        $view_send_button = FALSE;
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Recover Pollapplication
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
        
        if ($pollscheduling->getType() != 'not_evaluateds') {
            $pollapplication_query = $repository->createQueryBuilder('pa')
                ->select('pa.id, pa.entity_application_type entity_type, pa.entity_application_id entity_id, 
                          pa.initdate, pa.enddate, pa.evaluator_id, u.names, u.surname, u.lastname, pa.term_on, 
                          pa.deleted, pa.finished, pa.token')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->innerJoin('wi.user', 'u')
                ->where('pa.id = :pollapplication
                        AND pa.pollscheduling = :pollscheduling')
                ->setParameter('pollapplication', $pollapplicationid)
                ->setParameter('pollscheduling', $pollid)
                ->getQuery()
                ->getResult();
        }
        else { 
            $pollapplication_query = $repository->createQueryBuilder('pa')
                ->select('pa.id, pa.entity_application_type entity_type, pa.entity_application_id entity_id, 
                          pa.initdate, pa.enddate, pa.evaluator_id, pa.term_on, pa.deleted, pa.finished, pa.token')
                ->where('pa.id = :pollapplication
                        AND pa.pollscheduling = :pollscheduling')
                ->andWhere('pa.evaluated_id IS NULL')
                ->setParameter('pollapplication', $pollapplicationid)
                ->setParameter('pollscheduling', $pollid)
                ->getQuery()
                ->getResult();
        }
        
        $pollapplication = current($pollapplication_query);
        
        if ($logged) {
            
            // Recover Pollschedulingpeople
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
            $pollschedulingpeople_query = $repository->createQueryBuilder('psp')
                ->select('psp.id, psp.profile')
                ->where('psp.workinginformation = :workinginformation
                        AND psp.pollscheduling = :pollscheduling')
                ->setParameter('workinginformation', $wi['id'])
                ->setParameter('pollscheduling', $pollid)
                ->getQuery()
                ->getResult();
            $entity_application = current($pollschedulingpeople_query);
            
            $profile = $entity_application['profile'];
            
        }
        
        // Array Parameters
        
        if ($token != '' && !$logged) {
            $array_parameters = array(
                'pollid' => $pollid, 
                'company' => $company, 
                'pollapplicationid' => $pollapplicationid,
                'page' => $page,
                'token' => $token
            );
        }
        else {
            $array_parameters = array(
                'pollid' => $pollid, 
                'company' => $company, 
                'pollapplicationid' => $pollapplicationid,
                'page' => $page, 
                'profile' => $profile
            );
        }
        
        //ACCESS CONTROL
        
        if ($logged) {
        
            if($pollscheduling->getDeleted()){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
          
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
          
            if($pollapplication['deleted']) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            //La actividad esta asignada al wi del usuario actual
            $wi = $session->get('workinginformation');
            if($pollapplication['evaluator_id'] != $wi['id']){
                $session->getFlashBag()->add('error',
                    'La persona con la que está tratado de ingresar no tiene permiso para esta encuesta');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
        }
        else {
            $token_on = TRUE;
            
            if (!(strlen($token) == 64 && $token == $pollapplication['token'])) {
                $token_on = FALSE;
            }
            
            if (!$token_on) {
                return $this->redirect($this->generateUrl('_welcome'));
            }
        }
        
        // Poll Validate acces Control
        
        $poll_message = Pollapplication::pollValidateAccessControl($logged, $token_on, $pollscheduling, $pollapplication['finished'], $pollapplication['term_on']);
        
        if ($poll_message != '') {
            if ($poll_message == 'off') {
                if ($logged) {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_terms', array(
                        'pollid' => $pollid,
                        'pollapplicationid' => $pollapplication['id']
                    )));
                }
                else {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_terms', array(
                        'pollid' => $pollid,
                        'pollapplicationid' => $pollapplication['id'],
                        'token' => $token
                    )));
                }
            }
            elseif ($poll_message != '') {
                if ($logged) {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_message', array(
                        'pollid' => $pollid,
                        'pollapplicationid' => $pollapplication['id']
                    )));
                }
                else {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_message', array(
                        'pollid' => $pollid,
                        'pollapplicationid' => $pollapplication['id'],
                        'token' => $token
                    )));
                }
            }
        }
        
        //FIN - ACCESS CONTROL
        
        // Paginator

        $array_paginator = Pollapplicationquestion::getPaginator($this->getDoctrine(), $pollscheduling, $page);
        
        if ($array_paginator['number_pagers'] > 0 && $page > $array_paginator['number_pagers']) {
            $array_parameters['page'] = $array_paginator['number_pagers'];
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
        }
        
        // Recover Questions in page current
        
        $array['sections'] = $pollscheduling->getSerialized();
        
        if ($pollscheduling->getDivide() == 'sections_show') {
            $array_questions = Pollschedulingsection::getArraySectionShow($this->getDoctrine(), $array, $pollscheduling->getId(), $page);
        }
        else if ($pollscheduling->getDivide() == 'number_questions') {
            $array_questions = Pollschedulingsection::getArrayNumberQuestions($this->getDoctrine(), $array['sections'], $pollscheduling->getId(), $page, $pollscheduling->getNumberQuestion());
        }
        else if ($pollscheduling->getDivide() == 'none') {
            // jtt
            $ids = array(19,26,27,40); //array con id de encuestas de TEMPERAMENTO
            if (in_array($pollscheduling->getPoll()->getId(), $ids)) {
            foreach ($array['sections'] as $llave => $valor) {
                if (array_key_exists('questions', $valor)) {
                    uasort($array['sections'][$llave]['questions'], function($a, $b) {
                        return $a['sequence'] - $b['sequence'];
                    });
                }
            }
            }
            $array_questions = $array;
        }
        
        if (!empty($array_questions)) {
            $array_poll_questions = $array_questions['sections'];
        }
        else {
            $array_poll_questions = '';
        }
        
        // Update array Questions with id pollschedulingsection and pollapplicationquestion
        $poll_questions['sections'] = Pollapplicationquestion::getArrayQuestionAndAnswer($this->getDoctrine(), $array_poll_questions, $pollapplicationid);
        
        // get array Questions
        $array_questions = Pollapplicationquestion::getArrayQuestions($poll_questions['sections']);
        
        //Save data in Pollapplicationquestion
        
        $postData = $request->request->get('questions', null);
        $postCurrentPage = $request->request->get('current_page', null);
        $postPage = $request->request->get('page', null);
        $postFirst = $request->request->get('first');
        $postPrev = $request->request->get('prev');
        $postNext = $request->request->get('next');
        $postLast = $request->request->get('last');
        
        // suspend auto-commit
        $em->getConnection()->beginTransaction();
        
        // Try and make the transaction
        try {
            
            if (!empty($postData) || $request->request->get('send', false)) {
                
                if (!empty($postData)) {
                    
                    foreach ($postData as $answerId => $answerQuestion) {
                        
                        $entity = $em->getRepository('FishmanPollBundle:Pollapplicationquestion')->find($answerId);
                        
                        if ($answerQuestion) {
                            if (!in_array($array_questions[$answerId]['type'], array('simple_text', 'multiple_text'))) {
                                $default_options = $entity->getPollschedulingquestion()->getOptions();
                                $score = '';
                                
                                if ($array_questions[$answerId]['type'] == 'multiple_option') {
                                    foreach ($answerQuestion as $op) {
                                        $score += $default_options[$op]['score'];
                                    }
                                }
                                else {
                                    if ($answerQuestion !== '') {
                                        $score = $default_options[$answerQuestion]['score'];
                                    }
                                }
                        
                                $entity->setAnswerOptions($answerQuestion);
                                $entity->setScore($score);
                            }
                            else {     
                                $entity->setAnswer($answerQuestion);
                            }
                            
                            $entity->setFinished(TRUE);
                        }
                        else {
                            $entity->setAnswerOptions(NULL);
                            $entity->setScore(NULL);
                            $entity->setFinished(FALSE);
                        }
                                
                        $em->persist($entity);
                        $em->flush();
                    }
    
                }
                        
                if ($pollapplication['initdate'] == '') {
                    $pa_entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($pollapplication['id']);
                    $pa_entity->setInitdate(new \DateTime());
                
                    $em->persist($pa_entity);
                    $em->flush();
                }
                
                if (!$request->request->get('savedraft', false) && ($postPage > $postCurrentPage || $postNext || $postLast)) {
                    
                    // mandatory checks for unresolved answers current page
                    $unresolvedPage = Pollapplicationquestion::getUnResolvedCurrentPage($this->getDoctrine(), $postData);
                    
                    if ($unresolvedPage || count($postData) < $array_questions['questions_mandatory']) {
                        $session->getFlashBag()->add('error', 'No ha respondido todas las preguntas obligatorias de la página ' . $postCurrentPage . '. Gracias');
                        
                        $array_parameters['page'] = $postCurrentPage;
                        
                        // Commit the transactioncurrentversioncurrentversion
                        $em->getConnection()->commit();
                        return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
                    }
                    
                }
                
                if ($request->request->get('send', false)) {
                    
                    // mandatory checks for unresolved answers
                    $unresolved = Pollapplicationquestion::getUnResolved($this->getDoctrine(), $pollapplication['id']);
                    
                    if (!$unresolved) {
                        
                        Pollapplication::getAverageByPollapplication($this->getDoctrine(), $pollapplication['id']);
                        
                        // Add Pollapplicationreports
                        Pollapplicationreport::cloneRegisters($this->getDoctrine(), $pollapplication['id']);
                        
                        $pa_entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($pollapplication['id']);
                        
                        $pa_entity->setFinished(TRUE);
                        if ($pa_entity->getInitdate() == '') {
                            $pa_entity->setInitdate(new \DateTime());
                        }
                        $pa_entity->setEnddate(new \DateTime());
                        
                        $em->persist($pa_entity);
                        $em->flush();
                        
                        // Send trigger notifications
                        
                        //Doctrine registry
                        $mailer = $this->get('mailer');
                        $logger = $this->get('logger');
                        $router = $this->get('router');
                        $templating = $this->get('templating');
                        
                        Notificationexecution::sendTriggerNotifications($this->getDoctrine(), $mailer, $logger, $router, $templating, $pollapplication['id']);
                        
                        // Active notificationExecution status send
                        Notificationexecution::activeNotificationexecutions($this->getDoctrine(), $pa_entity->getId(), 'poll', TRUE, FALSE, '-1', FALSE);
                        
                        $pscrId = $pollscheduling->getPollschedulingrelationId();
                                
                        // Check if there is survey concerning unresolved
                        if ($pscrId != '') {
                            
                            $resolvedPscr = $this->getDoctrine()
                                ->getRepository('FishmanPollBundle:Pollapplication')
                                ->findOneBy(
                                      array(
                                          'pollscheduling' => $pscrId, 
                                          'evaluator_id' => $pollapplication['evaluator_id'],
                                          'finished' => FALSE, 
                                          'deleted' => FALSE
                                      )
                                  );
                                  
                            if (!empty($resolvedPscr)) {
                              
                                unset($array_parameters['pollapplicationid']);
                                $array_parameters['pollid'] = $pscrId;
                                
                                // Commit the transactioncurrentversioncurrentversion
                                $em->getConnection()->commit();
                                if ($logged) {
                                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
                                }
                                elseif ($token_on) {
                                    
                                    // Recover Pollschedulingpeople
                                    $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                                        'workinginformation' => $pollapplication['evaluator_id'],
                                        'pollscheduling' => $pscrId
                                    ));
                                    
                                    unset($array_parameters['token']);
                                    $array_parameters['personaltoken'] = $entity_application->getPersonaltoken();
                                    
                                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
                                }
                            }
                        }
                        
                        // Commit the transactioncurrentversioncurrentversion
                        $em->getConnection()->commit();
                        if ($logged) {
                            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_message', array(
                                'pollid' => $pollid,
                                'pollapplicationid' => $pollapplication['id']
                            )));
                        }
                        elseif ($token_on) {
                            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_message', array(
                                'pollid' => $pollid,
                                'pollapplicationid' => $pollapplication['id'],
                                'token' => $token
                            )));
                        }
                        
                    }
                    else {
                        $session->getFlashBag()->add('error', 'Existen preguntas obligatorias sin responder');
                        
                        // Commit the transactioncurrentversioncurrentversion
                        $em->getConnection()->commit();
                        return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
                    }
                }
                else {
                    
                    // Commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                    
                    if ($postFirst) {
                        $page = 1;
                    }
                    if ($postPrev) {
                        $page--;
                    }
                    if ($postPage) {
                        $page = $postPage;
                    }
                    if ($postNext) {
                        $page++;
                    }
                    if ($postLast) {
                        $page = $array_paginator['number_pagers'];
                    }
                    
                    if ($postPage || $postFirst || $postPrev || $postNext || $postLast) {
                        $array_parameters['page'] = $page;
                        return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
                    }
                    else {
                        $session->getFlashBag()->add('status', 'Los cambios han sido guardados exitosamente');
                    }
                }
                
                if ($logged) {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                        'pollid' => $pollid
                    )));
                }
                
                // Update array Questions with id pollschedulingsection and pollapplicationquestion
                $poll_questions['sections'] = Pollapplicationquestion::getArrayQuestionAndAnswer($this->getDoctrine(), $array_poll_questions, $pollapplicationid);
            }
            
        } catch (Exception $e) {
            // Rollback the failed transaction attempt
            $em->getConnection()->rollback();
            throw $e;
        }
        
        if ($request->request->get('send', false)) {
            $session->getFlashBag()->add('error', 'Debe resolver la encuesta');
        }
        
        if ($postFirst) {
            $page = 1;
        }
        if ($postPrev) {
            $page--;
        }
        if ($postPage) {
            $page = $postPage;
        }
        if ($postNext) {
            $page++;
        }
        if ($postLast) {
            $page = $array_paginator['number_pagers'];
        }
        
        if ($postPage || $postFirst || $postPrev || $postNext || $postLast) {
            
            // mandatory checks for unresolved answers current page
            $unresolvedPage = Pollapplicationquestion::getUnResolvedCurrentPage($this->getDoctrine(), $postData);
            
            if ($unresolvedPage || count($postData) < $array_questions['questions_mandatory']) {
                $session->getFlashBag()->add('error', 'No ha respondido todas las preguntas obligatorias de la página ' . $postCurrentPage . '. Gracias');
                
                $array_parameters['page'] = $postCurrentPage;
                
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
            }
            
            $array_parameters['page'] = $page;
            
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
        }
        
        // Advanced
        $advanced = Pollapplicationquestion::getAdvancedPoll($this->getDoctrine(), $pollscheduling->getStatusBar(), 'unique', $pollapplication['id']);
        
        // ViewSendButton
        if (!$array_paginator['number_pagers'] || $page == $array_paginator['number_pagers']) {
            $view_send_button = TRUE;
        }
        
        // Array Parameters
        
        $array_parameters = array(
            'poll' => $pollscheduling, 
            'company' => $company, 
            'pollapplication' => $pollapplication, 
            'page' => $page,
            'advanced' => $advanced, 
            'paginator' => $array_paginator['paginator'],
            'questions' => $poll_questions,
            'profile' => $profile, 
            'alignment' => 'vertical', 
            'viewsendbutton' => $view_send_button
        );
        
        if ($pollscheduling->getAlignment() == 'horizontal') {
            $array_parameters['alignment'] = 'horizontal';
        }

        if (!$logged) {
            $array_parameters['token'] = $token;
        }
        
        // Return
        
        if ($logged) {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll:edit.html.twig', $array_parameters);
        }
        else {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll:edit_token.html.twig', $array_parameters);
        }
    }

    /**
     * Resolve Poll Integrants Terms.
     *
     */
    public function pollEvaluatedsTermsAction(Request $request, $pollid, $pollapplicationid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $wi = $session->get('workinginformation');
        
        $token = '';
        if (isset($_GET['token'])) {
            $token = $_GET['token'];
        }
        $token_on = TRUE;
        $securityContext = $this->container->get('security.context');
        $logged = $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $profile = '';
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Recover Pollapplication
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
        
        if ($pollscheduling->getType() != 'not_evaluateds') {
            $pollapplication_query = $repository->createQueryBuilder('pa')
                ->select('pa.id, pa.entity_application_type entity_type, pa.entity_application_id entity_id, 
                          pa.initdate, pa.enddate, pa.evaluator_id, u.names, u.surname, u.lastname, pa.term_on, 
                          pa.deleted, pa.finished, pa.token')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->innerJoin('wi.user', 'u')
                ->where('pa.id = :pollapplication
                        AND pa.pollscheduling = :pollscheduling')
                ->setParameter('pollapplication', $pollapplicationid)
                ->setParameter('pollscheduling', $pollid)
                ->getQuery()
                ->getResult();
        }
        else {
            $pollapplication_query = $repository->createQueryBuilder('pa')
                ->select('pa.id, pa.entity_application_type entity_type, pa.entity_application_id entity_id, 
                          pa.initdate, pa.enddate, pa.evaluator_id, pa.term_on, pa.deleted, pa.finished, pa.token')
                ->where('pa.id = :pollapplication
                        AND pa.pollscheduling = :pollscheduling')
                ->andWhere('pa.evaluated_id IS NULL')
                ->setParameter('pollapplication', $pollapplicationid)
                ->setParameter('pollscheduling', $pollid)
                ->getQuery()
                ->getResult();
        }

        $pollapplication = current($pollapplication_query);

        if ($logged) {
            
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                
                $workshopschedulingid = $pollscheduling->getEntityId();
                
                // Recover Workshopapplication
                
                $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $workshopapplication_query = $repository->createQueryBuilder('wa')
                    ->select('wa.id, wa.profile')
                    ->where('wa.workinginformation = :workinginformation
                            AND wa.workshopscheduling = :workshopscheduling
                            AND wa.deleted = :deleted')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('workshopscheduling', $workshopschedulingid)
                    ->setParameter('deleted', FALSE)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($workshopapplication_query);
                
                $profile = $entity_application['profile'];
                
            }
            else {
    
                // Recover Pollschedulingpeople
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $pollschedulingpeople_query = $repository->createQueryBuilder('psp')
                    ->select('psp.id, psp.profile')
                    ->where('psp.workinginformation = :workinginformation
                            AND psp.pollscheduling = :pollscheduling')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('pollscheduling', $pollid)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($pollschedulingpeople_query);
                
                $profile = $entity_application['profile'];
                
            }
            
        }

        //ACCESS CONTROL
        
        $securityContext = $this->container->get('security.context');
        
        if ($logged) {
        
            if($pollscheduling->getDeleted()){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
          
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
          
            if($pollapplication['deleted']){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            //La actividad esta asignada al wi del usuario actual
            $wi = $session->get('workinginformation');
            if($pollapplication['evaluator_id'] != $wi['id']){
                $session->getFlashBag()->add('error',
                    'La persona con la que está tratado de ingresar no tiene permiso para esta encuesta');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
        }
        else {
            $token_on = TRUE;
            
            if (!(strlen($token) == 64 && $token == $pollapplication['token'])) {
                $token_on = FALSE;
            }
            
            if (!$token_on) {
                return $this->redirect($this->generateUrl('_welcome'));
            }
        }
        
        // Array Parameters
        
        if ($token != '' && !$logged) {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'pollapplicationid' => $pollapplicationid,
                'token' => $token
            );
        }
        else {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'pollapplicationid' => $pollapplicationid, 
                'profile' => $profile
            );
        }

        // Save data in Pollapplication

        $postData = $request->request->get('terms_on', null);
        
        $pa_entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($pollapplication['id']);
        
        if ($postData != '') {
            $pa_entity->setTermOn($postData);
            $pa_entity->setInitdate(new \DateTime());
       
            $em->persist($pa_entity);
            $em->flush();
        }
        
        // Poll Validate acces Control
        
        $poll_message = Pollapplication::pollValidateAccessControl($logged, $token_on, $pollscheduling, $pollapplication['finished'], $pa_entity->getTermOn());
        
        if (!$pollscheduling->getUseTerms() || ($pa_entity->getTermOn() && $pollscheduling->getUseTerms())) {
            if ($logged) {
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
            }
        }
        elseif ($poll_message != '' && $poll_message != 'off') {
            if ($logged) {
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_message', $array_parameters));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_message', $array_parameters));
            }
        }
        
        //FIN - ACCESS CONTROL
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling,
            'company' => $company, 
            'pollapplication' => $pollapplication,
            'terms' => $pollscheduling->getTerms(),
            'profile' => $profile
        );

        if (!$logged) {
            $array_parameters['token'] = $token;
        }
        
        // Verify if the survey is employed or dependent of a workshop
        
        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            $rol = $session->get('currentrol');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $pollapplication['evaluator_id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        if ($logged) {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll:terms.html.twig', $array_parameters);
        }
        else {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll:terms_token.html.twig', $array_parameters);
        }
    }

    /**
     * Resolve Poll Integrants Message.
     *
     */
    public function pollEvaluatedsMessageAction(Request $request, $pollid, $pollapplicationid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $wi = $session->get('workinginformation');
        
        $token = '';
        if (isset($_GET['token'])) {
            $token = $_GET['token'];
        }
        $token_on = TRUE;
        $securityContext = $this->container->get('security.context');
        $logged = $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $profile = '';
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Recover Pollapplication
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
        
        if ($pollscheduling->getType() != 'not_evaluateds') {
            $pollapplication_query = $repository->createQueryBuilder('pa')
                ->select('pa.id, pa.entity_application_type entity_type, pa.entity_application_id entity_id, 
                          pa.initdate, pa.enddate, pa.evaluator_id, u.names, u.surname, u.lastname, pa.term_on, 
                          pa.deleted, pa.finished, pa.token')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id')
                ->innerJoin('wi.user', 'u')
                ->where('pa.id = :pollapplication
                        AND pa.pollscheduling = :pollscheduling')
                ->setParameter('pollapplication', $pollapplicationid)
                ->setParameter('pollscheduling', $pollid)
                ->getQuery()
                ->getResult();
        }
        else {
            $pollapplication_query = $repository->createQueryBuilder('pa')
                ->select('pa.id, pa.entity_application_type entity_type, pa.entity_application_id entity_id, 
                          pa.initdate, pa.enddate, pa.evaluator_id, pa.term_on, pa.deleted, pa.finished, pa.token')
                ->where('pa.id = :pollapplication
                        AND pa.pollscheduling = :pollscheduling')
                ->andWhere('pa.evaluated_id IS NULL')
                ->setParameter('pollapplication', $pollapplicationid)
                ->setParameter('pollscheduling', $pollid)
                ->getQuery()
                ->getResult();
        }
        
        $pollapplication = current($pollapplication_query);
        
        if ($logged) {
            
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                
                $workshopschedulingid = $pollscheduling->getEntityId();
                
                // Recover Workshopapplication
                
                $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $workshopapplication_query = $repository->createQueryBuilder('wa')
                    ->select('wa.id, wa.profile')
                    ->where('wa.workinginformation = :workinginformation
                            AND wa.workshopscheduling = :workshopscheduling
                            AND wa.deleted = :deleted')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('workshopscheduling', $workshopschedulingid)
                    ->setParameter('deleted', FALSE)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($workshopapplication_query);
                
                $profile = $entity_application['profile'];
                
            }
            else {
    
                // Recover Pollschedulingpeople
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $pollschedulingpeople_query = $repository->createQueryBuilder('psp')
                    ->select('psp.id, psp.profile')
                    ->where('psp.workinginformation = :workinginformation
                            AND psp.pollscheduling = :pollscheduling')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('pollscheduling', $pollid)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($pollschedulingpeople_query);
                
                $profile = $entity_application['profile'];
                
            }
            
        }
        
        // ACCESS CONTROL
        
        if ($logged) {
            if($pollscheduling->getDeleted()){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            if($pollapplication['deleted']){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            //La actividad esta asignada al wi del usuario actual
            $wi = $session->get('workinginformation');
            if($pollapplication['evaluator_id'] != $wi['id']){
                $session->getFlashBag()->add('error',
                    'La persona con la que está tratado de ingresar no tiene permiso para esta encuesta');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
        }
        else {
            if (!(strlen($token) == 64 && $token == $pollapplication['token'])) {
                $token_on = FALSE;
            }
            
            if (!$token_on) {
                return $this->redirect($this->generateUrl('_welcome'));
            }
        }
        
        // FIN ACCESS CONTROL
        
        // Array Parameters
        
        if ($token != '' && !$logged) {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'pollapplicationid' => $pollapplicationid,
                'token' => $token
            );
        }
        else {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'pollapplicationid' => $pollapplicationid, 
                'profile' => $profile
            );
        }
        
        // Poll Validate acces Control
        
        $poll_message = Pollapplication::pollValidateAccessControl($logged, $token_on, $pollscheduling, $pollapplication['finished'], $pollapplication['term_on'], -1);
        
        // Poll Message
        
        $message_info = Pollapplication::pollMessage($logged, $token_on, $pollapplication['term_on'], $pollscheduling, $poll_message);
        
        if ($poll_message == '' || $message_info['edit']) {
            $session->getFlashBag()->add('error', 'La encuesta no ha sido enviada');
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', $array_parameters));
        }
        elseif ($poll_message == 'off') {
            $session->getFlashBag()->add('error', 'Debe aceptar los términos y condiciones');
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_terms', $array_parameters));
        }
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling,
            'company' => $company, 
            'pollapplication' => $pollapplication,
            'message_info' => $message_info,
            'profile' => $profile
        );

        if (!$logged) {
            $array_parameters['token'] = $token;
        }
        
        // Verify if the survey is employed or dependent of a workshop
        
        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            $rol = $session->get('currentrol');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $pollapplication['evaluator_id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }

        // Return
        
        if ($logged) {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll:message.html.twig', $array_parameters);
        }
        else {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll:message_token.html.twig', $array_parameters);
        }
    }

    /**
     * Resolve Poll pollEvaluateds Edit.
     *
     */
    public function pollMultipleevaluationEditAction(Request $request, $pollid, $page)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $wi = $session->get('workinginformation');
        
        $personaltoken = '';
        if (isset($_GET['personaltoken'])) {
            $personaltoken = $_GET['personaltoken'];
        }
        $token_on = TRUE;
        $term_on = TRUE;
        $finished = FALSE;
        $securityContext = $this->container->get('security.context');
        $logged = $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $profile = '';
        $view_send_button = FALSE;
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        
        if ($logged) {
          
            $wiId = $wi['id'];
          
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                
                $workshopschedulingid = $pollscheduling->getEntityId();
                
                // Recover Workshopapplication
                $entity_application = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
                    'workinginformation' => $wi['id'],
                    'workshopscheduling' => $workshopschedulingid, 
                    'deleted' => 0
                ));
                if (!empty($entity_application)) {
                    $profile = $entity_application->getProfile();
                }
            }
            else {
                // Recover Workshopapplication
                $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                    'workinginformation' => $wi['id'],
                    'pollscheduling' => $pollid
                ));
                if (!empty($entity_application)) {
                    $profile = $entity_application->getProfile();
                }
            }

        }
        else {
            
            $entity_application = '';
            
            if (strlen($personaltoken) == 64) {
                if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                    // Recover Workshopapplication
                    $entity_application = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
                        'personaltoken' => $personaltoken,
                        'workshopscheduling' => $pollscheduling->getEntityId(), 
                        'deleted' => 0
                    ));
                }
                else {
                    // Recover Pollschedulingpeople
                    $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                        'personaltoken' => $personaltoken,
                        'pollscheduling' => $pollscheduling->getId()
                    ));
                }
            }
                
            if (!empty($entity_application)) {  
                $wiId = $entity_application->getWorkinginformation()->getId();
                $profile = $entity_application->getProfile();
            }
            else {
                return $this->redirect($this->generateUrl('_welcome'));
            }
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Recover Pollapplication
        $pollapplication = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wiId,
            'deleted' => 0
        ));
            
        // Recover Pollapplication term on
        $pollapplication_term_on = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wiId,
            'finished' => 0,
            'term_on' => NULL
        ));
        
        if (!empty($pollapplication_term_on)) {
            $term_on = FALSE;
        }
        
        // Recover Pollapplication finished
        $pollapplication_finished = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wiId,
            'finished' => 0, 
            'deleted' => 0
        ));
        
        if (empty($pollapplication_finished)) {
            $finished = TRUE;
        }
        
        // Array Parameters
        
        if ($personaltoken != '' && !$logged) {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'page' => $page,
                'personaltoken' => $personaltoken
            );
        }
        else {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'page' => $page, 
                'profile' => $profile
            );
        }
        
        //ACCESS CONTROL
        
        if ($logged) {
        
            if($pollscheduling->getDeleted()){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
          
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            if (empty($pollapplication)) {
                $session->getFlashBag()->add('error', 'No existen personas a evaluar');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            // La encuesta esta asignada al wi del usuario actual
            $wi = $session->get('workinginformation');
            if ($pollapplication->getEvaluatorId() != $wiId) {
                $session->getFlashBag()->add('error',
                    'La persona con la que está tratado de ingresar no tiene permiso para esta encuesta');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
        }
        
        // Poll Validate acces Control
        
        $poll_message = Pollapplication::pollValidateAccessControl($logged, $token_on, $pollscheduling, $finished, $term_on);
        
        if ($poll_message != '') {
            if ($poll_message == 'off') {
                if ($logged) {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_terms', array(
                        'pollid' => $pollid
                    )));
                }
                else {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_terms', array(
                        'pollid' => $pollid,
                        'personaltoken' => $personaltoken
                    )));
                }
            }
            elseif ($poll_message != '') {
                if ($logged) {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_message', array(
                        'pollid' => $pollid,
                    )));
                }
                else {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_message', array(
                        'pollid' => $pollid,
                        'personaltoken' => $personaltoken
                    )));
                }
            }
        }

        //FIN - ACCESS CONTROL
        
        // Paginator
        
        $array_paginator = Pollapplicationquestion::getPaginator($this->getDoctrine(), $pollscheduling, $page);
        
        if ($array_paginator['number_pagers'] > 0 && $page > $array_paginator['number_pagers']) {
            $array_parameters['page'] = $array_paginator['number_pagers'];
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
        }

        // Recover Questions in page current
        
        $array['sections'] = $pollscheduling->getSerialized();
        
        if ($pollscheduling->getDivide() == 'sections_show') {
            $array_questions = Pollschedulingsection::getArraySectionShow($this->getDoctrine(), $array, $pollscheduling->getId(), $page);
        }
        else if ($pollscheduling->getDivide() == 'number_questions') {
            $array_questions = Pollschedulingsection::getArrayNumberQuestions($this->getDoctrine(), $array['sections'], $pollscheduling->getId(), $page, $pollscheduling->getNumberQuestion());
        }
        else if ($pollscheduling->getDivide() == 'none') {
            $array_questions = $array;
        }
        
        if (!empty($array_questions)) {
            $array_poll_questions = $array_questions['sections'];
        }
        else {
            $array_poll_questions = '';
        }
        
        // Recover Pollapplications
        $array_pollapplications = Pollapplication::getArrayPollapplications($this->getDoctrine(), $pollscheduling->getId(), $wiId);
        
        // Update array Questions with id pollschedulingsection and pollapplicationquestion
        $poll_questions['sections'] = Pollapplicationquestion::getArrayQuestionAndAnswer($this->getDoctrine(), $array_poll_questions, $array_pollapplications);
        
        // get array Questions
        $array_questions = Pollapplicationquestion::getArrayQuestions($poll_questions['sections']);
        
        //Save data in Pollapplicationquestion
        
        $postData = $request->request->get('questions', null);
        $postCurrentPage = $request->request->get('current_page', null);
        $postPage = $request->request->get('page', null);
        $postFirst = $request->request->get('first');
        $postPrev = $request->request->get('prev');
        $postNext = $request->request->get('next');
        $postLast = $request->request->get('last');
        
        // suspend auto-commit
        $em->getConnection()->beginTransaction();
        
        // Try and make the transaction
        try {
            
            if (!empty($postData) || $request->request->get('send', false)) {
                
                if (!empty($postData)) {
                    
                    foreach ($postData as $answerId => $answerQuestion) {
                        
                        $entity = $em->getRepository('FishmanPollBundle:Pollapplicationquestion')->find($answerId);
                        $type = $entity->getPollschedulingquestion()->getType();
                        
                        if ($answerQuestion) {
                            if (!in_array($type, array('simple_text', 'multiple_text'))) {
                                $default_options = $entity->getPollschedulingquestion()->getOptions();
                                $score = '';
                                
                                if ($type == 'multiple_option') {
                                    foreach ($answerQuestion as $op) {
                                        $score += $default_options[$op]['score'];
                                    }
                                }
                                else {
                                    $score = $default_options[$answerQuestion]['score'];
                                }
                                
                                $entity->setAnswerOptions($answerQuestion);
                                $entity->setScore($score);
                            }
                            else {
                                $entity->setAnswer($answerQuestion);
                            }
                            
                            $entity->setFinished(TRUE);
                        }
                        else {
                            $entity->setAnswerOptions(NULL);
                            $entity->setScore(NULL);
                            $entity->setFinished(FALSE);
                        }
                        
                        $em->persist($entity);
                        $em->flush();
                    }
    
                }
                
                foreach ($array_pollapplications as $pa) {
                    if ($pa['initdate'] == '') {
                        $pa_entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($pa['id']);
                        $pa_entity->setInitdate(new \DateTime());
                    
                        $em->persist($pa_entity);
                        $em->flush();
                    }
                }
                
                if (!$request->request->get('savedraft', false) && ($postPage > $postCurrentPage || $postNext || $postLast)) {
                    
                    // mandatory checks for unresolved answers current page
                    $unresolvedPage = Pollapplicationquestion::getUnResolvedCurrentPage($this->getDoctrine(), $postData);
                    
                    if ($unresolvedPage || count($postData) < $array_questions['questions_mandatory']) {
                        $session->getFlashBag()->add('error', 'No ha respondido todas las preguntas obligatorias de la página ' . $postCurrentPage . '. Gracias');
                        
                        $array_parameters['page'] = $postCurrentPage;
                        
                        // Commit the transactioncurrentversioncurrentversion
                        $em->getConnection()->commit();
                        return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
                    }
                    
                }
    
                if ($request->request->get('send', false)) {
                    
                    // mandatory checks for unresolved answers
                    $unresolved = FALSE;
                    foreach ($array_pollapplications as $pa) {
                        $pa_unresolved = Pollapplicationquestion::getUnResolved($this->getDoctrine(), $pa['id']);
                        if ($pa_unresolved) {
                            $unresolved = TRUE;
                        }
                    }
                    
                    if (!$unresolved) {
                        
                        //Doctrine registry
                        $mailer = $this->get('mailer');
                        $logger = $this->get('logger');
                        $router = $this->get('router');
                        $templating = $this->get('templating');
                      
                        foreach ($array_pollapplications as $pa) {
                            
                            Pollapplication::getAverageByPollapplication($this->getDoctrine(), $pa['id']);
                            
                            // Add Pollapplicationreports
                            Pollapplicationreport::cloneRegisters($this->getDoctrine(), $pa['id']);
                            
                            $pa_entity = $em->getRepository('FishmanPollBundle:Pollapplication')->find($pa['id']);
                            
                            $pa_entity->setFinished(TRUE);
                            if ($pa_entity->getInitdate() == '') {
                                $pa_entity->setInitdate(new \DateTime());
                            }
                            $pa_entity->setEnddate(new \DateTime());
                            
                            $em->persist($pa_entity);
                            $em->flush();
                            
                            // Send trigger notifications
                            Notificationexecution::sendTriggerNotifications($this->getDoctrine(), $mailer, $logger, $router, $templating, $pa['id']);
                            
                            // Active notificationExecution status send
                            Notificationexecution::activeNotificationexecutions($this->getDoctrine(), $pa_entity->getId(), 'poll', TRUE, FALSE, '-1', FALSE);
                            
                        }
                        
                        // Commit the transactioncurrentversioncurrentversion
                        $em->getConnection()->commit();
                        
                        if ($logged) {
                            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_message', array(
                                'pollid' => $pollid
                            )));
                        }
                        elseif ($token_on) {
                            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_message', array(
                                'pollid' => $pollid,
                                'personaltoken' => $personaltoken
                            )));
                        }
                    }
                    else {
                        $session->getFlashBag()->add('error', 'Existen preguntas obligatorias sin responder');
                        
                        // Commit the transactioncurrentversioncurrentversion
                        $em->getConnection()->commit();
                        return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
                    }
                }
                else {
                    
                    // Commit the transactioncurrentversioncurrentversion
                    $em->getConnection()->commit();
                    
                    if ($postFirst) {
                        $page = 1;
                    }
                    if ($postPrev) {
                        $page--;
                    }
                    if ($postPage) {
                        $page = $postPage;
                    }
                    if ($postNext) {
                        $page++;
                    }
                    if ($postLast) {
                        $page = $array_paginator['number_pagers'];
                    }
                    
                    if ($postPage || $postFirst || $postPrev || $postNext || $postLast) {
                        $array_parameters['page'] = $page;
                        return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
                    }
                    else {
                        $session->getFlashBag()->add('status', 'Los cambios han sido guardados exitosamente');
                    }
                }
                
                if ($logged) {
                    return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                        'pollid' => $pollid
                    )));
                }
                
                // Update array Questions with id pollschedulingsection and pollapplicationquestion
                $poll_questions['sections'] = Pollapplicationquestion::getArrayQuestionAndAnswer($this->getDoctrine(), $array_poll_questions, $array_pollapplications);
            }
            
            // Try and commit the transactioncurrentversioncurrentversion
            $em->getConnection()->commit();
        } catch (Exception $e) {
            // Rollback the failed transaction attempt
            $em->getConnection()->rollback();
            throw $e;
        }

        if ($request->request->get('send', false)) {
            $session->getFlashBag()->add('error', 'Debe resolver la encuesta');
        }
        
        if ($postFirst) {
            $page = 1;
        }
        if ($postPrev) {
            $page--;
        }
        if ($postPage) {
            $page = $postPage;
        }
        if ($postNext) {
            $page++;
        }
        if ($postLast) {
            $page = $array_paginator['number_pagers'];
        }
        
        if ($postPage || $postFirst || $postPrev || $postNext || $postLast) {
            
            // mandatory checks for unresolved answers current page
            $unresolvedPage = Pollapplicationquestion::getUnResolvedCurrentPage($this->getDoctrine(), $postData);
            
            if ($unresolvedPage || count($postData) < $array_questions['questions_mandatory']) {
                $session->getFlashBag()->add('error', 'No ha respondido todas las preguntas obligatorias de la página ' . $postCurrentPage . '. Gracias');
                
                $array_parameters['page'] = $postCurrentPage;
                
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
            }
            
            $array_parameters['page'] = $page;
            
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
        }
        
        // Advanced
        $advanced = Pollapplicationquestion::getAdvancedPoll($this->getDoctrine(), $pollscheduling->getStatusBar(), 'multiple', $entity_application->getId());
        
        // ViewSendButton
        if (!$array_paginator['number_pagers'] || $page == $array_paginator['number_pagers']) {
            $view_send_button = TRUE;
        }
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling, 
            'company' => $company, 
            'page' => $page,
            'advanced' => $advanced, 
            'paginator' => $array_paginator['paginator'],
            'questions' => $poll_questions,
            'profile' => $profile, 
            'viewsendbutton' => $view_send_button
        );

        if (!$logged) {
            $array_parameters['personaltoken'] = $personaltoken;
        }
        
        // Verify if the survey is employed or dependent of a workshop
        
        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            $rol = $session->get('currentrol');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        if ($logged) {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Multiple:edit.html.twig', $array_parameters);
        }
        else {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Multiple:edit_token.html.twig', $array_parameters);
        }
    }

    /**
     * Resolve Poll Integrants Terms.
     *
     */
    public function pollMultipleevaluationTermsAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $wi = $session->get('workinginformation');
        
        $personaltoken = '';
        if (isset($_GET['personaltoken'])) {
            $personaltoken = $_GET['personaltoken'];
        }
        $token_on = TRUE;
        $term_on = TRUE;
        $finished = TRUE;
        $securityContext = $this->container->get('security.context');
        $logged = $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $profile = '';
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        
        if ($logged) {
          
            $wiId = $wi['id'];
          
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                
                $workshopschedulingid = $pollscheduling->getEntityId();
                
                // Recover Workshopapplication
                $entity_application = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
                    'workinginformation' => $wi['id'],
                    'workshopscheduling' => $workshopschedulingid, 
                    'deleted' => 0
                ));
                if (!empty($entity_application)) {
                    $profile = $entity_application->getProfile();
                }
            }
            else {
                // Recover Workshopapplication
                $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                    'workinginformation' => $wi['id'],
                    'pollscheduling' => $pollid
                ));
                if (!empty($entity_application)) {
                    $profile = $entity_application->getProfile();
                }
            }

        }
        else {
            
            $entity_application = '';
            
            if (strlen($personaltoken) == 64) {
                if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                    // Recover Workshopapplication
                    $entity_application = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
                        'personaltoken' => $personaltoken,
                        'workshopscheduling' => $pollscheduling->getEntityId(), 
                        'deleted' => 0
                    ));
                }
                else {
                    // Recover Pollschedulingpeople
                    $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                        'personaltoken' => $personaltoken,
                        'pollscheduling' => $pollscheduling->getId()
                    ));
                }
            }
                
            if (!empty($entity_application)) {  
                $wiId = $entity_application->getWorkinginformation()->getId();
                $profile = $entity_application->getProfile();
            }
            else {
                return $this->redirect($this->generateUrl('_welcome'));
            }
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Recover Pollapplication
        $pollapplication = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wiId,
            'deleted' => 0
        ));
            
        // Recover Pollapplication term on
        $pollapplication_term_on = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wiId,
            'finished' => 0,
            'term_on' => NULL
        ));
        
        if (!empty($pollapplication_term_on)) {
            $finished = FALSE;
            $term_on = FALSE;
        }
        
        // Recover Pollapplication finished
        $pollapplication_finished = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wiId,
            'finished' => 0, 
            'deleted' => 0
        ));
        
        if (empty($pollapplication_finished)) {
            $finished = TRUE;
        }

        //ACCESS CONTROL
        
        if ($logged) {
        
            if ($pollscheduling->getDeleted()) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
          
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            if (empty($pollapplication)) {
                $session->getFlashBag()->add('error', 'No existen personas a evaluar');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            //La actividad esta asignada al wi del usuario actual
            if ($pollapplication->getEvaluatorId() != $wiId) {
                $session->getFlashBag()->add('error',
                    'La persona con la que está tratado de ingresar no tiene permiso para esta encuesta');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
        }
        
        // Array Parameters
        
        if ($personaltoken != '' && !$logged) {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'personaltoken' => $personaltoken
            );
        }
        else {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'profile' => $profile
            );
        }
        
        //Save data in Pollapplication

        $postData = $request->request->get('terms_on', null);
        
        if ($postData != '') {
            
            // Recover Pollapplications
            $pollapplications = $em->getRepository('FishmanPollBundle:Pollapplication')->findBy(array(
                'pollscheduling' => $pollscheduling->getId(), 
                'evaluator_id' => $wiId,
                'finished' => 0,
                'deleted' => 0
            ));
            
            foreach ($pollapplications as $pa_entity) {
                $pa_entity->setTermOn($postData);
                $pa_entity->setInitdate(new \DateTime());
       
                $em->persist($pa_entity);
            }
            
            $em->flush();
            
            $term_on = TRUE;
        }
        
        // Poll Validate acces Control
        $poll_message = Pollapplication::pollValidateAccessControl($logged, $token_on, $pollscheduling, $finished, $term_on);
        
        if (!$pollscheduling->getUseTerms() || ($term_on && $pollscheduling->getUseTerms())) {
            if ($logged) {
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
            }
        }
        elseif ($poll_message != '' && $poll_message != 'off') {
            if ($logged) {
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_message', $array_parameters));
            }
            else {
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_message', $array_parameters));
            }
        }
        
        //FIN - ACCESS CONTROL
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling,
            'company' => $company, 
            'terms' => $pollscheduling->getTerms(),
            'profile' => $profile
        );

        if (!$logged) {
            $array_parameters['personaltoken'] = $personaltoken;
        }
        
        // Verify if the survey is employed or dependent of a workshop
        
        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            $rol = $session->get('currentrol');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        if ($logged) {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Multiple:terms.html.twig', $array_parameters);
        }
        else {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Multiple:terms_token.html.twig', $array_parameters);
        }
    }

    /**
     * Resolve Poll Integrants Message.
     *
     */
    public function pollMultipleevaluationMessageAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $wi = $session->get('workinginformation');
        
        $personaltoken = '';
        if (isset($_GET['personaltoken'])) {
            $personaltoken = $_GET['personaltoken'];
        }
        $token_on = TRUE;
        $term_on = TRUE;
        $finished = TRUE;
        $securityContext = $this->container->get('security.context');
        $logged = $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $profile = '';
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        
        if ($logged) {
          
            $wiId = $wi['id'];
          
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                
                $workshopschedulingid = $pollscheduling->getEntityId();
                
                // Recover Workshopapplication
                $entity_application = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
                    'workinginformation' => $wi['id'],
                    'workshopscheduling' => $workshopschedulingid, 
                    'deleted' => 0
                ));
                if (!empty($entity_application)) {
                    $profile = $entity_application->getProfile();
                }
            }
            else {
                // Recover Workshopapplication
                $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                    'workinginformation' => $wi['id'],
                    'pollscheduling' => $pollid
                ));
                if (!empty($entity_application)) {
                    $profile = $entity_application->getProfile();
                }
            }

        }
        else {
            
            $entity_application = '';
            
            if (strlen($personaltoken) == 64) {
                if ($pollscheduling->getEntityType() == 'workshopscheduling') {
                    // Recover Workshopapplication
                    $entity_application = $em->getRepository('FishmanWorkshopBundle:Workshopapplication')->findOneBy(array(
                        'personaltoken' => $personaltoken,
                        'workshopscheduling' => $pollscheduling->getEntityId(), 
                        'deleted' => 0
                    ));
                }
                else {
                    // Recover Pollschedulingpeople
                    $entity_application = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                        'personaltoken' => $personaltoken,
                        'pollscheduling' => $pollscheduling->getId()
                    ));
                }
            }
                
            if (!empty($entity_application)) {  
                $wiId = $entity_application->getWorkinginformation()->getId();
                $profile = $entity_application->getProfile();
            }
            else {
                return $this->redirect($this->generateUrl('_welcome'));
            }
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Recover Pollapplication
        $pollapplication = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wiId,
            'deleted' => 0
        ));
        
        // Recover Pollapplication term on
        $pollapplication_term_on = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wiId,
            'finished' => 0,
            'term_on' => NULL
        ));
        
        if (!empty($pollapplication_term_on)) {
            $finished = FALSE;
            $term_on = FALSE;
        }
        
        // Recover Pollapplication finished
        $pollapplication_finished = $em->getRepository('FishmanPollBundle:Pollapplication')->findOneBy(array(
            'pollscheduling' => $pollscheduling->getId(),
            'evaluator_id' => $wi['id'],
            'finished' => 0, 
            'deleted' => 0
        ));
        
        if (empty($pollapplication_finished)) {
            $finished = TRUE;
        }
        
        // ACCESS CONTROL
        
        if ($logged) {
          
            if ($pollscheduling->getDeleted()) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }

            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            if (empty($pollapplication)) {
                $session->getFlashBag()->add('error', 'No existen personas a evaluar');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            //La actividad esta asignada al wi del usuario actual
            if ($pollapplication->getEvaluatorId() != $wiId) {
                $session->getFlashBag()->add('error',
                    'La persona con la que está tratado de ingresar no tiene permiso para esta encuesta');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
        }

        // FIN ACCESS CONTROL
        
        // Array Parameters
        
        if ($personaltoken != '' && !$logged) {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'personaltoken' => $personaltoken
            );
        }
        else {
            $array_parameters = array(
                'pollid' => $pollid,
                'company' => $company, 
                'profile' => $profile
            );
        }
        
        // Poll Validate acces Control
        $poll_message = Pollapplication::pollValidateAccessControl($logged, $token_on, $pollscheduling, $finished, $term_on, -1);
        
        // Poll Message
        $message_info = Pollapplication::pollMessage($logged, $token_on, $term_on, $pollscheduling, $poll_message);
        
        if ($poll_message == '' || $message_info['edit']) {
            $session->getFlashBag()->add('error', 'La encuesta no ha sido enviada');
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_edit', $array_parameters));
        }
        elseif ($poll_message == 'off') {
            $session->getFlashBag()->add('error', 'Debe aceptar los términos y condiciones');
            return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_multipleevaluation_terms', $array_parameters));
        }
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling,
            'company' => $company, 
            'message_info' => $message_info,
            'profile' => $profile
        );

        if (!$logged) {
            $array_parameters['personaltoken'] = $personaltoken;
        }
        
        // Verify if the survey is employed or dependent of a workshop
        
        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            $rol = $session->get('currentrol');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }

        // Return
        
        if ($logged) {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Multiple:message.html.twig', $array_parameters);
        }
        else {
            return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Multiple:message_token.html.twig', $array_parameters);
        }
    }

    /**
     * Resolve Poll Anonymous.
     *
     */
    public function pollAnonymousAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Declare variables
        
        $session = $this->getRequest()->getSession();
        
        $personaltoken = '';
        if (isset($_GET['personaltoken'])) {
            $personaltoken = $_GET['personaltoken'];
        }
        
        // Recover Pollscheduling
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || !$pollscheduling->getPollschedulingAnonymous()) {
            return $this->redirect($this->generateUrl('_welcome'));
        }
        
        if (strlen($personaltoken) == 64) {
            
            // Recover Pollschedulingpeople
            $pollschedulingpeople = $em->getRepository('FishmanPollBundle:Pollschedulingpeople')->findOneBy(array(
                'personaltoken' => $personaltoken
            ));
            
            if ($pollschedulingpeople) {
                
                // Add Pollapplications
                $pollapplication = Pollapplication::addPollapplicationToAnonymous($this->getDoctrine(), $pollschedulingpeople, $pollscheduling);
                
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds_edit', array(
                    'pollid' => $pollid,
                    'pollapplicationid' => $pollapplication->getId(),
                    'token' => $pollapplication->getToken()
                )));
            }
        }
        
        return $this->redirect($this->generateUrl('_welcome'));
    }

    /**
     * Lists all the Notifications of the Poll.
     *
     */
    public function pollNotificationsAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        $profile = '';

        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        // Recovering data
        
        $type_options = Notification::getListTypeOptions();
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'type' => '', 
            'sended' => NULL, 
            'sequence' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('type', 'choice', array(
                'choices' => $type_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('sended', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'type' => '', 
            'sended' => NULL, 
            'sequence' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
              
                $wa = $session->get('workshopapplication');
                
                // Recover Workshopapplication
                
                $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $workshopapplication_query = $repository->createQueryBuilder('wa')
                    ->select('wa.id, wa.profile')
                    ->where('wa.workinginformation = :workinginformation
                            AND wa.workshopscheduling = :workshopscheduling
                            AND wa.deleted = :deleted')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('workshopscheduling', $wa['ws_id'])
                    ->setParameter('deleted', FALSE)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($workshopapplication_query);
                
                $profile = $entity_application['profile'];
                
            }
            else {
    
                // Recover Pollschedulingpeople
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $pollschedulingpeople_query = $repository->createQueryBuilder('psp')
                    ->select('psp.id, psp.profile')
                    ->where('psp.workinginformation = :workinginformation
                            AND psp.pollscheduling = :pollscheduling')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('pollscheduling', $pollid)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($pollschedulingpeople_query);
                
                $profile = $entity_application['profile'];
                
            }
    
            // ACCESS CONTROL
            
            if($pollscheduling->getDeleted()){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_integrants', array(
                    'pollid' => $pollid
                )));
            }
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll_evaluateds', array(
                    'pollid' => $pollid
                )));
            }
            
            // Validate Date in Poll
            $validateDate = Pollscheduling::validateDateInPollscheduling($this->getDoctrine(), $pollscheduling->getInitdate(), $pollscheduling->getEnddate());
            if (!$validateDate['valid']) {
                $session->getFlashBag()->add('error', $validateDate['message']);
                return $this->redirect($this->generateUrl('fishman_front_end_polls'));
            }
            
            // FIN ACCESS CONTROL
            
        }
        
        // Recovers Notificationssend
        $query = Notificationsend::getPollListNotificationsends($this->getDoctrine(), $wi['id'], $pollid, $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollnotifications');
        $paginator->setMaxPagerItems(5, 'pollnotifications');
        $notifications = $paginator->paginate($query, 'pollnotifications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollnotifications');
        $endPageItem = $paginator->getEndPageItem('pollnotifications');
        $totalItems = $paginator->getTotalItems('pollnotifications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling,
            'company' => $company, 
            'notifications' => $notifications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'profile' => $profile,
            'form_data' => $formData
        );

        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            $rol = $session->get('currentrol');
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Notification:index.html.twig', $array_parameters);
    }

    /**
     * Show Poll Notificationsends.
     *
     */
    public function pollNotificationsshowAction($pollid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');

        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Recover Notificationsend
        
        $entity = Notificationsend::getPollShowNotificationsend($this->getDoctrine(), $pollid, $id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_poll_notifications', array(
                'pollid' => $pollid
            )));
        }
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling->getEntityType() == 'workshopscheduling') {
              
                $wa = $session->get('workshopapplication');
                
                // Recover Workshopapplication
                
                $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $workshopapplication_query = $repository->createQueryBuilder('wa')
                    ->select('wa.id, wa.profile')
                    ->where('wa.workinginformation = :workinginformation
                            AND wa.workshopscheduling = :workshopscheduling
                            AND wa.deleted = :deleted')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('workshopscheduling', $wa['ws_id'])
                    ->setParameter('deleted', FALSE)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($workshopapplication_query);
                
                $profile = $entity_application['profile'];
                
            }
            else {
    
                // Recover Pollschedulingpeople
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $pollschedulingpeople_query = $repository->createQueryBuilder('psp')
                    ->select('psp.id, psp.profile')
                    ->where('psp.workinginformation = :workinginformation
                            AND psp.pollscheduling = :pollscheduling')
                    ->setParameter('workinginformation', $wi['id'])
                    ->setParameter('pollscheduling', $pollid)
                    ->getQuery()
                    ->getResult();
                $entity_application = current($pollschedulingpeople_query);
                
                $profile = $entity_application['profile'];
                
            }
    
            // ACCESS CONTROL
            
            if($pollscheduling->getDeleted()){
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
            if (empty($entity_application)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
            
            // Validate Date in Poll
            $validateDate = Pollscheduling::validateDateInPollscheduling($this->getDoctrine(), $pollscheduling->getInitdate(), $pollscheduling->getEnddate());
            if (!$validateDate['valid']) {
                $session->getFlashBag()->add('error', $validateDate['message']);
                return $this->redirect($this->generateUrl('fishman_front_end_polls'));
            }
            
            // FIN ACCESS CONTROL
            
        }
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling,
            'company' => $company, 
            'entity' => $entity,
            'profile' => $profile
        );

        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Notification:show.html.twig', $array_parameters);
    }
    
    /**
     * Show of detail Report - Follow up Poll.
     *
     */
    public function pollFollowShowAction(Request $request, $pollid)
    {
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        $profile = '';
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('fishman_front_end_poll_notevaluateds_report_follow_show', array(
                'pollid' => $pollid
            )));
        }
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling['entity_type'] != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication) || in_array($profile, array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        // FIN ACCESS CONTROL
        
        // Find Entities
        
        $defaultData = array(
            'evaluated_code' => '', 
            'evaluated_names' => '', 
            'evaluated_surname' => '', 
            'evaluated_lastname' => '', 
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('evaluated_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_lastname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_lastname', 'text', array(
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'evaluated_code' => '', 
            'evaluated_names' => '', 
            'evaluated_surname' => '', 
            'evaluated_lastname' => '', 
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('responsible', 'company'))) {
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid, '', $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollapplications');
        $paginator->setMaxPagerItems(5, 'pollapplications');
        $pollapplications = $paginator->paginate($query, 'pollapplications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollapplications');
        $endPageItem = $paginator->getEndPageItem('pollapplications');
        $totalItems = $paginator->getTotalItems('pollapplications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling, 
            'pollapplications' => $pollapplications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'profile' => $profile,
            'form_data' => $formData
        );

        if ($pollscheduling['entity_type'] == 'workshopscheduling') {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling['entity_id'];
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling['entity_id']);
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Report:follow_show.html.twig', $array_parameters);
    }
    
    /**
     * Export of detail Report - Follow up Poll.
     *
     */
    public function pollFollowShowExportAction($pollid)
    {
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('fishman_front_end_poll_notevaluateds_report_follow_show_export', array(
                'pollid' => $pollid
            )));
        }
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling['entity_type'] != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication) || in_array($profile, array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        // FIN ACCESS CONTROL
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('responsible', 'company'))) {
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid);
        }
        
        $pollapplications = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Detalle de Encuesta");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta las evaluaciones de la encuesta");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Evaluaciones');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Empresa: ' . $pollscheduling['company']);
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Encuesta: ' . $pollscheduling['title'] . ' | ' .$pollscheduling['pollschedulingperiod']);
        
        if (!empty($pollapplications)) {
          
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('Código Evaluado');
            $worksheet->getCellByColumnAndRow(2, 5)->setValue('Evaluado');
            $worksheet->getCellByColumnAndRow(3, 5)->setValue('Correo Evaluado');
            $worksheet->getCellByColumnAndRow(4, 5)->setValue('Código Evaluador');
            $worksheet->getCellByColumnAndRow(5, 5)->setValue('Evaluador');
            $worksheet->getCellByColumnAndRow(6, 5)->setValue('Correo Evaluador');
            $worksheet->getCellByColumnAndRow(7, 5)->setValue('Encuesta');
            
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
    
            $row = 6;
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($pollapplications as $pa){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($pa['evaluated_code']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($pa['evaluated_surname'] . ' ' . $pa['evaluated_lastname'] . ', ' . $pa['evaluated_names']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($pa['evaluated_email']);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($pa['evaluator_code']);
                $worksheet->getCellByColumnAndRow(5, $row)->setValue($pa['evaluator_surname'] . ' ' . $pa['evaluator_lastname'] . ', ' . $pa['evaluator_names']);
                $worksheet->getCellByColumnAndRow(6, $row)->setValue($pa['evaluator_email']);
                $worksheet->getCellByColumnAndRow(7, $row)->setValue($twigExtension->finishedNameFilter($pa['finished']));
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-detalle-encuesta.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
    
    /**
     * Print of detail Report - Follow up Poll.
     *
     */
    public function pollFollowShowPrintAction($pollid)
    {   
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('fishman_front_end_poll_notevaluateds_report_follow_show_print', array(
                'pollid' => $pollid
            )));
        }
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling['entity_type'] != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication) || in_array($profile, array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        // FIN ACCESS CONTROL
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('responsible', 'company'))) {
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid);
        }
        
        $pollapplications = $query->getResult();
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Report:follow_show_print.html.twig', array(
            'pollapplications' => $pollapplications, 
            'poll' => $pollscheduling
        ));
    }
    
    /**
     * Show of detail Report - Follow up Poll not evaluateds.
     *
     */
    public function pollNotevaluatedsFollowShowAction(Request $request, $pollid)
    {
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        $profile = '';
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling['entity_type'] != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication) || in_array($profile, array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        
        // FIN ACCESS CONTROL
        
        // Find Entities
        
        $defaultData = array(
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('evaluator_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_lastname', 'text', array(
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('responsible', 'company'))) {
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid, $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'pollapplications');
        $paginator->setMaxPagerItems(5, 'pollapplications');
        $pollapplications = $paginator->paginate($query, 'pollapplications')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('pollapplications');
        $endPageItem = $paginator->getEndPageItem('pollapplications');
        $totalItems = $paginator->getTotalItems('pollapplications');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Array Parameters

        $array_parameters = array(
            'poll' => $pollscheduling, 
            'pollapplications' => $pollapplications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'profile' => $profile,
            'form_data' => $formData
        );

        if ($pollscheduling['entity_type'] == 'workshopscheduling') {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling['entity_id'];
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling['entity_id']);
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Report/EvaluatorNotEvaluateds:follow_show.html.twig', $array_parameters);
    }
    
    /**
     * Export of detail Report - Follow up Poll.
     *
     */
    public function pollNotevaluatedsFollowShowExportAction($pollid)
    {
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling['entity_type'] != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication) || in_array($profile, array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        
        // FIN ACCESS CONTROL
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('responsible', 'company'))) {
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid);
        }
        
        $pollapplications = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Detalle de Encuesta");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta las evaluaciones de la encuesta");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Evaluaciones');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Empresa: ' . $pollscheduling['company']);
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Encuesta: ' . $pollscheduling['title'] . ' | ' . $pollscheduling['pollschedulingperiod']);
        
        if (!empty($pollapplications)) {
            
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('Código Evaluador');
            $worksheet->getCellByColumnAndRow(2, 5)->setValue('Evaluador');
            $worksheet->getCellByColumnAndRow(3, 5)->setValue('Correo Evaluador');
            $worksheet->getCellByColumnAndRow(4, 5)->setValue('Encuesta');
            
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
    
            $row = 6;
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($pollapplications as $pa){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($pa['evaluator_code']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($pa['evaluator_surname'] . ' ' . $pa['evaluator_lastname'] . ', ' . $pa['evaluator_names']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($pa['evaluator_email']);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($twigExtension->finishedNameFilter($pa['finished']));
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-detalle-encuesta-sin-evaluados.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
    
    /**
     * Print of detail Report - Follow up Poll.
     *
     */
    public function pollNotevaluatedsFollowShowPrintAction($pollid)
    {   
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling['entity_type'] != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication) || in_array($profile, array('integrant', 'collaborator', 'boss'))) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        
        // FIN ACCESS CONTROL
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('responsible', 'company'))) {
            // Recovers Pollapplications
            $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid);
        }
        
        $pollapplications = $query->getResult();
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Report/EvaluatorNotEvaluateds:follow_show_print.html.twig', array(
            'pollapplications' => $pollapplications, 
            'poll' => $pollscheduling
        ));
    }
    
    /**
     * Lists all Report - Result up Poll.
     *
     */
    public function pollResultAction(Request $request, $pollid)
    {   
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
                                
        $workshopschedulingid = '';
        $profile = '';
        $messageError = '';
        $messagePollscheduling = '';

        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        elseif ($pollscheduling->getType() == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling->getEntityType() != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling->getDeleted()) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        // FIN ACCESS CONTROL
        
        // Define Data
        
        $pId = $pollscheduling->getPoll()->getId();
        $pollschedulings = '';
        $evaluateds = '';
        $levels = array(
            '1' => 'Nivel 1', 
            '2' => 'Nivel 2', 
            '3' => 'Nivel 3', 
            '4' => 'Nivel 4', 
            '5' => 'Nivel 5', 
            '6' => 'Nivel 6', 
            '7' => 'Nivel 7', 
            '8' => 'Nivel 8', 
            '9' => 'Nivel 9',
            'null' => 'Ninguno' 
        );
        $pollapplicationquestions = '';
        $ids = '';
        
        $postDataWI = $request->request->get('wi', null);
        $postDataPollschedulings = $request->request->get('asigned_options', null);
        $postDataEvaluated = $request->request->get('options_evaluated', null);
        $postDataLevel = $request->request->get('options_level', null);
        
        // PostData
        if ($postDataWI != '') {
            $postDataPollschedulings = array('data2' => array($pollid));
            $postDataEvaluated = $postDataWI;
            $postDataLevel = 'null';
        }
        
        // Verify if the survey is of a workshop
        
        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $workshopschedulingid = $pollscheduling->getEntityId();
            }
            else {
                $wa = $session->get('workshopapplication');
                $workshopschedulingid = $wa['ws_id'];
            }
        }
        
        $companyId = $pollscheduling->getCompanyId();
        
        // Recovering Data for Profile
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('responsible', 'company'))) {
            // Recovers Pollschedulings
            $pollschedulings = Pollscheduling::getListPollschedulingsByProfile($this->getDoctrine(), $workshopschedulingid, $pId, $companyId);
        }
        elseif (in_array($profile, array('integrant', 'collaborator'))) {
            // Recovers Employees Integrant, Collaborator
            $ids = Workinginformation::getIdWorkinginformationEmployees($this->getDoctrine(), $wi['id']);
            if ($ids != '') {
                $ids = $wi['id'] . ', ' . $ids;
            }
            else {
                $ids = $wi['id'];
            }
            // Recovers Pollschedulings
            $pollschedulings = Pollscheduling::getListPollschedulingsByProfile($this->getDoctrine(), $workshopschedulingid, $pId, $companyId, $ids);
        }
        elseif ($profile == 'boss') {
            // Recovers Employees Integrant, Collaborator
            $ids = Workinginformation::getIdWorkinginformationEmployees($this->getDoctrine(), $wi['id'], TRUE);
            if ($ids != '') {
                $ids = $wi['id'] . ', ' . $ids;
            }
            else {
                $ids = $wi['id'];
            }
            // Recovers Pollschedulings
            $pollschedulings = Pollscheduling::getListPollschedulingsByProfile($this->getDoctrine(), $workshopschedulingid, $pId, $companyId, $ids);
        }
        
        if (!empty($postDataPollschedulings)) {
 
            $pscIds = '';
            if (!empty($postDataPollschedulings['data2'])) {
                $j = 0;
                foreach ($postDataPollschedulings['data2'] as $psc) {
                    if ($j == 0) {
                        $pscIds = $psc;
                    }
                    else {
                        $pscIds = $pscIds . ', ' .$psc;
                    }
                    $j++;
                }
            }
            
            // Recovers Evaluateds
            $evaluateds = Pollapplication::getListPollschedulingEvaluatedsByProfile($this->getDoctrine(), '', $pscIds, $companyId, $ids);
        }
        
        // Array paramteres
        $array_parameters = array(
            'poll' => $pollscheduling,
            'company' => $company, 
            'pollapplicationquestions' => $pollapplicationquestions,
            'pollschedulings' => $pollschedulings,
            'evaluateds' => $evaluateds,
            'levels' => $levels, 
            'pollschedulingsData' => $postDataPollschedulings, 
            'evaluatedData' => $postDataEvaluated, 
            'levelData' => $postDataLevel, 
            'pollschedulingids' => '',
            'profile' => $profile,
            'ids' => $ids
        );
        
        if (!empty($postDataPollschedulings) && !empty($postDataEvaluated) && !empty($postDataLevel)) {
          
            $i = 0;
            $arrayPoll = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $pId);
            
            if (!empty($arrayPoll)) {
                
                $arrayGroupPoll['sections'] = Pollsection::arrayGroupQuestions($this->getDoctrine(), 0, $postDataLevel, $arrayPoll);
                
                // Recover pollscheduling ids
                $p = 0;
                $psc = 0;
                $pscIds = '';
                
                foreach ($postDataPollschedulings['data2'] as $key) {
                    
                    // Recover Pollscheduling
                    $pollschedulingKey = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($key);
                    
                    // Check for participating in the survey
                    $participatingPoll = Pollapplication::checkParticipatingPoll($this->getDoctrine(), $key, $postDataEvaluated);
                    
                    // Verify number of times the assessed evaluated
                    $numberResolvedPolls = Pollapplication::getNumberResolvedPolls($this->getDoctrine(), $key, $postDataEvaluated);
                    
                    if ($participatingPoll) {
                        if (($numberResolvedPolls >= $pollschedulingKey->getAccessViewNumber() 
                            && $pollschedulingKey->getAccessViewNumber() > 0) 
                            || $pollschedulingKey->getMultipleEvaluation() 
                            || $postDataEvaluated != $wi['id']) {
                            if ($p == 0) {
                                $pscIds = $key;
                            }
                            else {
                                $pscIds .= ', ' . $key;
                            }
                            $p++;
                        }
                        else {
                            $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO DISPONIBLE)' . '</br>';
                            
                            unset($postDataPollschedulings['data2'][$psc]);
                        }
                    }
                    else {
                        $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO PARTICIPA)' . '</br>';
                        
                        unset($postDataPollschedulings['data2'][$psc]);
                    }
                    $psc++;
                }
                
                $array_parameters['pollschedulingids'] = $pscIds;
                
                if (!empty($postDataPollschedulings['data2'])) {
                  
                    //Data to display polls resolved
                    foreach ($postDataPollschedulings['data2'] as $key) {
                        
                        $array =  $arrayGroupPoll;
                        
                        // Recover Pollapplicationquestions
                        $pollapplicationquestions = Pollapplicationquestion::getResultPollapplicationquestionByPollscheduling($this->getDoctrine(), $postDataEvaluated, $key);
                        
                        if (!empty($pollapplicationquestions)) {
                            
                            $pscs[$i] = $key;
                            
                            $pollschedulingReport = Pollscheduling::calculatePercentageSection($array, $pollapplicationquestions);
                            
                            $pollschedulingTotals[$i] = array(
                                'percentage' => $pollschedulingReport['percentage']['consolidate'], 
                                'count' => $pollschedulingReport['percentage']['count']
                            );
                            
                            if ($postDataLevel != 'null') {
                                $pollschedulingPercentages[$i] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections'], TRUE);
                            }
                            else {
                                $pollschedulingPercentages[$i] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections']);
                            }
                            
                            // Recover Number Evaluators by Evaluated
                            $numberEvaluators[$i] = Pollapplication::getNumberEvaluatorsByEvaluated($this->getDoctrine(), $key, $postDataEvaluated);
                            
                            // Ranking Top 25%
                            $rankingTop25[$i] = Pollapplicationquestion::getResultPollapplicationquestionTopAverage($this->getDoctrine(), 25, $key, $array);
                            
                            $i++;
                        }
                        else {
                            $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO HA SIDO EVALUADO)' . '</br>';
                        }
                    }

                }
                
                if (!empty($pollapplicationquestions)) {
                    
                    // Headers
                    $k = 0;
                    if (!empty($pscs)) {
                        foreach ($pscs as $psc) {
                            foreach ($pollschedulings as $ps) {
                                if ($psc == $ps['id']) {
                                    $pollschedulingHeaders[$k] = 'Encuesta ' . $ps['key'];
                                }
                            }
                            $k++;
                        }
                    }
        
                    // Titles
                            
                    if ($postDataLevel != 'null') {
                        $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections'], TRUE);
                    }
                    else {
                        $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections']);
                    }
                    
                    // Count report vertical
                    $countVertical = count($pollschedulingPercentages[0]);
                    
                    // Count report horizontal
                    $countHorizontal = $i;
                    
                    // Total Rows
                    for ($j = 0; $j < $countVertical; $j++) {
                        $percentageTotalRow = 0;
                        for ($n = 0; $n < $countHorizontal; $n++) {
                            $percentageTotalRow += $pollschedulingPercentages[$n][$j]['percentage'];
                        }
                        $pollschedulingPercentageRows[$j] = $percentageTotalRow / $countHorizontal;
                    }
                    
                    // Row Results
                    $row_total = 0;
                    foreach ($pollschedulingTotals as $total) {
                        $row_total += $total['percentage'];
                    }
                    $pollschedulingTotalRows = $row_total / $countHorizontal;
                    
                    // Total Ranking Top 25%
                    $pscIds = Pollscheduling::getPollschedulingIdsByPoll($this->getDoctrine(), $pId, $pollscheduling->getCompanyId());
                    $rankingTop25Total = Pollapplicationquestion::getResultPollapplicationquestionTopAverage($this->getDoctrine(), 25, $pscIds, $array);
                    
                    $array_parameters['header_results'] = $pollschedulingHeaders;
                    $array_parameters['title_results'] = $pollschedulingTitles;
                    $array_parameters['number_evaluators'] = $numberEvaluators;
                    $array_parameters['report_results'] = $pollschedulingPercentages;
                    $array_parameters['report_rows'] = $pollschedulingPercentageRows;
                    $array_parameters['total_results'] = $pollschedulingTotals;
                    $array_parameters['row_results'] = $pollschedulingTotalRows;
                    $array_parameters['count_horizontal'] = $countHorizontal;
                    $array_parameters['count_vertical'] = $countVertical;
                    $array_parameters['ranking_top'] = $rankingTop25;
                    $array_parameters['ranking_top_total'] = $rankingTop25Total;
                    
                }
                
                if ($messagePollscheduling != '') {
                    $messageError = $messagePollscheduling;
                }
                
            }
            else {
                $messageError = 'Actualmente la encuesta está sin preguntas.';
            }
            
        }

        if ($pollscheduling->getEntityType() == 'workshopscheduling') {
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $array_parameters['workshopid'] = $pollscheduling->getEntityId();
            }
            else {
                // Recover Workshopapplication
                $workshopapplication = Workshopapplication::getCurrentWorshopapplicationToWorkshopscheduling($this->getDoctrine(), $wi['id'], $pollscheduling->getEntityId());
                $array_parameters['workshopid'] = $workshopapplication->getId();
            }
        }
        
        if ($messageError != '') {
            $session->getFlashBag()->add('error', $messageError);
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Poll/Report:result.html.twig', $array_parameters);
    }
    
    /**
     * Export Result of the Report Poll.
     *
     */
    public function pollResultExportAction($pollschedulingids, $evaluatedid, $pollid = '')
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');

        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling->getEntityType() != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling->getDeleted()) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollid
            )));
        }
        // FIN ACCESS CONTROL
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('boss', 'responsible', 'company'))) {
            
            if (!empty($pollschedulingids) && !empty($evaluatedid)) {

                // Recovers Pollapplicationreports
                $pollapplicationreports = Pollapplicationreport::getListPollapplicationreportResults($this->getDoctrine(), $pollschedulingids, $evaluatedid);
                
                // Recover Company
                $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
                
                $content = '';
                $response = new Response();
                
                $content .= 'Reporte:' . ';' . 'Reporte de resultado de Encuesta' . "\r\n";
                $content .= 'Empresa:' . ';' . $company->getName() . "\r\n";
                $content .= 'Encuesta:' . ';' . $pollscheduling->getTitle() . ';' . $pollscheduling->getVersion() . ' | ' . $pollscheduling->getPollschedulingPeriod() . "\r\n";
                $content .= "\r\n";
                
                if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                    $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $company);
                }
                else {
                    $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $company, FALSE);
                }
                
                $response->setContent($content);
                
                $response->headers->set('Content-Type', 'text/csv');
                $response->headers->set('Content-Disposition', 'attachment;filename="reporte-resultado-encuesta.csv"');
                
                return $response;
            }
        }
        exit;
    }

    /**
     * Lists all Report - Follow Company.
     *
     */
    public function reportFollowAction(Request $request)
    {
        // Recovering data
        
        $status_options = array('ruc' => 'Desactivo', 1 => 'Activo');
        $nature_options = Nature::getListNatureOptions($this->getDoctrine());
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'nature' => '', 
            'status' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('nature', 'choice', array(
                'choices' => $nature_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('status', 'choice', array(
                'choices' => $status_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'nature' => '', 
            'status' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovers Companys
        $query = Company::getListCompaniesFollow($this->getDoctrine(), $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'reportcompanies');
        $paginator->setMaxPagerItems(5, 'reportcompanies');
        $companies = $paginator->paginate($query, 'reportcompanies')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('reportcompanies');
        $endPageItem = $paginator->getEndPageItem('reportcompanies');
        $totalItems = $paginator->getTotalItems('reportcompanies');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:follow.html.twig', array(
            'companies' => $companies,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Lists all Report - Follow Company Polls.
     *
     */
    public function reportFollowPollAction(Request $request, $companyid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        
        // FIN ACCESS CONTROL
               
        // Recovering data
        
        $period_options = array('day' => 'Días', 'week' => 'Semanas', 'month' => 'Meses');
        for ($i = 1; $i<= 20; $i++) {
            $sequence_options[$i] = $i;
        }
        
        // Find Entities
        
        $defaultData = array(
            'sequence' => '', 
            'word' => '', 
            'resolved' => '',
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('sequence', 'choice', array(
                'choices' => $sequence_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('resolved', 'text', array(
                'required' => FALSE
            ))
            ->add('duration', 'text', array(
                'required' => FALSE
            ))
            ->add('period', 'choice', array(
                'choices' => $period_options, 
                'empty_value' => 'Choose an option',
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'sequence' => '', 
            'word' => '', 
            'resolved' => '',
            'duration' => '', 
            'period' => '', 
            'initdate' => NULL, 
            'enddate' => NULL
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovers Pollschedulings
        $query = Pollscheduling::getListPollschedulingsByCompanyFollow($this->getDoctrine(), $companyid, $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'reportcompanypolls');
        $paginator->setMaxPagerItems(5, 'reportcompanypolls');
        $polls = $paginator->paginate($query, 'reportcompanypolls')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('reportcompanypolls');
        $endPageItem = $paginator->getEndPageItem('reportcompanypolls');
        $totalItems = $paginator->getTotalItems('reportcompanypolls');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:follow_poll.html.twig', array(
            'company' => $company, 
            'polls' => $polls,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Export all Report - Follow Company Polls.
     *
     */
    public function reportFollowPollExportAction($companyid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        
        // Recovers Pollschedulings
        $query = Pollscheduling::getListPollschedulingsByCompanyFollow($this->getDoctrine(), $companyid);
        
        $polls = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Encuestas");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta la lista de encuestas de la empresa");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Encuestas');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Empresa: ' . $company->getName());

        if (!empty($polls)) {
          
            $worksheet->getCellByColumnAndRow(1, 4)->setValue('Nombre');
            $worksheet->getCellByColumnAndRow(2, 4)->setValue('Resuelto');
            $worksheet->getCellByColumnAndRow(3, 4)->setValue('Duración');
            $worksheet->getCellByColumnAndRow(4, 4)->setValue('Inicio');
            $worksheet->getCellByColumnAndRow(5, 4)->setValue('Fin');
            $worksheet->getCellByColumnAndRow(6, 4)->setValue('Avance');
            
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
            $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
            $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
    
            $row = 5;
    
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($polls as $poll){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($poll['title']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($poll['time_number']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($poll['duration'] . ' ' .
                                        $twigExtension->periodNameFilter($poll['period']));
    
                if(!is_null($poll['initdate'])){
                    $worksheet->getCellByColumnAndRow(4, $row)->setValue(
                                 PHPExcel_Shared_Date::PHPToExcel($poll['initdate']->getTimestamp()));
                    $worksheet->getStyleByColumnAndRow(4, $row)
                                 ->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');
                }
    
                if(!is_null($poll['enddate'])){
                    $worksheet->getCellByColumnAndRow(5, $row)->setValue(
                                 PHPExcel_Shared_Date::PHPToExcel($poll['enddate']->getTimestamp()));
                    $worksheet->getStyleByColumnAndRow(5, $row)
                                 ->getNumberFormat()->setFormatCode('dd/mm/yyyy h:mm');                
                }
    
                $worksheet->getCellByColumnAndRow(6, $row)->setValue((int)$poll['percentaje'] . '%');
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 4)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-encuestas-empresa.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }

    /**
     * Print all Report - Follow Company Polls.
     *
     */
    public function reportFollowPollPrintAction($companyid = '')
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $polls = '';
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        
        // Recovers Pollschedulings
        $query = Pollscheduling::getListPollschedulingsByCompanyFollow($this->getDoctrine(), $companyid);
        
        // Recovers Pollschedulings
        
        if (!empty($query)) {
            $polls = $query->getResult();
        }
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:follow_poll_print.html.twig', array(
            'company' => $company, 
            'polls' => $polls
        ));
    }
    
    /**
     * Show of detail Report - Follow Company Poll.
     *
     */
    public function reportFollowPollShowAction(Request $request, $companyid, $pollid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow_poll_notevaluateds_show', array(
                'companyid' => $companyid, 
                'pollid' => $pollid
            )));
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if ($pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow', array(
                'companyid' => $companyid
            )));
        }
        
        // FIN ACCESS CONTROL
        
        // Find Entities
        
        $defaultData = array(
            'evaluated_code' => '', 
            'evaluated_names' => '', 
            'evaluated_surname' => '', 
            'evaluated_lastname' => '', 
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('evaluated_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluated_lastname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_lastname', 'text', array(
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'evaluated_code' => '', 
            'evaluated_names' => '', 
            'evaluated_surname' => '', 
            'evaluated_lastname' => '', 
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovers Pollapplications
        $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid, '', $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'reportcompanypollshow');
        $paginator->setMaxPagerItems(5, 'reportcompanypollshow');
        $pollapplications = $paginator->paginate($query, 'reportcompanypollshow')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('reportcompanypollshow');
        $endPageItem = $paginator->getEndPageItem('reportcompanypollshow');
        $totalItems = $paginator->getTotalItems('reportcompanypollshow');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return

        return $this->render('FishmanFrontEndBundle:Dashboard/Report:follow_poll_show.html.twig', array(
            'company' => $company,  
            'poll' => $pollscheduling, 
            'pollapplications' => $pollapplications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }
    
    /**
     * Export of detail Report - Follow Company Poll.
     *
     */
    public function reportFollowPollShowExportAction($companyid, $pollid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow_poll_notevaluateds_show', array(
                'companyid' => $companyid, 
                'pollid' => $pollid
            )));
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if ($pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow', array(
                'companyid' => $companyid
            )));
        }
        
        // Recovers Pollapplications
        $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid);
        
        $pollapplications = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Detalle de Encuesta");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta las evaluaciones de la encuesta");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Evaluaciones');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Empresa: ' . $company->getName());
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Encuesta: ' . $pollscheduling['title'] . ' | ' . $pollscheduling['pollschedulingperiod']);
        
        if (!empty($pollapplications)) {
          
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('Código Evaluado');
            $worksheet->getCellByColumnAndRow(2, 5)->setValue('Evaluado');
            $worksheet->getCellByColumnAndRow(3, 5)->setValue('Correo Evaluado');
            $worksheet->getCellByColumnAndRow(4, 5)->setValue('Código Evaluador');
            $worksheet->getCellByColumnAndRow(5, 5)->setValue('Evaluador');
            $worksheet->getCellByColumnAndRow(6, 5)->setValue('Correo Evaluador');
            $worksheet->getCellByColumnAndRow(7, 5)->setValue('Encuesta');
            
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
            
            $row = 6;
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($pollapplications as $pa){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($pa['evaluated_code']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($pa['evaluated_surname'] . ' ' . $pa['evaluated_lastname'] . ', ' . $pa['evaluated_names']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($pa['evaluated_email']);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($pa['evaluator_code']);
                $worksheet->getCellByColumnAndRow(5, $row)->setValue($pa['evaluator_surname'] . ' ' . $pa['evaluator_lastname'] . ', ' . $pa['evaluator_names']);
                $worksheet->getCellByColumnAndRow(6, $row)->setValue($pa['evaluator_email']);
                $worksheet->getCellByColumnAndRow(7, $row)->setValue($twigExtension->finishedNameFilter($pa['finished']));
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 6)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-detalle-encuesta-empresa.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
    
    /**
     * Print of detail Report - Follow Company Poll.
     *
     */
    public function reportFollowPollShowPrintAction($companyid, $pollid)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        elseif ($pollscheduling['type'] == 'not_evaluateds') {
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow_poll_notevaluateds_show', array(
                'companyid' => $companyid, 
                'pollid' => $pollid
            )));
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if ($pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow', array(
                'companyid' => $companyid
            )));
        }
        
        // Recovers Pollapplications
        $query = Pollapplication::getListPollapplicationsFollow($this->getDoctrine(), $pollid);
        
        $pollapplications = $query->getResult();

        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:follow_poll_show_print.html.twig', array(
            'company' => $company,  
            'poll' => $pollscheduling, 
            'pollapplications' => $pollapplications
        ));
    }
    
    /**
     * Show of detail Report - Follow up Poll not evaluateds.
     *
     */
    public function reportFollowPollNotevaluatedsShowAction(Request $request, $companyid, $pollid)
    {
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if ($pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow', array(
                'companyid' => $companyid
            )));
        }
        
        // FIN ACCESS CONTROL
        
        // Find Entities
        
        $defaultData = array(
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('evaluator_code', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_names', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_surname', 'text', array(
                'required' => FALSE
            ))
            ->add('evaluator_lastname', 'text', array(
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'evaluator_code' => '', 
            'evaluator_names' => '', 
            'evaluator_surname' => '', 
            'evaluator_lastname' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovering Pollapplications
        $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid, $data);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'reportcompanypollnotevaluatedsshow');
        $paginator->setMaxPagerItems(5, 'reportcompanypollnotevaluatedsshow');
        $pollapplications = $paginator->paginate($query, 'reportcompanypollnotevaluatedsshow')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('reportcompanypollnotevaluatedsshow');
        $endPageItem = $paginator->getEndPageItem('reportcompanypollnotevaluatedsshow');
        $totalItems = $paginator->getTotalItems('reportcompanypollnotevaluatedsshow');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Report/EvaluatorNotEvaluateds:follow_poll_show.html.twig', array(
            'company' => $company, 
            'poll' => $pollscheduling, 
            'pollapplications' => $pollapplications,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }
    
    /**
     * Export of detail Report - Follow up Poll.
     *
     */
    public function reportFollowPollNotevaluatedsShowExportAction($companyid, $pollid)
    {
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if ($pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow', array(
                'companyid' => $companyid
            )));
        }
        
        // FIN ACCESS CONTROL
        
        // Recovers Pollapplications
        $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid);
        
        $pollapplications = $query->getResult();
        
        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Detalle de Encuesta");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta las evaluaciones de la encuesta");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('Evaluaciones');

        $worksheet = $phpExcel->getSheet(0);
        $worksheet->getCellByColumnAndRow(1, 2)->setValue('Empresa: ' . $company->getName());
        $worksheet->getCellByColumnAndRow(1, 3)->setValue('Encuesta: ' . $pollscheduling['title'] . ' | ' . $pollscheduling['pollschedulingperiod']);
        
        if (!empty($pollapplications)) {
            
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('Código Evaluador');
            $worksheet->getCellByColumnAndRow(2, 5)->setValue('Evaluador');
            $worksheet->getCellByColumnAndRow(3, 5)->setValue('Correo Evaluador');
            $worksheet->getCellByColumnAndRow(4, 5)->setValue('Encuesta');
            
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
    
            $row = 6;
            $twigExtension = new FishmanExtension($this->container->get('kernel'));
            foreach($pollapplications as $pa){
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($pa['evaluator_code']);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($pa['evaluator_surname'] . ' ' . $pa['evaluator_lastname'] . ', ' . $pa['evaluator_names']);
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($pa['evaluator_email']);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($twigExtension->finishedNameFilter($pa['finished']));
                
                $row++;
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-seguimiento-detalle-encuesta-empresa-sin-evaluados.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
    
    /**
     * Print of detail Report - Follow up Poll.
     *
     */
    public function reportFollowPollNotevaluatedsShowPrintAction($companyid, $pollid)
    {   
        // Recover session varaible
        
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Recover Company
        $company = $this->getDoctrine()->getRepository('FishmanEntityBundle:Company')->find($companyid);
        
        // Recover Pollscheduling
        
        $pollscheduling = Pollscheduling::getCurrentPollschedulingFollow($this->getDoctrine(), $pollid);
        if (!$pollscheduling || ($pollscheduling['anonymous'] && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if (!$company) {
            $session->getFlashBag()->add('error', 'La Empresa no existe o no contiene evaluaciones de encuestas');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        if ($pollscheduling['deleted']) {
            $session->getFlashBag()->add('error', 'La Encuesta que está solicitando ha sido borrada');
            return $this->redirect($this->generateUrl('fishman_front_end_report_follow', array(
                'companyid' => $companyid
            )));
        }
        
        // FIN ACCESS CONTROL
        
        // Recovers Pollapplications
        $query = Pollapplication::getListPollapplicationsNotEvaluatedsFollow($this->getDoctrine(), $pollid);
        
        $pollapplications = $query->getResult();
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report/EvaluatorNotEvaluateds:follow_poll_show_print.html.twig', array(
            'company' => $company,  
            'poll' => $pollscheduling, 
            'pollapplications' => $pollapplications
        ));
    }
    
    /**
     * Result of the Report.
     *
     */
    public function reportResultAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        
        // FIN ACCESS CONTROL
        
        $messageError = '';
        $messagePollscheduling = '';
        
        $companies = '';
        $polls = '';
        $pollschedulings = '';
        $evaluateds = '';
        $levels = array(
            '1' => 'Nivel 1', 
            '2' => 'Nivel 2', 
            '3' => 'Nivel 3', 
            '4' => 'Nivel 4', 
            '5' => 'Nivel 5', 
            '6' => 'Nivel 6', 
            '7' => 'Nivel 7', 
            '8' => 'Nivel 8', 
            '9' => 'Nivel 9',
            'null' => 'Ninguno' 
        );
        $pollapplicationquestions = '';
        
        $postDataCompany = '';
        $postDataPoll = ''; 
        $postDataPollschedulings = '';
        $postDataEvaluated = '';
        $postDataLevel = '';
        $profileTemp = '';
        
        // PostData
        $postDataCompany = $request->request->get('options_company', null);
        $postDataPoll = $request->request->get('options_poll', null);
        $postDataPollschedulings = $request->request->get('asigned_options', null);
        $postDataEvaluated = $request->request->get('options_evaluated', null);
        $postDataLevel = $request->request->get('options_level', null);
        
        // Recovers Companies
        $companies = Pollscheduling::getListCompaniesByProfile($this->getDoctrine());
        
        if (!empty($postDataCompany)) {
            
            // Recovers Polls
            $polls = Pollscheduling::getListPollsByProfile($this->getDoctrine(), '', $postDataCompany);
            
            // Recovers Pollschedulings
            $pollschedulings = Pollscheduling::getListPollschedulingsByProfile($this->getDoctrine(), '', $postDataPoll, $postDataCompany);
 
            $pscIds = '';
            if (!empty($postDataPollschedulings['data2'])) {
                $j = 0;
                foreach ($postDataPollschedulings['data2'] as $psc) {
                    if ($j == 0) {
                        $pscIds = $psc;
                    }
                    else {
                        $pscIds = $pscIds . ', ' .$psc;
                    }
                    $j++;
                }
            }
            
            // Recovers Evaluateds
            $evaluateds = Pollapplication::getListPollschedulingEvaluatedsByProfile($this->getDoctrine(), '', $pscIds, $postDataCompany);
        }
        
        // Array paramteres
        $array_parameters = array(
            'pollapplicationquestions' => $pollapplicationquestions,
            'companies' => $companies, 
            'polls' => $polls,  
            'pollschedulings' => $pollschedulings,
            'evaluateds' => $evaluateds,
            'levels' => $levels, 
            'companyData' => $postDataCompany,
            'pollData' => $postDataPoll,
            'pollschedulingsData' => $postDataPollschedulings, 
            'evaluatedData' => $postDataEvaluated, 
            'levelData' => $postDataLevel, 
            'pollschedulingids' => ''
        );
        
        if (!empty($postDataPollschedulings) && !empty($postDataEvaluated) && !empty($postDataLevel)) {
            $i = 0;
            $arrayPoll = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $postDataPoll);
            
            if ($arrayPoll) {
                
                $arrayGroupPoll['sections'] = Pollsection::arrayGroupQuestions($this->getDoctrine(), 0, $postDataLevel, $arrayPoll);
                
                // Recover pollscheduling ids
                $p = 0;
                $psc = 0;
                $pscIds = '';
                
                foreach ($postDataPollschedulings['data2'] as $key) {
                    
                    // Recover Pollscheduling
                    $pollschedulingKey = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($key);
                    
                    // Check for participating in the survey
                    $participatingPoll = Pollapplication::checkParticipatingPoll($this->getDoctrine(), $key, $postDataEvaluated);
                    
                    // Verify number of times the assessed evaluated
                    $numberResolvedPolls = Pollapplication::getNumberResolvedPolls($this->getDoctrine(), $key, $postDataEvaluated);
                    
                    if ($participatingPoll) {
                        if (($numberResolvedPolls >= $pollschedulingKey->getAccessViewNumber() 
                            && $pollschedulingKey->getAccessViewNumber() > 0) 
                            || $pollschedulingKey->getMultipleEvaluation()) {
                            if ($p == 0) {
                                $pscIds = $key;
                            }
                            else {
                                $pscIds .= ', ' . $key;
                            }
                            $p++;
                        }
                        else {
                            $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO DISPONIBLE)' . '</br>';
                            
                            unset($postDataPollschedulings['data2'][$psc]);
                        }
                    }
                    else {
                        $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO PARTICIPA)' . '</br>';
                        
                        unset($postDataPollschedulings['data2'][$psc]);
                    }
                    $psc++;
                }

                $array_parameters['pollschedulingids'] = $pscIds;
                
                if (!empty($postDataPollschedulings['data2'])) {
                    
                    //Data to display polls resolved
                    foreach ($postDataPollschedulings['data2'] as $key) {
                        
                        $array =  $arrayGroupPoll;
                        
                        // Recover Pollapplicationquestions
                        $pollapplicationquestions = Pollapplicationquestion::getResultPollapplicationquestionByPollscheduling($this->getDoctrine(), $postDataEvaluated, $key);
                        
                        if (!empty($pollapplicationquestions)) {
                            
                            $pscs[$i] = $key;
                            
                            $pollschedulingReport = Pollscheduling::calculatePercentageSection($array, $pollapplicationquestions);
                            
                            $pollschedulingTotals[$i] = array(
                                'percentage' => $pollschedulingReport['percentage']['consolidate'], 
                                'count' => $pollschedulingReport['percentage']['count']
                            );
                            
                            if ($postDataLevel != 'null') {
                                $pollschedulingPercentages[$i] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections'], TRUE);
                            }
                            else {
                                $pollschedulingPercentages[$i] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections']);
                            }
                            
                            // Recover Number Evaluators by Evaluated
                            $numberEvaluators[$i] = Pollapplication::getNumberEvaluatorsByEvaluated($this->getDoctrine(), $key, $postDataEvaluated);
                            
                            // Ranking Top 25%
                            $rankingTop25[$i] = Pollapplicationquestion::getResultPollapplicationquestionTopAverage($this->getDoctrine(), 25, $key, $array);
                            
                            $i++;
                        }
                        else {
                            $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO HA SIDO EVALUADO)' . '</br>';
                        }
        
                    }
                    
                }
                
                if (!empty($pollapplicationquestions)) {
                    
                    // Headers
                    $k = 0;
                    if (!empty($pscs)) {
                        foreach ($pscs as $psc) {
                            foreach ($pollschedulings as $ps) {
                                if ($psc == $ps['id']) {
                                    $pollschedulingHeaders[$k] = 'Encuesta ' . $ps['key'];
                                }
                            }
                            $k++;
                        }
                    }
        
                    // Titles
                            
                    if ($postDataLevel != 'null') {
                        $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections'], TRUE);
                    }
                    else {
                        $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections']);
                    }
                    
                    // Count report vertical
                    $countVertical = count($pollschedulingPercentages[0]);
                    
                    // Count report horizontal
                    $countHorizontal = $i;
                    
                    // Total Rows
                    for ($j = 0; $j < $countVertical; $j++) {
                        $percentageTotalRow = 0;
                        for ($n = 0; $n < $countHorizontal; $n++) {
                            $percentageTotalRow += $pollschedulingPercentages[$n][$j]['percentage'];
                        }
                        $pollschedulingPercentageRows[$j] = $percentageTotalRow / $countHorizontal;
                    }
                    
                    // Row Results
                    $row_total = 0;
                    foreach ($pollschedulingTotals as $total) {
                        $row_total += $total['percentage'];
                    }
                    $pollschedulingTotalRows = $row_total / $countHorizontal;
                    
                    // Total Ranking Top 25%
                    $pscIds = Pollscheduling::getPollschedulingIdsByPoll($this->getDoctrine(), $postDataPoll, $postDataCompany);
                    $rankingTop25Total = Pollapplicationquestion::getResultPollapplicationquestionTopAverage($this->getDoctrine(), 25, $pscIds, $array);
                    
                    $array_parameters['header_results'] = $pollschedulingHeaders;
                    $array_parameters['title_results'] = $pollschedulingTitles;
                    $array_parameters['number_evaluators'] = $numberEvaluators;
                    $array_parameters['report_results'] = $pollschedulingPercentages;
                    $array_parameters['report_rows'] = $pollschedulingPercentageRows;
                    $array_parameters['total_results'] = $pollschedulingTotals;
                    $array_parameters['row_results'] = $pollschedulingTotalRows;
                    $array_parameters['count_horizontal'] = $countHorizontal;
                    $array_parameters['count_vertical'] = $countVertical;
                    $array_parameters['ranking_top'] = $rankingTop25;
                    $array_parameters['ranking_top_total'] = $rankingTop25Total;
                    
                }
                
                if ($messagePollscheduling != '') {
                    $messageError = $messagePollscheduling;
                }
                
            }
            else {
                $messageError = 'Actualmente la encuesta está sin preguntas.';
            }
            
        }
        
        if ($messageError != '') {
            $session->getFlashBag()->add('error', $messageError);
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:result.html.twig', $array_parameters);
    }
    
    /**
     * Export Result of the Report.
     *
     */
    public function reportResultExportAction($pollschedulingids, $evaluatedid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
        
            if (!empty($pollschedulingids) && !empty($evaluatedid)) {
                
                $pscId = explode(', ',  $pollschedulingids);
                
                // Recover Workshopscheduling
                $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pscId[0]);
                if (!$pollscheduling) {
                    $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
                    return $this->redirect($this->generateUrl('fishman_front_end_polls'));
                }
                
                // Recover Company
                $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
                if (!$company) {
                    $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
                    return $this->redirect($this->generateUrl('fishman_front_end_polls'));
                }
                
                // Recovers Pollapplicationreports
                $pollapplicationreports = Pollapplicationreport::getListPollapplicationreportResults($this->getDoctrine(), $pollschedulingids, $evaluatedid);
                
                $content = '';
                $response = new Response();
                
                $content .= 'Reporte:' . ';' . 'Benchmarking Externo' . "\r\n";
                $content .= 'Empresa:' . ';' . $company->getName() . "\r\n";
                $content .= 'Encuesta:' . ';' . $pollscheduling->getTitle() . ';' . $pollscheduling->getVersion() . ' | ' . $pollscheduling->getPollschedulingPeriod() . "\r\n";
                $content .= "\r\n";
                
                if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                    $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $company);
                }
                else {
                    $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $company, FALSE);
                }
                
                $response->setContent($content);
                
                $response->headers->set('Content-Type', 'text/csv');
                $response->headers->set('Content-Disposition', 'attachment;filename="reporte-resultado-encuesta.csv"');
                
                return $response;
            }
        }
        exit;
    }
    
    /**
     * Result of the Report.
     *
     */
    public function reportBenchmarkingInternalAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $user = $this->get('security.context')->getToken()->getUser();
        $date = new \DateTime();
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            
            $permission = Permission::checkPermissionToPollschedulingBenchmarking($this->getDoctrine(), $user->getId(), 'internal', $date);
            
            if (!$permission) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
            }
        }
        // FIN ACCESS CONTROL
        
        $messageError = '';
        $messagePollscheduling = '';
        $countVertical = '';
        $countHorizontal = '';
        $pollapplicationquestions = '';
        $pollschedulingHeaders = '';
        $pollschedulingPercentages = '';
        $pollschedulingTotals = '';
        $numberEvaluators = '';
        $rankingTop25 = '';
        $key = '';
        $typeFilter = '';
        
        // PostData
        $postDataCompany = $request->request->get('options_company', null);
        $postDataPoll = $request->request->get('options_poll', null);
        $postDataLevel = $request->request->get('options_level', null);
        $postDataPollscheduling = $request->request->get('options_pollscheduling', null);
        $postDataFilter = $request->request->get('options_filter', null);
        $postDataOrganizationalUnits = $request->request->get('organizationalunits_options', null);
        $postDataCharges = $request->request->get('charges_options', null);
        $postDataHeadquarters = $request->request->get('headquarters_options', null);
        $postDataEvaluateds = $request->request->get('evaluateds_options', null);
        $postDataFaculties = $request->request->get('faculties_options', null);
        $postDataCareers = $request->request->get('careers_options', null);
        $postDataGrades = $request->request->get('grades_options', null);
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            
            $pscIds = Pollscheduling::getPollschedulingsWithPermissionByUser($this->getDoctrine(), $user->getId(), '', $date);
            
            // Companies
            $selectCompanies = Pollscheduling::getWidgetCompanies(
                $this->getDoctrine(),
                $rol,
                $postDataCompany, 
                $pscIds
            );
            
            // Polls
            $selectPolls = Pollscheduling::getWidgetPollsByCompany(
                $this->getDoctrine(),
                $rol,
                $postDataCompany,
                $postDataPoll,
                $pscIds
            );
            
            // PollschedulingPeriods
            $selectPollschedulingPeriods = Pollscheduling::getWidgetPollschedulingPeriodsByPoll(
                $this->getDoctrine(),
                $rol, 
                $postDataPoll, 
                $postDataCompany,
                $postDataPollscheduling,
                $pscIds
            );
            
        }
        else {
            
            // Companies
            $selectCompanies = Pollscheduling::getWidgetCompanies(
                $this->getDoctrine(), 
                $rol, 
                $postDataCompany
            );
            
            // Polls
            $selectPolls = Pollscheduling::getWidgetPollsByCompany(
                $this->getDoctrine(), 
                $rol, 
                $postDataCompany,
                $postDataPoll
            );
            
            // PollschedulingPeriods
            $selectPollschedulingPeriods = Pollscheduling::getWidgetPollschedulingPeriodsByPoll(
                $this->getDoctrine(), 
                $rol,  
                $postDataPoll, 
                $postDataCompany,
                $postDataPollscheduling
            );
            
        }
        
        // Levels
        $selectLevels = Pollscheduling::getWidgetLevels(
            $postDataLevel
        );
        
        // Filters
        $selectFilters = Company::getWidgetFilterOptions(
            $this->getDoctrine(), 
            $postDataCompany, 
            $postDataPollscheduling, 
            $postDataFilter
        );
        
        // OrganizationalUnits
        $selectOrganizationalUnits = Company::getWidgetMultipleOrganizationalUnitsByCompany(
            $this->getDoctrine(),
            $postDataCompany,
            $postDataOrganizationalUnits
        );
        
        // Charges
        $selectCharges = Company::getWidgetMultipleChargesByCompany(
            $this->getDoctrine(),
            $postDataCompany,
            $postDataCharges
        );
        
        // Headquartes
        $selectHeadquarters = Company::getWidgetMultipleHeadquartersByCompany(
            $this->getDoctrine(),
            $postDataCompany,
            $postDataHeadquarters
        );
        
        // Evaluateds
        $selectEvaluateds = Pollscheduling::getWidgetMultipleEvaluatedsByPollscheduling(
            $this->getDoctrine(),
            $postDataPollscheduling,
            $postDataEvaluateds
        );
        
        // Faculties
        $selectFaculties = Company::getWidgetMultipleFacultiesByCompany(
            $this->getDoctrine(),
            $postDataCompany,
            $postDataFaculties
        );
        
        // Careers
        $selectCareers = Company::getWidgetMultipleCareersByCompany(
            $this->getDoctrine(),
            $postDataCompany,
            $postDataCareers
        );
        
        // Grades
        $selectGrades = Company::getWidgetMultipleGradesByCompany(
            $this->getDoctrine(),
            $postDataCompany,
            $postDataGrades
        );
        
        // Array paramteres
        $array_parameters = array(
            'pollapplicationquestions' => $pollapplicationquestions, 
            'selectCompanies' => $selectCompanies, 
            'selectPolls' => $selectPolls, 
            'selectLevels' => $selectLevels, 
            'selectPollschedulingPeriods' => $selectPollschedulingPeriods, 
            'selectFilters' => $selectFilters, 
            'selectOrganizationalUnits' => $selectOrganizationalUnits, 
            'selectCharges' => $selectCharges, 
            'selectHeadquarters' => $selectHeadquarters, 
            'selectEvaluateds' => $selectEvaluateds, 
            'selectFaculties' => $selectFaculties, 
            'selectCareers' => $selectCareers, 
            'selectGrades' => $selectGrades, 
            'companyData' => $postDataCompany, 
            'pollschedulingid' => '',
            'typefilter' => '',
            'filterids' => ''
        );
        
        // Recover Filters selecteds
        
        $dataFilters = array();
        $typeFilter = '';
        
        if ($postDataFilter == 'organizationalunits' && isset($postDataOrganizationalUnits['data2'])) {
            $dataFilters = $postDataOrganizationalUnits;
            $typeFilter = 'organizationalunits';
            $entityFilters = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $postDataCompany);
        }
        elseif ($postDataFilter == 'charges' && isset($postDataCharges['data2'])) {
            $dataFilters = $postDataCharges;
            $typeFilter = 'charges';
            $entityFilters = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $postDataCompany);
        }
        elseif ($postDataFilter == 'headquarters' && isset($postDataHeadquarters['data2'])) {
            $dataFilters = $postDataHeadquarters;
            $typeFilter = 'headquarters';
            $entityFilters = Headquarter::getListHeadquarterOptions($this->getDoctrine(), $postDataCompany);
        }
        elseif ($postDataFilter == 'evaluateds' && isset($postDataEvaluateds['data2'])) {
            $dataFilters = $postDataEvaluateds;
            $typeFilter = 'evaluateds';
            $entityFilters = Workinginformation::getListEvaluatedsByPollscheduling($this->getDoctrine(), $postDataPollscheduling);
        }
        elseif ($postDataFilter == 'faculties' && isset($postDataFaculties['data2'])) {
            $dataFilters = $postDataFaculties;
            $typeFilter = 'faculties';
            $entityFilters = Companyfaculty::getListCompanyfacultyOptions($this->getDoctrine(), $postDataCompany);
        }
        elseif ($postDataFilter == 'careers' && isset($postDataCareers['data2'])) {
            $dataFilters = $postDataCareers;
            $typeFilter = 'careers';
            $entityFilters = Companycareer::getListCompanycareerOptions($this->getDoctrine(), $postDataCompany);
        }
        elseif ($postDataFilter == 'grades' && isset($postDataGrades['data2'])) {
            $dataFilters = $postDataGrades;
            $typeFilter = 'grades';
            $entityFilters = Workinginformation::getListGradeOptions();
        }
        
        if (!empty($postDataPollscheduling) && !empty($postDataLevel)) {
            
            $i = 0;
            $arrayPoll = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $postDataPoll);
            
            if ($arrayPoll) {
                
                $arrayGroupPoll['sections'] = Pollsection::arrayGroupQuestions($this->getDoctrine(), 0, $postDataLevel, $arrayPoll);
                $array = $arrayGroupPoll;
                
                // Recover pollscheduling ids
                $f = 0;
                $filter = 0;
                $filterIds = '';
                
                // Recover Pollscheduling
                $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($postDataPollscheduling);
                
                /* TODO: Recover total pollschedulings */
                
                // Recover Pollapplicationquestions
                $pollapplicationquestions = Pollapplicationquestion::getResultPollapplicationquestionByFilter($this->getDoctrine(), $pollscheduling->getId());
                
                if (!empty($pollapplicationquestions)) {
                    
                    $pollschedulingReport = Pollscheduling::calculatePercentageSection($array, $pollapplicationquestions);
                    
                    // Total Rows
                    if ($postDataLevel != 'null') {
                        $pollschedulingPercentageRows = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections'], TRUE);
                    }
                    else {
                        $pollschedulingPercentageRows = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections']);
                    }
                    
                    // Row Results
                    $pollschedulingTotalRows = array(
                        'percentage' => $pollschedulingReport['percentage']['consolidate'], 
                        'count' => $pollschedulingReport['percentage']['count']
                    );
                    
                    // Recover Number Evaluators by Evaluated
                    $numberEvaluatorsTotal = Pollapplication::getNumberEvaluatorsByEvaluated($this->getDoctrine(), $pollscheduling->getId());
                    
                }
                else {
                    $messagePollscheduling .= $pollscheduling->getTitle() . ' - ' . $pollscheduling->getPollschedulingPeriod() . ' (NO ESTÁ DISPONIBLE)' . '</br>';
                }
                
                if (isset($dataFilters['data2'])) {
                    
                    foreach ($dataFilters['data2'] as $key) {
                        
                        // Recover Entity Filter
                        
                        switch ($typeFilter) {
                            case 'evaluateds':
                                $entityFilter = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($key);
                                break;
                            case 'organizationalunits':
                                $entityFilter = $em->getRepository('FishmanEntityBundle:Companyorganizationalunit')->find($key);
                                break;
                            case 'charges':
                                $entityFilter = $em->getRepository('FishmanEntityBundle:Companycharge')->find($key);
                                break;
                            case 'headquarters':
                                $entityFilter = $em->getRepository('FishmanEntityBundle:Headquarter')->find($key);
                                break;
                            case 'faculties':
                                $entityFilter = $em->getRepository('FishmanEntityBundle:Companyfaculty')->find($key);
                                break;
                            case 'careers':
                                $entityFilter = $em->getRepository('FishmanEntityBundle:Companycareer')->find($key);
                                break;
                            case 'grades':
                                $entityFilter = Workinginformation::getGradeName($key);
                                break;
                        }
                        
                        // Recover Name Filter
                        
                        if ($typeFilter == 'evaluateds') {
                            $nameFilter = $entityFilter->getUser()->getSurname() . ' ' . $entityFilter->getUser()->getLastname() . ' ' . $entityFilter->getUser()->getNames();
                        }
                        elseif ($typeFilter == 'grades') {
                            $nameFilter = $entityFilter;
                        }
                        elseif (in_array($typeFilter, array('organizationalunits', 'charges', 'headquarters', 'faculties', 'careers', 'grades'))) {
                            $nameFilter = $entityFilter->getName();
                        }
                        
                        // Check for participating in the survey
                        $participatingPoll = Pollapplication::checkParticipatingPoll($this->getDoctrine(), $pollscheduling->getId(), $key, $typeFilter);
                        
                        // Verify number of times the assessed evaluated
                        $numberResolvedPolls = Pollapplication::getNumberResolvedPolls($this->getDoctrine(), $pollscheduling->getId(), $key, $typeFilter);
                        
                        if ($participatingPoll) {
                            if (($numberResolvedPolls >= $pollscheduling->getAccessViewNumber() 
                                && $pollscheduling->getAccessViewNumber() > 0) || $pollscheduling->getMultipleEvaluation()) {
                                if ($f == 0) {
                                    $filterIds = "'" . $key . "'";
                                }
                                else {
                                    $filterIds .= ', ' . "'" . $key ."'";
                                }
                                $f++;
                            }
                            else {
                                $messagePollscheduling .= $nameFilter . ' (NO DISPONIBLE)' . '</br>';
                                
                                unset($dataFilters['data2'][$filter]);
                            }
                        }
                        else {
                            $messagePollscheduling .= $nameFilter . ' (NO PARTICIPA)' . '</br>';
                            
                            unset($dataFilters['data2'][$filter]);
                        }
                        $filter++;
                    }

                }
                
                $array_parameters['typefilter'] = $typeFilter;
                $array_parameters['filterids'] = $filterIds;
                
                if (isset($dataFilters['data2'])) {
                    
                    //Data to display filters selecteds
                    foreach ($dataFilters['data2'] as $key) {
                        
                        // Recover Pollapplicationquestions
                        $pollapplicationquestions = Pollapplicationquestion::getResultPollapplicationquestionByFilter($this->getDoctrine(), $pollscheduling->getId(), $key, $typeFilter);
                        
                        if (!empty($pollapplicationquestions)) {
                            
                            $filters[$i] = $key;
                            
                            $pollschedulingReport = Pollscheduling::calculatePercentageSection($array, $pollapplicationquestions);
                            
                            if ($postDataLevel != 'null') {
                                $pollschedulingPercentages[$i] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections'], TRUE);
                            }
                            else {
                                $pollschedulingPercentages[$i] = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections']);
                            }
                            
                            $pollschedulingTotals[$i] = array(
                                'percentage' => $pollschedulingReport['percentage']['consolidate'], 
                                'count' => $pollschedulingReport['percentage']['count']
                            );
                            
                            // Recover Number Evaluators by Evaluated
                            $numberEvaluators[$i] = Pollapplication::getNumberEvaluatorsByEvaluated($this->getDoctrine(), $pollscheduling->getId(), $key, $typeFilter);
                            
                            $i++;
                        }
                        else {
                            $messagePollscheduling .= $nameFilter . ' (NO HA SIDO EVALUADO)' . '</br>';
                        }
        
                    }
                    
                }
                
                // Headers
                $k = 0;
                if (!empty($filters)) {
                    foreach ($filters as $filter) {
                        foreach ($entityFilters as $key => $value) {
                            if ($filter == $key) {
                                $pollschedulingHeaders[$k] = $value;
                            }
                        }
                        $k++;
                    }
                }
                
                // Titles
                        
                if ($postDataLevel != 'null') {
                    $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections'], TRUE);
                }
                else {
                    $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections']);
                }
                
                // Count report vertical
                if ($pollschedulingPercentageRows != '') {
                    $countVertical = count($pollschedulingPercentageRows);
                }
                
                // Count report horizontal
                $countHorizontal = $i;
                
                // Ranking Top 25%
                $rankingTop25 = Pollapplicationquestion::getResultPollapplicationquestionTopAverage($this->getDoctrine(), 25, $pollscheduling->getId(), $array);
                
                $array_parameters['header_results'] = $pollschedulingHeaders;
                $array_parameters['title_results'] = $pollschedulingTitles;
                $array_parameters['number_evaluators'] = $numberEvaluators;
                $array_parameters['number_evaluators_total'] = $numberEvaluatorsTotal;
                $array_parameters['report_results'] = $pollschedulingPercentages;
                $array_parameters['report_rows'] = $pollschedulingPercentageRows;
                $array_parameters['total_results'] = $pollschedulingTotals;
                $array_parameters['row_results'] = $pollschedulingTotalRows;
                $array_parameters['count_horizontal'] = $countHorizontal;
                $array_parameters['count_vertical'] = $countVertical;
                $array_parameters['ranking_top'] = $rankingTop25;
                $array_parameters['pollschedulingid'] = $pollscheduling->getId();
                
                if ($messagePollscheduling != '') {
                    $messageError = $messagePollscheduling;
                }
                
            }
            else {
                $messageError = 'Actualmente la encuesta está sin preguntas.';
            }
            
        }
        
        if ($messageError != '') {
            $session->getFlashBag()->add('error', $messageError);
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:benchmarking_internal.html.twig', $array_parameters);
    }
    
    /**
     * Export Result of the Report.
     *
     */
    public function reportBenchmarkingInternalExportAction($pollschedulingid, $typefilter, $filterids)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $user = $this->get('security.context')->getToken()->getUser();
        $date = new \DateTime();
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            
            $permission = Permission::checkPermissionToPollschedulingBenchmarking($this->getDoctrine(), $user->getId(), 'internal', $date);
            
            if (!$permission) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
            }
        }
        // FIN ACCESS CONTROL
            
        if ($pollschedulingid != '') {
            
            // Recover Workshopscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
                return $this->redirect($this->generateUrl('fishman_front_end_polls'));
            }
            
            // Recover Company
            $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
            if (!$company) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
                return $this->redirect($this->generateUrl('fishman_front_end_polls'));
            }
            
            // Recovers Pollapplicationreports
            $pollapplicationreports = Pollapplicationreport::getListPollapplicationreportByFilters($this->getDoctrine(), $pollschedulingid, $filterids, $typefilter);
            
            $content = '';
            $response = new Response();
            
            $content .= 'Reporte:' . ';' . 'Benchmarking Interno' . "\r\n";
            $content .= 'Empresa:' . ';' . $company->getName() . "\r\n";
            $content .= 'Encuesta:' . ';' . $pollscheduling->getTitle() . ';' . $pollscheduling->getVersion() . ' | ' . $pollscheduling->getPollschedulingPeriod() . "\r\n";
            $content .= "\r\n";
            
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $company);
            }
            else {
                $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $company, FALSE);
            }
            //jtt
            $response->setContent(utf8_decode($content));
            
            $response->headers->set('Content-Type', 'text/csv');
            $response->headers->set('Content-Disposition', 'attachment;filename="reporte-benchmarking-interno-encuesta.csv"');
            
            return $response;
        }
        exit;
    }
    
    /**
     * Result of the Report.
     *
     */
    public function reportBenchmarkingExternalAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $user = $this->get('security.context')->getToken()->getUser();
        $date = new \DateTime();
        $pscIds = '';
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            
            $permission = Permission::checkPermissionToPollschedulingBenchmarking($this->getDoctrine(), $user->getId(), 'external', $date);
            
            if (!$permission) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
            }
        }
        // FIN ACCESS CONTROL
        
        $messageError = '';
        $messagePollscheduling = '';
        $countVertical = '';
        $countHorizontal = '';
        $pollapplicationquestions = '';
        $report_pollscheduling = array('number_evaluators' => '', 'percentages' => '', 'average' => '');
        $report_comparison = array('number_evaluators' => '', 'percentages' => '', 'average' => '');
        $report_total = array('number_evaluators' => '', 'percentages' => '', 'average' => '');
        
        // PostData
        $postDataCompany = $request->request->get('options_company', null);
        $postDataPoll = $request->request->get('options_poll', null);
        $postDataLevel = $request->request->get('options_level', null);
        $postDataPollscheduling = $request->request->get('options_pollscheduling', null);
        $postDataFilters = $request->request->get('options_filters', null);
        
        if (isset($postDataFilters['companytype'])) {
            if ($postDataFilters['companytype'] != 'school') {
                $postDataFilters['rangepensionschool'] = '';
                $postDataFilters['typeschool'] = '';
                $postDataFilters['gender'] = '';
                $postDataFilters['numberpeopleschool'] = '';
                $postDataFilters['religiousaffiliationon'] = '';
                $postDataFilters['religiousaffiliation'] = '';
                $postDataFilters['institutionalaffiliation'] = '';
                $postDataFilters['educationsystem'] = '';
                $postDataFilters['internalschool'] = '';
            }
            if ($postDataFilters['companytype'] != 'university') {
                $postDataFilters['rangepensionuniversity'] = '';
                $postDataFilters['typeuniversity'] = '';
                $postDataFilters['numberpeopleuniversity'] = '';
            }
        }
        
        $nature_array = Nature::getListNature($this->getDoctrine());
        $json_nature = json_encode($nature_array);
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $pscIds = Pollscheduling::getPollschedulingsWithPermissionByUser($this->getDoctrine(), $user->getId(), '', $date, 'external');
        }
        else {
            $pscIds = Pollscheduling::getPollschedulingsWithBenchmarking($this->getDoctrine());
        }
        
        // Companies
        $selectCompanies = Pollscheduling::getWidgetCompanies(
            $this->getDoctrine(),
            $rol,
            $postDataCompany, 
            $pscIds, 
            'external'
        );
        
        // Polls
        $selectPolls = Pollscheduling::getWidgetPollsByCompany(
            $this->getDoctrine(),
            $rol,
            $postDataCompany,
            $postDataPoll,
            $pscIds
        );
        
        // PollschedulingPeriods
        $selectPollschedulingPeriods = Pollscheduling::getWidgetPollschedulingPeriodsByPoll(
            $this->getDoctrine(),
            $rol, 
            $postDataPoll, 
            $postDataCompany,
            $postDataPollscheduling,
            $pscIds
        );
        
        // Levels
        $selectLevels = Pollscheduling::getWidgetLevels(
            $postDataLevel
        );
        
        // Industries
        $selectIndustries = Company::getWidgetIndustries(
            $this->getDoctrine(),
            $postDataFilters['industry']
        );
        
        // Natures
        $selectNatures = Company::getWidgetNatures(
            $this->getDoctrine(),
            $postDataFilters['nature']
        );
        
        // VolumeBusiness
        $selectVolumeBusiness = Company::getWidgetVolumeBusiness(
            $this->getDoctrine(),
            $postDataFilters['volumebusiness']
        );
        
        // NumberEmployees
        $selectNumberEmployees = Company::getWidgetNumberPeoples(
            $this->getDoctrine(),
            'working', 
            $postDataFilters['numberpeopleworking']
        );
        
        // RankingTops
        $selectRankingTops = Company::getWidgetRankingTops(
            $postDataFilters['rankingtop']
        );
        
        // RangePensionsSchool
        $selectRangePensionsSchool = Company::getWidgetRangePensions(
            $this->getDoctrine(),
            'school',
            $postDataFilters['rangepensionschool']
        );
        
        // TypeSchool
        $selectTypesSchool = Company::getWidgetTypes(
            'school', 
            $postDataFilters['typeschool']
        );
        
        // Genders
        $selectGenders = Company::getWidgetGenders(
            $postDataFilters['gender']
        );
        
        // NumberStudentsSchool
        $selectNumberStudentsSchool = Company::getWidgetNumberPeoples(
            $this->getDoctrine(),
            'school', 
            $postDataFilters['numberpeopleschool']
        );
        
        // ReligiousAffiliationOn
        $selectReligiousAffiliationOn = Company::getWidgetReligiousAffiliationOn(
            $postDataFilters['religiousaffiliationon']
        );
        
        // ReligiousAffiliations
        $selectReligiousAffiliations = Company::getWidgetReligiousAffiliations(
            $this->getDoctrine(),
            $postDataFilters['religiousaffiliation']
        );
        
        // InstitutionalAffiliations
        $selectInstitutionalAffiliations = Company::getWidgetInstitutionalAffiliations(
            $this->getDoctrine(),
            $postDataFilters['institutionalaffiliation']
        );
        
        // EducationSystems
        $selectEducationSystems = Company::getWidgetEducationSystems(
            $postDataFilters['educationsystem']
        );
        
        // InternalSchool
        $selectInternalSchool = Company::getWidgetInternalSchools(
            $postDataFilters['internalschool']
        );
        
        // RangePensionsUniversity
        $selectRangePensionsUniversity = Company::getWidgetRangePensions(
            $this->getDoctrine(),
            'university',
            $postDataFilters['rangepensionuniversity']
        );
        
        // TypesUniversity
        $selectTypesUniversity = Company::getWidgetTypes(
            'university', 
            $postDataFilters['typeuniversity']
        );
        
        // NumberStudentsUniversity
        $selectNumberStudentsUniversity = Company::getWidgetNumberPeoples(
            $this->getDoctrine(),
            'university', 
            $postDataFilters['numberpeopleuniversity']
        );
        
        // Array paramteres
        $array_parameters = array(
            'pollapplicationquestions' => $pollapplicationquestions, 
            'json_nature' => $json_nature,
            'selectCompanies' => $selectCompanies, 
            'selectPolls' => $selectPolls, 
            'selectLevels' => $selectLevels, 
            'selectPollschedulingPeriods' => $selectPollschedulingPeriods, 
            'selectIndustries' => $selectIndustries, 
            'selectNatures' => $selectNatures, 
            'selectVolumeBusiness' => $selectVolumeBusiness, 
            'selectNumberEmployees' => $selectNumberEmployees, 
            'selectRankingTops' => $selectRankingTops, 
            'selectRangePensionsSchool' => $selectRangePensionsSchool, 
            'selectTypesSchool' => $selectTypesSchool, 
            'selectGenders' => $selectGenders, 
            'selectNumberStudentsSchool' => $selectNumberStudentsSchool, 
            'selectReligiousAffiliationOn' => $selectReligiousAffiliationOn, 
            'selectReligiousAffiliations' => $selectReligiousAffiliations, 
            'selectInstitutionalAffiliations' => $selectInstitutionalAffiliations, 
            'selectEducationSystems' => $selectEducationSystems, 
            'selectInternalSchool' => $selectInternalSchool, 
            'selectRangePensionsUniversity' => $selectRangePensionsUniversity, 
            'selectTypesUniversity' => $selectTypesUniversity, 
            'selectNumberStudentsUniversity' => $selectNumberStudentsUniversity, 
            'companyData' => $postDataCompany, 
            'pollschedulingid' => '',
            'filters' => ''
        );
        
        // Recover Filters selecteds
        
        $dataFilters = $postDataFilters;
        
        if (!empty($postDataPollscheduling) && !empty($postDataLevel)) {
            
            $i = 0;
            $arrayPoll = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $postDataPoll);
            
            if ($arrayPoll) {
                
                $arrayGroupPoll['sections'] = Pollsection::arrayGroupQuestions($this->getDoctrine(), 0, $postDataLevel, $arrayPoll);
                $array = $arrayGroupPoll;
                
                // Recover pollscheduling ids
                $f = 0;
                $filter = 0;
                $filterIds = '';
                $valid = TRUE;
                
                /* TODO: Recover total pollscheduling */
                
                // Recover Pollscheduling
                $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($postDataPollscheduling);
                
                // Check for participating in the survey
                $participatingPoll = Pollapplication::checkParticipatingPoll($this->getDoctrine(), $pollscheduling->getId());
                
                // Verify number of times the assessed evaluated
                $numberResolvedPolls = Pollapplication::getNumberResolvedPolls($this->getDoctrine(), $pollscheduling->getId());
                
                if ($participatingPoll) {
                    if (!(($numberResolvedPolls >= $pollscheduling->getAccessViewNumber() 
                        && $pollscheduling->getAccessViewNumber() > 0) || $pollscheduling->getMultipleEvaluation())) {
                        $messagePollscheduling = $pollscheduling->getTitle() . ' - ' . $pollscheduling->getPollschedulingPeriod() . ' (NO DISPONIBLE)' . '</br>';
                        $valid = FALSE;
                    }
                }
                else {
                    $messagePollscheduling = $pollscheduling->getTitle() . ' - ' . $pollscheduling->getPollschedulingPeriod() . ' (NO PARTICIPA)' . '</br>';
                    $valid = FALSE;
                }
                
                if (!$valid) {
                    $session->getFlashBag()->add('error', $messagePollscheduling);
                    return $this->render('FishmanFrontEndBundle:Dashboard/Report:benchmarking_external.html.twig', $array_parameters);
                }
                
                /* TODO: Recover total pollscheduling */
                
                // Recover Pollapplicationquestions
                $pollapplicationquestions = Pollapplicationquestion::getResultPollapplicationquestionByPollscheduling($this->getDoctrine(), '', $pollscheduling->getId());
                
                if (!empty($pollapplicationquestions)) {
                    $report_pollscheduling = Pollscheduling::getReportResultPollschedulings(
                        $this->getDoctrine(), 
                        $array, 
                        $pollapplicationquestions, 
                        $postDataLevel, 
                        $pollscheduling->getId()
                    );
                }
                else {
                    $messagePollscheduling .= $pollscheduling->getTitle() . ' - ' . $pollscheduling->getPollschedulingPeriod() . ' (NO ESTÁ DISPONIBLE)' . '</br>';
                }
                
                /* TODO: Recover total comparison */
                
                $pscIds = Pollscheduling::getPollschedulingsByBenchmarking($this->getDoctrine(), $pollscheduling->getBenchmarking()->getId());
                
                // Recover Pollapplicationquestion PscIds By Filters
                $pollapplicationquestionPscIdsByFilters = Pollapplicationquestion::getResultPollapplicationquestionByPollschedulingsByFilters($this->getDoctrine(), $pscIds, $dataFilters, $pollscheduling->getId());
                
                if (!empty($pollapplicationquestionPscIdsByFilters)) {
                    $report_comparison = Pollscheduling::getReportResultPollschedulings(
                        $this->getDoctrine(), 
                        $array, 
                        $pollapplicationquestionPscIdsByFilters, 
                        $postDataLevel,  
                        $pollscheduling->getId(),
                        $pscIds
                    );
                }
                
                /* TODO: Recover total */
                
                // Recover Pollapplicationquestion PscIds
                $pollapplicationquestionPscIds = Pollapplicationquestion::getResultPollapplicationquestionByPollschedulingsByFilters($this->getDoctrine(), $pscIds);
                
                if (!empty($pollapplicationquestionPscIds)) {
                    $report_total = Pollscheduling::getReportResultPollschedulings(
                        $this->getDoctrine(), 
                        $array, 
                        $pollapplicationquestionPscIds, 
                        $postDataLevel, 
                        FALSE,
                        $pscIds
                    );
                }
                
                // Titles
                        
                if ($postDataLevel != 'null') {
                    $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($report_pollscheduling['report']['sections'], TRUE);
                }
                else {
                    $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($report_pollscheduling['report']['sections']);
                }
                
                // Count report vertical
                if ($report_pollscheduling['percentages'] != '') {
                    $countVertical = count($report_pollscheduling['percentages']);
                }
                
                $array_parameters['title_results'] = $pollschedulingTitles;
                $array_parameters['number_evaluators_pollscheduling'] = $report_pollscheduling['number_evaluators'];
                $array_parameters['number_evaluators_comparison'] = $report_comparison['number_evaluators'];
                $array_parameters['number_evaluators_total'] = $report_total['number_evaluators'];
                $array_parameters['results_pollscheduling'] = $report_pollscheduling['percentages'];
                $array_parameters['results_comparison'] = $report_comparison['percentages'];
                $array_parameters['results_total'] = $report_total['percentages'];
                $array_parameters['average_pollscheduling'] = $report_pollscheduling['average'];
                $array_parameters['average_comparison'] = $report_comparison['average'];
                $array_parameters['average_total'] = $report_total['average'];
                $array_parameters['count_vertical'] = $countVertical;
                $array_parameters['companyname'] = $dataFilters['companyname'];
                $array_parameters['pollschedulingid'] = $pollscheduling->getId();
                $array_parameters['filters'] = $dataFilters;
                
                if ($messagePollscheduling != '') {
                    $messageError = $messagePollscheduling;
                }
                
            }
            else {
                $messageError = 'Actualmente la encuesta está sin preguntas.';
            }
            
        }
        
        if ($messageError != '') {
            $session->getFlashBag()->add('error', $messageError);
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:benchmarking_external.html.twig', $array_parameters);
    }
    
    /**
     * Export Result of the Report.
     *
     */
    public function reportBenchmarkingExternalExportAction($pollschedulingid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $user = $this->get('security.context')->getToken()->getUser();
        $date = new \DateTime();
        
        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            
            $permission = Permission::checkPermissionToPollschedulingBenchmarking($this->getDoctrine(), $user->getId(), 'external', $date);
            
            if (!$permission) {
                $session->getFlashBag()->add('error', 'Permiso denegado.');
                return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
            }
        }
        // FIN ACCESS CONTROL
            
        if ($pollschedulingid != '') {
            
            // Recover Workshopscheduling
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
            if (!$pollscheduling) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
                return $this->redirect($this->generateUrl('fishman_front_end_polls'));
            }
            
            // Recover Company
            $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
            if (!$company) {
                $session->getFlashBag()->add('error', 'No se puede encontrar la empresa.');
                return $this->redirect($this->generateUrl('fishman_front_end_polls'));
            }
            
            // Recovers Pollapplicationreports
            $pollapplicationreports = Pollapplicationreport::getListPollapplicationreportResults($this->getDoctrine(), $pollschedulingid);
            
            $content = '';
            $response = new Response();
            
            $content .= 'Reporte:' . ';' . 'Benchmarking Externo' . "\r\n";
            $content .= 'Empresa:' . ';' . $company->getName() . "\r\n";
            $content .= 'Encuesta:' . ';' . $pollscheduling->getTitle() . ';' . $pollscheduling->getVersion() . ' | ' . $pollscheduling->getPollschedulingPeriod() . "\r\n";
            $content .= "\r\n";
            
            if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $company);
            }
            else {
                $content .= Pollapplicationreport::generateReportResultCSV($pollapplicationreports, $company, FALSE);
            }
            
            $response->setContent($content);
            
            $response->headers->set('Content-Type', 'text/csv');
            $response->headers->set('Content-Disposition', 'attachment;filename="reporte-benchmarking-externo-encuesta.csv"');
            
            return $response;
        }
        exit;
    }

    /**
     * Return polls select list with some companyid
     * TODO: Change this as reportResultPollsByCompany
     */
    public function reportResultPollsByCompanyAction()
    {
        $output = '';
        $wsId = '';
        $companyId = '';
        $ids = '';
        
        if (isset($_GET['workshopschedulingid'])) {
            $wsId = $_GET['workshopschedulingid'];
        }
        if (isset($_GET['companyid'])) {
            $companyId = $_GET['companyid'];
        }
        if (isset($_GET['ids'])) {
            $ids = $_GET['ids'];
        }
        
        // Recovers Polls
        $polls = Pollscheduling::getListPollsByProfile($this->getDoctrine(), $wsId, $companyId, $ids);
        
        $output .= '<select name="options_poll" id="options_poll" required >';
        $output .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        if (!empty($polls)) {
            foreach ($polls as $poll) {
                $output .= '<option value="' . $poll['id'] . '">' . $poll['title'] . '</option>';
            }
        }
        $output .= '</select>';
        
        $response = new Response(json_encode(
            array('output' => $output)
        ));
        
        return $response;
    }

    /**
     * Return pollschedulings select list with some pollid
     * TODO: Change this as ajaxPollschedulingsByPoll
     */
    public function reportResultPollschedulingsByPollAction()
    {
        $output = '';
        $wsId = '';
        $pId = $_GET['pollid'];
        $companyId = '';
        $ids = '';
        
        if (isset($_GET['workshopschedulingid'])) {
            $wsId = $_GET['workshopschedulingid'];
        }
        if (isset($_GET['companyid'])) {
            $companyId = $_GET['companyid'];
        }
        if (isset($_GET['ids'])) {
            $ids = $_GET['ids'];
        }
        
        // Recovers Pollschedulings
        $pollschedulings = Pollscheduling::getListPollschedulingsByProfile($this->getDoctrine(), $wsId, $pId, $companyId, $ids);
        
        if (!empty($pollschedulings)) {
            foreach ($pollschedulings as $pollscheduling) {
                $output .= '<option value="' . $pollscheduling['id'] . '">';
                $output .= $pollscheduling['key'] . ' - ' . $pollscheduling['period'];
                $output .= '</option>';
            }
        }
        
        $response = new Response(json_encode(
            array('output' => $output)
        ));
        
        return $response;
    }

    /**
     * Return pollschedulings select list with some pollid
     * TODO: Change this as ajaxPollschedulingsOnlyByPoll
     */
    public function reportResultPollschedulingsOnlyByPollAction()
    {
        $output = '';
        $wsId = '';
        $pId = $_GET['pollid'];
        $companyId = '';
        $ids = '';
        
        if (isset($_GET['workshopschedulingid'])) {
            $wsId = $_GET['workshopschedulingid'];
        }
        if (isset($_GET['companyid'])) {
            $companyId = $_GET['companyid'];
        }
        if (isset($_GET['ids'])) {
            $ids = $_GET['ids'];
        }
        
        // Recovers Pollschedulings
        $pollschedulings = Pollscheduling::getListPollschedulingsByProfile($this->getDoctrine(), $wsId, $pId, $companyId, $ids);
        
        $output .= '<select name="options_pollscheduling" id="options_pollscheduling" required >';
        $output .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        if (!empty($pollschedulings)) {
            foreach ($pollschedulings as $pollscheduling) {
                $output .= '<option value="' . $pollscheduling['id'] . '">';
                $output .= $pollscheduling['key'] . ' - ' . $pollscheduling['period'];
                $output .= '</option>';
            }
        }
        $output .= '</select>';
        
        $response = new Response(json_encode(
            array('output' => $output)
        ));
        
        return $response;
    }

    /**
     * Return evaluateds select list with some pollschedulingids
     * TODO: Change this as reportResultEvaluatedsByPollschedulings
     */
    public function reportResultEvaluatedsByPollschedulingsAction()
    {
        $output = '';
        $wsId = '';
        $psIds = 0;
        $companyId = '';
        $ids = '';
        
        if (isset($_GET['workshopschedulingid'])) {
            $wsId = $_GET['workshopschedulingid'];
        }
        if ($_GET['pollschedulingids'] != '') {
            $psIds = $_GET['pollschedulingids'];
        }
        if (isset($_GET['companyid'])) {
            $companyId = $_GET['companyid'];
        }
        if (isset($_GET['ids'])) {
            $ids = $_GET['ids'];
        }
        
        // Recovers Evaluateds
        $evaluateds = Pollapplication::getListPollschedulingEvaluatedsByProfile($this->getDoctrine(), $wsId, $psIds, $companyId, $ids);
        
        $output .= '<select name="options_evaluated" id="options_evaluated" required >';
        $output .= '<option value>SELECCIONE UNA OPCIÓN</option>';
        if (!empty($evaluateds)) {
            foreach ($evaluateds as $evaluated) {
                $output .= '<option value="' . $evaluated['id'] . '">';
                $output .= $evaluated['surname'] . ' ' . $evaluated['lastname'] . ', ' . $evaluated['names'];
                $output .= '</option>';
            }
        }
        $output .= '</select>';
        
        $response = new Response(json_encode(
            array('output' => $output)
        ));
        
        return $response;
    }

    /**
     * Return elements select list with some companyid
     * TODO: Change this as reportResultElementsByCompany
     */
    public function reportResultElementsByCompanyAction()
    {
        $output = '';
        $companyId = '';
        $benchmarking = '';
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $user = $this->get('security.context')->getToken()->getUser();
        $date = new \DateTime();
        
        if (isset($_GET['companyid'])) {
            $companyId = $_GET['companyid'];
        }
        if (isset($_GET['benchmarking'])) {
            $benchmarking = $_GET['benchmarking'];
        }
        
        if ($benchmarking == '') {
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $pscIds = Pollscheduling::getPollschedulingsWithPermissionByUser($this->getDoctrine(), $user->getId(), $companyId, $date, $benchmarking);
                $polls = Pollscheduling::getWidgetPollsByCompany($this->getDoctrine(), $rol, $companyId, '', $pscIds);
            }
            else {
                $polls = Pollscheduling::getWidgetPollsByCompany($this->getDoctrine(), $rol, $companyId);
            }
        }
        elseif ($benchmarking == 'external') {
            $pscIds = Pollscheduling::getPollschedulingsWithBenchmarking($this->getDoctrine());
            $polls = Pollscheduling::getWidgetPollsByCompany($this->getDoctrine(), $rol, $companyId, '', $pscIds);
        }
        
        if ($benchmarking == '') {
            $array_parameters = array(
                'polls' => $polls, 
                'organizationalunits' => Company::getWidgetMultipleOrganizationalUnitsByCompany($this->getDoctrine(), $companyId),
                'charges' => Company::getWidgetMultipleChargesByCompany($this->getDoctrine(), $companyId),
                'headquarters' => Company::getWidgetMultipleHeadquartersByCompany($this->getDoctrine(), $companyId),
                'faculties' => Company::getWidgetMultipleFacultiesByCompany($this->getDoctrine(), $companyId),
                'careers' => Company::getWidgetMultipleCareersByCompany($this->getDoctrine(), $companyId),
                'grades' => Company::getWidgetMultipleGradesByCompany($this->getDoctrine(), $companyId)
            );
        }
        elseif ($benchmarking == 'external') {
            $array_parameters = array(
                'polls' => $polls
            );
        }
        
        $response = new Response(json_encode($array_parameters));
        
        return $response;
    }

    /**
     * Return pollschedulings select list with some pollid
     * TODO: Change this as ajaxPollschedulingPeriodsByPoll
     */
    public function reportResultPollschedulingPeriodsByPollAction()
    {
        $output = '';
        $pollId = $_GET['pollid'];
        $companyId = '';
        $benchmarking = '';
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        $user = $this->get('security.context')->getToken()->getUser();
        $date = new \DateTime();
        
        if (isset($_GET['companyid'])) {
            $companyId = $_GET['companyid'];
        }
        if (isset($_GET['benchmarking'])) {
            $benchmarking = $_GET['benchmarking'];
        }
        
        if ($benchmarking == '') {
            if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
                $pscIds = Pollscheduling::getPollschedulingsWithPermissionByUser($this->getDoctrine(), $user->getId(), $companyId, $date, $benchmarking);
                $periods = Pollscheduling::getWidgetPollschedulingPeriodsByPoll($this->getDoctrine(), $rol, $pollId, $companyId, '', $pscIds);
            }
            else {
                $periods = Pollscheduling::getWidgetPollschedulingPeriodsByPoll($this->getDoctrine(), $rol, $pollId, $companyId);
            }
        }
        elseif ($benchmarking == 'external') {
            $pscIds = Pollscheduling::getPollschedulingsWithBenchmarking($this->getDoctrine());
            $periods = Pollscheduling::getWidgetPollschedulingPeriodsByPoll($this->getDoctrine(), $rol, $pollId, $companyId, '', $pscIds);
        }
        
        $response = new Response(json_encode(
            array('periods' => $periods)
        ));
        
        return $response;
    }

    /**
     * Return evaluateds select list with some pollschedulingid
     * TODO: Change this as reportResultEvaluatedsByPollscheduling
     */
    public function reportResultElementsByPollschedulingAction()
    {
        $output = '';
        $companyId = '';
        $pscId = '';
        
        if ($_GET['pollschedulingid'] != '') {
            $pscId = $_GET['pollschedulingid'];
        }
        
        if ($pscId) {
            $pollscheduling = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollscheduling')->find($pscId);
            $companyId = $pollscheduling->getCompanyId();
        }
        
        $evaluateds = Pollscheduling::getWidgetMultipleEvaluatedsByPollscheduling($this->getDoctrine(), $pscId);
        $filters = Company::getWidgetFilterOptions($this->getDoctrine(), $companyId, $pscId);
        $type = $pollscheduling->getType();
        
        $response = new Response(json_encode(
            array(
                'type' => $type, 
                'filters' => $filters, 
                'evaluateds' => $evaluateds
            )
        ));
        
        return $response;
    }
    
    /**
     * Result of the Report.
     *
     */
    public function reportDisaggregatedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // ACCESS CONTROL
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            $session->getFlashBag()->add('error', 'Permiso denegado.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }
        
        // FIN ACCESS CONTROL
        
        $messageError = '';
        $messagePollscheduling = '';
        
        $companies = '';
        $polls = '';
        $pollschedulings = '';
        $evaluateds = '';
        $pollapplicationquestions = '';
        
        $postDataCompany = '';
        $postDataPoll = ''; 
        $postDataPollscheduling = '';
        $postDataEvaluated = '';
        $profileTemp = '';
        
        // PostData
        $postDataCompany = $request->request->get('options_company', null);
        $postDataPoll = $request->request->get('options_poll', null);
        $postDataPollscheduling = $request->request->get('options_pollscheduling', null);
        $postDataEvaluated = $request->request->get('options_evaluated', null);
        
        // Recovers Companies
        $companies = Pollscheduling::getListCompaniesByProfile($this->getDoctrine());
        
        if (!empty($postDataCompany)) {
            
            // Recovers Polls
            $polls = Pollscheduling::getListPollsByProfile($this->getDoctrine(), '', $postDataCompany);
            
            // Recovers Pollschedulings
            $pollschedulings = Pollscheduling::getListPollschedulingsByProfile($this->getDoctrine(), '', $postDataPoll, $postDataCompany);

            // Recovers Evaluateds
            $evaluateds = Pollapplication::getListPollschedulingEvaluatedsByProfile($this->getDoctrine(), '', $postDataPollscheduling, $postDataCompany);
        }
        
        // Array paramteres
        $array_parameters = array(
            'pollapplicationquestions' => $pollapplicationquestions,
            'companies' => $companies, 
            'polls' => $polls,  
            'pollschedulings' => $pollschedulings,
            'evaluateds' => $evaluateds,
            'companyData' => $postDataCompany,
            'pollData' => $postDataPoll,
            'pollschedulingData' => $postDataPollscheduling, 
            'evaluatedData' => $postDataEvaluated
        );
        
        if (!empty($postDataPollscheduling) && !empty($postDataEvaluated)) {
            
            $i = 0;
            $arrayPoll['sections'] = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $postDataPoll);
            
            if ($arrayPoll) {

                // Recover Pollscheduling
                $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($postDataPollscheduling);
                
                $array_parameters['pollschedulingid'] = $pollscheduling->getId();
                
                $reportDisaggregated = Pollapplication::getReportDisaggregated($this->getDoctrine(), $arrayPoll, $postDataPollscheduling, $postDataEvaluated, $array_parameters);
                
                if (!$reportDisaggregated['valid']) {
                    $session->getFlashBag()->add('error', 'NO EXISTE NINGUNA EVALUACIÓN DISPONIBLE');
                    return $this->render('FishmanFrontEndBundle:Dashboard/Report:result_disaggregated.html.twig', $reportDisaggregated['parametters']);
                }
                
                if ($reportDisaggregated['message'] != '') {
                    $messageError = $messagePollscheduling;
                }
                
                $reportPTT = Pollapplication::getReportPTT(
                    $this->getDoctrine(), 
                    $pollscheduling->getPoll()->getId(), 
                    $pollscheduling->getId(), 
                    $postDataEvaluated, 
                    $reportDisaggregated['parametters']['num_evaluators'] - $reportDisaggregated['parametters']['num_rols']
                );

                $array_parameters = $reportDisaggregated['parametters'];
                $array_parameters['ptt_total'] = $reportPTT['total'];
                $array_parameters['ptt_percentages'] = $reportPTT['percentages'];
                
                //jtt: bug con encuesta 18, la 1ra fila esta mostrando el item "Seccion de Preguntas"
                if ($pollscheduling->getPoll()->getId()==18 && 
                    stripos($array_parameters['title_results'][0]['title'],'n de Preguntas')!==FALSE) {
                    array_shift($array_parameters['title_results']);
                    foreach ($array_parameters['report_results'] as $key => &$arr) {
                        array_shift($arr);
                    }
                    array_shift($array_parameters['report_rows']);
                    $array_parameters['count_vertical']= $array_parameters['count_vertical']-1;
                }
            }
            else {
                $messageError = 'Actualmente la encuesta está sin preguntas.';
            }
            
        }
        
        if ($messageError != '') {
            $session->getFlashBag()->add('error', $messageError);
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:result_disaggregated.html.twig', $array_parameters);
    }
    
    /**
     * Export Result of the Report.
     *
     */
    public function reportDisaggregatedExportAction($pollid, $pollschedulingid, $evaluatedid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }

        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());

        $wi = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($evaluatedid);
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling->getEntityType() != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollschedulingid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollschedulingid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollschedulingid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling->getDeleted()) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollscheudlingid
            )));
        }
        // FIN ACCESS CONTROL

        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Desagregado");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta la lista de las evaluaciones del evaluado divido por los roles asignados a los evaluadores");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('REPORTE DE DESAGREGADO');

        $worksheet = $phpExcel->getSheet(0);

        $evaluated = $wi->getUser()->getSurname() . ' ' . $wi->getUser()->getLastname() . ' ' . $wi->getUser()->getNames();

        $worksheet->getCellByColumnAndRow(1, 2)->setValue('REPORTE DE DESAGREGADO: ' . $evaluated);
        $worksheet->getCellByColumnAndRow(1, 4)->setValue('Empresa: ' . $company->getName());
        $worksheet->getCellByColumnAndRow(1, 5)->setValue('Encuesta: ' . $pollscheduling->getTitle() . ';' . $pollscheduling->getVersion() . ' | ' . $pollscheduling->getPollschedulingPeriod());

        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('boss', 'responsible', 'company'))) {
            
            if (!empty($pollschedulingid) && !empty($evaluatedid)) {
                
                // Recover Array Poll
                $arrayPoll['sections'] = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $pollid);
                
                // Recovers Pollapplicationreports
                $reportDisaggregated = Pollapplication::getReportDisaggregated($this->getDoctrine(), $arrayPoll, $pollschedulingid, $evaluatedid, array());
                
                $reportPTT = Pollapplication::getReportPTT(
                    $this->getDoctrine(), 
                    $pollscheduling->getPoll()->getId(), 
                    $pollscheduling->getId(), 
                    $evaluatedid, 
                    $reportDisaggregated['parametters']['num_evaluators'] - $reportDisaggregated['parametters']['num_rols']
                );

                if ($reportDisaggregated['parametters']) {

                    $parameters = $reportDisaggregated['parametters'];

                    //jtt: bug con encuesta 18, la 1ra fila esta mostrando el item "Seccion de Preguntas"
                    if ($pollscheduling->getPoll()->getId()==18 && 
                        stripos($parameters['title_results'][0]['title'],'n de Preguntas')!==FALSE) {
                        array_shift($parameters['title_results']);
                        foreach ($parameters['report_results'] as $key => &$arr) {
                            array_shift($arr);
                        }
                        array_shift($parameters['report_rows']);
                        $parameters['count_vertical']= $parameters['count_vertical']-1;
                    }

                    $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
                    $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);

                    // Roles

                    $r = 2;
                    foreach ($parameters['rol_results'] as $rol) {
                        for ($c = 0; $c < $rol['count']; $c++) {
                            $worksheet->getCellByColumnAndRow($r, 7)->setValue($rol['title']);
                            $r++;
                        }
                        $worksheet->getCellByColumnAndRow($r, 7)->setValue('PROMEDIO');
                        $r++;
                    }

                    // Headers

                    $worksheet->getCellByColumnAndRow(1, 8)->setValue('Sección / Pregunta');
                    
                    $h = 2;
                    $r = 0;
                    $i = 0;
                    foreach ($parameters['header_results'] as $header) {
                        $worksheet->getCellByColumnAndRow($h++, 8)->setValue($header);
                        $i++;
                        if ($i == $parameters['rol_results'][$r]['count']) {
                            $worksheet->getCellByColumnAndRow($h++, 8)->setValue('');
                            $r++;
                            $i = 0;
                        }
                    }
                    
                    $worksheet->getCellByColumnAndRow($h++, 8)->setValue('PTT');
                    $worksheet->getCellByColumnAndRow($h++, 8)->setValue('Promedio Total');
                    
                    // Recover Averages

                    $i = 0;
                    for ($i = 0; $i <= $parameters['count_vertical'] - 1; $i++) {
                        $j = 1;
                        $title = str_replace('<strong>','',$parameters['title_results'][$i]['title']);
                        $title = str_replace('</strong>','',$title);
                        $worksheet->getCellByColumnAndRow($j, $i+9)->setValue($title);
                        for ($j = 1; $j <= $parameters['count_horizontal']; $j++) {
                            $worksheet->getCellByColumnAndRow($j+1, $i+9)->setValue(round($parameters['report_results'][$j-1][$i]['percentage'],2));
                        }
                        $worksheet->getCellByColumnAndRow($j+1, $i+9)->setValue(round($reportPTT['percentages'][$i]['percentage'],2));
                        $worksheet->getCellByColumnAndRow($j+2, $i+9)->setValue(round($parameters['report_rows'][$i],2));
                    }
                    
                    // Recover Total Average

                    $p = 2;
                    $worksheet->getCellByColumnAndRow(1, $i+9)->setValue('Promedio Total');
                    foreach ($parameters['total_results'] as $total) {
                        $worksheet->getCellByColumnAndRow($p++, $i+9)->setValue(round($total['percentage'],2));
                    }
                    $worksheet->getCellByColumnAndRow($p++, $i+9)->setValue(round($reportPTT['total'],2));
                    $worksheet->getCellByColumnAndRow($p++, $i+9)->setValue(round($parameters['row_results'],2));

                }
            }
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 5)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-de-desagregado.xls"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
    
    /**
     * Export Result of the Report Excel Grafico.
     * jtt
     */
    public function reportDisaggregatedExportGraficoAction($pollid, $pollschedulingid, $evaluatedid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollschedulingid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_polls'));
        }

        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());

        $wi = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($evaluatedid);
        
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if ($pollscheduling->getEntityType() != 'workshopscheduling') {
                $entityApplication = Pollschedulingpeople::getCurrentPollschedulingpeople($this->getDoctrine(), $pollschedulingid, $wi['id']);
            }
            else {
                $entityApplication = Workshopapplication::getCurrentWorkshopapplicationToPollscheduling($this->getDoctrine(), $pollschedulingid, $wi['id']);
            }
            $profile = $entityApplication->getProfile();
        }

        // ACCESS CONTROL
        if (!in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER'))) {
            if (empty($entityApplication)) {
                $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                    'pollid' => $pollschedulingid
                )));
            }
        }
        if (empty($pollscheduling) || $pollscheduling->getDeleted()) {
            $session->getFlashBag()->add('error', 'No existe la encuesta que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_poll', array(
                'pollid' => $pollscheudlingid
            )));
        }
        // FIN ACCESS CONTROL

        $phpExcel = new PHPExcel();
        $phpExcel->getProperties()->setCreator("Sistema de Gestión de Capital Humano");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Reporte de Desagregado");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Muesta la lista de las evaluaciones del evaluado divido por los roles asignados a los evaluadores");

        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setTitle('REPORTE DE DESAGREGADO');

        $worksheet = $phpExcel->getSheet(0);
        //jtt
        $objDrawing = new PHPExcel_Worksheet_HeaderFooterDrawing(); 
        $objDrawing->setName('PHPExcel logo'); 
        $objDrawing->setPath('images/logo_excel.png'); 
        $objDrawing->setOffsetX(8);    // setOffsetX works properly
        $objDrawing->setCoordinates('B1');
        $objDrawing->setHeight(100); // logo height
        $objDrawing->setWorksheet($phpExcel->getActiveSheet()); 
        $hi = 6;

        
        //jtt
        $phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
        $style['general'] = array(
            'font' => array(
                'name' => 'Century Gothic',
                'size' => 11,
            ),
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN, 
                    'color' => array(
                        'rgb' => 'FFFFFF',
                    ),
                ),
            ),
        );
        $style['titulo_text'] = array(
            'font' => array(
                'bold' => true,
                'color' => array(
                    'rgb' => '2E75B6',
                ),
            ),
        );
        $style['cabecera_text'] = array(
            'font' => array(
                'bold' => true,
                'color' => array(
                    'rgb' => 'FFFFFF',
                ),
            ),
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN, 
                    'color' => array(
                        'rgb' => '000000',
                    ),
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => '2E75B6'
                ),
            ),
        );
        $style['detalle_text'] = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN, 
                    'color' => array(
                        'rgb' => '000000',
                    ),
                ),
            ),
        );
        
        $phpExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($style['general']);
        $phpExcel->getActiveSheet()->setShowGridlines(false);
        // $phpExcel->getActiveSheet()->getStyle('A4:C4')->applyFromArray($style['general']);
        
        $evaluated = $wi->getUser()->getSurname() . ' ' . $wi->getUser()->getLastname() . ' ' . $wi->getUser()->getNames();
        
        $worksheet->getCellByColumnAndRow(1, 2+$hi)->setValue('INFORME EVALUACIÓN DE LIDERAZGO: ' . $evaluated);
        $worksheet->getCellByColumnAndRow(1, 4+$hi)->setValue('Empresa: ' . $company->getName());
        $worksheet->getCellByColumnAndRow(1, 5+$hi)->setValue('Encuesta: ' . $pollscheduling->getTitle() . ' | ' . $pollscheduling->getPollschedulingPeriod());

        //jtt
        $phpExcel->getActiveSheet()->getStyleByColumnAndRow(1, 2+$hi)->applyFromArray($style['titulo_text']);
        $phpExcel->getActiveSheet()->getStyleByColumnAndRow(1, 4+$hi)->applyFromArray($style['titulo_text']);
        $phpExcel->getActiveSheet()->getStyleByColumnAndRow(1, 5+$hi)->applyFromArray($style['titulo_text']);
        
        if (in_array($rol, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_TEACHER')) || in_array($profile, array('boss', 'responsible', 'company'))) {
            
            if (!empty($pollschedulingid) && !empty($evaluatedid)) {
                
                // Recover Array Poll
                $arrayPoll['sections'] = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $pollid);
                
                // Recovers Pollapplicationreports
                $reportDisaggregated = Pollapplication::getReportDisaggregated($this->getDoctrine(), $arrayPoll, $pollschedulingid, $evaluatedid, array());
                
                $reportPTT = Pollapplication::getReportPTT(
                    $this->getDoctrine(), 
                    $pollscheduling->getPoll()->getId(), 
                    $pollscheduling->getId(), 
                    $evaluatedid, 
                    $reportDisaggregated['parametters']['num_evaluators'] - $reportDisaggregated['parametters']['num_rols']
                );

                if ($reportDisaggregated['parametters']) {

                    $parameters = $reportDisaggregated['parametters'];

                    //jtt: bug con encuesta 18, la 1ra fila esta mostrando el item "Seccion de Preguntas"
                    if ($pollscheduling->getPoll()->getId()==18 && 
                        stripos($parameters['title_results'][0]['title'],'n de Preguntas')!==FALSE) {
                        array_shift($parameters['title_results']);
                        foreach ($parameters['report_results'] as $key => &$arr) {
                            array_shift($arr);
                        }
                        array_shift($parameters['report_rows']);
                        $parameters['count_vertical']= $parameters['count_vertical']-1;
                    }

                    $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
                    $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
                    $phpExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);

                    // Roles

                    $r = 2;
                    $is_autoevaluacion = false;
                    foreach ($parameters['rol_results'] as $rol) {
                        //jtt
                        for ($c = 0; $c < $rol['count']; $c++) {
                            if ($rol['title']!=='Autoevaluado'){
                                $titulo = $rol['title'];
                            } else {
                                $titulo = 'Autoevaluación';
                                $is_autoevaluacion = true;
                            }
                            $worksheet->getCellByColumnAndRow($r, 7+$hi)->setValue($titulo);
                            $phpExcel->getActiveSheet()->getStyleByColumnAndRow($r, 7+$hi)->applyFromArray($style['cabecera_text']);
                            $r++;
                        }
                        $phpExcel->getActiveSheet()->getStyleByColumnAndRow($r, 7+$hi)->applyFromArray($style['cabecera_text']);
                        $worksheet->getCellByColumnAndRow($r, 7+$hi)->setValue('');
                        $r++;
                    }

                    // Headers

                    $worksheet->getCellByColumnAndRow(1, 8+$hi)->setValue('Sección / Pregunta');
                    $phpExcel->getActiveSheet()->getStyleByColumnAndRow(1, 8+$hi)->applyFromArray($style['cabecera_text']);
                    
                    $h = 2;
                    $r = 0;
                    $i = 0;
                    foreach ($parameters['header_results'] as $header) {
                        $i++;
                        //jtt
                        if ($parameters['rol_results'][$r]['title']==='Autoevaluado'){
                            $header = '';
                        } else {
                            $header = '# '.$i;
                        }
                        $phpExcel->getActiveSheet()->getStyleByColumnAndRow($h, 8+$hi)->applyFromArray($style['cabecera_text']);
                        $worksheet->getCellByColumnAndRow($h++, 8+$hi)->setValue($header);
                        
                        if ($i == $parameters['rol_results'][$r]['count']) {
                            $phpExcel->getActiveSheet()->getStyleByColumnAndRow($h, 8+$hi)->applyFromArray($style['cabecera_text']);
                            $worksheet->getCellByColumnAndRow($h++, 8+$hi)->setValue('PROMEDIO');
                            $r++;
                            $i = 0;
                        }
                        
                    }
                    
                    $coord1 = $worksheet->getCellByColumnAndRow($h, 8+$hi)->getCoordinate();
                    $coord2 = $worksheet->getCellByColumnAndRow($h, 8+$hi-1)->getCoordinate();
                    $rango = $coord2.':'.$coord1;
                    $worksheet->mergeCells($rango);
                    $phpExcel->getActiveSheet()->getStyleByColumnAndRow($h, 8+$hi-1)->applyFromArray($style['cabecera_text']);
                    $phpExcel->getActiveSheet()->getStyleByColumnAndRow($h, 8+$hi)->applyFromArray($style['cabecera_text']);
                    $worksheet->getCellByColumnAndRow($h++, 8+$hi-1)->setValue('Top Two Box'); //antes 'PTT'
                    
                    
                    $coord1 = $worksheet->getCellByColumnAndRow($h, 8+$hi)->getCoordinate();
                    $coord2 = $worksheet->getCellByColumnAndRow($h, 8+$hi-1)->getCoordinate();
                    $rango = $coord2.':'.$coord1;
                    $worksheet->mergeCells($rango);
                    $phpExcel->getActiveSheet()->getStyleByColumnAndRow($h, 8+$hi-1)->applyFromArray($style['cabecera_text']);
                    $phpExcel->getActiveSheet()->getStyleByColumnAndRow($h, 8+$hi)->applyFromArray($style['cabecera_text']);
                    $worksheet->getCellByColumnAndRow($h++, 8+$hi-1)->setValue('Puntos'); // antes 'Promedio Total'
                    
                    // Recover Averages

                    $i = 0;
                    $array_data = array(array('',''),);
                    for ($i = 0; $i <= $parameters['count_vertical'] - 1; $i++) {
                        $es_titulo = false;
                        $j = 1;
                        $title = str_replace('<strong>','',$parameters['title_results'][$i]['title']);
                        $title = str_replace('</strong>','',$title);
                        
                        if (strpos($title, '---') === false) {
                            $es_titulo = true;
                        } else {
                            $title = str_replace('---','',$title);
                        }
                        
                        if ($es_titulo) {
                            $phpExcel->getActiveSheet()->getStyleByColumnAndRow($j, $i+9+$hi)->applyFromArray($style['cabecera_text']);
                            array_push($array_data, array($title,   round($reportPTT['percentages'][$i]['percentage'],2)/100));
                        } else {
                            $phpExcel->getActiveSheet()->getStyleByColumnAndRow($j, $i+9+$hi)->applyFromArray($style['detalle_text']);
                        }
                        
                        $worksheet->getCellByColumnAndRow($j, $i+9+$hi)->setValue($title);
                        
                        for ($j = 1; $j <= $parameters['count_horizontal']; $j++) {
                            //jtt
                            $valor = round($parameters['report_results'][$j-1][$i]['percentage'],2);
                            
                            if ($es_titulo) {
                                $phpExcel->getActiveSheet()->getStyleByColumnAndRow($j+1, $i+9+$hi)->applyFromArray($style['cabecera_text']);
                            } else {
                                $phpExcel->getActiveSheet()->getStyleByColumnAndRow($j+1, $i+9+$hi)->applyFromArray($style['detalle_text']);
                            }
                            
                            $worksheet->getCellByColumnAndRow($j+1, $i+9+$hi)->setValue($valor);
                            $worksheet->getStyleByColumnAndRow($j+1, $i+9+$hi)->getNumberFormat()->applyFromArray( 
                                    array( 
                                        'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00
                                    )
                                );
                        }
                        if ($es_titulo) {
                            $phpExcel->getActiveSheet()->getStyleByColumnAndRow($j+1, $i+9+$hi)->applyFromArray($style['cabecera_text']);
                            $phpExcel->getActiveSheet()->getStyleByColumnAndRow($j+2, $i+9+$hi)->applyFromArray($style['cabecera_text']);
                        } else {
                            $phpExcel->getActiveSheet()->getStyleByColumnAndRow($j+1, $i+9+$hi)->applyFromArray($style['detalle_text']);
                            $phpExcel->getActiveSheet()->getStyleByColumnAndRow($j+2, $i+9+$hi)->applyFromArray($style['detalle_text']);
                        }
                        $worksheet->getCellByColumnAndRow($j+1, $i+9+$hi)->setValue(round($reportPTT['percentages'][$i]['percentage'],2)/100);
                        $worksheet->getStyleByColumnAndRow($j+1, $i+9+$hi)->getNumberFormat()->applyFromArray( 
                                array( 
                                    'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
                                )
                            );
                        $worksheet->getCellByColumnAndRow($j+2, $i+9+$hi)->setValue(round($parameters['report_rows'][$i],2));
                        $worksheet->getStyleByColumnAndRow($j+2, $i+9+$hi)->getNumberFormat()->applyFromArray( 
                                    array( 
                                        'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00
                                    )
                                );
                    }
                    
                    // Recover Total Average

                    $p = 2;
                    $phpExcel->getActiveSheet()->getStyleByColumnAndRow(1, $i+9+$hi)->applyFromArray($style['cabecera_text']);
                    $worksheet->getCellByColumnAndRow(1, $i+9+$hi)->setValue('TOTAL GENERAL'); // antes 'Promedio Total'
                    foreach ($parameters['total_results'] as $total) {
                        $phpExcel->getActiveSheet()->getStyleByColumnAndRow($p, $i+9+$hi)->applyFromArray($style['cabecera_text']);
                        $worksheet->getCellByColumnAndRow($p++, $i+9+$hi)->setValue(round($total['percentage'],2));
                    }
                    $phpExcel->getActiveSheet()->getStyleByColumnAndRow($p, $i+9+$hi)->applyFromArray($style['cabecera_text']);
                    $worksheet->getStyleByColumnAndRow($p, $i+9+$hi)->getNumberFormat()->applyFromArray( 
                                array( 
                                    'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
                                )
                            );
                    $worksheet->getCellByColumnAndRow($p++, $i+9+$hi)->setValue(round($reportPTT['total'],2)/100);
                    
                    array_push($array_data, array('TOTAL GENERAL',   round($reportPTT['total'],2)/100));
                    
                    $phpExcel->getActiveSheet()->getStyleByColumnAndRow($p, $i+9+$hi)->applyFromArray($style['cabecera_text']);
                    $worksheet->getStyleByColumnAndRow($p, $i+9+$hi)->getNumberFormat()->applyFromArray( 
                                    array( 
                                        'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00
                                    )
                                );
                    $worksheet->getCellByColumnAndRow($p++, $i+9+$hi)->setValue(round($parameters['row_results'],2));
                    
                    
                    $coordUltima = $worksheet->getCellByColumnAndRow($p, $i+9+$hi)->getCoordinate();
                }
            }
            
            if ($is_autoevaluacion){
                $phpExcel->getActiveSheet()->removeColumn('D', 1); //quitar el pomedio autoevaluado
            }
            
            $worksheet_data = new PHPExcel_Worksheet($phpExcel, 'Data'); // para colocar la data del grafico
            $worksheet_data->fromArray($array_data);
            $phpExcel->addSheet($worksheet_data);
            
            //	Set the Labels for each data series we want to plot
            $dataSeriesLabels = array(
                new PHPExcel_Chart_DataSeriesValues('String', 'Data!$B$1', NULL, 1),	//	2010
            );
            //	Set the X-Axis Labels
            $xAxisTickValues = array(
                new PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$2:$A$7', NULL, 4),	//	Q1 to Q4
            );
            //	Set the Data values for each data series we want to plot
            $dataSeriesValues = array(
                new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$B$2:$B$7', NULL, 4),
            );

            //	Build the dataseries
            $series = new PHPExcel_Chart_DataSeries(
                // PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
                PHPExcel_Chart_DataSeries::TYPE_BARCHART_3D,		// plotType
                PHPExcel_Chart_DataSeries::GROUPING_STANDARD,	// plotGrouping
                // PHPExcel_Chart_DataSeries::GROUPING_PERCENT_STACKED,	// plotGrouping
                range(0, count($dataSeriesValues)-1),			// plotOrder
                $dataSeriesLabels,								// plotLabel
                $xAxisTickValues,								// plotCategory
                $dataSeriesValues								// plotValues
            );
            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph
            $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

            {//jtt: para habilitar el 3dhttps://github.com/PHPOffice/PHPExcel/pull/170
            $layoutPlotArea = new PHPExcel_Chart_Layout();
            $layoutPlotArea->setManual3dAlign(true);
            $layoutPlotArea->setRightAngleAxes(true);
            $layoutPlotArea->setXRotation(20);
            $layoutPlotArea->setYRotation(15);
            $layoutPlotArea->setPerspective(0);
            }

            //	Set the series in the plot area
            $plotArea = new PHPExcel_Chart_PlotArea($layoutPlotArea, array($series));
            //	Set the chart legend
            $legend = NULL;
            // $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

            $title = NULL;
            // $title = new PHPExcel_Chart_Title('Gráfico');
            $yAxisLabel = NULL;
            // $yAxisLabel = new PHPExcel_Chart_Title('Porcentaje');
            $xAxis = new PHPExcel_Chart_Axis();
            // $xAxis->setAxisOptionsProperties(NULL, NULL, NULL, NULL, NULL, NULL, 0, 100, NULL, NULL);
            $xAxis->minimum = 0;
            $xAxis->maximum = 1;
            $xAxis->setAxisNumberProperties('0.00%');
            
            //	Create the chart
            $chart = new PHPExcel_Chart(
                'chart1',		// name
                $title,			// title
                $legend,		// legend
                $plotArea,		// plotArea
                true,			// plotVisibleOnly
                0,				// displayBlanksAs
                NULL,			// xAxisLabel
                $yAxisLabel,	// yAxisLabel
                $xAxis			// xAxis
            );
            // var_dump ($chart);

            $letra = substr($coordUltima,0,1);
            $numero['ini'] = substr($coordUltima,1)+2;
            $numero['fin'] = $numero['ini']+25;
            $inicio = 'B'.$numero['ini'];
            $fin = 'G'.$numero['fin'];

            //	Set the position where the chart should appear in the worksheet
            $chart->setTopLeftPosition($inicio);
            $chart->setBottomRightPosition($fin);
            
            //	Add the chart to the worksheet
            $worksheet->addChart($chart);
            $phpExcel->setActiveSheetIndex(0);
            
        }
        else {
            $worksheet->getCellByColumnAndRow(1, 5+$hi)->setValue('No hay registros que mostrar');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="reporte-desagregado.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $writer->setIncludeCharts(TRUE);
        $writer->save('php://output');
        exit;
    }

    /**
     * Lists Report Counselor.
     *
     */
    public function reportCounselorAction(Request $request)
    {
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        // Find Entities
        
        $defaultData = array(
            'word' => '', 
            'resolved' => '', 
            'company' => '', 
            'initdate' => NULL, 
            'enddate' => NULL
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->add('company', 'text', array(
                'required' => FALSE
            ))
            ->add('initdate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->add('enddate', 'date', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => '', 
            'resolved' => '', 
            'company' => '', 
            'initdate' => NULL, 
            'enddate' => NULL
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovers Pollschedulings
        $query = Pollscheduling::getListCounselorPollschedulings($this->getDoctrine(), $data, $rol, TRUE);
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'reportcounselor');
        $paginator->setMaxPagerItems(5, 'reportcounselor');
        $polls = $paginator->paginate($query, 'reportcounselor')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('reportcounselor');
        $endPageItem = $paginator->getEndPageItem('reportcounselor');
        $totalItems = $paginator->getTotalItems('reportcounselor');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Return
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:counselor.html.twig', array(
            'polls' => $polls,
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        ));
    }

    /**
     * Lists Report Counselor Poll.
     *
     */
    public function reportCounselorPollAction(Request $request, $pollid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $rol = $session->get('currentrol');
        
        $profile = '';
        
        // Recover Pollscheduling
        
        $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollid);
        if (!$pollscheduling || ($pollscheduling->getPollschedulingAnonymous() && $rol == 'ROLE_TEACHER')) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la encuesta.');
            return $this->redirect($this->generateUrl('fishman_front_end_report_counselor'));
        }
        if (!$pollscheduling->getCounselorReport()) {
            $session->getFlashBag()->add('error', 'Esta encuesta no tiene Reporte Consejero.');
            return $this->redirect($this->generateUrl('fishman_front_end_report_counselor'));
        }
        
        // Recover Company
        $company = $em->getRepository('FishmanEntityBundle:Company')->find($pollscheduling->getCompanyId());
        
        // Find Entities
        
        $defaultData = array(
            'word' => ''
        );
        $formData = array();
        $form = $this->createFormBuilder($defaultData)
            ->add('word', 'text', array(
                'required' => FALSE
            ))
            ->getForm();

        $data = array(
            'word' => ''
        );
        if (isset($_GET['form'])) {
            $formData = $_GET['form'];
        }
        if ($request->getMethod() == 'GET') {
            $form->bindRequest($request);
            $data = $form->getData();
        }
        
        // Recovering Integrants
        
        if ($pollscheduling->getType() != 'not_evaluateds') {
            $query = Pollapplication::getListPollapplicationsFinishedIntegrants($this->getDoctrine(), $pollid, FALSE, $data);
        }
        else {
            $query = Pollapplication::getListPollapplicationsFinishedIntegrants($this->getDoctrine(), $pollid, TRUE, $data);
        }
        
        // Paginator
        
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(20, 'reportcounselorpoll');
        $paginator->setMaxPagerItems(5, 'reportcounselorpoll');
        $integrants = $paginator->paginate($query, 'reportcounselorpoll')->getResult();
        
        $startPageItem = $paginator->getStartPageItem('reportcounselorpoll');
        $endPageItem = $paginator->getEndPageItem('reportcounselorpoll');
        $totalItems = $paginator->getTotalItems('reportcounselorpoll');
        
        if ($totalItems == 0) {
            $info_paginator = 'No hay registros que mostrar';
        }
        else {
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
        }
        
        // Array Parameters

        $array_parameters = array(
            'integrants' => $integrants,
            'poll' => $pollscheduling,
            'company' => $company, 
            'form' => $form->createView(),
            'paginator' => $paginator,
            'info_paginator' => $info_paginator,
            'form_data' => $formData
        );
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Report:counselor_poll.html.twig', $array_parameters);
         
    }

    /**
     * Return evaluation json 
     */
    public function reportResultEvaluationJsonAction($pollapplicationid)
    {   
        $output = '';
        $token = '';
        $pollapplication = FALSE;
        
        if (isset($_GET['token'])) {
            $token = $_GET['token'];
        }
        
        if ($token) {
            $pollapplication = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication')->findBy(array(
                'id' => $pollapplicationid,
                'token' => $token
            ));
        }
        
        if (!$pollapplication) {
            $response = new Response(json_encode(
                array('report' => $output)
            ));
        }
        
        // Recover Pollapplicationreport to Pollapplication
        
        $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplicationreport');
        $pollaplicationreports = $repository->createQueryBuilder('par')
            ->select('pa.initdate, pa.enddate, par.id, par.names evaluated_name, par.evaluated_code, par.evaluator_name, par.evaluator_code, pa.evaluated_id,
                      par.section, par.question_number, par.question, par.sequence, par.score, par.answer, 
                      par.ceil_score, par.result_score')
            ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'par.pollapplication_id = pa.id')
            ->where('par.pollapplication_id = :pollapplication')
            ->setParameter('pollapplication', $pollapplicationid)
            ->getQuery()
            ->getResult();
        
        if (!empty($pollaplicationreports)) {
            
            $output['evaluated']['name'] = NULL;
            $output['evaluated']['code'] = NULL;
            if ($pollaplicationreports[0]['evaluated_code']) {
                $output['evaluated']['name'] = $pollaplicationreports[0]['evaluated_name'];
                $output['evaluated']['code'] = $pollaplicationreports[0]['evaluated_code'];
            }
			
			$evaluated_id = NULL;
            $evaluated_id = $pollaplicationreports[0]['evaluated_id'];
            
            $repository2 = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $pollaplications = $repository2->createQueryBuilder('pa')
                ->select('pa.id, pa.token, pa.changed')
                ->innerJoin('pa.pollscheduling', 'ps')
                ->innerJoin('ps.poll', 'p')
                ->where('pa.evaluated_id = :evaluated')
                ->andWhere('p.id = 40')
                ->setParameter('evaluated', $evaluated_id)
                ->addOrderBy('pa.changed', 'DESC')
                ->getQuery()
                ->getResult();
                
            $output['encuesta']['id'] = NULL;
            $output['encuesta']['token'] = NULL;
            if (!empty($pollaplications)) {
                if ($pollaplications[0]['id']) {
                    $output['encuesta']['id'] = $pollaplications[0]['id'];
                    $output['encuesta']['token'] = $pollaplications[0]['token'];
                }
            }
			

            $output['evaluator']['name'] = $pollaplicationreports[0]['evaluator_name'];
            $output['evaluator']['code'] = $pollaplicationreports[0]['evaluator_code'];
            
            $output['initdate'] = $pollaplicationreports[0]['initdate'];
            $output['enddate'] = $pollaplicationreports[0]['enddate'];
            
            $i = 0;
            foreach ($pollaplicationreports as $par) {
                $output['result'][$i] = array(
                    'id' => $par['id'], 
                    'section' => $par['section'], 
                    'question_number' => $par['question_number'], 
                    'question' => $par['question'], 
                    'sequence' => $par['sequence'], 
                    'score' => $par['score'], 
                    'answer' => $par['answer'], 
                    'ceil_score' => $par['ceil_score'], 
                    'result_score' => $par['result_score']
                );
                $i++;
            }
        }
        
        $response = new Response(json_encode(
            array('report' => $output)
        ));
        
        return $response;
    }

    /**
     * Return evaluation level json 
     */
    public function reportResultEvaluationLevelJsonAction($pollapplicationid)
    {   
        $output = '';
        $token = '';
        $pollapplication = FALSE;
        
        if (isset($_GET['token'])) {
            $token = $_GET['token'];
        }
        
        if ($token) {
            $pollapplication = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication')->findBy(array(
                'id' => $pollapplicationid,
                'token' => $token
            ));
        }
        
        if (!$pollapplication) {
            $response = new Response(json_encode(
                array('report' => $output)
            ));
        }
        else {
            
            // Recover Pollscheduling
            $pollscheduling = $pollapplication[0]->getPollscheduling();
            
            $arrayPoll = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $pollscheduling->getPoll()->getId());
            
            $lvl = $pollscheduling->getLevel();
            if ($lvl==0) {
                $lvl=10;
            }
            $array['sections'] = Pollsection::arrayGroupQuestions($this->getDoctrine(), 0, $lvl, $arrayPoll);
            
            // Recover Pollapplicationreport to Pollapplication
            
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplicationreport');
            $pollaplicationreports = $repository->createQueryBuilder('par')
                ->select('pq.id pq_id, par.id, par.question_number, par.question, par.sequence, par.score, par.answer, 
                          par.ceil_score, par.result_score')
                ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'par.pollapplication_id = pa.id')
                ->innerJoin('FishmanPollBundle:Pollapplicationquestion', 'paq', 'WITH', 'par.pollapplicationquestion_id = paq.id')
                ->innerJoin('paq.pollschedulingquestion', 'psq')
                ->innerJoin('psq.pollquestion', 'pq')
                ->where('par.pollapplication_id = :pollapplication')
                ->setParameter('pollapplication', $pollapplicationid)
                ->getQuery()
                ->getResult();
            
            if (!empty($pollaplicationreports)) {
                
                $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
                $pa = $repository->createQueryBuilder('pa')
                    ->select('pa.initdate, pa.enddate, u_one.names ev_name, u_one.surname ev_surname, u_one.lastname ev_lastname, 
                              wi_one.code ev_code, u_two.names er_name, u_two.surname er_surname, u_two.lastname er_lastname, 
                              wi_two.code er_code')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_one', 'WITH', 'pa.evaluator_id = wi_one.id')
                ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi_two', 'WITH', 'pa.evaluated_id = wi_two.id')
                ->innerJoin('wi_one.user', 'u_one')
                ->innerJoin('wi_two.user', 'u_two')
                ->where('pa.id = :pollapplication')
                ->setParameter('pollapplication', $pollapplicationid)
                ->getQuery()
                ->getResult();
                
                $output['evaluated']['name'] = NULL;
                $output['evaluated']['code'] = NULL;
                if ($pa[0]['ev_code']) {
                    $output['evaluated']['name'] = $pa[0]['ev_name'] . ' ' . $pa[0]['ev_surname'] . ' ' . $pa[0]['ev_lastname'];
                    $output['evaluated']['code'] = $pa[0]['ev_code'];
                }
                $output['evaluator']['name'] = $pa[0]['er_name'] . ' ' . $pa[0]['er_surname'] . ' ' . $pa[0]['er_lastname'];
                $output['evaluator']['code'] = $pa[0]['er_code'];
                
                $output['initdate'] = $pa[0]['initdate'];
                $output['enddate'] = $pa[0]['enddate'];
                
                $result = Pollscheduling::getArrayReportPercentage($array, $pollaplicationreports);

                $output['result'] = Pollscheduling::getArrayReportCorrelative($result);
            }

            $response = new Response(json_encode(
                array('report' => $output)
            ));
        }
        
        return $response;
    }

    /**
     * Return evaluation person json 
     */
    public function reportResultEvaluationPersonJsonAction($pollapplicationid)
    {   
        $output = '';
        $token = '';
        $person = FALSE;
        
        if (isset($_GET['token'])) {
            $token = $_GET['token'];
        }
        
        if ($token) {
            $pollapplication = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication')->findBy(array(
                'id' => $pollapplicationid,
                'token' => $token
            ));
            $repository = $this->getDoctrine()->getRepository('FishmanPollBundle:Pollapplication');
            $queryBuilder = $repository->createQueryBuilder('pa')
                ->select('pa.id, wi.code, u.numberidentity, u.names, u.surname, u.lastname, u.sex, u.email, u.birthday, 
						wi.email email_information, c.name company, h.name headquarter, co.name organizationalunit, 
						cch.name charge, cf.id faculty_id, cf.code faculty_code, cf.name faculty, 
						ccr.id career_id, ccr.code career_code, ccr.name career, ccs.name course, pa.section')
                ->where('pa.id = :pollapplication');
            
            if ($pollapplication[0]->getPollscheduling()->getType() != 'not_evaluateds') {
                $queryBuilder
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluated_id = wi.id');
            }
            else {
                $queryBuilder
                    ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'pa.evaluator_id = wi.id');
            }
            
            $queryBuilder
                ->leftJoin('wi.user', 'u')
                ->leftJoin('wi.company', 'c')
                ->leftJoin('wi.headquarter', 'h')
                ->leftJoin('wi.companyorganizationalunit', 'co')
                ->leftJoin('wi.companycharge', 'cch')
                ->leftJoin('wi.companyfaculty', 'cf')
                ->leftJoin('wi.companycareer', 'ccr')
                ->leftJoin('FishmanEntityBundle:Companycourse', 'ccs', 'WITH', 'pa.course = ccs.id')
                ->setParameter('pollapplication', $pollapplicationid);
            
            $output = $queryBuilder->getQuery()->getResult();
            $output = $output[0];
            
        }
        
        $response = new Response(json_encode(
            array('user' => $output)
        ));
        
        return $response;
    }
    
}
