<?php

namespace Fishman\FrontEndBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\EntityBundle\Entity\Companyorganizationalunit;
use Fishman\EntityBundle\Entity\Companycharge;

use Fishman\AuthBundle\Entity\Workinginformation;
use Fishman\WorkshopBundle\Entity\Workshopapplication;
use Fishman\WorkshopBundle\Entity\Workshopapplicationactivity;
use Fishman\WorkshopBundle\Entity\Workbook;
use Fishman\WorkshopBundle\Entity\Myerbriggs;
use Fishman\WorkshopBundle\Form\MyerbriggsType;
use Fishman\WorkshopBundle\Form\PlangoalType;

use Fishman\NotificationBundle\Entity\Notificationsend;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Fishman\PollBundle\Entity\Pollsection;
use Fishman\PollBundle\Entity\Pollscheduling;
use Fishman\PollBundle\Entity\Pollapplication;
use Fishman\PollBundle\Entity\Pollapplicationquestion;
use Fishman\WorkshopBundle\Entity\Questionapplication;

use Fishman\WorkshopBundle\Entity\Workbooktalent;
use Fishman\WorkshopBundle\Entity\Plan;
use Fishman\WorkshopBundle\Entity\Plangoal;

class DashboardactivityController extends Controller
{
    /*
     * TODO: Puede implementarse la posibilidad de que si se agrega un nuevo usuario o cambia de estado: 
     * Desactivo a Activo, se crearán sus respectivas actividades del mismo.
     * Podría mostrarse una lista de las personas de la empresa en el taller progamado y escoger las cque 
     * aun no tienen registros en actividades.
     */
      
    /**
     * Lists Activities of workbook.
     *
     */
    public function workshopActivitiesWorkbookAction(Request $request, $activityid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');

        // Recover Workshopapplicationactivity
        
        $workshopapplicationactivity = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($activityid);
        if (!$workshopapplicationactivity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities'));
        }
        
        if ($workshopapplicationactivity->getActivityType() == 1) {

            $workshopapplicacion = $workshopapplicationactivity->getWorkshopapplication();

            //ACCESS CONTROL
            if($workshopapplicacion->getDeleted() == 1){
                $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
            }

            if($workshopapplicationactivity->getDeleted() == 1){
                $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
            }

            //La actividad esta asignada al wi del usuario actual
            $wi = $session->get('workinginformation');
            if($workshopapplicacion->getWorkinginformation()->getId() != $wi['id']){
                $session->getFlashBag()->add('error',
                    'La persona con la que está tratado de ingresar no tiene permiso para esta actividad');
                return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));            
            }
            //FIN - ACCESS CONTROL
            
            // Recovering data
            
            $organizationalunit_options = Companyorganizationalunit::getListCompanyorganizationalunitOptions($this->getDoctrine(), $workshopapplicacion->getWorkinginformation()->getCompany()->getId());
            $charge_options = Companycharge::getListCompanychargeOptions($this->getDoctrine(), $workshopapplicacion->getWorkinginformation()->getCompany()->getId());
            $status_options = array(0 => 'Pendiente', 1 => 'Completo');
            
            // Find Entities
            
            $defaultData = array(
                'word' => '', 
                'organizationalunit' => '', 
                'charge' => '', 
                'myerbriggs_status' => '', 
                'talents_status' => ''
            );
            $formData = array();
            $form = $this->createFormBuilder($defaultData)
                ->add('word', 'text', array(
                    'required' => FALSE
                ))
                ->add('organizationalunit', 'choice', array(
                    'choices' => $organizationalunit_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('charge', 'choice', array(
                    'choices' => $charge_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('myerbriggs_status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->add('talents_status', 'choice', array(
                    'choices' => $status_options, 
                    'empty_value' => 'Choose an option',
                    'required' => FALSE
                ))
                ->getForm();
    
            $data = array(
                'word' => '', 
                'organizationalunit' => '', 
                'charge' => '', 
                'myerbriggs_status' => '', 
                'talents_status' => ''
            );
            if (isset($_GET['form'])) {
                $formData = $_GET['form'];
            }
            if ($request->getMethod() == 'GET') {
                $form->bindRequest($request);
                $data = $form->getData();
            }
            
            if ($workshopapplicationactivity->getAction() == 'create') {
                $query = Workbook::createWorkbooks($this->getDoctrine(), $wi['id'], $wi['company_id'], $workshopapplicationactivity);
            }
            
            // Recovers Notificationssend
            $query = Workbook::getListWorkbooks($this->getDoctrine(), $workshopapplicationactivity->getId(), $data);
            
            $session = $this->getRequest()->getSession();
    
            //Se esta tratando de "enviar" la actividad
            if($request->getMethod() == 'POST') {
                
                if ($request->request->get('send', false)) {
                    //Comprueba que todos los workbook tengan la información completa
                    $isComplete = $em->createQuery('SELECT COUNT(wb.id) as num, 
                                                           SUM(wb.myerbriggs_status) as mb,
                                                           SUM(wb.talents_status) as ts
                                                    FROM FishmanWorkshopBundle:Workbook wb
                                                    WHERE wb.workshopapplicationactivity = :wsaa')
                                                   ->setParameter('wsaa', $workshopapplicationactivity)
                                                   ->getSingleResult();
                    
                    if($isComplete['num'] == $isComplete['mb'] && $isComplete['num'] == $isComplete['ts']) {
                        
                        //Se marca la actividad como completa
                        
                        $waa_entity = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($activityid);
                        
                        $waa_entity->setFinished(TRUE);
                        if ($waa_entity->getInitdate() == '') {
                            $waa_entity->setInitdate(new \DateTime());
                        }
                        $waa_entity->setEnddate(new \DateTime());
                        
                        $em->persist($waa_entity);
                        $em->flush();

                        $session->getFlashBag()->add('status', 'La actividad workbook ha sido enviado.');
                        
                        // Send trigger notifications
                        
                        //Doctrine registry
                        $mailer = $this->get('mailer');
                        $logger = $this->get('logger');
                        $router = $this->get('router');
                        $templating = $this->get('templating');

                        Notificationexecution::sendTriggerNotifications($this->getDoctrine(), $mailer, $logger, $router, $templating, $activityid);
                        
                        // Active notificationExecution status send
                        Notificationexecution::activeNotificationexecutions($this->getDoctrine(), $activityid, 'activity', TRUE, FALSE, '-1', FALSE);

                    }
                    else{
                         $session->getFlashBag()->add('error', 'No se puede enviar la actividad workbook porque no esta completa.');
                    }
                }
            }
            
            // Paginator
            
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(20, 'workbooks');
            $paginator->setMaxPagerItems(5, 'workbooks');
            $workbooks = $paginator->paginate($query, 'workbooks')->getResult();
            
            $startPageItem = $paginator->getStartPageItem('workbooks');
            $endPageItem = $paginator->getEndPageItem('workbooks');
            $totalItems = $paginator->getTotalItems('workbooks');
            
            $info_paginator = 'Mostrando de ' . $startPageItem . ' a ' . $endPageItem . ' de ' . $totalItems . ' entradas';
            
            // Return
            
            return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Workbook:index.html.twig', array(
                'activity' => $workshopapplicationactivity,
                'workbooks' => $workbooks,
                'form' => $form->createView(),
                'paginator' => $paginator,
                'info_paginator' => $info_paginator,
                'form_data' => $formData
            ));
            
        }
        else {
            
            $session->getFlashBag()->add('error', 'La ruta no existe.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
          
        }
    }
    
    /**
     * Workbook - Result of the Report Poll and Sections specifics.
     *
     */
    public function workshopActivitiesWorkbookPollResultAction(Request $request, $activityid, $evaluatedid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Recover session varaible
        $session = $this->getRequest()->getSession();
        $wi = $session->get('workinginformation');
        $wa = $session->get('workshopapplication');
        $rol = $session->get('currentrol');
        
        // Recover Workshopschedulingactivityid
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
        $workshopschedulingactivity = $repository->createQueryBuilder('wsa')
            ->select('wsa.pollscheduling_id, p.id poll_id')
            ->innerJoin('FishmanWorkshopBundle:Workshopapplicationactivity', 'waa', 'WITH', 'waa.workshopschedulingactivity_id = wsa.id')
            ->innerJoin('FishmanPollBundle:Pollscheduling', 'ps', 'WITH', 'ps.id = wsa.pollscheduling_id')
            ->innerJoin('ps.poll', 'p')
            ->where('waa.id = :workshopapplicationactivityid')
            ->setParameter('workshopapplicationactivityid', $activityid)
            ->getQuery()
            ->getResult();
        
        $workshopschedulingid = $wa['ws_id'];
        $pId = $workshopschedulingactivity[0]['poll_id'];
        //$pollsectionids = '47, 54, 60, 65, 67';
        $pollsectionids = '';
        $sectionIds = explode(', ', $pollsectionids);
        $psId = $workshopschedulingactivity[0]['pollscheduling_id'];
        $pollapplicationquestions = '';
        $messagePollscheduling = '';

        // Recover Workshopapplicationactivity
        $workshopapplicationactivity = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($activityid);
        if (!$workshopapplicationactivity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la aplicación de la actividad.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities_workbook', array(
                'activityid' => $activityid
            )));
        }
        
        // Recover Workshopscheduling
        $workshopscheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($workshopschedulingid);
        
        // Recover Poll
        $poll = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($psId);
        $companyId = $poll->getCompanyId();
        
        // Recovers Pollschedulings
        $pollscheduling = Pollscheduling::getListPollschedulingsByProfile(
            $this->getDoctrine(), 
            $workshopschedulingid, 
            $pId, 
            $companyId, 
            '', 
            $psId
        );
        
        // Recover Evaluated
        $evaluated = Pollapplication::getCurrentEvaluatedOrEvaluatorByPollscheduling(
            $this->getDoctrine(), 
            $workshopschedulingid, 
            $psId, 
            $evaluatedid,  
            TRUE
        );
        
        //ACCESSS CONTROL
        if (empty($evaluated)) {
            $session->getFlashBag()->add('error', 'Esta persona no participa como evaluador en la encuesta');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities_workbook', array(
                'activityid' => $activityid
            )));
        }
        //FIN ACCESS CONTROL
        
        // Array paramteres
        $array_parameters = array(
            'workshopscheduling' => $workshopscheduling, 
            'activity' => $workshopapplicationactivity, 
            'poll' => $poll, 
            'evaluated' => $evaluated,
            'pollapplicationquestions' => $pollapplicationquestions,
        );
        
        $i = 0;
        //$arrayPoll = Pollsection::arraySectionsAndQuestionsBySectionIds($this->getDoctrine(), $pId, '-1', $sectionIds);
        //$arrayPollChecks = Pollsection::arraySectionsAndQuestionsChecks($this->getDoctrine(), $arrayPoll);
        //$arrayGroupPoll['sections'] = Pollsection::arrayGroupQuestions($this->getDoctrine(), 0, 'null', $arrayPollChecks);
        $arrayPoll = Pollsection::arraySectionsAndQuestions($this->getDoctrine(), $pId, '-1', $sectionIds);
        $arrayGroupPoll['sections'] = Pollsection::arrayGroupQuestions($this->getDoctrine(), 0, 'null', $arrayPoll);
        
        if (!empty($pollscheduling)) {

            $pollscheduling = current($pollscheduling);

            // Recover Pollscheduling
            $pollschedulingKey = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($pollscheduling['id']);
            
            // Check for participating in the survey
            $participatingPoll = Pollapplication::checkParticipatingPoll($this->getDoctrine(), $pollscheduling['id'], $evaluatedid);
            
            // Verify number of times the assessed evaluated
            $numberResolvedPolls = Pollapplication::getNumberResolvedPolls($this->getDoctrine(), $pollscheduling['id'], $evaluatedid);
            
            if ($participatingPoll) {
                if (!(($numberResolvedPolls >= $pollschedulingKey->getAccessViewNumber() 
                    && $pollschedulingKey->getAccessViewNumber() > 0)  
                    || $pollschedulingKey->getMultipleEvaluation() 
                    || $evaluatedid != $wi['id'])) {

                    $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO DISPONIBLE)' . '</br>';
                    
                }
            }
            else {
                $messagePollscheduling .= $pollschedulingKey->getTitle() . ' - ' . $pollschedulingKey->getPollschedulingPeriod() . ' (NO PARTICIPA)' . '</br>';
            }

            // Recover Pollapplicationquestions
            $pollapplicationquestions = Pollapplicationquestion::getResultPollapplicationquestionByPollscheduling($this->getDoctrine(), $evaluatedid, $pollscheduling['id'], TRUE);
            
            if (!empty($pollapplicationquestions)) {
                
                $pollschedulingReport = Pollscheduling::calculatePercentageSection($arrayGroupPoll, $pollapplicationquestions);
                
                $pollschedulingTotals = array(
                    'percentage' => $pollschedulingReport['percentage']['consolidate'], 
                    'count' => $pollschedulingReport['percentage']['count']
                );
                
                $pollschedulingPercentages = Pollscheduling::percentagePollschedulingWithoutIndentation($pollschedulingReport['sections']);
                
            }
        }
        
        if (!empty($pollapplicationquestions)) {

            // Titles
            $pollschedulingTitles = Pollscheduling::titlePollschedulingWithIndentation($pollschedulingReport['sections']);
            
            // Count report vertical
            $countVertical = count($pollschedulingPercentages);
            
            // Count report horizontal
            $countHorizontal = 1;
            
            // Total Rows
            for ($j = 0; $j < $countVertical; $j++) {
                $percentageTotalRow = 0;
                $percentageTotalRow += $pollschedulingPercentages[$j]['percentage'];
                $pollschedulingPercentageRows[$j] = $percentageTotalRow / $countHorizontal;
            }
            
            // Row Results
            $row_total = 0;
            foreach ($pollschedulingTotals as $total) {
                $row_total += $total['percentage'];
            }
            $pollschedulingTotalRows = $row_total / $countHorizontal;

            $array_parameters['title_results'] = $pollschedulingTitles;
            $array_parameters['report_results'] = $pollschedulingPercentages;
            $array_parameters['report_rows'] = $pollschedulingPercentageRows;
            $array_parameters['total_results'] = $pollschedulingTotals;
            $array_parameters['row_results'] = $pollschedulingTotalRows;
            $array_parameters['count_horizontal'] = $countHorizontal;
            $array_parameters['count_vertical'] = $countVertical;
        }
        
        if ($messagePollscheduling != '') {
            $messageError = $messagePollscheduling;
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Workbook:poll_result.html.twig', $array_parameters);
    }

    /**
     * Muestra la información de un workbook. Acá el participante evalúa a uno de sus colaboradores
     * Incluye el Myer briggs, y los talentos más y menos desarrollados
     */
    public function workshopActivitiesWorkbookShowAction($workbookid)
    {
        $workbook = null;
        $myerbriggs = null;
        $moreTalents = null;
        $lessTalents = null;
        $evaluated = null;

        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();

        //Recover workbook
        $workbook = $em->getRepository('FishmanWorkshopBundle:Workbook')->find($workbookid);
        if (!$workbook) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities'));
        }

        $workshopapplicationactivity = $workbook->getWorkshopapplicationactivity();
        $workshopapplicacion = $workshopapplicationactivity->getWorkshopapplication();

        //ACCESS CONTROL
        if($workshopapplicacion->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        if($workshopapplicationactivity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        //La actividad esta asignada al wi del usuario actual
        $wi = $session->get('workinginformation');
        if($workshopapplicacion->getWorkinginformation()->getId() != $wi['id']){
            $session->getFlashBag()->add('error',
                'La persona con la que está tratado de ingresar no tiene permiso para esta actividad');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));            
        }
        //FIN - ACCESS CONTROL

        $myerbriggs = $workbook->getMyerbriggs();
        $moreTalents = $workbook->getMoreTalents();
        $lessTalents = $workbook->getLessTalents();

        //Evaluated
        $evaluated = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($workbook->getEvaluated());
        
        if (!$evaluated) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la información del evaluado.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities'));
        } else {
            $evaluatedName = $evaluated->getUser()->getNames().' '.$evaluated->getUser()->getSurname().' '.$evaluated->getUser()->getLastname();
        }
        
        // return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Workbook:show.html.twig', array(
            'workbook' => $workbook,
            'workshopapplicationactivity' => $workshopapplicationactivity,
            'myerbriggs' => $myerbriggs,
            'moreTalents' => $moreTalents,
            'lessTalents' => $lessTalents,
            'evaluatedName' => $evaluatedName,
            'evaluated' => $evaluated
        ));
    }

    /**
     * Esta misma acción es usado para editar los Myer Briggs
     */
    public function workshopActivitiesWorkbookMyerbriggsEditAction(Request $request, $workbookid)
    {
        $workbook = null;
        $myerbriggs = null;
        $js = null;

        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();

        $workbook = $em->getRepository('FishmanWorkshopBundle:Workbook')->find($workbookid);
        if (!$workbook) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad workbook.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities'));
        }

        $workshopapplicationactivity = $workbook->getWorkshopapplicationactivity();
        $workshopapplicacion = $workshopapplicationactivity->getWorkshopapplication();

        //ACCESS CONTROL
        if($workshopapplicacion->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        if($workshopapplicationactivity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        //La actividad esta asignada al wi del usuario actual
        $wi = $session->get('workinginformation');
        if($workshopapplicacion->getWorkinginformation()->getId() != $wi['id']){
            $session->getFlashBag()->add('error',
                'La persona con la que está tratado de ingresar no tiene permiso para esta actividad');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));            
        }

        if($workshopapplicationactivity->getFinished() == 1){
            $session->getFlashBag()->add('error', 'La actividad fue enviada. No se pueden hacer mas cambios en la misma');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }
        //FIN - ACCESS CONTROL


        $myerbriggs = $workbook->getMyerbriggs();

        //Si es que no existe crea uno nuevo
        if($myerbriggs == null){
            $myerbriggs = new MyerBriggs();
            $myerbriggs->setWorkbook($workbook);
        }

        $form = $this->createForm(new MyerbriggsType(), $myerbriggs);
        if($request->getMethod() == 'POST'){
            $form->bindRequest($request);

            $em->persist($myerbriggs);
            $em->flush();

            $wbQuery = $em->createQuery(
                'UPDATE FishmanWorkshopBundle:Workbook w
                 SET w.myerbriggs_status = :status,
                     w.changed = :changed
                 WHERE w.id = :workbookid')
                ->setParameter('workbookid', $workbookid)
                ->setParameter('changed', new \DateTime());

            //El myerbriggs esta completo
            if($myerbriggs->getEi() != '' &&
                $myerbriggs->getSn() != '' &&
                $myerbriggs->getTf() != '' &&
                $myerbriggs->getjP() != ''){

                $wbQuery->setParameter('status', 1);
            }
            else{
                $wbQuery->setParameter('status', 0);
            }
            $wbQuery->execute();                

            //Si la fecha de inicio de la actividad es null, le pone la fecha actual
            $workshopapplicationactivity = $workbook->getWorkshopapplicationactivity();
            if($workshopapplicationactivity->getInitdate() == null){
                $workshopapplicationactivity->setInitdate(new \DateTime());
            }
            $em->persist($workshopapplicationactivity);
            $em->flush();

            $js = 'parent.$.fancybox.close();';
        }
        
        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Workbook/Myerbriggs:edit.html.twig', array(
            'workbook' => $workbook,
            'myerbriggs', $myerbriggs,
            'form' => $form->createView(),
            'js' => $js
        ));
    }

    /**
     * Esta acción se usa tanto para crear como para editar los talentos de las personas
     * @param $workbookid, el id del workbook al que pertenecen los talentos
     * @param $type, el tipo. Puede ser 'more' o 'less'
     */
    public function workshopActivitiesWorkbookTalentsEditAction(Request $request, $workbookid, $type)
    {
        $workbook = null;
        $talents = null;
        $js = '';

        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();

        $workbook = $em->getRepository('FishmanWorkshopBundle:Workbook')->find($workbookid);
        if (!$workbook) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad workbook.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities'));
        }

        $workshopapplicationactivity = $workbook->getWorkshopapplicationactivity();
        $workshopapplicacion = $workshopapplicationactivity->getWorkshopapplication();

        //ACCESS CONTROL
        if($workshopapplicacion->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        if($workshopapplicationactivity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        //La actividad esta asignada al wi del usuario actual
        $wi = $session->get('workinginformation');
        if($workshopapplicacion->getWorkinginformation()->getId() != $wi['id']){
            $session->getFlashBag()->add('error',
                'La persona con la que está tratado de ingresar no tiene permiso para esta actividad');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));            
        }

        if($workshopapplicationactivity->getFinished() == 1){
            $session->getFlashBag()->add('error', 'La actividad fue enviada. No se pueden hacer mas cambios en la misma');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }
        //FIN - ACCESS CONTROL


        $direction = '';
        switch($type){
            case 'more':
                $talents = $workbook->getMoreTalents();
                $direction = '+';
                $oppositeDireccion = '-';
                break;
            case 'less':
                $talents = $workbook->getLessTalents();
                $direction = '-';
                $oppositeDireccion = '+';
                break;
        }


        if($request->getMethod() == 'POST') {

            //Borrar todos los talentos de ese tipo y volver a crearlos
            $query = $em->createQuery(
                'DELETE FROM FishmanWorkshopBundle:Workbooktalent wt
                 WHERE wt.workbook = :workbook
                     AND wt.direction = :direction')
                ->setParameter('workbook', $workbook)
                ->setParameter('direction', $direction);
            $query->execute();

            $talent_options = $request->request->get('talent_options');

            if (isset($talent_options['data2'])) {
                $talent_options = $talent_options['data2'];
                $sequence = 1;
                foreach($talent_options as $to){
                    $wt = new Workbooktalent();

                    $talent = $em->getRepository('FishmanEntityBundle:Talent')->find($to);
                    $wt->setTalent($talent);
                    $wt->setWorkbook($workbook);
                    $wt->setDirection($direction);
                    $wt->setSequence($sequence++);

                    $em->persist($wt);
                    $em->flush();
                }

                //Verifica si tiene un minimo de 12 talentos mas desarrollados y 12 menos desarrollados
                $oppositeTalentCount = $em->createQuery('SELECT count(t.id)
                                           FROM FishmanWorkshopBundle:Workbooktalent t
                                           WHERE t.workbook = :workbook
                                               AND t.direction = :direction')
                                           ->setParameter('workbook', $workbook)
                                           ->setParameter('direction', $oppositeDireccion)
                                           ->getSingleResult();
                
                $oppositeTalentCount = $oppositeTalentCount['1'];

                $wbQuery = $em->createQuery(
                    'UPDATE FishmanWorkshopBundle:Workbook w
                     SET w.talents_status = :status,
                         w.changed = :changed
                     WHERE w.id = :workbookid')
                    ->setParameter('workbookid', $workbookid)
                    ->setParameter('changed', new \DateTime());

                if( (count($talent_options) >= 12) && ($oppositeTalentCount >= 12) ){
                    $wbQuery->setParameter('status', 1);
                }
                else{
                    $wbQuery->setParameter('status', 0);
                }
                $wbQuery->execute();
            }

            //Si la fecha de inicio de la actividad es null, le pone la fecha actual
            $workshopapplicationactivity = $workbook->getWorkshopapplicationactivity();
            if($workshopapplicationactivity->getInitdate() == null){
                $workshopapplicationactivity->setInitdate(new \DateTime());
            }
            $em->persist($workshopapplicationactivity);
            $em->flush();
            
            $js = 'parent.$.fancybox.close();';
        }

        //Selected Options
        $selectedOptions = array();
        foreach($talents as $t){
            $selectedOptions[$t->getTalent()->getId()] = $t->getTalent()->getTalent();
        }

        //All options except the selected talents at opposite
        
        $repository = $em->getRepository('FishmanWorkshopBundle:Workbooktalent');
        $queryTalentsnoDirection = $repository->createQueryBuilder('wt')
            ->select('t.id')
            ->innerJoin('wt.talent', 't')
            ->where('wt.workbook = :workbook 
                    AND wt.direction != :direction')
            ->andWhere('t.status = 1')
            ->setParameter('workbook', $workbookid)
            ->setParameter('direction', $direction)
            ->getQuery()
            ->getResult();
        
        $noTalents = array();
        if ($queryTalentsnoDirection) {
            foreach ($queryTalentsnoDirection as $tnd) {
                $noTalents[] = $tnd['id'];
            }
        }
        
        $allActiveTalents = $em->getRepository('FishmanEntityBundle:Talent')->findByStatus(1);
        $allOptions = array();
        foreach($allActiveTalents as $at){
            if (!in_array($at->getId(), $noTalents)) {
                $allOptions[$at->getId()] = $at->getTalent();
            }
        }

        // Return
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Workbook/Talents:edit.html.twig', array(
            'workbook' => $workbook,
            'talents' => $talents,
            'selectedoptions' => $selectedOptions,
            'alloptions' => $allOptions,
            'direction' => $direction,
            'js' => $js
        ));
    }

    /**
     * Lists Activities of group plan.
     *
     */
    public function workshopactivitiesgroupplanAction(Request $request, $activityid)
    {
        return $this->_workshopactivitiesPlan($request, $activityid);
    }

    /**
     * Se usará para crear y editar objetivos de un plan grupal.
     * Para crear uno nuevo hay que pasarle el parametro $goalplanid como 0
     **/
    public function workshopActivitiesGroupplanPlangoalEditAction(Request $request, $planid, $plangoalid)
    {
        return $this->_workshopActivitiesPlangoalEdit($request, $planid, $plangoalid);
    }

    public function _workshopActivitiesPlangoalEdit(Request $request, $planid, $plangoalid)
    {
        $js = '';
        $session = $request->getSession();
        //$user = $this->get('security.context')->getToken()->getUser();
        //$roles = $user->getRoles();
        $em = $this->getDoctrine()->getManager();
        $plan = $em->getRepository('FishmanWorkshopBundle:Plan')->find($planid);
        
        //Se obtiene el wi de la sesión
        $wi = $session->get('workinginformation');
       
        //Verificar que tiene permiso para resolver el taller la actividad
        $activity = $plan->getWorkshopapplicationactivity();
        $workshopapplicacion = $activity->getWorkshopapplication();

        //ACCESS CONTROL
        if($workshopapplicacion->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        if($activity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        //La actividad esta asignada al wi del usuario actual
        if($workshopapplicacion->getWorkinginformation()->getId() != $wi['id']){
            $session->getFlashBag()->add('error',
                      'La persona con la que está tratado de ingresar no tiene permiso para esta actividad');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));            
        }

        //Comprobar que la actividad no este enviada
        if($activity->getFinished() == 1){
            $session->getFlashBag()->add('error',
                      'La actividad ya ha sido enviada. No se puede modificar su información');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));                        
        }
        //FIN - ACCESS CONTROL

        if($plan->getType() == 'group'){
            $renderPath = 'FishmanFrontEndBundle:Dashboard/Workshop/Activity/Groupplan:plangoaledit.html.twig';
        }
        else{
            $renderPath = 'FishmanFrontEndBundle:Dashboard/Workshop/Activity/Personalplan:plangoaledit.html.twig';
        }

        //Si es que es nuevo
        if($plangoalid == 0){
            $plangoal = new Plangoal();
            $plangoal->setCompleted(0);
            $plangoal->setPlan($plan);            
        }
        else{
            $plangoal = $em->getRepository('FishmanWorkshopBundle:Plangoal')->find($plangoalid);
        }
        
        $form = $this->createForm(new PlangoalType($this->getDoctrine(), $plangoal, $wi['id']), $plangoal);
        if($request->getMethod() == 'POST'){
            $form->bind($request);
            if($form->isValid()){
                
                $plangoal->setPlan($plan);
                $em->persist($plangoal);
                $em->flush();

                if ($activity->getInitdate() == '') {
                    $activity->setInitdate(new \DateTime());
                }
                $em->persist($activity);
                $em->flush();

                $js = 'parent.$.fancybox.close();';
                
                $session->getFlashBag()->add('status', 'Se guardó satisfactoriamente.');
            }

            //Debe mostrar solo un mensaje y luego cerrar.
        }
        
        // Return
        return $this->render($renderPath, array(
            'plangoal' => $plangoal,
            'form' => $form->createView(),
            'js' => $js
        ));
    }

    public function workshopActivitiesGroupplanPlangoalDropAction($plangoalid)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FishmanWorkshopBundle:Plangoal')->find($plangoalid);

        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad plan goal.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities'));
        }

        $deleteForm = $this->createDeleteForm($plangoalid);

        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Groupplan:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));        
    }

    public function workshopActivitiesGroupplanPlangoalDeleteAction(Request $request, $plangoalid)
    {
        return $this->_workshopActivitiesPlangoalDeleteAction($request, $plangoalid, 'group');
    }

    public function _workshopActivitiesPlangoalDeleteAction(Request $request, $plangoalid, $type)
    {
        $session = $request->getSession();
        $form = $this->createDeleteForm($plangoalid);
        $form->bind($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FishmanWorkshopBundle:Plangoal')->find($plangoalid);
        $plan = $entity->getPlan();
        $activity = $plan->getWorkshopapplicationactivity();
        $workshopapplicacion = $activity->getWorkshopapplication();

        //ACCESS CONTROL
        if($workshopapplicacion->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        if($activity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        //La actividad esta asignada al wi del usuario actual
        $wi = $session->get('workinginformation');
        if($workshopapplicacion->getWorkinginformation()->getId() != $wi['id']){
            $session->getFlashBag()->add('error',
                      'La persona con la que está tratado de ingresar no tiene permiso para esta actividad');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));            
        }

        //Comprobar que la actividad no este enviada
        if($activity->getFinished() == 1){
            $session->getFlashBag()->add('error',
                      'La actividad ya ha sido enviada. No se puede modificar su información');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));                        
        }
        //FIN - ACCESS CONTROL


        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad plan goal.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities'));
        }

        $activityid = $entity->getPlan()->getWorkshopapplicationactivity()->getId();

        if ($form->isValid()) {
            $em->remove($entity);
            $em->flush();
                
            // set flash messages
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('status', 'El registro fue eliminado satisfactoriamente.');
        }

        if($type == 'group'){
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities_groupplan', array(
                'activityid' => $activityid
            )));
        }
        else{
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities_individualplan', array(
                'activityid' => $activityid
            )));
        }
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Lists Activities of individual plan.
     */
    public function workshopactivitiesindividualplanAction(Request $request, $activityid)
    {
        return $this->_workshopactivitiesPlan($request, $activityid);
    }

    /**
     * Una abstracción para el plan grupal y plan individual
     */
    private function _workshopactivitiesPlan(Request $request, $activityid)
    {
        // Find Entities
        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();

        $activity = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($activityid);
        $workshopapplicacion = $activity->getWorkshopapplication();

        //ACCESS CONTROL
        if($workshopapplicacion->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        if($activity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        //La actividad esta asignada al wi del usuario actual
        $wi = $session->get('workinginformation');
        if($workshopapplicacion->getWorkinginformation()->getId() != $wi['id']){
            $session->getFlashBag()->add('error',
                      'La persona con la que está tratado de ingresar no tiene permiso para esta actividad');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));            
        }
        //FIN - ACCESS CONTROL

        $plan = $em->getRepository('FishmanWorkshopBundle:Plan')->findByWorkshopapplicationactivity($activity);
        if (!$plan) {
            //Se agrega el plan
            $plan = Plan::createPlan($this->getDoctrine(), $activity);
            if(!$plan){
                //Mensaje de que no es una actividad de tipo plan
                $session->getFlashBag()->add('error', 'La actividad no es de tipo ' . $typeText);
            }
        }
        else {
            $plan = $plan[0];
        }

        if($plan->getType() == 'group'){
            $typeText = ' Plan Grupal ';
            $path = 'fishman_front_end_workshop_activities_groupplan';
            $renderPath = 'FishmanFrontEndBundle:Dashboard/Workshop/Activity/Groupplan:index.html.twig';
        }
        else{
            $typeText = ' Plan Individual ';
            $path = 'fishman_front_end_workshop_activities_individualplan';
            $renderPath = 'FishmanFrontEndBundle:Dashboard/Workshop/Activity/Personalplan:index.html.twig';
        }

        //Solo se puede hacer estas operaciones si el plan ya existia
        if ($request->getMethod() == 'POST') {

            $waa_entity = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($activityid);

            if ($activity->getFinished() == 1) {
                $session->getFlashBag()->add('error', 'La actividad ya ha sido enviada. Esta operación no está permitida.');
            }
            else {
                
                $sendcomment = $request->request->get('sendcomment');
                $sendplan = $request->request->get('sendplan');

                $comment = trim($request->request->get('plancomment'));
                $plan->setComments($comment);

                if($sendcomment){ //Si se está actualizando el comentario 
                    
                    $em->persist($plan);

                    if ($waa_entity->getInitdate() == '') {
                        $waa_entity->setInitdate(new \DateTime());
                    }

                    $em->persist($waa_entity);
                    $em->flush();

                    return $this->redirect($this->generateUrl($path, array(
                        'activityid' => $activityid,
                        'type' => $plan->getType()
                    )));
                            
                }
                else if($sendplan && $comment == ''){
                    $session->getFlashBag()->add('error', 'El comentario es obligatorio para enviar la actividad');
                }
                else if($sendplan){ //Si se está enviando el plan

                    //Verificar si tiene al menos un objetivo
                    $hasGoal = $em->createQuery('SELECT COUNT(pg.id) as num
                                                 FROM FishmanWorkshopBundle:Plangoal pg
                                                 WHERE pg.plan = :plan')
                                               ->setParameter('plan', $plan)
                                               ->getSingleResult();
                    
                    if($hasGoal['num'] > 0) {

                        //Se marca la actividad como completa
                        
                        $em->persist($plan);
                        $em->flush();
                        
                        $waa_entity->setFinished(TRUE);
                        if ($waa_entity->getInitdate() == '') {
                            $waa_entity->setInitdate(new \DateTime());
                        }
                        $waa_entity->setEnddate(new \DateTime());
                        
                        $em->persist($waa_entity);
                        $em->flush();
                        
                        $session->getFlashBag()->add('status', 'La actividad' . $typeText . 'ha sido enviada.');

                        // Send trigger notifications
                        
                        //Doctrine registry
                        $mailer = $this->get('mailer');
                        $logger = $this->get('logger');
                        $router = $this->get('router');
                        $templating = $this->get('templating');

                        Notificationexecution::sendTriggerNotifications($this->getDoctrine(), $mailer, $logger, $router, $templating, $activityid);
                        
                        // Active notificationExecution status send
                        Notificationexecution::activeNotificationexecutions($this->getDoctrine(), $activityid, 'activity', TRUE, FALSE, '-1', FALSE);

                        //Se usa la redirección para evitar que cada vez que se recarga la página se vuelvan
                        //a enviar los datos por post
                        return $this->redirect($this->generateUrl($path, array(
                            'activityid' => $activityid,
                            'type' => $plan->getType()
                        )));
                    }
                    else{
                        $session->getFlashBag()->add('error', 'La actividad' . $typeText . 'debe tener al menos un objetivo.');
                        return $this->redirect($this->generateUrl($path, array(
                            'activityid' => $activityid,
                            'type' => $plan->getType()
                        )));
                    } // al menos un objetivo

                } //Fin sendplan ...

            } //Fin finished

        } //Fin method POST

        $plangoals = Plangoal::getPlangoalsList($this->getDoctrine(), $plan);
        
        return $this->render($renderPath, array(
            'activity' => $activity,
            'plan' => $plan,
            'plangoals' => $plangoals
        ));
    }

    public function workshopActivitiesIndividualplanPlangoalDropAction($plangoalid)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FishmanWorkshopBundle:Plangoal')->find($plangoalid);

        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la actividad plan goal.');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities'));
        }

        $deleteForm = $this->createDeleteForm($plangoalid);

        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Personalplan:drop.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));        
    }

    public function workshopActivitiesIndividualplanPlangoalDeleteAction(Request $request, $plangoalid)
    {
        return $this->_workshopActivitiesPlangoalDeleteAction($request, $plangoalid, 'personal');
    }

    public function workshopActivitiesIndividualplanPlangoalEditAction(Request $request, $planid, $plangoalid)
    {
        return $this->_workshopActivitiesPlangoalEdit($request, $planid, $plangoalid);
    }

    /**
     * Question of Lists Activities.
     *
     */
    public function workshopactivitiesquestionAction(Request $request, $activityid)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        $answer = '';
        
        //Recovering data to template
        
        $workshopapplicationactivity = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($activityid);
        $workshopapplicacion = $workshopapplicationactivity->getWorkshopapplication();
        $workshopschedulingactivity = $em->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity')->find($workshopapplicationactivity->getworkshopschedulingactivityId());
        
        //ACCESS CONTROL
        
        if($workshopapplicacion->getDeleted() == 1 || $workshopapplicationactivity->getDeleted() == 1){
            $session->getFlashBag()->add('error', 'No existe la actividad que está solicitando');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }
        
        //La actividad esta asignada al wi del usuario actual
        $wi = $session->get('workinginformation');
        if($workshopapplicacion->getWorkinginformation()->getId() != $wi['id']){
            $session->getFlashBag()->add('error',
                'La persona con la que está tratado de ingresar no tiene permiso para esta actividad');
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));            
        }
        
        //FIN - ACCESS CONTROL
        
        $repository = $this->getDoctrine()->getRepository('FishmanWorkshopBundle:Questionapplication');
        $questionapplication_query = $repository->createQueryBuilder('qa')
            ->where('qa.workshopapplicationactivity = :activity')
            ->setParameter('activity', $activityid)
            ->getQuery()
            ->getResult();
        $questionapplication = current($questionapplication_query);
        
        // Define variables
        
        $answerType = $workshopschedulingactivity->getAnswerType(); 
        $options = $workshopschedulingactivity->getOptions(); 
        $alignment = $workshopschedulingactivity->getAlignment();
        if (!empty($questionapplication)) {
            if (!in_array($answerType, array('simple_text', 'multiple_text'))) {
                $answer = $questionapplication->getAnswerOptions();
            }
            else {
                $answer = $questionapplication->getAnswer();
            }
        }

        //Save data in Questionapplication

        $postData = $request->request->get('questions', null);
        
        if (!empty($postData[$workshopapplicationactivity->getId()])) {
          
            $answerForm = current($postData);
            
            // Verify is Questionapplication si true
            
            if (!empty($questionapplication)) {
                
                $entity = $questionapplication;
                
                if (!in_array($answerType, array('simple_text', 'multiple_text'))) {
                    $default_options = $workshopschedulingactivity->getOptions();
                    $score = '';
                    
                    if ($answerType == 'multiple_option') {
                        foreach ($answerForm as $op) {
                            $score += $default_options[$op]['score'];
                        }
                    }
                    else {
                        $score = $default_options[$answerForm]['score'];
                    }
            
                    $entity->setAnswerOptions($answerForm);
                    $entity->setScore($score);
                }
                else {     
                    $entity->setAnswer($answerForm);
                }
                    
                $em->persist($entity);
                $em->flush();
            }
            else {
                $entity = new Questionapplication();
    
                $entity->setWorkshopapplicationactivity($workshopapplicationactivity);
                $entity->setAnswerType($answerType);
                if (!in_array($answerType, array('simple_text', 'multiple_text'))) {
                    $default_options = $workshopschedulingactivity->getOptions();
                    $score = '';

                    if ($answerType == 'multiple_option') {
                        foreach ($answerForm as $op) {
                            $score += $default_options[$op]['score'];
                        }
                    }
                    else {
                        $score = $default_options[$answerForm]['score'];
                    }
    
                    $entity->setAnswerOptions($answerForm);
                    $entity->setScore($score);
                }
                else {
                    $entity->setAnswer($answerForm);
                }
                
                $em->persist($entity);
                $em->flush();
            }

            if($request->request->get('send', false)){
              
                $workshopapplicationactivity->setFinished(TRUE);
                if ($workshopapplicationactivity->getInitdate() == '') {
                    $workshopapplicationactivity->setInitdate(new \DateTime());
                }
                $workshopapplicationactivity->setEnddate(new \DateTime());
                
                $em->persist($workshopapplicationactivity);
                $em->flush();
                
                $session->getFlashBag()->add('status', 'La actividad ha sido enviada');

                // Send trigger notifications
                
                //Doctrine registry
                $mailer = $this->get('mailer');
                $logger = $this->get('logger');
                $router = $this->get('router');
                $templating = $this->get('templating');

                Notificationexecution::sendTriggerNotifications($this->getDoctrine(), $mailer, $logger, $router, $templating, $activityid);
                
                // Active notificationExecution status send
                Notificationexecution::activeNotificationexecutions($this->getDoctrine(), $activityid, 'activity', TRUE, FALSE, '-1', FALSE);

            }
            else {
                $session->getFlashBag()->add('status', 'Los cambios han sido guardados exitosamente');
            }
            
            return $this->redirect($this->generateUrl('fishman_front_end_workshop_activities', array()));
        }

        $workshopapplicationactivity->setInitdate(new \DateTime());
        
        $em->persist($workshopapplicationactivity);
        $em->flush();

        if($request->request->get('send', false)){
            $session->getFlashBag()->add('error', 'Debe responder a la pregunta');
        }
        
        return $this->render('FishmanFrontEndBundle:Dashboard/Workshop/Activity/Question:index.html.twig', array(
           'activity' => $workshopapplicationactivity,
           'answer_type' => $answerType,
           'options' => $options,
           'alignment' => $alignment,
           'answer' => $answer
        ));
    }

}
