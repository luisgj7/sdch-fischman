<?php

namespace Fishman\FrontEndBundle\Twig;
use Symfony\Component\HttpKernel\Kernel;
use Fishman\PollBundle\Entity\Pollapplicationquestion;

class FishmanExtension extends \Twig_Extension
{
    private $kernel;

    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;
    }

    public function getName()
    {
        return 'fishman_extension';
    }
    
    public function getFilters()
    {
        return array(
            'booleanName' => new \Twig_Filter_Method($this, 'booleanNameFilter'),
            'statusName' => new \Twig_Filter_Method($this, 'statusNameFilter'),
            'statusNotificationName' => new \Twig_Filter_Method($this, 'statusNotificationNameFilter'),
            'maritalstatusName' => new \Twig_Filter_Method($this, 'maritalstatusNameFilter'),
            'sexName' => new \Twig_Filter_Method($this, 'sexNameFilter'),
            'identityName' => new \Twig_Filter_Method($this, 'identityNameFilter'),
            'ouTypeName' => new \Twig_Filter_Method($this, 'ouTypeNameFilter'),
            'ouUnitTypeName' => new \Twig_Filter_Method($this, 'ouUnitTypeNameFilter'),
            'levelName' => new \Twig_Filter_Method($this, 'levelNameFilter'),
            'orderName' => new \Twig_Filter_Method($this, 'orderNameFilter'),
            'divideName' => new \Twig_Filter_Method($this, 'divideNameFilter'),
            'sectionName' => new \Twig_Filter_Method($this, 'sectionNameFilter'),
            'answertypeName' => new \Twig_Filter_Method($this, 'answertypeNameFilter'),
            'alignmentName' => new \Twig_Filter_Method($this, 'alignmentNameFilter'),
            'mandatoryresponseName' => new \Twig_Filter_Method($this, 'mandatoryresponseNameFilter'),
            'usetermsName' => new \Twig_Filter_Method($this, 'usetermsNameFilter'),
            'typedocumentName' => new \Twig_Filter_Method($this, 'typedocumentNameFilter'),
            'typeevaluationName' => new \Twig_Filter_Method($this, 'typeevaluationNameFilter'),
            'periodName' => new \Twig_Filter_Method($this, 'periodNameFilter'),
            'finishedName' => new \Twig_Filter_Method($this, 'finishedNameFilter'),
            'notificationtypeName' => new \Twig_Filter_Method($this, 'notificationtypeNameFilter'),
            'ejecutiontypeName' => new \Twig_Filter_Method($this, 'ejecutiontypeNameFilter'),
            'triggerName' => new \Twig_Filter_Method($this, 'triggerNameFilter'),
            'replayName' => new \Twig_Filter_Method($this, 'replayNameFilter'),
            'repeatperiodName' => new \Twig_Filter_Method($this, 'repeatperiodNameFilter'),
            'finishName' => new \Twig_Filter_Method($this, 'finishNameFilter'),
            'rolName' => new \Twig_Filter_Method($this, 'rolNameFilter'),
            'profileName' => new \Twig_Filter_Method($this, 'profileNameFilter'),
            'completeName' => new \Twig_Filter_Method($this, 'completeNameFilter'),
            'cut' => new \Twig_Filter_Method($this, 'filterCut', array('length' => false, $wordCut = false, 'appendix' => false)),
            'typeOuName' => new \Twig_Filter_Method($this, 'typeOuFilter'),
            'validToString' => new \Twig_Filter_Method($this, 'validToStringFilter'),
            'myersbriggsName' => new \Twig_Filter_Method($this, 'myersbriggsNameFilter'),
            'myersbriggsAbbreviation' => new \Twig_Filter_Method($this, 'myersbriggsAbbreviationFilter'),
            'companyType' => new \Twig_Filter_Method($this, 'companyTypeFilter'),
            'companyTypeName' => new \Twig_Filter_Method($this, 'companyTypeNameFilter'),
            'genderName' => new \Twig_Filter_Method($this, 'genderNameFilter'),
            'educationSystemName' => new \Twig_Filter_Method($this, 'educationSystemNameFilter'),
            'gradeName' => new \Twig_Filter_Method($this, 'gradeNameFilter'),
            'viaName' => new \Twig_Filter_Method($this, 'viaNameFilter'),
            'filterCompanyName' => new \Twig_Filter_Method($this, 'filterCompanyNameFilter'),
            'onePerEvaluator' => new \Twig_Filter_Method($this, 'onePerEvaluatorFilter'),
            'workinginformationType' => new \Twig_Filter_Method($this, 'workinginformationTypeFilter'),
            'statusEvaluationName' => new \Twig_Filter_Method($this, 'statusEvaluationNameFilter'),
            'statusBarName' => new \Twig_Filter_Method($this, 'statusBarNameFilter'),
            'evaluationRolName' => new \Twig_Filter_Method($this, 'evaluationRolNameFilter')
        );
    }
    
    public function getFunctions()
    {
        return array(
            'calculatePercentage' => new \Twig_Function_Method($this, 'calculatePercentageFunction', array('quantity' => true)),
            'dateIsBefore' => new \Twig_Function_Method($this, 'dateIsBeforeFunction'),
            'dateIsAfter' => new \Twig_Function_Method($this, 'dateIsAfterFunction'),
            'asignedUnselectedOptions' => new \Twig_Function_Method($this, 'asignedUnselectedOptionsFunction'),
            'asignedUnselectedOptions2' => new \Twig_Function_Method($this, 'asignedUnselectedOptions2Function'),
            'asignedUnselectedOptionsPoll' => new \Twig_Function_Method($this, 'asignedUnselectedOptionsPollFunction'),
            'sexUnselectedOptions' => new \Twig_Function_Method($this, 'sexUnselectedOptionsFunction'),
            'profileUnselectedOptions' => new \Twig_Function_Method($this, 'profileUnselectedOptionsFunction'),
            'advancedSelectSplit' => new \Twig_Function_Method($this,  'advancedSelectSplitFunction'),
            'questionWidgets' => new \Twig_Function_Method($this,  'questionWidgetsFunction', array('type' => true, 'options' => false, 'alignment' => false)),
            'pollQuestions' => new \Twig_Function_Method($this,  'pollQuestionsFunction', array('preview' => false, 'preview_multiple' => false)),
            'pollQuestionsHorizontal' => new \Twig_Function_Method($this,  'pollQuestionsHorizontalFunction', array('preview' => false)),
        );
    }

    public function getGlobals()
    {
        $http_protocol = 'http';
        $base_root = $http_protocol . '://' . $_SERVER['HTTP_HOST'];
        $base_path = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/');
        $globaljavascript = "<script type='text/javascript'>\n";

        $environment = '';
        if($this->kernel->getEnvironment() == 'dev'){
            $environment = '/app_dev.php';
        }

        $variables['basepath'] = $base_root . $base_path . $environment;
        $variables['settings'] = array();

        $globaljavascript .= 'var globals = ' . json_encode($variables);
        $globaljavascript .= "\n</script>";
        
        return array(
            'globaljavascript'=> $globaljavascript,
        );
    }

    /**
     * return percentage
     * @$quantity, la cantidad sobre la que se calculara el porcentaje
     * @$total, el valor 100%
     */
    public function calculatePercentageFunction($quantity, $total)
    {
        if ($quantity == '') {
            $quantity = 0;
        }
        if ($total == 0) {
            return '0%';
        }
        else {
            $percentage = round((($quantity * 100) / $total), 2);
        }
        $percentage .= '%';
        
        return $percentage;
    }

    /**
     * return date is before
     * if the value is null will return true.
     */
    public function dateIsBeforeFunction($date)
    {
        if(is_null($date)) return true;

        $now = time();
        $initdate = strtotime($date->format('Y-m-d H:i:s'));

        if ($initdate <= $now) {
          return true;
        }
        
        return false;
    }

    /**
     * Function to return date is after the $ date
     * if the value is null will return true.
     * if the value is greater than the current date will return true.
     * and if the value does not meet the two options will return false.
     *
     * Variables:
     * - $date: data with the date to interact.
     * - $now: retrieve current date.
     * - $initdate: Gets the date processed by strtotime function.
     */
    public function dateIsAfterFunction($date)
    {
        if(is_null($date)) return true;

        $now = time();
        $enddate = strtotime($date->format('Y-m-d H:i:s'));
        
        if (($enddate + (60*60*24)) >= $now) {
          return true;
        }
        
        return false;
    }

    /**
     * return asigned unselected
     */
    public function asignedUnselectedOptionsFunction($options)
    {
        $select_options = '';
        
        $options_default = array(
            'boss' => 'Jefe',
            'collaborator' => 'Colaborador',
            'integrant' => 'Participante',
            'company' => 'Empresa',
            'responsible' => 'Responsable'
        );
        
        if (!empty($options)) {
            foreach ($options as $op_text) {
                foreach ($options_default as $opd_value => $opd_text) {
                    if ($op_text == $opd_value) {
                        unset($options_default[$opd_value]);
                    }
                }
            }
        }
        
        foreach ($options_default as $value => $text) {
            $select_options .= '<option value="' . $value . '">' . $text . '</option>';
        }
        
        return $select_options;
    }

    /**
     * return asigned unselected Only Poll
     */
    public function asignedUnselectedOptions2Function($options, $not_evaluated = FALSE)
    {
        $select_options = '';
        
        $options_default = array(
            'boss' => 'Jefe',
            'collaborator' => 'Colaborador',
            'evaluated' => 'Evaluado',
            'evaluator' => 'Evaluador',
            'company' => 'Empresa',
            'responsible' => 'Responsable'
        );
        
        if (!empty($options)) {
            foreach ($options as $op_text) {
                foreach ($options_default as $opd_value => $opd_text) {
                    if ($op_text == $opd_value) {
                        unset($options_default[$opd_value]);
                    }
                }
            }
        }
        
        if ($not_evaluated) {
            unset($options_default['evaluated']);
        }
        
        foreach ($options_default as $value => $text) {
            $select_options .= '<option value="' . $value . '">' . $text . '</option>';
        }
        
        return $select_options;
    }

    /**
     * return asigned unselected Only Poll
     */
    public function asignedUnselectedOptionsPollFunction($options, $not_evaluated = FALSE)
    {
        $select_options = '';
        
        $options_default = array(
            /*'boss' => 'Jefe',
            'collaborator' => 'Colaborador',*/
            'evaluated' => 'Evaluado',
            'evaluator' => 'Evaluador',
            /*'company' => 'Empresa',
            'responsible' => 'Responsable'*/
        );
        
        if (!empty($options)) {
            foreach ($options as $op_text) {
                foreach ($options_default as $opd_value => $opd_text) {
                    if ($op_text == $opd_value) {
                        unset($options_default[$opd_value]);
                    }
                }
            }
        }
        
        if ($not_evaluated) {
            unset($options_default['evaluated']);
        }
        
        foreach ($options_default as $value => $text) {
            $select_options .= '<option value="' . $value . '">' . $text . '</option>';
        }
        
        return $select_options;
    }

    /**
     * Esta función separa los opciones seleccionadas, de las no seleccionadas
     * para un control de tipo advaced select. El control advanced select
     * contiene dos selects, uno para las opciones no seleccionadas y otro para
     * las seleccionadas
     */
    public function advancedSelectSplitFunction($allOptions, $selectedOptions)
    {
        $noSelectedOptions = array_diff_assoc($allOptions, $selectedOptions);

        $options = array('noselectedoptions' => '', 'selectedoptions' => '');
        foreach ($noSelectedOptions as $value => $text) {
            $options['noselectedoptions'] .= '<option value="' . $value . '">' . $text . '</option>';
        }

        foreach ($selectedOptions as $value => $text) {
            $options['selectedoptions'] .= '<option value="' . $value . '">' . $text . '</option>';
        }
        
        return $options;
    }

    /**
     * return sex unselected
     */
    public function sexUnselectedOptionsFunction($option)
    {
        $select_options = '<option value="">Elija una opción</option>';
        
        $options_default = array(
            'm' => 'Masculino',
            'f' => 'Femenino'
        );
        
        if ($option != '') {
            foreach ($options_default as $value => $text) {
                if ($value == $option) {
                    $select_options .= '<option value="' . $value . '" selected=true>' . $text . '</option>';
                    unset($options_default[$value]);
                }
            }
        }
        
        foreach ($options_default as $value => $text) {
            $select_options .= '<option value="' . $value . '">' . $text . '</option>';
        }
        
        return $select_options;
    }

    /**
     * return profile unselected
     */
    public function profileUnselectedOptionsFunction($option)
    {
        $select_options = '<option value="">Elija una opción</option>';
        
        $options_default = array(
            'boss' => 'Jefe',
            'collaborator' => 'Colaborador',
            'integrant' => 'Participante',
            'company' => 'Empresa'
        );
        
        if ($option != '') {
            foreach ($options_default as $value => $text) {
                if ($value == $option) {
                    $select_options .= '<option value="' . $value . '" selected=true>' . $text . '</option>';
                    unset($options_default[$value]);
                }
            }
        }
        
        foreach ($options_default as $value => $text) {
            $select_options .= '<option value="' . $value . '">' . $text . '</option>';
        }
        
        return $select_options;
    }
    
    /**
     * get Boolean Name
     * 
     */
    public function booleanNameFilter($id)
    {
      $name = '';
      
      if ($id == 0) {
        $name = 'No';
      }
      elseif ($id == 1) {
        $name = 'Si';
      }
      
      return $name;
    }
    
    /**
     * get status of Notification
     * 
     */
    public function statusNotificationNameFilter($id)
    {
      $name = '';
      
      if ($id == 'pending') {
        $name = 'Pendiente';
      }
      elseif ($id == 'sent') {
        $name = 'Enviado';
      }
      elseif ($id == 'in progress') {
        $name = 'En proceso';
      }
      
      return $name;
    }
    
    /**
     * get status of Evaluation
     * 
     */
    public function statusEvaluationNameFilter($id)
    {
      $name = '';
      
      if ($id == 0) {
        $name = 'Pendiente';
      }
      elseif ($id == 1) {
        $name = 'Listo';
      }
      
      return $name;
    }
    
    /**
     * get status Name
     * 
     */
    public function statusNameFilter($id)
    {
      $name = '';
      
      if ($id == 0) {
        $name = 'Desactivo';
      }
      elseif ($id == 1) {
        $name = 'Activo';
      }
      
      return $name;
    }
    
    /**
     * get marital status Name
     * 
     */
    public function maritalstatusNameFilter($id)
    {
      $name = '';
      
      if ($id == 'single') {
        $name = 'Soltero';
      }
      elseif ($id == 'married') {
        $name = 'Casado';
      }
      elseif ($id == 'widower') {
        $name = 'Viudo';
      }
      elseif ($id == 'divorced') {
        $name = 'Divorciado';
      }
      elseif ($id == 'cohabitant') {
        $name = 'Conviviente';
      }
      
      return $name;
    }
    
    /**
     * get sex Name
     * 
     */
    public function sexNameFilter($id)
    {
      $name = '';
      
      if ($id == 'm') {
        $name = 'Masculino';
      }
      elseif ($id == 'f') {
        $name = 'Femenino';
      }
      
      return $name;
    }
    
    /**
     * get identity Name
     * 
     */
    public function identityNameFilter($id)
    {
      $name = '';
      
      if ($id == 'dni') {
        $name = 'DNI';
      }
      elseif ($id == 'license') {
        $name = 'Carnet de Extrangería';
      }
      elseif ($id == 'passport') {
        $name = 'Pasaporte';
      }
      
      return $name;
    }
    
    /**
     * get organizational level type Name
     * 
     */
    public function ouTypeNameFilter($id)
    {
      $name = '';
      
      if ($id == 'management') {
        $name = 'Gerencia';
      }
      elseif ($id == 'directorate') {
        $name = 'Dirección';
      }
      elseif ($id == 'headship') {
        $name = 'Jefatura';
      }
      elseif ($id == 'administrative') {
        $name = 'Administrativo';
      }
      
      return $name;
    }

    /**
     * get organizational unit type Name
     * 
     */
    public function ouUnitTypeNameFilter($id)
    {
      $name = '';
      
      if ($id == 'administrative') {
        $name = 'Administrativa';
      }
      elseif ($id == 'academic') {
        $name = 'Académica';
      }
      
      return $name;
    }

    /**
     * get Sección type Name
     * 
     */
    public function levelNameFilter($id)
    {
      $name = '';
      
      if ($id == 'null' || $id === '0') {
        $name = 'Ninguno';
      }
      elseif ($id == '1') {
        $name = 'Nivel 1';
      }
      elseif ($id == '2') {
        $name = 'Nivel 2';
      }
      elseif ($id == '3') {
        $name = 'Nivel 3';
      }
      elseif ($id == '4') {
        $name = 'Nivel 4';
      }
      elseif ($id == '5') {
        $name = 'Nivel 5';
      }
      elseif ($id == '6') {
        $name = 'Nivel 6';
      }
      elseif ($id == '7') {
        $name = 'Nivel 7';
      }
      elseif ($id == '8') {
        $name = 'Nivel 8';
      }
      elseif ($id == '9') {
        $name = 'Nivel 9';
      }
      elseif ($id == '10') {
        $name = 'Todos';
      }
      
      return $name;
    }

    /**
     * get Order type Name
     * 
     */
    public function orderNameFilter($id)
    {
      $name = '';
      
      if ($id == '1') {
        $name = 'Ordenado';
      }
      elseif ($id == '0') {
        $name = 'Aleatorio';
      }
      
      return $name;
    }

    /**
     * get Order type Name
     * 
     */
    public function divideNameFilter($id)
    {
      $name = '';
      
      if ($id == 'sections_show') {
        $name = 'Secciones a mostrar';
      }
      elseif ($id == 'number_questions') {
        $name = 'Cantidad de preguntas';
      }
      elseif ($id == 'none') {
        $name = 'Ninguna';
      }
      
      return $name;
    }

    /**
     * get Section type Name
     * 
     */
    public function sectionNameFilter($id)
    {
      $name = '';
      
      if ($id == '1') {
        $name = 'Evaluacíon Multiple';
      }
      elseif ($id == '0') {
        $name = 'Evaluacíon Unica';
      }
      
      return $name;
    }
    
    /**
     * get Answer Type Name
     * 
     */
    public function answertypeNameFilter($id)
    {
      $name = '';
      
      if ($id == 'simple_text') {
        $name = 'Cuadro de texto simple';
      }
      elseif ($id == 'unique_option') {
        $name = 'Opción única';
      }
      elseif ($id == 'multiple_option') {
        $name = 'Opción múltiple';
      }
      elseif ($id == 'selection_option') {
        $name = 'Opción selección';
      }
      elseif ($id == 'multiple_text') {
        $name = 'Cuadro de texto múltiple';
      }
      
      return $name;
    }
    
    /**
     * get Alignment Name
     * 
     */
    public function alignmentNameFilter($id)
    {
      $name = '';
      
      if ($id == 'horizontal') {
        $name = 'Horizontal';
      }
      elseif ($id == 'vertical') {
        $name = 'Vertical';
      }
      
      return $name;
    }
    
    /**
     * get Mandatory Response
     * 
     */
    public function mandatoryresponseNameFilter($id)
    {
      $name = '';
      
      if ($id == 0) {
        $name = 'No';
      }
      elseif ($id == 1) {
        $name = 'Si';
      }
      
      return $name;
    }
    
    /**
     * get Use Terms
     * 
     */
    public function usetermsNameFilter($id)
    {
      $name = '';
      
      if ($id == 0) {
        $name = 'No';
      }
      elseif ($id == 1) {
        $name = 'Si';
      }
      
      return $name;
    }

    /**
     * get Type Document type Name
     * 
     */
    public function typedocumentNameFilter($id)
    {
      $name = '';
      
      if ($id == 'video') {
        $name = 'Video';
      }
      elseif ($id == 'embed') {
        $name = 'Embed';
      }
      elseif ($id == 'document') {
        $name = 'Documento';
      }
      elseif ($id == 'website') {
        $name = 'Página web';
      }
      
      return $name;
    }
    
    /**
     * get Evaluation type Name
     * 
     */
    public function typeevaluationNameFilter($id)
    {
      $name = '';
      
      if ($id == 'self_evaluation') {
        $name = 'Autoevaluación';
      }
      elseif ($id == 'evaluation_360') {
        $name = 'Evaluación 360';
      }
      elseif ($id == 'evaluation_collaborators') {
        $name = 'Evaluación a Colaboradores';
      }
      elseif ($id == 'evaluation_boss') {
        $name = 'Evaluación a Jefes';
      }
      elseif ($id == 'evaluation_teachers') {
        $name = 'Evaluación a Profesores';
      }
      elseif ($id == 'not_evaluateds') {
        $name = 'Sin Evaluados';
      }
      
      return $name;
    }
    
    /**
     * get Period Name
     * 
     */
    public function periodNameFilter($id)
    {
      $name = '';
      
      if ($id == 'day') {
        $name = 'Día(s)';
      }
      elseif ($id == 'week') {
        $name = 'Semana(s)';
      }
      elseif ($id == 'month') {
        $name = 'Mes(es)';
      }
      
      return $name;
    }
    
    /**
     * get Finished Name
     * 
     */
    public function finishedNameFilter($id)
    {
      $name = '';
      
      if ($id == '0') {
        $name = 'Pendiente';
      }
      elseif ($id == '1') {
        $name = 'Enviado';
      }
      
      return $name;
    }
    
    /**
     * get Complete Name
     * 
     */
    public function completeNameFilter($id)
    {
      $name = '';
      
      if ($id == '0') {
        $name = 'Pendiente';
      }
      elseif ($id == '1') {
        $name = 'Completo';
      }
      
      return $name;
    }
    
    /**
     * get Notification type Name
     * 
     */
    public function notificationtypeNameFilter($id)
    {
      $name = '';
      
      if ($id == 'activity') {
        $name = 'Actividad';
      }
      elseif ($id == 'poll') {
        $name = 'Encuesta';
      }
      elseif ($id == 'message') {
        $name = 'Mensaje';
      }
      
      return $name;
    }
    
    /**
     * get Ejecution type Name
     * 
     */
    public function ejecutiontypeNameFilter($id)
    {
      $name = '';
      
      if ($id == 'automatic') {
        $name = 'Automática';
      }
      elseif ($id == 'manual') {
        $name = 'Manual';
      }
      elseif ($id == 'trigger') {
        $name = 'Con Disparador';
      }
      
      return $name;
    }
    
    /**
     * get Replay Name
     * 
     */
    public function replayNameFilter($id)
    {
      $name = '';
      
      if ($id == 0) {
        $name = 'No';
      }
      elseif ($id == 1) {
        $name = 'Si';
      }
      
      return $name;
    }
    
    /**
     * get Repeat period Name
     * 
     */
    public function repeatperiodNameFilter($id)
    {
      $name = '';
      
      if ($id == 'day') {
        $name = 'Cada día';
      }
      elseif ($id == 'week') {
        $name = 'Cada semana';
      }
      elseif ($id == 'month') {
        $name = 'Cada mes';
      }
      elseif ($id == 'year') {
        $name = 'Cada año';
      }
      return $name;
    }
    
    /**
     * get Finish Name
     * 
     */
    public function finishNameFilter($id)
    {
      $name = '';
      
      if ($id == 'never') {
        $name = 'Nunca';
      }
      elseif ($id == 'after') {
        $name = 'Después de ';
      }
      elseif ($id == 'end_activity') {
        $name = 'Después de finalizar actividad';
      }
      
      return $name;
    }
    
    /**
     * get Trigger Name
     * 
     */
    public function triggerNameFilter($id)
    {
      $name = '';
      
      if ($id == 'to_send') {
        $name = 'Al enviar';
      }
      
      return $name;
    }
    
    /**
     * get Profile Name
     * 
     */
    public function profileNameFilter($id)
    {
      $name = '';
      
      if ($id == 'boss') {
        $name = 'Jefe';
      }
      elseif ($id == 'evaluated') {
        $name = 'Evaluado';
      }
      elseif ($id == 'evaluator') {
        $name = 'Evaluador';
      }
      elseif ($id == 'integrant') {
        $name = 'Participante';
      }
      elseif ($id == 'collaborator') {
        $name = 'Colaborador';
      }
      elseif ($id == 'company') {
        $name = 'Empresa';
      }
      elseif ($id == 'responsible') {
        $name = 'Responsable';
      }
      
      return $name;
    }
    
    /**
     * get type OU Name
     * 
     */
    public function TypeOuFilter($id)
    {
      $name = '';
      
      if ($id == 'management') {
        $name = 'Gerencia';
      }
      elseif ($id == 'administrative') {
        $name = 'Administrativo';
      }
      
      return $name;
    }

    /**
     * transform canonical rol name to human rediable rol name
     * 
     */
    public function rolNameFilter($canonicalRolName)
    {
      switch($canonicalRolName){
          case 'ROLE_SUPER_ADMIN':
              return 'Super Administrador';
          case 'ROLE_ADMIN':
              return 'Administrador';
          case 'ROLE_TEACHER':
              return 'Responsable';
          case 'ROLE_USER':
              return 'Usuario';
      }
      
      return '';
    }

    /**
     * @param string $text
     * @param integer $length
     * @param boolean $wordCut
     * @param string $appendix
     * @return string
     */
    public function filterCut($text, $length = 160, $wordCut = true, $appendix = ' ...')
    {
        $maxLength = (int)$length - strlen($appendix);
        if (strlen($text) > $maxLength) {
            if($wordCut){
                $text = substr($text, 0, $maxLength + 1);
                $text = substr($text, 0, strrpos($text, ' '));
            }
            else {
                $text = substr($text, 0, $maxLength);
            }
            $text .= $appendix;
        }
        
        return $text;
    }

    /**
     * Se usa para ver si un dato o conjunto de datos se consideran válidos.
     * 0, no es válido. Devuelve 'No'
     * 1, es válido. Devuelve 'Sí'
     */
    public function validToStringFilter($valid)
    {
      $name = '';
      
      if ($valid == 0) {
        $name = 'No';
      }
      elseif ($valid == 1) {
        $name = 'Sí';
      }
      
      return $name;
    }
    
    /**
     * get Company Type Name
     * 
     */
    public function companyTypeNameFilter($id)
    {
      $name = '';
      
      if ($id == 'particular') {
        $name = 'Particular';
      }
      elseif ($id == 'national') {
        $name = 'Nacional';
      }
      
      return $name;
    }
    
    /**
     * get Company Type
     * 
     */
    public function companyTypeFilter($id)
    {
      $name = '';
      
      if ($id == 'working') {
        $name = 'Laboral';
      }
      elseif ($id == 'school') {
        $name = 'Colegio';
      }
      elseif ($id == 'university') {
        $name = 'Universidad';
      }
      
      return $name;
    }
    
    /**
     * get Gender Name
     * 
     */
    public function genderNameFilter($id)
    {
      $name = '';
      
      if ($id == 'mixed') {
        $name = 'Mixto';
      }
      elseif ($id == 'male') {
        $name = 'Masculino';
      }
      elseif ($id == 'female') {
        $name = 'Femenino';
      }
      
      return $name;
    }
    
    /**
     * get Education System Name
     * 
     */
    public function educationSystemNameFilter($id)
    {
      $name = '';
      
      if ($id == 'international') {
        $name = 'Internacional';
      }
      elseif ($id == 'national') {
        $name = 'Nacional';
      }
      
      return $name;
    }
    
    /**
     * get Myersbriggs Name
     * 
     */
    public function myersbriggsNameFilter($identified)
    {
        $name = '';
        switch($identified){
            case 'extraversion':
                $name = 'Extroversión';
                break;
            case 'introversion':
                $name = 'Introversión';
                break;
            case 'sensing':
                $name = 'Sensorial';
                break;
            case 'intuition':
                $name = 'Intuitivo';
                break;
            case 'thinking':
                $name = 'Racional';
                break;
            case 'feeling':
                $name = 'Emocional';
                break;
            case 'judging':
                $name = 'Calificador';
                break;
            case 'perception':
                $name = 'Perceptivo';
                break;
        }
        return $name;
    }
    
    /**
     * get Myersbriggs Abbreviation
     * 
     */
    public function myersbriggsAbbreviationFilter($identified)
    {
        $abbreviation = '';
        switch($identified){
            case 'extraversion':
                $abbreviation = 'E';
                break;
            case 'introversion':
                $abbreviation = 'I';
                break;
            case 'sensing':
                $abbreviation = 'S';
                break;
            case 'intuition':
                $abbreviation = 'N';
                break;
            case 'thinking':
                $abbreviation = 'T';
                break;
            case 'feeling':
                $abbreviation = 'F';
                break;
            case 'judging':
                $abbreviation = 'J';
                break;
            case 'perception':
                $abbreviation = 'P';
                break;
        }
        return $abbreviation;
    }
    
    /**
     * get Grade Name
     * 
     */
    public function gradeNameFilter($grade)
    {
        $name = '';
        switch($grade){
            case '1p':
                $name = '1ro de Primaria';
                break;
            case '2p':
                $name = '2do de Primaria';
                break;
            case '3p':
                $name = '3ro de Primaria';
                break;
            case '4p':
                $name = '4to de Primaria';
                break;
            case '5p':
                $name = '5to de Primaria';
                break;
            case '6p':
                $name = '6to de Primaria';
                break;
            case '1s':
                $name = '1ro de Secundaria';
                break;
            case '2s':
                $name = '2do de Secundaria';
                break;
            case '3s':
                $name = '3ro de Secundaria';
                break;
            case '4s':
                $name = '4to de Secundaria';
                break;
            case '5s':
                $name = '5to de Secundaria';
                break;
        }
        return $name;
    }
    
    /**
     * get Via Name
     * 
     */
    public function viaNameFilter($via)
    {
        $name = '';
        switch($via){
            case 'av':
                $name = 'Av.';
                break;
            case 'street':
                $name = 'Calle';
                break;
            case 'jr':
                $name = 'Jr.';
                break;
            case 'batch':
                $name = 'Lote';
                break;
            case 'via':
                $name = 'Vía';
                break;
        }
        return $name;
    }
    
    /**
     * get Filter Company Name
     * 
     */
    public function filterCompanyNameFilter($via)
    {
        $name = '';
        switch($via){
            case 'period':
                $name = 'Periodo';
                break;
            case 'industry':
                $name = 'Industria';
                break;
            case 'nature':
                $name = 'Naturaleza';
                break;
            case 'volume_business':
                $name = 'Volumen de Ventas';
                break;
            case 'number_employees':
                $name = 'Número de Empleados';
                break;
            case 'ranking_top':
                $name = 'Ranking Top';
                break;
            case 'range_pension':
                $name = 'Rango de Pensión';
                break;
            case 'type':
                $name = 'Tipo';
                break;
            case 'gender':
                $name = 'Genero';
                break;
            case 'number_students':
                $name = 'Número de Alumnos';
                break;
            case 'religious_affiliation':
                $name = 'Afiliación Religiosa';
                break;
            case 'institutional_affiliation':
                $name = 'Afiliaciones Intitucionales';
                break;
            case 'education_system':
                $name = 'Sistema Educativo';
                break;
            case 'internal_school':
                $name = 'Bachillerato Interno (IB)';
                break;
            case 'avarage_pension':
                $name = 'Pensión Promedio';
                break;
            case 'type':
                $name = 'Tipo';
                break;
            case 'number_students':
                $name = 'Número de Alumnos';
                break;
            case 'faculty':
                $name = 'Facultad';
                break;
        }
        return $name;
    }
    
    /**
     * get Filter One Per Evaluator
     * 
     */
    public function onePerEvaluatorFilter($value)
    {
        $name = '';
        switch($value){
            case 1:
                $name = 'Sólo una notificación por evaluador';
                break;
        }
        return $name;
    }
    
    /**
     * get Filter Workinginformation Type
     * 
     */
    public function workinginformationTypeFilter($value)
    {
        $name = '';
        switch ($value) {
            case 'workinginformation':
                $name = 'Trabajador';
                break;
            case 'schoolinformation':
                $name = 'Estudiante';
                break;
            case 'universityinformation':
                $name = 'Universitario';
                break;
        }
        return $name;
    }
    
    /**
     * get Evaluation Rol Name
     * 
     */
    public function evaluationRolNameFilter($id)
    {
      $name = '';
      
      if ($id == 'boss') {
        $name = 'Jefe';
      }
      elseif ($id == 'pair') {
        $name = 'Par';
      }
      elseif ($id == 'collaborator') {
        $name = 'Colaborador';
      }
      elseif ($id == 'self') {
        $name = 'Autoevaluado';
      }
      
      return $name;
    }
    
    /**
     * get status bar
     * 
     */
    public function statusBarNameFilter($id)
    {
      $name = '';
      
      if ($id == 'section') {
        $name = 'Por sección';
      }
      elseif ($id == 'question') {
        $name = 'Por pregunta';
      }
      
      return $name;
    }

    /**
     * question Widgets
     *
     */
    public function questionWidgetsFunction($id, $type, $options = null, $alignment = null, $answer = null)
    {
        $output = '';
        
        switch ($type) {
            case 'simple_text':
                $output .=  '<input type="text" name="questions[' . $id . ']" id="question_' . $id .'" value="' . $answer . '" maxLength="250" >';
                $output .= '(250 caracteres)';
                break;
            case 'unique_option':
                $class = '';
    
                if ($alignment == 'vertical') {
                  $class = 'vertical_option';
                }
    
                foreach ($options as $value => $op) {
                    $selected = false;
                    if ($answer != NULL) {
                        if ($value == $answer) {
                            $selected = true;                              
                        }
                    }
                    if ($selected) {
                        $output .= '<div class="clearfix radioContainer ' .$class. '"><input type="radio" name="questions[' . $id . ']" value="' . $value . '" id="question_' . $id .'" checked="checked" >';
                        $output .= '<label>'. $op['text'] .'</label></div>'; 
                    } else {
                        $output .= '<div class="clearfix radioContainer ' .$class. '"><input type="radio" name="questions[' . $id . ']" value="' . $value . '" id="question_' . $id .'" >';
                        $output .= '<label>'. $op['text'] .'</label></div>'; 
                    }
                }
    
                break;
            case 'multiple_option':
                $class = '';
                $i = 0;
                
                if ($alignment == 'vertical') {
                  $class = 'vertical_option';
                }
    
                foreach ($options as $value => $op) {
                    $selected = false;
                    if ($answer != NULL) {
                        foreach ($answer as $value_answer) {
                            if ($value == $value_answer) {
                                $selected = true;                              
                            }
                        }
                    }
                    if ($selected) {
                        $output .= '<div class="clearfix checkboxContainer ' .$class. '"><input type="checkbox" name="questions[' . $id . '][' . $i . ']' . '" value="' . $value . '" id="question_' . $id . '_' . $i . '" class="checker" checked="checked" >';
                        $output .= '<label>'. $op['text'] .'</label></div>'; 
                    } else {
                        $output .= '<div class="clearfix checkboxContainer ' .$class. '"><input type="checkbox" name="questions[' . $id . '][' . $i . ']' . '" value="' . $value . '" id="question_' . $id . '_' . $i . '" class="checker" >';
                        $output .= '<label>'. $op['text'] .'</label></div>'; 
                    }
                    $i++;
                }
                break;
            case 'selection_option':
                $output .= '<select name="questions[' . $id . ']" id="question_' . $id . '">';
                $output .='<option value="">SELECCIONE UNA OPCIÓN</option>';
                foreach ($options as $value => $op) {
                    $selected = false;
                    if ($answer != NULL) {
                        if ($value == $answer) {
                            $selected = true;                              
                        }
                    }
                    if ($selected) {
                        $output .='<option value="' .$value . '" selected="selected">' . $op['text'] . '</option>';
                    } else {
                        $output .='<option value="' .$value . '">' . $op['text'] . '</option>';
                    }
                }
                $output .= '</select>';
                break;
            case 'multiple_text':
                $output .= '<textarea name="questions[' . $id . ']" id="question_' . $id . '">' . $answer . '</textarea>';
                break;
        }
        
        return $output;
        
    }

    /**
     * question Widgets
     *
     */
    public function questionWidgetsAllPollapplicationsFunction($type, $options = null, $alignment = null, $pollapplications)
    {
        $output = '';
        
        if (!empty($pollapplications)) {
            
            if ($options) {
                $output .= '<div class="widget tableDefault">';
                $output .= '<table class="records_list dataTable">';
                $output .= '<thead>';
                $output .= '<tr>';
                $output .= '<th>Evaluados</th>';
                if ($type == 'selection_option') {
                    $output .= '<th></th>';
                }
                else {
                    foreach ($options as $value => $op) {
                        $output .= '<th>' . $op['text'] . '</th>';
                    }
                }
                $output .= '</tr>';
                $output .= '</thead>';
                $output .= '<tbody>';
            }
            
            foreach ($pollapplications as $pa) {
                
                switch ($type) {
                    case 'simple_text':
                        $output .= '<div class="formRowAnswer clearfix">';
                        $output .= '<label>' . $pa['name'] . '</label>';
                        $output .= '<input type="text" name="questions[' . $pa['id'] . ']" id="question_' . $pa['id'] .'" value="' . $pa['answer'] . '" maxLength="250" >';
                        $output .= '(250 caracteres)';
                        $output .= '</div>';
                        break;
                    case 'unique_option':
                        $class = '';
                        
                        $output .= '<tr>';
                        $output .= '<td class="evaluated">' . $pa['name'] . '</td>';
                        foreach ($options as $value => $op) {
                            $selected = false;
                            if ($pa['answer'] != NULL) {
                                if ($value == $pa['answer']) {
                                  $selected = true;                              
                                }
                            }
                            $output .= '<td class="answerCenter">';
                            if ($selected) {
                                $output .= '<input type="radio" name="questions[' . $pa['id'] . ']" value="' . $value . '" id="question_' . $pa['id'] .'" checked="checked" >';
                            } else {
                                $output .= '<input type="radio" name="questions[' . $pa['id'] . ']" value="' . $value . '" id="question_' . $pa['id'] .'" >';
                            }
                            $output .= '</td>';
                        }
                        $output .= '</tr>';
                        break;
                    case 'multiple_option':
                        $class = '';
                        $i = 0;
                        
                        $output .= '<tr>';
                        $output .= '<td class="evaluated">' . $pa['name'] . '</td>';
                        foreach ($options as $value => $op) {
                            $selected = false;
                            if ($pa['answer'] != NULL) {
                                foreach ($pa['answer'] as $value_answer) {
                                    if ($value == $value_answer) {
                                        $selected = true;                              
                                    }
                                }
                            }
                            $output .= '<td>';
                            if ($selected) {
                                $output .= '<div class="clearfix checkboxContainer"><input type="checkbox" name="questions[' . $pa['id'] . '][' . $i . ']' . '" value="' . $value . '" id="question_' . $pa['id'] . '_' . $i . '" class="checker" checked="checked" >';
                            } else {
                                $output .= '<div class="clearfix checkboxContainer"><input type="checkbox" name="questions[' . $pa['id'] . '][' . $i . ']' . '" value="' . $value . '" id="question_' . $pa['id'] . '_' . $i . '" class="checker" >';
                            }
                            $i++;
                            $output .= '</td>';
                        }
                        $output .= '</tr>';
                        break;
                    case 'selection_option':
                        $output .= '<tr>';
                        $output .= '<td class="evaluated">' . $pa['name'] . '</td>';
                        $output .= '<td>';
                        $output .= '<select name="questions[' . $pa['id'] . ']" id="question_' . $pa['id'] . '">';
                        $output .= '<option value="">SELECCIONE UNA OPCIÓN</option>';
                        foreach ($options as $value => $op) {
                            $selected = false;
                            if ($pa['answer'] != NULL) {
                                if ($value == $pa['answer']) {
                                    $selected = true;
                                }
                            }
                            if ($selected) {
                                $output .='<option value="' .$value . '" selected="selected">' . $op['text'] . '</option>';
                            } else {
                                $output .='<option value="' .$value . '">' . $op['text'] . '</option>';
                            }
                        }
                        $output .= '</select>';
                        $output .= '</td>';
                        $output .= '<tr>';
                        break;
                    case 'multiple_text':
                        $output .= '<div class="formRowAnswer clearfix">';
                        $output .= '<label>' . $pa['name'] . '</label>';
                        $output .= '<textarea name="questions[' . $pa['id'] . ']" id="question_' . $pa['id'] . '">' . $pa['answer'] . '</textarea>';
                        $output .= '</div>';
                        break;
                }
                
            }

            if ($options) {
                $output .= '</tbody>';
                $output .= '</table>';
                $output .= '</div>';
            }
            
        }
  
        return $output;
        
    }
    
    /**
     * poll Questions
     *
     */
    public function pollQuestionsFunction($array, $preview = FALSE, $preview_multiple = FALSE)
    {
      $output = '';
      
      if (!empty($array['sections'])) {
          foreach ($array['sections'] as $key_section => $section) {
              if (!empty($section['sections']) || !empty($section['questions'])) {
                  $output .= '<div class="grid12">';
                  $output .= '<div class="check cCheck widget by_user">';
                  
                  if (isset($section['title'])) {
                      if ($section['title'] != '') {
                          $output .= '<div class="whead pollSectionTitle clearfix">';
                          $output .= '<h6>' . $section['title'] . '</h6>';
                          $output .= '</div>';
                      }
                  }
                  
                  if (isset($section['description'])) {
                      if ($section['description'] != '') {
                          $output .= '<div class="formRow pollSectionDescription clearfix">';
                          $output .= $section['description'];
                          $output .= '</div>';
                      }
                  }
                  
                  if (!empty($section['questions'])) {
                      foreach ($section['questions'] as $key_question => $question) {
                            
                          $output .= '<div class="formRow pollQuestion clearfix">';
                          
                          if (!$question['mandatory_response']) {
                              $output .= '<h6>' . $question['question'] . '</h6>';
                          }
                          else {
                              $output .= '<h6 class="mandatory">' . $question['question'] . ' <span title="Respuesta obligatoria">(*)</span></h6>';
                          }
                          
                          if ($preview) {
                              if ($preview_multiple) {
                                  $output .= $this->questionWidgetsAllPollapplicationsFunction($question['type'], $question['options'], $question['alignment'], $preview_multiple);
                              }
                              else {
                                  $output .= $this->questionWidgetsFunction($question['id'], $question['type'], $question['options'], $question['alignment'], '');
                              }
                          }
                          else {
                              if (isset($question['pollapplications'])) {
                                  $output .= $this->questionWidgetsAllPollapplicationsFunction($question['type'], $question['options'], $question['alignment'], $question['pollapplications']);
                              }
                              else {
                                  $output .= $this->questionWidgetsFunction($question['id'], $question['type'], $question['options'], $question['alignment'], $question['answer']);
                              }
                          }
                          
                          $output .= '</div>';
                          
                      }
                  }
                  
                  if (!empty($section['sections'])) {
                      $output .= '<div class="formRow pollQuestion clearfix">';
                      $output .= $this->pollQuestionsFunction($section, $preview, $preview_multiple);
                      $output .= '</div>';
                  }
                  
                  $output .= '</div>';
                  $output .= '</div>';
              }
          }
      }
      
      return $output;
    }
    
    /**
     * poll Questions
     *
     */
    public function pollQuestionsHorizontalFunction($array, $preview = FALSE)
    {
      $output = '';
      
      if (!empty($array['sections'])) {
          foreach ($array['sections'] as $key_section => $section) {
              if (!empty($section['sections']) || !empty($section['questions'])) {
                  $output .= '<div class="grid12">';
                  $output .= '<div class="check cCheck widget by_user">';
                  
                  if (isset($section['title'])) {
                      if ($section['title'] != '') {
                          $output .= '<div class="whead pollSectionTitle clearfix">';
                          $output .= '<h6>' . $section['title'] . '</h6>';
                          $output .= '</div>';
                      }
                  }
                  
                  if (isset($section['description'])) {
                      if ($section['description'] != '') {
                          $output .= '<div class="formRow pollSectionDescription clearfix">';
                          $output .= $section['description'];
                          $output .= '</div>';
                      }
                  }
                  
                  if (!empty($section['questions'])) {
                      $i = 1;
                      $j = 1;
                      $output .= '<div class="pollQuestionsHorizontal clearfix">';
                      foreach ($section['questions'] as $key_question => $question) {
                          
                          if ($i == 3) {
                              $i = 0;
                          }
                          
                          $classQuestion = '';
                          if ($i == 1) {
                              $classQuestion .= 'formRowHorizontalOne';
                          }
                          if ($j > 3) {
                              $classQuestion .= ' formRowVertical';
                          }
                          
                          $output .= '<div class="formRow pollQuestion grid4 ' . $classQuestion . ' clearfix">';
                          
                          if (!$question['mandatory_response']) {
                              $output .= '<h6>' . $question['question'] . '</h6>';
                          }
                          else {
                              $output .= '<h6 class="mandatory">' . $question['question'] . ' <span title="Respuesta obligatoria">(*)</span></h6>';
                          }
                          
                          if ($preview) {
                              $output .= $this->questionWidgetsFunction($question['id'], $question['type'], $question['options'], $question['alignment'], '');
                          }
                          else {
                              $output .= $this->questionWidgetsFunction($question['id'], $question['type'], $question['options'], $question['alignment'], $question['answer']);
                          }
                          
                          $output .= '</div>';
                          
                          $i++;
                          $j++;
                          
                      }
                      $output .= '</div>';
                  }
                  
                  if (!empty($section['sections'])) {
                      $output .= '<div class="formRow pollQuestion clearfix">';
                      $output .= $this->pollQuestionsHorizontalFunction($section, $preview);
                      $output .= '</div>';
                  }
                  
                  $output .= '</div>';
                  $output .= '</div>';
                  
              }
          }
      }
      
      return $output;
    }

}
