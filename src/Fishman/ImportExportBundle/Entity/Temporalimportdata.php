<?php

namespace Fishman\ImportExportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fishman\ImportExportBundle\Entity\Temporalimportdata
 */
class Temporalimportdata
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $data
     */
    private $data;

    /**
     * @var string $entity
     */
    private $entity;

    /**
     * @var \DateTime $datetime
     */
    private $datetime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return Temporalimportdata
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set entity
     *
     * @param string $entity
     * @return Temporalimportdata
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    
        return $this;
    }

    /**
     * Get entity
     *
     * @return string 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Temporalimportdata
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
}