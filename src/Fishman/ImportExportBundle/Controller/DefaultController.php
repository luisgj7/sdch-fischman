<?php

//TODO: Garbage collector rutine to erase old temporal data

namespace Fishman\ImportExportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('FishmanImportExportBundle:Default:index.html.twig', array('name' => $name));
    }
}
