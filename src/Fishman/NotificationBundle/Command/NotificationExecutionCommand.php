<?php

namespace Fishman\NotificationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Fishman\NotificationBundle\Entity\Notificationsend;
use Fishman\NotificationBundle\Entity\Notificationexecution;

/**
 * Actualizar los notification execution:
 * - Al crear actividad o de la encuesta
 * - Al cambiar de estado de la actividad o de la encuesta
 *   - En caso de actividades con predecesora ¿es cuando la persona inicia?
 *     ¿o es cuando la fecha se cumple?
 * 
 */

/**
 * Este comando envia los correos y prepara los próximos envíos en caso de repetición.
 * En caso de error igual coloca la notificación en Notificationsend, indicando que hubo
 *    un problema para enviarlo por correo.
 */
class NotificationExecutionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('notification:today:execution')
             ->setDescription('Executes a notification');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Doctrine registry
        $doctrine = $this->getContainer()->get('doctrine');
        $mailer = $this->getContainer()->get('mailer');
        $logger = $this->getContainer()->get('logger');
        $router = $this->getContainer()->get('router');
        $templating = $this->getContainer()->get('templating');

        Notificationexecution::sendTodayNotifications($doctrine, $mailer, $logger, $router, $templating);
        $output->writeln('Se enviaron las notificaciones');
    }
}
