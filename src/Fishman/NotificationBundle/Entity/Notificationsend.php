<?php

namespace Fishman\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\NotificationBundle\Entity\Notificationsend
 */
class Notificationsend
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $entity_name
     */
    private $entity_name;

    /**
     * @var \DateTime $sended
     */
    private $sended;

    /**
     * @var string $subject
     */
    private $subject;

    /**
     * @var string $message
     */
    private $message;

    /**
     * @var boolean $sent
     */
    private $sent;

    /**
     * @var integer $workinginformation_id
     */
    private $workinginformation_id;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\NotificationBundle\Entity\Notificationexecution", inversedBy="notificationsends")
     * @ORM\JoinColumn(name="notificationexecution_id", referencedColumnName="id")
     */
    protected $notificationexecution;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set sended
     *
     * @param \DateTime $sended
     * @return Notificationsend
     */
    public function setSended($sended)
    {
        $this->sended = $sended;
    
        return $this;
    }

    /**
     * Get sended
     *
     * @return \DateTime 
     */
    public function getSended()
    {
        return $this->sended;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Notificationsend
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Notificationsend
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set sent
     *
     * @param boolean $sent
     * @return Notificationsend
     */
    public function setSent($sent)
    {
        $this->sent = $sent;
    
        return $this;
    }

    /**
     * Get sent
     *
     * @return boolean 
     */
    public function getSent()
    {
        return $this->sent;
    }


    /**
     * Set notificationexecution
     *
     * @param Fishman\NotificationBundle\Entity\Notificationexecution $notificationexecution
     * @return Notificationsend
     */
    public function setNotificationexecution(\Fishman\NotificationBundle\Entity\Notificationexecution $notificationexecution = null)
    {
        $this->notificationexecution = $notificationexecution;
    
        return $this;
    }

    /**
     * Get notificationexecution
     *
     * @return Fishman\NotificationBundle\Entity\Notificationexecution 
     */
    public function getNotificationexecution()
    {
        return $this->notificationexecution;
    }

    /**
     * Set entity_name
     *
     * @param string $entityName
     * @return Notificationsend
     */
    public function setEntityName($entityName)
    {
        $this->entity_name = $entityName;
    
        return $this;
    }

    /**
     * Get entity_name
     *
     * @return string 
     */
    public function getEntityName()
    {
        return $this->entity_name;
    }

    /**
     * Set workinginformation_id
     *
     * @param integer $workinginformationId
     * @return Notificationsend
     */
    public function setWorkinginformationId($workinginformationId)
    {
        $this->workinginformation_id = $workinginformationId;
    
        return $this;
    }

    /**
     * Get workinginformation_id
     *
     * @return integer 
     */
    public function getWorkinginformationId()
    {
        return $this->workinginformation_id;
    }

    /**
     * Get Show Notificationsend
     */
    public static function getShowNotificationsend(DoctrineRegistry $doctrineRegistry, $id)
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $result = $repository->createQueryBuilder('ns')
            ->select('ns.id, nsc.entity_name name, nsc.notification_type type, nsc.notification_type_name type_name, ns.sended, ns.subject, ns.message')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->where('ns.id = :notificationsend 
                    AND ns.sent = 1')
            ->setParameter('notificationsend', $id)
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();
            
        $entity = current($result);

        return $entity;
    }

    /**
     * Get Show Notificationsend to Workshop
     */
    public static function getPollShowNotificationsend(DoctrineRegistry $doctrineRegistry, $psId, $id)
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $result = $repository->createQueryBuilder('ns')
            ->select('ns.id, nsc.entity_name name, nsc.notification_type type, nsc.notification_type_name type_name, ns.sended, ns.subject, ns.message')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->where('nsc.entity_id = :pollscheduling 
                    AND nsc.entity_type = :type 
                    AND ns.id = :notificationsend 
                    AND ns.sent = 1')
            ->setParameter('pollscheduling', $psId)
            ->setParameter('type', 'pollscheduling')
            ->setParameter('notificationsend', $id)
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();
            
        $entity = current($result);

        return $entity;
    }

    /**
     * Get Show Notificationsend to Workshop
     */
    public static function getWorkshopShowNotificationsend(DoctrineRegistry $doctrineRegistry, $wsId, $id)
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $result = $repository->createQueryBuilder('ns')
            ->select('ns.id, nsc.entity_name name, nsc.notification_type type, nsc.notification_type_name type_name, ns.sended, ns.subject, ns.message')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->where('nsc.entity_id = :workshopscheduling 
                    AND nsc.entity_type = :type 
                    AND ns.id = :notificationsend 
                    AND ns.sent = 1')
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('type', 'workshopscheduling')
            ->setParameter('notificationsend', $id)
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();
            
        $entity = current($result);

        return $entity;
    }

    /**
     * Get Last Two Notificationsends
     */
    public static function getLastTwoNotificationsends(DoctrineRegistry $doctrineRegistry, $wiId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $query = $repository->createQueryBuilder('ns')
            ->select('ns.id, nsc.entity_name name, nsc.notification_type type, nsc.notification_type_name type_name, ns.sended, ns.subject, ns.message')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->where('ns.workinginformation_id = :workinginformation 
                    AND ns.sent = 1')
            ->setParameter('workinginformation', $wiId)
            ->orderBy('ns.sended', 'DESC')
            ->setMaxResults(2)
            ->getQuery();
        $output = $query->getResult();

        return $output;
    }

    /**
     * Get Last Two Notificationsends to Poll
     */
    public static function getPollLastTwoNotificationsends(DoctrineRegistry $doctrineRegistry, $wiId, $psId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $query = $repository->createQueryBuilder('ns')
            ->select('ns.id, nsc.entity_name name, nsc.notification_type type, nsc.notification_type_name type_name, ns.sended, ns.subject, ns.message')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->where('ns.workinginformation_id = :workinginformation 
                    AND ns.sent = 1 
                    AND nsc.entity_id = :pollscheduling 
                    AND nsc.entity_type = :type')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('pollscheduling', $psId)
            ->setParameter('type', 'pollscheduling')
            ->orderBy('ns.sended', 'DESC')
            ->setMaxResults(2)
            ->getQuery();
        $output = $query->getResult();

        return $output;
    }

    /**
     * Get Last Two Notificationsends to Workshop
     */
    public static function getWorkshopLastTwoNotificationsends(DoctrineRegistry $doctrineRegistry, $wiId, $wsId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $query = $repository->createQueryBuilder('ns')
            ->select('ns.id, nsc.entity_name name, nsc.notification_type type, nsc.notification_type_name type_name, ns.sended, ns.subject, ns.message')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->where('ns.workinginformation_id = :workinginformation 
                    AND ns.sent = 1 
                    AND nsc.entity_id = :workshopscheduling 
                    AND nsc.entity_type = :type')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('type', 'workshopscheduling')
            ->orderBy('ns.sended', 'DESC')
            ->setMaxResults(2)
            ->getQuery();
        $output = $query->getResult();

        return $output;
    }

    /**
     * Get List Notificationssend
     */
    public static function getListNotificationsends(DoctrineRegistry $doctrineRegistry, $wiId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $queryBuilder = $repository->createQueryBuilder('ns')
            ->select('nsc.sequence, ns.id, ns.subject, ns.message, nsc.notification_type type, 
                      nsc.notification_type_name type_name, nsc.entity_name, c.name company, ns.sended')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'ns.workinginformation_id = wi.id')
            ->innerJoin('wi.company', 'c')
            ->where('ns.sent = 1 
                    AND wi.id = :workinginformation')
            ->andWhere('ns.subject LIKE :subject 
                    OR ns.message LIKE :message')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('subject', '%' . $data['word'] . '%')
            ->setParameter('message', '%' . $data['word'] . '%')
            ->orderBy('ns.sended', 'DESC');
        
        // Add arguments
        
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('nsc.notification_type = :type')
                ->setParameter('type', $data['type']);
        }
        if ($data['entity'] != '') {
            $queryBuilder
                ->andWhere('nsc.entity_name LIKE :entity')
                ->setParameter('entity', '%' . $data['entity'] . '%');
        }
        if ($data['company'] != '') {
            $queryBuilder
                ->andWhere('c.name LIKE :company')
                ->setParameter('company', '%' . $data['company'] . '%');
        }
        if (!empty($data['sended'])) {
            $queryBuilder
                ->andWhere('ns.sended = :sended')
                ->setParameter('sended', $data['sended']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('nsc.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        
        $query = $queryBuilder->getQuery();
     
        return $query;
    }

    /**
     * Get List Notificationssend to Poll
     */
    public static function getPollListNotificationsends(DoctrineRegistry $doctrineRegistry, $wiId, $psId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $queryBuilder = $repository->createQueryBuilder('ns')
            ->select('nsc.sequence, ns.id, ns.subject, ns.message, nsc.notification_type type, ns.sended')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->where('ns.workinginformation_id = :workinginformation 
                    AND ns.sent = 1 
                    AND nsc.entity_id = :pollscheduling 
                    AND nsc.entity_type = :type')
            ->andWhere('ns.subject LIKE :subject 
                    OR ns.message LIKE :message')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('pollscheduling', $psId)
            ->setParameter('type', 'pollscheduling')
            ->setParameter('subject', '%' . $data['word'] . '%')
            ->setParameter('message', '%' . $data['word'] . '%')
            ->orderBy('ns.sended', 'DESC');
        
        // Add arguments
        
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('nsc.notification_type = :type')
                ->setParameter('type', $data['type']);
        }
        if (!empty($data['sended'])) {
            $queryBuilder
                ->andWhere('ns.sended = :sended')
                ->setParameter('sended', $data['sended']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('nsc.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }

    /**
     * Get List Notificationssend to Workshop
     */
    public static function getWorkshopListNotificationsends(DoctrineRegistry $doctrineRegistry, $wiId, $wsId, $data = '')
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationsend');
        $queryBuilder = $repository->createQueryBuilder('ns')
            ->select('nsc.sequence, ns.id, ns.subject, ns.message, nsc.notification_type type, ns.sended')
            ->innerJoin('ns.notificationexecution', 'ne')
            ->innerJoin('ne.notificationscheduling', 'nsc')
            ->where('ns.workinginformation_id = :workinginformation 
                    AND ns.sent = 1 
                    AND nsc.entity_id = :workshopscheduling 
                    AND nsc.entity_type = :type')
            ->andWhere('ns.subject LIKE :subject 
                    OR ns.message LIKE :message')
            ->setParameter('workinginformation', $wiId)
            ->setParameter('workshopscheduling', $wsId)
            ->setParameter('type', 'workshopscheduling')
            ->setParameter('subject', '%' . $data['word'] . '%')
            ->setParameter('message', '%' . $data['word'] . '%')
            ->orderBy('ns.sended', 'DESC');
        
        // Add arguments
        
        if ($data['type'] != '') {
            $queryBuilder
                ->andWhere('nsc.notification_type = :type')
                ->setParameter('type', $data['type']);
        }
        if ($data['sequence'] != '') {
            $queryBuilder
                ->andWhere('nsc.sequence = :sequence')
                ->setParameter('sequence', $data['sequence']);
        }
        
        $query = $queryBuilder->getQuery();
        
        return $query;
    }
}