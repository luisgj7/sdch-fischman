<?php

namespace Fishman\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\NotificationBundle\Entity\Notificationmanualsend
 */
class Notificationmanualsend
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $workinginformation
     */
    private $workinginformation;

    /**
     * @var \DateTime $date
     */
    private $date;

    /**
     * @var string $status
     */
    private $status;


    /**
     * @ORM\ManyToOne(targetEntity="Fishman\NotificationBundle\Entity\Notificationscheduling", inversedBy="notificationexecutions")
     * @ORM\JoinColumn(name="notificationscheduling_id", referencedColumnName="id")
     */
    protected $notificationscheduling;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\NotificationBundle\Entity\Notificationmanualdetails", mappedBy="notificationmanualsend")
     */
    protected $notificationmanualdetailss;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notificationmanualdetailss = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Notificationmanualdetails
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set workinginformation
     *
     * @param integer $workinginformation
     * @return Notificationmanualsend
     */
    public function setWorkinginformation($workinginformation)
    {
        $this->workinginformation = $workinginformation;
    
        return $this;
    }

    /**
     * Get workinginformation
     *
     * @return integer 
     */
    public function getWorkinginformation()
    {
        return $this->workinginformation;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Notificationmanualsend
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set notificationscheduling
     *
     * @param Fishman\NotificationBundle\Entity\Notificationscheduling $notificationscheduling
     * @return Notificationexecution
     */
    public function setNotificationscheduling(\Fishman\NotificationBundle\Entity\Notificationscheduling $notificationscheduling = null)
    {
        $this->notificationscheduling = $notificationscheduling;
    
        return $this;
    }

    /**
     * Get notificationscheduling
     *
     * @return Fishman\NotificationBundle\Entity\Notificationscheduling 
     */
    public function getNotificationscheduling()
    {
        return $this->notificationscheduling;
    }

    /**
     * Add notificationmanualdetailss
     *
     * @param Fishman\NotificationBundle\Entity\Notificationsend $notificationmanualdetailss
     * @return Notificationmanualsend 
     */
    public function addNotificationmanualdetails(\Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationmanualdetailss)
    {
        $this->notificationmanualdetailss[] = $notificationmanualdetailss;
    
        return $this;
    }

    /**
     * Remove notificationmanualdetailss
     *
     * @param Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationmanualdetailss
     */
    public function removeNotificationmanualdetails(\Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationmanualdetailss)
    {
        $this->notificationmanualdetailss->removeElement($notificationmanualdetailss);
    }

    /**
     * Set notificationmanualdetailss
     *
     * @param Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationdetails
     * @return Notificationmanualdetails
     */
    public function setNotificationmanualdetails(\Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationmanualdetailss = null)
    {
        $this->notificationmanualdetailss = $notificationmanualdetailss;
    
        return $this;
    }

    /**
     * Get notificationmanualdetailss
     *
     * @return Fishman\NotificationBundle\Entity\Notificationmanualdetails 
     */
    public function getNotificationmanualdetails()
    {
        return $this->notificationmanualdetailss;
    }
    
}