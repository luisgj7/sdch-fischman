<?php

namespace Fishman\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;
use Fishman\NotificationBundle\Entity\Notificationsend;

/**
 * Fishman\NotificationBundle\Entity\Notificationexecution
 */
class Notificationexecution
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var \DateTime $initdate
     */
    private $initdate;

    /**
     * @var \DateTime $enddate
     */
    private $enddate;

    /**
     * @var string $entity_type
     */
    private $entity_type;

    /**
     * @var integer $entity_id
     */
    private $entity_id;

    /**
     * @var \DateTime $next_notification
     */
    private $next_notification;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var string $profile
     */
    private $profile;

    /**
     * @var integer $workinginformation_id
     */
    private $workinginformation_id;

    /**
     * @var integer $workinginformation
     */
    private $workinginformation;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\NotificationBundle\Entity\Notificationscheduling", inversedBy="notificationexecutions")
     * @ORM\JoinColumn(name="notificationscheduling_id", referencedColumnName="id")
     */
    protected $notificationscheduling;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\NotificationBundle\Entity\Notificationsend", mappedBy="notificationexecution")
     */
    protected $notificationsends;    

    /**
     * @ORM\OneToMany(targetEntity="Fishman\NotificationBundle\Entity\Notificationmanualdetails", mappedBy="notificationexecution")
     */
    protected $notificationmanualdetailss;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notificationsends = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set initdate
     *
     * @param \DateTime $initdate
     * @return Notificationexecution
     */
    public function setInitdate($initdate)
    {
        $this->initdate = $initdate;
    
        return $this;
    }

    /**
     * Get initdate
     *
     * @return \DateTime 
     */
    public function getInitdate()
    {
        return $this->initdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return Notificationexecution
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set entity_id
     *
     * @param integer $entityId
     * @return Notificationexecution
     */
    public function setEntityId($entityId)
    {
        $this->entity_id = $entityId;
    
        return $this;
    }

    /**
     * Get entity_id
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entity_id;
    }

    /**
     * Set entity_type
     *
     * @param string $entityType
     * @return Notificationexecution
     */
    public function setEntityType($entityType)
    {
        $this->entity_type = $entityType;
    
        return $this;
    }

    /**
     * Get entity_type
     *
     * @return string 
     */
    public function getEntityType()
    {
        return $this->entity_type;
    }

    /**
     * Set next_notification
     *
     * @param \DateTime $nextNotification
     * @return Notificationexecution
     */
    public function setNextNotification($nextNotification)
    {
        $this->next_notification = $nextNotification;
    
        return $this;
    }

    /**
     * Get next_notification
     *
     * @return \DateTime 
     */
    public function getNextNotification()
    {
        return $this->next_notification;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Notificationexecution
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set profile
     *
     * @param string $profile
     * @return Pollschedulingpeople
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    
        return $this;
    }

    /**
     * Get profile
     *
     * @return string 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set notificationscheduling
     *
     * @param Fishman\NotificationBundle\Entity\Notificationscheduling $notificationscheduling
     * @return Notificationexecution
     */
    public function setNotificationscheduling(\Fishman\NotificationBundle\Entity\Notificationscheduling $notificationscheduling = null)
    {
        $this->notificationscheduling = $notificationscheduling;
    
        return $this;
    }

    /**
     * Get notificationscheduling
     *
     * @return Fishman\NotificationBundle\Entity\Notificationscheduling 
     */
    public function getNotificationscheduling()
    {
        return $this->notificationscheduling;
    }
    
    /**
     * Add notificationsends
     *
     * @param Fishman\NotificationBundle\Entity\Notificationsend $notificationsends
     * @return Notificationexecution
     */
    public function addNotificationsend(\Fishman\NotificationBundle\Entity\Notificationsend $notificationsends)
    {
        $this->notificationsends[] = $notificationsends;
    
        return $this;
    }

    /**
     * Remove notificationsends
     *
     * @param Fishman\NotificationBundle\Entity\Notificationsend $notificationsends
     */
    public function removeNotificationsend(\Fishman\NotificationBundle\Entity\Notificationsend $notificationsends)
    {
        $this->notificationsends->removeElement($notificationsends);
    }

    /**
     * Get notificationsends
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getNotificationsends()
    {
        return $this->notificationsends;
    }

    /**
     * Set workinginformation_id
     *
     * @param integer $workinginformationId
     * @return Notificationexecution
     */
    public function setWorkinginformationId($workinginformationId)
    {
        $this->workinginformation_id = $workinginformationId;
    
        return $this;
    }

    /**
     * Get workinginformation_id
     *
     * @return integer 
     */
    public function getWorkinginformationId()
    {
        return $this->workinginformation_id;
    }
    
    /**
     * Envia la notificación para esta notification execution.
     * Previamente reemplaza los tokens
     */
    public function sendNotification(DoctrineRegistry $doctrine, $mailer, $logger, $router, $templating, $type)
    {
            
        if (!$this->getStatus()) {
            return 0;
        }
        
        try{
            
            $ns = $this->getNotificationscheduling();
            $head = $this->getNotificationscheduling()->getHead();
            $message = $this->reeplaceTokens($ns->getMessage(), $doctrine, $router);
            $wi = $this->getWI($doctrine);
            $subject = $this->reeplaceTokens($ns->getSubject(), $doctrine, $router);
            
            $messageMail = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom(array('fischman-notificacion@effectusfischman.com' => 'Fischman - Notificación'))
                ->setTo($wi->getUser()->getEmail())
                ->setBody(
                    $templating->render('FishmanNotificationBundle:Notificationexecution:default.html.twig', array(
                        'head' => $head, 
                        'message' => $message 
                    )), 
                    'text/html'
                );
				
			//Evitar que el correo se vaya al SPAM [by MC]
            $messageMail->getHeaders()->addTextHeader('From', 'Effectus - Notificación <fischman-notificacion@effectusfischman.com>');
            $messageMail->getHeaders()->addTextHeader('Return-Path', 'fischman-notificacion@effectusfischman.com');
            $messageMail->getHeaders()->addTextHeader('Reply-To', 'fischman-notificacion@effectusfischman.com');
            $messageMail->getHeaders()->addTextHeader('Organization', 'F&A');
            $messageMail->getHeaders()->addTextHeader('MIME-Version', '1.0');
			$messageMail->getHeaders()->addTextHeader('Content-type', 'text/html; charset=utf-8');
            //$messageMail->getHeaders()->addTextHeader('Content-type', 'text/html; charset=iso-8859-1');
            $messageMail->getHeaders()->addTextHeader('Content-Transfer-Encoding', '');
            $messageMail->getHeaders()->addTextHeader('X-Priority', '2');
            $messageMail->getHeaders()->addTextHeader('X-Sender', 'fischman-notificacion@effectusfischman.com');
            $messageMail->getHeaders()->addTextHeader('X-Mailer', 'PHP '. phpversion());
			
            
            //Enviar correo información laboral
            if ($wi->getEmail()) {
                $messageMail->setTo($wi->getEmail());
                $mailer->send($messageMail);
            }
            
            if ($wi->getEmail() != $wi->getUser()->getEmail()) {
                //Envía correo personal
                $messageMail->setTo($wi->getUser()->getEmail());
                $mailer->send($messageMail);
            }
            
            //El número de repeticiones se obtiene de las veces que se ha enviado la notificación
            $notificationSend = new Notificationsend();
            $notificationSend->setEntityName($ns->getEntityname());
            $notificationSend->setSended(new \DateTime());
            $notificationSend->setSubject($subject);
            $notificationSend->setMessage($message);
            $notificationSend->setWorkinginformationId($this->getWorkinginformationId());
            $notificationSend->setNotificationExecution($this);
            $notificationSend->setSent(TRUE);
            
            $doctrine->getManager()->persist($notificationSend);
            $doctrine->getManager()->flush();
            
            if ($type == 'automatic') {
                $this->setNewNotificationDate($doctrine);
            }
            else {
                $this->setStatus(FALSE);
                $doctrine->getManager()->persist($this);
                $doctrine->getManager()->flush();
                
                $eaId = $notificationSend->getNotificationexecution()->getEntityId();
                $nsType = $notificationSend->getNotificationexecution()->getNotificationscheduling()->getNotificationType();
                $nsStatus = $notificationSend->getNotificationexecution()->getNotificationscheduling()->getNotificationTypeStatus();
                if ($notificationSend->getNotificationexecution()->getNextNotification()) {
                    $nsInitdate = strtotime($notificationSend->getNotificationexecution()->getNextNotification()->format('Y/m/d'));
                }
                else {
                    $nsInitdate = '';
                }
                $nsPredecessor = $notificationSend->getNotificationexecution()->getNotificationscheduling()->getId();
                
                self::activeNotificationexecutions($doctrine, $eaId, $nsType, $nsStatus, $nsInitdate, $nsPredecessor);
            }
            
            return TRUE;
        }
        catch(Exception $e){
            //No envio el correo
            $logger->debug('No se pudo enviar la notificación #' . $r['id'] .
                           '. El mensaje de error es ' . $e->getMessage());
            $notificationSend->setSent(0);
            
            $doctrine->getManager()->persist($notificationSend);
            $doctrine->getManager()->flush();
            return $e;
        }
    }

    /**
     * Recibiendo un notificationscheduling, reemplaza el texto para cada notificacion
     * execution. Como nombre
     * La forma general de los tokens es [entity][property]
     */
    public function reeplaceTokens($text, $doctrine, $router)
    {
        //User tokens
        $wi = $this->getWI($doctrine);

        //[user][sexidentified][oa] reemplaza el token por "o" si la persona es
        //de sexo masculino o por "a" si la persona es de sexo femenino
        $sexidentifiedoa = ($wi->getUser()->getSex() == 'm' ? 'o' : 'a');
        $resetpasswd = $router->generate('fos_user_resetting_request', array(), true);
        
        $tokens = array('[user][names]' => $wi->getUser()->getNames(),
                        '[user][primarylastname]' => $wi->getUser()->getSurname(),
                        '[user][secondarylastname]' => $wi->getUser()->getLastname(),
                        '[user][username]' => $wi->getUser()->getUsername(),
                        '[user][resetpasswordlink]' => $resetpasswd, //resetting/request
                        '[user][sexidentified][oa]' => $sexidentifiedoa,
                       );
		
        if ($this->getEntityType() == 'pollapplication') {
            
            $pollinternallink = '';
            $pollexternallink = '';
            
            //Recuperar el pollapplication que le corresponde
            $pollapplication = $doctrine->getRepository('FishmanPollBundle:Pollapplication')
                                 ->find($this->getEntityId());

            //Se verifica si se trata de una notificacion de encuesta que se envia una sola vez
            //por evaluador para crear el token [pollapplication][oneperevaluator][external][link] 
            if($this->getNotificationscheduling()->getOnePerEvaluator()){
                $personaltoken = '';
                if($pollapplication->getPollscheduling()->getEntityType() == 'workshopscheduling'){
                    $personaltoken = $doctrine->getRepository('FishmanWorkshopBundle:Workshopapplication')
                                         ->createQueryBuilder('wsa')
                                         ->select('wsa.personaltoken as personaltoken')
                                         ->where('wsa.id = :wsaid')
                                         ->setParameter('wsaid', $pollapplication->getEntityApplicationId())
                                         ->getQuery()
                                         ->getSingleResult();
                }
                else{
                    $personaltoken = $doctrine->getRepository('FishmanPollBundle:Pollschedulingpeople')
                                         ->createQueryBuilder('psp')
                                         ->select('psp.personaltoken as personaltoken')
                                         ->where('psp.id = :pspid')
                                         ->setParameter('pspid', $pollapplication->getEntityApplicationId())
                                         ->getQuery()
                                         ->getSingleResult();
                }

                $onePerEvaluatorExternalLink = $router->generate('fishman_front_end_poll_evaluateds',
                                            array('pollid' => $pollapplication->getPollscheduling()->getId(),
                                                  'personaltoken' => $personaltoken['personaltoken']), true);

                $tokens['[pollapplication][oneperevaluator][external][link]'] = $onePerEvaluatorExternalLink;
            }

            if ($pollapplication->getPollscheduling()->getMultipleEvaluation()) {
              
                // Recuperar el entityApplication que le corresponde
                if ($pollapplication->getEntityApplicationType() == 'workshopapplication') {
                    
                    $entityApplication = $doctrine->getRepository('FishmanWorkshopBundle:Workshopapplication')
                                     ->find($pollapplication->getEntityApplicationId());
                    
                }
                else {
                    
                    $entityApplication = $doctrine->getRepository('FishmanPollBundle:Pollschedulingpeople')
                                     ->find($pollapplication->getEntityApplicationId());
                    
                }
                
                $pollinternallink = $router->generate('fishman_front_end_poll_evaluateds_multipleevaluation_edit',
                    array('pollid' => $pollapplication->getPollscheduling()->getId()), true);
    
                $pollexternallink = $router->generate('fishman_front_end_poll_evaluateds_multipleevaluation_edit',
                    array('pollid' => $pollapplication->getPollscheduling()->getId(),
                          'personaltoken' => $entityApplication->getPersonaltoken()), true);
            
                $evaluatedWI = $doctrine->getRepository('FishmanAuthBundle:Workinginformation')
                                     ->find($pollapplication->getEvaluatedId());
                $evaluatedUser = $evaluatedWI->getUser();
                $evaluatedFullname = $evaluatedUser->getNames() . ' ' .
                                     $evaluatedUser->getSurname() . ' ' .
                                     $evaluatedUser->getLastname(); 
                    
                $tokens['[pollapplication][evaluated][fullname]'] = $evaluatedFullname;
                
            }
            else {
                
                $pollinternallink = $router->generate('fishman_front_end_poll_evaluateds_edit',
                    array('pollid' => $pollapplication->getPollscheduling()->getId(),
                          'pollapplicationid' => $pollapplication->getId()), true);
    
                $pollexternallink = $router->generate('fishman_front_end_poll_evaluateds_edit',
                    array('pollid' => $pollapplication->getPollscheduling()->getId(),
                          'pollapplicationid' => $pollapplication->getId(),
                          'token' => $pollapplication->getToken()), true);
            
                if ($pollapplication->getPollscheduling()->getType() != 'not_evaluateds') {
                    $evaluatedWI = $doctrine->getRepository('FishmanAuthBundle:Workinginformation')
                                         ->find($pollapplication->getEvaluatedId());
                    $evaluatedUser = $evaluatedWI->getUser();
                    $evaluatedFullname = $evaluatedUser->getNames() . ' ' .
                                         $evaluatedUser->getSurname() . ' ' .
                                         $evaluatedUser->getLastname(); 
                    
                    $tokens['[pollapplication][evaluated][fullname]'] = $evaluatedFullname;
                }
                
            }
            
            $tokens['[pollapplication][internal][link]'] = $pollinternallink;
            $tokens['[pollapplication][external][link]'] = $pollexternallink;
            
            $tokens['[talent][external][link]'] = 'http://talento.davidfischman.com/usuario?id=' . 
                                              $pollapplication->getId() . '&token=' . $pollapplication->getToken();
        
            $tokens['[pollapplication][report][link]'] = 'http://www.talentosdavidfischman.com/ResultadosUPC/Interes.aspx?id=' . 
                                              $pollapplication->getId() . '&token=' . $pollapplication->getToken();
    

            $tokens['[pollapplication][report][temperament][link]'] = 'http://www.talentosdavidfischman.com/ResultadosUPC/Temperamentos.aspx?id=' . 
                                              $pollapplication->getId() . '&token=' . $pollapplication->getToken();
        }

        $TokenKeys = array_keys($tokens);
        $TokenValues = array_values($tokens);
        $text = str_replace($TokenKeys, $TokenValues, $text);

        return $text;
    }

    private function getWI($doctrine)
    {
        if(!isset($this->wi)){
            $this->wi = $doctrine->getRepository('FishmanAuthBundle:Workinginformation')
                                 ->find($this->getWorkinginformationId());
        }
        return $this->wi;
    }

    /**
     * Para una notificación automática que se repite, hay que calcular la siguiente fecha
     * Si ya no se va a repetir se le cambia de status
     * @return la nueva fecha o false en caso no exista nueva fecha.
     */
    private function setNewNotificationDate($doctrine)
    {
        $sendNotification = FALSE;
        $notificationscheduling = $this->getNotificationscheduling();
        $em = $doctrine->getManager();
        
        //Verificar si es que se repite, sino se cambia de status
        if($notificationscheduling->getReplay() == 1){
            //Ver cuando finaliza (el caso 'never' solo continua con la acción)
            switch($notificationscheduling->getFinish()){
                case 'after': //después de n repeticiones
                    
                    //Verificar cuantas repeticiones van
                    $NSRepository = $doctrine->getRepository('FishmanNotificationBundle:Notificationsend');
                    $NSResult = $NSRepository->createQueryBuilder('nd')
                       ->select('COUNT(nd.id) as send')
                       ->where('nd.notificationexecution = :notificationexecution')
                       ->groupBy('nd.notificationexecution')
                       ->setParameter('notificationexecution', $this)
                       ->getQuery()
                       ->getSingleResult();
                    
                    if($NSResult['send'] >= $notificationscheduling->getRepetitionNumber()){
                        //Cambia el status de Notificationexecution
                        $this->setStatus(0);
                        $doctrine->getManager()->persist($this);
                        $doctrine->getManager()->flush();
                        
                        $sendNotification = FALSE;
                    }
                    
                    break;
                case 'end_activity': //al finalizar la actividad
                    
                    $now = time();
                    
                    switch ($notificationscheduling->getEntityType()){
                        case 'workshopscheduling':
                            $entityScheduling = $em->getRepository('FishmanWorkshopBundle:Workshopscheduling')->find($notificationscheduling->getEntityId());
                            break;
                        case 'pollscheduling':
                            $entityScheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($notificationscheduling->getEntityId());
                            break;
                    }
                    
                    $enddate = strtotime($entityScheduling->getEnddate()->format('Y-m-d H:i:s'));
                    
                    if ($now > $enddate) {
                      
                        $this->setStatus(0);
                        $doctrine->getManager()->persist($this);
                        $doctrine->getManager()->flush();
                        
                        $sendNotification = FALSE;
                    }
                    
                    break;
            }
            
            if (!$sendNotification) {  
                $eaId = $this->getEntityId();
                $nsType = $this->getNotificationscheduling()->getNotificationType();
                $nsStatus = $this->getNotificationscheduling()->getNotificationTypeStatus();
                $nsInitdate = strtotime($this->getNextNotification()->format('Y/m/d'));
                $nsPredecessor = $this->getNotificationscheduling()->getId();
                
                self::activeNotificationexecutions($doctrine, $eaId, $nsType, $nsStatus, $nsInitdate, $nsPredecessor);
            }
            
            //Intervalo en días
            $days = $notificationscheduling->daysToNextNotification();
            if($days == 0){
                $this->setStatus(0);
                $doctrine->getManager()->persist($this);
                $doctrine->getManager()->flush();
                return FALSE;   
            }

            //Calcula la próxima fecha
            $interval = new \DateInterval('P' . $days . 'D');
            $nextNotification = new \DateTime();
            $nextNotification->add($interval);
            $this->setNextNotification($nextNotification);
            
            //Grabar la notificationexecution
            $doctrine->getManager()->persist($this);
            $doctrine->getManager()->flush();

            return TRUE;
        }
        else{
            //Cambia el status, para que no vuelva a aparecer en la lista
            $this->setStatus(0);
            $doctrine->getManager()->persist($this);
            $doctrine->getManager()->flush();
            return FALSE;
        }
    }

    /**
     * Leer las notificacion execution que deben ejecutarse hoy (cada una es un envio que debe hacerse),
     * y realiza el envio. En caso la notificación tenga repeticiones, calcula la siguiente fecha de envió
     * y la actualiza.
     * 
     * Por cada notificación que envía guarda un Notificacionsend.
     */
    public static function sendTodayNotifications($doctrine, $mailer, $logger, $router, $templating)
    {
        $day = new \Datetime();
        $sqlDay = $day->format('Y-m-d');

        $repository = $doctrine->getRepository('FishmanNotificationBundle:Notificationexecution');
        $result = $repository->createQueryBuilder('ne')
            ->innerJoin('ne.notificationscheduling', 'ns')
            ->where("ne.next_notification = :next_notification 
                     AND ns.status = 1
                     AND ne.status = 1
                     AND ns.execution_type = 'automatic'")
            ->setParameter('next_notification', $sqlDay)
            ->getQuery()
            ->getResult();
        
        if (!empty($result)) {
            foreach ($result as $nx) {
                $sendOn = $nx->validateSendingMessage($doctrine);
                if ($sendOn) {
                    $nx->sendNotification($doctrine, $mailer, $logger, $router, $templating, 'automatic');
                }
            }
        }
    }

    /**
     * Envío de notificaciones con disparador de la evaluación terminada.
     */
    public static function sendTriggerNotifications($doctrine, $mailer, $logger, $router, $templating, $eaId)
    {   
        $repository = $doctrine->getRepository('FishmanNotificationBundle:Notificationexecution');
        $result = $repository->createQueryBuilder('ne')
            ->innerJoin('ne.notificationscheduling', 'ns')
            ->where("ns.status = 1
                     AND ne.status = 1
                     AND ns.execution_type = 'trigger' 
                     AND ne.entity_id = :entityapplication")
            ->setParameter('entityapplication', $eaId)
            ->getQuery()
            ->getResult();
        
        if (!empty($result)) {
            foreach ($result as $nx) {
                $sendOn = $nx->validateSendingMessage($doctrine);
                if ($sendOn) {
                    $nx->sendNotification($doctrine, $mailer, $logger, $router, $templating, 'trigger');
                }
            }
        }
    }

    /**
     * Crea una instancia de Notificationexecution con los datos correspondientes
     */
    public static function generateNotificationexecution($ns, $wiId, $entityType, $entityId, $initDate = NULL,
                                                         $endDate = NULL, $nextNotification = NULL, $status = 1,
                                                         $profile = NULL, $doctrineRegistry = NULL){
        
        //Si es un tipo de evaluacion de una por evaluador
        if ($profile == 'evaluator' && $ns->getOnePerEvaluator()) {
            //Verifica si es que ya existe una notificacion para este evaluador
            $count = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationexecution')
               ->createQueryBuilder('nx')
               ->select('count(nx.id)')
               ->leftJoin('nx.notificationscheduling', 'ns')
               ->where("nx.workinginformation_id = :wiid AND ns.id = :ns")
               ->setParameter('wiid', $wiId)
               ->setParameter('ns', $ns->getId())
               ->getQuery()
               ->getSingleResult();

            if ($count[1] != 0) {
                return null;
            }
        }
        
        $nx = new Notificationexecution();
        $nx->setNotificationscheduling($ns);
        $nx->setInitdate($ns->getInitdate());
        $nx->setEnddate($ns->getEnddate());
        $nx->setNextnotification($nextNotification);
        $nx->setStatus($status);
        $nx->setWorkinginformationId($wiId);
        $nx->setEntityType($entityType);
        $nx->setEntityId($entityId);
        $nx->setProfile($profile);
        
        return $nx;
    }

    /**
     * Remueve las Notificationexecution con los datos correspondientes
     */
    public static function removeNotificationexecutions(DoctrineRegistry $doctrineRegistry, $nsId = '', $entitySType = '', $entitySId = '', $wiId = '', $entityAType = '', $entityAId = '')
    {   
        $em = $doctrineRegistry->getManager();
        
        // Recover Notificationexecutions
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationexecution');
        $queryBuilder = $repository->createQueryBuilder('nx')
            ->innerJoin('nx.notificationscheduling', 'nsc')
            ->where('nsc.entity_id = :entityid 
                    AND nsc.entity_type = :entitytype')
            ->setParameter('entityid', $entitySId)
            ->setParameter('entitytype', $entitySType);
        
        if ($nsId != '') {
            $queryBuilder
                ->andWhere('nsc.id = :notificationscheduling')
                ->setParameter('notificationscheduling', $nsId);
        }
        
        if ($wiId != '') {
            $queryBuilder
                ->andWhere('nx.workinginformation_id = :workinginformation')
                ->setParameter('workinginformation', $wiId);
        }
        
        if ($entityAType != '') {
            $queryBuilder
                ->andWhere('nx.entity_type = :applicationtype 
                        AND nx.entity_id = :applicationid')
                ->setParameter('applicationtype', $entityAType)
                ->setParameter('applicationid', $entityAId);
        }
        
        $nx_results = $queryBuilder->getQuery()->getResult();
            
        foreach ($nx_results as $nx_entity) {
            $em->createQueryBuilder()
                ->delete('FishmanNotificationBundle:Notificationsend ns')
                ->innerJoin('', 'nsc')
                ->where('ns.notificationexecution = :notificationexecution')
                ->setParameter('notificationexecution', $nx_entity->getId())
                ->getQuery()
                ->execute();
            
            $em->createQueryBuilder()
                ->delete('FishmanNotificationBundle:Notificationmanualdetails nmd')
                ->where('nmd.notificationexecution = :notificationexecution')
                ->setParameter('notificationexecution', $nx_entity->getId())
                ->getQuery()
                ->execute();

            $em->remove($nx_entity);
            $em->flush();
        }
        
    }

    /**
     * Recupera las notificationes del usuario y les agrega fecha de ejecución
     */
    public static function activeNotificationexecutions(DoctrineRegistry $doctrine, $eaId, $eaType, $status = FALSE, $initdate = FALSE, $predecessor = '-1', $denailStatus = FALSE)
    {
        $em = $doctrine->getManager();
        
        switch ($eaType) {
            case 'poll':
                $entityApplication = $em->getRepository('FishmanPollBundle:Pollapplication')->find($eaId);
                
                // get init date pollscheduling
                if (!$status) {
                    if (!$initdate) {
                        $initdate = strtotime($entityApplication->getPollscheduling()->getInitdate()->format('Y/m/d'));
                    }
                }
                else {
                    if ($entityApplication->getEnddate()) {
                        $initdate = strtotime($entityApplication->getEnddate()->format('Y/m/d'));
                    }
                    else {
                        $initdate = strtotime(date('Y/m/d'));
                    }
                }
                
                break;
            case 'activity':
                $entityApplication = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($eaId);
                
                // get init date workshopscheduling
                if (!$status) {
                    if (!$initdate) {
                        $initdate = strtotime($entityApplication->getWorkshopapplication()->getWorkshopscheduling()->getInitdate()->format('Y/m/d'));
                    }
                    
                }
                else {
                    if ($entityApplication->getEnddate()) {
                        $initdate = strtotime($entityApplication->getEnddate()->format('Y/m/d'));
                    }
                    else {
                        $initdate = strtotime(date('Y/m/d'));
                    }
                }
                
                break;
        }
        
        $repository = $doctrine->getRepository('FishmanNotificationBundle:Notificationexecution');
        $queryBuilder = $repository->createQueryBuilder('ne')
            ->innerJoin('ne.notificationscheduling', 'ns')
            ->where("ns.status = 1
                     AND ne.status = 1 
                     AND ne.entity_id = :entityapplication 
                     AND ns.predecessor = :predecessor")
            ->setParameter('entityapplication', $eaId)
            ->setParameter('predecessor', $predecessor);
        
        if ($predecessor != '-1' && !$denailStatus) {
            $queryBuilder
                ->andWhere('ns.notification_type_status = :status')
                ->setParameter('status', $status);
        }
        elseif ($predecessor != '-1' && $denailStatus) {
            $queryBuilder
                ->andWhere('ns.notification_type_status <> :status')
                ->setParameter('status', $status);
        }
        
        $result = $queryBuilder->getQuery()->getResult();
        
        if (!empty($result)) {
            foreach ($result as $nx) {
                $sendOn = $nx->validateSendingMessage($doctrine);
                if ($sendOn) {
                    // case since
                    if ($nx->getNotificationscheduling()->getSince() > 0 ) {
                        $initdate = $initdate + ($nx->getNotificationscheduling()->getSince()*24*60*60);
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                    else {
                        $nextNotification = new \DateTime(date('Y-m-d', $initdate));
                    }
                    
                    $nx->setNextNotification($nextNotification);
                    $em->persist($nx);
                    $em->flush();
                }
            }
        }

    }
    
    /**
     * generate notificationexecutions
     * 
     */
    public static function generateNotificationexecutions(DoctrineRegistry $doctrine, $ns, $nextNotification, 
                                                          $asigned, $entityType, $entityId, $wiId, $psOrWsType, 
                                                          $psOrWsId, $companyId) 
    {
        $em = $doctrine->getManager();
        
        if ($asigned == 'integrant') {
            $nx = self::generateNotificationexecution($ns, $wiId,
                      $entityType, $entityId, $ns->getInitdate(),
                      $ns->getEnddate(), $nextNotification, 1, 'integrant', $doctrine);
            if(!is_null($nx)){
                $em->persist($nx);
                $em->flush();
            }
        }
        
        if ($asigned == 'evaluated') {
            $nx = self::generateNotificationexecution($ns, $wiId,
                      $entityType, $entityId, $ns->getInitdate(),
                      $ns->getEnddate(), $nextNotification, 1, 'evaluated', $doctrine);
            if(!is_null($nx)){
                $em->persist($nx);
                $em->flush();
            }
        }
        
        if ($asigned == 'evaluator') {
            $nx = self::generateNotificationexecution($ns, $wiId,
                      $entityType, $entityId, $ns->getInitdate(),
                      $ns->getEnddate(), $nextNotification, 1, 'evaluator', $doctrine);
            if(!is_null($nx)){
                $em->persist($nx);
                $em->flush();
            }
        }
        
        if (in_array($asigned, array('boss', 'company', 'collaborator'))) {
            
            if ($psOrWsType == 'workshopscheduling') {
                $Repository = $doctrine->getRepository('FishmanWorkshopBundle:Workshopapplication');
                $eas = $Repository->createQueryBuilder('wa')
                          ->where("wa.profile = :profile 
                                  AND wa.workshopscheduling = :workshopscheduling 
                                  AND wa.deleted = 0")
                          ->setParameter('profile', $asigned) 
                          ->setParameter('workshopscheduling', $psOrWsId) 
                          ->getQuery()
                          ->getResult();
            }
            elseif ($psOrWsType == 'pollscheduling') {
                $Repository = $doctrine->getRepository('FishmanPollBundle:Pollschedulingpeople');
                $eas = $Repository->createQueryBuilder('psp')
                          ->where("psp.profile = :profile 
                                  AND psp.pollscheduling = :pollscheduling ")
                          ->setParameter('profile', $asigned) 
                          ->setParameter('pollscheduling', $psOrWsId) 
                          ->getQuery()
                          ->getResult();
            }
  
            //Persist notification execution for each pollscheduling
            foreach($eas as $ea) {
                $wiid = $ea->getWorkinginformation()->getId();
                $nx = self::generateNotificationexecution($ns, $wiid,
                          $entityType, $entityId, $ns->getInitdate(),
                          $ns->getEnddate(), $nextNotification, 1, $ea->getProfile(),
                          $doctrine);
                if(!is_null($nx)){
                    $em->persist($nx);
                    $em->flush();
                }
            }
        }
        
        if ($asigned == 'responsible') {
            //Todos los usuarios con ROL_TEACHER
            $Repository = $doctrine->getRepository('FishmanAuthBundle:User');
            $au = $Repository->createQueryBuilder('au')
                     ->where("au.roles LIKE :rol ")
                     ->setParameter('rol', '%ROLE_TEACHER%')
                     ->getQuery()
                     ->getResult();
            foreach ($au as $a) {
                $wi = $doctrine->getRepository('FishmanAuthBundle:Workinginformation')->findOneBy(array(
                    'user' => $a->getId(), 
                    'company' => $companyId
                ));
                if ($wi != '') {
                    $nx = self::generateNotificationexecution($ns, $wi->getId(),
                              $entityType, $entityId, $ns->getInitdate(),
                              $ns->getEnddate(), $nextNotification, 1, 'responsible', $doctrine);
                    if(!is_null($nx)){
                        $em->persist($nx);
                        $em->flush();
                    }
                }
            }
        }
    }

    /**
     * validate Sending Message
     */
    public function validateSendingMessage(DoctrineRegistry $doctrine)
    {
        $output = TRUE;
        
        $em = $doctrine->getManager();
        $type = $this->getNotificationScheduling()->getNotificationType();
        
        switch ($type) {
            case 'poll':
                
                if (!$this->getNotificationScheduling()->getNotificationTypeStatus()) {
                    $pa = $em->getRepository('FishmanPollBundle:Pollapplication')->find($this->getEntityId());
                    if ($pa->getFinished()) {
                        $output = FALSE;
                    }
                }
                elseif ($this->getNotificationScheduling()->getNotificationTypeStatus()) {
                    $pa = $em->getRepository('FishmanPollBundle:Pollapplication')->find($this->getEntityId());
                    if (!$pa->getFinished()) {
                        $output = FALSE;
                    }
                }
                
                break;
            case 'activity':
                
                if (!$this->getNotificationScheduling()->getNotificationTypeStatus()) {
                    $waa = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($this->getEntityId());
                    if ($waa->getFinished()) {
                        $output = FALSE;
                    }
                }
                elseif ($this->getNotificationScheduling()->getNotificationTypeStatus()) {
                    $waa = $em->getRepository('FishmanWorkshopBundle:Workshopapplicationactivity')->find($this->getEntityId());
                    if (!$waa->getFinished()) {
                        $output = FALSE;
                    }
                }
                
                break;
            case 'message':
                
                $output = TRUE;
                break;
        }
        
        return $output;
    }

    /**
     * Función para realizar pruebas de envíuo de notificaciones automáticas.
     */
    public static function sendAutomaticNotifications($doctrine, $mailer, $logger, $router, $templating, $date)
    {
        $day = new \Datetime($date);
        $sqlDay = $day->format('Y-m-d');
        
        $repository = $doctrine->getRepository('FishmanNotificationBundle:Notificationexecution');
        $result = $repository->createQueryBuilder('ne')
            ->innerJoin('ne.notificationscheduling', 'ns')
            ->where("ne.next_notification = :next_notification 
                     AND ns.status = 1
                     AND ne.status = 1
                     AND ns.execution_type = 'automatic'")
            ->setParameter('next_notification', $sqlDay)
            ->getQuery()
            ->getResult();
        
        if (!empty($result)) {
            foreach ($result as $nx) {
                $sendOn = $nx->validateSendingMessage($doctrine);
                if ($sendOn) {
                    $nx->sendNotification($doctrine, $mailer, $logger, $router, $templating, 'automatic');
                }
            }
        }
    }

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $notificationmanualdetails;


    /**
     * Add notificationmanualdetails
     *
     * @param Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationmanualdetails
     * @return Notificationexecution
     */
    public function addNotificationmanualdetail(\Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationmanualdetails)
    {
        $this->notificationmanualdetails[] = $notificationmanualdetails;
    
        return $this;
    }

    /**
     * Remove notificationmanualdetails
     *
     * @param Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationmanualdetails
     */
    public function removeNotificationmanualdetail(\Fishman\NotificationBundle\Entity\Notificationmanualdetails $notificationmanualdetails)
    {
        $this->notificationmanualdetails->removeElement($notificationmanualdetails);
    }

    /**
     * Get notificationmanualdetails
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getNotificationmanualdetails()
    {
        return $this->notificationmanualdetails;
    }
}
