<?php

namespace Fishman\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection; 
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\NotificationBundle\Entity\Notification
 */
class Notification 
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $head
     */
    private $head;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $entity_type
     */
    private $entity_type;

    /**
     * @var integer $entity_id
     */
    private $entity_id;

    /**
     * @var string $entity_name
     */
    private $entity_name;

    /**
     * @var boolen $entity_status
     */
    private $entity_status;

    /**
     * @var integer $entity_poll_id
     */
    private $entity_poll_id;
    
    /**
     * @var smallint $sequence
     */
    private $sequence;

    /**
     * @var string $subject
     */
    private $subject;

    /**
     * @var string $message
     */
    private $message;

    /**
     * @var string $ejecution_type
     */
    private $ejecution_type;
    
    /**
     * @var string $selected_trigger
     */
    private $selected_trigger;

    /**
     * @var integer $since
     */
    private $since;

    /**
     * @var integer $predecessor
     */
    private $predecessor;

    /**
     * @var boolean $replay
     */
    private $replay;

    /**
     * @var string $repeat_period
     */
    private $repeat_period;

    /**
     * @var smallint $repeat_range
     */
    private $repeat_range;

    /**
     * @var string $finish
     */
    private $finish;

    /**
     * @var integer $repetition_number
     */
    private $repetition_number;

    /**
     * @var string $notification_type
     */
    private $notification_type;

    /**
     * @var integer $notification_type_id
     */
    private $notification_type_id;

    /**
     * @var string $notification_type_name
     */
    private $notification_type_name;

    /**
     * @var boolean $notification_type_status
     */
    private $notification_type_status;

    /**
     * @var array $asigned
     */
    private $asigned;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;
    
    /**
     * @var boolean $one_per_evaluator
     */
    private $one_per_evaluator;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\NotificationBundle\Entity\Notificationscheduling", mappedBy="notification")
     */
    protected $notificationschedulings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notificationschedulings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set head
     *
     * @param string $head
     * @return Notification
     */
    public function setHead($head)
    {
        $this->head = $head;
    
        return $this;
    }

    /**
     * Get head
     *
     * @return string 
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Notification
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set entity_id
     *
     * @param integer $entityId
     * @return Notification
     */
    public function setEntityId($entityId)
    {
        $this->entity_id = $entityId;
    
        return $this;
    }

    /**
     * Get entity_id
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entity_id;
    }

    /**
     * Set entity_poll_id
     *
     * @param integer $entityPollId
     * @return Notification
     */
    public function setEntityPollId($entityPollId)
    {
        $this->entity_poll_id = $entityPollId;
    
        return $this;
    }

    /**
     * Get entity_poll_id
     *
     * @return integer 
     */
    public function getEntityPollId()
    {
        return $this->entity_poll_id;
    }

    /**
     * Set entity_type
     *
     * @param string $entityType
     * @return Notification
     */
    public function setEntityType($entityType)
    {
        $this->entity_type = $entityType;
    
        return $this;
    }

    /**
     * Get entity_type
     *
     * @return string 
     */
    public function getEntityType()
    {
        return $this->entity_type;
    }

    /**
     * Set entity_name
     *
     * @param string $entityName
     * @return Notification
     */
    public function setEntityName($entityName)
    {
        $this->entity_name = $entityName;
    
        return $this;
    }

    /**
     * Get entity_name
     *
     * @return string 
     */
    public function getEntityName()
    {
        return $this->entity_name;
    }

    /**
     * Set entity_status
     *
     * @param boolean $entityStatus
     * @return Notification
     */
    public function setEntityStatus($entityStatus)
    {
        $this->entity_status = $entityStatus;
    
        return $this;
    }

    /**
     * Get entity_status
     *
     * @return boolean 
     */
    public function getEntityStatus()
    {
        return $this->entity_status;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Notification
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Notification
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set ejecution_type
     *
     * @param string $ejecutionType
     * @return Notification
     */
    public function setEjecutionType($ejecutionType)
    {
        $this->ejecution_type = $ejecutionType;
    
        return $this;
    }

    /**
     * Get ejecution_type
     *
     * @return string 
     */
    public function getEjecutionType()
    {
        return $this->ejecution_type;
    }

    /**
     * Set selected_trigger
     *
     * @param string $selectedTrigger
     * @return Notification
     */
    public function setSelectedTrigger($selectedTrigger)
    {
        $this->selected_trigger = $selectedTrigger;
    
        return $this;
    }

    /**
     * Get selected_trigger
     *
     * @return string 
     */
    public function getSelectedTrigger()
    {
        return $this->selected_trigger;
    }

    /**
     * Set since
     *
     * @param integer $since
     * @return Notification
     */
    public function setSince($since)
    {
        $this->since = $since;
    
        return $this;
    }

    /**
     * Get since
     *
     * @return integer 
     */
    public function getSince()
    {
        return $this->since;
    }

    /**
     * Set predecessor
     *
     * @param integer $predecessor
     * @return Notification
     */
    public function setPredecessor($predecessor)
    {
        $this->predecessor = $predecessor;
    
        return $this;
    }

    /**
     * Get predecessor
     *
     * @return integer 
     */
    public function getPredecessor()
    {
        return $this->predecessor;
    }

    /**
     * Set replay
     *
     * @param boolean $replay
     * @return Notification
     */
    public function setReplay($replay)
    {
        $this->replay = $replay;
    
        return $this;
    }

    /**
     * Get replay
     *
     * @return boolean 
     */
    public function getReplay()
    {
        return $this->replay;
    }

    /**
     * Set finish
     *
     * @param string $finish
     * @return Notification
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;
    
        return $this;
    }

    /**
     * Get finish
     *
     * @return string 
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set repeat_period
     *
     * @param string $repeatPeriod
     * @return Notification
     */
    public function setRepeatPeriod($repeatPeriod)
    {
        $this->repeat_period = $repeatPeriod;
    
        return $this;
    }

    /**
     * Get repeat_period
     *
     * @return string 
     */
    public function getRepeatPeriod()
    {
        return $this->repeat_period;
    }

    /**
     * Set repeat_range
     *
     * @param integer $repeatRange
     * @return Notification
     */
    public function setRepeatRange($repeatRange)
    {
        $this->repeat_range = $repeatRange;
    
        return $this;
    }

    /**
     * Get repeat_range
     *
     * @return integer 
     */
    public function getRepeatRange()
    {
        return $this->repeat_range;
    }

    /**
     * Set repetition_number
     *
     * @param integer $repetitionNumber
     * @return Notificationscheduling
     */
    public function setRepetitionNumber($repetitionNumber)
    {
        $this->repetition_number = $repetitionNumber;
    
        return $this;
    }

    /**
     * Get repetition_number
     *
     * @return integer 
     */
    public function getRepetitionNumber()
    {
        return $this->repetition_number;
    }

    /**
     * Get asigned
     *
     * @return array 
     */
    public function getAsigned()
    {
        return $this->asigned;
    }

    /**
     * Set asigned
     *
     * @param array $asigned
     * @return Notification
     */
    public function setAsigned($asigned)
    {
        $this->asigned = $asigned;
    
        return $this;
    }

    /**
     * Set sequence
     *
     * @param smallint $sequence
     * @return Notification
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return smallint 
     */
    public function getSequence()
    {
        return $this->sequence;
    }
   
    /**
     * Set status
     *
     * @param boolean $status
     * @return Notification
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set one per evaluator notification
     *
     * @param boolean $one_per_evaluator
     * @return Notification
     */
    public function setOnePerEvaluator($one_per_evaluator)
    {
        $this->one_per_evaluator = $one_per_evaluator;
        return $this;
    }

    /**
     * Get one per evaluator value
     *
     * @return boolean 
     */
    public function getOnePerEvaluator()
    {
        return $this->one_per_evaluator;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Notification
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Notification
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Notification
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Notification
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set notification_type
     *
     * @param string $notificationType
     * @return Notification
     */
    public function setNotificationType($notificationType)
    {
        $this->notification_type = $notificationType;
    
        return $this;
    }

    /**
     * Get notification_type
     *
     * @return string 
     */
    public function getNotificationType()
    {
        return $this->notification_type;
    }

    /**
     * Set notification_type_id
     *
     * @param integer $notificationTypeId
     * @return Notification
     */
    public function setNotificationTypeId($notificationTypeId)
    {
        $this->notification_type_id = $notificationTypeId;
    
        return $this;
    }

    /**
     * Get notification_type_id
     *
     * @return integer
     */
    public function getNotificationTypeId()
    {
        return $this->notification_type_id;
    }

    /**
     * Set notification_type_name
     *
     * @param string $notificationType_Name
     * @return Notification
     */
    public function setNotificationTypeName($notificationTypeName)
    {
        $this->notification_type_name = $notificationTypeName;
    
        return $this;
    }

    /**
     * Get notification_type_name
     *
     * @return string
     */
    public function getNotificationTypeName()
    {
        return $this->notification_type_name;
    }

    /**
     * Set notification_type_status
     *
     * @param boolean $notificationType_Status
     * @return Notification
     */
    public function setNotificationTypeStatus($notificationTypeStatus)
    {
        $this->notification_type_status = $notificationTypeStatus;
    
        return $this;
    }

    /**
     * Get notification_type_status
     *
     * @return boolean
     */
    public function getNotificationTypeStatus()
    {
        return $this->notification_type_status;
    }

    public function __toString()
    {
         return $this->name;
    }

    /**
     * Add notificationschedulings
     *
     * @param Fishman\NotificationBundle\Entity\Notificationscheduling $notificationschedulings
     * @return Notification
     */
    public function addNotificationscheduling(\Fishman\NotificationBundle\Entity\Notificationscheduling $notificationschedulings)
    {
        $this->notificationschedulings[] = $notificationschedulings;
    
        return $this;
    }

    /**
     * Remove notificationschedulings
     *
     * @param Fishman\NotificationBundle\Entity\Notificationscheduling $notificationschedulings
     */
    public function removeNotificationscheduling(\Fishman\NotificationBundle\Entity\Notificationscheduling $notificationschedulings)
    {
        $this->notificationschedulings->removeElement($notificationschedulings);
    }

    /**
     * Get notificationschedulings
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getNotificationschedulings()
    {
        return $this->notificationschedulings;
    }

    /**
     * Get Notification
     */
    public static function getNotification(DoctrineRegistry $doctrineRegistry, $nId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notification');
        $result = $repository->createQueryBuilder('n')
            ->select('n.id, n.entity_id, n.head, n.name, n.notification_type type, n.notification_type_name type_name, 
                      n.notification_type_status type_status, n.subject, n.message, np.name predecessor, n.ejecution_type, 
                      n.selected_trigger, n.since, n.replay, n.repeat_period, n.repeat_range, n.finish, n.repetition_number, 
                      n.asigned, n.one_per_evaluator, n.sequence, n.status, n.created, n.changed, n.created_by, n.modified_by')
            ->leftJoin('FishmanNotificationBundle:Notification', 'np', 'WITH', 'n.predecessor = np.id')
            ->where('n.id = :notificationid')
            ->setParameter('notificationid', $nId)
            ->getQuery()
            ->getResult();
        
        $output = current($result);

        return $output;
    }

    /**
     * Get list type options
     * 
     */
    public static function getListTypeOptions() 
    {
        $output = array(
          'activity' => 'Actividad',
          'poll' => 'Encuesta',
          'message' => 'Mensaje'
        );
        
        return $output;
    }
    
    /**
     * Get list notification options
     * 
     */
    public static function getListNotificationOptions(DoctrineRegistry $doctrine, $entityType, $entityId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanNotificationBundle:Notification');
        $queryBuilder = $repository->createQueryBuilder('n')
            ->select('n.id, n.name')
            ->where('n.status = 1 
                    AND n.entity_type = :entity_type 
                    AND n.entity_id = :entity_id')
            ->setParameter('entity_type', $entityType)
            ->setParameter('entity_id', $entityId)
            ->orderBy('n.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $output[$r['id']] = $r['name'];
        }
        
        return $output;
    }
}