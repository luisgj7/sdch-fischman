<?php

namespace Fishman\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

/**
 * Fishman\NotificationBundle\Entity\Notificationmanualdetails
 */
class Notificationmanualdetails
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $status
     */
    private $status;

    /**
     * @var string $aditional_information
     */
    private $aditional_information;

    /**
     * @var integer $notification_send_id
     */
    private $notification_send_id;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\NotificationBundle\Entity\Notificationmanualsend", inversedBy="notificationmanualdetailss")
     * @ORM\JoinColumn(name="notificationmanualsend_id", referencedColumnName="id")
     */
    protected $notificationmanualsend;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\NotificationBundle\Entity\Notificationexecution", inversedBy="notificationexecutionmanualdetailss")
     * @ORM\JoinColumn(name="notificationexecution_id", referencedColumnName="id")
     */

    protected $notificationexecution;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Notificationmanualdetails
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set aditional_information
     *
     * @param string $aditionalInformation
     * @return Notificationmanualdetails
     */
    public function setAditionalInformation($aditionalInformation)
    {
        $this->aditional_information = $aditionalInformation;
    
        return $this;
    }

    /**
     * Get aditional_information
     *
     * @return string 
     */
    public function getAditionalInformation()
    {
        return $this->aditional_information;
    }

    /**
     * Set notification_send_id
     *
     * @param integer $notificationSendId
     * @return Notificationmanualdetails
     */
    public function setNotificationSendId($notificationSendId)
    {
        $this->notification_send_id = $notificationSendId;
    
        return $this;
    }

    /**
     * Get notification_send_id
     *
     * @return integer 
     */
    public function getNotificationSendId()
    {
        return $this->notification_send_id;
    }

    /**
     * Set notificationexecution 
     *
     * @param Fishman\NotificationBundle\Entity\Notificationexecution $notificationexecution
     * @return Notificationmanualdetails
     */
    public function setNotificationexecution(\Fishman\NotificationBundle\Entity\Notificationexecution $notificationexecution = null)
    {
        $this->notificationexecution = $notificationexecution;
    
        return $this;
    }

    /**
     * Get notificationexecution 
     *
     * @return Fishman\NotificationBundle\Entity\Notificationexecution 
     */
    public function getNotificationexecution()
    {
        return $this->notificationexecution;
    }
    

    /**
     * Set notificationmanualsend
     *
     * @param Fishman\NotificationBundle\Entity\Notificationmanualsend $notificationmanualsend
     * @return Notificationmanualdetails
     */
    public function setNotificationmanualsend(\Fishman\NotificationBundle\Entity\Notificationmanualsend $notificationmanualsend = null)
    {
        $this->notificationmanualsend = $notificationmanualsend;
    
        return $this;
    }

    /**
     * Get notificationmanualsend
     *
     * @return Fishman\NotificationBundle\Entity\Notificationmanualsend 
     */
    public function getNotificationmanualsend()
    {
        return $this->notificationmanualsend;
    }
}