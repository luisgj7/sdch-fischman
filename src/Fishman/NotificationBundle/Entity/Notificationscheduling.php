<?php

namespace Fishman\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry as DoctrineRegistry;

use Fishman\NotificationBundle\Entity\Notificationscheduling;

/**
 * Fishman\NotificationBundle\Entity\Notificationscheduling
 */
class Notificationscheduling 
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $head
     */
    private $head;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $entity_type
     */
    private $entity_type;

    /**
     * @var integer $entity_id
     */
    private $entity_id;

    /**
     * @var string $entity_name
     */
    private $entity_name;

    /**
     * @var boolean $entity_status
     */
    private $entity_status;

    /**
     * @var smallint $sequence
     */
    private $sequence;

    /**
     * @var string $subject
     */
    private $subject;

    /**
     * @var string $message
     */
    private $message;

    /**
     * @var string $execution_type
     */
    private $execution_type;
    
    /**
     * @var string $selected_trigger
     */
    private $selected_trigger;

    /**
     * @var integer $since
     */
    private $since;

    /**
     * @var integer $predecessor
     */
    private $predecessor;

    /**
     * @var boolean $replay
     */
    private $replay;

    /**
     * @var string $replay_period
     */
    private $repeat_period;

    /**
     * @var smallint $repead_range
     */
    private $repeat_range;

    /**
     * @var string $finish
     */
    private $finish;

    /**
     * @var integer $repetition_number
     */
    private $repetition_number;

    /**
     * @var string $notification_type
     */
    private $notification_type;

    /**
     * @var integer $notification_type_id
     */
    private $notification_type_id;

    /**
     * @var string $notification_type_name
     */
    private $notification_type_name;

    /**
     * @var boolean $notification_type_status
     */
    private $notification_type_status;

    /**
     * @var array $asigned
     */
    private $asigned;

    /**
     * @var \DateTime $initdate
     */
    private $initdate;

    /**
     * @var boolean $status
     */
    private $status;

    /**
     * @var \DateTime $enddate
     */
    private $enddate;

    /**
     * @var \DateTime $created
     */
    private $created;

    /**
     * @var \DateTime $changed
     */
    private $changed;

    /**
     * @var integer $created_by
     */
    private $created_by;

    /**
     * @var integer $modified_by
     */
    private $modified_by;

    /**
     * @var boolean $own
     */
    private $own;

    /**
     * @var boolean $one_per_evaluator
     */
    private $one_per_evaluator;

    /**
     * @ORM\ManyToOne(targetEntity="Fishman\NotificationBundle\Entity\Notification", inversedBy="notificationschedulings")
     * @ORM\JoinColumn(name="notification_id", referencedColumnName="id")
     */
    protected $notification;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\NotificationBundle\Entity\Notificationmanualsend", mappedBy="notificationscheduling")
     */
    protected $notificationmanualsends;

    /**
     * @ORM\OneToMany(targetEntity="Fishman\NotificationBundle\Entity\Notificationexecution", mappedBy="notificationscheduling")
     */
    protected $notificationexecutions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notificationexecutions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set head
     *
     * @param string $head
     * @return Notificationscheduling
     */
    public function setHead($head)
    {
        $this->head = $head;
    
        return $this;
    }

    /**
     * Get head
     *
     * @return string 
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Notificationscheduling
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set entity_id
     *
     * @param integer $entityId
     * @return Notificationscheduling
     */
    public function setEntityId($entityId)
    {
        $this->entity_id = $entityId;
    
        return $this;
    }

    /**
     * Get entity_id
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entity_id;
    }

    /**
     * Set entity_type
     *
     * @param string $entityType
     * @return Notificationscheduling
     */
    public function setEntityType($entityType)
    {
        $this->entity_type = $entityType;
    
        return $this;
    }

    /**
     * Get entity_type
     *
     * @return string 
     */
    public function getEntityType()
    {
        return $this->entity_type;
    }

    /**
     * Set entity_name
     *
     * @param string $entityName
     * @return Notificationscheduling
     */
    public function setEntityName($entityName)
    {
        $this->entity_name = $entityName;
    
        return $this;
    }

    /**
     * Get entity_name
     *
     * @return string 
     */
    public function getEntityName()
    {
        return $this->entity_name;
    }

    /**
     * Set entity_status
     *
     * @param boolean $entityStatus
     * @return Notificationscheduling
     */
    public function setEntityStatus($entityStatus)
    {
        $this->entity_status = $entityStatus;
    
        return $this;
    }

    /**
     * Get entity_status
     *
     * @return boolean 
     */
    public function getEntityStatus()
    {
        return $this->entity_status;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Notificationscheduling
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Notificationscheduling
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set execution_type
     *
     * @param string $executionType
     * @return Notificationscheduling
     */
    public function setExecutionType($executionType)
    {
        $this->execution_type = $executionType;
    
        return $this;
    }

    /**
     * Get execution_type
     *
     * @return string 
     */
    public function getExecutionType()
    {
        return $this->execution_type;
    }

    /**
     * Set selected_trigger
     *
     * @param string $selectedTrigger
     * @return Notificationscheduling
     */
    public function setSelectedTrigger($selectedTrigger)
    {
        $this->selected_trigger = $selectedTrigger;
    
        return $this;
    }

    /**
     * Get selected_trigger
     *
     * @return string 
     */
    public function getSelectedTrigger()
    {
        return $this->selected_trigger;
    }

    /**
     * Set since
     *
     * @param integer $since
     * @return Notificationscheduling
     */
    public function setSince($since)
    {
        $this->since = $since;
    
        return $this;
    }

    /**
     * Get since
     *
     * @return integer 
     */
    public function getSince()
    {
        return $this->since;
    }

    /**
     * Set predecessor
     *
     * @param integer $predecessor
     * @return Notificationscheduling
     */
    public function setPredecessor($predecessor)
    {
        $this->predecessor = $predecessor;
    
        return $this;
    }

    /**
     * Get predecessor
     *
     * @return integer 
     */
    public function getPredecessor()
    {
        return $this->predecessor;
    }

    /**
     * Set replay
     *
     * @param boolean $replay
     * @return Notificationscheduling
     */
    public function setReplay($replay)
    {
        $this->replay = $replay;
    
        return $this;
    }

    /**
     * Get replay
     *
     * @return boolean 
     */
    public function getReplay()
    {
        return $this->replay;
    }

    /**
     * Set finish
     *
     * @param string $finish
     * @return Notificationscheduling
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;
    
        return $this;
    }

    /**
     * Get finish
     *
     * @return string 
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set repeat_period
     *
     * @param string $repeatPeriod
     * @return Notificationscheduling
     */
    public function setRepeatPeriod($repeatPeriod)
    {
        $this->repeat_period = $repeatPeriod;
    
        return $this;
    }

    /**
     * Get repeat_period
     *
     * @return string 
     */
    public function getRepeatPeriod()
    {
        return $this->repeat_period;
    }

    /**
     * Set repeat_range
     *
     * @param integer $repeatRange
     * @return Notificationscheduling
     */
    public function setRepeatRange($repeatRange)
    {
        $this->repeat_range = $repeatRange;
    
        return $this;
    }

    /**
     * Get repeat_range
     *
     * @return integer 
     */
    public function getRepeatRange()
    {
        return $this->repeat_range;
    }

    /**
     * Set repetition_number
     *
     * @param integer $repetitionNumber
     * @return Notificationscheduling
     */
    public function setRepetitionNumber($repetitionNumber)
    {
        $this->repetition_number = $repetitionNumber;
    
        return $this;
    }

    /**
     * Get repetition_number
     *
     * @return integer 
     */
    public function getRepetitionNumber()
    {
        return $this->repetition_number;
    }

    /**
     * Set asigned
     *
     * @param array $asigned
     * @return Notificationscheduling
     */
    public function setAsigned($asigned)
    {
        $this->asigned = $asigned;
    
        return $this;
    }

    /**
     * Get asigned
     *
     * @return array 
     */
    public function getAsigned()
    {
        return $this->asigned;
    }

    /**
     * Set sequence
     *
     * @param smallint $sequence
     * @return Notificationscheduling
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return smallint 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

   /**
     * Set status
     *
     * @param boolean $status
     * @return Notificationscheduling
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set initdate
     *
     * @param \DateTime $initdate
     * @return Notificationscheduling
     */
    public function setInitdate($initdate)
    {
        $this->initdate = $initdate;
    
        return $this;
    }

    /**
     * Get initdate
     *
     * @return \DateTime 
     */
    public function getInitdate()
    {
        return $this->initdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return Notificationscheduling
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Notificationscheduling
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param \DateTime $changed
     * @return Notificationscheduling
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;
    
        return $this;
    }

    /**
     * Get changed
     *
     * @return \DateTime 
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set created_by
     *
     * @param integer $createdBy
     * @return Notificationscheduling
     */
    public function setCreatedBy($createdBy)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set modified_by
     *
     * @param integer $modifiedBy
     * @return Notificationscheduling
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modified_by = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modified_by
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Set one_per_evaluator
     *
     * @param boolean $onePerEvaluator
     * @return Notificationscheduling
     */
    public function setOnePerEvaluator($onePerEvaluator)
    {
        $this->one_per_evaluator = $onePerEvaluator;
        return $this;
    }

    /**
     * Get one_per_evaluator
     *
     * @return boolean 
     */
    public function getOnePerEvaluator()
    {
        return $this->one_per_evaluator;
    }

    /**
     * Set notification_type
     *
     * @param string $notificationType
     * @return Notificationscheduling
     */
    public function setNotificationType($notificationType)
    {
        $this->notification_type = $notificationType;
    
        return $this;
    }

    /**
     * Get notification_type
     *
     * @return string 
     */
    public function getNotificationType()
    {
        return $this->notification_type;
    }

    /**
     * Set notification_type_id
     *
     * @param integer $notificationTypeId
     * @return Notificationscheduling
     */
    public function setNotificationTypeId($notificationTypeId)
    {
        $this->notification_type_id = $notificationTypeId;
    
        return $this;
    }

    /**
     * Get notification_type_id
     *
     * @return integer
     */
    public function getNotificationTypeId()
    {
        return $this->notification_type_id;
    }

    /**
     * Set notification_type_name
     *
     * @param string $notificationTypeName
     * @return Notificationscheduling
     */
    public function setNotificationTypeName($notificationTypeName)
    {
        $this->notification_type_name = $notificationTypeName;
    
        return $this;
    }

    /**
     * Get notification_type_name
     *
     * @return string
     */
    public function getNotificationTypeName()
    {
        return $this->notification_type_name;
    }

    /**
     * Set notification_type_status
     *
     * @param boolean $notificationType_Status
     * @return Notification
     */
    public function setNotificationTypeStatus($notificationTypeStatus)
    {
        $this->notification_type_status = $notificationTypeStatus;
    
        return $this;
    }

    /**
     * Get notification_type_status
     *
     * @return boolean
     */
    public function getNotificationTypeStatus()
    {
        return $this->notification_type_status;
    }

    /**
     * Set own
     *
     * @param boolean $own
     * @return Notificationscheduling
     */
    public function setOwn($own)
    {
        $this->own = $own;
    
        return $this;
    }

    /**
     * Get own
     *
     * @return boolean 
     */
    public function getOwn()
    {
        return $this->own;
    }

    /**
     * Set notification
     *
     * @param Fishman\NotificationBundle\Entity\Notification $notification
     * @return Notificationscheduling
     */
    public function setNotification(\Fishman\NotificationBundle\Entity\Notification $notification = null)
    {
        $this->notification = $notification;
    
        return $this;
    }

    /**
     * Get notification
     *
     * @return Fishman\NotificationBundle\Entity\Notification 
     */
    public function getNotification()
    {
        return $this->notification;
    }

        public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Add notificationexecutions
     *
     * @param Fishman\NotificationBundle\Entity\Notificationexecution $notificationexecutions
     * @return Notificationscheduling
     */
    public function addNotificationexecution(\Fishman\NotificationBundle\Entity\Notificationexecution $notificationexecutions)
    {
        $this->notificationexecutions[] = $notificationexecutions;
    
        return $this;
    }

    /**
     * Remove notificationexecutions
     *
     * @param Fishman\NotificationBundle\Entity\Notificationexecution $notificationexecutions
     */
    public function removeNotificationexecution(\Fishman\NotificationBundle\Entity\Notificationexecution $notificationexecutions)
    {
        $this->notificationexecutions->removeElement($notificationexecutions);
    }

    /**
     * Get notificationexecutions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getNotificationexecutions()
    {
        return $this->notificationexecutions;
    }

    /**
     * Add notificationmanualsends
     *
     * @param Fishman\NotificationBundle\Entity\Notificationmanualsend $notificationmanualsends
     * @return Notificationscheduling
     */
    public function addNotificationmanualsend(\Fishman\NotificationBundle\Entity\Notificationmanualsend $notificationmanualsends)
    {
        $this->notificationmanualsends[] = $notificationmanualsends;
    
        return $this;
    }

    /**
     * Remove notificationmanualsends
     *
     * @param Fishman\NotificationBundle\Entity\Notificationmanualsend $notificationmanualsends
     */
    public function removeNotificationmanualsend(\Fishman\NotificationBundle\Entity\Notificationmanualsend $notificationmanualsends)
    {
        $this->notificationmanualsends->removeElement($notificationmanualsends);
    }

    /**
     * Get notificationmanualsends
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getNotificationmanualsends()
    {
        return $this->notificationmanualsends;
    }

    /**
     * Get Notificationscheduling
     */
    public static function getNotificationscheduling(DoctrineRegistry $doctrineRegistry, $nsId)
    {
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $result = $repository->createQueryBuilder('ns')
            ->select('ns.id, ns.entity_id, ns.head, ns.name, ns.notification_type_id, ns.notification_type type, 
                      ns.notification_type_name type_name, ns.notification_type_status type_status, ns.subject, 
                      ns.message, nsp.name predecessor, ns.execution_type, ns.selected_trigger, ns.since, ns.replay, 
                      ns.repeat_period, ns.repeat_range, ns.finish, ns.repetition_number, ns.asigned, ns.one_per_evaluator, 
                      ns.sequence, ns.status, ns.own, ns.created, ns.changed, ns.created_by, ns.modified_by')
            ->leftJoin('FishmanNotificationBundle:Notificationscheduling', 'nsp', 'WITH', 'ns.predecessor = nsp.id')
            ->where('ns.id = :notificationschedulingid')
            ->setParameter('notificationschedulingid', $nsId)
            ->getQuery()
            ->getResult();
        
        $output = current($result);

        return $output;
    }

    /**
     * Set Notificationschedulings
     */
    public static function cloneChildren(DoctrineRegistry $doctrineRegistry, $parentNId = -1, $parentNSId = -1, $entityScheduling, $entitySchedulingType, $entityId, $entityType, $userBy)
    {
        $em = $doctrineRegistry->getManager();
        
        $repository = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notification');
        $results = $repository->createQueryBuilder('n')
            ->where('n.entity_type = :type 
                    AND n.entity_id = :entityid 
                    AND n.predecessor = :predecessor')
            ->setParameter('type', $entityType)
            ->setParameter('entityid', $entityId)
            ->setParameter('predecessor', $parentNId)
            ->orderBy('n.id', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach ($results as $r) {
            $ns_entity  = new Notificationscheduling();
            
            $ns_entity->setHead($r->getHead());
            $ns_entity->setName($r->getName());
            $ns_entity->setNotification($r);
            $ns_entity->setEntityType($entitySchedulingType);
            $ns_entity->setEntityId($entityScheduling->getId());
            if ($entitySchedulingType == 'pollscheduling') {
                $ns_entity->setEntityName($entityScheduling->getTitle());
            }
            else {
                $ns_entity->setEntityName($entityScheduling->getName());
            }
            $ns_entity->setOnePerEvaluator($r->getOnePerEvaluator());
            $ns_entity->setEntityStatus($entityScheduling->getStatus());
            $ns_entity->setNotificationType($r->getNotificationType());
            
            if ($entitySchedulingType == 'workshopscheduling' && $r->getNotificationType() == 'activity' ) {
                $repository2 = $doctrineRegistry->getRepository('FishmanWorkshopBundle:Workshopschedulingactivity');
                $workshopschedulingactivity = $repository2->createQueryBuilder('wsa')
                    ->where('wsa.workshopactivity = :workshopactivity 
                            AND wsa.workshopscheduling = :workshopscheduling') 
                    ->setParameter('workshopactivity', $r->getNotificationTypeId())
                    ->setParameter('workshopscheduling', $entityScheduling)
                    ->orderBy('wsa.id', 'ASC')
                    ->getQuery()
                    ->getSingleResult();

                $ns_entity->setNotificationTypeId($workshopschedulingactivity->getId());
            }
            elseif ($entitySchedulingType == 'workshopscheduling' && $r->getNotificationType() == 'poll') {
                $repository2 = $doctrineRegistry->getRepository('FishmanPollBundle:Pollscheduling');
                $pollscheduling = $repository2->createQueryBuilder('ps')
                    ->where('ps.entity_poll_id = :entity_poll 
                            AND ps.poll = :poll 
                            AND ps.entity_type = :type
                            AND ps.entity_id = :workshopscheduling') 
                    ->setParameter('entity_poll', $r->getEntityPollId())
                    ->setParameter('poll', $r->getNotificationTypeId())
                    ->setParameter('type', 'workshopscheduling')
                    ->setParameter('workshopscheduling', $entityScheduling->getId())
                    ->orderBy('ps.id', 'ASC')
                    ->getQuery()
                    ->getSingleResult();

                $ns_entity->setNotificationTypeId($pollscheduling->getId());
            }
            else {
                $ns_entity->setNotificationTypeId($r->getNotificationTypeId());
            }

            $ns_entity->setNotificationTypeName($r->getNotificationTypeName());
            $ns_entity->setNotificationTypeStatus($r->getNotificationTypeStatus());
            $ns_entity->setSubject($r->getSubject());
            $ns_entity->setMessage($r->getMessage());
            $ns_entity->setPredecessor($parentNSId);
            $ns_entity->setExecutionType($r->getEjecutionType());
            $ns_entity->setSelectedTrigger($r->getSelectedTrigger());
            $ns_entity->setSince($r->getSince());
            $ns_entity->setReplay($r->getReplay());
            $ns_entity->setRepeatPeriod($r->getRepeatPeriod());
            $ns_entity->setRepeatRange($r->getRepeatRange());
            $ns_entity->setFinish($r->getFinish());
            $ns_entity->setRepetitionNumber($r->getRepetitionNumber());
            $r->getNotificationType() == 'poll';
            
            if ($entitySchedulingType == 'pollscheduling') {
                if ($entityScheduling->getType() == 'not_evaluateds') {
                    $asigneds = array();
                    $a = 0;
                    foreach ($r->getAsigned() as $a_key => $a_value) {
                        if ($a_value != 'evaluated') {
                            $asigneds[$a] = $a_value;
                            $a++;
                        }
                    }
                    $ns_entity->setAsigned($asigneds);
                }
                else {
                    $ns_entity->setAsigned($r->getAsigned());
                }
            }
            else {
                $ns_entity->setAsigned($r->getAsigned());
            }
            
            $ns_entity->setSequence($r->getSequence());
            $ns_entity->setStatus($r->getStatus());
            $ns_entity->setOwn(false);
        
            $ns_entity->setCreatedBy($userBy->getId());
            $ns_entity->setModifiedBy($userBy->getId());
            $ns_entity->setCreated(new \DateTime());
            $ns_entity->setChanged(new \DateTime());
            
            $em->persist($ns_entity);
            $em->flush();
            
            self::cloneChildren($doctrineRegistry, $r->getId(), $ns_entity->getId(), $entityScheduling, $entitySchedulingType, $r->getEntityId(), $r->getEntityType(), $userBy);
        }
    }

    /**
     * Una planificación de notificación se envía de forma manual en ese momento
     * $id, es el id de la planificación
     * type, manual or automatic. En caso de automático actualiza otros valores como el número de veces, o la próxima
     *                notificación.
     * @return, Notificationscheduling
     */
    public static function sendNotifications($id, DoctrineRegistry $doctrineRegistry, $mailer, $logger, $router, $templating, $type)
    {
        //Se recupera todas las notificación execution de esta notification scheduling
        $ns = $doctrineRegistry->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        $nxs = $ns->getNotificationexecutions();
        
        //Se recorren
        foreach($nxs as $nx)
        {
           // $nx->setNotificationscheduling($ns);
            $nx->sendNotification($doctrineRegistry, $mailer, $logger, $router, $templating, $type);
        }
        return $ns;
    }

    /**
     * @return, el número de días que hay entre una notificación y la siguiente.
     *          false en caso no exista siguiente notificación
     */
    public function daysToNextNotification()
    {
        //Si se tiene que aplicar una nueva repetición
        if ($this->getReplay()) {
            //Transforma la repetición en días
            switch($this->getRepeatPeriod()){
                case 'day':
                    return $this->getRepeatRange();
                case 'week':
                    return $this->getRepeatRange() * 7;
                case 'month':
                    return $this->getRepeatRange() * 30;
                case 'year':
                    return $this->getRepeatRange() * 365;
            }
        }
        else{
            return 0;
        }
    }
    
    /**
     * Get list notificationscheduling options
     * 
     */
    public static function getListNotificationschedulingOptions(DoctrineRegistry $doctrine, $entityType, $entityId) 
    {
        $output = array();
        
        $repository = $doctrine->getRepository('FishmanNotificationBundle:Notificationscheduling');
        $queryBuilder = $repository->createQueryBuilder('ns')
            ->select('ns.id, ns.name')
            ->where('ns.status = 1 
                    AND ns.entity_type = :entity_type 
                    AND ns.entity_id = :entity_id')
            ->setParameter('entity_type', $entityType)
            ->setParameter('entity_id', $entityId)
            ->orderBy('ns.name', 'ASC');
            
        $result = $queryBuilder->getQuery()->getResult();
        
        foreach($result as $r) {
            $output[$r['id']] = $r['name'];
        }
        
        return $output;
    }
}