<?php

namespace Fishman\NotificationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;

use Fishman\WorkshopBundle\Entity\Activity;

class NotificationType extends AbstractType
{
    private $doctrine;
    
    public function __construct($notification, $type, $typeid, Registry $doctrine)
    {
        $this->type = $type;
        $this->typeid = $typeid;
        $this->notification = $notification;
        $this->doctrine = $doctrine;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $activities_options = array();
        $notifications_options = array();
        
        // Determine if the "type" is enabled or not
        if ($this->type == 'poll') {
            $type = array( 
                'poll' => 'Encuesta', 
                'message' => 'Mensaje'
            );
        }
        else {
            $type = array(
                'activity' => 'Actividad', 
                'poll' => 'Encuesta', 
                'message' => 'Mensaje'
            );
        }
        
        // Recovers all activities with exception id 4
        $repository = $this->doctrine->getRepository('FishmanWorkshopBundle:Activity');
        $result = $repository->createQueryBuilder('a')
            ->select('a.id, a.name')
            ->where('a.status = 1
                    AND a.id <> 4')
            ->orderBy('a.name', 'ASC')
            ->getQuery()
            ->getResult();
            
        foreach($result as $r) {
            $activities_options[$r['id']] = $r['name'];
        }
        
        // Recovers all notifications
        $repository = $this->doctrine->getRepository('FishmanNotificationBundle:Notification');
        $result = $repository->createQueryBuilder('n')
            ->select('n.id, n.name')
            ->where('n.status = 1 
                    AND n.entity_type = :type 
                    AND n.entity_id = :entity_id')
            ->setParameter('type', $this->type)
            ->setParameter('entity_id', $this->typeid)
            ->orderBy('n.name', 'ASC')
            ->getQuery()
            ->getResult();
        
        foreach($result as $r) {
            if ($this->notification->getId() != $r['id']) {
                $notifications_options[$r['id']] = $r['name'];
            }
        }
        
        $builder
            ->add('name')
            ->add('head')
            ->add('notification_type', 'choice', array(
                'choices' => $type
            ))
            ->add('notification_type_id', 'hidden', array(
                'required' => false
            ))
            ->add('notification_type_name', 'text', array(
                'required' => false
            ))
            ->add('notification_type_status', 'choice', array(
                'choices' => array(
                    0 => 'Pendiente', 
                    1 => 'Enviado'
                ),
                'empty_value' => 'Choose an option', 
                'required' => false
            ))
            ->add('one_per_evaluator', 'checkbox', array(
                'label'   => 'Solo una notificación por evaluador',
                'required'  => false,
            ))
            ->add('subject')
            ->add('message', 'textarea', array(
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'medium' // simple, advanced, bbcode
                ),
            ))
            ->add('predecessor', 'choice', array(
                'choices' => $notifications_options,
                'empty_value' => 'choose an option',
                'required' => false
            ))
            ->add('ejecution_type', 'choice', array(
                'choices'   => array(
                    'automatic' => 'Automática', 
                    'manual' => 'Manual',
                    'trigger' => 'Con Disparador'
                )
            ))
            ->add('selected_trigger', 'choice', array(
                'choices'   => array(
                    'to_send' => 'Al enviar (require que el Estado de Encuesta/Actividad sea "Enviado")'
                ),
                'expanded' => true,
                'required' => false
            ))
            ->add('since', 'text', array( 
                'required' => false
            ))
            ->add('replay', 'choice', array(
                'choices' => array( 
                    1 => 'Si',
                    0 => 'No'
                )
            ))
            ->add('repeat_period', 'choice', array(
                'choices'   => array(
                    'day' => 'Cada día',
                    'week' => 'Cada semana',
                    'month' => 'Cada mes',
                    'year' => 'Cada año'
                ), 
                'empty_value' => 'choose an option', 
                'required' => false
            ))
            ->add('repeat_range', 'choice', array(
                'choices' => array(
                    1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 
                    7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12
                ), 
                'empty_value' => 'choose an option', 
                'required' => false
            ))
            ->add('finish', 'choice', array(
                'choices' => array(
                    'never' => 'Nunca', 
                    'after' => 'Después de (n) repeticiones', 
                    'end_activity' => 'Después de finalizar actividad'
                ),
                'expanded' => true,
                'required' => false
            ))
            ->add('repetition_number', 'text' , array( 
                'required' => false
            ))
            ->add('sequence', 'choice', array(
                'choices' => array(
                    1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 
                    8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 
                    15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20
                ), 
                'empty_value' => 'choose an option',
            ))
            ->add('status', 'choice', array(
                'choices'   => array(
                    1 => 'Activo', 
                    0 => 'Desactivo'
                ),
                'empty_value' => 'Choose an option'
            ))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\NotificationBundle\Entity\Notification'
        ));
    }

    public function getName()
    {
        return 'fishman_notificationbundle_notificationtype';
    }
}
