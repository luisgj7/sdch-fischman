<?php

namespace Fishman\NotificationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NotificationsendType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('entity_name')
            ->add('sended')
            ->add('subject')
            ->add('message')
            ->add('sent')
            ->add('workinginformation_id')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fishman\NotificationBundle\Entity\Notificationsend'
        ));
    }

    public function getName()
    {
        return 'fishman_notificationbundle_notificationsendtype';
    }
}
