<?php

namespace Fishman\NotificationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NotificationexecutionController extends Controller
{
    /**
     * Send one notificationexecution
     */
    public function sendAction($nxid, $nmdid, $status)
    {
        $result = $status;
        
        if ($status) {
            $doctrine = $this->getDoctrine();
            $mailer = $this->get('mailer');
            $logger = $this->get('logger');
            $router = $this->get('router');
            $templating = $this->get('templating');
            
            $em = $this->getDoctrine()->getManager();
            $nx = $em->getRepository('FishmanNotificationBundle:Notificationexecution')->find($nxid);
            if ($nx) {
                
                $result = TRUE;
                $sendOn = $nx->validateSendingMessage($doctrine);
                
                if ($sendOn) {
                    $result = $nx->sendNotification($doctrine, $mailer, $logger, $router, $templating, 'manual');
                    if($nmdid > 0){
                        $nmd = $em->getRepository('FishmanNotificationBundle:Notificationmanualdetails')->find($nmdid);
                        if($nmd){
                            $nmd->setStatus('sent');
                            $em->persist($nmd);
                            $em->flush();
                        }
                    }
                    else {
                        $result = FALSE;
                    }
                }
                else {
                    $result = FALSE;
                }
            }
            else {
                $result = FALSE;
            }
        }

        if($this->getRequest()->isXmlHttpRequest()){
            $response = new Response(json_encode(
                array('nxid' => $result)));
            return $response;
        }
        else {
            if(is_object($result)){
                $session = $this->getRequest()->getSession();
                $session->getFlashBag()->add('error', 'La notificación no pudo ser enviada. ' . $result->getMessage());

                return $this->redirect($this->generateUrl('notificationscheduling_manual_send_one_by_one', array(
                    'id' => $nx->getNotificationscheduling()->getId()
                )));
            }
            else {
                return $this->redirect($this->generateUrl('notificationscheduling_manual_send_one_by_one', array(
                    'id' => $nx->getNotificationscheduling()->getId()
                )));
            }
            
        }
    }

    /**
     * Send message 
     */
    public function sendmessageAction($nxid, $nmdid)
    {
        $em = $this->getDoctrine()->getManager();
        $notificationExecution = $em->getRepository('FishmanNotificationBundle:Notificationexecution')->find($nxid);

        $notificationManualDetails = $em->getRepository('FishmanNotificationBundle:Notificationmanualdetails')->find($nmdid);

        $workinginformation = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($notificationExecution->getWorkinginformationId());

        //Antes de enviar las notificaciones solicita una confirmación
        return $this->render('FishmanNotificationBundle:Notificationexecution:sendmessage.html.twig', array(
            'notificationexecution' => $notificationExecution,
            'notificationmanualdetails' => $notificationManualDetails,
            'workinginformation' => $workinginformation
        ));
    }

}
