<?php

namespace Fishman\NotificationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\NotificationBundle\Entity\Notificationscheduling;
use Fishman\NotificationBundle\Entity\Notificationexecution;
use Fishman\NotificationBundle\Entity\Notificationmanualsend;
use Fishman\NotificationBundle\Entity\Notificationmanualdetails;
use Fishman\NotificationBundle\Form\NotificationschedulingType;

/**
 * Notificationscheduling controller.
 *
 */
class NotificationschedulingController extends Controller
{
    /**
     * Lists all Notificationscheduling entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->findAll();

        return $this->render('FishmanNotificationBundle:Notificationscheduling:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Notificationscheduling entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notificationscheduling entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanNotificationBundle:Notificationscheduling:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Notificationscheduling entity.
     *
     */
    public function newAction()
    {
        $entity = new Notificationscheduling();
        $form   = $this->createForm(new NotificationschedulingType(), $entity);

        return $this->render('FishmanNotificationBundle:Notificationscheduling:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Notificationscheduling entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Notificationscheduling();
        $form = $this->createForm(new NotificationschedulingType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('notificationscheduling_show', array('id' => $entity->getId())));
        }

        return $this->render('FishmanNotificationBundle:Notificationscheduling:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Notificationscheduling entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notificationscheduling entity.');
        }

        $editForm = $this->createForm(new NotificationschedulingType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanNotificationBundle:Notificationscheduling:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Notificationscheduling entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notificationscheduling entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new NotificationschedulingType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('notificationscheduling_edit', array('id' => $id)));
        }

        return $this->render('FishmanNotificationBundle:Notificationscheduling:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanNotificationBundle:Notificationscheduling:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Notificationscheduling entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Notificationscheduling entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('notificationscheduling'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }
    
    /**
     * Envío manual de las notificaciones de una planificación.
     * Puede recibir, por get, un parámetro "destination" para al finalizar redirigir a esa ruta
     *
     * El control de acceso es porque está en una ruta que comienza con admin
     */
    public function manualsendAction(Request $request, $id)
    {
        $form = $this->createFormBuilder(array('id' => $id))
                     ->add('id', 'hidden')
                     ->getForm();

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $mailer = $this->get('mailer');
                $logger = $this->get('logger');
                $router = $this->get('router');
                $templating = $this->get('templating');

                $ns = Notificationscheduling::sendNotifications($id, $this->getDoctrine(), $mailer, $logger, $router, $templating,'manual');

                //message
                $session = $this->getRequest()->getSession();
                $session->getFlashBag()->add('status', 'Se enviaron las notificaciones.');

                //Devuelve a la página de vista de la planificación de la notificación
                //De acuerdo a si es una notificación de un taller o de una encuesta debe redireccionar a diferente lugar
                //TODO: para una mayor flexibilidad podría usarse el parámetro "destination"
                switch($ns->getEntityType()){
                    case 'pollscheduling':
                        return $this->redirect($this->generateUrl('pollschedulingnotification_show', array('id' => $id)));
                        break;
                    case 'workshopscheduling':
                        return $this->redirect($this->generateUrl('workshopschedulingnotification_show', array('id' => $id)));
                        break;
                    default:
                        return $this->redirect($this->generateUrl('workshopschedulingnotification_show', array('id' => $id)));
                        break;
                }
            }
        }

        //Antes de enviar las notificaciones solicita una confirmación
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        return $this->render('FishmanNotificationBundle:Notificationscheduling:manualsend.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Metodo creado para la historia Notification manual Envío de notificaciones uno a uno
     **/
    public function onebyoneAction(Request $request, $id)
    {
        $form = $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->add('notifications', 'choice', array(
                'required' => false, 
                'multiple' => true, 
                'expanded' => true
            ))
            ->getForm();
        
        //Antes de enviar las notificaciones solicita una confirmación
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationscheduling')->find($id);
        
        $dateSend = '';
        $ntStatus = self::getNameNotificationTypeStatus($entity->getNotificationTypeStatus());
        $nmsEntityType = $entity->getEntityType();
        $nmsNotificationType = $entity->getNotificationType();
        $nsEntity = FALSE;
        
        // Recover session variable workinginformation
        $session = $this->getRequest()->getSession();
        $winformation = $session->get('workinginformation');
        $rol = $session->get('currentrol');
        
        if (!$entity->getStatus()) {                        
            $session->getFlashBag()->add('status', 'Actualmente la notificación no está activa.');
            return $this->render('FishmanNotificationBundle:Notificationscheduling:onebyonemessage.html.twig', array(
                'entity' => $entity
            ));
        }
        elseif ($entity->getExecutionType() == 'automatic' || $entity->getExecutionType() == 'trigger') {
            $session->getFlashBag()->add('error', 'La notificación debe ser de ejecución "manual".');
            return $this->render('FishmanNotificationBundle:Notificationscheduling:onebyonemessage.html.twig', array(
                'entity' => $entity
            ));
        }
        
        $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationmanualsend');
        $query = $repository->createQueryBuilder('nms')
             ->where('nms.notificationscheduling = :notificationscheduling AND nms.status = :status')
             ->setParameter('notificationscheduling', $id)
             ->setParameter('status', $ntStatus)
             ->getQuery();
        
        try {
            $notificationManualSend = $query->getSingleResult();
        } catch (\Doctrine\Orm\NoResultException $e) {
            $notificationManualSend = false;
        }
        
        $notificationExecution = $entity->getNotificationexecutions();
        
        if (empty($notificationManualSend)) {
            
            $nManualSend = new Notificationmanualsend();
            $nManualSend->setWorkinginformation($winformation['id']);
            $nManualSend->setDate(new \DateTime());
            $nManualSend->setNotificationscheduling($entity);
            $nManualSend->setStatus($ntStatus);
            
            $em->persist($nManualSend);
            $em->flush();
            
            foreach ($notificationExecution as $ne) {
                $nManualDetails = new Notificationmanualdetails();
                $nManualDetails->setNotificationmanualsend($nManualSend);
                $nManualDetails->setNotificationexecution($ne);
                $nManualDetails->setStatus('pending');

                if($entity->getOnePerEvaluator()){
                    $nManualDetails->setAditionalInformation('Una notificacion por evaluador');
                }
                else{
                    $nManualDetails->setAditionalInformation('');
                }

                $nManualDetails->setNotificationSendId(NULL);

                $em->persist($nManualDetails);
                $em->flush();
            }

            $wiCreator = $this->get('security.context')->getToken()->getUser()->getNames() .' '.
                         $this->get('security.context')->getToken()->getUser()->getSurname() .' '.
                         $this->get('security.context')->getToken()->getUser()->getLastname();

        } else {
            
            $nManualSend = $notificationManualSend;
            $wiTemp = $em->getRepository('FishmanAuthBundle:Workinginformation')->find($winformation['id']);
            $wiCreator = $wiTemp->getUser()->getNames() .' '.
                         $wiTemp->getUser()->getSurname().' '.
                         $wiTemp->getUser()->getLastname();
            
            foreach ($notificationExecution as $ne) {
                
                //TODO: Tiene que identificar que sea del mismo NotificationManualSend
                $nmdex = $em->getRepository('FishmanNotificationBundle:Notificationmanualdetails')->findByNotificationexecution($ne);
                
                if (empty($nmdex)) {
                    $nManualDetails = new Notificationmanualdetails();
                    $nManualDetails->setNotificationmanualsend($nManualSend);
                    $nManualDetails->setNotificationexecution($ne);
                    $nManualDetails->setStatus('pending');

                    if($entity->getOnePerEvaluator()){
                        $nManualDetails->setAditionalInformation('Una notificacion por evaluador');
                    }
                    else{
                        $nManualDetails->setAditionalInformation('');
                    }

                    $nManualDetails->setNotificationSendId(NULL);

                    $em->persist($nManualDetails);
                    $em->flush();
                }

            }
        }
        
        // Get type and date
        if (is_array($nManualSend)) {
            foreach ($nManualSend as $nms) {
                $dateSend = $nms->getDate();
            }
        } else {
            $dateSend = $nManualSend->getDate();
            $nms = $nManualSend;
        }
        
        if ($nmsEntityType == 'workshopscheduling') {
            
            $pollscheduling = FALSE;
            
            if ($nmsNotificationType == 'poll') {
                
                $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entity->getNotificationTypeId());
                
                if ($pollscheduling->getType() != 'not_evaluateds') {
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationmanualdetails');
                    $notificationManualDetails = $repository->createQueryBuilder('nmd')
                        ->select('nmd.id, nmd.status, nmd.aditional_information, wa.profile, pa.finished, 
                                  nx.workinginformation_id, nx.id nxid, nx.profile, nx.entity_type, wi.id wiid, 
                                  wi.code, wi.email wi_email, u.names, u.surname, u.lastname, u.email,  
                                  ue.names ue_names, ue.surname ue_surname, ue.lastname ue_lastname, 
                                  ur.names ur_names, ur.surname ur_surname, ur.lastname ur_lastname')
                         ->where('nmd.notificationmanualsend = :nms AND wa.workshopscheduling = :ws')
                         ->innerJoin('FishmanNotificationBundle:Notificationexecution', 'nx', 'WITH', 'nx = nmd.notificationexecution')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = nx.workinginformation_id')
                         ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'u.id = wi.user')
                         ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'pa.id = nx.entity_id')
                         ->innerJoin('FishmanWorkshopBundle:Workshopapplication', 'wa', 'WITH', 'wa.id = pa.entity_application_id')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wie', 'WITH', 'wie.id = pa.evaluated_id')
                         ->innerJoin('FishmanAuthBundle:User', 'ue', 'WITH', 'ue.id = wie.user')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wir', 'WITH', 'wir.id = pa.evaluator_id')
                         ->innerJoin('FishmanAuthBundle:User', 'ur', 'WITH', 'ur.id = wir.user')
                         ->setParameter('nms', $nms)
                         ->setParameter('ws', $entity->getEntityId())
                         ->orderBy('wir.id', 'ASC', 'wie.id', 'ASC')
                         ->getQuery()
                         ->getResult();
                         
                }
                else {
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationmanualdetails');
                    $notificationManualDetails = $repository->createQueryBuilder('nmd')
                        ->select('nmd.id, nmd.status, nmd.aditional_information, wa.profile, pa.finished, 
                                  nx.workinginformation_id, nx.id nxid, nx.profile, nx.entity_type, wi.id wiid, 
                                  wi.code, wi.email wi_email, u.names, u.surname, u.lastname, u.email, 
                                  ur.names ur_names, ur.surname ur_surname, ur.lastname ur_lastname')
                         ->where('nmd.notificationmanualsend = :nms AND wa.workshopscheduling = :ws')
                         ->innerJoin('FishmanNotificationBundle:Notificationexecution', 'nx', 'WITH', 'nx = nmd.notificationexecution')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = nx.workinginformation_id')
                         ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'u.id = wi.user')
                         ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'pa.id = nx.entity_id')
                         ->innerJoin('FishmanWorkshopBundle:Workshopapplication', 'wa', 'WITH', 'wa.id = pa.entity_application_id')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wir', 'WITH', 'wir.id = pa.evaluator_id')
                         ->innerJoin('FishmanAuthBundle:User', 'ur', 'WITH', 'ur.id = wir.user')
                         ->setParameter('nms', $nms)
                         ->setParameter('ws', $entity->getEntityId())
                         ->orderBy('wir.id', 'ASC', 'wie.id', 'ASC')
                         ->getQuery()
                         ->getResult();
                     
                }
                
            }
            elseif ($nmsNotificationType == 'activity') {
                
                $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationmanualdetails');
                $notificationManualDetails = $repository->createQueryBuilder('nmd')
                    ->select('nmd.id, nmd.status, nmd.aditional_information, wa.profile, waa.finished, 
                              nx.workinginformation_id, nx.id nxid, nx.profile, nx.entity_type, wi.id wiid, 
                              wi.code, wi.email wi_email, u.names, u.surname, u.lastname, u.email')
                     ->where('nmd.notificationmanualsend = :nms AND wa.workshopscheduling = :ws')
                     ->innerJoin('FishmanNotificationBundle:Notificationexecution', 'nx', 'WITH', 'nx = nmd.notificationexecution')
                     ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = nx.workinginformation_id')
                     ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'u.id = wi.user')
                     ->innerJoin('FishmanWorkshopBundle:Workshopapplicationactivity', 'waa', 'WITH', 'waa.id = nx.entity_id')
                     ->innerJoin('waa.workshopapplication', 'wa')
                     ->setParameter('nms', $nms)
                     ->setParameter('ws', $entity->getEntityId())
                     ->orderBy('wi.id', 'ASC')
                     ->getQuery()
                     ->getResult();
                
            }
            elseif ($nmsNotificationType == 'message') {
                
                $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationmanualdetails');
                $notificationManualDetails = $repository->createQueryBuilder('nmd')
                    ->select('nmd.id, nmd.status, nmd.aditional_information, wa.profile, 
                              nx.workinginformation_id, nx.id nxid, nx.profile, nx.entity_type, wi.id wiid, 
                              wi.code, wi.email wi_email, u.names, u.surname, u.lastname, u.email')
                     ->where('nmd.notificationmanualsend = :nms AND wa.workshopscheduling = :ws')
                     ->innerJoin('FishmanNotificationBundle:Notificationexecution', 'nx', 'WITH', 'nx = nmd.notificationexecution')
                     ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = nx.workinginformation_id')
                     ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'u.id = wi.user')
                     ->innerJoin('FishmanWorkshopBundle:Workshopapplication', 'wa', 'WITH', 'wa.id = nx.entity_id')
                     ->setParameter('nms', $nms)
                     ->setParameter('ws', $entity->getEntityId())
                     ->orderBy('wi.id', 'ASC')
                     ->getQuery()
                     ->getResult();
                 
            }
             
        } elseif ($nmsEntityType == 'pollscheduling') {
            
            $pollscheduling = $em->getRepository('FishmanPollBundle:Pollscheduling')->find($entity->getEntityId());
            
            if ($nmsNotificationType == 'poll') {
                
                if ($pollscheduling->getType() != 'not_evaluateds') {
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationmanualdetails');
                    $notificationManualDetails = $repository->createQueryBuilder('nmd')
                        ->select('nmd.id, nmd.status, nmd.aditional_information, pa.finished, 
                                  nx.workinginformation_id, nx.id nxid, nx.profile, nx.entity_type, wi.id wiid,  
                                  wi.code, wi.email wi_email, u.names, u.surname, u.lastname, u.email,  
                                  ue.names ue_names, ue.surname ue_surname, ue.lastname ue_lastname, 
                                  ur.names ur_names, ur.surname ur_surname, ur.lastname ur_lastname')
                         ->innerJoin('FishmanNotificationBundle:Notificationexecution', 'nx', 'WITH', 'nx = nmd.notificationexecution')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = nx.workinginformation_id')
                         ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'u.id = wi.user')
                         ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'pa.id = nx.entity_id')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wie', 'WITH', 'wie.id = pa.evaluated_id')
                         ->innerJoin('FishmanAuthBundle:User', 'ue', 'WITH', 'ue.id = wie.user')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wir', 'WITH', 'wir.id = pa.evaluator_id')
                         ->innerJoin('FishmanAuthBundle:User', 'ur', 'WITH', 'ur.id = wir.user')
                         ->where('nmd.notificationmanualsend = :nms')
                         ->setParameter('nms', $nms)
                         ->orderBy('wir.id', 'ASC', 'wie.id', 'ASC')
                         ->getQuery()
                         ->getResult();
                    
                }
                else {
                    
                    $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationmanualdetails');
                    $notificationManualDetails = $repository->createQueryBuilder('nmd')
                        ->select('nmd.id, nmd.status, nmd.aditional_information, pa.finished, 
                                  nx.workinginformation_id, nx.id nxid, nx.profile, nx.entity_type, wi.id wiid, 
                                  wi.code, wi.email wi_email, u.names, u.surname, u.lastname, u.email, 
                                  ur.names ur_names, ur.surname ur_surname, ur.lastname ur_lastname')
                         ->innerJoin('FishmanNotificationBundle:Notificationexecution', 'nx', 'WITH', 'nx.id = nmd.notificationexecution')
                         ->innerJoin('nx.notificationscheduling', 'nsc')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = nx.workinginformation_id')
                         ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'u.id = wi.user')
                         ->innerJoin('FishmanPollBundle:Pollapplication', 'pa', 'WITH', 'pa.id = nx.entity_id')
                         ->innerJoin('FishmanAuthBundle:Workinginformation', 'wir', 'WITH', 'wir.id = pa.evaluator_id')
                         ->innerJoin('FishmanAuthBundle:User', 'ur', 'WITH', 'ur.id = wir.user')
                         ->where('nmd.notificationmanualsend = :nms')
                         ->setParameter('nms', $nms)
                         ->orderBy('wir.id', 'ASC', 'wie.id', 'ASC')
                         ->getQuery()
                         ->getResult();
                }
            }
            elseif ($nmsNotificationType == 'message') {
                
                $repository = $this->getDoctrine()->getRepository('FishmanNotificationBundle:Notificationmanualdetails');
                $notificationManualDetails = $repository->createQueryBuilder('nmd')
                    ->select('nmd.id, nmd.status, nmd.aditional_information, psp.profile, 
                              nx.workinginformation_id, nx.id nxid, nx.profile, nx.entity_type, wi.id wiid, 
                              wi.code, wi.email wi_email, u.names, u.surname, u.lastname, u.email')
                     ->where('nmd.notificationmanualsend = :nms AND psp.pollscheduling = :ps')
                     ->innerJoin('FishmanNotificationBundle:Notificationexecution', 'nx', 'WITH', 'nx = nmd.notificationexecution')
                     ->innerJoin('FishmanAuthBundle:Workinginformation', 'wi', 'WITH', 'wi.id = nx.workinginformation_id')
                     ->innerJoin('FishmanAuthBundle:User', 'u', 'WITH', 'u.id = wi.user')
                     ->innerJoin('FishmanPollBundle:Pollschedulingpeople', 'psp', 'WITH', 'psp.id = nx.entity_id')
                     ->setParameter('nms', $nms)
                     ->setParameter('ps', $entity->getEntityId())
                     ->orderBy('wi.id', 'ASC')
                     ->getQuery()
                     ->getResult();
                 
            }
        }
        
        $nmdSentCount = $repository->createQueryBuilder('nmd')
                 ->select('count(nmd.id)')
                 ->where('nmd.notificationmanualsend = :nms')
                 ->andWhere("nmd.status = 'sent'")
                 ->setParameter('nms', $nms->getId())
                 ->getQuery()
                 ->getSingleResult();
        
        return $this->render('FishmanNotificationBundle:Notificationscheduling:onebyone.html.twig', array(
            'ns_entity' => $pollscheduling, 
            'notification_manual_details' => $notificationManualDetails,
            'notification_manual_details_json' => json_encode($notificationManualDetails),
            'notification_manual_send' => $nManualSend,
            'user_creator' => $wiCreator,
            'date_send' => $dateSend,
            'entity' => $entity,
            'nmdsent' => $nmdSentCount[1],
            'nmdcount' => count($notificationManualDetails),
            'form'   => $form->createView()
        ));

    }
    
    /**
     * Message.
     *
     */
    public function confirmationDiscardAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // set flash messages
        $session = $this->getRequest()->getSession();
        
        $entity = $em->getRepository('FishmanNotificationBundle:Notificationmanualsend')->find($id);
        if (!$entity) {
            $session->getFlashBag()->add('error', 'No se puede encontrar la notificación de envío manual.');
            return $this->redirect($this->generateUrl('fishman_front_end_homepage'));
        }

        $form = $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();

        if ($request->isMethod('POST')) {
            
            $onebyoneData = $request->request->get('form', true);
            
            if ($onebyoneData) {
                
                $nmsPost = $em->getRepository('FishmanNotificationBundle:Notificationmanualsend')->find($id);
                
                $em->createQueryBuilder()
                    ->update('FishmanNotificationBundle:Notificationexecution ne')
                    ->set('ne.status', ':status')
                    ->where('ne.notificationscheduling = :nsid')
                    ->setParameter('status', TRUE)
                    ->setParameter('nsid', $nmsPost->getNotificationscheduling()->getId())
                    ->getQuery()
                    ->execute();
                    
                $nmsPost->setStatus('closed');
                $em->persist($nmsPost);
                $em->flush();
                
                if ($nmsPost->getNotificationscheduling()->getEntityType() == 'pollscheduling') {
                    return $this->redirect($this->generateUrl('pollschedulingnotification_show', array(
                        'id' => $nmsPost->getNotificationscheduling()->getId()
                    )));
                } elseif ($nmsPost->getNotificationscheduling()->getEntityType() == 'workshopscheduling') {
                    return $this->redirect($this->generateUrl('workshopschedulingnotification_show', array(
                        'id' => $nmsPost->getNotificationscheduling()->getId()
                    )));
                }

            }
        }

        return $this->render('FishmanNotificationBundle:Notificationscheduling:confirmationdiscard.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
        ));

    }

    /**
     * get name notification type status.
     *
     */
    public function getNameNotificationTypeStatus($value)
    {
        $output = '';
        
        switch ($value) {
          case 0:
            $output = 'pending';
            break;
          case 1:
            $output = 'send';
            break;
        }
        
        return $output;
    }
}
