<?php

namespace Fishman\NotificationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fishman\NotificationBundle\Entity\Notificationsend;
use Fishman\NotificationBundle\Form\NotificationsendType;

/**
 * Notificationsend controller.
 *
 */
class NotificationsendController extends Controller
{
    /**
     * Lists all Notificationsend entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('FishmanNotificationBundle:Notificationsend')->findAll();

        return $this->render('FishmanNotificationBundle:Notificationsend:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Notificationsend entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanNotificationBundle:Notificationsend')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notificationsend entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanNotificationBundle:Notificationsend:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Notificationsend entity.
     *
     */
    public function newAction()
    {
        $entity = new Notificationsend();
        $form   = $this->createForm(new NotificationsendType(), $entity);

        return $this->render('FishmanNotificationBundle:Notificationsend:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Notificationsend entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Notificationsend();
        $form = $this->createForm(new NotificationsendType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('notificationsend_show', array('id' => $entity->getId())));
        }

        return $this->render('FishmanNotificationBundle:Notificationsend:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Notificationsend entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanNotificationBundle:Notificationsend')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notificationsend entity.');
        }

        $editForm = $this->createForm(new NotificationsendType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FishmanNotificationBundle:Notificationsend:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Notificationsend entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FishmanNotificationBundle:Notificationsend')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notificationsend entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new NotificationsendType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('notificationsend_edit', array('id' => $id)));
        }

        return $this->render('FishmanNotificationBundle:Notificationsend:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
        
        // User
        $createdBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getCreatedBy());
        $createdByName = $createdBy->getNames() . ' ' . $createdBy->getSurname() . ' ' . $createdBy->getLastname();
        $modifiedBy = $this->getDoctrine()->getRepository('FishmanAuthBundle:User')->find($entity->getModifiedBy());
        $modifiedByName = $modifiedBy->getNames() . ' ' . $modifiedBy->getSurname() . ' ' . $modifiedBy->getLastname(); 

        return $this->render('FishmanNotificationBundle:Notificationsend:edit.html.twig', array(
            'entity'      => $entity,
            'createdByName' => $createdByName,
            'modifiedByName' => $modifiedByName,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Deletes a Notificationsend entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FishmanNotificationBundle:Notificationsend')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Notificationsend entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('notificationsend'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
